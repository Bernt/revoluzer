#!/usr/bin/python

import random
import sys
from optparse import OptionParser

usage = "usage: %prog [options]"
parser = OptionParser(usage)
parser.add_option("-r", action="store", dest="repeats", type="int", default=1, metavar="REP", help="produce REP genomes")
parser.add_option("-n", action="store", dest="n", type="int", metavar="N", help="produce genomes of length N")
parser.add_option("-u", action = "store_false", dest="signed", default = True, help = "unsigned" )
(options, args) = parser.parse_args()

if options.n == None or options.n < 2:
	sys.stderr.write("error: genome length must be >= 2")
	sys.exit()

random.seed()

g = [ x+1 for x in xrange(options.n) ]
for i in range( options.repeats ):
	random.shuffle( g )
	G = g
	if options.signed:
		for j in range(len(G)):
			G[j] *= random.choice([1,-1])

	print ">",i
	print " ".join( [str(x) for x in G ] )

