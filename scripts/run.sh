#!/bin/bash

infile=""
outfile=""
prog=""
maxfiles=0
i=0
# get the parameters
while [ $# -gt 0 ]
do
    case "$1" in
		--input) infile="$2"; shift;;
		--output) outfile="$2"; shift;;
		--prog) prog=$2; shift;;
		--maxdata) maxfiles=$2; shift;;
		*) prog=$prog" "$1;;
	esac
    shift
done

# test the parameters
if [ ${#infile} -eq 0 ]
then
	echo "No infile given"
	echo "Usage: run.sh -i infile -o outfile -p program [optional_parameters]"
	exit 
fi
if [ ${#outfile} -eq 0 ]
then
	echo "No outfile given"
	echo "Usage: run.sh -i infile -o outfile -p program [optional_parameters]"
	exit 
fi
if [ ${#prog} -eq 0 ]
then
	echo "No program given"
	echo "Usage: run.sh -i infile -o outfile -p program [optional_parameters]"
	exit 
fi

if [ -e $outfile ]
then
	echo "outfile exist -- aborting"
	exit
fi

# if infile is a file 
# iterate over all datasets (separated by ~filename) print the dataset 
# in a tempfile; execute the program on it; and delete the tmpfile
if [ -f $infile ]; then
	exec < $infile		# use file as stdin
	while read line		# read the lines
	do
		if [ `expr index "$line" \~` == 1 ]
		then

			tmpdir=/tmp/$RANDOM`date "+%H%M%S"`
			mkdir $tmpdir
			tmpfile=$tmpdir"/"${line:1}
			if [ -f $tmpfile ]; then
				rm $tmpfile
			fi
			for index in $(seq 1 $((${#data[@]}))); do
				echo ${data[index]} >> $tmpfile
			done
			
			echo "#"$tmpfile>> $outfile
			#nice -n 10 
			$prog $tmpfile >> $outfile

			rm $tmpfile
			rmdir $tmpdir
			
			unset data
			
			let "i += 1"
			if [ $maxfiles -ne 0 -a "$i" -gt "$maxfiles" ]
			then
				exit
			fi
			continue
		fi
		data[${#data[@]}+1]=$line
	done
elif [ -d $infile ]; then 	# if $2 is a directory

	for f in $(ls $infile); do
        if [ -f $infile"$f" ]; then
			echo "#"$f>> $outfile
			#nice -n 10 
			$prog $infile$f >> $outfile

			let "i += 1"
			if [ "$maxfiles" -ne 0 -a "$i" -gt "$maxfiles" ]
			then
				exit
			fi
        fi
	done
fi



