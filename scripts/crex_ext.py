from datetime import datetime
import crex
import os, random
from os import popen
import re
from reportlab.lib.units import cm
from reportlab.lib.colors import *
from reportlab.pdfgen import canvas

TMPPATH="/tmp/"
CNVPATH=""

# maximal number of negative blocks which are tried to reverse in a p-node
MAXPREV=4

def timestamp():
    return str(datetime.now()).replace(" ","_").replace(":","").replace("-","").replace(".","")

class node:
    def __init__(self, itree, p=None ):
        self.parent = p
        self.children = []
        self.text = ""
        self.type = ""
        self.sign = ""
        self.bgcolor = ""
        self.width = 0
        itreee = itree
        m = re.match("^(\|\w*\|)?([\+\-\? ])<(\d+)-(\d+)>([\(\[\{].*[\)\]\}])$", itree)
#        raise itree
        if m != None:
            if m.group(1) == None:
                if p == None:
                    self.bgcolor = ""
                else: 
                    self.bgcolor = p.bgcolor
            else:
                self.bgcolor = m.group(1)[1:-1]
            self.sign = m.group(2)
            self.start = m.group(3)
            self.end = m.group(4)
            itree = m.group(5)
        else:
            raise "no match ", itree
#            sign = "?"
#            self.start = -1
#            self.end = -1


        if itree[0] == "(" and itree[-1] == ")":
        	self.type = "pri"
        elif itree[0] == "[" and itree[-1] == "]":
            if self.sign == "+":
                self.type = "inc"
            elif self.sign == " ":
                self.type = "inc"
                self.sign = "+"
            elif self.sign == "-":
                self.type = "dec"

        if self.type != "pri" and self.type != "inc" and self.type != "dec":
        	raise "node type error", "sign "+self.sign+" brack "+itree[0]+itree[-1]+ "tree "+itreee

        # leaf check .. and label extraction
        m = re.match("^[\(\[\{]([^\(\[\{\)\]\}]+)[\)\]\}]$", itree)
        if m != None:
            self.text = m.group(1)
            return
        
        depth = 0
        stree=""
        for i in itree:
            if i==',' or i==';':
                continue
            if i=='(' or i=='[' or i=='{':
                if depth > 0:
                    stree+=i
                depth+=1
            elif i==')' or i==']' or i=='}':
                depth-=1
                if depth > 0:
                    stree+=i
                if depth == 1:
                    self.children.append( node( stree, self ) )
                    if len(self.children) > 0:
                        self.text += " "
                    self.text += self.children[-1].text
                    stree = ""
            else:
                stree+=i

    def distances(self, rdist=0):
        self.maxldist = 1
        self.rdist = rdist
        for c in self.children:
            ldist = c.distances( self.rdist + 1 ) + 1
            self.maxldist = max( self.maxldist, ldist )
        return self.maxldist

    def draw(self, canv, x, theight, colors={}, linewidth=1, margin=1, fontname="Helvetica", fontsize=20):
#        if self.bgcolor != "":
#            
        
        if colors.has_key(self.type+"color"):
            col = colors[self.type+"color"]
        else:
            col = "#000000"

        if self.bgcolor != "":
            canv.saveState()
            if colors.has_key(self.bgcolor+"color"):
                bgcol = colors[self.bgcolor+"color"]
            else:
                bgcol = "#000000"
            canv.setFillColor( HexColor( bgcol ) )
            fill = 1
        else:
            fill = 0
            # total space needed for the lines and the margins for the deepest branch of the tree
        tlmspace = theight*(margin + linewidth)
            # draw the rectangle
        y = tlmspace - self.maxldist*(margin+linewidth) 
        height = fontsize + 2*self.maxldist*(margin+linewidth) 
        height -= fontsize*0.3
        canv.saveState()
        canv.setStrokeColor(HexColor(col))
#        raise str(self.width), str(height)
        canv.rect(x, y, self.width, height, stroke=1, fill=fill)
        canv.restoreState()

            # draw the children
        offset = linewidth+margin
            # trigger the drawing of the children
            # the width needed for all the children is: the width needed by the children self 
            # (2 lines + 2 margins .. this is included in the children -> just add the children width)
            # and a marging between two consecutive children
        for i in range(len(self.children)):
            self.children[i].draw(canv, x + offset, theight = theight, colors=colors, linewidth=linewidth, margin= margin, fontname=fontname, fontsize=fontsize)
            offset += self.children[i].width
            if i < len(self.children)-1:
                offset += margin+2*(linewidth)
            
            # draw text
        if len(self.children) == 0:
            textx = x+ linewidth+margin
            canv.saveState()
            canv.setFillColor( "black" )
            canv.setStrokeColor("black")
            canv.drawString(textx, tlmspace, self.text)
            canv.restoreState()

        if self.bgcolor != "":
            canv.restoreState()


    def draw_tree(self, canv, x, colors={}, linewidth=1, margin=1, fontname="Helvetica", fontsize=20,
          distance=2, signboxscale = 2.0):
        """
        \param distance the number of boxes distance between levels of the tree
        """
        
        if colors.has_key(self.type+"color"):
            col = colors[self.type+"color"]
        else:
            col = "#000000"

        if self.bgcolor != "":
            canv.saveState()
            if colors.has_key(self.bgcolor+"color"):
                bgcol = colors[self.bgcolor+"color"]
            else:
                bgcol = "#000000"
            fill = 1
        else:
            fill = 0
            bgcol = "#000000"

        offset = 0
        boxheight = (2*margin+2*linewidth+fontsize)

            # draw the rectangle
        y = (self.maxldist -1 )*boxheight + (self.maxldist-1)*distance*boxheight
        height = 2*(margin+linewidth)+fontsize
        canv.saveState()
        canv.setStrokeColor(HexColor(col))
        canv.setFillColor( HexColor( bgcol ) )
        if self.type == "pri":
            if self.width < height:
                radius = self.width / 2.0
            else:
                radius = height / 2.0
            canv.roundRect(x, y, self.width, height, radius, fill=fill)
        else:
            canv.rect(x, y, self.width, height, fill=fill)
            
            # draw the sign rectangle
        height = width = boxheight / signboxscale
        sby = y+boxheight
        sbx = x + self.width / 2.0 - width/2.0
        if self.sign == '+' or self.sign == '-':
            canv.line( sbx + linewidth + margin, sby+height/2.0, sbx+width- (linewidth + margin), sby+height/2.0 )
        if self.sign == '+':
            canv.line( sbx+width/2.0, sby+linewidth + margin, sbx+width/2.0, sby+height-(linewidth + margin) )
        canv.rect(sbx, sby, width, height)
        canv.restoreState()

            # trigger the drawing of the children
        lines = []
        yself = (self.maxldist-1)*(distance+1)*boxheight
        for i in range(len(self.children)):
            self.children[i].draw_tree(canv, x + offset, colors=colors, linewidth=linewidth, margin= margin, fontname=fontname, fontsize=fontsize, distance=distance, signboxscale = signboxscale)
            ychild = ((self.maxldist-1)*(distance+1)*boxheight)-(distance*boxheight) + (boxheight/signboxscale)
            lines.append( [0, yself, x + offset+self.children[i].width/2.0 , ychild] )
            offset += self.children[i].width
            if i < len(self.children)-1:
                offset += margin+2*(linewidth)

            # draw lines to the children
        canv.saveState()
        canv.setLineWidth(linewidth/2.0)
        for i in range(len(self.children)):
            lines[i][0] = x+self.width/2.0
            canv.line(lines[i][0], lines[i][1], lines[i][2], lines[i][3])
            if not self.maxldist == self.children[i].maxldist + 1:
                ychild = ((self.children[i].maxldist )*(distance+1)*boxheight)-(distance*boxheight)+ (boxheight/signboxscale)
                canv.line( lines[i][2], lines[i][3], lines[i][2], ychild)
        canv.restoreState()

        # node text
        if len(self.children) == 0:
            offset += margin
            canv.drawString(x + margin, margin+linewidth, self.text)
            offset += canv.stringWidth(self.text, fontName = fontname, fontSize = fontsize)
            offset += margin
        else:
            y = (self.maxldist -1 )*boxheight + (self.maxldist-1)*distance*boxheight
            
            fs = fontsize
            while self.width < canv.stringWidth(self.text, fontName = fontname, fontSize = fs):
                fs -= 1
            canv.saveState()
            canv.setFont(fontname, fs)
            canv.drawCentredString(x + self.width/2.0, y+margin+linewidth, self.text)
            canv.restoreState()

    
    
    def html_list(self):
    	ret = "<li> %s - %s\n"%(self.text, self.type)
    	if len(self.children) > 0:
    		ret += "<ul>\n"
	     	for c in self.children:
	     		ret += "<li> %s </li>\n" %(c.html_list())
    		
    		ret += "</ul>\n"
    	ret += "</li>\n"
     	return ret

    def html_tab(self, prefix="", colors={}, borderwidth=2, margin=1):
        if colors.has_key(self.type+"color"):
            col = colors[self.type+"color"]
        else:
            col = "#000000"
            
        if self.bgcolor != "" and colors.has_key(self.bgcolor+"color"):
            bgcol = "background-color:" + colors[self.bgcolor+"color"]+";"
        elif self.bgcolor != "":
            raise "color ", self.bgcolor+"color"
        else: 
            bgcol = ""
            
        if prefix != "":
            id = "id=\""+prefix+"_"+self.start+"_"+self.end +"\""
        else:
            id = ""
        ret = "<table border=1 %s rules=\"none\" style=\"border-color:%s;%smargin:%spx;border-width:%spx\"><tr>" %(id, col, bgcol, str(margin), str(borderwidth))
        for c in self.children:
            ret += "<td> %s </td>" %(c.html_tab(prefix=prefix, colors=colors, borderwidth=borderwidth, margin=margin))
        if len(self.children) == 0:
            if self.text[0] != '+' and self.text[0] != '-':
                space = "-"
            else:
                space = ""
            ret += "<td><span style=\"visibility:hidden\">%s</span>%s</td>" %(space, self.text)
    	ret += "</tr></table>"
    	return ret

    def init_width(self, canv, linewidth=1, margin=1, fontname="Helvetica", fontsize=20):
        self.width = linewidth+margin
        for i in range(len(self.children)):
            self.children[i].init_width(canv, linewidth=linewidth,  margin=margin, fontname=fontname, fontsize=fontsize)
            self.width += self.children[i].width
            if i < len(self.children)-1:
                self.width += margin+2*(linewidth)
        if len(self.children) == 0:
            self.width += canv.stringWidth(self.text, fontName = fontname, fontSize = fontsize)
        self.width += linewidth+margin

    def init_width_tree(self, canv, linewidth=1, margin=1, fontname="Helvetica", fontsize=20):
        self.width = 0
        for i in range(len(self.children)):
            self.children[i].init_width_tree(canv, linewidth=linewidth, margin=margin, fontname=fontname, fontsize=fontsize)
            self.width += self.children[i].width
            if i < len(self.children)-1:
                self.width += margin+2*(linewidth)

        # node text
        if len(self.children) == 0:
            self.width += margin
            self.width += canv.stringWidth(self.text, fontName = fontname, fontSize = fontsize)
            self.width += margin

            # draw the rectangle


    def output(self):
        print self.type[0],
        for c in self.children:
            c.output()
        print self.text,
        print self.type[1],
        

class famdiag:
    def __init__(self, itree):
        """
        Initialize 
        \param itree the tree to draw (text description)
        """
        self.itree = node(itree)
        self.height = self.itree.distances()

    def draw(self, canv, x=0, colors={}, linewidth=1, margin=1, font="Helvetica", fontsize=20):
        """
        draw the famaily diagram to the canvas
        \param canv the canvas
        \param x x-coordinate of the top left corner
        \param y y-coordinate of the top left corner
        """
        
        self.itree.init_width(canv, linewidth=linewidth, margin=margin, fontname=font, fontsize=fontsize)
        canv.saveState()
        canv.setFont(font, fontsize)
        canv.setLineWidth(linewidth)
        
        self.itree.draw(canv, x, theight = self.height, colors=colors, linewidth=linewidth, margin=margin, fontname=font, fontsize=fontsize)
        canv.setPageSize( (self.itree.width, (fontsize+2*self.height*(margin+linewidth))) )
        canv.restoreState()

    def draw_tree(self, canv, x=0, colors={}, linewidth=1, margin=1, font="Helvetica", fontsize=20, distance = 2, signboxscale = 2.0):
        """
        draw the famaily diagram to the canvas
        \param canv the canvas
        \param x x-coordinate of the bottom left corner
        """
        self.itree.init_width_tree(canv, linewidth=linewidth, margin=margin, fontname=font, fontsize=fontsize)
        canv.saveState()
        canv.setFont(font, fontsize)
        canv.setLineWidth(linewidth)
        
        self.itree.draw_tree(canv, x, colors=colors, linewidth=linewidth, margin=margin, fontname=font, fontsize=fontsize, distance=distance, signboxscale = signboxscale )
        boxwidth = (2*(margin+linewidth)+fontsize)
        canv.setPageSize( (self.itree.width, (self.height + (distance*(self.height-1)))*boxwidth + boxwidth/signboxscale)  )
        canv.restoreState()

    def html_list(self):
    	"""
    	return html list representation of the tree
    	"""
    	return "<ul>"+self.itree.html_list()+"</ul>"

    def html_tab(self, prefix="", colors={}, borderwidth=2, margin=1):
    	"""
    	return html table representation of the tree
    	\param colors dict specifying inccolor, deccolor, pricolor
    	"""
    	return self.itree.html_tab(prefix=prefix, colors=colors, borderwidth=borderwidth, margin=margin)

def abs_gene(g):
    """
    return the absulute value of a gene, i.e. remove a possible '-'
    """
    if g[0] == '-':
        return g[1:]
    else:
        return g

def append_if(p, g, map):
    i = gene2int(g, map )
    if i != None:
        p.append(i)

def check_geneset( self ):
    """
    check if each gene in the dataset occurs once per genome
    append warnings to the session
    """
    data = self.REQUEST.SESSION["crex_data"]
    
    if len(data) < 2:
        self.warnerr_append("error", "only %d genome(s) found in the data, nothing to compare" %( len(data) ))

    geneset = {}
    for d in range(len(data)):
        for g in data[d]["genome"]:
            if g[0] == '-':
                absg = g[1:]
            else:
                absg = g
            if not geneset.has_key(absg):
                geneset[absg] = [0]*len(data)
            geneset[absg][d]+=1
            
    # check for multiplied genes
    for g in geneset:
        for i in range(len(geneset[g])):
            if geneset[g][i] > 1:
                self.warnerr_append("warning", "%s contains %d copies of %s" %(data[i]['name'], geneset[g][i], g))
    
    # check that every gene occuring in the data set occurs in all genomes
    for g in geneset:
        occ = []
        occ.append([])
        occ.append([])
        for i in range(len(geneset[g])):
            if geneset[g][i] == 0:
                occ[0].append(data[i]['name'])
            else:
                occ[1].append(data[i]['name'])

        if len(occ[0]) > 0:
            if len(occ[0]) <= len(occ[1]):
                self.warnerr_append("warning", "%s does not occur in %s" %(g, ", ".join(occ[0])))
            else:
                self.warnerr_append("warning", "%s only occurs in %s" %(g, ", ".join(occ[1])))


def delete_duplicates( self ):
    """
    delete duplicate gene orders from the session
    """

    session = self.REQUEST.SESSION

    todel = [ None for i in range(len(session["crex_data"]))]
    for i in range(len(session["crex_data"])):
        if todel[i] != None:
            continue

        for j in range(i+1, len(session["crex_data"])):
            p1, p2, map = genome2permutation(self, session["crex_data"][i], session["crex_data"][j])
            
            if equal(p1, p2, session['crex_circular']):
                todel[j] = i
#                self.warnerr_append("info", "== "+str(len(session["crex_data"][i]['genome']))+" "+str(session["crex_data"][i]['genome'])+"<br>"+str(len(session["crex_data"][j]['genome']))+" "+str(session["crex_data"][j]['genome']))
#            else:
#                self.warnerr_append("info", "!= "+str(session["crex_data"][i]['genome'])+str(session["crex_data"][j]['genome']))

    for i in range(len(session["crex_data"])-1, -1, -1):
        if( todel[i] != None ):
            self.warnerr_append("warning", "removing: %s (equal to %s)" %(session["crex_data"][i]["name"], session["crex_data"][todel[i]]["name"]) )
            session["crex_data"][todel[i]]["name"] += ", "+session["crex_data"][i]["name"]
            del session["crex_data"][i]
            
def distance(self, i, j):
    """
    get the distance of two gene orders, bp, ci, or rd
    """
    session = self.REQUEST.SESSION
    p1, p2, map = genome2permutation(self, session["crex_data"][i], session["crex_data"][j])
    
    if not session.has_key("crex_distance") or session["crex_distance"] == "common":
        dist = crex.cnt_intervals(p1, p2, session['crex_circular'])
        cdist = dist
        maxd = len(p1)*len(p2)/2
        if session['crex_circular'] != 0:
            maxd *= 2
    elif session["crex_distance"] == "breakpoint":
        maxd = len(p1)-1
        if session['crex_circular'] != 0:
            maxd += 1
        dist = crex.cnt_breakpoints(p1, p2, session['crex_circular'])
        cdist = maxd - dist
    elif session["crex_distance"] == "reversal":
        maxd = len(p1)+1
        if session['crex_circular'] != 0:
            maxd -= 1
        dist = crex.cnt_reversals(p1, p2, session['crex_circular'])
        cdist = maxd - dist
        
    col = str( hex(128 + int( float(cdist)/maxd*128 )) )[2:]
    if len(col) == 1:
        col = "0"+col
    col += col+col
    return {'number':dist, 'color':"#"+col}


def equal(p1, p2, circular):
    """
    checks if two gene orders are equal
    """
    
    if len(p1) != len(p2):
        return False
    
    return crex.equal(p1, p2, circular)


def famdiag_export( self ):
    request = self.REQUEST
    session = request.SESSION
    response = request.RESPONSE
    sessionid = session.getBrowserIdManager().getBrowserId()
    
    if request.has_key("tree"):
        tree = request["tree"]
    
    if request.has_key("type"):
        if request["type"] == None or request["type"] == "None":
            type="famd"
        else:
            type = request["type"]

    if request.has_key("filetype"):
        if request["filetype"] == None or request["filetype"] == "None":
            filetype="png"
        else:
            filetype = request["filetype"]
    else:
        filetype="png"

    
    fname=TMPPATH+sessionid+timestamp()
    c = canvas.Canvas(fname+".pdf")
    
    fd = famdiag( tree )
    
    colors = {"inccolor":session["crex_inccolor"], "deccolor":session["crex_deccolor"], "pricolor":session["crex_pricolor"], \
           "rev1color":session["crex_rev1color"], "tra1color":session["crex_tra1color"], "tra2color":session["crex_tra2color"]}

    if type=="famd":
        fd.draw(c, colors=colors, font="Helvetica", fontsize=20, linewidth=session["crex_linewidth"], margin=session["crex_margin"])
    elif type=="tree":
        fd.draw_tree(c, colors=colors, font="Helvetica", fontsize=20, linewidth=session["crex_linewidth"], margin=session["crex_margin"])
    else:
        raise "export", "unknown type %s"%type
    
    c.showPage()
    c.save()

    if filetype == "pdf":
        data = c.getpdfdata()
    elif filetype == "eps":
        popen("pdftops -eps "+fname+".pdf")
    elif filetype == "ps":
        popen("pdftops "+fname+".pdf")
    else:
        # ugly workaround for strange ghostscript effects on the server: pdf -> eps -> image (but it works)
        popen("pdftops -eps "+fname+".pdf")
        popen(CNVPATH+"convert -trim "+fname+".eps "+fname+"."+filetype)
    if filetype != "pdf":
        f = open(fname+"."+filetype)
        data = f.read()
        f.close()

    response.setHeader("Content-Type", filetypes()[filetype]) 
    response.setHeader("Content-Length", len(data))
    return data

#def famdiag_img( self, tree ):


#def famdiag_img(self, i=-1, j=-1, font="Helvetica", fontsize=20, margin=1, linewidth=1):
#    """
#    draw the family diagram as image
#    """
#    t = tree(self, i, j)
#    return famdiag_img(self, t)

#def famdiag_tree_img( self, tree ):
#    """
#    draw the family diagram .. given the tree in paranthesised form
#    """
#    fd = famdiag( tree )
#
#    fname=TMPPATH+sessionid+timestamp()
#    c = canvas.Canvas(fname+".pdf")
#
#    colors = {"inccolor":session["crex_inccolor"], "deccolor":session["crex_deccolor"], "pricolor":session["crex_pricolor"], \
#           "rev1color":session["crex_rev1color"], "tra1color":session["crex_tra1color"], "tra2color":session["crex_tra2color"],\
#           "revtra1color":session["crex_revtra1color"], "revtra2color":session["crex_revtra2color"], "tdl1color":session["crex_tdl1color"],\
#           "tdl2color":session["crex_tdl2color"]}
#
#    
#    c.showPage()
#    c.save()
##    data = c.getpdfdata()
#    os.popen(CNVPATH+"convert "+fname+".pdf "+fname+".png")
#    f = open(fname+".png")
#    data = f.read()
#    f.close()
#
#    response.setHeader("Content-Type", "image/png") 
#    response.setHeader("Content-Length", len(data))
#    return data

#def famdiag_tree_img( self, i=-1, j=-1, font="Helvetica", fontsize=20, margin=1, linewidth=1 ):
#    """
#    draw the family diagram of two parmutations from the session as image 
#    \param i and j: the indices of the permutations 
#    """
#    
#    t = tree(self, i, j, pfx)
#    return famdiag_img( t )

def famdiag_html( self, i, j, pfx):
    """
    draw the family diagram as html table(s)
    take the data from the session
    """

    session = self.REQUEST.SESSION

    t = tree(self, i, j, pfx)
    fd = famdiag( t )

    colors = {"inccolor":session["crex_inccolor"], "deccolor":session["crex_deccolor"], "pricolor":session["crex_pricolor"], \
           "rev1color":session["crex_rev1color"], "tra1color":session["crex_tra1color"], "tra2color":session["crex_tra2color"]}
    return fd.html_tab(colors=colors, prefix=pfx, borderwidth=session["crex_linewidth"], margin=session["crex_margin"])
CNVPATH
def famdiag_html_string( self, tree, pfx):

    session = self.REQUEST.SESSION
    
    fd = famdiag( tree )
    colors = {"inccolor":session["crex_inccolor"], "deccolor":session["crex_deccolor"], "pricolor":session["crex_pricolor"], \
           "rev1color":session["crex_rev1color"], "tra1color":session["crex_tra1color"], "tra2color":session["crex_tra2color"]}

    return fd.html_tab(colors=colors, prefix=pfx, borderwidth=session["crex_linewidth"], margin=session["crex_margin"])

def filetypes():
    return {"eps":"image/x-eps", "ps":"application/postscript", "pdf":"application/pdf", "jpg":"image/jpg", "png":"image/png"}

def fonts(self):
    request = self.REQUEST
    response = self.REQUEST.RESPONSE
    session = self.REQUEST.SESSION

    c = canvas.Canvas(TMPPATH+session.getBrowserIdManager().getBrowserId()+".pdf")
    return c.getAvailableFonts()

def gene2int( g, namemap):
    """
    get the integer for a name
    """
    if g[0] == '-':
        absg = g[1:]
        sign = -1
    else:
        absg = g
        sign = +1
    if namemap.has_key(absg) and namemap[absg] > 0:
        return sign * namemap[absg]
    
    return None

def genome2permutation(self, d1, d2):
    """
    get permutations (of integers)
    """
    err=["",""]
    
    occ = {}
    p1 = []
    p2 = []


    # count the occurences of the genes in the two genomes
    for i in d1['genome']:
        if i[0] == '-':
            i=i[1:]
        if not occ.has_key(i):
            occ[i] = [0, 0]
        occ[i][0] += 1
    
    for i in d2['genome']:
        if i[0] == '-':
            i=i[1:]
        if not occ.has_key(i):
            occ[i] = [0, 0]
        occ[i][1] += 1

    # check the occurences (they have to be (1,1))
    n = 1
    k = occ.keys()
    for o in k:
        if occ[o][0] == 1 and occ[o][1] == 1:
            occ[o] = n
            n += 1
        else:
            if occ[o][0] != 1 :
                err[0] += "%s(%dx), " %(o, occ[o][0])
            if occ[o][1] != 1 :
                err[1] += "%s(%dx), " %(o, occ[o][1])
            del(occ[o])

    for i in d1['genome']:
        append_if( p1, i, occ )
    
    for i in d2['genome']:
        append_if( p2, i, occ )

#    raise str(d1['genome'])+" ... "+str(d2['genome'])+" ... "+str(occ), str(p1)+" ... "+str(p2)

    map = [ "" for i in range(len(occ)+1) ]
    for i in occ.keys():
        map[ occ[i] ] = i

#    error = ""
#    for i in range(len(err)):
#        if err[i] != "":
#            if i == 0:
#                error += d1['nam']+": "
#            else:
#                error += d2['nam']+": "
#            error += err[i][:-2]+" &rarr; delete<br>"
#        
#    if error != "":
#        error = "comparison: "+ d1['nam']+" - "+d2['nam']+":<br>"+error
#        self.warnerr_append("error", error)

    return p1, p2, map

#def intervals(self, i, j):
#    """
#    get the number of common intervals
#    """
#    session = self.REQUEST.SESSION
#    
#    p1, p2, map = genome2permutation(self, session["data"][i], session["data"][j])
##    raise str(p1), str(p2)
#    
#    	# maximal number of intervals
#
#    col = str( hex(128 + int( float(cnt)/max*128 )) )[2:]
#    if len(col) == 1:
#    	col = "0"+col
#    col += col+col
#    return {'number':cnt, 'color':"#"+col}

#def breakpoints(self, i, j):
#    """
#    get the number of breakpoints
#    """
#    session = self.REQUEST.SESSION
#    
#    p1, p2, map = genome2permutation(self, session["data"][i], session["data"][j])
##    raise str(p1), str(p2)
#    cnt = 
#    
    


def read_genomes( self, data ):
    """
    read genomes into the session
    """
    
    session = self.REQUEST.SESSION
    session["crex_data"] = []
    session["crex_ubiq"] = []

    genome = []
    name = ""
    # read the data and store it in the session
    lines = data.splitlines()
    for l in lines:
        l = re.sub("[\[\{\(\)\}\]]","", l)
    	if len(l) == 0 or l[0] == '#':
    		continue
    	elif l[0] == '>':
    		name = l[1:].lstrip().rstrip()
    	else:
    		genome = l.replace(","," ").lstrip().rstrip().split()
    		session["crex_data"].append({ "genome":genome, "name":name })

    counter = {}
        # determine the genes which are in all genomes
    for d in session['crex_data']:
        for g in d['genome']:
            g = abs_gene(g)
            if not counter.has_key( g ):
                counter[g] = 0
            counter[g] += 1
        # save the genes which are in all genomes in a list
    for d in session['crex_data']:
        for g in d['genome']:
            g = abs_gene(g)
            if counter[g] == len(session['crex_data']):
                session["crex_ubiq"].append( g )
                counter[g] = -1
                
  
def shortnames( self ):
    """
    get short names from the names
    """
    session = self.REQUEST.SESSION

    for d in session["crex_data"]:
        nam = d["name"].split(',')
        d["nam"] = str(nam)
        if len(nam) > 1:
            nam = nam[:1]
        d["nam"] = " ".join(nam).rstrip(',')
        d["na"] = "".join([t[0] for t in d["nam"].split()])

def szenario( self, i, j ):
    session = self.REQUEST.SESSION
        # get the permutations
    p1, p2, map = genome2permutation(self, session["crex_data"][i], session["crex_data"][j])
        # get the element where we should rotate to (for linear -> just take one)
    if session['crex_circular']:
        rot = map.index(session['crex_rotate'])
    else:
        rot = 1
        # compute the scenario
    
    szen = crex.compare(p1, p2, map, session["crex_circular"], rot, MAXPREV)
    szenario = []
    lasti = (0, 0)
    for s in szen:
        mop = re.match("^(rev|revtra|tra|tdl)$", s)
        mmark = re.match("^([rq])\|(rev|tra|pnode)(1|2)\|(\d+)([,;])(\d+)$", s)
        mnext = re.match("^#next(-*\d+)_(-*\d+)$", s)
        if mnext != None:
            nexti = (mnext.group(1), mnext.group(2) )
            if ( lasti != nexti ):
                szenario.append( [] )
            szenario[-1].append( {"op":[], "mark":"", "tree":[]} )
            lasti = nexti
            op = "pnode"
        elif mop != None:
            if s == "rev":
                szenario[-1][-1]["op"].append("reversal")
            elif s == "revtra":
                szenario[-1][-1]["op"].append("reverse transposition")
            elif s == "tra":
                szenario[-1][-1]["op"].append("transposition")
            elif s == "tdl":
                szenario[-1][-1]["op"].append("tdrl")
            else:
                raise "UnknownOperation"
            op = s
#            out += "OP "+ s+"<br>"
        elif mmark != None:
#            raise "mark" +s, str(mmark.group(1))+"-"+str(mmark.group(2))+"-"+str(mmark.group(3))+"-"+str(mmark.group(4))+"-"+str(mmark.group(5))+"-"+str(mmark.group(6))
            colname = "crex_"+mmark.group(2)+mmark.group(3)+"color"
            
#            +op
#            if mmark.group(3) == ',':
#                colname +="1"
#            else:
#                colname +="2"
#            colname += 
            if(session.has_key(colname)):
                color = session[colname]
            else:
                raise "color", colname
                color = "#000000"
            szenario[-1][-1]["mark"] += "highlight_interval('"+mmark.group(1)+str(i)+"_"+str(j)+"',"+ mmark.group(4)+","+ mmark.group(6)+", '"+color+"');"
        else:
            if len(szenario[-1][-1]["tree"]) == 0 or len(szenario[-1][-1]["tree"][-1]) == 2:
                szenario[-1][-1]["tree"].append([])
            szenario[-1][-1]["tree"][-1].append( s )

    return szenario





#    session = self.REQUEST.SESSION
#        # get the permutations
#    p1, p2, map = genome2permutation(self, session["crex_data"][i], session["crex_data"][j])
#        # get the element where we should rotate to (for linear -> just take one)
#    if session['crex_circular']:
#        rot = map.index(session['crex_rotate'])
#    else:
#        rot = 1
#        # compute the scenario
#    
#    szen = crex.compare(p1, p2, map, session["crex_circular"], rot)
#    szenario = []
#    
#    for s in szen:
#        mop = re.match("^(rev|revtra|tra|tdl)$", s)
#        mmark = re.match("^([rq])(\d+)([,;])(\d+)$", s)
#        mnext = re.match("^#next(\d+)_(\d+)$", s)
#        if mnext != None:
#            szenario.append( {"op":[], "mark":"", "tree":[]} )
#            op = "pnode"
#        elif mop != None:
#            if s == "rev":
#                szenario[-1]["op"].append("reversal")
#            elif s == "revtra":
#                szenario[-1]["op"].append("reverse transposition")
#            elif s == "tra":
#                szenario[-1]["op"].append("transposition")
#            elif s == "tdl":
#                szenario[-1]["op"].append("tdrl")
#            else:
#                raise "UnknownOperation"
#            op = s
##            out += "OP "+ s+"<br>"
#        elif mmark != None:
#            colname = "crex_"+op
#            if mmark.group(3) == ',':
#                colname +="1"
#            else:
#                colname +="2"
#            colname += "color"
#            if(session.has_key(colname)):
#                color = session[colname]
#            else:
#                raise "color", colname
#                color = "#000000"
#            szenario[-1]["mark"] += "highlight_interval('"+mmark.group(1)+str(i)+"_"+str(j)+"',"+ mmark.group(2)+","+ mmark.group(4)+", '"+color+"');"
#        else:
#            if len(szenario[-1]["tree"]) == 0 or len(szenario[-1]["tree"][-1]) == 2:
#                szenario[-1]["tree"].append([])
#            szenario[-1]["tree"][-1].append( s )
#
#    return szenario

def tree( self, i, j, pfx="" ):
    session = self.REQUEST.SESSION
    p1, p2, map = genome2permutation(self, session["crex_data"][i], session["crex_data"][j])
    if session['crex_circular']:
        rot = map.index(session['crex_rotate']) * session['crex_rotate_sign']
    else:
        rot = 1
    
    return crex.tree(p1, p2, map, session["crex_circular"], rot)

    
