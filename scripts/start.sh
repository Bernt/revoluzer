#!/bin/bash

# recursively start a program for all geneorder files in a directory
# start inputdir outputdir "program params" timelimit

recurse(){

	#~ echo $1 $2 $3
	
	for s in $1/*
	do
		if [ -d "$s" ]
		then
			bs=`basename $s`
			#~ echo "base "$bs
			if [ ! -d $2"/"$bs ]
			then 
				mkdir $2"/"$bs
			fi
			recurse $s $2"/"$bs  "$3"
			continue
		elif [ `cat $s | grep -c "^>"` -gt "0" ]
		then
			bs=`basename $s`
			o=$2"/"$bs
			if [ ! -e "$o" ]
			then
				# imediately create output 
				# (shell redirection may create the file later)
				touch $o

				# start the program 
				$3 $s &> $o
				echo "finished "$3 $s
				#~ break
			else
				# if somebody else is working in this directory -> abort 
				break
			fi
		fi
	done
}

if [ $# -lt "3" -o $# -gt "4" ]
then
  echo "This script needs at exactly 3 or 4 command-line arguments!"
  exit
fi  

INDIR=$1
OUTDIR=$2
PROG=$3

# check parameters
if [ ! -d $INDIR ]
then
	echo $INDIR" does not exist"
	exit 1
fi
if [ ! -d $OUTDIR ]
then
	echo $OUTDIR" does not exist"
	exit 1
fi

# set timelimit if provided
if [ $# -eq "4" ]
then 
	if [ $4 -gt "0" ]
	then 
		ulimit -t $4
	fi
fi

recurse $INDIR $OUTDIR "$PROG"
