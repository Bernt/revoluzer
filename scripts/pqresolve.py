#!/usr/bin/python

from optparse import OptionParser
from os import listdir
from re import match
from sys import stderr, exit
import subprocess

PQRESOLVE = "/home/maze/workspace/revoluzer_build/src/pqresolve"

def dot( file, type ):
	if not file.endswith( ".dot" ):
		stderr.write( "error: cant dot non dot file %s\n" % file )
		exit()

	ofname = file[:-4] + "." + type
	of = open( ofname, "w" )
	print "call dot", file
	p = subprocess.Popen( ["dot", "-T%s" % ( type ), "%s" % ( file )], shell = False, stdout = of, stderr = subprocess.PIPE, close_fds = True )
	p.wait()
	of.close()
	print "finished dot"


	err = []
	for l in p.stderr.readlines():
		err.append( l )
	if len( err ) > 0:
		stderr.write( "dot returned an error\n" )
		for e in err:
			stderr.write( e )
		exit()
	return ofname


usage = "usage: %prog [options]"
parser = OptionParser( usage )
parser.add_option( "-t", dest = "tfname", action = "store", type = "string", metavar = "FILE", help = "get the tree(s) from FILE" )
parser.add_option( "-f", dest = "gfname", action = "store", type = "string", metavar = "FILE", help = "get the genomes from FILE" )
parser.add_option( "-d", dest = "odname", action = "store", type = "string", metavar = "DIR", help = "store output in DIR" )
parser.add_option( "-o", dest = "otype", default = "latex", action = "store", type = "string", metavar = "TYPE", help = "output in format TYPE (html/latex)" )

( options, args ) = parser.parse_args()

if options.tfname == None:
	parser.error( "tree file must be specified" )
if options.gfname == None:
	parser.error( "genome file must be specified" )
if options.odname == None:
	parser.error( "output directorymust be specified" )

if options.otype == "latex":
	imagetype = "ps"
	head = "\\documentclass[a4paper,10pt]{article}\n\\usepackage{fullpage}\n\\usepackage{graphicx}\n\\begin{document}\n"
	foot = "\\end{document}\n"

elif options.otype == "html":
	head = "<html>\n<body>\n"
	foot = "</body>\n</html>\n"
	imagetype = "png"
else:
	parser.error( "unsupported output type" )

print "call pqresolve"
#print "%s -f %s -t %s -d %s" %(PQRESOLVE, options.gfname, options.tfname, options.odname)
p = subprocess.Popen( "%s -f %s -t %s -d %s -m 2" % ( PQRESOLVE, options.gfname, options.tfname, options.odname ), shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds = True )
p.wait()
print "finished pqresolve"

scores = []
cperms = []
err = []
for l in p.stdout.readlines() + p.stderr.readlines():
	m = match( "^SCORE = ([0-9]+)$", l )
	if m != None:
		scores.append( int( m.group( 1 ) ) )
		continue
	m = match( "^CPERM = ([0-9]+)$", l )
	if m != None:
		cperms.append( int( m.group( 1 ) ) )
		continue
	err.append( l )

if len( err ) > 0:
	stderr.write( "pqresolve returned errors:\n" )
	for e in err:
		stderr.write( "\t%s" % e )
	#exit()

d = listdir( options.odname )
d.sort()

if  options.otype == "html" :
	of = open( "%s/out.html" % ( options.odname ), "w" )
elif options.otype == "latex" :
	of = open( "%s/out.tex" % ( options.odname ), "w" )



of.write( head )

for f in d:
	if not f.endswith( ".dot" ):
		continue

	ifname = dot( options.odname + "/" + f, imagetype )

	m = match( "^tree(\\d+).dot$", f )
	if m != None:
		continue
		nr = int( m.group( 1 ) )
		if  options.otype == "html" :
			of.write( "<h2> Tree %d </h2>\n" % ( nr ) )
			of.write( "<ul>\n <li>Score = %s</li>\n<li>Consistent Permutations = %s</li>\n </ul>\n" % ( scores[nr], cperms[nr] ) )
		elif options.otype == "latex" :
			of.write( "\\section{Tree %d}\n" % ( nr ) )
			of.write( "\\begin{itemize}\n\\item Score = %s\n\\item Consistent Permutations = %s\\end{itemize}\n" % ( scores[nr], cperms[nr] ) )

	m = match( "^tree(\\d+)node(.*).dot$", f )
	if m != None:
		if  options.otype == "html" :
			of.write( "<h3> Tree %s Node %s </h3>\n" % ( m.group( 1 ), m.group( 2 ) ) )
		elif options.otype == "latex" :
			of.write( "\\subsection{Tree %s Node %s}\n" % ( m.group( 1 ), m.group( 2 ) ) )

	if  options.otype == "html" :
		of.write( "<img src=\"%s\" width=\"100%%\">\n" % ( ifname ) )
	elif options.otype == "latex" :
		of.write( "\\includegraphics[width=\\textwidth]{%s}\n" % ( ifname ) )

of.write( foot )
of.close()

