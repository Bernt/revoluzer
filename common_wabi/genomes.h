#ifdef __cplusplus
  extern "C" {
#endif

#ifndef GENOMES_H
#define GENOMES_H
/*****************************************************************************
 *  FILE   : genomes.h
 *  AUTHOR : Jens Stoye
 *  DATE   : 07/2001
 *****************************************************************************
 *
 * Functions regarding genomes:
 *
 *   read_genomes()
 *   write_genomes()
 *   free_genomes()
 *   copy_genomes()
 *   sort_genomes()
 *   is_unsigned()
 *   is_noncircular()
 *   is_unichromosomal()
 *   are_permutations()
 *   new_direct_chromosome_vector()
 *
 *
 * Syntax of genomes file (multichromosomal, signed, circular or linear
 * (or mixed), family genomes, possibly with missing genes):
 *
 * (1) Each genome starts with a header line whose first character is '>'.
 * (2) Genes are denoted by the numbers {1,2,...,max}, separated by spaces.
 * (3) Strand information of a gene is indicated by a plus (+) or minus (-)
 *     sign before the number; default (no sign) implies plus.
 * (4) Chromosome ends are denoted by | (linear chromosome) or ) (circular
 *     chromosome); default (no sign after last gene) is one linear chromosome.
 *
 *****************************************************************************/

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdio.h> //FILE
#include "bool.h" //BOOL

/*****************************************************************************
 * CONSTANTS
 *****************************************************************************/
#define GENOMES_PLUS TRUE
#define GENOMES_MINUS FALSE

#define GENOMES_NO_END 0
#define GENOMES_LINEAR +1
#define GENOMES_CIRCULAR -1

#define GENOMES_COMMENT_SIGN (int)'>'
#define GENOMES_PLUS_SIGN (int)'+'
#define GENOMES_MINUS_SIGN (int)'-'
#define GENOMES_LINEAR_SIGN (int)'|'
#define GENOMES_CIRCULAR_SIGN (int)')'

/*****************************************************************************
 * TYPES
 *****************************************************************************/
typedef struct GENOMES {
  int num, //number of genomes
      max, //maximum entry
      *len, //lengths of the genomes
      **g, //vector of genes
      **c; //vector of chromosome boundaries: NO_END, LINEAR, or CIRCULAR
  BOOL **s; //vector of signs (GENOMES_PLUS (+) or GENOMES_MINUS (-))
  char **name; //names of the genomes
} genomes;

/*****************************************************************************
 * FUNCTION PROTOTYPES
 *****************************************************************************/

/*============================================================================
 * read_genomes()
 *
 * Read genomes from file and return result in genomes struct. The sytanx of
 * the genomes file is described in the header of this file (genomes.h).
 *
 * Parameters:
 *   fptr - file pointer to be read from
 *
 *---------------------------------------------------------------------------*/
genomes *read_genomes(FILE *fptr);

/*============================================================================
 * write_genomes()
 *
 * Write genomes to file. Same sytanx as in read_genomes().
 *
 * Parameters:
 *   fptr - file pointer to be written to
 *   g - the genomes struct
 *
 *---------------------------------------------------------------------------*/
void write_genomes(FILE *fptr,const genomes *g);

/*============================================================================
 * free_genomes()
 *
 * Free memory of genomes struct.
 *
 * Parameters:
 *   g - the genomes struct
 *
 *---------------------------------------------------------------------------*/
void free_genomes(genomes *g);

/*============================================================================
 * copy_genomes()
 *
 * Return a copy of genomes struct g.
 *
 * Parameters:
 *   g - the genomes struct
 *
 *---------------------------------------------------------------------------*/
genomes *copy_genomes(const genomes *g);

/*============================================================================
 * sort_genomes()
 *
 * Re-label the genes in the genomes of genomes struct g auch that the
 * first genome becomes the identity permutation 1,2,...,n. Makes
 * sense only if all genomes are permutations of the numbers 1,2,...,n.
 *
 * Parameters:
 *   g - the genomes struct
 *
 *---------------------------------------------------------------------------*/
void sort_genomes(genomes *g);

/*============================================================================
 * is_unichromosomal()
 *
 * Test if all genomes are unichromosomal.
 *
 * Parameters:
 *   g - the genomes struct
 *
 *---------------------------------------------------------------------------*/
BOOL is_unichromosomal(const genomes *g);

/*============================================================================
 * is_unsigned()
 *
 * Test if all genomes are unsigned: For each genome, all its genes
 * have the same sign (either all plus (+) or all minus (-)).
 *
 * Parameters:
 *   g - the genomes struct
 *
 *---------------------------------------------------------------------------*/
BOOL is_unsigned(const genomes *g);

/*============================================================================
 * is_non_circular()
 *
 * Test if all genomes are non-circular.
 *
 * Parameters:
 *   g - the genomes struct
 *
 *---------------------------------------------------------------------------*/
BOOL is_non_circular(const genomes *g);

/*============================================================================
 * are_permutations()
 *
 * Test genomes struct for correct permutations: All genomes must be
 * of the same size and each genome must contain each of the numbers
 * 0 .. g->len-1 exactly once.
 *
 * Parameters:
 *   g - the genomes struct
 *
 *---------------------------------------------------------------------------*/
BOOL are_permutations(const genomes *g);
 
/*============================================================================
 * new_direct_chromosome_vector()
 *
 * Compute the (direct) chromosome vector of a (linear) genome from
 * its chromosomes, restricted (and shifted) to the region [start .. end].
 *
 * Parameters:
 *   g - genomes struct
 *   k - number of genome
 *   start - start of the relevant region
 *   end - end of the relevant region
 *
 *---------------------------------------------------------------------------*/
int *new_direct_chromosome_vector(const genomes *g,const int k,
                                  const int start,const int end);

/****** EOF (genomes.h) ******************************************************/
#endif

#ifdef __cplusplus
 }
#endif
