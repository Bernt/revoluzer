 #ifdef __cplusplus
  extern "C" {
 #endif

/*****************************************************************************
 *  FILE   : irreducible.c
 *  AUTHOR : Jens Stoye
 *  DATE   : 05/2001
 *****************************************************************************
 *
 * Functions regarding irreducible intervals. See irreducible.h for details.
 *
 *****************************************************************************/

/*****************************************************************************
 * SWITCHES
 *****************************************************************************/
//~ #define DEBUG_ORIGINAL_PERMUTATIONS
//~ #define DEBUG_SIGNS
//~ #define DEBUG_BREAKPOINTS
//~ #define DEBUG_REARRANGEMENT
//~ #define DEBUG_INTERVALS
//~ #define DEBUG_PARTITIONS

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <limits.h> //INT_MIN,INT_MAX

#include "alloc.h" //ALLOC,FREE
#include "minmax.h" //MINEQ
#include "bool.h" //BOOL,TRUE,FALSE

#include "genomes.h" //...
#include "intervals.h" //...
#include "ilist.h" //...
#include "irreducible.h" //...

int MINMAX_tmp_var1,MINMAX_tmp_var2,MINMAX_tmp_var3;

/*****************************************************************************
 * LOCAL FUNCTIONS
 *****************************************************************************/
int *new_relative_permutation(int *pi1,int *pi2,int start,int end,int offset1,
                              int offset2)
{
  int j,len,max,*mypi1,*mypi2,*sort,*pi;

  len = end-start+1;
  ALLOC(mypi1,int,len);
  ALLOC(mypi2,int,len);
  ALLOC(pi,int,len);

  max = INT_MIN;
  for(j=0; j<len; j++) {
    mypi1[j] = pi1[start+(offset1+j)%len];
    mypi2[j] = pi2[start+(offset2+j)%len];
    MAXEQ(max,mypi2[j]);
  }

  ALLOC(sort,int,max+1);

  for(j=0; j<len; j++) {
    sort[mypi2[j]] = j;
  }
  for(j=0; j<len; j++) {
    pi[j] = sort[mypi1[j]];
  }

  FREE(mypi1);
  FREE(mypi2);
  FREE(sort);
  
  return pi;
} //new_relative_permutation()

/*===========================================================================*/
int *new_inverse_chromosome_vector(genomes *g,int k)
{
  int i,c_num,*gicv;

  ALLOC(gicv,int,g->len[k]);
  c_num = 0;
  for(i=0; i<g->len[k]; i++) {
    gicv[g->g[k][i]] = c_num;
    if(g->c[k][i]!=GENOMES_NO_END) {
      c_num++;
    }
  }

  return gicv;
} //new_inverse_chromosome_vector()

/*===========================================================================*/
void circular_shift(genomes *g,int k,int start,int end,int offset)
{
  int i,len,*old_g,*old_c;
  BOOL *old_s;

  len = end-start+1;
  ALLOC(old_g,int,len);
  ALLOC(old_c,int,len);
  ALLOC(old_s,BOOL,len);

  for(i=0; i<len; i++) {
    old_g[i] = g->g[k][start+i];
    old_c[i] = g->c[k][start+i];
    old_s[i] = g->s[k][start+i];
  }

  for(i=0; i<len; i++) {
    g->g[k][start+i] = old_g[(len-offset+i)%len];
    g->c[k][start+i] = old_c[(len-offset+i)%len];
    g->s[k][start+i] = old_s[(len-offset+i)%len];
  }

  FREE(old_g);
  FREE(old_c);
  FREE(old_s);
  return;
} //circular_shift()

/*===========================================================================*/
#define IRREDUCIBLE_LINEAR TRUE
#define IRREDUCIBLE_CIRCULAR FALSE
intervals *iri(int *pi,int *chr,intervals *i,BOOL linear)
{
  intervals *i_new;
  list *ylist;
  list_entry *xle,*yle;
  hlist *llist,*ulist;
  hlist_entry *ule,*lle;
  ilist *blist;
  ilist_entry *ile,*ale;
  int len,x;

  //initializations
  len = i->len;
  i_new = new_intervals(len,i->num);

  blist = new_ilist(i);
  ylist = new_list(len);
  ulist = new_hlist();
  llist = new_hlist();
  x = len-1;
  xle = init_ylist(ylist);
  ule = new_hlist_entry(pi[x],xle);
  prepend_hlist_entry(ule,ulist);
  lle = new_hlist_entry(pi[x],xle);
  prepend_hlist_entry(lle,llist);
  xle->u = ule;
  xle->l = lle;

  while(x > 0) {

    //step left to next list element
    x--;
    xle = xle->prev;

    //activate intervals
    for(ile=blist->lpos[x]; ile!=NULL; ile=ile->up) {
      activate_ilist_entry(ile,blist,ylist);
    }

    if(pi[x] > pi[x+1]) { //this is the case of the paper

      //update llist
      lle = new_hlist_entry(pi[x],xle);
      prepend_hlist_entry(lle,llist);
      xle->l = lle;

      //prune ulist and ylist (Lemma 1)
      for(ule=ulist->head;
          ule!=NULL && ule->next!=NULL && ule->next->val<pi[x];
          ule=ulist->head) {
        for(yle=xle->next; yle->u==ule; yle=xle->next) {
          if(yle->l->lasty == yle) { //this happens: 4 2 1 3
            remove_hlist_entry(yle->l,llist);
          }
          merge_ilist_entries(blist->rpos[yle->pos],blist);
          delete_list_entry(yle,ylist);
        }
        remove_hlist_entry(ule,ulist);
      }
      ulist->head->val = pi[x];
      xle->u = ulist->head;

      //prune ylist (Lemma 2)
      for(yle=ulist->head->lasty; //ystar
          yle!=NULL && yle->next!=NULL && f(xle,yle)>f(xle,yle->next);
          yle=ulist->head->lasty) {
        if(yle->l->lasty == yle) { //this happens: 4 2 1 5 3
          remove_hlist_entry(yle->l,llist);
        }
        ulist->head->lasty = yle->prev;
        merge_ilist_entries(blist->rpos[yle->pos],blist);
        delete_list_entry(yle,ylist);
      }

    }
    else { //this is the symmetric case

      //update ulist
      ule = new_hlist_entry(pi[x],xle);
      prepend_hlist_entry(ule,ulist);
      xle->u = ule;

      //prune llist and ylist (Lemma 1)
      for(lle=llist->head;
          lle!=NULL && lle->next!=NULL && lle->next->val>pi[x];
          lle=llist->head) {
        for(yle=xle->next; yle->l==lle; yle=xle->next) {
          if(yle->u->lasty == yle) {
            remove_hlist_entry(yle->u,ulist);
          }
          merge_ilist_entries(blist->rpos[yle->pos],blist);
          delete_list_entry(yle,ylist);
        }
        remove_hlist_entry(lle,llist);
      }
      llist->head->val = pi[x];
      xle->l = llist->head;

      //prune ylist (Lemma 2)
      for(yle=llist->head->lasty; //ystar
          yle!=NULL && yle->next!=NULL && f(xle,yle)>f(xle,yle->next);
          yle=llist->head->lasty) {
        if(yle->u->lasty == yle) {
          remove_hlist_entry(yle->u,ulist);
        }
        llist->head->lasty = yle->prev;
        merge_ilist_entries(blist->rpos[yle->pos],blist);
        delete_list_entry(yle,ylist);
      }
    }

    //find and store all irreducible intervals that lie on only one chr.
    //in the circular case also test for length
    while((ile=blist->lpos[x])!=NULL &&
          (ale=ile->mychain->first_active)!=NULL &&
          f(xle,(yle=&ylist->space[ale->end]))==0 &&
          chr[yle->u->val]==chr[yle->l->val] &&
          (linear==IRREDUCIBLE_LINEAR || yle->pos-x<len/2)) {
      append_interval(i_new,x,yle->pos);
      deactivate_ilist_entry(ale,blist,x);
    }
  } //while x

  //cleanup
  ci_free_list(ylist);
  remove_hlist(ulist);
  remove_hlist(llist);
  free_ilist(blist);
  FREE(pi);

  return i_new;
} //iri()

/*===========================================================================*/
intervals *iri_linear(genomes *g,int start,int end)
{
  intervals *i,*i_new;
  int len,k,*pi,*chr;

  len = end-start+1;
  i = new_intervals(len,len-1);
  init_intervals(i,g,start,end);
#ifdef DEBUG_INTERVALS
	fprintf(stderr,"A\n");
  write_intervals(stderr,i);
	fprintf(stderr,"a\n");
#endif
  for(k=1; k<g->num; k++) {
    pi = new_relative_permutation(g->g[0],g->g[k],start,end,0,0);
    chr = new_direct_chromosome_vector(g,k,start,end);
    i_new = iri(pi,chr,i,IRREDUCIBLE_LINEAR);
    free_intervals(i);
    FREE(chr);
    i = i_new;
#ifdef DEBUG_INTERVALS
	fprintf(stderr,"B\n");
    write_intervals(stderr,i);
	fprintf(stderr,"b\n");
#endif
  }
  return i;
} //iri_linear()

/*===========================================================================*/
intervals *iri_circular(genomes *g,int start,int end)
{
  intervals *i,*i_new,*i_tmp;
  int len,k,*pi,*chr;

  len = end-start+1;
  i = new_intervals(len,len);
  init_intervals(i,g,start,end);
#ifdef DEBUG_INTERVALS
  write_intervals(stderr,i);
#endif
  chr = new_direct_chromosome_vector(g,0,start,end); //constant: circular only

  for(k=1; k<g->num; k++) {
    pi = new_relative_permutation(g->g[0],g->g[k],start,end,0,0);
    i_new = iri(pi,chr,i,IRREDUCIBLE_CIRCULAR);
#ifdef DEBUG_INTERVALS
    write_intervals(stderr,i_new);
#endif
    pi = new_relative_permutation(g->g[0],g->g[k],start,end,0,len/2);
    i_tmp = iri(pi,chr,i,IRREDUCIBLE_CIRCULAR);
	umerge_intervals(i_new,i_tmp);
    free_intervals(i_tmp);
#ifdef DEBUG_INTERVALS
    printf("C");write_intervals(stderr,i_new);
#endif

    shift_intervals(i,len-len/2,INTERVALS_SMALLEST_TO_FRONT);

    pi = new_relative_permutation(g->g[0],g->g[k],start,end,len/2,0);
    i_tmp = iri(pi,chr,i,IRREDUCIBLE_CIRCULAR);
    shift_intervals(i_tmp,len/2,INTERVALS_LARGEST_TO_FRONT);
    umerge_intervals(i_new,i_tmp);
    free_intervals(i_tmp);
#ifdef DEBUG_INTERVALS
    write_intervals(stderr,i_new);
#endif
    pi = new_relative_permutation(g->g[0],g->g[k],start,end,len/2,len/2);
    i_tmp = iri(pi,chr,i,IRREDUCIBLE_CIRCULAR);
    shift_intervals(i_tmp,len/2,INTERVALS_LARGEST_TO_FRONT);
    umerge_intervals(i_new,i_tmp);
    free_intervals(i_tmp);
#ifdef DEBUG_INTERVALS
    write_intervals(stderr,i_new);
#endif

    free_intervals(i);
    i = i_new;
#ifdef DEBUG_INTERVALS
    write_intervals(stderr,i);
#endif
  }
  FREE(chr);
  return i;

} //iri_circular()

/*===========================================================================*/
intervals *iri_mixed(genomes *g,int start,int end)
{
  intervals *i;

  fprintf(stderr,"Mixed case (%d,%d): Not yet implemented!\n",start+1,end+1);

  ALLOC(i,intervals,1);
  i->num = 0;
  return i;

} //iri_mixed()

/*****************************************************************************
 * FUNCTION DEFINITIONS
 *****************************************************************************/
#define NO_NEW_BREAKPOINT -1

intervals *find_common_intervals(FILE *fptr,genomes *g, int *g0_shift, int irreducible)
{
  int l,j,k,g_len,c_start,c_end,c_num,c_this,
      last_new_breakpoint,*c0,*ck,*my_chr,*Cstart,*Cnext;
  genomes *g2;
  intervals *i = NULL,
	*all_intervals = NULL;
  BOOL linear_only,circular_only;
  *g0_shift = 0;
  g_len = g->len[0];
	
	all_intervals = new_intervals(g_len, 1);
#ifdef DEBUG_ORIGINAL_PERMUTATIONS
  fprintf(stderr,"Original permutations:\n");
  write_genomes(stderr,g);
#endif

  //*** Preprocessing 1: insert breakpoints for signs ***
// MB sign changes also in pi_0
  for(k=0; k<g->num; k++) {
	  
	// MC-Genomes : search start and end point of one chromosome
	// than search between c_start && c_end for sign changes
    for(c_end=0; c_end<g_len; c_end++) {
      for(c_start=c_end; c_end<g_len && g->c[k][c_end]==GENOMES_NO_END; c_end++) {
        /* nothing */ ;
      }
      last_new_breakpoint = NO_NEW_BREAKPOINT;
      for(j=c_start; j<c_end; j++) {
        if(g->s[k][j] != g->s[k][j+1]) {
          g->c[k][j] = GENOMES_LINEAR;
          last_new_breakpoint = j;
        }
      }
      if(g->c[k][c_end]==GENOMES_CIRCULAR && last_new_breakpoint!=NO_NEW_BREAKPOINT) {
        if(g->s[k][c_end] != g->s[k][c_start]) {
          g->c[k][c_end] = GENOMES_LINEAR;
          //last_new_breakpoint = c_end; (not necessary here)
        }
        else {
          g->c[k][c_end] = GENOMES_NO_END;
          circular_shift(g,k,c_start,c_end,c_end-last_new_breakpoint);
		  if(k==0)
			  *g0_shift += c_end-last_new_breakpoint;
        }
      }
    }
  }

#ifdef DEBUG_SIGNS
  fprintf(stderr,"After setting breakpoints for sign changes:\n");
  write_genomes(stderr,g);
#endif

  //*** Preprocessing 2: insert artificial breakpoints for circular chrs. ***
	//MB do this twice
	for(l=0; l<2; l++){
  
	  //insert new artificial breakpoints into pi_0
	  for(k=1; k<g->num; k++) {
		ck = new_inverse_chromosome_vector(g,k);
		for(c_end=0; c_end<g_len; c_end++) {
		  for(c_start=c_end; c_end<g_len && g->c[0][c_end]==GENOMES_NO_END; c_end++) {
			/* nothing */ ;
		  }
		  last_new_breakpoint = NO_NEW_BREAKPOINT;
		  for(j=c_start; j<c_end; j++) {
			if(ck[g->g[0][j]] != ck[g->g[0][j+1]]) {
			  g->c[0][j] = GENOMES_LINEAR;
			  last_new_breakpoint = j;
			}
		  }
		  if(g->c[0][c_end]==GENOMES_CIRCULAR && last_new_breakpoint!=NO_NEW_BREAKPOINT) {
			if(ck[g->g[0][c_end]] != ck[g->g[0][c_start]]) {
			  g->c[0][c_end] = GENOMES_LINEAR;
			  //last_new_breakpoint = c_end; (not necessary here)
			}
			else {
			  g->c[0][c_end] = GENOMES_NO_END;
			  circular_shift(g,0,c_start,c_end,c_end-last_new_breakpoint);
			  *g0_shift += c_end-last_new_breakpoint;
			   
			//~ printf("shift by %d\n",c_end-c_start+1-(c_end-last_new_breakpoint));
			}
		  }
		}
		FREE(ck);
	  }
	
	  //insert new artificial breakpoints into pi_k
	  c0 = new_inverse_chromosome_vector(g,0);
	  for(k=1; k<g->num; k++) {
		for(c_end=0; c_end<g_len; c_end++) {
		  for(c_start=c_end; c_end<g_len && g->c[k][c_end]==GENOMES_NO_END; c_end++) {
			/* nothing */ ;
		  }
		  last_new_breakpoint = NO_NEW_BREAKPOINT;
		  for(j=c_start; j<c_end; j++) {
			if(c0[g->g[k][j]] != c0[g->g[k][j+1]]) {
			  g->c[k][j] = GENOMES_LINEAR;
			  last_new_breakpoint = j;
			}
		  }
		  if(g->c[k][c_end]==GENOMES_CIRCULAR && last_new_breakpoint!=NO_NEW_BREAKPOINT) {
			if(c0[g->g[k][c_end]] != c0[g->g[k][c_start]]) {
			  g->c[k][c_end] = GENOMES_LINEAR;
			  last_new_breakpoint = c_end;
			}
			else {
			  g->c[k][c_end] = GENOMES_NO_END;
			}
			circular_shift(g,k,c_start,c_end,c_end-last_new_breakpoint);
		  }
		}
	  }
	  FREE(c0);
  }
  
#ifdef DEBUG_BREAKPOINTS
  fprintf(stderr,"After setting breakpoints for incompatible chromosomes:\n");
  write_genomes(stderr,g);
#endif

  //*** Preprocessing 3: re-order partitions in each permutation as in pi_0 ***
  ALLOC(my_chr,int,g_len);
  ALLOC(Cstart,int,g_len + 1);	// MB: seems that one byte more is needed
  ALLOC(Cnext,int,g_len);

  //assign chromosome numbers to genes
  c_num = 0;
  Cstart[0] = 0;
  for(j=0; j<g_len; j++) {
    my_chr[g->g[0][j]] = c_num;
    if(g->c[0][j] != GENOMES_NO_END) {
      c_num++;
      Cstart[c_num] = j+1;
    }
  }

  //copy old genomes struct to g2 (g will be replaced by reordering)
  g2 = copy_genomes(g);
 
  //copy chromosomes to their final position in g
  for(k=1; k<g->num; k++) {
    for(j=0; j<c_num; j++) {
      Cnext[j] = Cstart[j];
    }
    for(c_end=0; c_end<g_len; c_end++) {
      c_this = my_chr[g2->g[k][c_end]];
      for(c_start=c_end; c_end<g_len && g2->c[k][c_end]==GENOMES_NO_END;
          c_end++) {
        g->g[k][Cnext[c_this]] = g2->g[k][c_end];
        g->c[k][Cnext[c_this]] = g2->c[k][c_end];
        g->s[k][Cnext[c_this]] = g2->s[k][c_end];
        Cnext[c_this]++;
      }
      g->g[k][Cnext[c_this]] = g2->g[k][c_end];
      g->c[k][Cnext[c_this]] = g2->c[k][c_end];
      g->s[k][Cnext[c_this]] = g2->s[k][c_end];
      Cnext[c_this]++;
    }
  }
  
  //free memory
  FREE(my_chr);
  FREE(Cstart);
  FREE(Cnext);
  free_genomes(g2);

#ifdef DEBUG_REARRANGEMENT
  fprintf(stderr,"After re-ordering the partitions as in pi_0:\n");
  write_genomes(stderr,g);
#endif

  //*** Process one partition after the other ***
  for(c_end=0; c_end<g_len; c_end++) {
    for(c_start=c_end; c_end<g_len && g->c[0][c_end]==GENOMES_NO_END; c_end++) {
      /* nothing */ ;
    }
    
    
    circular_only = g->c[0][c_end]==GENOMES_CIRCULAR;
    linear_only = g->c[0][c_end]==GENOMES_LINEAR;
    for(k=1; k<g->num; k++) {
      circular_only &= g->c[k][c_end]==GENOMES_CIRCULAR;
      linear_only &= g->c[k][c_end]==GENOMES_LINEAR;
    }

    if(linear_only) {
      //Case 1: all permutations are linear
#ifdef DEBUG_PARTITIONS
      fprintf(stderr,"Here is a linear only: %d,%d\n",c_start,c_end);
		for(k=c_start; k<c_end; k++)
			fprintf(stderr,"%d ",g->g[0][k]);
		fprintf(stderr,"\n");
#endif
      i = iri_linear(g,c_start,c_end);
		//~ write_common_intervals(stderr,i,c_start,ILIST_LINEAR);
		//~ printf("\n");
	  if(irreducible){
		  if(i->num > 0){
			 for(k=0; k<i->num; k++){
				 i->start[k] += c_start;
				 i->end[k] += c_start;
			 }
			 umerge_intervals(all_intervals,i);
		   }
	  }else{
		//~ write_common_intervals(stderr,i,c_start,ILIST_LINEAR);
		get_common_intervals(&all_intervals,i,c_start,ILIST_LINEAR);  
	  }

    }
    else if(circular_only) {
      //Case 2: all permutations are circular
#ifdef DEBUG_PARTITIONS
      fprintf(stderr,"Here is a circular only: %d,%d\n",c_start,c_end);
#endif
      i = iri_circular(g,c_start,c_end);
	  if(irreducible){
		  if(i->num > 0){
			 for(k=0; k<i->num; k++){
				 i->start[k] += c_start;
				 i->end[k] += c_start;
			 }
			 umerge_intervals(all_intervals,i);
		   }
	   }else{
		get_common_intervals(&all_intervals,i,c_start,ILIST_CIRCULAR); 
	  }
    }
    else {
      //Case 3: the mixed case
#ifdef DEBUG_PARTITIONS
      fprintf(stderr,"Here is a mixed case: %d,%d\n",c_start,c_end);
#endif
      i = iri_mixed(g,c_start,c_end);
	 //~ umerge_intervals(all_intervals,i);

      //~ write_common_intervals(fptr,i,c_start,ILIST_LINEAR);
    }
	
    free_intervals(i);
  }
	
  if(*g0_shift != 0){
	shift_intervals(all_intervals,g_len-*g0_shift, INTERVALS_SMALLEST_TO_FRONT);
  }
  
  
  return all_intervals;
} //find_common_intervals()

/****** EOF (irreducible.c) **************************************************/

#ifdef __cplusplus
 }
#endif
