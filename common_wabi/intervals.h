#ifndef INTERVALS_H
#define INTERVALS_H
/*****************************************************************************
 *  FILE   : intervals.h
 *  AUTHOR : Jens Stoye
 *  DATE   : 05/2001
 *****************************************************************************
 *
 * Functions regarding intervals:
 *
 *   new_intervals()
 *   init_intervals()
 *   dup_intervals()
 *   append_interval()
 *   empty_intervals()
 *   free_intervals()
 *   write_interval()
 *   write_intervals()
 *   shift_intervals()
 *   umerge_intervals()
 *   sort_intervals()
 *   add_complements()
 *
 *****************************************************************************/

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdio.h> //FILE
#include "genomes.h" //genomes

/*****************************************************************************
 * TYPES
 *****************************************************************************/
typedef struct INTERVALS {
  int len, 	//length of the underlying genomes
      num, 	//number of intervals currently stored
      *start, 	//vector of interval start points
      *end; 	//vector of interval end points
} intervals;

/*****************************************************************************
 * FUNCTION PROTOTYPES
 *****************************************************************************/

/*============================================================================
 * new_intervals()
 *
 * Allocate memory for a new intervals struct.
 *
 * Parameters:
 *   len - length of the intervals
 *   max - maximal number of intervals in this struct
 *
 *---------------------------------------------------------------------------*/
intervals *new_intervals(const int len,const int max);

/*============================================================================
 * init_intervals()
 *
 * Initialize an intervals struct with the (2-element) irreducible intervals
 * from genomes number 0 of a genomes struct that lie in the region
 * [start,end].
 * For circular intervals, also the 'wrap-around' interval is stored.
 *
 * Parameters:
 *   i - the intervals struct
 *   p - the genomes struct
 *   start - leftmost element of relevant region
 *   end - rightmost element of relevant region
 *
 *---------------------------------------------------------------------------*/
void init_intervals(intervals *i,const genomes *p,const int start,
                    const int end);

/*============================================================================
 * dup_intervals()
 *
 * Return a duplicate of an intervals struct.
 *
 * Parameters:
 *   i - the intervals struct to be duplicated
 *
 *---------------------------------------------------------------------------*/
intervals *dup_intervals(const intervals *i);

/*============================================================================
 * append_interval()
 *
 * Append a new interval to an existing intervals struct.
 *
 * Parameters:
 *   i - the intervals struct
 *   start - left end of the new interval
 *   end - right end of the new interval
 *
 *---------------------------------------------------------------------------*/
void append_interval(intervals *i,const int start,const int end);

/*============================================================================
 * empty_intervals()
 *
 * Make an intervals struct empty (but do not free the memory).
 *
 * Parameters:
 *   i - the intervals struct
 *
 *---------------------------------------------------------------------------*/
void empty_intervals(intervals *i);

/*============================================================================
 * free_intervals()
 *
 * Free memory of intervals struct.
 *
 * Parameters:
 *   i - the intervals struct
 *
 *---------------------------------------------------------------------------*/
void free_intervals(intervals *i);

/*============================================================================
 * write_interval()
 *
 * Write one interval to fptr (interval borders might be increased by one).
 *
 * Parameters:
 *   fptr - file pointer to be written to
 *   start - starting position of the interval
 *   end - ending position of the interval
 *
 *---------------------------------------------------------------------------*/
void write_interval(FILE *fptr,const int start,const int end);

/*============================================================================
 * write_intervals()
 *
 * Write the (irreducible) intervals of an intervals struct.
 *
 * Parameters:
 *   fptr - file pointer to be written to
 *   i - the intervals struct
 *
 *---------------------------------------------------------------------------*/
void write_intervals(FILE *fptr,const intervals *i);

/*============================================================================
 * shift_intervals()
 *
 * Shift all intervals by the given offset to the right (circularly).
 * Negative shifts cause problems.
 * After the shift a circular rotation is performed where either the
 * largest or the smallest element is moved to the front.
 *
 * Parameters:
 *   i - intervals struct whose intervals shall be shifted
 *   offset - offset by which the intervals shall be shifted to the right
 *   which_to_front - flag indicating whether largest or smallest element
 *                    is moved to the front
 *
 *---------------------------------------------------------------------------*/
#define INTERVALS_SMALLEST_TO_FRONT TRUE
#define INTERVALS_LARGEST_TO_FRONT FALSE
void shift_intervals(intervals *i,const int shift, const BOOL which_to_front);

/*============================================================================
 * umerge_intervals()
 *
 * Merge intervals from two intervals structs, remove duplicates and return
 * the result in the first intervals struct.
 *
 * This function requires that the intervals in the structs are in decreasing
 * order with respect to their left end, and in case of ties in increasing
 * order with respect to their right end.
 *
 * Parameters:
 *   i - first intervals struct, also target
 *   i2 - second intervals struct
 *
 *---------------------------------------------------------------------------*/
void umerge_intervals(intervals *i,const intervals *i2);

/*============================================================================
 * sort_intervals()
 *
 * Sort intervals according to their left end, ties are sorted by their right
 * end, both in increasing order.
 *
 * Parameters:
 *   i - the intervals struct to be sorted
 *
 *---------------------------------------------------------------------------*/
void sort_intervals(intervals *i);

/*============================================================================
 * add_complements()
 *
 * Add the complement to each interval.
 *
 * Parameters:
 *   i - the intervals struct to be processed
 *
 *---------------------------------------------------------------------------*/
void add_complements(intervals *i);

/****** EOF (intervals.h) ****************************************************/
#endif
