#ifndef MINMAX_H
#define MINMAX_H

extern int MINMAX_tmp_var1,MINMAX_tmp_var2,MINMAX_tmp_var3;
#ifndef MAX
#define MAX(X,Y) (((MINMAX_tmp_var1=(X)) > (MINMAX_tmp_var2=(Y))) ? \
                  (MINMAX_tmp_var1) : (MINMAX_tmp_var2))
#endif
#ifndef MIN
#define MIN(X,Y) (((MINMAX_tmp_var1=(X)) < (MINMAX_tmp_var2=(Y))) ? \
                  (MINMAX_tmp_var1) : (MINMAX_tmp_var2))
#endif
#define MAX3(X,Y,Z) (((MINMAX_tmp_var1=(X)) > (MINMAX_tmp_var2=(Y))) ? \
                     (MINMAX_tmp_var1 > (MINMAX_tmp_var3=(Z)) ? \
                      MINMAX_tmp_var1 : MINMAX_tmp_var3) : \
                     (MINMAX_tmp_var2 > (MINMAX_tmp_var3=(Z)) ? \
                      MINMAX_tmp_var2 : MINMAX_tmp_var3))
#define MIN3(X,Y,Z) (((MINMAX_tmp_var1=(X)) < (MINMAX_tmp_var2=(Y))) ? \
                     (MINMAX_tmp_var1 < (MINMAX_tmp_var3=(Z)) ? \
                      MINMAX_tmp_var1 : MINMAX_tmp_var3) : \
                     (MINMAX_tmp_var2 < (MINMAX_tmp_var3=(Z)) ? \
                      MINMAX_tmp_var2 : MINMAX_tmp_var3))

#define MAXEQ(X,Y) if((MINMAX_tmp_var1=(Y))>(X)) (X)=(MINMAX_tmp_var1)
#define MINEQ(X,Y) if((MINMAX_tmp_var1=(Y))<(X)) (X)=(MINMAX_tmp_var1)
#define ABS(X) (((MINMAX_tmp_var1 = (X)) < 0) ? \
                -MINMAX_tmp_var1 : MINMAX_tmp_var1)
#define ROUND(X) ((int)((X)+0.5))
#define NUM(X) ((int)X-(int)'0')

#endif /* MINMAX_H */
