#ifndef ARGS_H
#define ARGS_H

#define CHECKARGNUM(N,S)	if (argc != (N)+1) fprintf(stderr,"Usage: %s %s\n",argv[0],S), exit(1)

#define CHECKARGRANGE(B,E,S)    if (argc < (B)+1 || argc > (E)+1) fprintf(stderr,"Usage: %s %s\n",argv[0],S), exit(1)

#define CHECKTHR(N)	threshold = atoi(argv[N]);\
			if (threshold<0) fprintf(stderr,"%s: threshold has to be a non-negative integer\n",argv[0]), exit(1)
#endif /* ARGS_H */
