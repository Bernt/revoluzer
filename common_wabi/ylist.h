#ifndef YLIST_H
#define YLIST_H
/*****************************************************************************
 *  FILE   : ylist.h
 *  AUTHOR : Jens Stoye
 *  DATE   : 05/2001
 *****************************************************************************
 *
 * Functions regarding ylists and hlists (llists or ulists):
 *
 *   new_list()
 *   init_ylist()
 *   delete_list_entry()
 *   ci_free_list()
 *   new_hlist()
 *   new_hlist_entry()
 *   prepend_hlist_entry()
 *   remove_hlist_entry()
 *   remove_hlist()
 *   f()
 *
 *****************************************************************************/

/*****************************************************************************
 * CONSTANTS
 *****************************************************************************/
#define DELETED  -1

/*****************************************************************************
 * TYPES
 *****************************************************************************/
typedef struct LIST {
  struct LIST_ENTRY *space; //memory block for ylist elements
  int len; //length of ylist
} list;

typedef struct LIST_ENTRY {
  int pos;
  struct LIST_ENTRY *prev, //previous (undeleted) element in ylist
                    *next; //next (undeleted) element in ylist
  struct HLIST_ENTRY *u, //pointer to appropriate ulist element
                     *l; //pointer to appropriate llist element
} list_entry;

typedef struct HLIST {
  struct HLIST_ENTRY *head; //head of ulist resp. llist
} hlist;

typedef struct HLIST_ENTRY {
  int val; //value of ulist resp. llist element
  struct HLIST_ENTRY *prev, //previous element in ulist resp. llist
                     *next; //next element in ulist resp. llist
  struct LIST_ENTRY *lasty; //last element of ylist with this l/u value
} hlist_entry;

/*****************************************************************************
 * LOCAL FUNCTION DEFINITIONS
 *****************************************************************************/

/*============================================================================
 * new_list()
 *
 * Allocate memory and create a new empty ylist.
 *
 * Parameters:
 *   len - length of the ylist
 *
 *---------------------------------------------------------------------------*/
list *new_list(const int len);

/*============================================================================
 * init_ylist()
 *
 * Initialize ylist and return the last element.
 *
 * Parameters:
 *   l - ylist to be initialized
 *
 *---------------------------------------------------------------------------*/
list_entry *init_ylist(list *l);

/*============================================================================
 * delete_list_entry()
 *
 * Delete an entry from ylist.
 *
 * Parameters:
 *   le - ylist entry to be deleted
 *   l - ylist
 *
 *---------------------------------------------------------------------------*/
void delete_list_entry(list_entry *le,list *l);

/*============================================================================
 * ci_free_list()
 *
 * Free the memory occupied by ylist.
 *
 * Parameters:
 *   l - the ylist
 *
 *---------------------------------------------------------------------------*/
void ci_free_list(list *l);

/*============================================================================
 * new_hlist()
 *
 * Create a new empty hlist (llist or ulist).
 *
 * Parameters:
 *   <none>
 *
 *---------------------------------------------------------------------------*/
hlist *new_hlist();

/*============================================================================
 * new_hlist_entry()
 *
 * Allocate and intialize a new hlist entry (interval of ylist).
 *
 * Parameters:
 *   val - 'height' of this interval
 *   lasty - pointer to last ylist element in this interval
 *
 *---------------------------------------------------------------------------*/
hlist_entry *new_hlist_entry(const int val,list_entry *lasty);

/*============================================================================
 * prepend_hlist_entry()
 *
 * Prepend hlist entry at the beginnig of a hlist.
 *
 * Parameters:
 *   hle - the hlist entry
 *   hl - the hlist
 *
 *---------------------------------------------------------------------------*/
void prepend_hlist_entry(hlist_entry *hle,hlist *hl);

/*============================================================================
 * remove_hlist_entry()
 *
 * Remove hlist entry from its hlist and free the memory.
 *
 * Parameters:
 *   hle - the hlist entry
 *   hl - its hlist
 *
 *---------------------------------------------------------------------------*/
void remove_hlist_entry(hlist_entry *hle,hlist *hl);

/*============================================================================
 * remove_hlist()
 *
 * Free memory of a complete hlist with all its entries.
 *
 * Parameters:
 *   hl - the hlist
 *
 *---------------------------------------------------------------------------*/
void remove_hlist(hlist *hl);

/*============================================================================
 * f()
 *
 * Compute f(x,y) = u(x,y) - l(x,y) - (y-x)
 *
 * Parameters:
 *   xle - ylist entry at position x
 *   yle - ylist entry at position y
 *
 *---------------------------------------------------------------------------*/
int f(const list_entry *xle,const list_entry *yle);

/****** EOF (ylist.h) ********************************************************/
#endif
