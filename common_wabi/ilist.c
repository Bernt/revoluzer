/*****************************************************************************
 *  FILE   : ilist.c
 *  AUTHOR : Jens Stoye
 *  DATE   : 05/2001
 *****************************************************************************
 *
 * Functions regarding interval lists (ilists). See ilist.h for details.
 *
 *****************************************************************************/

/*****************************************************************************
 * SWITCHES
 *****************************************************************************/
//#define DEBUG_ILIST

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdio.h> //...

#include "alloc.h" //ALLOC,FREE
#include "bool.h" //BOOL

#include "ylist.h" //list
#include "intervals.h" //intervals,write_interval()
#include "ilist.h" //...

/*****************************************************************************
 * FUNCTION DEFINITIONS
 *****************************************************************************/

/*---------------------------------------------------------------------------*/
ilist *new_ilist(const intervals *i)
{
  ilist *il;
  ilist_entry **sort,**sort2,**stack,*ile,*ile2;
  int j,pos,*count,nextfree;

#ifdef DEBUG_ILIST
  fprintf(stderr,"Initial intervals:");
  write_intervals(stderr,i);
#endif

  //allocate memory
  ALLOC(il,ilist,1);

  il->num = i->num;
  il->len = i->len;
  il->next_ilist_entry = il->next_chain = 0;

  ALLOC(il->lpos,ilist_entry*,il->len); //last never used
  ALLOC(il->rpos,ilist_entry*,il->len); //first never used
  for(j=0; j<il->len; j++) {
    il->lpos[j] = NULL;
    il->rpos[j] = NULL;
  }

  ALLOC(il->chain_space,chain,il->num);
  ALLOC(il->ilist_space,ilist_entry,il->num);
  for(j=0; j<i->num; j++) {
    if(i->start[j]<i->end[j]) { //ignore circular intervals
      new_ilist_entry(il,i->start[j],i->end[j]);
    }
    else {
      il->num--;
    }
  }

  //increasing counting sort according to start points
  ALLOC(count,int,il->len);
  ALLOC(sort,ilist_entry*,il->num);
  ALLOC(sort2,ilist_entry*,il->num);
  for(j=0; j<il->len; j++) {
    count[j] = 0;
  }
  for(j=0; j<il->num; j++) {
    ile = &(il->ilist_space[j]);
    count[ile->pos]++;
  }
  for(j=1; j<il->len; j++) {
    count[j] += count[j-1];
  }
  for(j=0; j<il->num; j++) {
    ile = &(il->ilist_space[j]);
    pos = --count[ile->pos];
    sort[pos] = ile;
  }

#ifdef DEBUG_ILIST
  fprintf(stderr," After increasing left sorting:");
  for(j=0; j<il->num; j++) {
    fprintf(stderr," [%d,%d]",sort[j]->pos,sort[j]->end);
  }
  fprintf(stderr,"\n");
#endif

  //decreasing counting sort according to end points with start points reversed
  for(j=0; j<il->len; j++) {
    count[j] = 0;
  }
  for(j=0; j<il->num; j++) {
    ile = sort[j];
    count[ile->end]++;
  }
  for(j=1; j<il->len; j++) {
    count[j] += count[j-1];
  }
  for(j=0; j<il->num; j++) {
    ile = sort[j];
    pos = --count[ile->end];
    sort2[il->num-pos-1] = ile;
  }

#ifdef DEBUG_ILIST
  fprintf(stderr," After decreasing right sorting:");
  for(j=0; j<il->num; j++) {
    fprintf(stderr," [%d,%d]",sort2[j]->pos,sort2[j]->end);
  }
  fprintf(stderr,"\n");
#endif
  FREE(count);
  FREE(sort);

  //creation of the interval data structure (right-to-left) using a stack
  ALLOC(stack,ilist_entry*,il->num);
  nextfree = 0;
  
  for(j=0; j<il->num; j++) {
    ile = sort2[j];
    while(nextfree>0 && (ile2=stack[nextfree-1])->pos>ile->end) {
      nextfree--;
    }
    if(nextfree>0 && ile2->pos>ile->pos) { //max. 1 proper overlap
      prepend_ilist_entry(ile,ile2->mychain);
      nextfree--;
    }
    else {
      prepend_ilist_entry(ile,new_chain(il));
    }
    stack[nextfree++] = ile;

    ile->up = il->lpos[ile->pos];
    ile->down = NULL;
    if(ile->up != NULL) {
      ile->up->down = ile;
    }
    il->lpos[ile->pos] = ile;
  }

  FREE(sort2);
  FREE(stack);

#ifdef DEBUG_ILIST
  fprintf(stderr," Final ilist:");
  write_ilist(stderr,il);
#endif

  return il;
} // new_ilist()

/*---------------------------------------------------------------------------*/
void free_ilist(ilist *il)
{
  FREE(il->chain_space);
  FREE(il->ilist_space);
  FREE(il->lpos);
  FREE(il->rpos);
  FREE(il);
  return;
} // free_ilist()

/*---------------------------------------------------------------------------*/
void write_ilist(FILE *fptr,const ilist *il)
{
  int j;
  ilist_entry *ile;

  for(j=0; j<il->len; j++) {
    for(ile=il->lpos[j]; ile!=NULL; ile=ile->up) {
      fprintf(fptr," [%d,%d]",ile->pos,ile->end);
    }
  }
  fprintf(fptr,"\n");
  return;
} // write_ilist()

/*---------------------------------------------------------------------------*/
chain *new_chain(ilist *il)
{
  chain *c;

  c = &(il->chain_space[il->next_chain++]);
  c->ilist = NULL;
  c->first_active = NULL;
  return c;
} // new_chain()

/*---------------------------------------------------------------------------*/
ilist_entry *new_ilist_entry(ilist *il,const int pos,const int end)
{
  ilist_entry *ile;

  ile = &(il->ilist_space[il->next_ilist_entry++]);
  ile->pos = pos;
  ile->end = end;
  ile->prev = ile->next = ile->down = ile->up = NULL;
  ile->prev_active = ile->next_active = NULL;
  ile->mychain = NULL;

  return ile;
} // new_ilist_entry()

/*---------------------------------------------------------------------------*/
void delete_ilist_entry(ilist_entry *ile,ilist *il)
{
  if(ile->prev != NULL) {
    ile->prev->next = ile->next;
  }
  if(ile->next != NULL) {
    ile->next->prev = ile->prev;
  }
  if(ile->down != NULL) {
    ile->down->up = ile->up;
  }
  else {
    il->lpos[ile->pos] = ile->up;
  }
  if(ile->up != NULL) {
    ile->up->down = ile->down;
  }
  return;
} // delete_ilist_entry()

/*---------------------------------------------------------------------------*/
void prepend_ilist_entry(ilist_entry *ile,chain *c)
{
  ile->next = c->ilist;
  ile->prev = NULL;
  ile->mychain = c;
  if(c->ilist != NULL) {
    c->ilist->prev = ile;
  }
  c->ilist = ile;

  return;
} // prepend_ilist_entry()

/*---------------------------------------------------------------------------*/
void merge_ilist_entries(ilist_entry *ile,ilist *il)
{
  if(ile != NULL) {
    if(ile->next == NULL) {
      if(ile->prev_active != NULL) {
        ile->prev_active->next_active = ile->next_active; // (NULL)
      }
      else if(ile->mychain->first_active == ile) {
        ile->mychain->first_active = ile->next_active; // (NULL)
      }
      delete_ilist_entry(ile,il);
    }
    else {
      if(il->rpos[ile->next->end] == ile->next) {
        il->rpos[ile->next->end] = ile;
      }
      ile->end = ile->next->end;
      if(ile->next_active == ile->next) {
        ile->next_active = ile->next->next_active;
      }
      if(ile->next->next_active != NULL) {
        ile->next_active = ile->next->next_active;
        ile->next_active->prev_active = ile;
      }
      if(ile->next->prev_active!=NULL && ile->next->prev_active!=ile) {
        ile->prev_active = ile->next->prev_active;
        ile->prev_active->next_active = ile;
      }
      if(ile->next->prev_active==NULL &&
         ile->mychain->first_active==ile->next) {
        ile->prev_active = NULL;
        ile->mychain->first_active = ile;
      }
      delete_ilist_entry(ile->next,il);
    }
  }
  return;
} // merge_ilist_entries()

/*---------------------------------------------------------------------------*/
void activate_ilist_entry(ilist_entry *ile,ilist *il,const list *l)
{
  ile->next_active = ile->mychain->first_active;
  if(ile->mychain->first_active != NULL) {
    ile->mychain->first_active->prev_active = ile;
  }
  ile->mychain->first_active = ile;

  if(l->space[ile->end].pos == DELETED) {
    merge_ilist_entries(ile,il);
  }
  il->rpos[ile->end] = ile;

  return;

} // activate_ilist_entry()

/*---------------------------------------------------------------------------*/
void deactivate_ilist_entry(ilist_entry *ile,ilist *il,const int x)
{
  if(ile->next_active != NULL) {
    ile->next_active->prev_active = ile->prev_active;
  }
  if(ile->prev_active != NULL) {
    ile->prev_active->next_active = ile->next_active;
  }
  else {
    ile->mychain->first_active = ile->next_active;
  }
  ile->next_active = NULL;
  ile->prev_active = NULL;

  //re-direct head of vertical list at position x if the active sublist of this
  //chain is empty (test necessary, but why?)
  if(ile->mychain->first_active == NULL) {
    il->lpos[x] = il->lpos[x]->up;
  }
  return;

} // deactivate_ilist_entry()

/*---------------------------------------------------------------------------*/
list_entry *get_y_candidate(const int x,const ilist *il,const list *l)
{
  ilist_entry *ile;

  ile = il->lpos[x];
  if(ile == NULL) {
    return NULL;
  }
  ile = ile->mychain->first_active;
  if(ile == NULL) {
    return NULL;
  }
  return &l->space[ile->end];
} // get_y_candidate()

/*---------------------------------------------------------------------------*/
void write_common_intervals(FILE *fptr,const intervals *i, const int offset,
                            const BOOL circularity)
{
  ilist *il;
  chain *c;
  ilist_entry *ile1,*ile2;
  intervals *i2;
  int j,start,end,maxlen,shift;

  if(i->num==0) {
    return;
  }

  if(circularity==ILIST_LINEAR) {

    il = new_ilist(i);
    for(j=0; j<il->next_chain; j++) {
      c = &il->chain_space[j];
      for(ile1=c->ilist; ile1!=NULL; ile1=ile1->next) {
        for(ile2=ile1; ile2!=NULL; ile2=ile2->next) {
          start = ile1->pos;
          end = ile2->end;
          write_interval(fptr,offset+start,offset+end);
        }
      }
    }
    free_ilist(il);
  }

  else { //circular case

    //WARNING: It might happen that (very few) intervals are reported
    //         more than once. This occurs for common intervals of length
    //         i->len/2 at the very beginning and the very end of the
    //         permutation, for example in 
    //         pi = 1 2 3 4 5 6 7 8 )
    //         If this is a problem, once could try to fix it by explicitly
    //         testing for the described condition.

    maxlen = i->len/2;
    shift = i->len/2;

    //report ordinary short intervals + complements
    il = new_ilist(i);
    for(j=0; j<il->next_chain; j++) {
      c = &il->chain_space[j];
      for(ile1=c->ilist; ile1!=NULL; ile1=ile1->next) {
        for(ile2=ile1; ile2!=NULL && (end=ile2->end)-(start=ile1->pos)<maxlen;
            ile2=ile2->next) {
          write_interval(fptr,offset+start,offset+end);
		  write_interval(fptr,offset+(end+1)%i->len,
                         offset+(start+i->len-1)%i->len);
        }
      }
    }
    free_ilist(il);

    //report circular short intervals + complements
    i2 = dup_intervals(i);
    shift_intervals(i2,shift,INTERVALS_LARGEST_TO_FRONT);

    il = new_ilist(i2);
    for(j=0; j<il->next_chain; j++) {
      c = &il->chain_space[j];
      for(ile1=c->ilist; ile1!=NULL; ile1=ile1->next) {
        for(ile2=ile1; ile2!=NULL && (end=ile2->end)-(start=ile1->pos)<maxlen-1;
            ile2=ile2->next) {
          if(start<shift && end>=shift) {
            write_interval(fptr,offset+(start-shift+i->len)%i->len,
                           offset+(end-shift+i->len)%i->len);
			write_interval(fptr,offset+(end+1-shift+i->len)%i->len,
                           offset+(start-1-shift+i->len)%i->len);
          }
        }
      }
    }
    free_ilist(il);
    free_intervals(i2);
  }

  fprintf(fptr,"\n");

  return;
} // write_common_intervals()

void get_common_intervals(intervals ** all, const intervals *i, const int offset,
                            const BOOL circularity)
{
	
  ilist *il;
  chain *c;
  ilist_entry *ile1,*ile2;
  intervals *i2;
  int j,start,end,maxlen,shift;

  if(i->num==0) {
    return;
  }

  if(circularity==ILIST_LINEAR) {
    il = new_ilist(i);
    for(j=0; j<il->next_chain; j++) {
      c = &il->chain_space[j];
      for(ile1=c->ilist; ile1!=NULL; ile1=ile1->next) {
        for(ile2=ile1; ile2!=NULL; ile2=ile2->next) {
          start = ile1->pos;
          end = ile2->end;
		  
		  (*all)->num++;
		  REALLOC((*all)->start,int,(*all)->num);
		  REALLOC((*all)->end,int,(*all)->num);
          (*all)->start[(*all)->num-1] = offset+start;
		  (*all)->end[(*all)->num-1] = offset+end;
		  //~ write_interval(fptr,offset+start,offset+end);
        }
      }
    }
    free_ilist(il);
  }

  else { //circular case
    //WARNING: It might happen that (very few) intervals are reported
    //         more than once. This occurs for common intervals of length
    //         i->len/2 at the very beginning and the very end of the
    //         permutation, for example in 
    //         pi = 1 2 3 4 5 6 7 8 )
    //         If this is a problem, once could try to fix it by explicitly
    //         testing for the described condition.

    maxlen = i->len/2;
    shift = i->len/2;

    //report ordinary short intervals + complements
    il = new_ilist(i);
    for(j=0; j<il->next_chain; j++) {
      c = &il->chain_space[j];
      for(ile1=c->ilist; ile1!=NULL; ile1=ile1->next) {
        for(ile2=ile1; ile2!=NULL && (end=ile2->end)-(start=ile1->pos)<maxlen;
            ile2=ile2->next) {
		  
		  (*all)->num += 2;
		  REALLOC((*all)->start,int,(*all)->num);
		  REALLOC((*all)->end,int,(*all)->num);
          (*all)->start[(*all)->num-2] = offset+start;
		  (*all)->end[(*all)->num-2] = offset+end;
		  (*all)->start[(*all)->num-1] = offset+(end+1)%i->len;
		  (*all)->end[(*all)->num-1] = offset+(start+i->len-1)%i->len;
		  
          //~ write_interval(stdout,offset+start,offset+end);
          //~ write_interval(stdout,offset+(end+1)%i->len,
                         //~ offset+(start+i->len-1)%i->len);
        }
      }
    }
    free_ilist(il);

    //report circular short intervals + complements
    i2 = dup_intervals(i);
    shift_intervals(i2,shift,INTERVALS_LARGEST_TO_FRONT);

    il = new_ilist(i2);
    for(j=0; j<il->next_chain; j++) {
      c = &il->chain_space[j];
      for(ile1=c->ilist; ile1!=NULL; ile1=ile1->next) {
        for(ile2=ile1; ile2!=NULL && (end=ile2->end)-(start=ile1->pos)<maxlen-1;
            ile2=ile2->next) {
          if(start<shift && end>=shift) {
			(*all)->num += 2;
			REALLOC((*all)->start,int,(*all)->num);
		    REALLOC((*all)->end,int,(*all)->num);  
			(*all)->start[(*all)->num-2] = offset+(start-shift+i->len)%i->len;
		    (*all)->end[(*all)->num-2] = offset+(end-shift+i->len)%i->len;
		    (*all)->start[(*all)->num-1] = offset+(end+1-shift+i->len)%i->len;
		    (*all)->end[(*all)->num-1] = offset+(start-1-shift+i->len)%i->len;
            
			//~ write_interval(stdout,offset+(start-shift+i->len)%i->len,
                           //~ offset+(end-shift+i->len)%i->len);
            //~ write_interval(stdout,offset+(end+1-shift+i->len)%i->len,
                           //~ offset+(start-1-shift+i->len)%i->len);
          }
        }
      }
    }
    free_ilist(il);
    free_intervals(i2);
  }

  //~ fprintf(fptr,"\n");

  return;
} // get_common_intervals()




/****** EOF (ilist.c) ********************************************************/
