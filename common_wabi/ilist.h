#ifndef ILIST_H
#define ILIST_H
/*****************************************************************************
 *  FILE   : ilist.h
 *  AUTHOR : Jens Stoye
 *  DATE   : 05/2001
 *****************************************************************************
 *
 * Functions regarding interval lists (ilists):
 *
 *   new_ilist()
 *   free_ilist()
 *   write_ilist()
 *   new_chain()
 *   new_ilist_entry()
 *   delete_ilist_entry()
 *   prepend_ilist_entry()
 *   merge_ilist_entries()
 *   activate_ilist_entry()
 *   deactivate_ilist_entry()
 *   get_y_candidate()
 *   write_common_intervals()
 *
 *****************************************************************************/

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdio.h> //FILE
#include "bool.h" //BOOL
#include "ylist.h" //list
#include "intervals.h" //intervals
#include "genomes.h" //genomes

/*****************************************************************************
 * TYPES
 *****************************************************************************/
typedef struct ILIST {
  int num, //number of entries in this list
      len, //length of underlying ylist
      next_ilist_entry, //pointer to next available ilist_entry in ilist_space
      next_chain; //pointer to next available chain in chain_space
  struct CHAIN *chain_space; //memory block for the chains of this ilist
  struct ILIST_ENTRY *ilist_space, //memory block for the entries in this ilist
                     **lpos, //starting points of vertical lists
                     **rpos; //pointer to (only) active interval ending here
} ilist;

typedef struct CHAIN {
  struct ILIST_ENTRY *ilist, //ilist this chain belogs to
                     *first_active; //ptr to first active entry in this chain
} chain;

typedef struct ILIST_ENTRY {
  int pos, //starting position of this interval
      end; //ending position of this interval
  struct ILIST_ENTRY *prev, //previous element in this list
                     *next, //next element in this list
                     *down, //previous element in vertical list
                     *up, //next element in vertical list
                     *prev_active, //previous element in active sublist
                     *next_active; //next element in active sublist
  struct CHAIN *mychain; //pointer to chain this element belongs to
} ilist_entry;

/*****************************************************************************
 * FUNCTION PROTOTYPES
 *****************************************************************************/

/*============================================================================
 * new_ilist()
 *
 * Allocate memory and intialize a new interval list based on the intervals
 * in intervals struct i: first sort the intervals, then create chains based
 * on their nesting level.
 *
 * Parameters:
 *   i - intervals struct
 *
 *---------------------------------------------------------------------------*/
ilist *new_ilist(const intervals *i);

/*============================================================================
 * free_ilist()
 *
 * Free the memory occupied by an interval list.
 *
 * Parameters:
 *   il - the interval list
 *
 *---------------------------------------------------------------------------*/
void free_ilist(ilist *il);

/*============================================================================
 * write_ilist()
 *
 * Write the intervals currently stored in the interval list in left-to-right
 * oder to fptr.
 *
 * Parameters:
 *   fptr - file pointer to be written to
 *   il - the interval list
 *
 *---------------------------------------------------------------------------*/
void write_ilist(FILE *fptr,const ilist *il);

/*============================================================================
 * new_chain()
 *
 * Create a new empty chain of list elements.
 *
 * Parameters:
 *   il - ilist this chain belongs to
 *
 *---------------------------------------------------------------------------*/
chain *new_chain(ilist *il);

/*============================================================================
 * new_ilist_entry()
 *
 * Generate and initialize a new ilist entry.
 *
 * Parameters:
 *   il - interval list the new entry belongs to
 *   pos - starting position of the interval
 *   end - ending position of the interval
 *
 *---------------------------------------------------------------------------*/
ilist_entry *new_ilist_entry(ilist *il,const int pos,const int end);

/*============================================================================
 * delete_ilist_entry()
 *
 * Delete an ilist entry from its list and vertical list. Do not free the
 * memory.
 *
 * Parameters:
 *   ile - ilist entry to be deleted
 *   il - interval list this entry belongs to
 *
 *---------------------------------------------------------------------------*/
void delete_ilist_entry(ilist_entry *ile,ilist *il);

/*============================================================================
 * prepend_ilist_entry()
 *
 * Prepend an ilist entry at the beginning of a chain.
 *
 * Parameters:
 *   ile - ilist entry to be prepended
 *   c - the chain where ile is to be prepended
 *
 *---------------------------------------------------------------------------*/
void prepend_ilist_entry(ilist_entry *ile,chain *c);

/*============================================================================
 * merge_ilist_entries()
 *
 * Merge an interval list entry with its right neighbor, if that neighbor
 * exists. If not, delete the entry. Update the vertical list, the interval
 * end pointer and the active sublist.
 *
 * Parameters:
 *   ile - ilist entry to be merged with its right neighbor
 *   il - interval list this element belongs to
 *
 *---------------------------------------------------------------------------*/
void merge_ilist_entries(ilist_entry *ile,ilist *il);

/*============================================================================
 * activate_ilist_entry()
 *
 * Activate interval list entry, i.e. prepend to the active sublist of its
 * chain. If the right end of this intervals is already deleted from the ylist,
 * also merge this interval with its right neighbor.
 *
 * Parameters:
 *   ile - ilist entry to be activated
 *   il - the interval list
 *   l - the ylist
 *
 *---------------------------------------------------------------------------*/
void activate_ilist_entry(ilist_entry *ile,ilist *il,const list *l);

/*============================================================================
 * deactivate_ilist_entry()
 *
 * Deactivate an interval list entry, i.e. remove it from its active sublist.
 * Keep the interval in its chain. Update the vertical list at position x if
 * the active sublist of this chain is empty.
 *
 * Parameters:
 *   ile - ilist entry to be deactivated
 *   il - the interval list
 *   x - current value of x needed for update of head of vertical list
 *
 *---------------------------------------------------------------------------*/
void deactivate_ilist_entry(ilist_entry *ile,ilist *il,const int x);

/*============================================================================
 * get_y_candidate()
 *
 * Return the next right end candidate, i.e. the head of the vertical list
 * at position x.
 *
 * Parameters:
 *   x - the current value of x
 *   il - the interval list
 *   l - the ylist
 *
 *---------------------------------------------------------------------------*/
list_entry *get_y_candidate(const int x,const ilist *il,const list *l);

/*============================================================================
 * write_common_intervals()
 *
 * Write the common intervals generated by the irreducible intervals of an
 * intervals struct.
 *
 * Parameters:
 *   fptr - file pointer to be written to
 *   i - the intervals struct
 *   g - genomes struct (needed to detect circular chromosomes)
 *   offset - for interval borders (needed in multichromosomal genomes)
 *
 *---------------------------------------------------------------------------*/
#define ILIST_CIRCULAR TRUE
#define ILIST_LINEAR FALSE
void write_common_intervals(FILE *fptr,const intervals *i,const int offset,
                            const BOOL circularity);
							
void get_common_intervals(intervals ** all, const intervals *i,const int offset,
                            const BOOL circularity);


/****** EOF (ilist.h) ********************************************************/
#endif
