/*****************************************************************************
 *  FILE   : genomes.c
 *  AUTHOR : Jens Stoye
 *  DATE   : 07/2001
 *****************************************************************************
 *
 * Functions regarding genomes. See genomes.h for details.
 *
 *****************************************************************************/

/*****************************************************************************
 * SWITCHES
 *****************************************************************************/
//#define DEBUG_GENOMES

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdio.h> //...
#include <ctype.h> //isdigit()
#include <limits.h> //INT_MIN
#include <string.h> //strdup()

#include "alloc.h" //ALLOC,FREE
#include "minmax.h" //NUM
#include "bool.h" //BOOL,TRUE,FALSE
#include "files.h" //FGETS()

#include "genomes.h" //...

/*****************************************************************************
 * FUNCTION DEFINITIONS
 *****************************************************************************/
#define GENOMES_ALLOCSTEP 1000
#define GENOMES_MAX_LENGTH 1000

genomes *read_genomes(FILE *fptr)
{
  genomes *g;
  int val,num,gene,chromosome;
  char genome_name[GENOMES_MAX_LENGTH];
  BOOL sign;

  //allocate struct and initialize first genome
  ALLOC(g,genomes,1);
  g->num = 0;
  g->max = INT_MIN;
  ALLOC(g->name,char*,1);
  ALLOC(g->len,int,1);
  ALLOC(g->g,int*,1);
  ALLOC(g->c,int*,1);
  ALLOC(g->s,BOOL*,1);
  g->len[g->num] = 0;
  ALLOC(g->g[g->num],int,1);
  ALLOC(g->c[g->num],int,1);
  ALLOC(g->s[g->num],BOOL,1);
  
  //read first genome name
  val = fgetc(fptr);
  while(!feof(fptr) && val!=GENOMES_COMMENT_SIGN) {
    val = fgetc(fptr); //proceed to first comment sign
  }
  FGETS(genome_name,GENOMES_MAX_LENGTH,fptr);
  g->name[0] = strdup(genome_name);

  //proceed to next relevant character
  val = fgetc(fptr);
  while(!feof(fptr) && val!=GENOMES_COMMENT_SIGN &&
        val!=GENOMES_PLUS_SIGN && val!=GENOMES_MINUS_SIGN &&
        !isdigit(val)) {
    val = fgetc(fptr);
  }

  //read genes and following genomes
  while(!feof(fptr)) {
 
    if(val == GENOMES_COMMENT_SIGN) { //begin of new genome determined
      if(g->len[g->num] > 0) {
        if(g->c[g->num][g->len[g->num]-1] == GENOMES_NO_END) {
          g->c[g->num][g->len[g->num]-1] = GENOMES_LINEAR;
        }
        REALLOC(g->g[g->num],int,g->len[g->num]); //prune genome length
        REALLOC(g->c[g->num],int,g->len[g->num]);
        REALLOC(g->s[g->num],BOOL,g->len[g->num]);
        g->num++;
        REALLOC(g->name,char*,g->num+1);
        REALLOC(g->len,int,g->num+1);
        REALLOC(g->g,int*,g->num+1);
        REALLOC(g->c,int*,g->num+1);
        REALLOC(g->s,BOOL*,g->num+1);
        g->len[g->num] = 0;
        ALLOC(g->g[g->num],int,1);
        ALLOC(g->c[g->num],int,1);
        ALLOC(g->s[g->num],BOOL,1);
      }
      FGETS(genome_name,GENOMES_MAX_LENGTH,fptr); //read next genome name
      g->name[g->num] = strdup(genome_name);
      val = fgetc(fptr);
    }
    else {
      //read and set sign
      while(!feof(fptr) && val!=GENOMES_PLUS_SIGN && val!=GENOMES_MINUS_SIGN &&
            !isdigit(val)) {
        val = getc(fptr);
      }
      sign = val!=GENOMES_MINUS_SIGN;

      //read and set gene
      while(!feof(fptr) && !isdigit(val)) {
        val = getc(fptr);
      }
      num = 0;
      while(!feof(fptr) && isdigit(val)) {
        num *= 10;
        num += NUM(val);
        val = fgetc(fptr);
      }
      gene = num-1;

      //read and set chromosome end
      while(!feof(fptr) && val!=GENOMES_COMMENT_SIGN &&
            val!=GENOMES_LINEAR_SIGN && val!=GENOMES_CIRCULAR_SIGN &&
            val!=GENOMES_PLUS_SIGN && val!=GENOMES_MINUS_SIGN &&
            !isdigit(val)) {
        val = fgetc(fptr);
      }
      chromosome = val==GENOMES_LINEAR_SIGN ? GENOMES_LINEAR :
                   (val==GENOMES_CIRCULAR_SIGN ? GENOMES_CIRCULAR :
                    GENOMES_NO_END);

      //store gene etc.
      if(g->len[g->num]%GENOMES_ALLOCSTEP == 0) {
        REALLOC(g->g[g->num],int,g->len[g->num]+GENOMES_ALLOCSTEP);
        REALLOC(g->c[g->num],int,g->len[g->num]+GENOMES_ALLOCSTEP);
        REALLOC(g->s[g->num],BOOL,g->len[g->num]+GENOMES_ALLOCSTEP);
      }
      g->g[g->num][g->len[g->num]] = gene;
      g->c[g->num][g->len[g->num]] = chromosome;
      g->s[g->num][g->len[g->num]] = sign;
      g->len[g->num]++;
      MAXEQ(g->max,gene);
    }

    //proceed to next relevant character
    while(!feof(fptr) && val!=GENOMES_COMMENT_SIGN &&
          val!=GENOMES_PLUS_SIGN && val!=GENOMES_MINUS_SIGN &&
          !isdigit(val)) {
      val = fgetc(fptr);
    }

  }

  //prune last genome
  if(g->len[g->num] > 0) {
    if(g->c[g->num][g->len[g->num]-1] == GENOMES_NO_END) {
      g->c[g->num][g->len[g->num]-1] = GENOMES_LINEAR;
    }
    if(g->len[g->num] > 0) {
      REALLOC(g->g[g->num],int,g->len[g->num]);
      REALLOC(g->c[g->num],int,g->len[g->num]);
      REALLOC(g->s[g->num],BOOL,g->len[g->num]);
    }
    g->num++;
  }

#ifdef DEBUG_GENOMES
  write_genomes(stderr,g);
#endif

  return g;
} //read_genomes()

/*---------------------------------------------------------------------------*/
#define GENOMES_OUTPUT_LINE_LEN 40

void write_genomes(FILE *fptr,const genomes *g)
{
  int i,j,x;

  //if unsigned: do not write plus signs
  if(is_unsigned(g)) {
    for(i=0; i<g->num; i++) {
      fprintf(fptr,"%c%s\n",GENOMES_COMMENT_SIGN,g->name[i]);
      for(x=0; x<g->len[i]; x+=GENOMES_OUTPUT_LINE_LEN) {
        for(j=x; j<x+GENOMES_OUTPUT_LINE_LEN && j<g->len[i]; j++) {
          fprintf(fptr,"%d%c",g->g[i][j]+1,
                  g->c[i][j]==GENOMES_LINEAR?GENOMES_LINEAR_SIGN:
                  (g->c[i][j]==GENOMES_CIRCULAR?GENOMES_CIRCULAR_SIGN:' '));
        }
      }
      fprintf(fptr,"\n");
    }
  }
  else {
    for(i=0; i<g->num; i++) {
      fprintf(fptr,"%c%s\n",GENOMES_COMMENT_SIGN,g->name[i]);
      for(x=0; x<g->len[i]; x+=GENOMES_OUTPUT_LINE_LEN) {
        for(j=x; j<x+GENOMES_OUTPUT_LINE_LEN && j<g->len[i]; j++) {
          fprintf(fptr,"%c%d%c",
                  g->s[i][j]?GENOMES_PLUS_SIGN:GENOMES_MINUS_SIGN,
                  g->g[i][j]+1,
                  g->c[i][j]==GENOMES_LINEAR?GENOMES_LINEAR_SIGN:
                  (g->c[i][j]==GENOMES_CIRCULAR?GENOMES_CIRCULAR_SIGN:' '));
        }
      }
      fprintf(fptr,"\n");
    }
  }

  return;
} //write_genomes()

/*---------------------------------------------------------------------------*/
void free_genomes(genomes *g)
{
  int k;

  for(k=0; k<g->num; k++) {
    FREE(g->g[k]);
    FREE(g->c[k]);
    FREE(g->s[k]);
    FREE(g->name[k]);
  }
  FREE(g->len);
  FREE(g->g);
  FREE(g->c);
  FREE(g->s);
  FREE(g->name);
  FREE(g);
  return;
} //free_genomes()

/*---------------------------------------------------------------------------*/
genomes *copy_genomes(const genomes *g)
{
  genomes *g2;
  int k,j;

  ALLOC(g2,genomes,1);
  ALLOC(g2->len,int,g->num);
  ALLOC(g2->g,int*,g->num);
  ALLOC(g2->c,int*,g->num);
  ALLOC(g2->s,BOOL*,g->num);
  ALLOC(g2->name,char*,g->num);
  for(k=0; k<g->num; k++) {
    ALLOC(g2->g[k],int,g->len[k]);
    ALLOC(g2->c[k],int,g->len[k]);
    ALLOC(g2->s[k],BOOL,g->len[k]);
  }

  g2->num = g->num;
  g2->max = g->max;
  for(k=0; k<g->num; k++) {
    g2->len[k] = g->len[k];
    for(j=0; j<g->len[k]; j++) {
      g2->g[k][j] = g->g[k][j];
      g2->c[k][j] = g->c[k][j];
      g2->s[k][j] = g->s[k][j];
    }
    g2->name[k] = strdup(g->name[k]);
  }

  return g2;
} //copy_genomes()

/*---------------------------------------------------------------------------*/
void sort_genomes(genomes *g)
{
  int *pi0,*pi0inv,*pii,*piiinv,i,j;
  BOOL *si0,*sii;

  ALLOC(pi0,int,g->len[0]);
  ALLOC(pi0inv,int,g->len[0]);
  ALLOC(si0,BOOL,g->len[0]);
  ALLOC(pii,int,g->len[0]);
  ALLOC(piiinv,int,g->len[0]);
  ALLOC(sii,BOOL,g->len[0]);
  for(j=0; j<g->len[0]; j++) {
    pi0[j] = g->g[0][j];
    pi0inv[g->g[0][j]] = j;
    si0[j] = g->s[0][j];
  }
  for(i=0; i<g->num; i++) {
    for(j=0; j<g->len[0]; j++) {
      pii[j] = g->g[i][j];
      piiinv[g->g[i][j]] = j;
      sii[j] = g->s[i][j];
    }
    for(j=0; j<g->len[0]; j++) {
      g->g[i][j] = pi0inv[pii[j]];
      g->s[i][j] = si0[pi0inv[pii[j]]]==sii[piiinv[pii[j]]];
    }
  }
  FREE(pi0);
  FREE(pi0inv);
  FREE(si0);
  FREE(pii);
  FREE(piiinv);
  FREE(sii);
  return;
} /* sort_genomes() */

/*---------------------------------------------------------------------------*/
BOOL is_unichromosomal(const genomes *g)
{
  BOOL t;
  int i,j;

  for(t=TRUE,i=0; t && i<g->num; i++) {
    for(j=0; t && j<g->len[i]-1; j++) {
      t &= g->c[i][j]==GENOMES_NO_END;
    }
  }
  return t;
} //is_unichromosomal()

/*---------------------------------------------------------------------------*/
BOOL is_unsigned(const genomes *g)
{
  BOOL t;
  int i,j;
  
  for(t=TRUE,i=0; t && i<g->num; i++) {
    for(j=1; t && j<g->len[i]; j++) {
      t &= g->s[i][j]==g->s[i][0];
    }
  }
  return t;
} //is_unsigned()

/*---------------------------------------------------------------------------*/
BOOL is_non_circular(const genomes *g)
{
  BOOL t;
  int i,j;

  for(t=TRUE,i=0; t && i<g->num; i++) {
    for(j=0; t && j<g->len[i]; j++) {
      t &= g->c[i][j]!=GENOMES_CIRCULAR;
    }
  }
  return t;
} //is_non_circular()

/*---------------------------------------------------------------------------*/
BOOL are_permutations(const genomes *g)
{
  BOOL *test,t;
  int len,i,j;

  for(len=g->len[0],i=1; i<g->num; i++) {
    if(g->len[i] != len) {
      fprintf(stderr,"Warning: genomes differ in length!\n");
      return FALSE;
    }
  }
  ALLOC(test,BOOL,len);
  for(t=TRUE,i=0; t && i<g->num; i++) {
    for(j=0; j<len; j++) {
      test[j] = FALSE;
    }
    for(j=0; j<len; j++) {
      test[g->g[i][j]] = TRUE;
    }
    for(j=0; t && j<len; j++) {
      t &= test[j];
    }
  }
  FREE(test);
  return t;
} //are_permutations()

/*---------------------------------------------------------------------------*/
int *new_direct_chromosome_vector(const genomes *g,const int k,
                                  const int start,const int end)
{
  int j,len,c_num,*c;

  len = end-start+1;
  ALLOC(c,int,len);
  c_num = 0;
  for(j=0; j<len; j++) {
    c[j] = c_num;
    if(g->c[k][start+j] != GENOMES_NO_END) {
      c_num++;
    }
  }
  return c;
} //new_direct_chromosome_vector()

/****** EOF (genomes.c) ******************************************************/
