#ifdef __cplusplus
  extern "C" {
#endif

#ifndef IRREDUCIBLE_H
#define IRREDUCIBLE_H
/*****************************************************************************
 *  FILE   : irreducible.h
 *  AUTHOR : Jens Stoye
 *  DATE   : 05/2001
 *****************************************************************************
 *
 * Functions regarding irreducible intervals:
 *
 *   find_common_intervals()
 *
 *****************************************************************************/

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdio.h> //FILE
#include "intervals.h" //intervals

/*****************************************************************************
 * LOCAL FUNCTION DEFINITIONS
 *****************************************************************************/

/*============================================================================
 * find_common_intervals()
 *
 * Separate chromosomes into partitions, reorder genomes accordingly.
 * For each partition, call the appropriate iri_... function.
 * Write resulting common intervals to fptr.
 *
 * Parameters:
 *   fptr - file pointer
 *   g - genomes (in fact these must be permutations!)
 *
 *---------------------------------------------------------------------------*/
intervals *find_common_intervals(FILE *fptr,genomes *g, int *g0_shift, int irreducible);

/****** EOF (irreducible.h) **************************************************/
#endif

#ifdef __cplusplus
 }
#endif
