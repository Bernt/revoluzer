/*****************************************************************************
 *  FILE   : intervals.c
 *  AUTHOR : Jens Stoye
 *  DATE   : 05/2001
 *****************************************************************************
 *
 * Functions regarding intervals. See intervals.h for details.
 *
 *****************************************************************************/

/*****************************************************************************
 * SWITCHES
 *****************************************************************************/
//#define DEBUG_INTERVALS
//#define DEBUG_SORT_INTERVALS
//~ #define DEBUG_SHIFT_INTERVALS
//~ #define DEBUG_UMERGE_INTERVALS

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdio.h> //...
#include <limits.h> //INT_MIN

#include "alloc.h" //ALLOC,FREE

#include "genomes.h" //genomes
#include "intervals.h" //..

/*****************************************************************************
 * FUNCTION DEFINITIONS
 *****************************************************************************/

/*---------------------------------------------------------------------------*/
intervals *new_intervals(const int len,const int max)
{
	//~ fprintf(stderr,"new_intervals(len=%d, max=%d);\n",len, max);
  intervals *i;

  ALLOC(i,intervals,1);
  i->len = len;
  i->num = 0;
  ALLOC(i->start,int,max);
  ALLOC(i->end,int,max);
  return i;
} //new_intervals()

/*---------------------------------------------------------------------------*/
void init_intervals(intervals *i,const genomes *g,const int start,
                    const int end)
{
	//~ fprintf(stderr,"init_intervals(start=%d, end=%d);\n",start, end);
  int j,first;

  //~ for(j=start; j<end-1; j++) {
  for(j=start; j<end; j++) {
    for(first=j; j<g->len[0]-1 && g->c[0][j]==GENOMES_NO_END; j++) {
      if(g->s[0][j] == g->s[0][j+1]) {
        append_interval(i,j-start,j+1-start);
      }
    }
    if(g->c[0][j]==GENOMES_CIRCULAR) {
      append_interval(i,j-start,first-start);
    }
  }
  return;
} //init_intervals()

/*---------------------------------------------------------------------------*/
intervals *dup_intervals(const intervals *i)
{
  intervals *i_new;
  int j;

  i_new = new_intervals(i->len,i->num);
  for(j=0; j<i->num; j++) {
    append_interval(i_new,i->start[j],i->end[j]);
  }
  return i_new;
} //dup_intervals()

/*---------------------------------------------------------------------------*/
void append_interval(intervals *i,const int start,const int end)
{
#ifdef DEBUG_INTERVALS
  fprintf(stderr," New interval: [%d,%d]\n",start,end);
#endif

  i->start[i->num] = start;
  i->end[i->num] = end;
  i->num++;
  return;
} //append_interval()

/*---------------------------------------------------------------------------*/
void empty_intervals(intervals *i)
{
  i->num = 0;
  return;
} //empty_intervals()

/*---------------------------------------------------------------------------*/
void free_intervals(intervals *i)
{
  FREE(i->start);
  FREE(i->end);
  FREE(i);
  return;
} //free_intervals()

/*---------------------------------------------------------------------------*/
void write_interval(FILE *fptr,const int start,const int end)
{
  fprintf(fptr," [%d,%d]",start+1,end+1);
  return;
} //write_interval()

/*---------------------------------------------------------------------------*/
void write_intervals(FILE *fptr,const intervals *i)
{
  int j;

  for(j=0; j<i->num; j++) {
    write_interval(fptr,i->start[j],i->end[j]);
  }
  fprintf(fptr,"\n");
  return;
} //write_intervals()

/*---------------------------------------------------------------------------*/
void shift_intervals(intervals *i,const int shift,const BOOL which_to_front)
{
  intervals *i_tmp;
  int j,start,end,first,min,minpos=0,max,maxpos=0;

#ifdef DEBUG_SHIFT_INTERVALS
  fprintf(stderr,"Before shifting by %d :\n", shift);
  //~ write_intervals(stderr,i);
#endif

  i_tmp = new_intervals(i->len,i->num);
  max = INT_MIN;
  min = INT_MAX;
  for(j=0; j<i->num; j++) {
    start = (i->start[j]+shift)%i->len;
    end = (i->end[j]+shift)%i->len;
#ifdef DEBUG_SHIFT_INTERVALS
    fprintf(stderr,"(%d, %d) -> (%d,%d)\n",i->start[j],i->end[j], start, end);
#endif

   if(start>max) {
      max = start;
      maxpos = j;
    }
    if(end<min) {
      min = end;
      minpos = j;
    }
    append_interval(i_tmp,start,end);
  }

#ifdef DEBUG_SHIFT_INTERVALS
  fprintf(stderr," After shifting:");
  //~ write_intervals(stderr,i_tmp);
#endif

  if(which_to_front==INTERVALS_SMALLEST_TO_FRONT) {
    first = minpos;
  }
  else {
    first = maxpos;
  }
  for(j=0; j<i->num; j++) {
    i->start[j] = i_tmp->start[(first+j)%i->num];
    i->end[j] = i_tmp->end[(first+j)%i->num];
  }
  free_intervals(i_tmp);

#ifdef DEBUG_SHIFT_INTERVALS
  fprintf(stderr," After moving %s interval to front:",
          which_to_front==INTERVALS_LARGEST_TO_FRONT ? "largest" : "smallest");
  //~ write_intervals(stderr,i);
#endif

  return;
} //shift_intervals()

/*---------------------------------------------------------------------------*/
void umerge_intervals(intervals *i,const intervals *i2)
{
  int j,k, sorted;

#ifdef DEBUG_UMERGE_INTERVALS
  fprintf(stderr,"Before merging:");
  write_intervals(stderr,i);
  fprintf(stderr," and");
  write_intervals(stderr,i2);
#endif

  if(i->num+i2->num == 0)
	  return;
  REALLOC(i->start,int,i->num+i2->num);
  REALLOC(i->end,int,i->num+i2->num);
  
	// MB: it seemed that i and i2 arent sortet (sometimes) and that this makes problems
	// so i sorted it (totaly dumb .. but it works), eg: 
	//~ >
	//~ 1 2 6 5 7 3 4 8 9 10)
	//~ >
	//~ 1 2 10 9 8 7 6 4 5 3)
	//~ >
	//~ 1 2 5 4 3 6 7 10 8 9)
	//~ >
	//~ 1 2 3 4 5 6 7 8 10 9)
  sorted = 0;
  while(sorted == 0){
	  sorted = 1;
	  for(j=0; j<i->num-1; j++){
		  if( i->start[j] < i->start[j+1] || (i->start[j] == i->start[j+1] && 
			i->end[j] > i->end[j+1]  )){
				k = i->start[j];
				i->start[j] = i->start[j+1];
				i->start[j+1] = k;
				k = i->end[j];
				i->end[j] = i->end[j+1];
				i->end[j+1] = k;
				
				sorted = 0;
			}
	  }
  }
sorted = 0;
  while(sorted == 0){
	  sorted = 1;
	  for(j=0; j<i2->num-1; j++){
		  if( i2->start[j] < i2->start[j+1] || (i2->start[j] == i2->start[j+1] && 
			i2->end[j] > i2->end[j+1]  )){
				k = i2->start[j];
				i2->start[j] = i2->start[j+1];
				i2->start[j+1] = k;
				k = i2->end[j];
				i2->end[j] = i2->end[j+1];
				i2->end[j+1] = k;
				
				sorted = 0;
			}
	  }
  }
 #ifdef DEBUG_UMERGE_INTERVALS
  fprintf(stderr,"After sorting:");
  write_intervals(stderr,i);
  fprintf(stderr," and");
  write_intervals(stderr,i2);
#endif 
  j = i->num;
  k = i2->num;
  while(j>0 || k>0) {

	if(j>0 && (k==0 || i->start[j-1]<i2->start[k-1] ||
              (i->start[j-1]==i2->start[k-1] && i->end[j-1]>i2->end[k-1]))) {
      j--;
      i->start[j+k] = i->start[j];
      i->end[j+k] = i->end[j];
    }
    else {
      k--;
      i->start[j+k] = i2->start[k];
      i->end[j+k] = i2->end[k];
    }
  }
  i->num += i2->num;

#ifdef DEBUG_UMERGE_INTERVALS
  fprintf(stderr," After merging:");
  write_intervals(stderr,i);
#endif

  for(k=0,j=1; j<i->num; j++) {
    if(i->start[j]!=i->start[k] || i->end[j]!=i->end[k]){
      k++;
      i->start[k] = i->start[j];
      i->end[k] = i->end[j];
    }
  }
  i->num = k+1;

#ifdef DEBUG_UMERGE_INTERVALS
  fprintf(stderr," After removal of duplicates:");
  write_intervals(stderr,i);
#endif

  return;
} //umerge_intervals()

/*---------------------------------------------------------------------------*/
void sort_intervals(intervals *i)
{
  int j,pos,*count,*sort,*start,*end;

#ifdef DEBUG_SORT_INTERVALS
  fprintf(stderr," Initial unsorted intervals:");
  write_intervals(stderr,i);
#endif

  //initializations
  ALLOC(count,int,i->len);
  ALLOC(sort,int,i->num);
  ALLOC(start,int,i->num);
  ALLOC(end,int,i->num);

  //decreasing counting sort according to end points
  for(j=0; j<i->len; j++) {
    count[j] = 0;
  }
  for(j=0; j<i->num; j++) {
    count[i->end[j]]++;
  }
  for(j=1; j<i->len; j++) {
    count[j] += count[j-1];
  }
  for(j=0; j<i->num; j++) {
    pos = --count[i->end[j]];
    sort[i->num-pos-1] = j;
  }

#ifdef DEBUG_SORT_INTERVALS
  fprintf(stderr," After decreasing right sorting:");
  for(j=0; j<i->num; j++) {
    fprintf(stderr," [%d,%d]",i->start[sort[j]],i->end[sort[j]]);
  }
  fprintf(stderr,"\n");
#endif

  //increasing counting sort according to start points
  for(j=0; j<i->len; j++) {
    count[j] = 0;
  }
  for(j=0; j<i->num; j++) {
    count[i->start[sort[j]]]++;
  }
  for(j=1; j<i->len; j++) {
    count[j] += count[j-1];
  }
  for(j=0; j<i->num; j++) {
    pos = --count[i->start[sort[j]]];
    start[pos] = i->start[sort[j]];
    end[pos] = i->end[sort[j]];
  }

  //copy back into original struct
  for(j=0; j<i->num; j++) {
    i->start[j] = start[j];
    i->end[j] = end[j];
  }

#ifdef DEBUG_SORT_INTERVALS
  fprintf(stderr," After increasing left sorting:");
  write_intervals(stderr,i);
#endif

  //cleanup
  FREE(count);
  FREE(sort);
  FREE(start);
  FREE(end);

  return;
} //sort_intervals()

/*---------------------------------------------------------------------------*/
void add_complements(intervals *i)
{
  int oldnum,j;

#ifdef DEBUG_ADD_COMPLEMENTS
  fprintf(stderr," Initial intervals without compleents:");
  write_intervals(stderr,i);
#endif

  oldnum = i->num;
  REALLOC(i->start,int,oldnum*2);
  REALLOC(i->end,int,oldnum*2);
  for(j=0; j<oldnum; j++) {
    append_interval(i,(i->end[j]+1)%i->len,(i->start[j]+i->len-1)%i->len);
  }
#ifdef DEBUG_SORT_INTERVALS
  fprintf(stderr," After adding the complements:");
  write_intervals(stderr,i);
#endif

  return;
} //add_complements()

/****** EOF (intervals.c) ****************************************************/
