/*****************************************************************************
 *  FILE   : ylist.c
 *  AUTHOR : Jens Stoye
 *  DATE   : 05/2001
 *****************************************************************************
 *
 * Functions regarding ylists and hlists (llists or ulists). See ylist.h for
 * details.
 *
 *****************************************************************************/

/*****************************************************************************
 * SWITCHES
 *****************************************************************************/
//#define DEBUG_YLIST

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdio.h> //...

#include "alloc.h" //ALLOC,FREE

#include "ylist.h" //...

/*****************************************************************************
 * FUNCTION DEFINITIONS
 *****************************************************************************/
list *new_list(const int len)
{
  list *new_l;
  list_entry *le;
  int j;
  
  ALLOC(new_l,list,1);
  ALLOC(new_l->space,list_entry,len);
  new_l->len = len;
  for(j=0; j<len; j++) {
    le = &(new_l->space[j]);
    le->pos = j;
    le->u = le->l = NULL;
  }
  return new_l;
} // new_list()

/*---------------------------------------------------------------------------*/
list_entry *init_ylist(list *l)
{
  int j;
  list_entry *le;

  for(j=0; j<l->len; j++) {
    le = &(l->space[j]);
    le->pos = j;
    le->prev = j==0 ? NULL : &(l->space[j-1]);
    le->next = j==l->len-1 ? NULL : &(l->space[j+1]);
    le->u = le->l = NULL;
  }
  return &(l->space[l->len-1]); //returns last element of ylist
} // init_ylist()

/*---------------------------------------------------------------------------*/
void delete_list_entry(list_entry *le,list *l)
{
#ifdef DEBUG_YLIST
  fprintf(stderr,"Deleting %d from ylist\n",le->pos);
#endif
  if(le->prev != NULL) {
    le->prev->next = le->next;
  }
  if(le->next != NULL) {
    le->next->prev = le->prev;
  }
  le->pos = DELETED;
  return;
} // delete_list_entry()

/*---------------------------------------------------------------------------*/
void ci_free_list(list *l)
{
  FREE(l->space);
  FREE(l);
  return;
} // ci_free_list()

/*===========================================================================*/
hlist *new_hlist()
{
  hlist *new_hl;
  
  ALLOC(new_hl,hlist,1);
  new_hl->head = NULL;
  return new_hl;
} // new_hlist()

/*---------------------------------------------------------------------------*/
hlist_entry *new_hlist_entry(const int val,list_entry *lasty)
{
  hlist_entry *new_hle;
  
  ALLOC(new_hle,hlist_entry,1);
  new_hle->val = val;
  new_hle->lasty = lasty;
  return new_hle;
} // new_hlist_entry()

/*---------------------------------------------------------------------------*/
void prepend_hlist_entry(hlist_entry *hle,hlist *hl)
{
  hle->prev = NULL;
  hle->next = hl->head;
  if(hl->head != NULL) {
    hl->head->prev = hle;
  }
  hl->head = hle;
  return;
} // prepend_hlist_entry()

/*---------------------------------------------------------------------------*/
void remove_hlist_entry(hlist_entry *hle,hlist *hl)
{
  if(hle->prev != NULL) {
    hle->prev->next = hle->next;
  }
  else {
    hl->head = hle->next;
  }
  if(hle->next != NULL) {
    hle->next->prev = hle->prev;
  }
  FREE(hle);
  return;
} // remove_hlist_entry()

/*---------------------------------------------------------------------------*/
void remove_hlist(hlist *hl)
{
  while(hl->head != NULL) {
    remove_hlist_entry(hl->head,hl);
  }
  FREE(hl);
  return;
} // remove_hlist()

/*===========================================================================*/
int f(const list_entry *xle,const list_entry *yle)
{
  return yle->u->val-yle->l->val-(yle->pos-xle->pos);
} // f()

/****** EOF (ylist.c) ********************************************************/
