/*****************************************************************************
 *  FILE   : common_wabi.c
 *  AUTHOR : Jens Stoye
 *  DATE   : 07/2001
 *****************************************************************************
 *
 * Find all common intervals in k multichromosomal, signed, circular, or linear
 * (or NOT YET mixed) permutations of N = {1,2,...,n}.
 *
 * Optimal O(kn + |output|) time and O(kn) space implementation.
 *
 *****************************************************************************/

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include <stdio.h> //...

#include "args.h" //CHECKARGNUM()
#include "files.h" //FOPEN_READABLE(),FCLOSE()
#include "bool.h" //BOOL,TRUE

#include "genomes.h" //genomes,read_,are_,free_genomes()
#include "irreducible.h" //find_common_intervals()

/*****************************************************************************
 * GLOBAL FUNCTION MAIN
 *****************************************************************************/
int main(const int argc, char *argv[])
{
  FILE *fptr;
  char *filename;
  genomes *g;
 int g0_shift;
	
  //parse command line arguments
  CHECKARGNUM(1,"genomes-filename");
  filename = argv[1];

  //read genomes
  FOPEN_READABLE(fptr,filename,ON_FAILURE_EXIT);
  g = read_genomes(fptr);
  FCLOSE(fptr);
  if(g->num < 1) {
    fprintf(stderr,"Error: No genomes found!\n");
    exit(1);
  }
  if(!are_permutations(g)) {
    fprintf(stderr,"Error: These are not %d permutations of [1 .. %d]!\n",
            g->num,g->len[0]);
    exit(1);
  }
  //compute and print common intervals
  find_common_intervals(stdout,g, &g0_shift, 0);
  
  //cleanup
  free_genomes(g);

  return 0;

} // main()
/****** EOF (common_wabi.c) **************************************************/
