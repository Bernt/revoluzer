include_directories (
	${revoluzer_SOURCE_DIR}/common_wabi
	${revoluzer_SOURCE_DIR}/common_wabi/include
)

set(grappa_dist_src 
	invdist.c 
	med_util.c
	uf.c
)

set(grappa_capr_src 
	inversion_median_alberto.c
)

set_source_files_properties(inversion_median_alberto.c
	PROPERTIES
	LANGUAGE CXX )


add_library (grappa_dist ${grappa_dist_src})
add_library (grappa_capr ${grappa_capr_src})

set_target_properties(grappa_capr 
	PROPERTIES 
	LINKER_LANGUAGE CXX 
	LANGUAGE CXX)
