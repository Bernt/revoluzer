/**
 * 'remove' duplicate geneorders from a given file
 * author: Matthias Bernt
 */

#include <ostream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <stdlib.h>
#include <vector>

#include "genom.hpp"
#include "helpers.hpp"
#include "io.hpp"
#include "tree.hpp"

using namespace std;

/**
 * data to attach to phylogenetic tree nodes
 */
typedef struct nodedata {
	string name;
	genom g;
} nddata;

/**
 * print some usage information and exit the program with 0
 */
void usage();

/**
 * here the work is done :-)
 */
int main(int argc, char *argv[]) {
	bool report = false;
	stringstream out;
	ofstream file;
	string filename, // genome fasta file name
		ofilename,
		otfname,
		tfname;	// tree file name
	vector<genom> genomes;	// gene orders
	vector<string> names, // species names
		nmap,		// gene name map
		nwktrees;	// newick trees
	vector<unsigned> cnt; // multiplicity of removed genes
	vector<vector<string> > tax;
	char circular = 1;

	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'l': {circular=0; break;}
			case 'h': {usage(); return EXIT_SUCCESS; break;}
			case 'f': {i++;filename = argv[i];break;}
			case 'g': {i++;ofilename = argv[i];break;}
			case 'r': {report=true; break;}
			case 't': {i++;tfname = argv[i];break;}
			case 'u': {i++;otfname = argv[i];break;}
			default:{			// unknown parameter
				cout << "unknown option: "<<argv[i]<<endl;
				usage();
				exit(EXIT_FAILURE);
			}
		}
	}

		// check if a filename was specified
	if ( filename.size() == 0 ) {		// check if filename specified
		cerr << "error: no input file specified" << endl;
		usage();
		exit(EXIT_FAILURE);
	}

		// read the file
	read_genomes(filename, genomes, names, circular, nmap, true, true );
	read_taxonomy(filename, tax);


	if( tax.size() != 0 && tax.size() != names.size() ){
		cerr << "error: unequal number of tax and names "<<endl;
		exit( EXIT_FAILURE );
	}

	//	read_taxa(filename, genomes, names, circular);
	if(genomes.size() == 0){
		cerr << "error: no genomes found in the input file " << endl;
		usage(); exit(EXIT_FAILURE);
	}



	if ( tfname.size() > 0 ) {		// check if tree filename specified
		read_trees( tfname, nwktrees );
		if( nwktrees.size() == 0 ){
			cerr << "error: no trees found in the input file " << endl;
			usage(); exit(EXIT_FAILURE);
		}else if(nwktrees.size() > 1){
			cerr << "warning: more than one tree in the input file -> using first!" << endl;
		}
		map<string, vector<string> > eq;
		phy<genom> tree;
		vector<genom> leaves( genomes.size() );
		vector<node<genom> *> po;

		tree = phy<genom>(nwktrees[0], names, genomes, true);
		tree.depthorder_nodes( po, false );

		for(unsigned i=0; i<names.size(); i++){
			eq[names[i]] = vector<string>(1, names[i]);
		}

		for( unsigned i=0; i<po.size(); i++ ){
//			bool all  = true;
			vector<node<genom> *> ch;
			ch = po[i]->get_childs();

			unsigned j=0;
			while( j<ch.size() ){
				unsigned k = j+1;
				if( ch[j]->get_value().size() == 0 ){
					j++;
					continue;
				}

				while( k<ch.size() ){
					if( ch[k]->get_value().size() == 0 ){
						k++;
						continue;
					}

					if( ch[j]->get_value() != ch[k]->get_value() ){
						k++;
						continue;

					}
					// equal
					// 1 merge eq map entries
					eq[ ch[j]->get_name() ].insert( eq[ ch[j]->get_name() ].end(), eq[ ch[k]->get_name() ].begin(), eq[ ch[k]->get_name() ].end() ) ;
					eq.erase( ch[k]->get_name() );

					// 2 delete child j
					po[i]->remove_child( ch[k] );
					ch.erase( ch.begin()+k );
				}
				j++;
			}

			if ( ch.size() == 1 ){
				po[i]->set_value( ch[0]->get_value() );
				po[i]->set_name( ch[0]->get_name() );
				po[i]->prune();
			}


//			// check if all children have assigned data
//			for( unsigned j=0; j<ch.size(); j++ ){
////				cerr << "child "<<j<< " "<<ch[j]->get_name()<<endl;
//				if( ch[j]->get_value().size() == 0 ){
//					all = false; break;
//				}
//			}
//			if( !all ){
////				cerr << "# not all data ================="<<endl;
//				continue;
//			}
//
//			// check if all have the same gene order
//			all = true;
//			for( unsigned j=0; j<ch.size()-1; j++ ){
//				if( ch[j]->get_value() != ch[j+1]->get_value() ){
//					all = false; break;
//				}
//			}
//			if( !all ){
////				cerr << "# not all equal ================="<<endl;
//				continue;
//			}
//
//
//			po[i]->set_value( ch[0]->get_value() );
//			po[i]->set_name( ch[0]->get_name() );
//
////			cerr <<"eq"<<endl;
////			copy(eq[ ch[0]->get_name() ].begin(), eq[ ch[0]->get_name() ].end(), ostream_iterator<string>(cerr," "));cerr << endl;
//			for( unsigned j=1; j<ch.size(); j++ ){
//
//				vector<string> ins = eq[ ch[j]->get_name() ];
//				eq[ ch[0]->get_name() ].insert( eq[ ch[0]->get_name() ].end(), ins.begin(), ins.end() ) ;
//				eq.erase( ch[j]->get_name() );
//			}
////			cerr <<"EQ"<<endl;
////			copy(eq[ ch[0]->get_name() ].begin(), eq[ ch[0]->get_name() ].end(), ostream_iterator<string>(cerr," "));cerr << endl;
//
////			cerr <<"pruning subtree: "<<*(po[i]) <<endl;
//			po[i]->prune();
////			cerr <<"=>               "<<*(po[i]) <<endl;
////			cerr << "# ================="<<endl;
		}

		if( report ){
			for( map<string, vector<string> >::iterator it = eq.begin(); it!=eq.end(); it++ ){
				for( unsigned i=1; i<it->second.size(); i++ ){
					cerr << it->first << "==" <<it->second[i]<<endl;
				}
			}
		}

		// output genomes
		tree.preorder_nodes( po, true );
		for( unsigned i=0; i<po.size(); i++ ){
			if( po[i]->get_value().size() == 0 ){
				continue;
			}
			string nm = po[i]->get_name();
			//if( eq.find( po[i]->get_name() ) != eq.end() && eq[po[i]->get_name()].size()>1){
			//	nm+="*"t2string( eq[po[i]->get_name()].size()-1 );+in
			//}

			out << "> "<< nm <<endl;
			out << po[i]->get_value()<<endl;

			po[i]->set_name( nm );
		}
		if( ofilename != "" ){
			file.open( ofilename.c_str() );
			file << out.str()<<endl;
			file.close();
		}else{
			cout << out.str()<<endl;
		}
		out.str("");

		// output tree
		out << tree << endl;
		if( ofilename != "" ){
			file.open( otfname.c_str() );
			file << out.str()<<endl;
			file.close();
		}else{
			cout << out.str()<<endl;
		}
		out.str("");
	}
		// without tree
	else{

		unique( genomes, names, tax, cnt, report );

		for(unsigned i=0; i<genomes.size(); i++){
//			genomes[i].set_nmap(NULL);
//			out << "x "<< cnt[i]<<endl;
			if(tax.size()>0){
				out << "]"; copy(tax[i].begin(), tax[i].end(), ostream_iterator<string>(out," "));out<< endl;
			}
			out << ">"<<names[i]<<endl<< genomes[i]<<endl;
		}
		if( ofilename != "" ){
			file.open( ofilename.c_str() );
			file << out.str()<<endl;
			file.close();
		}else{
			cout << out.str()<<endl;
		}
		out.str("");
	}


	return EXIT_SUCCESS;
}


void usage() {
	cerr << "usage :"<<endl;
	cerr << "unique -f inputfile [OPTIONS]"<<endl;

	cerr << "-h: print this help "<<endl;
	cerr << "-l: linear gene orders"<<endl;
	cerr << "-f FNAME: input file name gene orders"<<endl;
	cerr << "-g FNAME: output file for gene orders (default: stdout)"<<endl;
	cerr << "-r: print info on removed gene orders to stderr"<<endl;
	cerr << "-t FNAME: input file name tree"<<endl;
	cerr << "-u FNAME: output file for tree (default: stdout)"<<endl;

}
