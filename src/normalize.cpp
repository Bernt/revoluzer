/*
 * normalize.cpp
 *
 * rotate circular gene orders to a specific gene
 * @author: Matthias Bernt
 */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

#include "genom.hpp"
#include "io.hpp"

using namespace std;

/**
 * print some usage information and exit the program with 0
 */
void usage();

/**
 * here the work is done :-)
 */
int main(int argc, char *argv[]) {
	int circular = 0,
		normto;
	string filename,
		rot;
	vector<genom> genomes;
	vector<vector<string> > tax;
	vector<string> names,
		nmap;

	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'f': {i++;filename = argv[i];break;}
			case 'r': {i++;rot = argv[i];break;}
			default:{			// unknown parameter
				cout << "unknown option: "<<argv[i]<<endl;
				usage();
				break;
			}
		}
	}

		// check if a file name was specified
	if ( filename == "") {
		cout << "Error: no input file specified" << endl;
		usage();
	}
		// check if a gene name was specified
	if ( rot == "") {
		cout << "Error: no gene name" << endl;
		usage();
	}

	// read the file
	read_genomes(filename, genomes, names, 1, nmap, true, true);
	read_taxonomy(filename, tax);
	if( tax.size() != 0 && tax.size() != names.size() ){
		cerr << "unequal number of tax and names "<<endl;
		exit( EXIT_FAILURE );
	}

	if(genomes.size() == 0){
		cerr << "error: no genomes found in the input file " << endl;
		usage();
	}

	vector<string>::iterator rotit = find(nmap.begin(), nmap.end(), rot);
	if(rotit == nmap.end()){
		cerr << "error: gene "<<rot<<" not found"<<endl;
		usage();
	}
	normto = distance(nmap.begin(), rotit);

	for(unsigned i=0; i<genomes.size(); i++){
		genomes[i].normalize(normto);
		if(tax.size()>0){
			cout << "]"; copy(tax[i].begin(), tax[i].end(), ostream_iterator<string>(cout," "));cout<< endl;
		}
		cout << ">"<<names[i]<<endl<< genomes[i]<<endl;
	}

	return 0;
}


void usage() {

	cout << "usage :"<<endl;
	cout << "normalize -f infile -r rotto"<<endl;
	exit(0);
}
