/** @file tmrloc.cpp
 * locate tmrl events 
 */
 
#include <iostream>

#include "genom.hpp"
#include "io.hpp"
#include "tmrl.hpp"

using namespace std;

/**
 * print usage information and abort 
 */
void usage();

/**
 * do the work
 */
int main(int argc, char *argv[]){
	int circular = 1,		// flag indicating if the genomes should be handled as circular genomes
		n = 0;				// the length of the genomes
	string fname;			// input filename
	vector<genom> genomes;	// the input genomes
	vector<pair<int,int> > rtdrli;
	vector<string> names,	// the names of the input genomes
		nmap;				// name mapping  
	vector<tmrl> rtdrls;
	
		// get and check parameters
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'f': {i++; fname = argv[i]; break;}
			case 'l': {circular = 0; break;}
			default:{
				cout << "unknown parameter "<<argv[i]<<endl;
				usage();
			}
		}
	}
	if(fname.size()==0){
		cout << "no filename given ! "<<endl;
		usage();
	}

	read_genomes(fname, genomes, names, circular, nmap);
	if(genomes.size() == 0){
		cout << "no genomes found in "<<fname<<" !"<<endl;
		usage();
	}
	n = genomes[0].size();
	
		// start the analysis for all pairs
	for(unsigned i=0; i<genomes.size(); i++){
		for(unsigned j=0; j<genomes.size(); j++){
			if(i==j){
				continue;
			}
			
			tmrloc_itree( genomes[i], genomes[j], n, circular, rtdrli, rtdrls );
			if( rtdrls.size() > 0 ){
				for (unsigned k=0; k<rtdrls.size(); k++){
					cout << rtdrls[k].maxblkcnt <<" "<< abs(rtdrli[k].second - rtdrli[k].first) << " [" << rtdrli[k].first<<","<< rtdrli[k].second <<"] : "<<names[i]<<" - "<<names[j]<<endl;
				}
			}
			
		}
	}
}

void usage(){
	cout << "tmrloc -f file [-l]"<<endl;
	exit(1);
}
