/*
 * gdiff.cpp
 * compare two gene order files
 * @author M. Bernt
 */

#include <algorithm>
#include <iostream>
#include <limits>
#include <iterator>
//#include <vector>

#include "genom.hpp"
#include "io.hpp"

using namespace std;

/**
 * print some usage information and exit the program with 0
 */
void usage();

/**
 * here the work is done :-)
 */
int main(int argc, char *argv[]) {
	int circular = 1;
//		normto;
//	string rot;
	vector<string> fname;
	vector<vector<genom> > genomes;
	vector<vector<string> > names,
		nmap;

	for (int i = 1; i < argc; i++) {
		if( argv[i][0] == '-' ){
			switch (argv[i][1]) {
				case 'l': {circular = 0; break;}
				default:{			// unknown parameter
					cout << "unknown option: "<<argv[i]<<endl;
					usage();
					break;
				}
			}
		}else{
			fname.push_back( argv[i] );
		}
	}

		// check if two file names were specified
	if ( fname.size() != 2) {
		cerr << "error: two input files necessary" << endl;
		usage();
	}

	genomes = vector<vector<genom> >(2);
	names = vector<vector<string> >(2);
	nmap = vector<vector<string> >(2);

	// read the files
	for( unsigned i=0; i<fname.size(); i++ ){
		read_genomes(fname[i], genomes[i], names[i], 1, nmap[i], true, true);
		if(genomes.size() == 0){
			cerr << "error: no genomes found in the input file " << endl;
			usage();
		}
	}

	// construct combined name map
	map<string, int> nnmapi;
	vector<string> nnmap(1,"");
	vector<int> rename;
	for( unsigned i=0; i<nmap.size(); i++ ){
//		cout << "nmap "<<fname[i]<<endl;

		for( unsigned j=1; j<nmap[i].size(); j++ ){
//			cout <<"\t"<< j <<" : "<<nmap[i][j]<<endl;
			if( nnmapi.find( nmap[i][j] ) == nnmapi.end() ){
				nnmapi[ nmap[i][j] ] = nnmap.size();
				nnmap.push_back(nmap[i][j]);
			}

			if(i==1){
				while( rename.size() < j+1 )
					rename.push_back( std::numeric_limits< int >::max() );
				rename[ j ] = nnmapi[ nmap[i][j] ];
//				cout << "\trename "<<j<<" -> "<<rename[ j ]<<endl;
			}
		}
	}

//	cout << "rename "; copy( rename.begin(), rename.end(), ostream_iterator<int>(cout, " ") ); cout << endl;

	for( unsigned i=0; i<genomes[1].size(); i++ ){
		for( unsigned j=0; j<genomes[1][i].chromosom.size(); j++ ){
//			cout << "renaming "<<genomes[1][i].chromosom[j];
			genomes[1][i].chromosom[j] = (genomes[1][i].chromosom[j]<0)?-1*rename[ abs(genomes[1][i].chromosom[j]) ]:rename[ abs(genomes[1][i].chromosom[j]) ] ;
//			cout << " to "<<genomes[1][i].chromosom[j]<<endl;
		}
		genomes[1][i].set_nmap( &nnmap );
	}
	for( unsigned i=0; i<genomes[0].size(); i++ ){
		genomes[0][i].set_nmap( &nnmap );
	}

	map<string, vector<int> > namesi;
	for( unsigned i=0; i<names.size(); i++ ){
		for(unsigned j=0; j<names[i].size(); j++){
			if( namesi.find(names[i][j]) == namesi.end()){
				namesi[names[i][j]] = vector<int>(2, std::numeric_limits< int >::max());
			}
			namesi[names[i][j]][ i ] = j;
		}
	}

	for( map< string, vector<int> >::const_iterator it=namesi.begin(); it!= namesi.end(); it++ ){
		bool compare = true;
		for( unsigned i=0; i<it->second.size(); i++ ){
			if( it->second[i] == std::numeric_limits< int >::max() ){
				cout << it->first << " not present in "<<fname[i]<<endl;
				compare = false;
			}
		}
		if( !compare )
			continue;

		if( circular != 0 ){
			int normto;
			vector<string>::iterator rotit = find(nnmap.begin(), nnmap.end(), "COX1");
			if(rotit == nnmap.end()){
				cerr << "error: gene COX1 not found"<<endl;
				usage();
			}
			normto = distance(nnmap.begin(), rotit);
			genomes[0][ it->second[0] ].normalize(normto);
			genomes[1][ it->second[1] ].normalize(normto);
		}
		if( genomes[0][it->second[0]] != genomes[1][it->second[1]] ){
			cout << ">"<<it->first<<endl;
			cout << genomes[0][it->second[0]]<<endl;
			cout << genomes[1][it->second[1]]<<endl;

			if (genomes[0][it->second[0]].size() == genomes[1][it->second[1]].size() ){
				for( unsigned i=0; i<genomes[0][it->second[0]].size(); i++ ){
					if( genomes[0][it->second[0]][i] != genomes[1][it->second[1]][i] ){
						cout << it->first<<" ";
						print_element( genomes[0][it->second[0]][i], cout, 1, "", &nnmap );
						cout << " != ";
						print_element( genomes[1][it->second[1]][i], cout, 1, "", &nnmap );
						if( genomes[0][it->second[0]][i] == -1 * genomes[1][it->second[1]][i] ){
							cout << " strand switch"<<endl;
						}

						cout << endl;
					}
				}
			}
		}else{
			cout <<it->first<< " equal"<<endl;
		}


	}
	return 0;
}


void usage() {

	cerr << "usage :"<<endl;
	cerr << "gdiff file1 file2 [-l]"<<endl;
	cerr << "   -l: genomes are linear"<<endl;

	exit(0);
}
