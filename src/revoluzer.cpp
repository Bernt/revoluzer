#include <algorithm>
#include <iostream>
#include <limits>
#include <map>
#include <vector>

#include "conserved.hpp"
#include "dstnc_inv.hpp"
#include "helpers.hpp"
#include "io.hpp"

#include <unistd.h>
#include <time.h>

using namespace std;

void usage();

/**
 * get the 'best' reversals from the genome with index idx to the others
 *@param used the genomes used so far 
 *@param G_orig the original vector of genomes (their origins)
 *@param G the vector of genomes
 *@param idx the index (in the vec) of the genome to work on
 *@param doit do the reversal or just try
 *@param cup if true -> take preserving reversals to at least one genome; else take pr to most other genomes
 *@param use_rev_suc use reversals on different cycles of the same unoriented component (additionaly)
 *@param rdist reversal distance foo
 */
int getBestReversals(map<genom, unsigned> &used, 
	vector<genom> &G_orig, 
	vector<genom> &G, 
	unsigned idx, 
	bool doit, 
	bool cup,
	bool use_rev_suc,
	dstnc_inv *rdist ){

	int distanceSum = 0,
		origDistance = 0,
		maxScore = 0,
		maxDelta = 0;				// distance reduction
	map< pair<int, int>, int > score;
	vector< pair<int, int> > bestReversals;
	vector< pair<int, int> > tempReversals;

	//~ cout << "START getBestReversals"<<endl;

		// get the preserving reversals (on the same cycle)
	for(unsigned j=0; j<G.size(); j++){
		if(idx!=j){
			vector< pair<int, int> > pr;
			if(use_rev_suc){
				pr = G[idx].getReversalsSameUnorientedComponentAndSameCycle(G[j]);
			}else{
				pr = G[idx].getReversalsSameCycle(G[j]);
			}

			for(unsigned k=0; k<pr.size(); k++){
					score[pr[k]]++;
			}
		}
	}

		// get the best (preserving to the most other permutations)
	for(map< pair<int, int>, int >::iterator it=score.begin(); it!=score.end(); it++){
		if(cup)
			bestReversals.push_back(it->first);
		else{
			if(it->second == maxScore){
				bestReversals.push_back(it->first);
			}
			if(it->second > maxScore){
				maxScore =  it->second;
				bestReversals.clear();
				bestReversals.push_back(it->first);
			}
		}
	}
	if(doit && !cup)
		cout << "maxscore "<<maxScore<<endl;
		// get the reversals which reduce the distance most
	
	for(unsigned j=0; j<G.size(); j++){
		if(idx!=j){
			distanceSum += rdist->calc(G[idx],G_orig[j]);
		}
	}
	for(unsigned j=0; j<bestReversals.size(); j++){
		int delta = distanceSum;
		genom temp=G[idx];
		reverse(temp, bestReversals[j]);
		for(unsigned k=0; k<G.size(); k++){
			if(idx!=k){
				delta -= rdist->calc(temp,G_orig[k]);
			}
		}
		if(delta == maxDelta)
			tempReversals.push_back(bestReversals[j]);
		if(delta > maxDelta){
			maxDelta =  delta;
			tempReversals.clear();
			tempReversals.push_back(bestReversals[j]);
		}
	}
	if(tempReversals.size() || !doit)
		bestReversals = tempReversals;
	tempReversals.clear();

		// filter the reversals which not lead farther away
	origDistance = rdist->calc(G[idx],G_orig[idx]);
	for(unsigned j=0; j<bestReversals.size(); j++){
		genom temp=G[idx];
		reverse(temp, bestReversals[j]);
		if((rdist->calc(temp,G_orig[idx]) - origDistance)>=0){
			tempReversals.push_back(bestReversals[j]);
		}
	}
	if(tempReversals.size() || !doit)
		bestReversals = tempReversals;
	tempReversals.clear();
	
	if(doit && bestReversals.size()>1){ // get the reversals which result in the most preserving reversals
		int maxNext=0;
		for(unsigned j=0; j<bestReversals.size(); j++){
			int curNext=0;
			vector<genom> trial = G;
			reverse(trial[idx], bestReversals[j]);
			curNext = getBestReversals(used ,G_orig, trial, idx, false, cup, use_rev_suc, rdist);

			if(curNext == maxNext)
				tempReversals.push_back(bestReversals[j]);
			if(curNext > maxNext){
				maxNext = curNext;
				tempReversals.clear();
				tempReversals.push_back(bestReversals[j]);
			}
		}
		if(tempReversals.size() || !doit)
			bestReversals = tempReversals;
		tempReversals.clear();
		//~ cout << "resulting "<<bestReversals.size() << endl;
	}

	int done = 0;
	for(unsigned j=0; j<bestReversals.size(); j++){
		genom temp = G[idx];					// temporary genome to 'try around'
		reverse(temp, bestReversals[j]);
		
		//map<genom, bool>::iterator lused_it;
		map<genom, unsigned>::iterator used_it;

		/*lused_it = lused.find(temp);
		if(lused_it != lused.end()){
			continue;
		}else{
			lused[temp] = true;
		}*/
		
		used_it = used.find(temp);
		if(used_it != used.end()){						// this genome got visited already
			if(used_it->second == idx+1){		// by the same genome ?? -> then this reversal is crap
				continue;
			}else{								// or an other -> then the reversal is just perfect because it merges 2
				if(doit){
					G.clear();
					G.push_back(temp);
					done = 1;
					break;
				}else{
					tempReversals.push_back(bestReversals[j]);
					//~ if(!maxDelta) maxDelta = 1;
					//~ maxDelta *= 1000;
				}
			}
		}else{									// this is a genome what wasn't visited -> this would be ok
			if(doit){
				G[idx] = temp;
				used[temp] = idx+1;
				done = 1;
				break;
			}else{
				tempReversals.push_back(bestReversals[j]);
			}
		} 
	}
	//~ cout << "END getBestReversals"<<endl;
/* 	lused.clear(); */
	if(doit){
		if(done){
			return 1;
		}else{
			return -1;
		}
	}
	else{
		return(tempReversals.size()*maxDelta);
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
	vector<vector<genom> > traces;
	vector<genom> temp;
	map<genom, unsigned> used;
	genom id;
	string filename;
	vector<genom> G,		// the genomes
		G_orig;				// a copy of the genomes in their initial state
	vector<string> names;
	unsigned m = 0,		// count of genomes 
		n = 0,		// length of genomes
		d = 0, 		// distance from idendity
		step = 0,	// stepcounter
		steplimit = 0,
		r_start = 0,// length of the reversals which generated the set
		r_end = 0;
	bool cup = true,		// union mode ??
		use_rev_suc=false,	// use reversals on diff. cycles of the same unoriented component ??
		realdata = false;
	char circ = 0;
	dstnc_inv *rdist = NULL;
	
	init_rng();

	struct timespec ts,tt;
	clock_gettime(CLOCK_REALTIME, &ts);
	
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'c': {use_rev_suc = true; break;}
			case 'C': {circ = 1; break;}
			case 'f': {i++; filename = argv[i]; break; }
			case 'i': {cup = false;break;}
			case 'r': {realdata = true; break;}
		}
	}
	if (!filename.size()) {		// check if filename specified
		cout << "no input file specified" << endl;
		usage();
		exit(0);
	}else{
		read_taxa(filename.c_str(), G, names, circ);	// read the genom file

		if(realdata){
			// d = (G[0].distance(G[1])+G[0].distance(G[2])+G[1].distance(G[2]))/2;
			n = G[0].size();
			m = G.size();
		}else{
			//~ parseFilename(filename.c_str(), m, n, d);
			parseFilename(filename.c_str(), m, n, d, r_start, r_end);
			//~ cout << "m "<<m <<" n "<<n << " k "<<d<< " r_start "<<r_start<< " r_end " <<r_end<<endl;
		}
		rdist = new dstnc_inv(n ,circ);
		steplimit = 2 * (rdist->calc(G[0],G[1])+rdist->calc(G[0],G[2])+rdist->calc(G[1],G[2]));
		//~ cout << d << endl;
		//~ m = G.size();
		//~ n = G[0].size();
		//~ cout << r_end << endl;
	}
	
	id = genom(n, circ);
	G_orig = G;									// copy the genome

	for(unsigned i=0; i<G.size(); i++)	// set all initial genomes as used
		used[G[i]] = i+1;

	//~ traces.push(G);
	traces.resize(G.size());
	for(unsigned i=0; i<G.size(); i++)
		traces[i].push_back(G[i]);
	
	//~ reversals = G[0].getReversals();
	while(G.size()>2){
		int emptyTraces = 0;
		step ++;			// increase step counter
		for (unsigned i=0; i<G.size() && G.size()>2 ; i++){
			int done;
			done = getBestReversals(used, G_orig, G, i, true, cup, use_rev_suc, rdist);
			if(G.size()>1){	// if the function leafes more than one Genome 
				if(done>0){		// and somthing was done 
					traces[i].push_back(G[i]);	// store the new genome on the trace
				}else{			// if nothing was done
					if(traces[i].size() > 0){		// and the trace is not empty
						G[i] = traces[i].back();		// set the genome to the top of the trace
						traces[i].pop_back();				// and pop the stack top
					}
				}
			}
		}

		// used.clear();
		for(unsigned i=0; i<traces.size() && G.size()>2; i++){	// check every trace
			// for (unsigned j=0; j<traces[i].size(); j++)
			// 	used[traces[i][j]] = i+1;
			if(!traces[i].size()){						// if its empty
				emptyTraces++;							// and count empty traces
			}
		}

		if(G.size()>2 && (emptyTraces == 3 || step > steplimit)){
			if(emptyTraces == 3)
				cout << "trace empty " <<filename<< " ";
			if(step > steplimit)
					cout << "step limit "<<filename<< " ";
			if(!cup){
				cout << "cup => true"<<endl;
				cup = true;
				traces.clear();
				used.clear();
				traces.resize(G.size());
				G = G_orig;
				for(unsigned i=0; i<G.size(); i++){
					traces[i].push_back(G[i]);
					used[G[i]] = i+1;
				}
				step = 0;
			}
			else{
				cout << endl;
				genom min_ug;
				unsigned minscore=std::numeric_limits< unsigned >::max();
				for(map<genom, unsigned>::iterator it=used.begin(); it!=used.end(); it++){
					genom ug = it->first;
					unsigned sc = rdist->calcsum(ug,G_orig);
					if(sc<minscore){
						minscore = sc;
						min_ug = ug;
					}
				}
				cout << "used "<<endl;
				if(realdata){	
					cout << min_ug << endl;
					cout << m <<" "<<n<<" "<<rdist->calcsum(min_ug,G_orig) <<" "<<rdist->calcsum(min_ug,G_orig) - (int)ceil((double)(rdist->calc(G_orig[0],G_orig[1]) + rdist->calc(G_orig[0],G_orig[2]) + rdist->calc(G_orig[1],G_orig[2]) )/2.0)<<" "
						<<" "<<intervalDistance(min_ug, G_orig)<<endl;
				}else{
					cout << m <<" "<<n<<" "<< d <<" ";
					if(r_end != 0){
						cout << r_start <<" "<<r_end<<" ";
					}
					cout <<rdist->calc(id,min_ug) <<" "<< rdist->calcsum(min_ug,G_orig) - rdist->calcsum(id,G_orig)<<" "
						<<intervalDistance(id, min_ug) <<" "<<intervalDistance(min_ug, G_orig) <<" "
						<< rdist->calcsum(min_ug,G_orig) - (int)ceil((double)(rdist->calc(G_orig[0],G_orig[1]) + rdist->calc(G_orig[0],G_orig[2]) + rdist->calc(G_orig[1], G_orig[2]) )/2.0) <<endl;
				}
				
				clock_gettime(CLOCK_REALTIME, &tt);
				printf("TIME %lf sec\n", (tt.tv_sec-ts.tv_sec) + (tt.tv_nsec-ts.tv_nsec)/1000000000.0  );
				exit(1);
			}
		}
		//~ if(worked > 0)
			//~ traces.push(G);
		//~ else{
			//~ step--;
			//~ if(traces.size()<1){
				//~ cout << "trace empty " <<filename<<endl;
				//~ exit(1);
			//~ }else{
				//~ G = traces.top();
				//~ traces.pop();
			//~ }
		//~ }
		//~ cout << worked<<"-"<< traces.size()<<endl;
	}
		// ckeck if in one of the traces is a better solution 
	int oldDistance = rdist->calcsum(G[0],G_orig);			// get the current solution quality (distance sum)
	for(unsigned z=0; z<traces.size(); z++){			// for every trace
		genom t;
		while (traces[z].size()){						// empty the trace
			t = traces[z].back();						// get the top
			if( rdist->calcsum(t,G_orig) < oldDistance){		// if its a better solution
				G[0] = t;								// update the solution
				oldDistance = rdist->calcsum(G[0],G_orig);
			}
			traces[z].pop_back();						// pop the top
		}
	}
		// output solution 

	if(realdata){
		cout << G[0]<<endl;
		cout << m <<" "<<n<<" "<<rdist->calcsum(G[0],G_orig) <<" "<<rdist->calcsum(G[0],G_orig) - (int)ceil((double)(rdist->calc(G_orig[0],G_orig[1]) + rdist->calc(G_orig[0],G_orig[2]) + rdist->calc(G_orig[1],G_orig[2]) )/2.0)<<" "
			<<intervalDistance(G[0], G_orig)<<endl;
	}else{
		cout << m <<" "<<n<<" "<< d <<" ";
		if(r_end != 0){
			cout << r_start <<" "<<r_end<<" ";
		}
		cout <<rdist->calc(id,G[0]) <<" "<< rdist->calcsum(G[0],G_orig) - rdist->calcsum(id,G_orig)<<" "
			<<intervalDistance(id, G[0]) <<" "<<intervalDistance(G[0], G_orig) <<" "
			<< rdist->calcsum(G[0],G_orig) - (int)ceil((double)(rdist->calc(G_orig[0],G_orig[1]) + rdist->calc(G_orig[0],G_orig[2]) + rdist->calc(G_orig[1],G_orig[2]) )/2.0) <<endl;
	}
	delete rdist;
	clock_gettime(CLOCK_REALTIME, &tt);
	printf("TIME %lf sec\n", (tt.tv_sec-ts.tv_sec) + (tt.tv_nsec-ts.tv_nsec)/1000000000.0  );
	return 1;
}

void usage() {
	cout << "revoluzer_sim -f inputfile [-i] [-c]"<<endl;
	cout << "-i use intersection mode (default union)"<<endl;
	cout << "-c use reversals on the same unor. component (default false)"<<endl;
	cout << "-C circular genomes"<<endl;
}

