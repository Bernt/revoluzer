/*
 * mediansolver.cpp
 *
 *  Created on: Jul 14, 2008
 *      Author: maze
 */

#include <getopt.h>
#include <iostream>
#include <string>
#include <vector>

#include "dstnc_bp.hpp"
#include "dstnc_com.hpp"
#include "genom.hpp"
#include "io.hpp"
#include "caprara.hpp"
#include "rtmedian.hpp"

using namespace std;

/**
 * print usage information and exit
 */
void usage();

/**
 * @param wr reversal weight for rtmedian
 * @param wt transposition weight for rtmedian
 */
void getoptions( int argc, char *argv[], string &fname, string &solverstr,
		string &distancestr, int &allmed,
		int &circular, int &pairpd, bool &alt, unsigned &maxcon, unsigned &maxscen, bool &bps,
		int &verbose, int &wr, int &wt, bool &bound );

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int main(int argc, char *argv[]){
	bool bps = true,
		alt = true,
		bounds = false;
	dstnc *dst = NULL;	// a distance function for the generic enumerative median solver
	int allmed = 1,
		circular = 1,
		verbose = 0,
		m = -1,
		n = -1,
		mediancnt = 0,
		score = 0,
		pairpd = 0,			// 0: pairwise or 1: global perfect reversal distance
		wr = 1,		// weight of reversals (for rtmedian)
		wt = 1;		// weight of transpositions (for rtmedian)
	mediansolver *solver = NULL;
	string fname,
		solverstr,
		distancestr;
	unsigned maxcon,
		maxscen;
	vector<genom> genomes,
		medians;
	vector<string> names,
		nmap;

		// get options
	getoptions(argc, argv, fname, solverstr, distancestr, allmed, circular, pairpd, alt,
			maxcon, maxscen, bps, verbose, wr, wt, bounds);

		// read genomes
	read_genomes( fname,  genomes,  names, circular, nmap, false, false);
	m = genomes.size();
	if( m < 3 ){
		cerr << "error: need at least three gene orders, "<<m <<" found"<<endl;
		usage();
	}
	n = genomes[0].size();
	if( n < 3 ){
		cerr << "error: genomes have only "<<n<<" genes"<<endl;
		usage();
	}


	if( solverstr == "rmp" ){
		solver = new rmp( n, circular );
	}else if( solverstr == "rtmedian" ){
		solver = new rtmedian( n, argc, argv );
	}else if( solverstr == "tcip" || solverstr == "comrmp" ){
		solver = new rmp_common( n, circular, TCIP, 0 );
	}else if( solverstr == "enum" ){
		if(distancestr == "inv"){
			dst = new dstnc_inv(n, circular);
		}else if( distancestr == "pinv" ){
			dst = new dstnc_pinv(n);
		}else if(distancestr == "bp"){
			dst = new dstnc_bp(n, circular, !circular);
		}else if(distancestr == "com"){
			dst = new dstnc_com(n, circular);
		}else{
			cout << "error: unknown distance function "<< distancestr << endl;
			usage();
		}
		solver = new median_hcenum( dst, n, circular );
//	}else if( solverstr == "ecip" ){
//		solver = new rmp_common( ECIP, pairpd, &hd );
//	}else if( solverstr == "cip" ){
//		solver = new rmp_common( CIP, pairpd, &hd );
//	}else if (solverstr == "trex"){
//		solver = new trexsolve( maxcon, bps, maxscen, alt );
//	}else if (solverstr == "ci"){
//		solver = new cisolve();
//		cerr << "ci not implemented yet"<< endl;
//		exit(1);
//		//genomes = solver->unpack(genomes);
//		//		make genomes unsigned, maybe pass by reference to init
	}else{
		cout << "error: unknown solver "<< solverstr << endl;
		usage();
	}

	if( bounds ){
		cout << "# bounds "<<solver->lower_bound(genomes)<<" "<<solver->upper_bound(genomes)<<endl;
	}else{
		solver->solve(genomes, allmed, 0, INT_MAX, mediancnt, score, medians);

		if (solverstr == "ci"){
	//		make genomes signed again, maybe make a function in cisolve
		}
		cout << "# "<<mediancnt<<" medians of score "<<score<<endl;
		for( unsigned i=0; i<medians.size(); i++ ){
			cout << "> median "<<i<<endl;
			cout << medians[i]<<endl;
		}
	}



	if( dst != NULL ){
		delete dst;
	}
	delete solver;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void getoptions( int argc, char *argv[], string &fname, string &solverstr,
		string &distancestr, int &allmed, \
		int &circular, int &pairpd, bool &alt, unsigned &maxcon, unsigned &maxscen, \
		bool &bps, int &verbose, int &wr, int &wt, bool &bounds){

	int c,
		bp=(int)bps,
		al=(int)alt,
		bn=(int)bounds;

	while (1) {
		static struct option long_options[] = {
			{"noalt",   no_argument,       &al, 0},
			{"bounds",  no_argument, 	   &bn, 1},
			{"linear",  no_argument,       &circular, 0},
			{"distance",    required_argument,       0, 'd'},
            {"help",    no_argument,       0, 'h'},
            {"file",    required_argument, 0, 'f'},
            {"onemed",  no_argument,       &allmed, 0},
            {"maxcon",  required_argument, 0, 'm'},
            {"maxscen", required_argument, 0, 'n'},
            {"nobps",   no_argument,       &bp, 0},
            {"pairwise",	no_argument,	&pairpd, 1},
            {"solver",  optional_argument, 0, 's'},
            {"verbose", optional_argument, 0, 'v'},
            {"wr",    required_argument, 0, 'R'},
            {"wt",    required_argument, 0, 'T'},
            {0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:hor:s:v:",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'd':
				distancestr = optarg;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case 'm':
				maxcon = atoi(optarg);
				break;
			case 'n':
				maxscen = atoi(optarg);
				break;
			case 'o':
				allmed = 0;
				break;
			case 's':
				solverstr = optarg;
				break;
			case 'v':
				verbose += atoi(optarg);
				break;
			case 'R':
				wr += atoi(optarg);
				break;
			case 'T':
				wt += atoi(optarg);
				break;
			case '?':
				cout << "?"<<endl;
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	printf ("non-option ARGV-elements: ");
    	while (optind < argc)
    		printf ("%s ", argv[optind++]);
    	putchar ('\n');
	}

    bps = (bp==1)?true:false;
    alt = (al==1)?true:false;
    bounds = (bn==1)?true:false;
    if( fname == "" ){
    	cerr << "error: a filename must be specified"<<endl;
    	usage();
    }
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	mediansolver *solver;

	cout << "usage: mednw -f [OPTIONS]"<<endl;
	cout << " -f --file    input file name"<<endl;
	cout << "    --linear  treat genomes as linear"<<endl;
	cout << " -v --verbose control verbosity"<<endl;
	cout << "              +1: status/progress to stderr "<<endl;
	cout << "    --bounds  print median problem bounds"<<endl;
	cout << " -h --help    print this help message"<<endl;
	cout << "median solver options "<<endl;
	cout << " -s --solver  select median solver"<<endl;
	cout << "              rmp/trex/tcip/rtmedian/enum"<<endl;
	cout << " -o --onemed  get only one median"<<endl;
//	cout << "trex median solver options"<<endl;
//	cout << "    --noalt   dont generate alternative scenarios"<< endl;
//	cout << "    --maxscen maximal number of alt. for rev+tdl scenarios"<<endl;
//	cout << "    --nobps   dont construct alternative break point scenario"<<endl;
//	cout << " -m --maxcon  maximal allowed consistency"<<endl;
	cout << "cip and ecip option"<<endl;
	cout << "    --pairwise do perfect reversal distance computation wrt. pairwise common intervals"<<endl;
	cout << "generic enumerative median solver"<<endl;
	cout << "  --distance  use the specified distance function (inv|pinv|bp|com)"<<endl;


	solver = new rtmedian( 0, 0, NULL );
	solver->printopt();
	delete solver;
	exit(0);
}

