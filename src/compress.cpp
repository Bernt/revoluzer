/*
 * compress.cpp
 *
 * removes stretches of genes that are common to all input gene orders
 *
 *  Created on: Jan 27, 2012
 *      Author: maze
 */
#include <algorithm>
#include <climits>
#include <cstdlib>
#include <getopt.h>
#include <iostream>

#include "io.hpp"

using namespace std;

/**
 * read command line options
 * @param[in] argc number of arguments
 * @param[in] argv the arguments
 * @param[out] fname input genome filename
 * @param[in,out] circular the circularity of the genomes
 * @param[in,out] perm return geneorders consisting of consecutive integers
 */
void getoptions( int argc, char *argv[], string &fname, bool &circular,
		bool &perm);

/**
 * print usage information and exit
 */
void usage();

int main(int argc, char *argv[]) {
	bool circular = true,
			perm = false;
	string fname;
	unsigned n=0,nn=0; // max element
	set<int> del;
	vector<genom> genomes;
	map<int,vector<int> > delog;
	vector<string> names,
		nmap, nnmap;
	vector<vector<int> > inv;
	vector<vector<int> > log;

	// get and check parameters
	getoptions( argc, argv, fname, circular, perm);


	// read the genomes and the trees
	read_genomes(fname, genomes, names, circular, nmap, true, true);


	// determine maximum element in the permutations -> n
	for( unsigned i=0; i<genomes.size(); i++ ){
//		cout << genomes[i]<<endl;
		for( unsigned j=0; j<genomes[i].size(); j++ ){
			n = max( (unsigned)abs(genomes[i][j]), n );
		}
	}

	// get "inverse permutations"
	for( unsigned i=0; i<genomes.size(); i++ ){
		inv.push_back( vector<int>(n+1, INT_MAX) );
		for( unsigned j=0; j<genomes[i].size(); j++ ){
			inv[i][ abs(genomes[i][j]) ] = j;
		}
	}

	// TODO CHECK circular for circular genomes
	// check if elements neighboured in genomes[0] (e,ee) are
	// also neighboured in the other genomes
	// if not mark ee for deletion
	for( unsigned j=0; j<genomes[0].size()-1; j++ ){
		unsigned e = abs(genomes[0][j]),
				ee = abs(genomes[0][j+1]);
		bool nb = true;
		for( unsigned i=0; nb==true && i<genomes.size(); i++ ){
			if( inv[i][ee] - inv[i][e] == 1 && genomes[0][ inv[0][e] ] == genomes[i][inv[i][e]] && genomes[0][ inv[0][ee] ] == genomes[i][inv[i][ee]] ){

			}else if(inv[i][ee] - inv[i][e] == -1 && genomes[0][ inv[0][e] ] == -1*genomes[i][inv[i][e]] && genomes[0][ inv[0][ee] ] == -1*genomes[i][inv[i][ee]]){

			}else{
				nb = false;
//				cout << "NOPE"<<endl;
			}
		}

		if( nb == true ){
			del.insert( ee );
		}
	}

	// delete the marked elements
//	cerr << "DEL "; copy(del.begin(), del.end(), ostream_iterator<int>(cerr, " ") );cerr << endl;
	unsigned l = abs(genomes[0][0]);
	delog[l] = vector<int>();
	for( unsigned i=0; i<genomes.size(); i++ ){
		vector<int> chr = genomes[i].getChromosom();
		for( unsigned j=0; j<chr.size();  ){
			if( del.find(abs(chr[j]))==del.end() ){
				if(i==0){
					l = abs(chr[j]);
					delog[l] = vector<int>();
					delog[l].push_back(chr[j]);
//					cerr <<endl<< chr[j]<<" ";
				}
				j++;
			}else{
				if(i==0){
					delog[l].push_back(chr[j]);
//					cerr <<chr[j]<<" ";
				}
				chr.erase( chr.begin()+j );
			}

		}
		genomes[i].setChromosom(chr);

		nn = max(nn, genomes[i].size());
//		if(i==0)cerr << endl;
//		cerr << genomes[i]<<endl;
	}

	// rename to permutations
	map<int,int> rename;
	unsigned idx = 0;
	for( unsigned i=0; i<genomes.size(); i++ ){
		for( unsigned j=0; j<genomes[i].size(); j++ ){
			if( rename.find(abs(genomes[i][j])) == rename.end() ){
				idx++;
				rename[abs(genomes[i][j])] = idx;
				if( genomes[i][j] < 0 ){
					rename[abs(genomes[i][j])] *= -1;
				}
			}

			if( (genomes[i][j] < 0 && rename[abs(genomes[i][j])] < 0) || (genomes[i][j] > 0 && rename[abs(genomes[i][j])] > 0)  ){
				genomes[i][j] = abs(rename[abs(genomes[i][j])]);
			}else{
				genomes[i][j] = -1*abs(rename[abs(genomes[i][j])]);
			}
		}
	}

	// rename name map
	nnmap = vector<string>(nn+1,"");
	for( map<int,int>::iterator it=rename.begin(); it != rename.end(); it++ ){
//		cerr << it->second<<" - "<<it->first<<endl;
		nnmap[ abs(it->second) ] = nmap[abs(it->first)];
	}

//	copy(nmap.begin(), nmap.end(), ostream_iterator<string>(cerr," "));cerr<<endl;
//	copy(nnmap.begin(), nnmap.end(), ostream_iterator<string>(cerr," "));cerr<<endl;

	// output results
	for(unsigned i=0; i<genomes.size(); i++){
		if( perm ){
			genomes[i].set_nmap( NULL );
		}else{
			genomes[i].set_nmap( &nnmap );
		}
		cout << ">"<<names[i]<<endl;
		cout << genomes[i]<<endl;
	}

	// output rename log
	cerr << "deletion log"<<endl;
	for(map<int, vector<int> >::iterator it=delog.begin(); it!=delog.end(); it++){
		if(perm){
			print_element( abs(rename[it->first]), cerr, 1, "", NULL );
		}else{
			print_element( abs(rename[it->first]), cerr, 1, "", &nnmap );
		}

		cerr <<" & ";
		for( unsigned i=0; i<it->second.size(); i++ ){
			print_element( it->second[i], cerr, 1, "", &nmap );
			cerr<< " ";
		}
		cerr <<"\\\\"<<endl;
//		cerr << abs(rename[it->first])<<" -> "; copy( it->second.begin(), it->second.end(), ostream_iterator<int>(cerr," ") ); cerr<<endl;
	}

}

void getoptions( int argc, char *argv[], string &fname, bool &circular,
		bool &perm){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"file",    required_argument, 0, 'f'},
			{"perm",    no_argument, 0, 'p'},
            {0, 0, 0, 0}
		};
        int option_index = 0;	// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:l",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case 'l':
				circular = false;;
				break;
			case 'p':
				perm = true;
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
				break;
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

		// check parameters
	if(fname == ""){
		cerr << "no genome file given ! "<<endl;
		usage();
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cerr << "compress -f FILE [OPTIONS]"<<endl;
	cerr << "-l: assume linear genomes"<<endl;
	exit( EXIT_FAILURE );
}
