/**
 * program to ...
 * @author: M. Bernt
 */

#include <algorithm>
#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <math.h>
#include <set>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/betweenness_centrality.hpp>
#include <boost/graph/exterior_property.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/iteration_macros.hpp>
//#include <boost/graph/johnson_all_pairs_shortest.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/strong_components.hpp>

#include <boost/property_map/property_map.hpp>

#include "crex.hpp"
#include "counter.hpp"
#include "genom.hpp"
#include "helpers.hpp"
#include "io.hpp"

#define DEFAULT_CIRCULAR true	// default circularity:true (linear:false)
#define DEFAULT_CREXBPS true	// use breakpoint szenarios for prime nodes
#define DEFAULT_MAXSZEN 2	// maximum number of alternatives for prime nodes
#define DEFAULT_ALTERNATIVES false // compute alternative szenarios
#define DEFAULT_PVALMETH "mo"	// pvalue computation method
#define DEFAULT_REPEATS 100
#define DEFAULT_MODFRAC 0.5
#define DEFAULT_STRONG false // compute alternative szenarios


class ArcP{
public:
	rrrmt* rrrm;
	double cent;
	double pval;
	double weig;
};

class VertexP{
public:
	bool anc;	// ancestral node
	genom gen;	// genome
	vector<string> nam;// all names with the same gene order
	string tax;	//taxonomic information
};

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, VertexP, ArcP> Graph;

typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;
typedef boost::graph_traits<Graph>::vertex_iterator Vertex_iter;
typedef boost::property_map<Graph, boost::vertex_index_t>::type VertexIndexMap;

typedef boost::graph_traits<Graph>::edge_descriptor Arc;
typedef boost::graph_traits<Graph>::edge_iterator Arc_iter;

typedef std::map<Arc, int> StdEdgeIndexMap;
typedef boost::associative_property_map< StdEdgeIndexMap > EdgeIndexMap;



using namespace std;


/**
 * add connection between two nodes.
 * if not longer then the reverse edge.
 *
 * @param[in,out] g the graph
 * @param[in] n1 start of the edge
 * @param[in] n2 end of the edge
 * @param[in] crexbps CREx parameter
 * @param[in] maxszen CREx parameter
 * @param[in] mkalt CREx parameter
 * @param[in] maxd maximum allowed distance
 * @param[in] prime true => allow only prime comparisons, false => allow only linear comparisons
 * @return true iff an edge was added
 */
bool add_arc(Graph &g, Vertex &n1, Vertex &n2,
		bool crexbps, unsigned maxszen, bool mkalt,
		unsigned maxd = numeric_limits<unsigned>::max(),
		bool prime = false);

/**
 * add connection between two nodes. i.e. the forward and reverse edge.
 * if one of both is longer, then only the shorted is added
 *
 * @param[in,out] g the graph
 * @param[in] n1 start of the edge
 * @param[in] n2 end of the edge
 * @param[in] crexbps CREx parameter
 * @param[in] maxszen CREx parameter
 * @param[in] mkalt CREx parameter
 * @param[in] d allowed distance
 * @param[in] maxtdrl max allowed number of tdrls
 * @return true iff an edge was added
 */
bool add_arc_maxtdrl(Graph &g, Vertex &n1, Vertex &n2,
		bool crexbps, unsigned maxszen, bool mkalt,
		unsigned d = numeric_limits<unsigned>::max(),
		unsigned maxtdrl = numeric_limits<unsigned>::max());

/**
 * add an arc between two vertices and initialize it with the arc properties
 * of another arc
 * @param[in,out] g the graph
 * @param[in] a an arc for copying the properties
 * @param[in] nsrc source vertex
 * @param[in] ntgt target vertex
 * @return true iff the arc was added
 */
bool add_arc_wprop( Graph &g, const Arc &a, Vertex &nsrc, Vertex &ntgt );

/**
 * compute strong/normal connected components of a graph and their number
 * @param[g] g the graph
 * @param[in] strong compute strong connected components iff true
 * @param[out] comp the components (vertor is initialized in the function)
 * @return the number of components
 */
unsigned components(const Graph &g, bool strong, vector<int> &comp);

///**
// * contract edges with common parts in their corresponding rearrangement scenarios
// * @param[in,out] g the graph
// * @param[in,out] gd
// * @param[in] crexbps
// * @param[in] maxscen
// * @param[in] mkalt
// * @param[in] in true consider inedges of a node, false consider outedges
// */
//void contract_edges(ListDigraph &g, graph_data &gd,
//		bool crexbps, unsigned maxszen, bool mkalt, bool in );
//
///**
// * delete (fwd & rev) connection between two nodes
// * - forward is deleted anyway
// * - reverse is only deleted if the rearrangement scenario has the same length
// *   as the one assocated to the fwd edge
// * .
// *
// * @param[in,out] g the graph
// * @param[in,out] ar arc rearrangement scenario map
// * @param[in] n1 source node
// * @param[in] n1 target node
// */
//void erase_arc( ListDigraph &g, graph_data &gd,
//		const ListDigraph::Node &n1, const ListDigraph::Node &n2 );

/**
 * read command line options
 * @param[in] argc number of arguments
 * @param[in] argv the arguments
 * @param[out] fname input filename
 * @param[in,out] circular the circularity of the genomes
 * @param[in,out] crexbps use breakpoint szenarios for prime nodes
 * @param[in,out] maxszen maximum number of alternatives for prime nodes
 * @param[in,out] mkalt compute alternative szenarios
 * @param[in,out] strong strong components
 * @param[out] pvalmeth pvalue computation method
 * @param[out] repeats number of repeats for p-value computation
 * @param[out] modfrac fraction of the edges of a component to relocate
 * for p-value computation
 */
void getoptions( int argc, char *argv[], string &fname, bool &circular,
		bool &crexbps, unsigned &maxszen, bool &mkalt, bool &strong,
		map<string,string> &ainfo,
		string &pvalmeth, unsigned &repeats, double &modfrac);

/**
 * initialize a network with where each node represents a genome
 * @param[in] genomes
 * @param[in] names
 * @param[in,out] g
 */
void init_network( const vector<genom> &genomes, const vector<string> &names,
		Graph &g, map<string,string> &ainfo );


/**
 * compute the initial minimum spanning network
 *
 * @param[out] g min spanning network
 * @param[in] strong use strong connectivity
 */
void init_spanning_network(
		bool crexbps, unsigned maxszen, bool mkalt,
		Graph &g, bool strong);

/**
 * compute the complete network
 *
 * @param[out] g min spanning network
 */
void init_complete_network(
		bool crexbps, unsigned maxszen, bool mkalt,
		Graph &g);


map<string, string> read_info(string filename){
	fstream file;
	string line;
	vector<string> split;
	map<string, string> m;

	file.open(filename.c_str(), ios::in);

	while(file){
		getline(file, line);
		split_string(line, split);

//		cerr << line<<endl;
//		copy( split.begin(), split.end(), ostream_iterator<string>(cerr, "|") );cerr << endl;
		if(split.size()>1){
			m[split[0]] = split[1];
		}
		split.clear();
	}

	file.close();
	return m;
}

/**
 * compute pvalues. randomistion by permutation of the edge weights.
 * @param[in] g the graph
 * @param[in] repeats number of repeats to be done for p-value computation
 */
void pvalues_permweight(Graph &g, unsigned repeats);

/**
 * compute p-values for the edges by relocating the edges itself for which the
 * p-value is to be computes. the number of relocations is limited to repeats
 * and the fraction modfrac of the number of edges of the component (whatever
 * matches first)
 *
 * @param[in] g the graph
 * @param[in] strong use strong components
 * @param[in] repeats number of samples to use for p-value computation
 * @param[in] modfrac fraction of the edges of a component to modify
 *
 */
void pvalues_modedge(Graph &g, bool strong, unsigned repeats, double modfrac);

/**
 * compute pvalues for the edges by relocating a fraction of the edges of each
 * component.
 *
 * @param[in] g the graph
 * @param[in] strong use strong components
 * @param[in] repeats number of samples to use for p-value computation
 * @param[in] modfrac fraction of the edges of a component to modify
 */
void pvalues_modother(Graph &g, bool strong, unsigned repeats, double modfrac);

/**
 * print usage info and exit
 */
void usage();

/**
 * write graph
 * @param[in] g the graph
 * @param[in,out] out the stream to write into
 * @param[in] armap arc rearrangememnt scenario map
 */
void write_graph( const Graph &g, ostream &out, string type, map<rrrmt*, unsigned, HDereferenceLess> &ridx );

////void write_gml( const ListDigraph &g, ostream &out,  );
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


int main(int argc, char *argv[]){

	bool circular = DEFAULT_CIRCULAR,	// treat genomes as circular
		crexbps = DEFAULT_CREXBPS,	// use breakpoint szenarios for prime nodes
		mkalt = DEFAULT_ALTERNATIVES,	// compute alternative szenarios
		strong = DEFAULT_STRONG;		// use strong components instead of components
	double modfrac = DEFAULT_MODFRAC;
	Graph g;
	string fname, 	// file name for input
		pvalmeth = DEFAULT_PVALMETH;
	unsigned maxszen = DEFAULT_MAXSZEN, 	// maximum number of alternatives for prime nodes
		repeats = 0;	// number of repeats for p-value computation
	vector<genom> genomes,
		ingenomes; // the input genomes
	vector<string> names, 	// the names of the input genomes
		nmap; 				// the names of the genes
	map<rrrmt*, unsigned, HDereferenceLess> ridx;	// rearrangement index
	map<string, string> ainfo;

	init_rng( );

	getoptions( argc, argv, fname, circular, crexbps, maxszen, mkalt, strong,
			ainfo, pvalmeth, repeats, modfrac );

	if( fname == "" ){
		cerr << "error: no file name given "<<endl;
		usage();
	}

	// read the genomes from the file
	read_genomes(fname, ingenomes, names, circular, nmap, true, false);
	genomes = ingenomes;
	if( ingenomes.size() == 0 ){
		cerr << "error: no gene orders found in "<<fname<<endl;
		exit(EXIT_FAILURE);
	}

	cerr << "# "<< ingenomes.size()<<" gene orders"<<endl;

	// init nodes of the network
	init_network(genomes, names, g, ainfo);

	// init edges of the network as spanning network
	init_spanning_network( crexbps, maxszen, mkalt,  g, strong);

//	init_complete_network( crexbps, maxszen, mkalt, g);

	unsigned compcnt = 0;	// number or components
	vector<int> comp; 	// the components
	vector<unsigned> compedges;	// the edges of the components
	vector<unsigned> compvert;
	VertexIndexMap vindex = get(boost::vertex_index, g);

	compcnt = components( g, strong, comp );
	compedges = vector<unsigned>(compcnt, 0);
	compvert = vector<unsigned>(compcnt, 0);
	BGL_FORALL_EDGES( edge, g, Graph ){
		if( vindex[ source(edge, g) ] > vindex[ target(edge, g) ] ||
				! boost::edge(target(edge, g), source(edge, g), g).second ){
			compedges[ comp[vindex[source(edge, g)]] ]++;
		}
	}
	BGL_FORALL_VERTICES(v, g, Graph){
		compvert[ comp[vindex[v]] ]++;
	}
	for( unsigned i=0; i<compcnt; i++ ){
		cerr << "# component "<<i<< ": "<<compedges[i]<<" connections "<<compvert[i]<<" vertices"<<endl;
	}


	// compute p-values of the edges
	if( pvalmeth == "wp" ){
		pvalues_permweight(g, repeats);
	}else if( pvalmeth == "me" ){
		pvalues_modedge(g, strong, repeats, modfrac);
	}else if( pvalmeth == "mo" ){
		pvalues_modother(g, strong, repeats, modfrac);
	}else{
		cerr << "error: unknown pvalue computation method: "<<pvalmeth<< endl;
		exit(EXIT_FAILURE);
	}

    // output value
    BGL_FORALL_EDGES(edge, g, Graph){
    	cerr <<edge<<" "<<g[edge].pval<<" "<<g[edge].cent<<" "<<g[edge].weig<<endl;
    }
////	ofstream dot;
////	dot.open ("/tmp/mst.gml");
////	write_gml(g, dot, armap);
////	dot.close();
////
////	// contract edges with common end points
////	// first the inedges
////	contract_edges(g, gd, crexbps, maxszen, mkalt, true);
////	// then the ouedges
////	contract_edges(g, gd, crexbps, maxszen, mkalt, false);
//
//	write_graph(g, cout, "gexf", ridx);
	write_graph(g, cout, "gml", ridx);
//
////	// write rearrangement index
////	for(map<rrrmt*, unsigned, HDereferenceLess>::const_iterator it=ridx.begin(); it!=ridx.end(); it++){
////		(it->first)->output(cerr, ridx);
////		cerr << " : ";
////		(it->first)->output(cerr, 0, 0);
////	}
////
////	// write nodes
////	for (ListDigraph::NodeIt vit(g); vit != INVALID; ++vit){
////		cerr << "> "<<g.id(vit)<<endl;
////		cerr << (*gd.vgmap)[vit]<<endl;
////	}
//
////	// free memory

	BGL_FORALL_EDGES(edge, g, Graph){
		delete g[edge].rrrm;
	}

	return EXIT_SUCCESS;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool add_arc_maxtdrl(Graph &g, Vertex &n1, Vertex &n2,
		bool crexbps, unsigned maxszen, bool mkalt, unsigned d, unsigned maxtdrl ){

	pair<Arc, bool> arc;

	// do not create loops
	if( n1 == n2 ){
		return false;
	}

	if (edge(n1, n2, g).second){
		return false;
	}

	bool added = false;
	rrrmt *fwd,
			*rev;
	unsigned fs=0, rs=0;

	fwd =  new crex( g[n1].gen, g[n2].gen, crexbps, maxszen, mkalt );
	fs = fwd->length( ALTMIN );

	set<rrrmt*, HDereferenceLess> fwdrset;
	unsigned fwdtdrlcnt = 0;
	fwd->getrrrmt( fwdrset );
	for( set<rrrmt*, HDereferenceLess>::const_iterator it=fwdrset.begin(); it!=fwdrset.end(); it++ ){
		if((*it)->typestrg(1) == "TDRL"){
			fwdtdrlcnt += 1;
		}
		delete (*it);
	}

	if(fs != d || fwdtdrlcnt > maxtdrl ){
		delete fwd;
		return false;
	}

	rev = new crex( g[n2].gen, g[n1].gen, crexbps, maxszen, mkalt );
	rs = rev->length( ALTMIN );
	set<rrrmt*, HDereferenceLess> revrset;
	unsigned revtdrlcnt = 0;
	rev->getrrrmt( revrset );
	for( set<rrrmt*, HDereferenceLess>::const_iterator it=revrset.begin(); it!=revrset.end(); it++ ){
		if((*it)->typestrg(1) == "TDRL"){
			revtdrlcnt += 1;
		}
		delete (*it);
	}
	delete rev;

	if( fs <= rs  ){
		arc = add_edge(n1, n2, g);
		g[arc.first].rrrm = fwd;
		g[arc.first].cent = 0;
		g[arc.first].pval = 0;
		g[arc.first].weig = fs;
		added = true;
	}else{
		delete fwd;
	}

	return added;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool add_arc(Graph &g, Vertex &n1, Vertex &n2,
		bool crexbps, unsigned maxszen, bool mkalt, unsigned maxd, bool prime ){

	pair<Arc, bool> arc;

	// todo optimize: precompute is_prime, distances, and maybe scenarios

	// do not create loops
	if( n1 == n2 ){
		return false;
	}

	if( prime != interval_tree_is_prime( g[n1].gen, g[n2].gen )){
		return false;
	}

	bool added = false;
	rrrmt *fwd,
			*rev;
	unsigned fs=0, rs=0;

	fwd =  new crex( g[n1].gen, g[n2].gen, crexbps, maxszen, mkalt );
	fs = fwd->length( ALTMIN );

	rev = new crex( g[n2].gen, g[n1].gen, crexbps, maxszen, mkalt );
	rs = rev->length( ALTMIN );

	if( fs <= maxd && fs <= rs ){
		arc = add_edge(n1, n2, g);
		g[arc.first].rrrm = fwd;
		g[arc.first].cent = 0;
		g[arc.first].pval = 0;
		g[arc.first].weig = fwd->length(ALTMIN);
		added = true;
	}else{
		delete fwd;
	}

	return added;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool add_arc_wprop( Graph &g, const Arc &a, Vertex &nsrc, Vertex &ntgt ){

	// do not create loops
	if( nsrc == ntgt ){
		return false;
	}

	// do not create duplicate edges
	if( boost::edge(nsrc, ntgt, g).second ){
		return false;
	}

	pair<Arc, bool> arc = add_edge(nsrc, ntgt, g);
	g[arc.first].rrrm = g[a].rrrm;
	g[arc.first].cent = g[a].cent;
	g[arc.first].pval = g[a].pval;
	g[arc.first].weig = g[a].weig;

	return arc.second;

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned components(const Graph &g, bool strong, vector<int> &comp){
	comp = vector<int>(num_vertices(g), 0);
	if( strong ){
		return strong_components(g, &comp[0]);
	}else{
		return connected_components(g, &comp[0]);
	}
}


//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//void contract_edges(ListDigraph &g, graph_data &gd,
//		bool crexbps, unsigned maxszen, bool mkalt, bool in ){
//
//	bool acted;
//	do{
//
//		set<ListDigraph::Arc> edges;	// incident edges (in/out)
//		set<ListDigraph::Node> arcmn;	// arcs with common parts
//		map<set< ListDigraph::Node>, rrrmt*> todolst;
//
//		acted = false;
//
//		for (ListDigraph::NodeIt vit(g); vit != INVALID; ++vit){
//
//			// get the set of edges
//			cerr << "============================"<<endl;
//			cerr << (*gd.vgmap)[vit]<<endl;
//			if(in){
//				for(ListDigraph::InArcIt e(g, vit); e!=INVALID; ++e){
//					edges.insert( e );
//					cerr <<"+++++++ "<< g.id(g.source(e))<< " -> "<<g.id(g.target(e))<<endl;
//					cerr << *((*gd.armap)[e])<<endl;
//				}
//			}else{
//				for(ListDigraph::OutArcIt e(g, vit); e!=INVALID; ++e){
//					edges.insert( e );
//					cerr <<"+++++++ "<< g.id(g.source(e))<< " -> "<<g.id(g.target(e))<<endl;
//					cerr << *((*gd.armap)[e])<<endl;
//				}
//			}
//
//
//			for( set<ListDigraph::Arc>::const_iterator e=edges.begin(); e!=edges.end(); ++e){
//
//				rrrmt *intrsc = (*gd.armap)[*e]->clone(),
//						*tmp = NULL;
//				for( set<ListDigraph::Arc>::const_iterator f=edges.begin(); f!=edges.end(); ++f){
//
//					// get the intersection for the inarcs the suffix, for the outarcs the prefix
//					tmp = intrsc->intersect((*gd.armap)[*f], !in);
//					if( tmp->isempty() ){
//						delete tmp;
//					}else{
//						delete intrsc;
//						intrsc = tmp;
//
//						if(in){
//							arcmn.insert( g.source(*f) );
//						}else{
//							arcmn.insert( g.target(*f) );
//						}
//					}
//				}
//
//				// check if there are edges that have something in common with e
//				// and add the set to the todolst
//				if( arcmn.size() > 1 &&  todolst.find( arcmn ) == todolst.end()){
//					todolst[arcmn] = intrsc;
//				}else{
//					delete intrsc;
//				}
//
//				arcmn.clear();
//			}
//
//			// Process todo list
//			for( map<set<ListDigraph::Node>, rrrmt* >::iterator it=todolst.begin(); it!=todolst.end(); it++ ){
//				acted = true;
//				genom ag;		// ancestral gene arrangement
//				ListDigraph::Node av; 	// node corresponding to the ancestral gene arrangement
//
//				rrrmt *intrsc = it->second;
//
//				cerr << "processing "<<g.id(vit)<<" <- ";
//				for( set<ListDigraph::Node>::const_iterator jt=(it->first).begin(); jt!=(it->first).end(); jt++){
//					cerr <<g.id( *jt )<<" ";
//				}
//				cerr <<endl;
//				cerr <<*intrsc;
//
////				ListDigraph::Node t = vit;		// target node
//
//				// determine ancestral gene arrangement
//				ag = (*gd.vgmap)[vit];
//				if(in){
//					intrsc->deapply( ag );
//				}else{
//					intrsc->apply( ag );
//				}
//				av = add_node(g, gd, ag, false);
//				cerr << "anc node "<< g.id(av)<< " : "<<ag<<endl;
//
//				// delete old connections
//				for( set<ListDigraph::Node>::const_iterator jt=(it->first).begin(); jt!=(it->first).end(); jt++){
//					if(in){
//						erase_arc( g, gd, *jt, vit );
//					}else{
//						erase_arc( g, gd, vit, *jt  );
//					}
//				}
//
//				// set new connections
//				if(in){
//					add_arc(g, gd, av, vit, crexbps, maxszen, mkalt );
//				}else{
//					add_arc(g, gd, vit, av, crexbps, maxszen, mkalt );
//				}
//
//				for( set<ListDigraph::Node>::const_iterator jt=(it->first).begin(); jt!=(it->first).end(); jt++){
//					if(in){
//						add_arc(g, gd, *jt, av, crexbps, maxszen, mkalt );
//					}else{
//						add_arc(g, gd, av, *jt, crexbps, maxszen, mkalt );
//					}
//				}
//			}
//			todolst.clear();
//			edges.clear();
//		}
////		cerr << "#########################"<<endl;
//	}while( acted );
//}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void init_network( const vector<genom> &genomes, const vector<string> &names,
		Graph &g, map<string,string> &ainfo){

	Vertex v;
	for ( unsigned i=0; i<genomes.size(); i++){
		set<string> taxset;
		vector<string> strs;

		boost::split( strs, names[i], boost::is_any_of( " ," ));
		for(unsigned j=0; j<strs.size(); j++){
			taxset.insert( ainfo[strs[j]]);
		}
		if(taxset.size()>2){
			copy(taxset.begin(), taxset.end(), ostream_iterator<string>(cerr, " ")); cerr << endl;
		}

		v = add_vertex(g);
		g[v].gen = genomes[i];
		g[v].nam = strs;
		g[v].tax = ainfo[strs[0]];
		g[v].anc = true;


	}
	cerr << "# initialized "<<boost::num_vertices(g)<<" nodes"<<endl;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void init_complete_network(
		bool crexbps, unsigned maxszen, bool mkalt,
		Graph &g){

	// determine longest distance
	BGL_FORALL_VERTICES(p, g, Graph){
		BGL_FORALL_VERTICES(q, g, Graph){
			if( p==q )
				continue;

			pair<Arc, bool> arc;
			rrrmt *fwd;
			fwd =  new crex( g[p].gen, g[q].gen, crexbps, maxszen, mkalt );
			arc = add_edge(p, q, g);


			g[arc.first].rrrm = fwd;
			g[arc.first].cent = 0;
			g[arc.first].pval = 0;
			g[arc.first].weig = fwd->length( ALTMIN );
		}
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void init_spanning_network(
		bool crexbps, unsigned maxszen, bool mkalt,
		Graph &g, bool strong){

	unsigned cc; // component count

	// TODO method
	// - separate thresholds for linear and prime comparisons
	// - add linear comparisons until no further can be added
	// - add a single prime comparison level
	// - re start linear and prime counters accordingly

	unsigned d = 1,	// iteration distance
		maxd = 0; 	// maximum distance
//	bool prime = false;	// prime comparisons allowed

	// determine longest distance
	BGL_FORALL_VERTICES(p, g, Graph){
		BGL_FORALL_VERTICES(q, g, Graph){
			rrrmt *rr =  new crex( g[p].gen, g[q].gen, crexbps, maxszen, mkalt );
			maxd = max( maxd, rr->length(ALTMIN));
			delete rr;
		}
	}

//	// add edges
//	do{
//		Vertex p,
//			q;
//		vector<int> comp(num_vertices(g));	// the components
//
//		cc = components( g, strong, comp);
//
//		cerr <<"D "<< d <<"/"<<maxd<<" COMP "<< cc<<endl;
//		BGL_FORALL_VERTICES(p, g, Graph){
//			BGL_FORALL_VERTICES(q, g, Graph){
//				if( comp[p] == comp[q] ){
//					continue;
//				}
//				add_arc(g, p, q, crexbps, maxszen, mkalt, d, prime)	;
//			}
//		}
//		d++;
//		if ( d == maxd){
//			prime = true;
//			d = 1;
//		}
//		if( prime == true && d > 1 ){
//			break;
//		}
//	}while( cc>1 );


	// add edges
	bool added;
	do{
		vector<int> comp(num_vertices(g));	// the components

		added = false;
		cc = components( g, strong, comp);
		cerr <<"# d="<< d<<"/"<< maxd <<" "<<cc<<" components "<<boost::num_edges(g)<<" edges"<<endl;

		BGL_FORALL_VERTICES(p, g, Graph){
			BGL_FORALL_VERTICES(q, g, Graph){
				if( comp[p] == comp[q] ){
					continue;
				}
				added |= add_arc_maxtdrl(g, p, q, crexbps, maxszen, mkalt, d, 1)	;
			}
		}
		d++;
	}while( added );
	cerr <<"# initialised "<<boost::num_edges(g)<< " edges in "<<cc<<" components" <<endl;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void pvalues_permweight(Graph &g, unsigned repeats){

	cerr << "# pvalue computation by weight permutation"<<endl;

    // std::map used for convenient initialization
    StdEdgeIndexMap my_e_index;
    // associative property map needed for iterator property map-wrapper

    EdgeIndexMap e_index(my_e_index);
    int i = 0;
    BGL_FORALL_EDGES(edge, g, Graph){
    	my_e_index.insert(std::pair< Arc, int >( edge, i));
    	++i;
    }

    // compute edge betweenness centrality for original graph
    brandes_betweenness_centrality( g,
    		edge_centrality_map(get(&ArcP::cent, g)).
    		weight_map( get(&ArcP::weig, g) )
    );

    unsigned maxd = 0;
    BGL_FORALL_EDGES(edge, g, Graph){
    	maxd = max(maxd, g[edge].rrrm->length(ALTMIN));
//      std::cerr << edge <<"["<< e_index[edge] <<"]"<< ": " << e_centrality_map[edge] << std::endl;
    }
    vector<double> rweight_vec(boost::num_edges(g));
    boost::iterator_property_map< std::vector< double >::iterator, EdgeIndexMap >
            rweight_map(rweight_vec.begin(), e_index);
    BGL_FORALL_EDGES(e, g, Graph){
    	rweight_map[e] = g[e].weig;
    }


    for(unsigned i=0; i<repeats; i++){
    	// shuffle weights
    	//random_shuffle( rweight_vec.begin(), rweight_vec.end() );
    	for(unsigned j=0; j<rweight_vec.size(); j++){
    		rweight_vec[j] = ask_rng(1, maxd);
    	}

        vector<double> re_centrality_vec(boost::num_edges(g), 0.0);
        boost::iterator_property_map< std::vector< double >::iterator, EdgeIndexMap >
                re_centrality_map(re_centrality_vec.begin(), e_index);

        brandes_betweenness_centrality( g,
        		edge_centrality_map( re_centrality_map ).
        		weight_map( rweight_map ) );

//        copy(re_centrality_vec.begin(), re_centrality_vec.end(), ostream_iterator<double>(cerr, " "));cerr <<endl;

        BGL_FORALL_EDGES(edge, g, Graph){
        	if( re_centrality_map[edge] >= g[edge].cent ){
        		g[edge].pval += 1;
        	}
        }
    }

    BGL_FORALL_EDGES(edge, g, Graph){
   		g[edge].pval /= repeats;
    }

//    copy(pvalue.begin(), pvalue.end(), ostream_iterator<double>(cerr, endl));cerr <<endl;
//    copy(e_centrality_vec.begin(), e_centrality_vec.end(), ostream_iterator<double>(cerr, endl));cerr <<endl;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void pvalues_modedge(Graph &g, bool strong, unsigned repeats, double modfrac){
	cerr << "# pvalue computation by modifying other edges"<<endl;


	unsigned compcnt = 0;	// number or components
	vector<int> comp; 	// the components
	vector<Arc> edgevec;	// the edges of the components
	vector<vector<Arc> > compedges;	// the edges of the components
	vector<vector<Vertex> > compvert1,
		compvert2; // vertices of the components
	VertexIndexMap vindex = get(boost::vertex_index, g);

	// get betweenness for the graph
    brandes_betweenness_centrality( g,
    		edge_centrality_map(get(&ArcP::cent, g)).
    		weight_map( get(&ArcP::weig, g) )
    );

	// get information on the components
	// - i.e. list of contained edges (only one of bidirectional edges)
	//   and vertices
	compcnt = components( g, strong, comp );
	compedges = vector<vector<Arc> >(compcnt);
	compvert1 = vector<vector<Vertex> >(compcnt);
	BGL_FORALL_EDGES( edge, g, Graph ){
		if( vindex[ source(edge, g) ] > vindex[ target(edge, g) ] ||
				! boost::edge(target(edge, g), source(edge, g), g).second ){
			compedges[ comp[vindex[source(edge, g)]] ].push_back( edge );
		}
		edgevec.push_back( edge );
	}
	BGL_FORALL_VERTICES(v, g, Graph){
		compvert1[ comp[vindex[v]] ].push_back( v );
	}
	compvert2 = compvert1;

//	for(unsigned i=0; i<compvert1.size(); i++){
//		cerr << "component "<<i<<" " <<compvert1[i].size()<<" nodes"<<endl;
//	}
//	for(unsigned i=0; i<compedges.size(); i++){
//		cerr << "component "<<i<<" " <<compedges[i].size()<<" edges"<<endl;
//	}

	for( unsigned eidx=0; eidx < edgevec.size(); eidx++){

		pair<Arc, bool> em;		// reverse edge
		unsigned modcnt = 0,	// number of modifications applied to the edge
			cidx = -1; 			// component of edge (only this component will be randomized)
		Vertex s, t, 	// source & target
			ns, nt;		// new source and target

		s = source( edgevec[eidx], g );
		t = target( edgevec[eidx], g );

		em = boost::edge(t, s, g);

		cidx = comp[vindex[ s ]];

		// test (in random order) all pairs of vertices of the component
		// as new source and target for the edge
		random_shuffle( compvert1[cidx].begin(), compvert1[cidx].end() );
		for(unsigned vidx=0; vidx<compvert1[cidx].size(); vidx++){
			ns = (compvert1[cidx][vidx]);
			random_shuffle( compvert2[cidx].begin(), compvert2[cidx].end() );
			for(unsigned vjdx=0; vjdx<compvert2[cidx].size(); vjdx++){
				nt = (compvert2[cidx][vjdx]);

				// new souce and target must not be:
				// - the same node
				// - already connected
				if(ns == nt){
					continue;
				}

				if( boost::edge(ns, nt, g).second || boost::edge(nt, ns, g).second ){
					continue;
				}
				// try the modification
				// add ns->nt and remove s->t
				// add nt->ns and remove t->s (if t->s was present)
				add_arc_wprop( g, boost::edge(s, t, g).first, ns, nt );
				g.remove_edge(boost::edge(s, t, g).first);
				if( em.second ){
					add_arc_wprop( g, boost::edge(t, s, g).first, nt, ns );
					g.remove_edge(boost::edge(t, s, g).first);
				}

				// check connectedness with the new edge. s and t should
				// still be in the same component. if not -> revert
				vector<int> ncomp;
				components(g, strong, ncomp);
				if( ncomp[s] == ncomp[t] ){
					modcnt++;

					// std::map used for convenient initialization
					StdEdgeIndexMap my_e_index;
					EdgeIndexMap e_index(my_e_index);
					int l = 0;
					BGL_FORALL_EDGES(x, g, Graph){
						my_e_index.insert(std::pair< Arc, int >( x, l));
						++l;
					}
					// Define EdgeCentralityMap
					std::vector< double > e_centrality_vec(boost::num_edges(g), 0.0);
					// Create the external property map
					boost::iterator_property_map< std::vector< double >::iterator, EdgeIndexMap >
							e_centrality_map(e_centrality_vec.begin(), e_index);

					// compute new betweenness
					brandes_betweenness_centrality( g,
							edge_centrality_map( e_centrality_map ).
							weight_map( get(&ArcP::weig, g) ) );

					// ! modify properties of the edge (s,t) via (ns,nt) .. since edge currently is gone
//					cerr <<"ME "<< edgevec[eidx]<<" " << g[boost::edge(ns, nt, g).first].cent <<" "<< e_centrality_map[edgevec[eidx]]<<" "<<modcnt<<endl;
					if( e_centrality_map[boost::edge(ns, nt, g).first] >= g[boost::edge(ns, nt, g).first].cent ){
						g[boost::edge(ns, nt, g).first].pval += 1;
					}
				}

				// revert changes
				add_arc_wprop( g, boost::edge(ns, nt, g).first, s, t );
				g.remove_edge(boost::edge(ns, nt, g).first);
				if ( em.second ){
					add_arc_wprop( g, boost::edge(nt, ns, g).first, t, s );
					g.remove_edge(boost::edge(nt, ns, g).first);
				}

				if( ((double)modcnt) / ((double)compedges[cidx].size() ) >= modfrac || modcnt >= repeats ){
					break;
				}
			}
			if( ((double)modcnt) / ((double)compedges[cidx].size() ) >= modfrac || modcnt >= repeats){
				break;
			}
		}

//		cerr << edgevec[eidx] << " "<<  g[boost::edge(s, t, g).first].pval << " "<< modcnt<<endl;
		if(modcnt > 0){
			g[boost::edge(s, t, g).first].pval /= (double) modcnt;
		}else{
			g[boost::edge(s, t, g).first].pval = 1;
		}
//		cerr << edgevec[eidx] << " "<<  g[boost::edge(s, t, g).first].pval << " "<< modcnt<< endl;
//		cerr<< boost::edge(s, t, g).first<<" " <<g[boost::edge(s, t, g).first].pval<<" "<<g[boost::edge(s, t, g).first].cent<< " "<< modcnt<<endl;
//		cerr<< boost::edge(t, s, g).first<<" " <<g[boost::edge(t, s, g).first].pval<<" "<<g[boost::edge(t, s, g).first].cent<< " "<< modcnt<<endl;
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void pvalues_modother(Graph &g, bool strong, unsigned repeats, double modfrac){

	cerr << "# pvalue computation by modifying edge itself"<<endl;

	unsigned compcnt = 0;	// number or components
	vector<int> comp; 	// the components
	vector<vector<Arc> > compedges;	// the edges of the components
	vector<vector<Vertex> > compvert1,
		compvert2; // vertices of the components
	VertexIndexMap vindex = get(boost::vertex_index, g);


	// get betweenness centrality for the original graph
	// result is stored in the cent property of the edges

    brandes_betweenness_centrality( g,
    		//centrality_map(boost::make_iterator_property_map(v_centrality_vec.begin(), get(boost::vertex_index, g), double())).
    		edge_centrality_map(get(&ArcP::cent, g)).
//    		weight_map(m)
    		weight_map( get(&ArcP::weig, g) )
    );

	// get information on the components
	// - list of contained edges (only one of bidirectional edges)
    //   (only those i->j with i>j or j->i does not exist)
	// - and vertices (2 copies needed)
	compcnt = components( g, strong, comp );
	compedges = vector<vector<Arc> >(compcnt);
	compvert1 = vector<vector<Vertex> >(compcnt);
	BGL_FORALL_EDGES( edge, g, Graph ){
		if( vindex[ source(edge, g) ] > vindex[ target(edge, g) ] ||
				! boost::edge(target(edge, g), source(edge, g), g).second ){
			compedges[ comp[vindex[source(edge, g)]] ].push_back( edge );
		}
	}
	BGL_FORALL_VERTICES(v, g, Graph){
		compvert1[ comp[vindex[v]] ].push_back( v );
	}
	compvert2 = compvert1;

	// compute p-value for each edge
	BGL_FORALL_EDGES( ed, g, Graph ){
		double avgmodcnt = 0.0;

		// do #repeats repetitions
		for( unsigned i=0; i<repeats; i++ ){
//			cerr << "computing p-value for "<<edge<<" repeat "<<i<<endl;

			Graph gcpy;
			unsigned modcnt = 0,// number of modified edges
				cidx = -1; 		// component of edge (only this component will be randomized)

			copy_graph(g, gcpy);
			cidx = comp[vindex[source(ed, g)]];
//			BGL_FORALL_EDGES( e, gcpy, Graph ){
//				gcpy[e].cent = 0.0;
//			}
			// modify the edges of the component in random order
			random_shuffle( compedges[cidx].begin(), compedges[cidx].end() );
			for( unsigned meidx = 0; meidx < compedges[cidx].size() && ((double)modcnt) / ((double)compedges[cidx].size() ) < modfrac; meidx++ ){
//				cerr << modcnt<<endl;
				Arc me;	// edge to modify
				pair<Arc, bool> em;
				Vertex s, t, ns, nt;

				// the edge ...
				me = compedges[cidx][meidx];
				s = source(me, gcpy);
				t = target(me, gcpy);
				// ... and reverse edge
				em = boost::edge(t,s,gcpy);

				// do not modify the edge (or reverse edge) for which we try to get the  p-value ..
				if( me == ed || ( em.second && em.first == ed ) ){
					continue;
				}

				// choose a random new target for the edge: test all vertices of
				// the component in random order take the first eligible
				random_shuffle( compvert1[cidx].begin(), compvert1[cidx].end() );
				random_shuffle( compvert2[cidx].begin(), compvert2[cidx].end() );
//				cerr << "checking "<<compvert1[cidx].size()<<" nodes"<<endl;
				for(unsigned vidx=0; vidx<compvert1[cidx].size(); vidx++){
					ns = (compvert1[cidx][vidx]);
					for(unsigned vjdx=0; vjdx<compvert2[cidx].size(); vjdx++){
						nt = (compvert2[cidx][vjdx]);
//						cerr << ns <<" -> "<<nt << " ";
						if(ns == nt){
							continue;
						}

						// only allow unconnected edges
						if( boost::edge(ns, nt, gcpy).second || boost::edge(nt, ns, gcpy).second  ){
							continue;
						}

						// try the modification
						// add s->ntgt and remove s->t
						// add ntgt->s and remove t->s (if t->s was present)
						add_arc_wprop( gcpy, me, ns, nt );
						gcpy.remove_edge(boost::edge(s, t, gcpy).first);
						if( em.second){
							add_arc_wprop( gcpy, boost::edge(t, s, gcpy).first, nt, ns );
							gcpy.remove_edge(boost::edge(t, s, gcpy).first);
						}

						// check connectedness with the new edge. s and t should
						// still be in the same component. if not -> revert
						vector<int> ncomp;
						components(gcpy, strong, ncomp);
						if( ncomp[s] != ncomp[t] ){
//							cerr << "disconnect"<<endl;
							add_arc_wprop( gcpy, boost::edge(ns, nt, gcpy).first, s, t );
							gcpy.remove_edge(boost::edge(ns, nt, gcpy).first);
							if ( em.second ){
								add_arc_wprop( gcpy, boost::edge(nt, ns, gcpy).first, t, s );
								gcpy.remove_edge(boost::edge(nt, ns, gcpy).first);
							}
							continue;
						}
						// stop searching for a new target as soon as possible
						else{
//							cerr << "found"<< endl;
							break;
						}
					}//ENF FOR NT

					if ( ! boost::edge(s, t, gcpy).second){
						break;
					}
				}//ENF FOR NS
				if( ! boost::edge(s, t, gcpy).second ){
					modcnt++;
				}
			}


		    // std::map used for convenient initialization
		    StdEdgeIndexMap my_e_index;
		    EdgeIndexMap e_index(my_e_index);
		    int l = 0;
		    BGL_FORALL_EDGES(x, gcpy, Graph){
		      my_e_index.insert(std::pair< Arc, int >( x, l));
		      ++l;
		    }
		    // Define EdgeCentralityMap
		    std::vector< double > e_centrality_vec(boost::num_edges(gcpy), 0.0);
		    // Create the external property map
		    boost::iterator_property_map< std::vector< double >::iterator, EdgeIndexMap >
		            e_centrality_map(e_centrality_vec.begin(), e_index);

			// compute betweenness centrality for the modified graph
			brandes_betweenness_centrality( gcpy,
//					edge_centrality_map(get(&ArcP::cent, gcpy)).
            		edge_centrality_map( e_centrality_map ).
					weight_map( get(&ArcP::weig, gcpy) ) );

//			cerr << ed<<" " << e_centrality_map[ed]<<" "<<g[ed].cent  <<" "<<modcnt<<endl;
			avgmodcnt += modcnt;
			if( e_centrality_map[ed] >= g[ed].cent ){
				g[ed].pval += 1;
			}
		}// END FOR REPEATS
//		cerr << ed <<": "<< g[ed].pval<<" "<<g[ed].cent<< " "<< avgmodcnt<<endl;
		g[ed].pval /= (double) repeats;
		avgmodcnt/=(double) repeats;
		cerr << ed <<": "<< g[ed].pval<<" "<<g[ed].cent<< " "<< avgmodcnt<<endl;
	}// END FOR EDGES

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void getoptions( int argc, char *argv[], string &fname, bool &circular,
		bool &crexbps, unsigned &maxszen, bool &mkalt, bool &strong,
		map<string,string> &ainfo,
		string &pvalmeth, unsigned &repeats, double &modfrac){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"file",    required_argument, 0, 'f'},
			{"help",    no_argument,       0, 'h'},
			{"frac",  required_argument, 0, 'g'},
			{"pvmeth",  required_argument, 0, 'p'},
			{"repeat",  required_argument, 0, 'r'},
			{"strong",  no_argument, 0, 's'},
            {0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "af:hi:lm:op:r:s",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cerr << "option "<< long_options[option_index].name;
				if (optarg)
					cerr << " with arg " << optarg;
				cout << endl;
				break;
			case 'a':
				mkalt = false;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'g':
				modfrac = atof(optarg);
				break;
			case 'h':
				usage();
				break;
			case 'i':
				ainfo = read_info( optarg );
				break;
				circular = false;
				break;
			case 'm':
				maxszen = atoi(optarg);
				break;
			case 'o':
				crexbps = false;
				break;
			case 'p':
				pvalmeth = optarg;
				if( pvalmeth != "wp" && pvalmeth != "me" && pvalmeth != "mo" ){
					cerr << "error: unknown pvalue computation method: "<<pvalmeth<< endl;
					exit(EXIT_FAILURE);
				}
				break;
			case 'r':
				repeats = atoi(optarg);
				if( repeats == 0 ){
					repeats = numeric_limits<unsigned>::max();
				}
				break;
			case 's':
				strong = true;
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
				break;
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cout << "usage"<<endl;
	cout << "general options"<<endl;
	cout << " -a: don't compute alternatives"<<endl;
//	cout << " -b: compute breakpoint scenario [ZhaoBourque07]"<<endl;
	cout << " -o: don't use breakpoint scenario for prime nodes"<<endl;
	cout << " -m: maximal number of reversals for reversal+tdrl scenarios (default: 2)"<<endl;
	cout << " -f: specify a filename "<<endl;
	cout << " -l: handle genomes as linear (default: circular)"<<endl;
	cout << " -i FNAME node info data file"<<endl;
	cout << " -s --strong: use strong components"<<endl;
	cout << endl;
	cout << "p-value computation"<<endl;
	cout << " --pvmeth: p-value computation method (default: "<< DEFAULT_PVALMETH <<")"<<endl;
	cout << "      wp: permute edge weights"<<endl;
	cout << "      me: relocate edge"<<endl;
	cout << "      mo: relocate other edges"<<endl;
	cout << " --repeat: number of repetitions for p-value computation (default: "<< DEFAULT_REPEATS <<")"<<endl;
	cout << "      a value of 0 gives unlimited repeats (useful only for me)"<<endl;
	cout << " --frac: fraction of the number of edges of a component (default: "<< DEFAULT_REPEATS <<")"<<endl;
	cout << "      (me: determines how often the edge itself is relocated)"<<endl;
	cout << "      (mo: determines #edges to be relocated for determining the p-value of an edge"<<endl;
	exit(EXIT_FAILURE);
}

void write_graph_header( string type, ostream &out ){
	if( type == "gexf"){
		out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"<<endl;
		out << "<gexf xmlns=\"http://www.gexf.net/1.2draft\" version=\"1.2\" xmlns:viz=\"http://www.gexf.net/1.2draft/viz\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.gexf.net/1.2draft http://www.gexf.net/1.2draft/gexf.xsd\">"<<endl;
		out <<"<graph mode=\"static\" defaultedgetype=\"directed\">"<<endl;
		out << "<attributes class=\"node\">"<<endl;
		out << "<attribute id=\"0\" title=\"info\" type=\"string\"/>"<<endl;
		out << "</attributes>"<<endl;

	}else if( type== "gml"){
		out << "graph ["<<endl;
		out << "directed 1"<<endl;
	}else{
		cerr << "unknown graph type "<< type <<endl;
		exit(EXIT_FAILURE);
	}
}

void write_graph_footer( string type, ostream &out ){
	if( type == "gexf"){
		out <<"</graph>"<<endl;
		out <<"</gexf>"<<endl;
	}else if( type== "gml"){
		out << "]"<<endl;
	}else{
		cerr << "unknown graph type "<< type <<endl;
		exit(EXIT_FAILURE);
	}
}

void write_graph_edge( const Graph &g,
		const Arc &e, unsigned maxd, ostream &out, string type,
		map<rrrmt*, unsigned, HDereferenceLess> &ridx){

	unsigned tcnt=0, icnt=0,
			tdrlcnt=0, idcnt=0;
	string label, shape;
	stringstream ss;

	float red=0, green=0, blue=0;

	set<rrrmt*, HDereferenceLess> rset;
	g[e].rrrm->getrrrmt( rset );
	for( set<rrrmt*, HDereferenceLess>::const_iterator it=rset.begin(); it!=rset.end(); it++ ){
		if((*it)->typestrg(1) == "TDRL"){
			tdrlcnt += 1;
		}else if((*it)->typestrg(1)=="I"){
			icnt += 1;
		}else if((*it)->typestrg(1)=="T"){
			tcnt += 1;
		}else if((*it)->typestrg(1)=="iT"){
			tcnt +=1;
			icnt += 1;
		}else if((*it)->typestrg(1)=="iD" || (*it)->typestrg(1)=="Id"){
			idcnt +=1;
		}else{
			cerr << "unknown rearrangement " << (*it)->typestrg(1)<<" "<<*(*it) <<endl;
			exit(EXIT_FAILURE);
		}
	}

	red = (float)tdrlcnt / (tdrlcnt+icnt+ tcnt);
	green = (float)icnt / (tdrlcnt+icnt+ tcnt);
	blue = (float)tcnt / (tdrlcnt+icnt+ tcnt);


	g[e].rrrm->output(ss, ridx);
	label = ss.str();
	ss.clear();

/*
	if( idcnt > 0 ){
		shape = "dashed";
	}else{
		shape = "solid";
	}
*/

	if(type=="gexf"){
		out << "<edge id=\""<< e
				<<"\" source=\""<<source(e, g)
				<<"\" target=\""<<target(e,g)
				<<"\" weight=\""<<(maxd+1-g[e].rrrm->length(ALTMIN))
				<<"\" label=\""<<label
				<<"\">"<<endl;
		out <<"<viz:color r=\""<< (unsigned)round(255*red)<<"\" g=\""<< (unsigned)round(255*green)<<"\" b=\""<< (unsigned)round(255*blue)<<"\" a=\""<< 1<<"\"/>"<<endl;
		out <<"<viz:thickness value=\"1\"/>"<<endl;
		//out <<"<viz:shape value=\""<< shape <<"\"/>"<<endl;
		out <<"</edge>"<<endl;

	}else if(type == "gml"){
		out << "edge ["<<endl;
		// out << "id "<< source(e,g)<<"-"<<target(e,g) <<endl;
		out << "source "<<source(e,g)<<endl;
		out << "target "<<target(e,g)<<endl;
		out << "weight "<<(maxd+1-g[e].rrrm->length(ALTMIN))<<endl;
		out << "relevance "<<std::fixed<<g[e].cent<<endl;
		out << "pvalue "<<std::fixed<<g[e].pval<<endl;
		out << "label "<< "\""<<label<<"\""<<endl;
		out << "]"<<endl;
	}
}

void write_graph_edges( const Graph &g, ostream &out,
		string type, map<rrrmt*, unsigned, HDereferenceLess> &ridx ){

	unsigned maxd = 0;
    BGL_FORALL_EDGES(edge, g, Graph)
    {
		maxd = max(maxd, g[edge].rrrm->length(ALTMIN));
    }

//	Arc_iter ei, ei_end;
//	for(tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
//		maxd = max(maxd, g[*ei].r->length(ALTMIN));
//	}


	if(type == "gexf"){
		out << "<edges>"<<endl;
	}

    BGL_FORALL_EDGES(edge, g, Graph)
    {
		write_graph_edge(g, edge, maxd, out, type, ridx);
    }
//	for(tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
//		write_graph_edge(g, *ei, maxd, out, type, ridx);
//	}
	if(type == "gexf"){
		out << "</edges>"<<endl;
	}
}

void write_graph_node( const Graph &g, const Vertex &n, ostream &out, string type){
	unsigned size;

//	size = ceil(log(g[n].nam.size()+1)/log(10));
	size = g[n].nam.size();
//	size = 1;


	if(type == "gexf"){
		out << "<node id=\""<< n <<"\" label=\"";
		if(g[n].nam.size() != 0 ){
			out << g[n].nam[0];
		}
		out <<"\">" << endl;
		out << "<viz:size value=\""<< size <<"\"/>"<<endl;
		out <<"<attvalues>"<<endl;
		out <<"<attvalue for=\"0\" value=\""<< g[n].tax <<"\" />"<<endl;
		out <<"</attvalues>"<<endl;
		out << "</node>" << endl;
	}else if(type=="gml"){
		 out << "node [" << endl;
		 out << "id "<<n<<endl;
		 out << "label \""<<g[n].nam[0]<<"\""<<endl;
		 out << "size "<<size <<endl;
		 out << "tax \""<<g[n].tax<<"\""<<endl;
		 out << "]" << endl;
	}


}

void write_graph_nodes(const Graph &g, ostream &out, string type ){
	if(type == "gexf"){
		out << "<nodes>"<<endl;
	}

	BGL_FORALL_VERTICES(v, g, Graph){
		write_graph_node(g, v, out, type);
	}

	if(type=="gexf"){
		out << "</nodes>"<<endl;
	}
}

void write_graph( const Graph &g, ostream &out,
		string type, map<rrrmt*, unsigned, HDereferenceLess> &ridx){

	write_graph_header( type, out );
	write_graph_nodes(g, out, type);
	write_graph_edges(g, out, type, ridx);
	write_graph_footer( type, out );
}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//void write_dot( const ListDigraph &g, ostream &out,
//		const ListDigraph::ArcMap<rrrmt*> &armap ){
//
//	 map<rrrmt*, unsigned, HDereferenceLess> ridx; // rearrangement index
//
//
//	 out << "digraph nrex {" << endl;
////	 out << "concentrate=true;"<<endl;
//	 out << "  node [ shape=box, fontname=Helvetica, fontsize=10, margin=0 ];" << endl;
//	 for(ListDigraph::NodeIt n(g); n!=INVALID; ++n) {
//	     out << "  n" << g.id(n)
//	          << " [ label=\"" << g.id(n) << "\", margin=0, height=0.2 ]; "<<endl;
//	 }
////	 out << " edge [ shape=ellipse, fontname=Helvetica, fontsize=10 ];" << endl;
//
//	 for(ListDigraph::NodeIt n(g); n!=INVALID; ++n) {
//		 for(ListDigraph::NodeIt m(g, n); m!=INVALID; ++m) {
//			 ListDigraph::Arc nm = findArc(g, n, m),
//					 mn = findArc(g, m, n);
//
//			 if( nm != INVALID && nm != INVALID ){
////				 ", label=\""; armap[nm]->output(out, ridx); out<<
//				 out << "  n" << g.id(n) << " -> " << " n" << g.id(m)<<" [len=\""<< armap[nm]->length(ALTMIN) <<"\", dir=\"both\"];"<<endl;
//			 }else if( nm != INVALID && mn == INVALID ){
////				 ", label=\""; armap[nm]->output(out, ridx); out
//				 out << "  n" << g.id(n) << " -> " << " n" << g.id(m)<<" [len=\""<< armap[nm]->length(ALTMIN) <<"\"];"<<endl;
//			 }else if( nm == INVALID && mn != INVALID ){
////				 , label=\""; armap[mn]->output(out, ridx); out<<
//				 out << "  n" << g.id(m) << " -> " << " n" << g.id(n)<<" [len=\""<< armap[mn]->length(ALTMIN) <<"\"];"<<endl;
//			 }
//		 }
//	 }
////	 for(ListDigraph::ArcIt e(g); e!=INVALID; ++e) {
////	 	out << "  n" << g.id(g.source(e)) << " -> " << " n" << g.id(g.target(e))<<";"<<endl;
////	 }
//
////	 for( map<rrrmt*, unsigned, HDereferenceLess>::const_iterator it=ridx.begin();
////			 it!=ridx.end(); it++){
////
////		 cerr << (it->first)->output(cerr, ridx) << " "<<*(it->first);
////	 }
//
//	 out << "}" << endl;
//}
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
