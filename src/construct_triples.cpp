/** @file construct_tripels.cpp
 * remark: name for historical reasons .. the program can contruct arbitrary tuples
 *
 * construct from a given set of genomes all or a random selection of
 * possible tuples
 */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <limits>
#include <fstream>
#include <vector>
#include <regex.h>

#include<dirent.h>
#include<sys/dir.h>
#include<unistd.h>
#include<sys/stat.h>

#include "io.hpp"
#include "counter.hpp"
#include "genom.hpp"
#include "helpers.hpp"

using namespace std;

void usage();

void write_file(string directory, unsigned sep, unsigned &cur_ds, unsigned &cur_dir, const vector<genom> &genomes, const vector<string> &names, const vector<unsigned> &idx){
	string fname;
	ofstream file;

	fname = directory;
	if(sep != std::numeric_limits< unsigned >::max()){
		if(cur_ds % sep == 0){
			cur_dir++;
			string dname = directory;
			dname += int2string(cur_dir);
//			cout << dname<<endl;
			mkdir(dname.c_str(), 0755);
		}
		fname += int2string(cur_dir);
	}
	fname += "/"+int2string(cur_ds);
	//~ cout << fname<<endl;

	file.open(fname.c_str(), ios::trunc);
	if (!file.is_open()){
		cout << "Error: could not open "<< fname<< endl;
		exit(1);
	}

	for(unsigned j=0; j<idx.size(); j++){
		file << ">"<<names[ idx[j] ]<<endl;
		file << genomes[ idx[j] ]<<endl;
	}
	file.close();
	cur_ds++;

}


int main(int argc, char *argv[]){
	string filename,
		directory;
//	vector<int> idx;
	counter idx;
	vector<vector<unsigned> > tuples;

	vector<genom> genomes;
	vector<string> nmap;
	vector<string> names;

	unsigned cur_ds = 0,
		cur_dir = 0,
		sep = std::numeric_limits< unsigned >::max(),
		nb = std::numeric_limits< unsigned >::max(),
		m = 3,
		cnt = 0;
	char circ = 0;

	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'd': {i++;directory = argv[i];break;}
			case 'f': {i++;filename = argv[i];break;}
			case 'n': {i++;nb=atoi(argv[i]); break;}
			case 'm': {i++;m = atoi(argv[i]); break;}
			case 's': {i++;sep=atoi(argv[i]); break;}
			default: {
				cout <<"Error: unknown parameter: -"<<argv[i][1]<<endl;
				usage();
				break;
			}
		}
	}
	if(!directory.size()){
		cout << "Error: no output directory specified" << endl;
		usage();
	}

	if (!filename.size()) {		// check if filename specified
		cout << "Error: no input file specified" << endl;
		usage();
	}else{
		read_genomes( filename, genomes, names, 0, nmap );
		//read_taxa(filename, genomes, names, circ);	// read the genom file
	}

	init_rng();

		// generate the tuples
	idx = counter( m, genomes.size(), circ, true);
	do{
//		copy(idx.begin(), idx.end(), ostream_iterator<unsigned>(cout, " ")); cout << endl;
//		if (nb != std::numeric_limits< unsigned >::max()){
			tuples.push_back( idx.get_counter() );
//		}else{
//			write_file(directory, sep, cur_ds, cur_dir, genomes, names, idx.get_counter()  );
//			cnt++;
//		}
		idx++;
	}while( idx.isvalid() );

	cout <<"spezies: "<<genomes.size()<<endl;

	if(true || nb != std::numeric_limits< unsigned >::max()){
		cout <<"tuples: " << tuples.size()<<endl;

		random_shuffle(tuples.begin(), tuples.end());
		for(unsigned i=0; i<tuples.size() && i<nb; i++){
			write_file(directory, sep, cur_ds, cur_dir, genomes, names, tuples[i]);
			cnt++;
//
//		string fname;
//		fname = directory;
//		if(sep != std::numeric_limits< unsigned >::max()){
//			if(cur_ds % sep == 0){
//				cur_dir++;
//				string dname = directory;
//				dname += int2string(cur_dir);
//				cout << dname<<endl;
//				mkdir(dname.c_str(), 0755);
//			}
//			fname += int2string(cur_dir);
//		}
//		fname += "/"+int2string(cur_ds);
//		//~ cout << fname<<endl;
//
//		file.open(fname.c_str(), ios::trunc);
//		if (!file.is_open()){
//			cout << "Error: could not open "<< fname<< endl;
//			exit(1);
//		}
//
//		for(unsigned j=0; j<m; j++){
//			file << ">"<<names[ tuples[i][j] ]<<endl;
//			file << genomes[ tuples[i][j] ]<<endl;
//		}
//		file.close();
//		cur_ds++;
		}
	}
	cout << "written: "<<cnt<<endl;

	return 1;
}


void usage(){
	cout << "construct_tripels -f file -d dir [-u] [-m] [-n nb] [-s size]"<<endl;
	cout << "constructs subsets of a certain size "<<endl;
	cout << "file: datafile"<<endl;
	cout << "dir:  output directory (do not forget /)"<<endl;
	cout << "-u: remove duplicates from given data"<<endl;
	cout << "-m: subset size (default 3)"<<endl;
	cout << "-n: output nb random triples; else all"<<endl;
	cout << "-s: split the triples in subdirectories each containing size triples"<<endl;
	exit(1);
}
