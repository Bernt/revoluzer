#include <stdlib.h>
#include <iostream>

#include "io.hpp"
#include "genom.hpp"
//#include "median.hpp"

using namespace std;

void usage(void);

int main(int argc, char *argv[]){
	int circular = 0,
		sign = 1,
		n = 0;
	vector<genom> genomes;

		// get the parameters
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'c': {circular = 1; break;}
			case 'u': {sign = 0; break;}
			case 'n': {i++; n = atoi(argv[i]); break;}
			default:{
				cout << "unknown parameter "<<argv[i]<<endl;
				usage();
				exit(1);
			}
		}
	}

	if( n < 2 ){
		cerr << "error: length < 2"<<endl;
		usage();
	}

	get_all_permutations(n, circular, sign, genomes);

	for (unsigned i=0; i<genomes.size(); i++){
		cout << "> "<<i+1<<endl;
		cout << genomes[i]<<endl;
	}
}

void usage(void){
	cerr << "usage -n LEN [-c]"<<endl;
	cerr << " -c: circular"<<endl;
	cerr << " -u: unsigned"<<endl;
	exit( EXIT_FAILURE );
}
