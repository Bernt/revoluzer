/** @file mgrResultParser.cpp
 * program to parse the html-output of mgr to extract the relevant data for test:
 * m, n, k, score difference and distance of solution
 * there is a program gc which takes this output in stdin and outputs a file
 * where score-difference and distance of solution are added for every k
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <regex.h>

#include "conserved.hpp"
#include "dstnc_inv.hpp"
#include "genom.hpp"
#include "helpers.hpp"

using namespace std;

/**
 * main foo
 */
int main(int argc, char *argv[]){
	dstnc *rdist = NULL;
	ifstream mgrOut;		// mgr outpufile
	string line,			// lines of the outputfile
		s,						// string for temporary usage
		filename;
	regex_t re_ifname,
		re_ifname_ext,
		re_pregenom,
		re_gene,
		re_error,
		re_pageend,
		re_dataset_begin;
	regmatch_t match[7];
	genom a,				// gene to read in 
		g,					// genome for temporary usage
		id;
	vector<genom> G;	// the genomes inputed to mgr
	int m = 0,			// # of genomes
		n = 0,			// # of genes per genome
		e = 0, 			// the experiment number
		k = 0,			// distance 
		r_start = 0,
		r_end = 0,
		status;
	bool realdata = false,
		printed = true;

	// regex for the inputfile line like <TR><TD>Input:</TD> <TD>sets/m03_n30/0_m3_n30_k1</TD></TR>
	status = regcomp(&re_ifname,"^<TR><TD>Input:</TD> <TD>.*/([[:digit:]]+)_m([[:digit:]]+)_n([[:digit:]]+)_k([[:digit:]]+)</TD></TR>$",REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_ifname."<<endl;
		exit(-1);
	}
	status = regcomp(&re_ifname_ext,"^<TR><TD>Input:</TD> <TD>.*/([[:digit:]]+)_m([[:digit:]]+)_n([[:digit:]]+)_k([[:digit:]]+)_w([[:digit:]]+)-([[:digit:]]+)</TD></TR>$",REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_ifname_ext."<<endl;
		exit(-1);
	}

	status = regcomp(&re_pregenom,"^<b><a NAME=\"Genome[[:digit:]]\"></a>(.*)</b> <br>$",REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_pregenom."<<endl;
		exit(-1);
	}
		// regex for 1 gene
	status = regcomp(&re_gene,"^([[:space:]]*-{0,1}[[:digit:]]+)",REG_ICASE|REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_gene."<<endl;
		exit(-1);
	}
	status = regcomp(&re_error,"^couldn't solve this triplet$",REG_ICASE|REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_error."<<endl;
		exit(-1);
	}
	status = regcomp(&re_pageend,"^</HTML>$",REG_ICASE|REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_pageend."<<endl;
		exit(-1);
	}
	status = regcomp(&re_dataset_begin,"^#",REG_ICASE|REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_dataset_begin."<<endl;
		exit(-1);
	}
	
	
		// check args
	
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'f': {i++; filename = argv[i]; break;}
			case 'r': {realdata = true; break;}
		}
	}
	
	if(filename.size() == 0){
		cout << "usage: mgrResultParser -f FILE [-r]"<<endl;
		exit(0);
	}
		// open file
	mgrOut.open(filename.c_str());
	if(!mgrOut.is_open()){
		cout << "unable to open "<<filename<<endl;
		exit(0);
	}

		// read the file linewise
	while(mgrOut){
		getline(mgrOut, line);

		if(regexec (&re_dataset_begin, &line[0], 1, &match[0], 0)==0){
			if (!printed){
				cout << "error "<<endl;
			}
			printed = false;
		}

		//~ if(regexec (&re_error, &line[0], 1, &match[0], 0)==0){
			//~ cout << "error "<<endl;
		//~ }

		// if filename line extract m,n,k
		if(regexec (&re_ifname, &line[0], 5, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			e = atoi(s.c_str());
			s.assign(line.begin()+match[2].rm_so, line.begin()+match[2].rm_eo);
			m = atoi(s.c_str());
			s.assign(line.begin()+match[3].rm_so, line.begin()+match[3].rm_eo);
			n = atoi(s.c_str());
			if( rdist != NULL )
				rdist = new dstnc_inv(n ,false);
			
			s.assign(line.begin()+match[4].rm_so, line.begin()+match[4].rm_eo);
			k = atoi(s.c_str());
		}
		if(regexec (&re_ifname_ext, &line[0], 7, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			e = atoi(s.c_str());
			s.assign(line.begin()+match[2].rm_so, line.begin()+match[2].rm_eo);
			m = atoi(s.c_str());
			s.assign(line.begin()+match[3].rm_so, line.begin()+match[3].rm_eo);
			n = atoi(s.c_str());
			if( rdist != NULL )
				rdist = new dstnc_inv(n ,false);
			s.assign(line.begin()+match[4].rm_so, line.begin()+match[4].rm_eo);
			k = atoi(s.c_str());
			s.assign(line.begin()+match[5].rm_so, line.begin()+match[5].rm_eo);
			r_start = atoi(s.c_str());
			s.assign(line.begin()+match[6].rm_so, line.begin()+match[6].rm_eo);
			r_end = atoi(s.c_str());
		}
			// if the next line is a genome
		if(regexec (&re_pregenom, &line[0], 2, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);	// in s is an 'A' if the genome is the ancestral
			getline(mgrOut, line);	// get the next line
			//~ cout << line<<endl;
			while(regexec (&re_gene, &line[0], 1, &match[0], 0)==0){
				string gene_str;
				gene_str.assign(line.begin()+match[0].rm_so, line.begin()+match[0].rm_eo);
				line = line.substr(match[0].rm_eo ,line.length());
				g.push_back(string2int(gene_str));
			}
			if(s=="A3"){
				a = g;
			}else{
				G.push_back(g);
			}
			g.clear();
		}
		if(regexec (&re_pageend, &line[0], 1, &match[0], 0)==0){
			// if all data is complete then output it and reset the data
			printed = true;
			if(G.size()!=3 || a.size() == 0){
				cout << "error"<<endl;
			}else{
				if(realdata){
					m = G.size();
					n = a.size();
				}
				id = genom(n, false);				// init idendity with new length (most of the time the same)
				
				if(realdata){
					cout << m <<" "<<n<<" "<<rdist->calcsum(a,G) <<" "<<rdist->calcsum(a,G) - (int)ceil((double)(rdist->calc(G[0],G[1]) + rdist->calc(G[0],G[2]) + rdist->calc(G[1],G[2]) )/2.0)<<" "
						<<intervalDistance(a, G)<<endl;
				}else{
					cout << m <<" "<< n <<" "<< k << " ";
					if(r_end != 0){
						cout << r_start <<" "<<r_end<<" ";
					}
					cout	<< rdist->calc(id,a) <<" "<< rdist->calcsum(a,G)-rdist->calcsum(id,G)<<" "
						<< intervalDistance(id, a) <<" "<< intervalDistance(a, G)<< " "
						<< rdist->calcsum(a,G) - (int)ceil((double)(rdist->calc(G[0],G[1]) + rdist->calc(G[0],G[2]) + rdist->calc(G[1],G[2]) )/2.0)<<endl;
				}
			}
			a.clear();
			G.clear();
		}
			
	}
	if (!printed){
		cout << "error "<<endl;
	}
	delete rdist;
	return 1;
}
