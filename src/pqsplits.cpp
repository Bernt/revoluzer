/** @file construct_tripels.cpp
 * construct from a given set of genomes all or a random selection of 
 * the possible triples
 */

#include <iostream>
#include <sstream>
#include <iterator>
#include <fstream>
#include <vector>
#include <regex.h>

#include<dirent.h>
#include<sys/dir.h>
#include<unistd.h>
#include<sys/stat.h>

#include "io.hpp"
#include "caprara.hpp"  
#include "genom.hpp"
#include "helpers.hpp"

//#define DEBUG

using namespace std;

/**
 * print usage information
 */
void usage();

vector<int> cnt_splits( const vector<vector<int> > &splits ){
	unsigned sum;
	map<vector<int>, int > cnt;
	vector<int> splitcnt;
	
	for (unsigned i=0; i<splits.size(); i++){
		sum = 0;
		for(unsigned j=0; j<splits[i].size(); j++){
			sum += splits[i][j];
		}
		
		if (sum <= 1 || sum >= 3){
			continue;
		}
		
		cnt[splits[i]] = sum;
	}
	
//	for(map<vector<int>, int >::iterator it = cnt.begin(); it != cnt.end(); it++){
//		copy( it->first.begin(), it->first.end(), ostream_iterator<int>(cout, " "));
//		cout << it->second<<endl;
//	}
	
	vector<int> split(4, 1);
//	12 / 34
	split[0] = 0; split[1] = 0; split[2] = 1; split[3] = 1;
	splitcnt.push_back( cnt[split] );
//	13 / 24
	split[0] = 0; split[1] = 1; split[2] = 0; split[3] = 1;
	splitcnt.push_back( cnt[split] );   
//	14 / 23
	split[0] = 0; split[1] = 1; split[2] = 1; split[3] = 0;
	splitcnt.push_back( cnt[split] );
	
	return splitcnt;
}

// splits <- input splits
// inf <- indices of informative splits
// uninf <- indices of uninformative splits
// conf <- pairs of conflicting splits
bool eval_splits(const vector<vector<int> > &splits, vector<int> &inf, vector<int> &uninf, vector<pair<int, int> > &conf){
	unsigned uninfcnt = 0,	// # uninformative splits 
		infcnt = 0,		// # informative splits
		confcnt = 0;		// # pairs of conflicting splits
	vector<unsigned> sum(splits.size(), 0);
	
	inf.clear();
	uninf.clear();
	conf.clear();
	
	for (unsigned i=0; i<splits.size(); i++){
		for(unsigned j=0; j<splits[i].size(); j++){
			sum[i]+=splits[i][j];
		}
		if( sum[i] <= 1 || sum[i] >= splits[i].size()-1 ){
			uninfcnt++;
			uninf.push_back(i);
		}
		else{
			infcnt++;
			inf.push_back(i);
		}
//		copy(splits[i].begin(), splits[i].end(), ostream_iterator<int>(cout, " "));cout << " -> "<<sum[i]<<endl;
	}
	

	for(unsigned i=0; i<splits.size(); i++){
		if(sum[i] <= 1 || sum[i] >= splits[i].size()-1)
			continue;
		
		for(unsigned j=i+1; j<splits.size(); j++){
			if(sum[j] <= 1 || sum[j] >= splits[j].size()-1)
				continue;
										
			if (splits[i] != splits[j]){
				confcnt++;
				conf.push_back(make_pair(i,j));
//				copy(splits[i].begin(), splits[i].end(), ostream_iterator<int>(cout, " ")); 
//				cout << " !!! ";
//				copy(splits[j].begin(), splits[j].end(), ostream_iterator<int>(cout, " ")); cout << endl;
			}
		}
	}
	
//	cout << inf << " " << uninf << " " << conf << endl;
	
	if (confcnt > 0)
		return true;
	else
		return false;
}

vector<genom> get_tuple(const vector<genom> &genomes, const vector<string> &names, const vector<unsigned> &idx){
	vector<genom> tuple;
	
	for(unsigned i=0; i<idx.size(); i++){
		tuple.push_back(genomes[idx[i]]);
	}

	return tuple;
}


void mpqtree_splits(mpqnode *node, int m, vector<vector<int> > &splits, vector<mpqnode *> &splitnodes){
//	int diff = 0;
	vector<int> split;
	for(unsigned i=0; i<node->children.size(); i++){
		mpqtree_splits(node->children[i], m, splits, splitnodes);
	}

//	if( node->type == LIN && ( node->parent == NULL || node->parent->type == LIN ) ){
//		for(int i=0; i < m; i++){
//			diff = 0;
//			if(node->parent == NULL){
////				cout << "#(+";
//				if( mpqnode_getsign(node, i, m) == DEC ){
////					cout << "-)";
//					diff=1;
//				}else{
////					cout << "+)";
//				}
//			}else{
////				if (mpqnode_getsign(node->parent, i, m) == INC)
////					cout << "#(+";
////				else
////					cout << "(-";
////				if (mpqnode_getsign(node, i, m) == INC)
////					cout << "+)";
////				else
////					cout << "-)";
//				
//				if( mpqnode_getsign(node, i, m) != mpqnode_getsign(node->parent, i, m) ){
//					diff=1;
//				}
//			}
//			split.push_back(diff);
//		}
////		cout << "["<<node->i.first << ","<<node->i.second<<"] -> ";
////		copy(split.begin(), split.end(), ostream_iterator<int>(cout, " ")); cout << endl;
//		
//		splits.push_back(split);
//		splitnodes.push_back(node);
//	}
}

void linsubsets( const vector<genom> &genomes, const vector<string> &names, vector<unsigned> &cur, vector<unsigned> &result, unsigned n, hdata &hd );

/**
 * cluster gene orders which are blocking, i.e. every pqtree (pairwise)
 * has a prime node
 */
void pqblock( const vector<genom> &genomes, int n, vector<int> &idx, vector<int> &block ){
	mpqnode *mpqroot;
	vector<genom> pr;	// a pair of genomes
	vector<unsigned> cnt;	// count how often genome at i leads to a pqtree with prime node
	
	cnt = vector<unsigned>(idx.size(), 0);

	for( unsigned i=0; i<idx.size(); i++ ){
		pr.push_back( genomes[idx[i]] );
		for( unsigned j=i+1; j<idx.size(); j++ ){
			pr.push_back( genomes[idx[j]] );

			mpqtree(pr, n, &mpqroot);
			if( mpqtree_haspnode(mpqroot) ){
				cnt[i]++;
				cnt[j]++;
			}
			mpqtree_free(mpqroot);

			pr.pop_back( );
		}
		pr.pop_back( );
	}

	for( unsigned i=0; i<idx.size(); ){
		if( cnt[i] == idx.size()-1 ){
			block.push_back( idx[i] );
			idx.erase( idx.begin()+i );
		}else{
			i++;
		}
	}
}

/**
 * given a set of genomes: 
 * determine equivalence classes of genomes which have co-linear quotient permutations
 * for the prime nodes of the pq tree of the genomes
 * 
 * @param[in] genomes the input genomes
 * @param[in] idx the indices of a subset of genomes
 * @return the splits (size == 1) if all nodes are linear or idx has size 1 
 */
vector<vector<int> > pqsplit( const vector<genom> &genomes, int n, vector<int> &idx){
	int qpermclscnt = 0;		// number of equivalence classes found for a prime node
	genom rqperm; 				// reverse quotient permutations
	map<genom,int> qpermcls;	// mapping from quotient permutations to equivalence classes
	map<genom,int>::iterator qpcit;
	mpqnode *mpqroot;	// the (root of the) mpqtree
	vector<genom> sgenomes,	// the set of genomes specified by idx
		qperm;				// the quotient permutations
	vector<mpqnode *> pnode;	// primenodes of the mpqtree
	vector<vector<int> > splits, 	// the resulting splits
		inv_sgenomes;				// inverse genomes of sgenomes
	vector<vector<vector<int> > > eclasses; // equivalence classes (i.e. splits) for each pnode 	

	cout << "pqsplit"<<endl;	
		// if there are less than 2 genomes in the set -> unsplitable
	if( idx.size() < 2 ){
		splits.push_back(idx);
		return splits;
	}
	
		// construct the genome subset and the mpqtree
	for(unsigned i=0; i<idx.size(); i++){
		sgenomes.push_back( genomes[idx[i]] ); 
	}
	mpqtree(sgenomes, n, &mpqroot);
	mpqtree_primenodes(mpqroot, pnode);

	if( pnode.size() == 0 ){
		splits.push_back( idx );
		mpqtree_free(mpqroot);
		return splits;
	} 
	
	inv_sgenomes = vector<vector<int> >(genomes.size(), vector<int>(n+1,0));
	for(unsigned j=0; j<genomes.size(); j++){
		for(unsigned k=0; k<genomes[j].size(); k++){
			inv_sgenomes[j][ abs(genomes[j][k]) ] = k;
		}
	}
	#ifdef DEBUG
	cout << pnode.size()<<" pnodes"<<endl;
	#endif//DEBUG
	for( unsigned i=0; i<pnode.size(); i++ ){
		eclasses.push_back( vector<vector<int> >() );
		
		mpqtree_quotientpermutations( pnode[i], sgenomes, n, inv_sgenomes, false, qperm );
		#ifdef DEBUG
		cout << "pnode "<< i<<endl;
		#endif//DEBUG
		
		for(unsigned j=0; j<qperm.size(); j++){
			#ifdef DEBUG
			cout << "   "<<qperm[j]<<endl;
			#endif//DEBUG
			
			qpcit = qpermcls.find(qperm[j]);
			if( qpcit == qpermcls.end() ){
				rqperm = genom( qperm[j].size(), 0 );
				for(unsigned k=0; k<qperm[j].size(); k++){
					rqperm[ rqperm.size()-1-k ] = qperm[j][k];
				}
				qpermcls[qperm[j]] = qpermclscnt;
				qpermcls[rqperm] = qpermclscnt;
				eclasses.back().push_back( vector<int>() );
				(eclasses.back())[ qpermclscnt ].push_back( idx[j] ); 
				
				#ifdef DEBUG
				cout << "   -> not found new class "<< qpermclscnt <<endl;
				cout << "      "<<qperm[j]<<endl;
				cout << "      "<<rqperm<<endl;
				#endif//DEBUG
				qpermclscnt++;
			}else{
				#ifdef DEBUG
				cout << "   -> belongs to existent class "<< qpcit->second <<endl;
				#endif//DEBUG
				(eclasses.back())[ qpcit->second ].push_back( idx[j] );
			}
		}
		qpermcls.clear();
		qpermclscnt = 0;
		#ifdef DEBUG
		cout << "equivalence classes "<<endl;
		for( unsigned j=0; j< eclasses.back().size(); j++ ){
			cout << "  "; copy( (eclasses.back())[j].begin(), (eclasses.back())[j].end(), ostream_iterator<int>(cout, " ") );cout<<endl;
		}
		#endif//DEBUG
	}

	
	//@todo check for inconsistencies in eclasses  
	mpqtree_free(mpqroot);
	return eclasses.front();
}

/**
 * @param[] genomes the input genomes
 * @param[] ie the actual include exclude 'bool' array
 *  ie[i] == 0 genomes[i] is not included, ie[i] == 1 genomes[i] is included,
 * @param[] subset
 * @param[] l recursion level
 * @param[] n
 * @param[] maxsub
 * @param[] maxss
 */
void recursive_add( const vector<genom> &genomes, vector<int> &ie,
		vector<genom> &gsubset,
		vector<unsigned> &subset, unsigned l, unsigned n,
		vector<vector<unsigned> > &maxsub,
		map<unsigned, vector<vector<unsigned> > > &maxss, unsigned &cnt
		){

	if( l>= genomes.size() ){
//		copy(ie.begin(), ie.end(), ostream_iterator<int>(cerr, " ")); cerr << endl;
		return;
	}

	for( int i=0; i<2; i++ ){
		ie[l] = i;

		bool pnode = false;
		if ( i==1 ){
			subset.push_back(l);
			gsubset.push_back(genomes[l]);

			if(subset.size() > 1){
//				vector<genom> ss;
//				for( unsigned i=0; i<subset.size(); i++ ){
//					ss.push_back( genomes[subset[i]] );
//				}

				mpqnode *mpqroot = mpqtree_construct(gsubset, n);
				pnode = mpqtree_haspnode(mpqroot);
				mpqtree_free( mpqroot );
				cnt++;
			}

			if( !pnode && subset.size() >= 3){
				if( maxsub.size()>0 && subset.size() > maxsub[0].size() ){
					maxsub.clear();
//					cerr << "max "<<maxsub.size()<<endl;
				}
				if( maxsub.size() == 0 || subset.size() == maxsub[0].size()){
					maxsub.push_back( subset );
					cerr << "max "<<maxsub.size()<<" " <<maxsub[0].size()<<" "<<cnt<<endl;
				}

				if( maxss.find( subset.size() ) == maxss.end()){
					maxss[subset.size()] = vector<vector<unsigned> >();
				}
				maxss[subset.size()].push_back( subset );
			}
		}

		if( !pnode ){
			recursive_add( genomes, ie, gsubset, subset, l+1, n, maxsub, maxss, cnt );
		}

		if ( i==1 ){
			subset.pop_back();
			gsubset.pop_back();
		}
	}
}

int main(int argc, char *argv[]) {
	int n,				// length of the genomes
		m = 0,			// number of genomes
		circular = 0;	// circular genomes (1) / linear (0)
	vector<vector<int> > q;	// queue holding (indices of) subsets to be clustered
	string fname,			// input genomes file name
		tfname; 			// input tree file name
	vector<genom> genomes, 	// input gene orders
		gsubset;
	vector<int> ie;
	vector<string> names,	// names of the input gene orders
		nmap;				// interger -> gene name map
	unsigned cnt = 0;

	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'c': {circular = 1; break;}
			case 'f': {i++; fname = argv[i]; break;}
			case 't': {i++; tfname = argv[i]; break;}
			default:{
				cout << "unknown parameter "<<argv[i]<<endl;
				usage();
				break;
			}
		}
	}
	
		// check and read input
	if(fname.size() == 0){
		cerr << "error: no file given"<<endl;
		usage();
	}
	read_genomes(fname, genomes, names, circular, nmap);
	m = genomes.size();

	if( m == 0 ){
		cerr << "error: no genomes found in "<<fname<<endl;
		usage();
	}

	if( genomes[0].size() < 2 ){
		cerr << "error: input genomes have length < 2"<<endl;
		usage();
	}
	n = genomes[0].size();
	
	// init include / exclude map
	ie = vector<int>(genomes.size(), -1);
	vector<unsigned> subset;
	vector<vector<unsigned> > maxsub;
	map<unsigned, vector<vector<unsigned> > > maxss;
	recursive_add( genomes, ie, gsubset, subset, 0, n, maxsub, maxss, cnt );

//	for( map<unsigned, vector<vector<unsigned> > >::iterator it=maxss.begin(); it!=maxss.end(); it++){
//		cerr <<endl<<"==============="<<it->first<< endl;
//		for( unsigned i=0; i<it->second.size(); i++ ){
//			for(unsigned j=0; j<it->second[i].size(); j++){
//				cout << names[it->second[i][j]]<<" ";
//			}
//			cerr <<endl<<"==============="<< endl;
//		}
//	}

	if(maxsub.size() == 0){
		cerr << "no linear subset found"<<endl;
	}else{
		cerr << maxsub[0].size()<<endl;
		for( unsigned i=0; i<maxsub.size(); i++ ){
			for(unsigned j=0; j<maxsub[i].size(); j++){
				cout << ">"<<names[maxsub[i][j]]<<endl;
				cout <<genomes[maxsub[i][j]]<<endl;
			}
			cout <<endl<<"#==============="<< endl;
		}
	}
//	pqblock( genomes, n, qt, block );
//	#ifdef DEBUG
//	cout << block.size() <<" blocking gene orders "<<endl;
//	for(unsigned i=0; i<block.size(); i++){
//		cout << ">"<<names[block[i]] << endl;
//		cout << genomes[block[i]] << endl;
//	}
//	#endif//DEBUG
//	for(unsigned i=0; i<block.size(); i++){
//		linsplits.push_back( vector<int>(1, block[i]  ) );
//		cout << ">"<<names[block[i]] << endl;
//		cout << genomes[block[i]] << endl;
//	}
//	q.push_back( qt );
//	qt.clear();
/*
		// iterate until queue is empty
	while( q.size() > 0 ){
		#ifdef DEBUG
		cout << "|q|"<< q.size() << " pop "; copy( q.back().begin(), q.back().end(), ostream_iterator<int>( cout, " " ) ); cout << endl;
		#endif//DEBUG
		qt = q.back();
		q.pop_back();

			// split the data set
		tmpsplits = pqsplit( genomes, n, qt);
		cout << "pqsplits returned "<<tmpsplits.size()<<endl;
			// if there are no prime nodes, i.e. no split: 
			// -> put the current queue top to the results 
			// otherwise: 
			// -> put the splits in the queue
		if ( tmpsplits.size() == 1 ){
			linsplits.push_back( tmpsplits[0] );
		}else{
			qt.clear();
			for( unsigned i=0; i<tmpsplits.size(); i++ ){
//				if( i>0 ){
//					for(unsigned j=0; j<tmpsplits[i].size(); j++){
////						cout << ">"<< names[ tmpsplits[i][j] ]<<endl;
//						cout << genomes[ tmpsplits[i][j] ]<<"\t"<<names[ tmpsplits[i][j] ]<<endl;
//					}
//				}
				
				if( tmpsplits[i].size() > 1 ){
					q.push_back(tmpsplits[i]);
				}else{ 
					qt.push_back(tmpsplits[i][0]);
				}
			}
			if( qt.size() > 0 ){
				q.push_back(qt);
				qt.clear();
			}
		}
		
		tmpsplits.clear();
	}
*/

/*	
	cout << "graph G{"<<endl;
	
	fgenomes = vector<vector<genom> >( fname.size() );
	fnames = vector<vector<string> >( fname.size() );
	for( unsigned i=0; i<fname.size(); i++ ){
			// read the genom filea0 -> a1 -> a2 -> a3;
		cout << "subgraph cluster"<<i<<" {"<<endl;
		for(unsigned j=0; j<fnames[i].size(); j++){
			cout << fnames[i][j]<<endl;
		}
		
		cout << "label = \""<<fname[i]<<"\";"<<endl;
		cout << "}"<<endl;
		
		genomes.insert(genomes.end(), fgenomes[i].begin(), fgenomes[i].end() );
		names.insert(names.end(), fnames[i].begin(), fnames[i].end() );
	}
	
	
	if(genomes.size() == 0){
		cout << "no genomes in the file"<<endl;
		usage();
	}
//	for(unsigned i=0; i<genomes.size(); i++)
//		cout << names[i] <<" - "<< genomes[i]<<endl;
//	for(unsigned i=0; i<nmap.size(); i++)
//		cout << i << ": "<<nmap[i]<<endl;
	
	m = genomes.size();
	n = genomes[0].size();
	init_data(hd, n, circular);
	
	vector<unsigned> cur, result;
	

	do{
		result.clear();
		linsubsets( genomes, names, cur, result, n, hd );
		if(result.size() > 0){
			for(int i=result.size()-1; i>=0; i--){
				cout << names[result[i]]  <<"--";
				genomes.erase( genomes.begin()+result[i] );
				names.erase( names.begin()+result[i] );
			}
			cout << endl;
		}

	}while( result.size() > 0 );

	cout << "}"<<endl;
	return 1;*/

/**
	if(dname == ""){
		cout << "need dirname "<<endl;
		usage();
	}
	genom front;
 	vector<genom> tuple;
	vector<unsigned> idx;
	vector<vector<int> > splits;
	vector<mpqnode *> splitnodes;
	vector<int> inf, uninf;
	vector<pair<int,int> > conf;
	int fcnt=0;
	idx = vector<unsigned>(4, 0);
	for (unsigned i=1; i<idx.size(); i++){
		idx[i] = i;
	}
	
	do{
		int i; 

		tuple = get_tuple(genomes, names, idx);
		mpqroot = mpqtree_construct(tuple, n, hd);
//		mpqtree_print(mpqroot, 4);
		
		if (!mpqtree_haspnode(mpqroot)){
			
//			for (unsigned i=0; i<tuple.size(); i++){
//				cout << ">" <<i<<endl;
//				print_genom(cout, tuple[i], nmap); cout << endl;
//			}
			
			mpqtree_splits(mpqroot, 4, splits, splitnodes );
			eval_splits(splits, inf, uninf, conf);
			cout << inf.size()<<" " <<uninf.size()<<" " <<conf.size()<<endl;
			if(conf.size() > 0){
				string ofname = dname+int2string(fcnt);
				fcnt++;
				ofstream of(ofname.c_str());
				
				for(unsigned i=0; i<inf.size(); i++){
					of << "#inf ";copy(splits[inf[i]].begin(), splits[inf[i]].end(), ostream_iterator<int>(of, " ")); of << endl;
				}
				for(unsigned i=0; i<uninf.size(); i++){
					of << "#uninf ";copy(splits[uninf[i]].begin(), splits[uninf[i]].end(), ostream_iterator<int>(of, " ")); of << endl;
				}
				for(unsigned i=0; i<conf.size(); i++){
					of << "#conf ";
					copy(splits[conf[i].first].begin(), splits[conf[i].first].end(), ostream_iterator<int>(of, " "));
					mpqtree_front( splitnodes[conf[i].first], tuple[0], front );
					of << "(";print_genom(of, front, nmap);of << ") $\\leftrightarrow$ ";
					front.clear();
					copy(splits[conf[i].second].begin(), splits[conf[i].second].end(), ostream_iterator<int>(of, " "));
					mpqtree_front( splitnodes[conf[i].second], tuple[0], front );
					of <<"("; print_genom(of, front, nmap);of << ")";
					front.clear();
					of << endl;
				}
				
				for(unsigned i=0; i<tuple.size(); i++){
					of << "> "<<names[idx[i]]<<endl;
					print_genom(of, tuple[i], nmap);of << endl;
				}
				of.close();
			}
		}
		mpqtree_free(mpqroot);
		splits.clear();
		splitnodes.clear();
		tuple.clear();


		if(idx[0] >= genomes.size()-(idx.size()))
			break;
		
		idx[ idx.size()-1 ]++;
		for(i=idx.size()-1; i>=0; i--){
			if( idx[i]<genomes.size()-( idx.size()-(i+1) ) ){
				break;
			}else if( i>0 ){
				idx[i-1]++;
			}
		}
		for( unsigned k=i+1; k<idx.size(); k++ )
			idx[k] = idx[k-1]+1;
		
		
	}while(idx[0] < genomes.size()-(idx.size()-1) );
*/


/** CODE FOR GETTING THE NUMBER OF INFORMATIVE, UNINFORMATIVE, CONFLICTING SPLITS
 	vector<genom> tuple;
	vector<unsigned> idx;
	vector<vector<int> > splits;
	idx = vector<unsigned>(4, 0);
	for (unsigned i=1; i<idx.size(); i++){
		idx[i] = i;
	}
	do{
		int i; 

		tuple = get_tuple(genomes, names, idx);
		mpqroot = mpqtree_construct(tuple, n, hd);
		if (!mpqtree_haspnode(mpqroot)){
			mpqtree_splits(mpqroot, 4, splits );
			eval_splits(splits);
//			if (eval_splits(splits) ){
//				for(unsigned i=0; i<splits.size(); i++){
//					copy(splits[i].begin(), splits[i].end(), ostream_iterator<int>(cout, " ")); cout << endl;
//				}
//				cout << tuple << endl;
//				exit(1);
//			}
		}
		mpqtree_free(mpqroot);
		splits.clear();
		tuple.clear();


		if(idx[0] >= genomes.size()-(idx.size()))
			break;
		
		idx[ idx.size()-1 ]++;
		for(i=idx.size()-1; i>=0; i--){
			if( idx[i]<genomes.size()-( idx.size()-(i+1) ) ){
				break;
			}else if( i>0 ){
				idx[i-1]++;
			}
		}
		for( unsigned k=i+1; k<idx.size(); k++ )
			idx[k] = idx[k-1]+1;
		
		
	}while(idx[0] < genomes.size()-(idx.size()-1) );
*/


/** CODE FOR PRINTING QNET FILE  
	cout << "taxanumber:  "<<genomes.size()<<";"<<endl;
	cout << "description: artificial data;"<<endl;
 	cout << "sense: max;"<<endl;
 	for ( unsigned i=0; i<names.size(); i++ ){
 		cout << "taxon:   "<<i+1<<"    name: "<<names[i]<<";"<<endl;
 	}

	vector<genom> tuple;
	vector<unsigned> idx;
	vector<vector<int> > splits;
	vector<int> splitcnt;
		// generate the triples
	idx = vector<unsigned>(4, 0);
	for (unsigned i=1; i<idx.size(); i++){
		idx[i] = i;
	}
	do{
		int i; 

		tuple = get_tuple(genomes, names, idx);
		mpqroot = mpqtree_construct(tuple, n, hd);
//		if (!mpqtree_haspnode(mpqroot)){
//			cout << "lin"<<endl;
			mpqtree_splits(mpqroot, 4, splits );
			splitcnt = cnt_splits(splits);
			cout << "quartet: ";
			for(unsigned i=0; i<idx.size(); i++){
				cout << idx[i]+1<<" ";
			}
			cout << "weights: ";
			for (unsigned i=0; i<splitcnt.size(); i++){
				cout << splitcnt[i]<<" ";
			}
			cout << ";"<<endl;
			

//			if (eval_splits(splits) ){
//				for(unsigned i=0; i<splits.size(); i++){
//					copy(splits[i].begin(), splits[i].end(), ostream_iterator<int>(cout, " ")); cout << endl;
//				}
//				cout << tuple << endl;
//				exit(1);
//			}
//		}
//		else{
//			cout << "pri"<<endl;
//		}
		mpqtree_free(mpqroot);
		splits.clear();
		tuple.clear();


		if(idx[0] >= genomes.size()-(idx.size()))
			break;
		
		idx[ idx.size()-1 ]++;
		for(i=idx.size()-1; i>=0; i--){
			if( idx[i]<genomes.size()-( idx.size()-(i+1) ) ){
				break;
			}else if( i>0 ){
				idx[i-1]++;
			}
		}
		for( unsigned k=i+1; k<idx.size(); k++ )
			idx[k] = idx[k-1]+1;
		
		
	}while(idx[0] < genomes.size()-(idx.size()-1) );
*/

	
/*	CODE FOR GETTING THE NUMBER OF K-TUPLES AND THE NUMBER OF SPECIES INCLUDED IN K-TUPLES*/
/*	int t = 0; 			// tupel size
	vector<genom> tuple;
	
	vector<unsigned> idx;
	vector<vector<unsigned> > idxs, 
		nextidxs; 
	vector<bool> incspec;	// species included in p node free pqtree tuple
	unsigned incspeccnt, 	// .. and their number
		ltupcnt;			// number of pnode free tuples

	incspec = vector<bool>(genomes.size(), false);
	
		// generate the triples
	idx = vector<unsigned>(3, 0);
	for (unsigned i=1; i<idx.size(); i++){
		idx[i] = i;
	}
	do{
		int i; 
		idxs.push_back(idx);
//		copy(idx.begin(), idx.end(), ostream_iterator<unsigned>(cout, " ")); cout << endl;

		if(idx[0] >= genomes.size()-(idx.size()))
			break;

		idx[ idx.size()-1 ]++;
		for(i=idx.size()-1; i>=0; i--){
			if( idx[i]<genomes.size()-( idx.size()-(i+1) ) ){
				break;
			}else if( i>0 ){
				idx[i-1]++;
			}
		}
		for( unsigned k=i+1; k<idx.size(); k++ )
			idx[k] = idx[k-1]+1;
	}while( idx[0] < genomes.size()-(idx.size()-1) );
	
	t = 3;
	do{
		incspec.assign(genomes.size(), false);
		incspeccnt = 0;
		ltupcnt = 0;

			// remove the t-tuples which have p nodes in their pqtrees
		for(unsigned i=0; i<idxs.size(); i++){
			tuple = get_tuple(genomes, names, idxs[i]);
			mpqroot = mpqtree_construct(tuple, n, hd);
			if (mpqtree_haspnode(mpqroot)){
				idxs[i].clear();
			}else{
				for(unsigned j=0; j<idxs[i].size(); j++){
					incspec[ idxs[i][j] ] = true;
				}
				ltupcnt++; 
			}

			mpqtree_free(mpqroot);
			tuple.clear();

		}
		
		for (unsigned i=0; i<incspec.size(); i++){
			if ( incspec[i] ){ 
				incspeccnt++;
				// TODO write to file
			}
		}
		
		if ( incspeccnt == 0 ){
			break;
		}
		
		cerr <<t <<" "<< ltupcnt<< " "<<incspeccnt<<" "<<endl;
		
			// get the t+1 tuples
		for(unsigned i=0; i<idxs.size(); i++){
			if(idxs[i].size() == 0){
				continue;
			}
			idx = idxs[i];
			idx.push_back(0);
			for(unsigned j=idx[t-1]+1; j<genomes.size(); j++){
				idx[t] = j;
				nextidxs.push_back(idx);
			}
		}
		
		idxs.clear();
		idxs = nextidxs;
		nextidxs.clear();
	
		t++;
	}while(1);
	
	genomes.clear();
	names.clear();
	nmap.clear();
	free_data(hd);
	*/
}

void linsubsets( const vector<genom> &genomes, const vector<string> &names, vector<unsigned> &cur, vector<unsigned> &result, unsigned n, hdata &hd ){
	unsigned max = 0;
	bool ext = false;
	vector<genom> tuple;
	mpqnode *mpqroot = NULL;

	if( cur.size() > 0 ){
		max = cur.back();
	}
	for( unsigned i=max+1; i<genomes.size(); i++ ){
		cur.push_back(i);
		if(cur.size() >= 3){
			tuple = get_tuple(genomes, names, cur);
			mpqroot = mpqtree_construct(tuple, n);
			if ( !mpqtree_haspnode(mpqroot) ){
				linsubsets(genomes, names, cur, result, n, hd);
				ext = true;
			}
			mpqtree_free(mpqroot);
			tuple.clear();
		}else{
			ext = true;
			linsubsets(genomes, names, cur, result, n, hd);
		}
		cur.pop_back();
	}
	
	if ( !ext && cur.size() >= 3 && cur.size() > result.size() ){
		result = cur;
	}
}

void usage(){
	cerr << "no usage info not implemented yet"<<endl;
	exit(1);
}
