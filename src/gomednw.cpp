/*
 * mednw.cpp
 *
 *  Created on: May 18, 2010
 *      Author: maze
 */

#include <cstdlib>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <iterator>
#include <set>
#include <vector>

//#include <claw/trie.hpp>

#include "counter.hpp"
#include "dstnc_crex.hpp"
#include "distmat.hpp"
#include "io.hpp"
#include "helpers.hpp"
#include "chrctr/ccc.hpp"
#include "chrctr/chrctr.hpp"
#include "chrctr/mednw.hpp"
#include "geneset.hpp"
#include "geneset-chrctr.hpp"
#include "pqrtree.hpp"

using namespace std;

#define DEFAULT_MAXSIZE std::numeric_limits< unsigned >::max()// default maximum set size
#define DEFAULT_ADJACENCY false	// default for the use of specialized adjacency methods
#define DEFAULT_CIRCULAR true	// default circularity:true (linear:false)
#define DEFAULT_DIRECTED false  // default for directed:false (undirected:true)
#define DEFAULT_SGN_HANDLING DOUBLE	// default sign handling option
#define DEFAULT_ONLINE true	// default online
#define DEFAULT_NMISS 0 // default number of allowed missing neg constraints
#define DEFAULT_PMISS std::numeric_limits< unsigned >::max() // default percentage of allowed missing neg constraints

#define DEBUG_MEDNWCD

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
class ccc_adj : public ccc<T>{
private:
	bool _circular;	/* the circularity of the genomes */
	bool _directed;
	/**
	 * for each node [0:n]:
	 * - UINT_MAX iff it is an inner node
	 * - the index of the component for end points
	 */
	vector<unsigned> _cmp;

	/**
	 * for each component [0:n]
	 * - the two end points
	 * - UINT_MAX if no such component exists
	 **/
	vector<vector<unsigned> > _idx;

	unsigned _ccnt;	/* component count */

	vector<string> *_nmap; /* gene name map */
public:
	/**
	 * default constructor
	 */
	ccc_adj(){};

	/**
	 * set up an consistency checker for adjacencies
	 * for genomes of length n
	 * @param[in] n the length of the genomes
	 * @param[in] circular the circularity of the genomes
	 * @param[in] directed directed genomes, i.e. framed by 0 and n+1
	 * @param[in] minobs the minimum number of observations to be achieved
	 * @param[in] nmap gene name map
	 */
	ccc_adj( unsigned n, bool circular, bool directed, unsigned minobs,
			vector<string> *nmap  );

	/**
	 * destructor
	 */
	virtual ~ccc_adj(){};

private:
	/**
	 * add a new adjacency
	 * @param[in] pair an adjacency
	 * @return true if the addition of the pair allows
	 * for a linear/circular ordering
	 */
	bool add( const T &con );

public:
	ccc<T>* clone()const;

	unsigned cntconsistent() const;

	/**
	 * get one consistent permutation
	 * @return the a permutation
	 */
	genom getpermutation() const;

	/**
	 * get all consistent permutation
	 * @return the a permutation
	 */
	vector<genom> getpermutations() const;

	/**
	 * get the number of connected components
	 * @return number of connected component
	 */
	unsigned nrcmp( ) const;

	/**
	 * the output function of the adjacency consistency checker
	 * @param[in,out] the stream to write into
	 * @return the written stream
	 */
	ostream &output( ostream &os )const;
};

template<class T>
class ccc_interval : public ccc<T>{
private:
	bool _circular,	/* circularity */
		_directed;
	pqrtree<int> _pqr;	/* a pqrtree */
	SgnHandType _sgnhand;	/* sign handling (DOUBLE/UNSIGN) */
	unsigned _n;	/* the length of the genomes */
	vector<string> *_nmap; /* gene name map */
public:
	/**
	 * default constructor
	 */
	ccc_interval() : _nmap (NULL) {};

	/**
	 * set up an consistency checker for intervals
	 * for genomes of length n
	 * @param[in] n the length of the genomes
	 * @param[in] circular the circularity of the genomes
	 * @param[in] directed or undirected
	 * @param[in] signhand signhandling option
	 * @param[in] minobs the minimum number of observation to be achieved
	 * @param[in] nmap a gene name map
	 */
	ccc_interval( unsigned n, bool circular, bool directed,
			SgnHandType signhand, unsigned minobs, vector<string> *nmap );

	/**
	 * destructor
	 */
	virtual ~ccc_interval(){};

private:
	/**
	 * add a new adjacency
	 * @param[in] con a new interval
	 * @return true if the addition of the interval allows
	 * for a linear/circular ordering
	 */
	bool add( const T &con );

public:
	virtual ccc<T>* clone()const;

	/**
	 * determine the number of permutations consistent with the
	 * current constraints
	 * @return the number of consistent permutations
	 */
	unsigned cntconsistent() const;

	/**
	 * get a consistent permutation
	 * @return a consistent permutation
	 */
	genom getpermutation() const;

	/**
	 * get all consistent permutation
	 * @return a consistent permutation
	 */
	vector<genom> getpermutations() const;


	/**
	 * the output function of the interval consistency checker
	 * @param[in,out] the stream to write into
	 * @return the written stream
	 */
	ostream &output( ostream &os )const;
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
ccc_adj<T>::ccc_adj( unsigned n, bool circular, bool directed,
		unsigned minobs, vector<string> *nmap  ) : ccc<T>( minobs ){

	_circular = circular;
	_directed = directed;
	_nmap = nmap;

	//	cerr << "ccc_adj("<< n<<","<<circular <<")"<<endl;

	// reserve memory for the components and the indices
	// in the directed case permutations of the elements {0,...,n'}
	// are to be handled and {1,...,n'} otherwise
	// as the parameter n is the length of the permutations it is
	// n'-1 in the undirected case. but one element more for 0
	// (even if it is not used) is necessary
	_cmp = vector<unsigned>( (directed)?n:n+1, 0 );
	_idx = vector<vector<unsigned> >( _cmp.size() );
	for( unsigned i=0; i< _cmp.size(); i++ ){
		_cmp[ i ] = i;
		_idx[ i ] = vector<unsigned>( 2, i );
	}

	// number of components = number of elements, i.e. each element
	// is its own component
	_ccnt = n;

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
bool ccc_adj<T>::add( const T &con ){
	unsigned u = *(con.begin()),
		v = *(++(con.begin()));

//	cerr << "add("; print_element(u, cerr, 1, "", _nmap);cerr<<"="<<u<<",";
//	print_element(v, cerr, 1, "", _nmap);cerr<<"="<<v<<")"<< endl;
//	cerr << *this << endl;

	// in order to allow for a linear/circular ordering
	// new connections can only be made between the end points of
	// different already existing path
	// for circular genomes allow closing the cycle, but only if only
	// one component is left
//	cerr << "cmpv "<< _cmp[u] << " cmpu "<< _cmp[v] <<endl;
	if( _cmp[u] == std::numeric_limits< unsigned >::max() || _cmp[v] == std::numeric_limits< unsigned >::max() ||
			( _cmp[u]==_cmp[v] && !(_circular && _ccnt==1) ) ){

//		cerr << "false"<<endl;
//		cerr << "==> "<< *this << endl;
		return false;
	}

	unsigned cmpu = _cmp[u],	// the component index of u
		cmpv = _cmp[v],			// .. and v
		up,						// the other endpoint of the component
		vp;						// .. including u and v

	// determine the other end point of the component including v
	vp = (_idx[ cmpv ][0] == v) ? _idx[ cmpv ][1] : _idx[ cmpv ][0];

	// determine the other end point of the component including u
	up = (_idx[ cmpu ][0] == u) ? _idx[ cmpu ][1] : _idx[ cmpu ][0];

//	cerr <<"u'="<<up<<" v'="<<vp<<endl;

	// the other endpoint of the path starting at v (i.e. vp) becomes
	// the same component index as u
	_cmp[ vp ] = cmpu;

	// the nodes u and v become inner nodes (take care of paths consisting
	// of a single node)
	if( v != vp ){
		_cmp[v] = std::numeric_limits< unsigned >::max();
	}
	if( u != up ){
		_cmp[u] = std::numeric_limits< unsigned >::max();
	}
//	cerr <<"cmp u"<<cmpu<<" v"<<cmpv<<endl;

	// component v is erased from the index
	_idx[ cmpv ][0] = std::numeric_limits< unsigned >::max();
	_idx[ cmpv ][1] = std::numeric_limits< unsigned >::max();

	// the start and end index of component u are updated
	// (up is just set again to avoid further ifs)
	_idx[ cmpu ][0] = up;
	_idx[ cmpu ][1] = vp;

	// the number of components is reduced by 1
	_ccnt--;

	// in the directed case it is not allowed that 0 and n+1
	// are connected in one path if not all elements are included
	// in that path (i.e. one component)
	if( _directed && _cmp[0] == _cmp.back() && _ccnt != 1 ){
		return false;
	}

	//	cerr << "==> "<< *this << endl;
	return true;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template< class T >
ccc<T>* ccc_adj<T>::clone( )const{
	return new ccc_adj<T>(*this);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
unsigned ccc_adj<T>::cntconsistent() const{
	// @todo
	cerr << "cnt consistent not implemented for ccc_adj"<<endl;
	exit(EXIT_FAILURE);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
genom ccc_adj<T>::getpermutation() const{
	return genom();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
vector<genom> ccc_adj<T>::getpermutations() const {
	return vector<genom>();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


template< class T >
unsigned ccc_adj<T>::nrcmp() const{
	unsigned cmpcnt = 0;

	for( unsigned i=0; i<_idx.size(); i++ ){
		if (_idx[i][0] != std::numeric_limits< unsigned >::max() ){
			cmpcnt++;
		}
	}

	return cmpcnt;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template<class T>
ostream &ccc_adj<T>::output( ostream &os ) const{
	os << _ccnt << " cmp ";
	copy(_cmp.begin(), _cmp.end(), ostream_iterator<unsigned>(os, " "));
	os<<endl;
	os << "idx ";
	for(unsigned i=0; i<_idx.size(); i++){
		os << _idx[i][0]<<" ";
	}os<< endl;
	for(unsigned i=0; i<_idx.size(); i++){
		os << _idx[i][0]<<" ";
	}os<< endl;

//	copy(_idx[0].begin(), _idx[0].end(), ostream_iterator<unsigned>(os, " "));
//	os<<endl;
//	copy(_idx[1].begin(), _idx[1].end(), ostream_iterator<unsigned>(os, " "));
//	os<<endl;
	return os;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template< class T >
ccc_interval<T>::ccc_interval(unsigned n, bool circular, bool directed,
		SgnHandType sgnhand, unsigned minobs, vector<string> *nmap ) : ccc<T>( minobs ){

	set<int> lf;

	_circular = circular;
	_directed = directed;
	_n = n;
	_nmap = nmap;
	_sgnhand = sgnhand;

	if( circular ){
		_pqr = pqrtree<int>(1, _n);

		// 1 is basically disconnected. for an properly interpretable
		// result pair 1 and 2 in the double case
		if( sgnhand == DOUBLE ){
			_pqr.reduce(1, 2);
		}
	}else{
		if( directed ){
			_pqr = pqrtree<int>(0, _n-1);

			// reduce the pqr tree with [0..n-2] and [1..n-1]
			// in order to make the root node linear with first
			// child 0 and last child n
			if( _sgnhand == DOUBLE ){
				for(unsigned i=0; i<=n-2; i++){lf.insert(i);}
				_pqr.reduce(lf.begin(), lf.end());
				lf.clear();
				for(unsigned i=1; i<=n-1; i++){lf.insert(i);}
				_pqr.reduce(lf.begin(), lf.end());
				lf.clear();
			}
		}else{
			_pqr = pqrtree<int>(1, _n);
		}
	}

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template< class T >
bool ccc_interval<T>::add( const T &con ){

//	cerr <<"add "<< con <<endl;
	bool res = _pqr.reduce( con.begin(), con.end() );
//	cerr << res<< " "<< _pqr<<endl;
//	cerr << "++++++++++++++++++++++++++++++++++" <<endl;
	return res;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
ccc<T>* ccc_interval<T>::clone( )const{
	return new ccc_interval<T>(*this);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
unsigned ccc_interval<T>::cntconsistent() const{
	return _pqr.permutations_count();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template< class T >
genom ccc_interval<T>::getpermutation() const{
	genom g;

	pqrtree<int> tmp = _pqr;
	tmp.normalise();
	if( _sgnhand == DOUBLE ){
		tmp.collapse(2*_n+1);
	}
	g = tmp.permutation();
	g.set_nmap( _nmap );
	if( ! _circular && g[0] != 0 ){
		reverse( g.chromosom.begin(), g.chromosom.end() );
	}
	return g;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template< class T >
vector<genom> ccc_interval<T>::getpermutations() const {
	vector<vector<int> > gv;
	vector<genom> g;

	pqrtree<int> tmp = _pqr;
	tmp.normalise();
	if( _sgnhand == DOUBLE ){
		tmp.collapse(2*_n+1);
	}
	tmp.permutations( gv, _circular || _directed );

	for(unsigned i = 0; i<gv.size(); i++){
		g.push_back(genom(gv[i], 0));
		g[i].set_nmap( _nmap );
		if( (! _circular && g[i][0] != 0) || (_circular && g[i][0] != 1 ) ){
			reverse( g[i].chromosom.begin(), g[i].chromosom.end() );
		}
	}
	return g;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
ostream &ccc_interval<T>::output( ostream &os ) const{
	os << "ccc_interval "<< this->getobs()<<" "<< cntconsistent() <<" ";
	pqrtree<int> tmp = _pqr;
	cout << _pqr <<endl;

	tmp.normalise();
	if( _sgnhand == DOUBLE ){
		tmp.collapse(2*_n+1);
	}
	tmp.named_output( *_nmap, os );

	return os;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T> class mednw;

template<typename T>
class mednwnd{
private:

	vector< mednwnd* > _nb;			/* neighbours */
	vector< vector<unsigned> > _dm;	/* change dimension(s) for the neighbours */
	ccc<T> *_cc;					/* consistency checker */

	/**
	 * add a new neighbour the edge corresponds to dimension index k
	 * @param[in] k the dimension index
	 * @param[in] nb the neighboud
	 */
	void addnb( unsigned k, mednwnd<T> *nb );
public:
	/**
	 * copy constructor
	 */
	mednwnd( const mednwnd<T> &other );

	/**
	 * constructor
	 * @param[in] cc the consistency checker to use (memory is from now on
	 * handled by the node, i.e. the memory is freed by the destructor)
	 */
	mednwnd( ccc<T> *cc );

	/**
	 * destructor
	 */
	~mednwnd();

	void apply( const mednwnd<T> *tgt, vector<bool> &sts );

	/**
	 * the new node is marked (with 1)
	 * @param[in] k the index of the dimension corresponding to the created edge
	 */
	void duplicate( unsigned k );

	friend class mednw<T>;
};

template<class T>
mednwnd<T>::mednwnd( const mednwnd<T> &other ){
	_nb = other._nb;
	_dm = other._dm;
	if( other._cc != NULL )
		_cc = other._cc->clone();
	else
		_cc = NULL;
}

template<typename T>
mednwnd<T>::mednwnd( ccc<T> *cc ){
	_cc = cc;
}

template<typename T>
mednwnd<T>::~mednwnd( ){
	if( _cc != NULL )
		delete _cc;
}

template<typename T>
void mednwnd<T>::addnb( unsigned k, mednwnd<T> *nb ){
	_nb.push_back( nb );
	_dm.push_back( vector<unsigned>(1, k) );
}


template<typename T>
void mednwnd<T>::apply( const mednwnd<T> *tgt, vector<bool> &sts ){
	for(unsigned i=0; i<_nb.size(); i++){
		if( _nb[i] != tgt )
			continue;
		for( unsigned j=0; j<_dm[i].size(); j++ ){
			sts[ _dm[i][j] ] != sts[ _dm[i][j] ];
		}
	}

	cerr << "mednwnd::apply called for non neighbour nodes"<<endl;
	exit( EXIT_FAILURE );
}

template<typename T>
void mednwnd<T>::duplicate( unsigned k ){
	mednwnd<T> *nn;	// the new node

	// TODO mark node and its duplicate
	// copy the node incl. all out edges
	nn = new mednwnd<T>( *this );
	// connect the two nodes
	this->addnb( k, nn );
	nn->addnb( k, this );

	// check if edges need to be relocated
	// TODO
	// iterate over the out edges of the new node (except the
	// one that was just added)
	for( unsigned i=0; i<nn->_nb.size()-1; i++ ){
		mednwnd<T> *y = nn->_nb[i];
		for( unsigned j=0; j<y->_dm.size(); y++ ){
			if( find( y->_dm[j].begin(), y->_dm[j].end(), k) == y->_dm[j].end()){
				continue;
			}
			nn->_nb[i] = y->_nb[ j ]; break;
		}
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
class mednw{
	mednwnd<T> *_null;	// the starting node
	vector<chrctr<T> > _data;
public:
	class forward_iterator : public iterator<forward_iterator_tag, pair<mednwnd<T> *,vector<bool> >  >{
	private:
		friend class mednw<T>;

		pair<mednwnd<T> *, vector<bool> > _cur;	/* current node states */
		stack<mednwnd<T> *> _trc;				/* call stack */
		set<mednwnd<T> *> _vst;					/* set of already visited nodes */

		/**
		 * constructor creating an empty stack
		 * current iterator is initialised to NULL and all-zero states
		 * serves as end
		 */
		forward_iterator( );

		/**
		 * constructor creating an empty stack
		 * current iterator is initialised to the given pointer and all-zero
		 * states serves as end
		 * @param[in] s the initial mednwnd pointer on the stack
		 * @param[in] nstates a number at least as big as the number of chararacters
		 *  represented in the network. if bigger then the unaffected stay false.
		 */
		forward_iterator( mednwnd<T> *s, unsigned nchar);
	public:
		forward_iterator &operator++();
		forward_iterator  operator++( int n );
		bool operator==(forward_iterator const &other) const;
		bool operator!=(forward_iterator const &other) const;
		pair<mednwnd<T> *,vector<bool> > &operator*();
//		bool operator<(forward_iterator const &other) const;
//		forward_iterator const operator+(int step) const;
//		forward_iterator &operator+=(int step); // increment over ‘n’ steps
		pair<mednwnd<T> *,vector<bool> > *operator->();// access the fields of the
										// struct an iterator points
										// to. E.g., it->length()
	};

	/**
	 * begin iterator for the iteration through the nodes of the median network
	 * - stack is initialised to the _null pointer and
	 * - the states pointer is initialised to all-zero
	 */
	forward_iterator begin();

	/**
	 * end iterator for the iteration through the nodes of the median network
	 * - stack is initialised to an empty stack and
	 * - the states pointer is initialised to all-zero
	 */
	forward_iterator end();

	/**
	 * constructor
	 * @param[in,out] characters (order might be changed
	 * @param[in] cc the consistency checker to use
	 * @param[in] online apply the consistency checker online
	 */
	mednw( vector<chrctr<T> > &chars, const ccc<T> *cc, bool online );
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
mednw<T>::mednw( vector<chrctr<T> > &chars, const ccc<T> *cc, bool online ){
	ccc<T> *tmpcc = NULL;	// for copying the consistency checker
	unsigned k=0;			// iteration counter of the algorithm
	// for a given split A|B the sets A_i and B_i give the set of characters
	// (0/1) found in the data matrix at column i in rows corresponding to rows
	// in A (resp. B). the sets A_i and B_i are represented here as bool vector
	// of length 2: the first entry of A_i is true iff 0\in A_i and the
	// second if 1\in A_i (same for B_i)
	vector<vector<bool > > A, B;
	mednw<T>::forward_iterator it;			// pointer for iteration of through the network
	vector<bool> st;		// states of it

	// sort the data in order to speed up computation
	sort( chars.begin(), chars.end(), cmpsplitsize<T> );
	_data = chars;

	cerr << "data"<<endl;
	for( unsigned i=0; i<_data.size(); i++ ){
		cerr <<
//				_toadd[i]<<" " <<
				_data[i] << endl;
	}

	// init the median network to one node
	// and init the consistency checker if online
	if( cc != NULL ){
		_null = new mednwnd<T>( cc->clone());
	}else{
		_null = new mednwnd<T>( NULL );
	}

//	_nodes.insert(_nodes.begin(), vector<bool>(chars.size()));
//	if( online ){ _ndcc.insert(_ndcc.begin(), cc->clone());}

	// check first column for constant. if so then set the first
	// column in the node of the median network
	bool constant = _data[0].isconstant();
#ifdef DEBUG_MEDNWCD
	if( constant ){
		cerr << "constant 1st row: init graph with one node labeled by "<<_data[0][0]<<endl;
	}else{
		cerr << "init graph as usual with one node with an empty label"<<endl;
	}
#endif//DEBUG_MEDNWCD

	if( constant ){
		if( _null->_cc != NULL ){ _null->_cc->add( _data[0], _data[0][0], 0 );}
		k = 1;
	}else{
		k = 0;
	}

	// init A and B
	A = vector<vector<bool> >( _data.size(), vector<bool>(2, false) );
	B = vector<vector<bool> >( _data.size(), vector<bool>(2, false) );

	st = vector<bool>( _data.size(), false );

	// iterate
	for( ; k<_data.size(); k++ ){
#ifdef DEBUG_MEDNWCD
		cerr << "### k = "<<k<< " ### ";
		cerr <<_data[k]<<endl;
#endif//DEBUG_MEDNWCD

		// compute A_i and B_i for i\in [0:k-1]
		for( unsigned i=0; i<k; i++ ){
			// reset first
			A[i][0] = false; A[i][1] = false;
			B[i][0] = false; B[i][1] = false;
			// compute
			for( unsigned j=0; j<_data[k].size(); j++ ){
				if( _data[k][j] ){
					B[i][ _data[i][j] ] = true;
				}else{
					A[i][ _data[i][j] ] = true;
				}
			}
		}

#ifdef DEBUG_MEDNWCD
		cerr << "A";
		for( unsigned i=0; i<k; i++ ){
			if( A[i][0] && A[i][0] ){cerr << "*";}
			else if( A[i][0] && !A[i][0]  ){cerr << "0";}
			else if( !A[i][0] && A[i][0] ){cerr << "1";}
			else{cout << "?";}
		}
		cerr << endl;
		cerr << "B";
		for( unsigned i=0; i<k; i++ ){
			if( B[i][0] && B[i][0]  ){cerr  << "*";}
			else if( B[i][0] && !B[i][0]  ){cerr  << "0";}
			else if( !B[i][0] && B[i][0]  ){cerr  << "1";}
			else{cout << "?";}
		}
		cerr << endl;
#endif//DEBUG_MEDNWCD

		// iterate over all nodes and
		// A) decide if the node is in [A], [B], or both
		//    (indicated by the flags a and b)
		// B) set the k-th character of the nodes and duplicate
		//    nodes appropriately

//		typename list<ccc<T> *>::iterator jt = _ndcc.begin();
//		list<vector<bool> >::iterator it = _nodes.begin();

		for( typename mednw<T>::forward_iterator it = begin(); it!= end(); it++ ){
			// A)
			bool a = true,
				b = true;

			for( unsigned j=0; j<k; j++ ){
				if(  ! A[j][ st[j] ] ){
					a = false;
					break;
				}
			}

			for( unsigned j=0; j<k; j++ ){
				if(  ! B[j][ st[j] ]  ){
					b = false;
					break;
				}
			}

#ifdef DEBUG_MEDNWCD
			cerr << "node ";
			for( unsigned j=0; j<k; j++){cout << st[j];}
			if( a && b ){cerr << " ab"<<endl;}
			else if( a && !b ){cerr << " a"<<endl;}
			else if( !a && b ){cerr << " b"<<endl;}
			else{cerr<< "??"<<endl;}
#endif//DEBUG_MEDNWCD

			// B)
			// in online mode check if appending 0 or 1 is valid
			// and modify a and b accordingly

//			if( online && it->_cc != NULL ){
//				bool aval = true, bval = true;
//				if( a && b ){
//					tmpcc = it->_cc->clone();
//					if( ! (it->_cc)->add( _data[k], false, _toadd[k] ) ){
//						a = aval = false;
//					}
//					if( ! tmpcc->add( _data[k], true, _toadd[k] ) ){
//						b = bval = false;
//						delete tmpcc;
//					}
//					if( a && b ){
////						_ndcc.insert( jt, tmpcc );
//					}else if( !a && b ){
//						delete it->_cc;
//						it->_cc = tmpcc;
//					}
//				}else if( a && !b ){
//					if( ! (it->_cc)->add( _data[k], false, _toadd[k] ) )
//						a = aval = false;
//				}else if ( !a && b ){
//					if( ! (it->_cc)->add( _data[k], true, _toadd[k] ) )
//						b = bval = false;
//				}
//
//				if( !aval || !bval ){
//					cout << "# invalid ";
//					copy(it->_states.begin(), it->_states.begin()+(k-1), ostream_iterator<bool>(cout,""));
//				}
//				if( !aval && !bval ) cout<<"**"<<endl;
//				if( !aval && bval ) cout<<"0*"<<endl;
//				if( aval && !bval ) cout<<"1*"<<endl;
//
//#ifdef DEBUG_MEDNWCD
//				cerr << "online mode modification";
//				if( a && b ){cerr << " ab"<<endl;}
//				else if( a && !b ){cerr << " a"<<endl;}
//				else if( !a && b ){cerr << " b"<<endl;}
//				else{cerr<< "??"<<endl;}
//#endif//DEBUG_MEDNWCD
//			}

			if( a && b ){
				// construct a new node with true appended
				it->first->duplicate( k );
			}
//			else if( a && !b ){
//				it->_states[k] = false;
//			}else if( !a && b ){
//				it->_states[k] = true;
//			}

			// in the offline mode each node in at least one of [A] or [B]
//			if( !online && !( a || b ) ){
//				cerr<< "mednw: neither a nor b is true "<<endl;
//				exit(EXIT_FAILURE);
//			}

//			if( online && !( a || b ) ){
//				it = _nodes.erase( it );
//			}else{
//				it++; // jt++;
//			}
		}

//		cout <<data[k].splitsize()<<" " << newnodes.size()<<" " <<(float)newnodes.size()/_nodes.size()<<endl;

//#ifdef DEBUG_MEDNWCD
//		cerr <<"k = "<<k+1<<"/"<< _data.size()<<" size "<< _nodes.size()<<" nodes"<<"                       \r";

//#endif//DEBUG_MEDNWCD
	}
	cerr<< endl;

	// online mode: output the final number of nodes
	// offline mode: apply the constraints
//	if( online || cc == NULL  ){
//		cout <<"# " << _nodes.size() <<" nodes"<< endl;
//	}else{
//		apply( cc );
//	}

//	cerr << "final "<<_nodes.size()<<" nodes"<<endl;
//	for( unsigned i=0; i<_nodes.size(); i++ ){
//		copy( _nodes[i].begin(), _nodes[i].end(), ostream_iterator<bool>(cerr,"") );cerr << endl;
//	}

//	for( unsigned i=0; i<_nodes.size(); i++ ){
//		unsigned cnt = 0;
//		for( unsigned j=0; j<_nodes[i].size(); j++ ){
//			if( _nodes[i][j] == true )
//				cnt++;
//		}
//		cout <<cnt<< endl;
//	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
mednw<T>::forward_iterator::forward_iterator( ){
	_cur.first = NULL;
	_cur.second = vector<bool>(0, false);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
mednw<T>::forward_iterator::forward_iterator( mednwnd<T> *s, unsigned nchar ){
	_cur = make_pair( s,  vector<bool>( nchar, false ));
	_vst.insert( s );
}

// ++ pre-increment +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
typename mednw<T>::forward_iterator &
mednw<T>::forward_iterator::operator++( ){

		// don't iterate an 'invalide' iterator (i.e. end())
	if( _cur.first == NULL ){
		return *this;
	}

	mednwnd<T> *nn = NULL;	// next node
	do{
		// get an unvisited neighbour of the current node
		for( unsigned i=0; i< _cur.first->_nb.size(); i++ ){
			if( _vst.find( _cur.first->_nb[i] ) == _vst.end() ){
				nn = _cur.first->_nb[i];
				break;
			}
		}

		// if there is no such neigbour then get the last from the call trace
		if( nn == NULL ){
			if( _trc.size() > 0 ){
								// apply difference to the last in the trace
				_cur.first->apply( _trc.top(), _cur.second );
				_cur.first = _trc.top(); // set cur to last in trace
				_trc.pop(); // pop trace
			}
		}else{
			_cur.first->apply( nn, _cur.second );  // apply difference to nn
			_trc.push( _cur.first );
			_cur.first = nn;
			_vst.insert( _cur.first );
			return *this;
		}
	}while( nn != NULL && _trc.size() > 0 );

	if( nn == NULL && _trc.size() == 0 ){
		return *this;
	}else{
		cerr << "mednw iterator: non sync stack=0 and nn=0"<<endl;
		exit(EXIT_FAILURE);
	}
}

// ++ post-increment ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
typename mednw<T>::forward_iterator
mednw<T>::forward_iterator::operator++( int n ){
	mednw<T>::forward_iterator copy = *this;
	++(*this);
	return copy;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
bool mednw<T>::forward_iterator::operator==( const mednw<T>::forward_iterator &other ) const {
	return _cur == other._cur;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
bool mednw<T>::forward_iterator::operator!=( const mednw<T>::forward_iterator &other ) const {
	return _cur != other._cur;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
pair<mednwnd<T> *,vector<bool> > &mednw<T>::forward_iterator::operator*() {
	return _cur;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
pair<mednwnd<T> *,vector<bool> > *mednw<T>::forward_iterator::operator->() {
	return &_cur;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
typename mednw<T>::forward_iterator mednw<T>::begin( ){
	return forward_iterator( _null, _data.size() );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
typename mednw<T>::forward_iterator mednw<T>::end(){
	return forward_iterator( NULL, _data.size() );
}



/**
 * read command line options
 * @param[in] argc number of arguments
 * @param[in] argv the arguments
 * @param[out] fname input filename
 * @param[in,out] circular the circularity of the genomes
 * @param[in,out] signhand the sing handling function
 * @param[in,out] adjacency use adjacencies
 * @param[in,out] online make the algorithm online
 * @param[in,out] directed the directed (vs undirected) genome model
 * @param[in,out] maxsize maximum set size
 * @param[in,out] nmiss allowed number of missing neg constraints
 * @param[in,out] pmiss allowed percentage of missing neg constraints
 */
void getoptions( int argc, char *argv[], string &fname, bool &circular,
		SgnHandType &signhand, bool &adjacency,
		bool &online, bool &directed, unsigned &maxsize, unsigned &nmiss,
		unsigned &pmiss);



/**
 * print usage and exit
 */
void usage( );

int main(int argc, char *argv[]){

	bool adjacencies = DEFAULT_ADJACENCY,	// adjacency mode
		circular = DEFAULT_CIRCULAR,		// circularity of the genomes
		online = DEFAULT_ONLINE,			// online/offline behaviour flag
		directed = DEFAULT_DIRECTED;
	ccc<geneset> *cc;					// consistency checker
	mednw<geneset> *mn;		// the median network
	string fname; 						// file name of the input
	SgnHandType sgnhand = DEFAULT_SGN_HANDLING; // sign handling option
	unsigned maxsize = DEFAULT_MAXSIZE, 	// maximum set size
			minobs = std::numeric_limits< unsigned >::max(),	// minimum number of observations in the
								// reconstructed ancestral gene orders
			nmiss = DEFAULT_NMISS,
			pmiss = DEFAULT_PMISS;
	vector<chrctr<geneset> > data;	// the binary characters
	vector<genom> genomes;
	vector<string> names,	// names of the species
		nmap, lnmap;	    // names of the genes


	// get command line options
	getoptions( argc, argv, fname, circular, sgnhand, adjacencies,
			online, directed, maxsize, nmiss, pmiss );

	// read the genomes from the file
	read_genomes(fname, genomes, names, circular, nmap);

	// make the genomes unsigned
	// - as usual -> genomes of length 2n + 2 (and construct new name mapping)
	// - or by forgetting the signs
	if( sgnhand == DOUBLE ){
		lnmap = double_nmap(nmap, directed );
		double_genomes(genomes, &lnmap, directed);
//		double_genomes(genomes, NULL, directed);
	}else if( sgnhand == UNSIGN ){
		lnmap = nmap;	// no long nmap. but the var is needed in both cases
		unsign_genomes( genomes, nmap, directed );
	}

//	for( unsigned i=0; i<genomes.size(); i++ ){
//		cerr << "> "<< names[i]<< endl;
//		cerr << genomes[i] <<endl;
//	}

	cout << "# m = "<<genomes.size()<<endl;
	cout << "# n = "<<genomes[0].size()<<endl;

	// inititialise the characters
	data = init_characters( genomes, circular, adjacencies, maxsize, minobs );

	if( pmiss != std::numeric_limits< unsigned >::max() ){
		minobs -= (unsigned) (minobs*pmiss/100.0);
	}else if( nmiss != std::numeric_limits< unsigned >::max() ){
		if( nmiss > minobs ){
			cerr << "error: minobs < nmiss"<<endl; usage();
		}
		minobs -= nmiss;
	}

	// initialise the consistency checker
	if( adjacencies ){
		cc = new ccc_adj<geneset>( genomes[0].size(), circular, directed,
				minobs, &lnmap );
	}else{
		cc = new ccc_interval<geneset>( genomes[0].size(), circular, directed,
				sgnhand, minobs, &nmap);
	}

	// construct the median network
	mn = new mednw<geneset>( data, cc, online );


	// make the genomes unsigned
	// - as usual -> genomes of length 2n + 2 (and construct new name mapping)
	// - or by forgetting the signs
	if( sgnhand == DOUBLE ){
		undouble_genomes(genomes, &nmap, directed);
	}else if( sgnhand == UNSIGN ){
		cerr << "attention: with -s unsign the sign information is not restored"<<endl;
	}

	unsigned acnt = 0;
	vector<genom> ccperm;				// all consistent permutations

	/*
	for( list<mednwnde<geneset> >::const_iterator it=mn->begin(); it!=mn->end(); it++){
 		bool org;
		unsigned k=0;
		ccc<geneset> *ccc = it->getccc();
		vector<bool> states = it->getstates();
		vector<genom> cperm;	//consistent permutations

		// determine if it is original binary data data, i.e. not if one of
		// the consistent permutations is equal to a input permutation
		for( k=0; k< data[0].size();  k++){
			org = true;
			for( unsigned j=0; j<states.size(); j++ ){
				if( states[j] != data[ j ][k] ){
					org = false; break;
				}
			}
			if( org ){ break; }
		}

		cperm = ccc->getpermutations();
		ccperm.insert( ccperm.end(), cperm.begin(), cperm.end () );

//		cout <<": "<< ccc->cntconsistent() <<" "<< ccc->getobs()<<endl;

//		if( ccc->cntconsistent() <= 1 && ccc->getobs() == 2628 ){
//
//			cout <<"# (";
//			copy( states.begin(), states.end(), ostream_iterator<bool>(cout,"") );
//			cout <<") ";
//			if( org ){
//				cout <<" == "<<names[k]<< " ";
//			}
//			cout << endl << "# " <<  *ccc << endl;
//
//			cperm = ccc->getpermutations();
//			for(unsigned i=0; i<cperm.size(); i++){
//				genom ctmp;
//				vector<genom>::iterator fit;
//				if( sgnhand == DOUBLE ){
//					ctmp = undouble_genom( cperm[i], &nmap, directed );
//				}else if( sgnhand == UNSIGN ){
//					ctmp = cperm[i];
//				}
//				fit = find( genomes.begin(), genomes.end(), ctmp );
//				if( fit != genomes.end() ){
//					cout << "# "<< names[fit-genomes.begin()]<<endl;
//				}else{
//					names.push_back( "A"+int2string(acnt) );
//					genomes.push_back( ctmp );
//					cout << "> "<<names.back()<< endl;
//					cout << genomes.back()<<endl;
//					acnt++;
//				}
//			}
//			cout << endl;
//		}

//		else{
//			cerr << "error: median network contained inconsistent node"<<endl;
//			exit(EXIT_FAILURE);
//		}
		delete ccc;
 	}
 	unique( ccperm );
 	cerr << ccperm.size()<< " genomes"<<endl;


 	dstnc
//		*rtd = new dstnc_rt( genomes[0].size(), circular, 1, 1, false),
// 		*tdrld = new dstnc_tdl( ),
 		*mbtd = new dstnc_crex( true, false, 2);

// 	vector<dstnc*> mtbdv;
// 	mtbdv.push_back( rtd );
// 	mtbdv.push_back( tdrld );
//
// 	mbtd = new dstnc_comb<minfunc<unsigned> >( mtbdv, minfunc<unsigned>() );

 	vector<vector<unsigned> > dmat;
  	dmat = distance_matrix( ccperm, mbtd );
 	for( unsigned i=0; i<dmat.size(); i++ ){
 		copy( dmat[i].begin(), dmat[i].end(), ostream_iterator<unsigned>(cout, " "));
 		cout << endl;
 	}

 	unsigned maximum = 0;
 	for( unsigned i=0; i<dmat.size(); i++ ){
 		unsigned m=std::numeric_limits< unsigned >::max();
 		for( unsigned j=0; j<dmat[i].size(); j++ ){
 			if( i==j ) continue;
 	 		if( dmat[i][j] < m )
 	 			m = dmat[i][j];
 		}
 		cout << "min "<<m<<endl;
		if( m > maximum )
			maximum = m;
 	}
 	cout << "max min bp "<<maximum<<endl;

//	delete rtd;
//	delete tdrld;
	delete mbtd;

// 	undouble_genomes( mngenomes, &nmap, circular );
*/






 	genomes.clear();

	delete cc;
	delete mn;
	return EXIT_SUCCESS;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void getoptions( int argc, char *argv[], string &fname, bool &circular,
		SgnHandType & signhand, bool &adjacencies,
		bool &online, bool &directed, unsigned &maxsize, unsigned &nmiss,
		unsigned &pmiss){
	int c,
		cb = (circular)?1:0,	// circular int
		db = (directed)?1:0,	// directed int
		ob = (online)?1:0;		// online int

	while (1) {
		static struct option long_options[] = {
			{"adj",     no_argument,       0, 'a'},
			{"file",    required_argument, 0, 'f'},
			{"help",    no_argument,       0, 'h'},
			{"lindir",  no_argument,	   0, 'l'},
			{"maxsize", required_argument, 0, 'm'},
			{"nmiss",   required_argument, 0, 'n'},
			{"linund",  no_argument,	   0, 'u'},
			{"offline", no_argument,     &ob,  0 },
			{"sign",    required_argument, 0, 's'},
            {0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:hm:s:",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'a':
				adjacencies = true;
				maxsize = 2;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case 'l':
				circular = false; directed = true;
				break;
			case 'm':
				maxsize = atoi( optarg );
				break;
			case 'n':
				if( optarg[ strlen(optarg)-1 ] == '%' ){
					pmiss = atoi( optarg );
					nmiss = std::numeric_limits< unsigned >::max();
				}else{
					nmiss = atoi( optarg );
					pmiss = std::numeric_limits< unsigned >::max();
				}
				break;
			case 'u':
				circular = false; directed = false;
				break;
			case 's':
				if( strcmp ( optarg, "double" ) == 0 ){
					signhand = DOUBLE;
				}else if( strcmp ( optarg, "unsign" ) == 0 ){
					signhand = UNSIGN;
				}else{
					cerr << "unknown sign handling function: "<<optarg<<endl;
					usage();
				}
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

    // set the online bool variable
    online = (ob==0)?false:true;

    if( maxsize < 2 ){
    	cerr << "error: maxsize < 2"<<endl;
    	usage();
    }

    if( pmiss != std::numeric_limits< unsigned >::max() && pmiss > 100){
    	cerr << "error: allowed percentage of missing constraints > 100%"<<endl;
    	usage();
    }

	// set the adjacency bool varible
    if( adjacencies && maxsize != 2 ){
		cerr << "error: adjacency mode incompatible with maxsize "<<maxsize<<endl;
		usage();
	}

    if( circular && directed ){
    	cerr << "circular directed not implemented"<<endl;
    	exit(EXIT_FAILURE);
    }


		// check parameters
	if( fname == "" ){
		cerr << "no file given"<<endl;
		usage();
	}
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cerr << "mednw -f FILE [OPTIONS]"<<endl;
	cerr << "--help -h: print this message"<<endl;
	cerr << "--lindir: assume linear directed genomes"<<endl;
	cerr << "--linund: assume linear undirected genomes"<<endl;
	cerr << "-m MAXS : set max interval size to MAXS (default: ";
	if(DEFAULT_MAXSIZE == std::numeric_limits< unsigned >::max())cerr <<"auto";
	else cerr<<DEFAULT_MAXSIZE;
	cerr << " )"<<endl;
	cerr << "--adj   : adjacencies (default interval)"<<endl;
	cerr << "--sign -s : sign handling. default: ";
	if( DEFAULT_SGN_HANDLING == DOUBLE ) cerr << "double";
	else if( DEFAULT_SGN_HANDLING == UNSIGN ) cerr << "unsign";
	else{
		cerr << "error: undefined DEFAULT_SGN_HANDLING "<<DEFAULT_SGN_HANDLING<<endl;
		exit(EXIT_FAILURE);
	}
	cerr << endl;
	cerr << "         double: e->2i-1,2i; -e->2|i|,2|i|-1"<<endl;
	cerr << "         unsign: remove signs"<<endl;
	cerr << "--offline : apply constraints offline. default: online"<<endl;
	cerr << "--nmiss NR: allowed number of missing negative constraints"<<endl;
	cerr << "            if '%' is appended it is the allowed percentage of the maximum"<<endl;

	exit( EXIT_FAILURE );
}
