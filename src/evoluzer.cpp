/** @file evoluzer.cpp
 * 'simulate evolution' -> construct datasets
 */

#include <algorithm>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <iterator>
#include <numeric>

#include "tree.hpp"
#include "common.hpp"
#include "genom.hpp"
#include "helpers.hpp"
#include "rearrangements.hpp"

using namespace std;

void getoptions( int argc, char *argv[], bool &root, bool &unroot, vector<float> &prob,
		int &r, int &k, int &n, string &treefile, bool &fix, unsigned &imult, unsigned &omult,
		bool &randint, unsigned &width, bool &verbose, bool &id);

/**
 * get the max similarity of rearrangements in rrrmt
 * @param[in] r
 * @return similarity
 */
vector<float> similarity( const rrrmt *r ){
	vector<set<int> > e;
	set<int> is, sd;	// intersection
	float m  = 0, s;
	vector<float> sims;

	r->elements( e, true );

	set_intersection(e[0].begin(), e[0].end(),
			e.back().begin(), e.back().end(),
			insert_iterator<set<int> >(is,is.begin()));
	set_union(e[0].begin(), e[0].end(),
			e.back().begin(), e.back().end(),
			insert_iterator<set<int> >(sd,sd.begin()));

	s = (float)is.size() / (sd.size());
//					/min(e[i].size(), e[j].size());
	sims.push_back(s);


//	for( unsigned i=0; i<e.size(); i++ ){
//		for( unsigned j=i+1; j<e.size(); j++ ){
//			is.clear();
//			set_intersection(e[i].begin(), e[i].end(),
//					e[j].begin(), e[j].end(),
//					insert_iterator<set<int> >(is,is.begin()));
//			set_union(e[i].begin(), e[i].end(),
//					e[j].begin(), e[j].end(),
//					insert_iterator<set<int> >(sd,sd.begin()));
//
//			s = (float)is.size() / (sd.size());
////					/min(e[i].size(), e[j].size());
//			sims.push_back(s);
////			m = max( m, s );
//		}
//	}
	return sims;
}

/**
 * get the sizes of rearrangements in rrrmt
 * @param[in] r
 * @return sizes
 */
vector<float> lgths( const rrrmt *r, int n ){
	vector<set<int> > e;
	vector<float> lens;

	r->elements( e, true );

	for( unsigned i=0; i<e.size(); i++ ){
		lens.push_back(min( e[i].size(), n-e[i].size()) );
	}
	return lens;
}


/**
 * get the max similarity of rearrangements in rrrmt
 * @param[in] r
 * @return similarity
 */
vector<float> similarity( const rrrmt *r, const rrrmt *s  ){
	vector<set<int> > e, f;
	set<int> is, sd;	// intersection
	float m  = 0, sim;
	vector<float> sims;

	r->elements( e, true );
	s->elements( f, true );

	for( unsigned i=0; i<e.size(); i++ ){
		for( unsigned j=0; j<f.size(); j++ ){
			is.clear();
			set_intersection(e[i].begin(), e[i].end(),
					f[j].begin(), f[j].end(),
					insert_iterator<set<int> >(is,is.begin()));
			set_union(e[i].begin(), e[i].end(),
					f[j].begin(), f[j].end(),
					insert_iterator<set<int> >(sd,sd.begin()));

			sim = (float)is.size() / (sd.size());
			//max(e[i].size(), f[j].size());
			sims.push_back(sim);
//			m = max( m, s );
		}
	}
	return sims;
}


/**
 * print usage information and quit
 */
void usage();


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cout << "evoluzer [OPTIONS]"<<endl;
	cout << " --length -n: length of the genomes"<<endl;
	cout << " --rate   -k: number of events"<<endl;
	cout << " --tree   -t: tree file"<<endl;
	cout << " --fix      : constant number of events on all edges (default: [1:2k])"<<endl;
	cout << " --wr       : probability of reversal"<<endl;
	cout << " --wt       : probability of transposition"<<endl;
	cout << " --wrt      : probability of reverse transposition"<<endl;
	cout << " --wtdl     : probability of tdrl"<<endl;
	cout << " --width  -w: max nr of affected elements per rrrmt"<<endl;
	cout << "          -z: simulate TDRL on complete permutation (default: in a rand. chosen interval)"<<endl;
	cout << " --root     : root the tree"<<endl;
	cout << " --unroot   : unroot the tree"<<endl;
	cout << " --imult    : edge length multiplicator for inner edges"<<endl;
	cout << " --omult    : edge length multiplicator for outer edges"<<endl;
	cout << "  --id      : add identity to output "<<endl;
	cout << " --help     : produce this message"<<endl;
	cout << " --verbose  : be verbose, i.e. print rrrmt"<<endl;
//	cout << "   -r number of datasets to generate"<<endl;
	exit(EXIT_FAILURE);
}

/**
 *
 */
genom simulate_clusters( int n, int r, int clucnt, int ioratio );

int main(int argc, char *argv[]){
	bool printid=false,	// add id to output
			root = false,		// flags controling if trees in the file should be rooted
		unroot = false, 	// .. or unrooted
		fix = false,		// fixed mutation rates
		randint = true,		// simulate tdrls in a randomly chosen interval
		verbose = false;
	genom id,				// start point of simulations
		tgt;
	int n = 0, 				// length of the genomes
		kk = 0,
		k = 0,				// number of events
		r = 1;				// number of repetitions
	string treefname;		// name of a file containing trees
	unsigned imult = 1, 	// multiplicator for inner edge length
		omult = 1,			// multiplicator for outer edge length
		width = std::numeric_limits< unsigned >::max(); 	// max. number of elements affected by a rrrmt
	vector<float> prob;		// propability of a reversal, transposition, reverse transposition, and tdrl
	vector<genom> trace,
		genomes;
	vector<string> names;

	prob = vector<float>(4, 0);
	getoptions( argc, argv, root, unroot, prob, r, k, n, treefname, fix, imult, omult, randint, width, verbose, printid);

	if(n < 2){cerr << "error: genomes must have length > 1"<<endl; usage();}
//	if(k < 1){cerr << "error: k must be greater 0"<<endl; usage();}
//	if(r < 1){cerr << "error: r must be greater 0"<<endl; usage();}
//	if(prob.size() != 4){cerr<<"error: not all probabilities given"<<endl; usage();}
	if( root == true && unroot == true ){cerr << "error: --root and --unroot not possible"<<endl; usage();}

	init_rng( );
	id = genom(n, false);

	// case tree given
	if(treefname != ""){
		phy<genom> *bt;
		ifstream tfile;
		string line;
		vector<node<genom> *> po;
		vector<rrrmt *> rand_sce;

		tfile.open (treefname.c_str(), ifstream::in);
		if(!tfile.is_open()){
			cout << "could not open treefile: "<< treefname<<endl;
			usage();
		}
		while(tfile){
			getline(tfile, line);
			if(line[0] != '(')
				continue;
			break;
		}
		tfile.close();

		bt = new phy<genom>(line, root, unroot);

//		cout << "# tree: "<<*bt<<endl;

		bt->preorder_nodes( po, true);
		for( unsigned i=0; i<po.size(); i++ ){
			node<genom> *pp = po[i]->get_parent();
			if(pp == NULL){	// set root to identity
				po[i]->set_value( id );
				rand_sce.push_back( new emptyscen() );
			}
			else{			// simulate rearrangements to the new node
				tgt = pp->get_value();
				if( fix ){		// fixed mutation rates, i.e. same number of rrrmt on each edge
					kk = k;
				}else{			// mutation rate randomly choosen from (1..2k)
					kk = ask_rng(1, 2*k);
				}
				if( ! po[i]->is_leaf() ){	// edge length multiplication for inner edges
					kk*=imult;
				}else{						// edge length multiplication for outer edges
					kk*=omult;
				}

				rand_sce.push_back(new ordered( kk, prob, tgt, trace, randint, width));
				po[i]->set_value( tgt );

				if(verbose){
					vector<rrrmt*> rset;
					rand_sce[i]->getrrrmt( rset );
					for ( unsigned z=0; z<rset.size(); z++ ){
						cerr << "#" <<pp->get_name()<<"->"<<po[i]->get_name()<<" "<<i<<" "<<po[i]->is_leaf()<<" ";
								rset[z]->output(cerr, 0, 1);cerr << endl;
					}

				}
			}
//			cout << endl;
			if( po[i]->is_leaf() ){
				genomes.push_back( po[i]->get_value() );
				names.push_back( po[i]->get_name() );
			}
		}

		for( unsigned i=1; i<rand_sce.size(); i++ ){
			vector<float> sims;
			vector<float> lens;
			node<genom> *ppi = po[i]->get_parent();

			for( unsigned j=i+1; j<rand_sce.size(); j++ ){
				node<genom> *ppj = po[j]->get_parent();

				sims = similarity( rand_sce[i], rand_sce[j] );
				cerr <<"# similarity diff edge ";
				cerr << ppi->get_name()<<"->"<<po[i]->get_name()<<" ";
				cerr << ppj->get_name()<<"->"<<po[j]->get_name();
				cerr<<" " << po[i]->is_leaf()<<" "<<po[j]->is_leaf() <<" "<<(* min_element(sims.begin(), sims.end()))<<" "<<
						(* max_element(sims.begin(), sims.end()))<<" "<<
						accumulate(sims.begin(), sims.end(), 0.0) / sims.size()<<endl;
			}
			sims = similarity( rand_sce[i] );

			if( sims.size() > 0 ){
				cerr <<"# similarity equal edge " << ppi->get_name()<<"->"<<po[i]->get_name();
				cerr << " " << po[i]->is_leaf()<<" "<<po[i]->is_leaf() <<" "<<(* min_element(sims.begin(), sims.end()))<<" "<<
						(* max_element(sims.begin(), sims.end()))<<" "<<
						accumulate(sims.begin(), sims.end(), 0.0) / sims.size()<<endl;
			}
			lens = lgths( rand_sce[i], n );
			cerr <<"#length "<< ppi->get_name()<<"->"<<po[i]->get_name();
			cerr << " " << po[i]->is_leaf()<<" "<<po[i]->is_leaf()<<" "
					<<(* min_element(lens.begin(), lens.end()))<<" "<<
					(* max_element(lens.begin(), lens.end()))<<" "<<
					accumulate(lens.begin(), lens.end(), 0.0) / lens.size()<<endl;

		}

		for( unsigned i=0; i<rand_sce.size(); i++ ){
			delete rand_sce[i];
		}


//		init(line, id, r, applied, hd);
//		leaf_genomes(bt, names, genomes);
//		inner_genomes(bt, innernames, inner);
//		cout << "# ";
//		print_newick(bt, 1, 1);
//		cout <<endl;
//		cout << "# applied "<<applied<<endl;
//		cout << "# score"<< score(bt)<<endl;
//		for(unsigned i=0; i<inner.size(); i++){
//			cout << "#>"<<innernames[i]<<endl;
//			cout << "#"<<inner[i]<<endl;
//		}
		delete bt;
	}else{
		rrrmt *rand_sce = NULL;

		for (int i = 0; i < r; i++){
			genom g(n,0);
			vector<genom> trace;
			names.push_back(int2string(i));
			rand_sce = new ordered( k, prob, g, trace, randint, width);

			if(verbose){
				vector<rrrmt*> rset;
				rand_sce->getrrrmt( rset );
				for ( unsigned z=0; z<rset.size(); z++ ){
					cerr << "#" <<i<<" ";
							rset[z]->output(cerr, 0, 1);cerr << endl;
				}
				vector<float> sims = similarity( rand_sce );
				vector<float> lens = lgths( rand_sce, n );
				cerr <<"#similarity "<<
						(* min_element(sims.begin(), sims.end()))<<" "<<
						(* max_element(sims.begin(), sims.end()))<<" "<<
						accumulate(sims.begin(), sims.end(), 0.0) / sims.size()<<endl;
				cerr <<"#length "<<(* min_element(lens.begin(), lens.end()))<<" "<<
						(* max_element(lens.begin(), lens.end()))<<" "<<
						accumulate(lens.begin(), lens.end(), 0.0) / lens.size()<<endl;
			}

			genomes.push_back(g);
		}
	}

//	if( m == 1 ){
//		names.push_back("id");
//		genomes.push_back(id);
//	}
//
	if( printid ){
		names.push_back("id");
		genomes.push_back( id );
	}


	vector<unsigned> shuffle;
	shuffle = vector<unsigned>(genomes.size());
	for(unsigned i=0; i<shuffle.size(); i++){
		shuffle[i] = i;
	}
	random_shuffle(shuffle.begin(), shuffle.end());

	for(unsigned i=0; i<genomes.size(); i++){
		cout << ">"<<names[ shuffle[i] ]<<endl;
		cout << genomes[ shuffle[i] ]<< endl;
	}

//	free_data(hd);
	return 1;
//	for (int i=0; i<r; i++){
//		tgt = genom(n, 0);
//		rand_sce = new ordered( k, prob, tgt, trace);
//		cout << "> "<<i<< endl;
//		cout << tgt << endl;
//
//		delete rand_sce;
//		//cout << "random scenario "<<*rand_sce << endl;
//	}


/*


	ifstream tfile;
	string treefname,
		line;
	int l=0,	// length of the applied operations (0 = random)
		m = 3, // count of the species
		n = -1, // length of the genomes
		r = -1,	// nr of reversals to apply
		t = 0, // probability (%) of a transposition
		applied = 0,
		cluster = 0,	// number of clusters to preserve
		ioratio = 50; // percent of reversals inside a cluster; the remaining are made outside

	genom id, g;
	hdata hd;
	vector<int> scale;
	vector<genom> genomes,
		inner,
		tmp_genomes;
	vector<string> innernames,
		names,
		tmp_names;
	vector<unsigned> shuffle;

	// get the parameters
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'c': {i++; cluster=atoi(argv[i]); i++; ioratio=atoi(argv[i]); break; }
			case 'f': {i++; treefname=argv[i]; break;}
			case 'l': {i++; l = atoi(argv[i]); break;}
			case 'n': {i++; n = atoi(argv[i]); break;}
			case 'm': {i++; m = atoi(argv[i]); break;}
			case 'r': {i++; r = atoi(argv[i]); break;}
			case 's': {i++; scale.push_back( atoi(argv[i]) ); break;}
			case 't': {i++; t = atoi(argv[i]); break;}
		}
	}

	if(n<0 || r<0){
		usage();
	}
	if(ioratio < 0 || ioratio > 100){
		cerr <<"error: inside-outside ratio not in 0...100"<<endl;
		usage();
	}

	init_rng();
	init_data(hd, n, 0);

	id = genom(n, false);

	if(treefname != ""){
		bintree *bt;

		tfile.open (treefname.c_str(), ifstream::in);
		if(!tfile.is_open()){
			cout << "could not open "<< treefname<<endl;
			usage();//
		}
		while(tfile){
			getline(tfile, line);
			if(line[0] == '('){
				bt = init(line, id, r, applied, hd);
				leaf_genomes(bt, names, genomes);
				inner_genomes(bt, innernames, inner);
				cout << "# ";
				print_newick(bt, 1, 1);
				cout <<endl;
				cout << "# applied "<<applied<<endl;
				cout << "# score"<< score(bt)<<endl;
				for(unsigned i=0; i<inner.size(); i++){
					cout << "#>"<<innernames[i]<<endl;
					cout << "#"<<inner[i]<<endl;
				}
				free_tree(bt);
				break;
			}
		}
		tfile.close();

	}else{
		for (int i=scale.size(); i<m; i++)
			scale.push_back(1);
		for (int i = 0; i < m; i++){
			names.push_back(int2string(i));


			if( cluster > 0 ){
				g = simulate_clusters(n , scale[i]*r, cluster, ioratio);
			}else{
				g = id;
				g.evolve(scale[i]*r, t, l);
			}

			genomes.push_back(g);
		}
	}

	if( m == 1 ){
		names.push_back("id");
		genomes.push_back(id);
	}

	shuffle = vector<unsigned>(genomes.size());
	tmp_genomes = vector<genom>(genomes.size());
	tmp_names = vector<string>(genomes.size());
	for(unsigned i=0; i<shuffle.size(); i++){
		shuffle[i] = i;
	}

	random_shuffle(shuffle.begin(), shuffle.end());
	for(unsigned i=0; i<shuffle.size(); i++){
		tmp_names[i] = names[ shuffle[i] ];
		tmp_genomes[i] = genomes[ shuffle[i] ];
	}
	names = tmp_names;
	genomes = tmp_genomes;

	for(unsigned i=0; i<genomes.size(); i++){
		cout << ">"<<names[i]<<endl;
		cout << genomes[i]<< endl;
	}

	free_data(hd);
	return 1;

	*/
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void getoptions( int argc, char *argv[], bool &root, bool &unroot, vector<float> &prob,
		int &r, int &k, int &n, string &treefile, bool &fix, unsigned &imult, unsigned &omult,
		bool &randint, unsigned &width, bool &verbose, bool &id){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"id",   no_argument,       0, 'j'},
			{"root",   no_argument,       0, 'a'},
			{"unroot", no_argument,       0, 'b'},
			{"wr",     required_argument, 0, 'c'},
			{"wt",     required_argument, 0, 'd'},
			{"wrt",    required_argument, 0, 'e'},
			{"wtdl",   required_argument, 0, 'f'},
			{"help",   no_argument,       0, 'h'},
			{"rate",   required_argument, 0, 'k'},
			{"fix",    no_argument,       0, 'x'},
			{"length", required_argument, 0, 'n'},
			{"imult"  , required_argument, 0, 'i'},
			{"omult"  , required_argument, 0, 'o'},
			{"tree",   required_argument, 0, 't'},
			{"verbose"  , no_argument, 0, 'v'},
			{"width"  , required_argument, 0, 'w'},
			{"randint", no_argument, 0, 'z'},
            {0, 0, 0, 0}
		};

        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "k:n:r:t:hvw:z",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'a':
				root = true;
				break;
			case 'b':
				unroot = true;
				break;
			case 'c':
				prob[0] = atof(optarg);
				break;
			case 'd':
				prob[1] = atof(optarg);
				break;
			case 'e':
				prob[2] = atof(optarg);
				break;
			case 'f':
				prob[3] = atof(optarg);
				break;
			case 'h':
				usage();
				break;
			case 'i':
				imult = atoi(optarg);
				break;
			case 'j':
				id = true;
				break;
			case 'k':
				k = atoi(optarg);
				break;
			case 'n':
				n = atoi(optarg);
				break;
			case 'o':
				omult = atoi(optarg);
				break;
			case 'r':
				r = atoi(optarg);
				break;
			case 't':
				treefile = optarg;
				break;
			case 'v':
				verbose = true;
				break;
			case 'w':
				width = atoi(optarg);
				break;
			case 'x':
				fix = true;
				break;
			case 'z':
				randint = false;
				break;
			case '?':
				cerr << "?"<<endl;
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

genom simulate_clusters( int n, int r, int clucnt, int ioratio ){
	double rara = 0;
	genom g;
	int rio, 			// for choosing inside vs outside
		rcl,			// for choosing a random cluster
		rstart,
		rend;
	vector<double> rand;
	vector<pair<int,int> > cluster;

	if(clucnt < 1){
		cerr <<"simulate_clusters: less than 1 cluster impossible"<<endl;
		exit(1);
	}
	if(clucnt > n){
		cerr <<"simulate_clusters: more than n clusters impossible"<<endl;
		exit(1);
	}

	g = genom(n, 0);	// init as id

		// initialise the clusters
		// - generate accumulated random numbers in an array
		// - scale them to 0..n
		// - calculate the start-end pairs
	rand.push_back(0.0);
	for(unsigned i=0; i<(unsigned)clucnt; i++){
		rand.push_back( rand[rand.size()-1] + ((double)ask_rng() / (double)RAND_MAX) );
	}
	rara = (n-1) / rand.back();
	for(unsigned i=0; i<rand.size(); i++){
		rand[i] *= rara;
	}
	//copy(rand.begin(), rand.end(), ostream_iterator<double>(cout, " ")); cout << endl;
	for(unsigned i=1; i<rand.size(); i++){
		if (i<(rand.size()-1))
			cluster.push_back( make_pair( (int)round(rand[i-1]), (int)round(rand[i])-1 ) );
		else
			cluster.push_back( make_pair( (int)round(rand[i-1]), (int)round(rand[i]) ) );

		if( cluster[cluster.size()-1].second < cluster[cluster.size()-1].first ){
			cluster[cluster.size()-1].second++;
		}
	}
	unique(cluster.begin(), cluster.end());
	//for(unsigned i=0; i<cluster.size(); i++){
	//	cout << "["<< cluster[i].first<<","<<cluster[i].second<<"] ";
	//}
	//cout << endl;

	for( int i=0; i<r; i++ ){
		rio = ask_rng() % 100;
		if (rio < ioratio){ 	// inside
			//cout << "inside"<<endl;
			rcl = ask_rng() % cluster.size();
			rstart = ask_rng() % ( cluster[rcl].second - cluster[rcl].first + 1 ) + cluster[rcl].first;
			rend = ask_rng() % ( cluster[rcl].second - cluster[rcl].first + 1 ) + cluster[rcl].first;
			if (rstart > rend)
				swap(rstart, rend);

		}else{	// outside
			//cout << "outside"<<endl;
			rstart = ask_rng() % (cluster.size());
			rend = ask_rng() % (cluster.size());
			//cout << "sel clu "<<rstart<<" "<<rend<<endl;
			if (rstart > rend)
				swap(rstart, rend);
			rstart = cluster[ rstart ].first;
			rend = cluster[ rend ].second;
		}

		//cout << "rev "<<rstart<<","<<rend<<endl;
		reverse(g, rstart, rend);
		//cout << g<< endl;
	}

	return g;
}


