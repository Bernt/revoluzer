/** @file gc_rl.cpp
 * 'garbage collector'; purpose: collect the results of cim and 
 * mgrResultparser it adds score difference and distance of solution
 * for all k; the input should come from stdin
 */

#include <iostream>
#include <map>
#include <boost/regex.hpp>


using namespace std;

int main(int argc, char *argv[]){
	char in[256];
	boost::regex re_result("^(-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+)$");
	//~ boost::regex re_result2("^(-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+)$");
	boost::regex re_used("^used (\\d+)$");
	boost::regex re_resulting("^resulting (\\d+)$");
	boost::smatch match;
	int m=0, n=0, k=0, 
		//~ r=0,
		dos=0, sd=0, 			// distance of solution and score difference
		dos_id=0, sd_id=0,		// distance of solution and score difference in interval distance
		mult,
		used = 0,
		resulting = 0;
	string s;						// string for temporary usage
	//~ map<int, int> indices;
	map<int, int> indices;
	vector<vector<int> > dos_vec, 
		sd_vec,
		dos_id_vec,
		sd_id_vec;
	
	if(argc!=2){
		cout << "usage: gc multipy(3/4)"<<endl;
		exit(1);
	}
	mult = atoi(argv[1]);
	
	while(!cin.eof()){
		string in_s;
		cin.getline(in, 255);
		in_s = in;
		
		if(boost::regex_match(in_s, match, re_used)){
			s.assign(match[1].first, match[1].second);
			used += atoi(s.c_str());
		}
		if(boost::regex_match(in_s, match, re_resulting)){
			s.assign(match[1].first, match[1].second);
			resulting += atoi(s.c_str());
		}
			
		if(boost::regex_match(in_s, match, re_result)){
			s.assign(match[1].first, match[1].second);
			m = atoi(s.c_str());
			s.assign(match[2].first, match[2].second);
			n = atoi(s.c_str());
			s.assign(match[3].first, match[3].second);
			k = atoi(s.c_str());
			
			//~ s.assign(match[4].first, match[4].second);
			//~ r = atoi(s.c_str());
			s.assign(match[4].first, match[4].second);
			dos = atoi(s.c_str());
			s.assign(match[5].first, match[5].second);
			sd = atoi(s.c_str());

			s.assign(match[6].first, match[6].second);
			dos_id = atoi(s.c_str());
			s.assign(match[7].first, match[7].second);
			sd_id = atoi(s.c_str());
			
			//~ cout << in_s<<endl;;			
			//~ cout << m <<" "<<n<< " "<<k<<" "<<r<<" "<<dos<<" "<<sd<<" "<<dos_id<<" "<<sd_id<<endl;;
			
			//~ pair<int,int> kr = pair<int,int>(k,r);
			
			if(indices[k]==0){
				indices[k] = k+1;
				dos_vec.push_back(vector<int>());
				sd_vec.push_back(vector<int>());
				dos_id_vec.push_back(vector<int>());
				sd_id_vec.push_back(vector<int>());
			}
			
			dos_vec[indices[k]-1].push_back(dos);
			sd_vec[indices[k]-1].push_back(sd);
			dos_id_vec[indices[k]-1].push_back(dos_id);
			sd_id_vec[indices[k]-1].push_back(sd_id);

		}
	}
	cout << "# dos: distance of solution (to the identity)"<<endl;
	cout << "# sd : score difference (to 3*k)"<<endl;
	cout << "# id : distance in intervaldistance"<<endl;
	cout << "# ratio, avg_dos, avg_sd, avg_dos_id, avg_sd_id"<<endl;
	for(map<int, int >::iterator it=indices.begin(); it!=indices.end(); it++){
		float dos_avg = 0,
			sd_avg = 0,
			dos_id_avg  = 0,
			sd_id_avg = 0, 
			dos_var = 0,		// variances
			sd_var = 0,
			sd_id_var = 0,
			dos_id_var = 0;
		
		k = it->first;

			// calc averages
		for(unsigned i=0; i<dos_vec[it->second-1].size(); i++ ){
			dos_avg += dos_vec[it->second-1][i];
			sd_avg += sd_vec[it->second-1][i];
			dos_id_avg += dos_id_vec[it->second-1][i];
			sd_id_avg += sd_id_vec[it->second-1][i];
		}
		dos_avg /= dos_vec[it->second-1].size();
		sd_avg  /= sd_vec[it->second-1].size();
		dos_id_avg /= dos_id_vec[it->second-1].size();
		sd_id_avg  /= sd_id_vec[it->second-1].size();

		for(unsigned i=0; i<dos_vec[it->second-1].size(); i++ ){
			dos_var 		+= pow(dos_vec[it->second-1][i] - dos_avg, 2);
			sd_var 		+= pow(sd_vec[it->second-1][i] - sd_avg, 2);
			dos_id_var 	+= pow(dos_id_vec[it->second-1][i] - dos_id_avg, 2);
			sd_id_var 	+= pow(sd_id_vec[it->second-1][i] - sd_id_avg, 2);
		}

		dos_var 		= sqrt(dos_var 	/ (dos_vec[it->second-1].size()-1));
		sd_var		= sqrt(sd_var 	/ (sd_vec[it->second-1].size()-1));
		dos_id_var 	= sqrt(dos_id_var/ (dos_id_vec[it->second-1].size()-1));
		sd_id_var 	= sqrt(sd_id_var / (sd_id_vec[it->second-1].size()-1));

					//~ // output
		cout << ((float)mult*k)/n << "\t"
			<< dos_avg 		<<"\t"
			<< sd_avg 		<<"\t"
			<< dos_id_avg 	<<"\t"
			<< sd_id_avg	<<endl;
			// output
		//~ cout << ((float)mult*k)/n << "\t"<< r << "\tr "
			//~ << dos_avg 		<<"\t"<<dos_avg - dos_var 	<<"\t"<<dos_avg + dos_var 		<<"\t" 
			//~ << sd_avg 		<<"\t"<<sd_avg - sd_var 		<<"\t"<<sd_avg + sd_var 		<<"\t"
			//~ << dos_id_avg 	<<"\t"<<dos_id_avg - dos_id_var <<"\t"<<dos_id_avg + dos_id_var 	<<"\t"
			//~ << sd_id_avg	<<"\t"<<sd_id_avg - sd_id_var 	<<"\t"<<sd_id_avg + sd_id_var	<<endl;
	}
	cout << "used "<<used << endl;
	cout << "resulting "<<resulting << endl;
	return 1;

	//~ char in[81];
	//~ boost::regex re_result("^(-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+) (-{0,1}\\d+)$");
	//~ boost::smatch match;
	//~ int m=0, n=0, k=0, dos=0, sd=0, max_k=0;
	//~ string s;						// string for temporary usage
	//~ map<int, int> counter,
		//~ dos_map,
		//~ sd_map;

	//~ while(!cin.eof()){
		//~ string in_s;
		//~ cin.getline(in, 80);
		//~ in_s = in;
		
		//~ if(boost::regex_match(in_s, match, re_result)){
			//~ s.assign(match[1].first, match[1].second);
			//~ m = atoi(s.c_str());
			//~ s.assign(match[2].first, match[2].second);
			//~ n = atoi(s.c_str());
			//~ s.assign(match[3].first, match[3].second);
			//~ k = atoi(s.c_str());
			//~ s.assign(match[4].first, match[4].second);
			//~ dos = atoi(s.c_str());
			//~ s.assign(match[5].first, match[5].second);
			//~ sd = atoi(s.c_str());
			
			//~ if(k>max_k)
				//~ max_k = k;
			
			//~ counter[k]++;
			//~ dos_map[k] += dos;
			//~ sd_map[k] += sd;
		//~ }
	//~ }
	//~ cout << "# dos: distance of solution (to the identity)"<<endl;
	//~ cout << "# sd : score difference (to 3*k)"<<endl;
	//~ cout << "# ratio, avg_dos, avg_sd"<<endl;
	//~ for(int i=0; i<= max_k; i++){
		//~ if(counter[i]){
			//~ cout << (3.0*i)/n ;
			//~ cout << " "<<(float)dos_map[i] / counter[i] ;
			//~ cout <<" "<<(float) sd_map[i] / counter[i]<<endl;
		//~ }
	//~ }
	//~ return 1;
}
