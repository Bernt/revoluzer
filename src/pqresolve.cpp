/**
 * @author: Matthias Bernt
 * @todo: try maybe states
 */

#include <algorithm>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <fstream>

#include "crex.hpp"
#include "dstnc_bp.hpp"
#include "dstnc_com.hpp"
#include "dstnc_crex.hpp"
#include "geneset.hpp"
#include "io.hpp"
#include "pqrtree.hpp"
#include "character.hpp"
#include "tree.hpp"

//#define DEBUG
//#define DEBUG_EVAL
//#define DEBUG_COMBINE
//#define DEBUG_C2PQR
//#define DEBUG_CREXRESOLVE
//#define DEBUG_CREXDISTRESOLVE

#define DEFAULT_CIRCULAR true	// default circularity:true (linear:false)
#define DEFAULT_DIRECTED false  // default for directed:false (undirected:true)
#define DEFAULT_MAXSIZE 100000 // default maximum set size
#define DEFAULT_SGN_HANDLING DOUBLE	// default sign handling option
#define DEFAULT_DFOO ""	// default distance funtion
#define DEFAULT_STR 0 // default conflict strategy
#define DEFAULT_NPERM -2 // default number of permutations to enumerate

using namespace std;

/**
 * class storing the data for a node of a phylogenetic tree
 */
class pqrd{
	private:
		bool _directed;
	public:
		genom g;	// a genome corresponding to the pqr tree
		int dist;
		vector<int> states; // gene set absence presence characters
		vector<float> weights;	/* the weights of the characters */
		pqrtree<int> pqr;    /* a pqr tree */
		vector<string> *nmap;

	pqrd(){
		dist = numeric_limits< int >::max();
	}

	/**
	 * init a node, i.e. init the character vector to a given value and init a universal pqr tree with a certain nuber of leafes
	 * @param[in] n length of the sequences will init the pqr tree to 2n+2 leafs [0:2n+1]
	 * @param[in] size number of gene set characters to handle
	 * @param[in] value initial state of the characters
	 */
	pqrd( bool directed, int gssize, int value, float w, vector<string> *nm ){
		_directed = directed;
		states = vector<int>(gssize, value);
		weights = vector<float>(gssize, w);
		dist = numeric_limits< int >::max();
		nmap = nm;

	}

//	pqrd( genom g, vector<string> *nm ){
//		g = g;
//		dist = numeric_limits< int >::max();
//		nmap = nm;
//	}

	void dotedgeprop(const pqrd &parent, string &label);

	void dotnodeprop(string &fillcolor, string &label);
};

void pqrd::dotedgeprop(const pqrd &parent, string &label){

//	for( unsigned i=0; i<sstart; i+=2 ){
//		if( states[i] < 0 && parent.states[i] > 0 ){
//			label += "-"+(*nmap)[i/2];
//		}else if(states[i] > 0 && parent.states[i] < 0){
//			label += "+"+(*nmap)[i/2];
//		}
//	}

}
void pqrd::dotnodeprop(string &fillcolor, string &label){
//	unsigned def = 0,
//		uns = 0,
//		defn = 0;

//	// output the number of defined / unsure / undefined states
//	for( unsigned i=0; i<states.size(); i++ ){
//		if(states[i] < 0){
//			defn++;
//		}else if( states[i] == 0 ){
//			uns++;
//		}else{
//			def++;
//		}
//	}
//	label += "\\n+: "+int2string( def )+" ?"+int2string( uns )+" -"+int2string( defn );

	if(pqr.has_ndtype( RNODE )){
		fillcolor = "red";
	}else{
		if( g.size() > 0 ){
			fillcolor = "green";
		}else{
			fillcolor = "yellow";
		}
	}

}


/**
 * read command line options
 * @param[in] argc number of arguments
 * @param[in] argv the arguments
 * @param[out] fname input genome filename
 * @param[out] dname output dir name
 * @param[out] fname input tree filename
 * @param[in,out] circular the circularity of the genomes
 * @param[in,out] maxsize maximum set size
 * @param[in,out] signhand the sing handling function
 * @param[in,out] directed the directed (vs undirected) genome model
 * @param[in,out] dfoostr string description of distance function
 * @param[in,out] nperm max number of permutations to enumerate for dist resolve
 * @param[in,out] cs conflict strategy
 */
void getoptions( int argc, char *argv[], string &fname, string &dname, string &tname,
		bool &circular, unsigned &maxsize, SgnHandType &signhand, bool &directed,
		string &dfoostr, int &nperm, int &cs);

/**
 * print usage information and exit
 */
void usage();

/**
 * construct the pqrtrees from the characters
 * @param[in] node a nodes of the tree
 * @param[in] greedy
 * - 0: not greedy, i.e. apply all characters > 0
 * - 1: greedy, i.e. try to apply characters with the same support, in case of conflict revert all
 * - 2: greedy, i.e. try to apply single character, in case of conflict revert
 * @param[in] idx_chr list of gene sets defining the characters
 * @param[in] chr_idx character index to element set mapping
 * @param[in] n number of elements
 * @param[in] circular circularity
 * @param[in] directed directed permutations
 * @param[in] nmap name map
 * @param[in] sh sign handling mode
 * @return true iff the result is a resolved permutation
 */
bool characters2pqr( node<pqrd> * node, int greedy,
		const vector< geneset > &idx_chr, const map< geneset, int> &chr_idx,
		unsigned n, bool circular, bool directed,
		vector<string> *nmap, SgnHandType sh );

/**
 * the length of the character states vectors might become different
 * this function initialises the missing elements accordingly
 * for unresolved nodes with 0 and with -1 otherwise (only the gene set
 * characters might change)
 * @param[in,out] nv the node
 * @param[in] idx_chr the new characters
 */
void check_characters( node<pqrd> *nv, const vector<geneset> &idx_chr);

/**
 * combine sets of intervals
 * - in bottom up mode (parent unknown) combine the childs
 * - in top down mode add/combine the parent
 * @param[in] nd the node of the tree to process
 * @param[in] top_down mode iff true, bottom up iff false
 */
void combine( node<pqrd> *nd, bool top_down );

/**
 * get the score of the nodes (the sum of the distance to the childs)
 * @param[in] nodes the nodes
 * @param[out] score the score
 * @param[out] perm sum of the number of consistent permutations
 */
void eval_score( const vector<node<pqrd> *> &nodes, unsigned &score, unsigned &perm, bool circular, bool directed);

/**
 * try to resolve a node by minimising the distance to the neighbours
 * @param[in] nd the node
 * @param[in] directed directed genomes?
 * @param[in] dfoo distance function
 * @param[in] sh sign handling method
 * @param[in] nmap (short) name map
 * @param[in] nperm number of permutations to test max
 * @return 1 if resolved, 0 otherwise
 */
bool distance_resolve( node<pqrd> *nd, bool circular, bool directed, dstnc *dfoo, SgnHandType sh, vector<string> *nmap, int nperm );

/**
 * init the character vectors from the unsigned gene orders
 * @param[in] genomes the unsigned genomes
 * @param[in] n the larges gene
 * @param[in] circular the circularity of the gene orders
 * @param[in] complement applies to circular genomes only
 *  if true: use both of the complementary intervals
 * 	else: only one is used (the one which does not contain the first element
 *  of the first genome)
 * @param[in,out] maxsize the maximum size of the intervals. maxsize <= n must
 *  hold. if std::numeric_limits< unsigned >::max() is given the maximum is set automatically to the max,
 *  i.e. n-1;
 *  note: for circular genomes the actual size of the returned intervals
 *  might be bigger, but it holds that the smaller of the complementary
 *  pair is <= maxsize
 * @param[out] idx_chr mapping from the indices in the character vector to the
 * corresponding character (set)
 * @param[out] chr_idx mapping from the character (set) to the index in the
 * character vectors
 */
void init_characters(const vector<genom > &genomes, unsigned n, SgnHandType sh, bool circular,
		bool complement, unsigned &maxsize, vector< geneset > &idx_chr,
		map< geneset, int> &chr_idx );

/**
 * init the characters of a node (i.e. of the genome it defines)
 * @param[in,out] v the data of the node
 * @param[in] n the larges gene
 * @param[in] circular the circularity of the gene orders
 * @param[in] complement applies to circular genomes only
 *  if true: use both of the complementary intervals
 * 	else: only one is used (the one which does not contain the first element
 *  of the first genome)
 * @param[in,out] maxsize the maximum size of the intervals. maxsize <= n must
 *  hold. if std::numeric_limits< unsigned >::max() is given the maximum is set automatically to the max,
 *  i.e. n-1;
 *  note: for circular genomes the actual size of the returned intervals
 *  might be bigger, but it holds that the smaller of the complementary
 *  pair is <= maxsize
 * @param[out] idx_chr mapping from the indices in the character vector to the
 * corresponding character (set)
 * @param[out] chr_idx mapping from the character (set) to the index in the
 * character vectors
 */
void init_characters( node<pqrd> *v, unsigned n, SgnHandType sh,  bool circular, bool complement, unsigned &maxsize,
		vector< geneset > &idx_chr, map< geneset, int> &chr_idx );

/**
 * init the weights of the characters for one node
 * inner node: all 0
 * leaf: if state 1 -> 1.0 elso 0.0
 * @param[in] nv pointer to the node
 */
void init_weights( node<pqrd> *nv );

/**
 * print binary states
 * @param[in] c the states
 */
void print_states( const pqrd &c );

/**
 * print a set of binary states
 * @param[in] c the states
 */
void print_states( const vector<pqrd> &c );

/**
 * start working!!
 */
int main(int argc, char *argv[]) {
	bool circular = DEFAULT_CIRCULAR,		// circularity of the genomes
		directed = DEFAULT_DIRECTED;
	dstnc *dfoo = NULL;	// distance funtion to be used for resolving
	int n = 0, 			// the length of the genomes
		N = 0, 			// the length of the doubled / unsigned genomes
		cs = DEFAULT_STR,			// conflict strategy
		nperm = DEFAULT_NPERM;		// max number of permutations to test with dist resolve
	unsigned
//		score,
//		cperm,
		maxsize = DEFAULT_MAXSIZE, 	// maximum set size
		roundcnt = 1;	// round counter
	string odname = "", // output dir
		gfname = "",	// file containing the genomes
		tfname = "",	// file containing the trees
		outfname,
		dfoostr = DEFAULT_DFOO;
	vector<genom> genomes;	// the input genomes (leaves)
//	vector<vector<int> > genomes; // the input genomes (again)
//	vector<vector<char> > characters, // the characters (sets) of the leaves (given genomes)
//		echaracters;	// gene absence presence characters
	vector<pqrd> leaves;
	vector<string> names, 	// the names of the genomes/species
		nwktrees,			// the given newick formatted trees
		lnmap,nmap;				// namemap .. int -> string map
	set<int> genes;			// the set of genes
	SgnHandType sgnhand = DEFAULT_SGN_HANDLING; // sign handling option

	pqrd initv; 			// initial value to assign to the nodes
//	pqrd *tmpv;			// temp variable for pqrd data structure
	phy<pqrd> tree;	// the current phylogenetic trees
	vector<node<pqrd> *> po;// nodes of a tree in postorder
	pqrtree<int> pqr;			// a pqr tree for output

	vector< geneset > idx_chr;
	map< geneset, int> chr_idx;

		// variables for rearrangement reconstruction
	rrrmt *c;
	map<rrrmt *, unsigned, HDereferenceLess> rrrmtidx;
	set<rrrmt *, HDereferenceLess> allrrrmt;
		// get and check parameters
	getoptions( argc, argv, gfname, odname, tfname,
			circular, maxsize, sgnhand, directed, dfoostr, nperm, cs );

//	cerr << "# directed "<<directed<<endl;
//	cerr << "# circular "<<circular<<endl;

		// read the genomes and the trees
	read_genomes(gfname, genomes, names, circular, nmap, true, false);
#ifdef DEBUG_NONAMES
	nmap.clear();
#endif//DEBUG_NONAMES

	read_trees(tfname, nwktrees);

		// check if we got data to process
	if( genomes.size() == 0 ){
		cout << "no genomes in "<<gfname<<endl;
		usage();
	}
	if(nwktrees.size() == 0){
		cout << "no trees in "<<tfname<<endl;
		usage();
	}



	cerr << "# circular     : "<<circular<<endl;
	cerr << "# directed     : "<<directed<<endl;
	cerr << "# sign handling: "<<sgnhand<<endl;
	cerr << "# distance funtion: "<<dfoostr<<endl;
	cerr << "# conflict str : "<<cs<<endl;
	cerr << "# nperm        : "<<nperm<<endl;
	for(unsigned i=0; i<genomes.size(); i++){
		n = max((unsigned)n, genomes[i].size());
	}

	if( dfoostr == "bp" ){
		dfoo = new dstnc_bp( n, circular, directed );
	}else if( dfoostr == "int" ){
		dfoo = new dstnc_com( n, circular );
	}else if( dfoostr == "crex" ){
		dfoo = new dstnc_crex( n, true, false, 2 );
	}else if( dfoostr !="" ){
		cerr << "#unknown distance function "<<dfoostr<<endl;
		usage();
		exit(EXIT_FAILURE);
	}

	// make the genomes unsigned
	// - as usual -> genomes of length 2n + 2 (and construct new name mapping)
	// - or by forgetting the signs
	if( sgnhand == DOUBLE ){
#ifdef DEBUG_NONAMES
		double_genomes(genomes, NULL, directed);
#else
		lnmap = double_nmap(nmap, directed );
		double_genomes(genomes, &lnmap, directed);
#endif//DEBUG_NONAMES
	}else if( sgnhand == UNSIGN ){
#ifdef DEBUG_NONAMES
		unsign_genomes( genomes, NULL, directed );
#else
		lnmap = nmap;	// no long nmap. but the var is needed in both cases
		unsign_genomes( genomes, &nmap, directed );
#endif//DEBUG_NONAMES
	}

	for(unsigned i=0; i<genomes.size(); i++){
		N = max((unsigned)N, genomes[i].size());
	}

//	for(unsigned i=0; i<genomes.size(); i++){
//		cerr << ">" <<names[i]<<endl<<genomes[i]<<endl;
//	}

		// initialise the character sets
	init_characters(genomes, N, sgnhand, circular, false, maxsize, idx_chr, chr_idx);
	cerr << "# "<<genomes.size() << " genomes with max. length " << n <<endl;
	cerr << "# "<<idx_chr.size() << " characters" << endl;

		// init the data that will be assigned to the leaves
	initv = pqrd( directed, idx_chr.size(), 0, 0, &nmap );
	for(unsigned i=0; i<genomes.size(); i++){
		leaves.push_back( initv );
		leaves.back().g = genomes[i];
	}
	// the data for the inner nodes
	initv = pqrd( directed, idx_chr.size(), 0, 0.0, &nmap );

	// solve each tree
	for( unsigned i=0; i<nwktrees.size(); i++ ){
		// construct the tree and get the nodes in postorder
		tree = phy<pqrd>(nwktrees[i], names, leaves, false, false, initv);

		tree.postorder_nodes(po, true);

		cerr << "# "<<po.size() << " nodes"<<endl;
		unsigned incnt = 0;
		for( unsigned j=0; j<po.size(); j++ ){
			if(po[j]->is_leaf())
				incnt++;
		}
		cerr << "# "<<incnt << " inner nodes"<<endl;

		unsigned nresolve = 0;	// is there a new resolved node?
		do{
			// reset number of resolved nodes
			nresolve = 0;
			cerr <<"################## Round "<<roundcnt<<endl;
//#ifdef DEBUG
			cerr << "# Initialise characters ..."<<endl;
//#endif//DEBUG

			// define the binary states for the resolved nodes
			for( unsigned j=0; j!=po.size(); j++ ){
				init_characters( po[j], N, sgnhand, circular, false, maxsize, idx_chr, chr_idx );
			}
			for( unsigned j=0; j!=po.size(); j++ ){
				check_characters( po[j], idx_chr);
				init_weights( po[j] );
			}

//#ifdef DEBUG
			cerr << "# Bottom up phase ..."<<endl;
//#endif//DEBUG
			for(unsigned j=0; j<po.size(); j++){
				combine( po[j], false );
			}
//#ifdef DEBUG
			cerr << "# Top Down phase ..."<<endl;
//#endif//DEBUG
			for(int j=po.size()-2; j>=0; j--){
				combine(po[j], true);
			}

//#ifdef DEBUG
			cerr << "# Resolve with PQ tree ..."<<endl;
//#endif//DEBUG
			for(unsigned j=0; j<po.size(); j++){
#ifdef DEBUG_NONAMES
				if( characters2pqr(po[j], cs, idx_chr, chr_idx, N, circular, directed, NULL, sgnhand) ){
#else
				if( characters2pqr(po[j], cs, idx_chr, chr_idx, N, circular, directed, &lnmap, sgnhand) ){
#endif//DEBUG_NONAMES
					nresolve++;
				}
			}

			// if no new node was resolved,
			// then try to resolve with distance (if the user provided a distance funtion)
			if( dfoo != NULL && nresolve == 0 ){
//#ifdef DEBUG
				cerr << "# Resolve with distance ..."<<endl;
//#endif//DEBUG
//				IMPLEMENT CREX

//				USE PRE ORDER IN ORDER TO RESOLVE ONLY THE TOPMOST
//				UNRESOLVED NODES WITH THE DISTANCE METHOD
				for(int j=po.size()-1; j>=0; j--){
#ifdef DEBUG_NONAMES
					if( distance_resolve( po[j], circular, directed, dfoo, sgnhand, NULL, nperm ) ){
#else
					if( distance_resolve( po[j], circular, directed, dfoo, sgnhand, &nmap, nperm ) ){
#endif//DEBUG_NONAMES
//						cerr <<"#   distance resolved "<<po[j]->get_name()<<endl;
						nresolve++;
					}
				}
//				exit(EXIT_FAILURE);
			}


			cerr <<"# "<<nresolve<<" new resolved nodes"<<endl;
			roundcnt++;
		}while( nresolve > 0 );

		cout <<"################## FIN "<<endl;

		// iterate once through tree:
		// - undouble genomes
		// - get the set of rearrangements
		for( unsigned j=0; j<po.size(); j++ ){
			if( po[j]->get_value_ptr()->g.size() > 0 ){
//				cout << "in the tree "<<po[j]->get_name()<<" " <<po[j]->get_value_ptr()->g<<endl;

				if( sgnhand == DOUBLE ){
#ifdef DEBUG_NONAMES
					po[j]->get_value_ptr()->g = undouble_genom( po[j]->get_value_ptr()->g, NULL, directed);
#else
					po[j]->get_value_ptr()->g = undouble_genom( po[j]->get_value_ptr()->g, &nmap, directed);
#endif//DEBUG_NONAMES
				}

				if( circular ){
					if( sgnhand == DOUBLE ){
						po[j]->get_value_ptr()->g.normalize(1);
					}else if( sgnhand == UNSIGN && po[j]->get_value_ptr()->g[0] != 1 ){
						std::reverse(po[j]->get_value_ptr()->g.chromosom.begin(), po[j]->get_value_ptr()->g.chromosom.end());
					}
				}
			}
		}

		// iterate once to compute crex scenarios
		for( unsigned j=0; j<po.size(); j++ ){
			if( po[j]->get_parent() == NULL )
				continue;

//			cerr <<"=========== CREx " << po[j]->get_parent()->get_name()<<" -> " << po[j]->get_name()<<endl;
			pqrd *nd, *p;
			nd = po[j]->get_value_ptr();
			p = po[j]->get_parent()->get_value_ptr();
			if( nd->g.size() > 0 && p->g.size() > 0 ){
				c = new crex( p->g, nd->g, true, 1, false );
//				cout << *c << endl;
				c->getrrrmt( allrrrmt, ALTMIN );
				delete c;
			}
		}
		rrrmtidx = getrrrmtidx( allrrrmt );

		ofstream outf;

//		// output dot file
//		outfname = odname+"/tree"+int2string(i)+".dot";
//		outf.open( outfname.c_str() );
//		tree.todot( outf );
//		outf.close();

		// output tree
		outfname = odname+"/tree"+int2string(i)+".phy";
		outf.open( outfname.c_str() );
		outf<< tree<<endl;
		outf.close();

		// output orn file for nw_display
		outfname = odname+"/tree"+int2string(i)+".orn";
		outf.open( outfname.c_str() );
		tree.tonw_displaycss( outf );
		outf.close();

		// output css file for nw_display
		outfname = odname+"/tree"+int2string(i)+".css";
		outf.open( outfname.c_str() );
		for( unsigned j=0; j<po.size(); j++ ){

//			if( po[j]->get_parent() == NULL )
//				continue;

			pqrd *nd;
//				*p;
			nd = po[j]->get_value_ptr();
//			p = po[j]->get_parent()->get_value_ptr();

			if( !po[j]->is_leaf() ){
				if( nd->g.size() > 0){
//					cout<<n->g<<endl;
//					n->g.set_nmap( &lnmap );
					cout << "> "<<po[j]->get_name()<<endl;
					cout<<nd->g<<endl;
				}else{
//					cout << "]"<<po[j]->get_name()<<endl;
//					cout << n->pqr.permutations_count( directed ) <<endl;

					cout << "> "<<po[j]->get_name()<<endl;
#ifdef DEBUG_NONAMES
					nd->pqr.named_output( NULL, cout );
#else
					nd->pqr.named_output( &lnmap, cout );
#endif//DEBUG_NONAMES
					cout << endl;
				}
				outfname = odname+"/tree"+int2string(i)+"node"+po[j]->get_name()+".dot";
				ofstream outf( outfname.c_str() );
#ifdef DEBUG_NONAMES
				nd->pqr.todot(outf, NULL);
#else
				nd->pqr.todot(outf, &lnmap);
#endif//DEBUG_NONAMES
				outf.close();

//				if(n->pqr.has_rnode()){
//					cout << po[j]->get_name()<<" R"<<endl;
//				}else{
//					if( n->pqr.permutations_count() == 1 ){
//						cout << po[j]->get_name()<<" F"<<endl;
//					}else{
//						cout << po[j]->get_name()<<" P"<<endl;
//					}
//				}
			}

//			if( n->g.size() > 0 && p->g.size() > 0 && n->g != p->g ){
//				outf << "stroke:red Individual "<<po[j]->get_name()<<endl;
//			}
		}

		outf.close();
	}



/*
		// solve the trees
	for( unsigned i=0; i<nwktrees.size(); i++ ){
		score = 0;
		cperm = 0;


#ifdef DEBUG
		cout << "Bottom up phase ..."<<endl;
#endif//DEBUG
		for(unsigned j=0; j<po.size(); j++){
			combine( po[j], false );
		}
#ifdef DEBUG
		cout << "Top Down phase ..."<<endl;
#endif//DEBUG
		for(int j=po.size()-2; j>=0; j--){
			combine(po[j], true);
		}

		po.clear();
		tree.postorder_nodes(po, true);
			// get the correstponding pqr trees for all nodes in the tree
//		cout << "c2p "<<endl;
		characters2pqr(po, idx_chr, chr_idx, n, circular, lnmap, sgnhand);

			// get the number of binary state changes and the number of
			// consistent permutations in the tree
		eval_score(po, score, cperm);
		cout << "SCORE = "<<score << endl;
		cout << "CPERM = "<<cperm << endl;






//			cout<<po[j]->get_name() << " =>> " ;
//			opqr.named_output(nmap, cout); cout << endl;

//			outfname = odname+"/tree"+int2string(i)+"node"+po[j]->get_name()+".dot";
//			ofstream outf( outfname.c_str() );
//			opqr.todot(outf, &nmap);
//			outf.close();
//		}
//		vector<char> b = po.back()->get_value();
//		print_states(b);
//		copy(b.begin(), b.end(), ostream_iterator<int>(cout,""));cout<<endl;
//		vector< vector<char> > c = po.back()->get_child_values();
//		cout << c.size()<<endl;
	}*/

	for( set<rrrmt*, HDereferenceLess>::iterator it = allrrrmt.begin(); it != allrrrmt.end(); it++ ){
		delete *it;
	}
	allrrrmt.clear();
	nwktrees.clear();
	delete dfoo;
}

// *******************************************************************************************
//
// *******************************************************************************************

bool characters2pqr( node<pqrd> *node, int greedy,
		const vector< geneset > &idx_chr,
		const map< geneset, int> &chr_idx, unsigned n, bool circular,
		bool directed,
		vector<string> *nmap, SgnHandType sh ){

	if( node->get_value_ptr()->g.size() > 0 ){
#ifdef DEBUG_COMBINE
		cout << "already resolved "<<node->get_name()<<endl;
#endif//DEBUG_COMBINE
		return false;
	}
	cerr << "#    trying "<< node->get_name()<<endl;
	set<int> pqleafs;	// the leafs to be included in the pqrtree
	pqrd *v = node->get_value_ptr();	// the reconstructed states
	pqrtree<int> pqrcp, pqrcpg;	// pqrtree copies for reverting to pre conflict state

	map<vector<float>, vector<unsigned> > score_idx; 	// mapping from score to index
	vector<unsigned> score_ord;
	vector<float> score(3, 0.0);

	// initialise character (ie. gene set processing order)
	//	SORT BY
	//	1. STATE 1, 0, -1 (first insert the sure states)
	//	2. SIZE
	//	3. WEIGHT
	//	store the components in a float vector
	for( unsigned i=0; i<idx_chr.size(); i++ ){
//		if( v->states[i] > 0 ){
//			score[0] = -1;
//		}else if( v->states[i] < 0 ){
//			score[0] = 1;
//		}else{
//			score[0] = 0;
//		}
		score[0] = v->states[i];
		score[1] = v->weights[i];
		score[2] = idx_chr[i].size();
		if(circular){
			geneset comp = idx_chr[i];
			comp.complement();
			score[2] = min( score[2], (float)comp.size() );
		}

		if( score_idx.find( score ) == score_idx.end() ){
			score_idx[score]=vector<unsigned>();
		}
		score_idx[score].push_back( i );
	}

	for( map<vector<float>,vector<unsigned> >::reverse_iterator it=score_idx.rbegin(); it!=score_idx.rend(); it++ ){
//		cout << "= "; copy( it->first.begin(), it->first.end(), ostream_iterator<float>(cout," ") ); cout<<endl;
		score_ord.insert( score_ord.end(), it->second.begin(), it->second.end() );
	}
//	score_idx.clear();


	// get the element set for the current node
	// now insert the gene sets corresponding to 1 states
	for( unsigned k=0; k<v->states.size(); k++){
		if( v->states[k] > 0 ){
			set<int> tmp = idx_chr[k].get_set();
			pqleafs.insert(tmp.begin(), tmp.end());
		}
	}

#ifdef DEBUG_C2PQR
	cerr << "c2p "<<node->get_name()<<endl;
	cerr << "gene set: ";
	for(set<int>::const_iterator it=pqleafs.begin(); it!=pqleafs.end(); it++){
		if( nmap != NULL ){
			cerr << (*nmap)[*it];
		}
		cerr << "("<< *it <<") ";
	}
	cerr << endl;
#endif//DEBUG_C2PQR

	// initialise pqr tree
	// if sign handling mode is double -> init pairs 2i-1, 2i
	v->pqr = pqrtree<int>( pqleafs );
	if( sh == DOUBLE ){
//		set<int>::iterator mit = max_element(pqleafs.begin(), pqleafs.end());
		for( set<int>::const_iterator it=pqleafs.begin(); it!=pqleafs.end(); it++ ){
			if( (*it)%2!=0 && pqleafs.find((*it)+1)!=pqleafs.end() ){
				v->pqr.reduce((*it), (*it)+1);
			}
		}
	}

//	v->pqr.named_output( nmap, cout ); cout << endl;
//	cout << "start"<<endl;
//	v->pqr.print_leaves();
	for( map<vector<float>,vector<unsigned> >::reverse_iterator it=score_idx.rbegin(); it!=score_idx.rend(); it++ ){
		pqrcpg = pqrtree<int>( v->pqr );

		for( vector<unsigned>::iterator jt=it->second.begin(); jt!=it->second.end(); jt++ ){
			if( v->states[*jt] > 0 ){
				set<int> tmp = idx_chr[*jt].get_set();
#ifdef DEBUG_C2PQR
				cerr << "reduce ("<<v->states[*jt]<<","<<v->weights[*jt]<<") "<<idx_chr[*jt]<< endl;
#endif//DEBUG_C2PQR
				pqrcp = pqrtree<int>( v->pqr );
				v->pqr.reduce( tmp.begin(), tmp.end() );

	//			 * - 0: not greedy, i.e. apply all characters > 0
	//			 * - 2: greedy, i.e. try to apply single character, in case of conflict revert
				if( v->pqr.has_ndtype( RNODE ) ){
					if( ! pqrcp.has_ndtype( RNODE ) ){
						cerr << "#    - new conflict introduced "<< v->states[*jt];
						if( greedy == 0 ) cerr <<" (not showing add. conflicts)"<< endl;
						else cerr << endl;
					}
					if( greedy == 1 ){
						cerr << "#    - conflicting state "<< v->states[*jt]<< endl;
						cerr << "#      skipping remaining with the same support"<<endl;
						break;
					}

					if( greedy == 2 ){
						cerr << "#    - conflicting state ignored "<< v->states[*jt]<< endl;
						v->pqr = pqrcp;
					}else{
						//	cerr << v->states[k]<< " no conflict"<<endl;
					}
				}
#ifdef DEBUG_C2PQR
				v->pqr.named_output( nmap, cerr ); cerr << endl;
#endif//DEBUG_C2PQR
			}
			// greedy 1: revert the consequences of all characters with the current support
			if( v->pqr.has_ndtype( RNODE ) ){
				if( greedy == 1 ){
					cerr << "#    - conflicting states remove all with "<< it->first[0]<< endl;
					v->pqr = pqrcpg;
				}
			}
		}
	}

//	// now insert the gene sets corresponding to 1 states
//	for( unsigned i=0; i<score_ord.size(); i++){
//		unsigned k = score_ord[i];
//		if( v->states[k] > 0 ){
//			set<int> tmp = idx_chr[k].get_set();
//#ifdef DEBUG_C2PQR
////			if (idx_chr[k].size() > 2){
////				geneset x = idx_chr[k];
////				x.complement();
////				cerr << "reduce "<<x<<" : "<<  idx_chr[k] << endl;
////			}else{
//				cerr << "reduce ("<<v->states[k]<<","<<v->weights[k]<<") "<<idx_chr[k]<< endl;
////			}
//#endif//DEBUG_C2PQR
//			pqrtree<int> pqrcp = pqrtree<int>( v->pqr );
//			v->pqr.reduce( tmp.begin(), tmp.end() );
//
////			 * - 0: not greedy, i.e. apply all characters > 0
////			 * - 1: greedy, i.e. try to apply single character, in case of conflict revert
//
//			if( v->pqr.has_ndtype( RNODE ) ){
//				cerr << "# conflict warning "<< v->states[k]<< endl;
//				if( greedy == 1 ){
//					v->pqr = pqrcp;
//				}else{
//					//	cerr << v->states[k]<< " no conflict"<<endl;
//				}
//			}
//
//#ifdef DEBUG_C2PQR
//			v->pqr.named_output( nmap, cerr ); cerr << endl;
//#endif//DEBUG_C2PQR
//		}
//	}

//		cout << v->pqr<<endl<<endl;
//		v->pqr.collapse(2*n+1);
//		v->pqr.print_leaves();

	// if there is only one permutation then set it as result



	unsigned pcnt = v->pqr.permutations_count(directed);

//	cerr <<pcnt<<endl;
	if( pcnt == 1 ){
		v->g = genom(v->pqr.permutation(), 0 );
		v->g.set_nmap( nmap );

		if( circular && v->g[0] != 1){
			std::reverse(v->g.chromosom.begin(), v->g.chromosom.end());
		}
//		cout << "found "<<v->g<<endl;
		cerr <<"#    -> resolved ("<<node->get_name()<<")"<<endl;
		return true;
	}else{
		cerr <<"#    -> unresolved ("<<node->get_name()<<")"<<endl;
		return false;
	}
}

// *******************************************************************************************
//
// *******************************************************************************************
void combine( node<pqrd> *nd, bool top_down ){
	pqrd *p, 			// the parent
		*v;				// the value of the node
	vector< pqrd *> c; 	// the childs

#ifdef DEBUG_COMBINE
	vector<string> n;
	n = nd->get_child_names();
	cout << "COMBINE ";
	if (top_down)
		cout << "topdown ";
	else
		cout << "bottomup ";
	for(unsigned i=0; i<n.size(); i++)
		cout << n[i]<<" ";
	cout << endl;
#endif//DEBUG_COMBINE

	v = nd->get_value_ptr();
	if( v->g.size() > 0 ){
#ifdef DEBUG_COMBINE
		cout << "already resolved"<<endl;
#endif//DEBUG_COMBINE
		return;
	}

	p = nd->get_parent()->get_value_ptr();
	for( vector<node<pqrd>*>::iterator it = nd->get_child_begin(); it!=nd->get_child_end(); it++ ){
		c.push_back( (*it)->get_value_ptr() );
	}

#ifdef DEBUG_COMBINE
	cout << "p "<<p->states.size()<<endl;
	cout << "v "<<v->states.size()<<endl;
	for( unsigned i=0; i<c.size(); i++ ){
		cout << "c"<<i<<" "<<c[i]->states.size()<<endl;
	}
#endif//DEBUG_COMBINE

//	cout << "combine"<<endl;
//	for(unsigned i=0; i<c.size(); i++){
//		copy(c[i].begin(), c[i].end(), ostream_iterator<int>(cout, ""));
//		cout << endl;
//	}

	for( unsigned i=0; i<v->states.size(); i++ ){
		if(!top_down){	// bottom up
			for(unsigned j=0; j<c.size(); j++){
				v->weights[i] += c[j]->weights[i];

				if( c[j]->states[i] > 0 ){
					v->states[i]++;
				}else if(c[j]->states[i] < 0) {
					v->states[i]--;
				}
			}
			v->weights[i] /= c.size();
		}else{// top down
			// if the character was not rejected or supported by all childs use the outroup info
			if( abs(v->states[i]) != c.size()  && abs(v->states[i]) != 0 ){
				if( p->states[i] > 0 ){
					v->states[i]++;
				}else if( p->states[i] < 0 ){
					v->states[i]--;
				}
			}
		}
	}

//	copy(v.begin(), v.end(), ostream_iterator<int>(cout, ""));
//	cout << endl;

//	nd->set_value( v );
}

// *******************************************************************************************
//
// *******************************************************************************************

bool distance_resolve( node<pqrd> *nd, bool circular, bool directed, dstnc *dfoo,
		SgnHandType sh, vector<string> *nmap, int nperm){

	genom p;
	pqrd *v = NULL;				// the value of the node
	vector<genom> genomes;
	vector<pair<unsigned,unsigned> > d;	// vectors to hold the computed distance
	vector<genom> c;
	vector<vector<int> > perm;
	vector<string> *lnmap = NULL;

	v = nd->get_value_ptr();

	// check if the node itself need to be resolved
	if( nd->get_value_ptr()->g.size() > 0 ){
#ifdef DEBUG_CREXDISTRESOLVE
		cout << "already resolved"<<endl;
#endif//DEBUG_CREXDISTRESOLVE
		return false;
	}
	//#ifdef DEBUG_CREXDISTRESOLVE
		cerr << "#    distance resolve "<<nd->get_name()<<endl;
	//#endif//DEBUG_CREXDISTRESOLVE

	// check if the node has linear nodes only
	if( nperm == -2 && ( v->pqr.cnt_ndtype( PNODE ) +  v->pqr.cnt_ndtype( RNODE ) > 0 ) ){
//#ifdef DEBUG_CREXRESOLVE
		cerr << "#    has P/R nodes "<<endl;
//#endif//DEBUG_CREXDISTRESOLVE
		return false;
	}

	if( nperm > -1 && (unsigned)nperm < v->pqr.permutations_count( directed ) ){
//#ifdef DEBUG_CREXRESOLVE
		cerr << "#     too many permutations"<<endl;
//#endif//DEBUG_CREXDISTRESOLVE
		return false;
	}



	// check if the children are resolved
	for( vector<node<pqrd>*>::iterator it = nd->get_child_begin();
			it!=nd->get_child_end(); it++ ){
		c.push_back( (*it)->get_value_ptr()->g );
		if(lnmap == NULL){
			lnmap = c.back().get_nmap();
		}
		if( c.back().size() == 0 ){
#ifdef DEBUG_CREXDISTRESOLVE
			cout << "unresolved child node "<<endl;
#endif//DEBUG_CREXDISTRESOLVE
			return false;
		}
		if( sh == DOUBLE ){
			c.back() = undouble_genom( c.back(), nmap, directed);
		}
		if( circular ){
			if( sh == DOUBLE ){
				c.back().normalize(1);
			}else if( sh == UNSIGN && c.back()[0] != 1 ){
				std::reverse(c.back().chromosom.begin(), c.back().chromosom.end());
			}
		}
#ifdef DEBUG_CREXDISTRESOLVE
		cerr << "> child"<<endl<< c.back() <<endl;
#endif//DEBUG_CREXDISTRESOLVE

	}

	if( nd->get_parent() != NULL ){
		p = nd->get_parent()->get_value_ptr()->g;
		if(p.size() > 0){
			if( sh == DOUBLE ){
				p = undouble_genom( p, nmap, directed);
			}
			if( circular ){
				if( sh == DOUBLE ){
					p.normalize(1);
				}else if( sh == UNSIGN && p[0] != 1 ){
					std::reverse(p.chromosom.begin(), p.chromosom.end());
				}
			}
			p.set_nmap( lnmap );
		}
#ifdef DEBUG_CREXDISTRESOLVE
		cerr << "> parent"<<endl<< p <<endl;
#endif//DEBUG_CREXDISTRESOLVE
	}


	v->pqr.permutations(perm, directed);
//	cerr << perm.size()<<" permutations"<<endl;
//	if( perm.size() <= 1 ){
//		v->pqr.named_output( lnmap, cerr ); cerr << endl;
//	}

//	for(unsigned i=0; i<perm.size(); i++){
//		cout << i+1 <<"/"<<perm.size()<<" : ";
//		copy( perm[i].begin(), perm[i].end(), ostream_iterator<int>(cout," ") ); cout << endl;
//	}

	for(unsigned i=0; i<perm.size(); i++){
		d.push_back( make_pair(0, i) );
		genomes.push_back( genom( perm[i], 0 ) );
		if( sh == DOUBLE ){
			genomes.back() = undouble_genom( genomes.back(), nmap, directed);
		}
		if( circular ){
			if( sh == DOUBLE ){
				genomes.back().normalize(1);
			}else if( sh == UNSIGN && genomes.back()[0] != 1 ){
				std::reverse(genomes.back().chromosom.begin(), genomes.back().chromosom.end());
			}
		}
		genomes.back().set_nmap( lnmap );
	}

	sort(d.begin(), d.end());

	for(unsigned i=0; i<genomes.size(); i++){
#ifdef DEBUG_CREXDISTRESOLVE
		cerr << ">permutation "<<i<<"/"<<perm.size()<<endl;
		cerr << genomes[i]<<endl;
#endif//DEBUG_CREXDISTRESOLVE

		if( p.size() > 0 ){
			d[i].first += dfoo->calc( p, genomes[i] );
		}

		for( unsigned j=0; j<c.size(); j++ ){
			d[i].first += dfoo->calc( genomes[i], c[j] );
		}
	}

	sort( d.begin(), d.end() );

//	cerr << "DIST-"<< *dfoo <<" ("<< nd->get_name()<<") ";
//	for( unsigned i=0; i<d.size(); i++ )
//		cerr << "("<<d[i].first<<","<<d[i].second<<") ";
//	cerr <<endl;
//	e = d;


	if( d.size()==1 || d[0].first < d[1].first ){
		cerr << "#    resolved "<<nd->get_name()<< " ["<<*dfoo<<"] min = "<<d[0].first;
		if( d.size() != 1 )
			cerr << " < "<<d[1].first;
		cerr << " (out of "<<perm.size()<<")"<<endl;

		v->g = genom( perm[d[0].second], 0 );
		v->g.set_nmap( lnmap );
		if( circular && v->g[0] != 1 ){
			std::reverse(v->g.chromosom.begin(), v->g.chromosom.end());
		}
//		cout << "found "<<v->g<<endl;
		return true;
	}else{	// no unique minimum of the distance
//		cerr << "# no uniue distance min "<<endl;
		cerr << "#    NOT resolved "<<nd->get_name()<< " ["<<*dfoo<<"] min = "<<d[0].first;
		unsigned ecnt = 0;
		for( unsigned i=0; i<d.size(); i++ ){
			if( d[i].first == d[0].first ){
				ecnt++;
			}
		}
//		if( d.size() != 1 )
//			cerr << " >= "<<d[1].first;
		cerr << " ("<<ecnt<<" out of "<<perm.size()<<")"<<endl;

		return false;
	}
}


// *******************************************************************************************
//
// *******************************************************************************************

void eval_score( const vector<node<pqrd > *> &nodes, unsigned &score, unsigned &perm,
		bool circular, bool directed ){
	int dist = 0;			// distance from a child to the node
	pqrd *n,				// the characters of a node
		*c;					// the characters of a child
	vector<node<pqrd> *> child;	// the child nodes

	for(unsigned i=0; i<nodes.size(); i++){

#ifdef DEBUG_EVAL
		cout << "eval node "<<nodes[i]->get_name()<<endl;
#endif//DEBUG_EVAL
		n = nodes[i]->get_value_ptr();
		child = nodes[i]->get_childs();

		for(unsigned j=0; j<child.size(); j++){
			c = child[j]->get_value_ptr();
			for( unsigned k=0; k<n->states.size(); k++){
				if( ( n->states[k] >= 0 && c->states[k] < 0) || (n->states[k] < 0 && c->states[k] >= 0)){
					dist++;
				}
			}
#ifdef DEBUG_EVAL
			cout << "\t";
			for(unsigned k=0; k<n.size(); k++){
				cout << k%10;
			}
			cout << endl;
			cout << nodes[i]->get_name()<<"\t";print_states( n );
			cout << child[j]->get_name()<<"\t";print_states( *c );
			cout << "dist "<<dist<<endl;
#endif//DEBUG_EVAL
			score += dist;
//			child[j]->set_dist(dist);
			c->dist = dist;
			dist = 0;
		}

		perm += n->pqr.permutations_count( directed );
	}
}

// *******************************************************************************************
// init the character vector
// *******************************************************************************************
void init_characters(const vector<genom > &genomes, unsigned n, SgnHandType sh, bool circular, bool complement,
		unsigned &maxsize, vector< geneset > &idx_chr, map< geneset, int> &chr_idx ){
	set<geneset> gs;	// the gene sets of a genome

	set<int> tmpset;

	idx_chr.clear();
	chr_idx.clear();

	for(unsigned i=0; i<genomes.size(); i++){
		gs.clear();
		getgenesets(genomes[i], n, circular, complement, sh, maxsize, false, inserter(gs, gs.begin()));
		// artificially add the pair (g[0],g[1])
		if( circular ){
			geneset tmpset(genomes[i].get_nmap(), n, genomes[i].chromosom);
			tmpset.insert(genomes[i][0]);
			tmpset.insert(genomes[i][1]);
			gs.insert( tmpset );
		}

		for( set<geneset>::const_iterator it=gs.begin(); it!=gs.end(); it++ ){
			if( chr_idx.find(*it) == chr_idx.end() ){
				chr_idx[*it] = idx_chr.size();
				idx_chr.push_back( *it );
			}
		}
	}
}

// *******************************************************************************************

void init_characters( node<pqrd> *nv, unsigned n, SgnHandType sh, bool circular, bool complement, unsigned &maxsize,
		vector<geneset> &idx_chr, map< geneset, int> &chr_idx){

	pqrd *v = nv->get_value_ptr();

	if( v->g.size() == 0 ){ 	// unresolved, i.e. no genome set -> init with 0
		fill(v->states.begin(), v->states.end(), 0);
		return;
	}else{
		fill(v->states.begin(), v->states.end(), -1);
		if( v->states.size() < idx_chr.size() ){
			v->states.insert(v->states.end(), idx_chr.size() - v->states.size(), -1);
		}
//		for( unsigned i=0; i<v->g.size(); i++ ){
//			v->states[ abs(v->g[i]) ] = ( v->g[i] >= 0 ) ? 1 : -1;
//		}
	}

//	cerr << "init characters "<<endl;
//	cerr << "genome "<<v->g<<endl;

	set<geneset> gs;	// the gene sets of a genome
	getgenesets(v->g, n, circular, complement, sh, maxsize, false, inserter(gs, gs.begin()));

	// artificially add the pair (g[0],g[1])
	if( circular ){
		geneset tmpset( v->g.get_nmap(), n, v->g.chromosom );
		tmpset.insert( v->g[0] );
		tmpset.insert( v->g[1] );
		gs.insert( tmpset );
	}


	for( set<geneset>::const_iterator it=gs.begin(); it!=gs.end(); it++ ){
//		cerr << "processing "<<*it<<endl;
		if( chr_idx.find(*it) == chr_idx.end() ){
			cerr << "#warning: previously unknown character found: "<<*it<<endl;
			chr_idx[*it] = idx_chr.size();
			idx_chr.push_back( *it );
			v->states.push_back( 1 );
		}else{
//			cerr << "write it to"<<chr_idx[*it] + v->gsstart<<endl;
			v->states[ chr_idx[*it] ] = 1;
		}
	}

//	for( map< geneset, int>::const_iterator it = chr_idx.begin(); it!=chr_idx.end(); it++ ){
//		cerr << it->first<<" : "<< v->states[ it->second ] <<endl;
//	}
}

// *******************************************************************************************

void init_weights( node<pqrd> *nv ){
	pqrd *v = nv->get_value_ptr();

	v->weights = vector<float>( v->states.size(), 0.0 );
	if( nv->is_leaf() ){
		for( unsigned i=0; i<v->states.size(); i++ ){
			if( v->states[i] == 1 ){
				v->weights[i] = 1.0;
			}
		}
	}
}

// *******************************************************************************************

void check_characters( node<pqrd> *nv, const vector<geneset> &idx_chr){

	if(nv == NULL){
		cerr << "error: check_characters NULL node"<<endl;
		exit(EXIT_FAILURE);
	}

	pqrd *v = nv->get_value_ptr();

	if(v == NULL){
		cerr << "error: check_characters NULL data"<<endl;
		exit(EXIT_FAILURE);
	}

	while( v->states.size() < idx_chr.size() ){
		if( v->g.size() == 0 )		// unresolved nodes are (re)initialised with 0
			v->states.push_back(0);
		else						// resolved nodes with -1 (we know that the char is absent)
			v->states.push_back(-1);
	}
}

// *******************************************************************************************
// print binary states
// *******************************************************************************************
void print_states( const vector<char> &c ){
	for(unsigned i=0; i<c.size(); i++){
		if( c[i] > 0 )
			cout << "+";
		else if( c[i] < 0 )
			cout << "-";
		else
			cout << "?";
	}
	cout << endl;
}

// *******************************************************************************************
// print binary states
// *******************************************************************************************
void print_states( const vector<vector<char> > &c ){
	for(unsigned i=0; i<c.size(); i++){
		print_states(c[i]);
	}
}



void getoptions( int argc, char *argv[], string &fname, string &dname, string &tname,
		bool &circular, unsigned &maxsize, SgnHandType &signhand, bool &directed,
		string &dfoostr, int &nperm, int &cs){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"adj",     no_argument,       0, 'a'},
			{"str", required_argument, 0, 'c'},
			{"dist",    required_argument, 0, 'e'},
			{"file",    required_argument, 0, 'f'},
			{"help",    no_argument,       0, 'h'},
			{"lindir",  no_argument,	   0, 'l'},
			{"linund",  no_argument,	   0, 'u'},
			{"maxsize", required_argument, 0, 'm'},
			{"nperm", required_argument, 0, 'n'},
			{"sign",    required_argument, 0, 's'},
            {0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "d:f:hm:s:t:",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'a':
				maxsize = 2;
				break;
			case 'c':
				cs = atoi( optarg );
				break;
			case 'd':
				dname = optarg;
				break;
			case 'e':
				dfoostr = optarg;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case 'l':
				circular = false; directed = true;
				break;
			case 'm':
				maxsize = atoi( optarg );
				break;
			case 'n':
				nperm = atoi( optarg );
				break;
			case 's':
				if( strcmp ( optarg, "double" ) == 0 ){
					signhand = DOUBLE;
				}else if( strcmp ( optarg, "unsign" ) == 0 ){
					signhand = UNSIGN;
				}else{
					cerr << "unknown sign handling function: "<<optarg<<endl;
					usage();
				}
				break;
			case 't':
				tname = optarg;
				break;
			case 'u':
				circular = false; directed = false;
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

    if( circular && directed ){
    	cerr << "circular directed not implemented"<<endl;
    	exit(EXIT_FAILURE);
    }

		// check parameters
	if( fname == "" ){
		cerr << "no file given"<<endl;
		usage();
	}
		// check parameters
	if(fname == ""){
		cerr << "no genome file given ! "<<endl;
		usage();
	}

	if(tname == ""){
		cerr << "no tree file given ! "<<endl;
		usage();
	}

	if(dname == ""){
		cerr << "no out directorygiven ! "<<endl;
		usage();
	}
}

// *******************************************************************************************
//
// *******************************************************************************************

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cerr << "pqresolve -f FILE -d outdir -t treefile [OPTIONS]"<<endl;
	cerr << "--help -h: print this message"<<endl;
	cerr << "--lindir: assume linear directed genomes"<<endl;
	cerr << "--linund: assume linear undirected genomes"<<endl;
	cerr << "-m MAXS : set max interval size to MAXS (default: ";
	if(DEFAULT_MAXSIZE == std::numeric_limits< unsigned >::max())cerr <<"auto";
	else cerr<<DEFAULT_MAXSIZE;
	cerr << " )"<<endl;
	cerr << "--adj   : adjacencies (default interval)"<<endl;
	cerr << "--sign -s : sign handling. default: ";
	if( DEFAULT_SGN_HANDLING == DOUBLE ) cerr << "double";
	else if( DEFAULT_SGN_HANDLING == UNSIGN ) cerr << "unsign";
	else{
		cerr << "error: undefined DEFAULT_SGN_HANDLING "<<DEFAULT_SGN_HANDLING<<endl;
		exit(EXIT_FAILURE);
	}
	cerr << endl;
	cerr << "--str : conclict strategy (default: \""<<DEFAULT_STR<<"\""<<endl;
	cerr << "         0: accept conflicts"<<endl;
	cerr << "         1: revert all conflicts with the same support"<<endl;
	cerr << "         2: revert every conflict"<<endl;

	cerr << "--dist : distance function for resolving conflicting ancestral states (default: \""<<DEFAULT_DFOO<<"\""<<endl;
	cerr << "         bp: breakpoint distance, int: common intervals, crex: CREx"<<endl;
	cerr << "         if unset the no resolving by distance"<<endl;

	cerr << "--nperm : up to how many permutations consistent with a node's PQ-Tree start distance resolving (default: \""<<DEFAULT_NPERM<<"\""<<endl;
	cerr << "         a number greater or equal 0: specifies the max explicitely"<<endl;
	cerr << "         -1: unlimited "<<endl;
	cerr << "         -2: only for linear trees "<<endl;


	exit( EXIT_FAILURE );
}
