/*
 * treedegree.cpp
 *
 *  Created on: Jan 25, 2011
 *      Author: maze
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#include "tree.hpp"

using namespace std;

void usage();

int main(int argc, char *argv[]){
	bool rt = false;	// trees are unrooted and have to be rooted
	ifstream file;
	string line;
	vector<phy<unsigned> > trees;
	vector<string> fnames;

		// get and check parameters
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'f': {i++; fnames.push_back( argv[i] ); break;}
			case 'h': {usage(); break;}
			case 'u': {rt=true;break;}
			default:{
				cout << "unknown parameter "<<argv[i]<<endl;
				usage();
			}
		}
	}

	for(unsigned i=0; i<fnames.size(); i++){
		file.open (fnames[i].c_str(), ifstream::in);
		if(!file.is_open()){
			cout << "could not open "<< fnames[i]<<endl;
			usage();
		}

	    while ( file.good() ){
			getline(file, line);
			if(line[0] == '('){
//				 cout << line << endl;
				trees.push_back( phy<unsigned>(line, rt) );
//				cout << "# "<< trees.back()<<endl;
				// break;
			}
	    }
		file.close();
		file.clear();
	}

	for(unsigned i=0; i<trees.size(); i++){

		vector<node<unsigned> *> nds;
		trees[i].preorder_nodes( nds, false);
		for( unsigned j=0; j<nds.size(); j++ ){
			cout << nds[j]->get_child_size()<<endl;
		}
	}
}

void usage(){
	cout << "robfould [-f filename]*"<<endl;
	exit(0);
}
