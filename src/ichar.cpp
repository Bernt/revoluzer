/**
 * program to print out the binary data of interval absence / presence
 * in nexus format
 * @author M. Bernt
 */

#include <numeric>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <vector>

#include "io.hpp"
//#include "chrctr/chrctr.hpp"
//#include "chrctr/comp_lp.hpp"
#include "geneset.hpp"
#include "geneset-chrctr.hpp"
#include "tree.hpp"

#define DEFAULT_MAXSIZE 100000 // default maximum set size
#define DEFAULT_ADJACENCY false	// default for the use of specialized adjacency methods
#define DEFAULT_CIRCULAR true	// default circularity:true (linear:false)
#define DEFAULT_DIRECTED false  // default for directed:false (undirected:true)
#define DEFAULT_SGN_HANDLING DOUBLE	// default sign handling option
#define DEFAULT_CONSTANT false	// default for inclusion of constant characters
#define DEFAULT_UNINF ""	// default for inclusion of uninformative characters
#define DEFAULT_NEXUS true	// default for printung nexus file
#define DEFAULT_COMP ""	// default method for determining compatible subset
#define DEFAULT_ILPLIM std::numeric_limits< int >::max() // time limit for ILP solver
#define DEFAULT_WEIGHT_CHAR "prob"	// character weighting funtion
#define DEFAULT_WEIGHT_SPLIT "binomial"	// split weighting funtion



using namespace std;




/**
 * read command line options
 * @param[in] argc number of arguments
 * @param[in] argv the arguments
 * @param[out] fname input filename
 * @param[in,out] circular the circularity of the genomes
 * @param[in,out] signhand the sing handling function
 * @param[in,out] adjacency use adjacencies
 * @param[in,out] directed the directed (vs undirected) genome model
 * @param[in,out] maxsize maximum set size
 * @param[in,out] otype output type
 * @param[in,out] constant allow constant parameters
 * @param[in,out] uninf allow uninformative parameters
 * @param[in,out] nexus output as nexus / plain
 * @param[in,out] comp method for determining compatible subset
 * @param[in,out] ilplim time limit for ILP solver
 * @param[in,out] n permutation length (for prob. estimation)
 * @param[in,out] rep number of repetions (for prob. estimation)
 * @param[in,out] wfoos split weighting function
 * @param[in,out] wfooc character weighting function
 */
void getoptions( int argc, char *argv[], string &fname, bool &circular,
		SgnHandType &signhand, bool &adjacency,
		bool &directed, unsigned &maxsize, string &otype,
		bool &constant, string &uninf, bool &nexus, string &comp, int &ilplim,
		unsigned &n, unsigned &rep, string &wfoos, string &wfooc);

/**
 * print usage and exit
 */
void usage( );

/**
 * main
 */
int main(int argc, char *argv[]){

	bool adjacencies = DEFAULT_ADJACENCY,	// adjacency mode
		circular = DEFAULT_CIRCULAR,		// circularity of the genomes
		directed = DEFAULT_DIRECTED,
		constant = DEFAULT_CONSTANT,		// allow constant characters
		nexus = DEFAULT_NEXUS;			// print nexus file
	int ilplim = DEFAULT_ILPLIM;	// time limit for ilp solver
	string fname, 						// file name of the input
		otype,
		comp = DEFAULT_COMP,			// method for determining compatible subset
		uninf = DEFAULT_UNINF;			// allow uninformative characters
	SgnHandType sgnhand = DEFAULT_SGN_HANDLING; // sign handling option
	unsigned minobs = 0,	// number of intervals per genome
		maxsize = DEFAULT_MAXSIZE, 	// maximum set size
		n = 0, 						// permutation length (for prob. estimation)
		m = 0,						// number of genomes
		rep = 0;					// number of repeats (for prob. estimation)
	splitsystem *spltsys = NULL;
	split_weight *spltwfoo = NULL;
	character_weight *charwfoo = NULL;
	string wfoos = DEFAULT_WEIGHT_SPLIT,
			wfooc = DEFAULT_WEIGHT_CHAR;
	vector<genom> genomes;	// the gene orders
	vector<string> names,	// names of the species
		nmap, lnmap;	    // names of the genes

	// get command line options
	getoptions( argc, argv, fname, circular, sgnhand, adjacencies,
			directed, maxsize, otype, constant, uninf, nexus, comp, ilplim,
			n, rep, wfoos, wfooc);

//	// if n and a number of repetitions is given
//	// -> construct artificial gene orders
//	if( n!=0 && rep!=0 ){
//
//		map<unsigned,unsigned> stat;
//		for( unsigned r=0; r<rep; r++ ){
//
//			if( r%1000 == 0 ){
//				cerr << r <<"/"<<rep<<"         \r";
//			}
//			genomes.push_back( genom(n, 0) );
//			genomes.push_back( genom(n, 0) );
//			genomes[1].randomise();
//
////			cerr << genomes[1]<<endl;
//
//			// make the genomes unsigned
//			// - as usual -> genomes of length 2n + 2 (and construct new name mapping)
//			// - or by forgetting the signs
//			if( sgnhand == DOUBLE ){
////				lnmap = double_nmap(nmap, directed );
//				double_genomes(genomes, NULL, directed);
//		//		double_genomes(genomes, NULL, directed);
//			}else if( sgnhand == UNSIGN ){
////				lnmap = nmap;	// no long nmap. but the var is needed in both cases
//				unsign_genomes( genomes, NULL, directed );
//			}
//
////			// inititialise the characters
//			spltsys = new interval_splitsystem( genomes, sgnhand, circular,
//					adjacencies, maxsize, minobs, true, spltwfoo );
//			bool zero = true;
///*			for( unsigned i=0; i<data.size(); i++ ){
//				if( ! data[i].isconstant() ){
//					continue;
//				}
//
//				zero = false;
////				if( stat.find(data[i].getobscnt() ) == stat.end()  ){
////					stat[data[i].getobscnt()]=0;
////				}
////				stat[ data[i].getobscnt() ] ++;
//				cout << data[i].getobscnt()<<endl;
//
//			}
//			if(zero){
////				if( stat.find( 0 ) == stat.end()  ){
////					stat[0]=0;
////				}
////				stat[ 0 ]++;
//				cout << 0 <<endl;
//			}*/
//
//
//			genomes.clear();
//			delete spltsys;
//		}
//		cerr << endl;
//
////		unsigned sum = 0;
////		for( map<unsigned, unsigned>::const_iterator it=stat.begin(); it!= stat.end(); it++ ){
////			cout<< it->first <<" "<<it->second<<endl;
////			sum += it->second;
////		}
////		if( sum != rep ){
////			cerr << "error: not all repetitions have been counted"<<endl;
////			exit(EXIT_FAILURE);
////		}
//
//		return EXIT_SUCCESS;
//	}

	// read the genomes from the file
	read_genomes(fname, genomes, names, circular, nmap);
	//	for( unsigned i=0; i<genomes.size(); i++ ){
	//		cerr << "> "<< names[i]<< endl;
	//		cerr << genomes[i] <<endl;
	//	}

	m = genomes.size();
	n = genomes[0].size();

	cerr << "# m = "<<m<<endl;
	cerr << "# n = "<<n<<endl;

	// init character weighting
	if( wfooc == "unit" ){
		charwfoo = new character_weight_unit();
	}else if( wfooc == "prob" ){
		charwfoo = new geneset_weight(n, circular, sgnhand, directed);
	}else{
		cerr << "unknown character weight function: "<<wfoos<<endl;
		usage();
	}

	// init split weighting
	if( wfoos == "unit" ){
		spltwfoo = new split_weight_unit( charwfoo );
	}else if(wfoos == "linsup"){
		spltwfoo = new split_weight_support( charwfoo );
	}else if(wfoos == "binomial"){
		spltwfoo = new split_weight_binomial( charwfoo, m );
	}else{
		cerr << "unknown split weight function: "<<wfoos<<endl;
		usage();
	}

	// make the genomes unsigned
	// - as usual -> genomes of length 2n + 2 (and construct new name mapping)
	// - or by forgetting the signs
	if( sgnhand == DOUBLE ){
#ifdef DEBUG_NONAMES
		lnmap = nmap;
		double_genomes(genomes, NULL, directed);
#else
		lnmap = double_nmap(nmap, directed );
		double_genomes(genomes, &lnmap, directed);
#endif//DEBUG_NONAMES
	}else if( sgnhand == UNSIGN ){
		lnmap = nmap;	// no long nmap. but the var is needed in both cases
		unsign_genomes( genomes, &nmap, directed );
	}

//	cout << genomes<<endl;

	// inititialise the characters
	// use the bool value adjacencies for the parameter complement.. because we want
	// gene sets of size two for adjacencies...
	spltsys = new interval_splitsystem( genomes, sgnhand, circular, adjacencies,
			maxsize, minobs, false, spltwfoo );

	// remove constant characters if whished
	if( !constant ){
		cerr << "# removing constant characters"<<endl;
		spltsys->remove_constant( );
	}

	// remove uninformative characters if whished
	if( uninf == "del" ){
		cerr << "# removing uninformative characters"<<endl;
		spltsys->remove_uninformative();
	}
	// other wise add it (without support) if not already present
	else if(uninf == "add"){
		cerr << "# adding missing uninformative characters"<<endl;
		spltsys->add_uninformative();
	}

	// remove incompatibilities
	if( comp == ""){
		// nothing
	}else if( comp == "greedy" ){
		cerr << "# removing incompatible characters (greedy)"<<endl;
		spltsys->remove_incompatible_greedy( );
		cerr << "# max weight "<< spltsys->supportcnt()<< " (greedy)" <<endl;
	}else if( comp == "ilp" ){
		cerr << "# removing incompatible characters (ILP)"<<endl;
		spltsys->remove_incompatible_lp( ilplim );
		cerr << "# max weight "<< spltsys->supportcnt()<< " (ILP)" <<endl;
	}else{
		cerr << "unknown method "<<comp << endl;
	}


	if( nexus && ( otype == "chars" || otype == "splits" )){
		cout << "#nexus"<<endl;
		cout << "begin taxa;"<<endl;
		cout << "dimensions ntax="<< genomes.size() <<";"<<endl;
		cout << "TAXLABELS"<<endl;
		for( unsigned i=0; i<names.size(); i++ ){
			cout <<"["<< i+1 <<"]\t'"<<names[i]<<"'"<<endl;
		}
		cout << ";"<<endl;
		cout << "end; [TAXLABELS]"<<endl;
		cout << endl;
	}

//	if(otype == "chars"){
//		if( nexus ){
//			cout << "begin characters;"<<endl;
//			cout << "dimensions nchar="<<  wsum <<";"<<endl;
//			cout << "format datatype=standard labels=no;"<<endl;
//			//	cout << "CHARSTATELABELS "<<endl;
//			//		"character-number [character-name]"
//			//	        , ...;]
//			cout << "matrix"<<endl;
//		}
//		unsigned t=0;
//		for( unsigned j=0; j<genomes.size(); j++ ){
//			if(nexus)
//				cout << "["<<j+1<<"] ";
//			for( unsigned i=0; i<data.size(); i++ ){
//				for( unsigned k=0; k<data[i].getobscnt(); k++ ){
//						cout <<data[i][j];
//					t++;
//				}
//			}
//			cout << endl;
//		}
//		if(nexus){
//			cout << ";"<<endl;
//			cout << "END; [CHARACTERS]"<<endl;
//		}
//	}else
	if( otype == "splits" ){
		spltsys->output_nexus( cout );
	}else if( otype == "chars" ){
		spltsys->output_nexus_chars(cout);
	}else{		// otype = name of a file containing a tree -> analyse, i.e. compare computed splits with the splits in the tree
		phy<genom> *bt;
		set<set<set<unsigned> > > osplits;
		vector<split> data, odata;
		vector<string> trees;

		read_trees( otype, trees );

		if( trees.size() == 0 ){
			cerr << "error: 0 trees in "<<otype<<endl;
			exit(EXIT_FAILURE);
		}else if( trees.size() > 1 ){
			cerr << "warning: more than one tree in "<<otype<<endl;
		}

		bt = new phy<genom>(trees[0], false, false);
		osplits = bt->bipartitions(names);
		for( set<set<set<unsigned> > >::iterator it=osplits.begin(); it!=osplits.end(); it++ ){
			odata.push_back( split(*it) );
		}
		osplits.clear();
		delete bt;

		data = spltsys->get_splits();

		for( unsigned i=0; i<data.size(); i++ ){
			cout << i<<"\t"<<data[i].splitsize() <<"\t"<< spltwfoo->calc( data[i] )<<"\t";
			if( find( odata.begin(), odata.end(), data[i] ) != odata.end() ){
				cout << "TP ";
			}else{
				cout << "FP ";
			}
			cout << "\t"<<data[i]<<endl;
		}

		for( unsigned j=0; j<odata.size(); j++ ){
			if( find(data.begin(), data.end(), odata[j]) == data.end() ){
				cout <<"-"<< j<<"\t"<<odata[j].splitsize() <<"\t0\t";
				cout << "FN ";
				cout << "\t"<<odata[j]<<endl;
			}else{
//					cout << "TP ";
			}
		}

//		ifstream tfile;
//		phy<genom> *bt;
////		set<set<set<unsigned> > > osplits;
//
//		string line;
//
//		tfile.open (otype.c_str(), ifstream::in);
//		if(!tfile.is_open()){ cout << "could not open treefile: "<< otype << endl; usage();}
//		while(tfile){getline(tfile, line);if(line[0] != '('){continue;} break;}
//		tfile.close();
//
//		bt = new phy<genom>(line, false, false);
//		osplits = bt->bipartitions(names);
//		for( set<set<set<unsigned> > >::iterator it=osplits.begin(); it!=osplits.end(); it++ ){
//			odata.push_back( chrctr<geneset>(*it) );
//		}
//		delete bt;
//
//
////		if( !uninformative && !constant ){
////			informative_splits( osplits, genomes.size());
////		}
//
//		// code for printing for each split if it is 'correct' or not and the sum of the weigths of the correct and 'wrong'
//		vector<float> wcorr, wwrng;
//		for( unsigned i=0; i<data.size(); i++ ){
//			cout << i<<" "<<data[i].splitsize() <<" "<<data[i].getobscnt()<<" ";
//			if( find( odata.begin(), odata.end(), data[i] ) != odata.end() ){
//				wcorr.push_back( data[i].getobscnt());
//				cout << "1 ";
//			}else{
//				wwrng.push_back( data[i].getobscnt());
//				cout << "0 ";
//			}
//			cout << data[i]<<endl;
//		}
//
//		while( wcorr.size() + wwrng.size() < 3 ){
//			wwrng.push_back(0.0);
//		}
//		copy(wcorr.begin(), wcorr.end(), ostream_iterator<float>(cout," ")); cout << endl<<endl;;
//		copy(wwrng.begin(), wwrng.end(), ostream_iterator<float>(cout," ")); cout<<endl;
//		cout << accumulate(wcorr.begin(), wcorr.end(), 0.0);cout <<" ";
//		cout << accumulate(wwrng.begin(), wwrng.end(), 0.0);cout <<endl;

//		for( unsigned i=0; i<splits.size(); i++ ){
//			cout << similar_split( splits[i], osplits )<<" "<<split_weights[i]<<endl;
//		}

//		for( unsigned i=0; i<splits.size(); i++ ){
//			for( unsigned j=i+1; j<splits.size(); j++ ){
//				split_differencexx(splits[i], splits[j]);
//			}
//		}
	}

	delete spltsys;
	delete spltwfoo;
	delete charwfoo;
 	genomes.clear();
	return EXIT_SUCCESS;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void getoptions( int argc, char *argv[], string &fname, bool &circular,
		SgnHandType & signhand, bool &adjacencies,
		bool &directed, unsigned &maxsize, string &otype,
		bool &constant, string &uninf, bool &nexus, string &comp, int &ilplim,
		unsigned &n, unsigned &rep, string &wfoos, string &wfooc){

	int c;
//		cb = (circular)?1:0,	// circular int
//		db = (directed)?1:0;	// directed int

	while (1) {
		static struct option long_options[] = {
			{"adj",     no_argument,       0, 'a'},
			{"greedy", no_argument,       0, 'g'},
			{"ilp", 	no_argument,       0, 'i'},
			{"ilplim", 	required_argument,       0, 'j'},
			{"file",    required_argument, 0, 'f'},
			{"help",    no_argument,       0, 'h'},
			{"lindir",  no_argument,	   0, 'l'},
			{"maxsize", required_argument, 0, 'm'},
			{"linund",  no_argument,	   0, 'u'},
			{"otype",   required_argument,     0,  'o' },
			{"plain",   no_argument,		0, 'p'},
			{"repeats",    required_argument, 0, 'r'},
			{"sign",    required_argument, 0, 's'},
			{"wfoos",   required_argument,		0, 'w'},
			{"wfooc",   required_argument,		0, 'x'},
			{"const",   no_argument,		0, 'y'},
			{"uninf",   required_argument,		0, 'z'},
            {0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:hm:n:o:r:s:x",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'a':
				adjacencies = true;
				maxsize = 2;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'g':
				comp = "greedy";
				break;
			case 'h':
				usage();
				break;
			case 'i':
				comp = "ilp";
				break;
			case 'j':
				ilplim = atoi(optarg);
				break;

			case 'l':
				circular = false; directed = true;
				break;
			case 'm':
				maxsize = atoi( optarg );
				break;
			case 'n':
				n = atoi( optarg );
				break;
			case 'o':
				otype = optarg;
				break;
			case 'p':
				nexus = !nexus;
				break;
			case 'r':
				rep = atoi( optarg );
				break;
			case 's':
				if( strcmp ( optarg, "double" ) == 0 ){
					signhand = DOUBLE;
				}else if( strcmp ( optarg, "unsign" ) == 0 ){
					signhand = UNSIGN;
				}else{
					cerr << "unknown sign handling function: "<<optarg<<endl;
					usage();
				}
				break;
			case 'u':
				circular = false; directed = false;
				break;
			case 'w':
				wfoos = optarg;
				break;
			case 'x':
				wfooc = optarg;
				break;
			case 'y':
				constant = !constant;
				break;
			case 'z':
				uninf = optarg;
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

    if( maxsize < 2 ){
    	cerr << "error: maxsize < 2"<<endl;
    	usage();
    }

	// set the adjacency bool varible
    if( adjacencies && maxsize != 2 ){
		cerr << "error: adjacency mode incompatible with maxsize "<<maxsize<<endl;
		usage();
	}

    if( circular && directed ){
    	cerr << "circular directed not implemented"<<endl;
    	exit(EXIT_FAILURE);
    }

		// check parameters
	if( fname == "" && n==0 && rep == 0 ){
		cerr << "no file and no n/rep given"<<endl;
		usage();
	}
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cerr << "ichar -f FILE [OPTIONS]"<<endl;
	cerr << "--help -h: print this message"<<endl;
//	otype
	cerr << "input data handling"<<endl;
	cerr << "--lindir: assume linear directed genomes"<<endl;
	cerr << "--linund: assume linear undirected genomes"<<endl;
	cerr << "-m MAXS : set max interval size to MAXS (default: ";
	if(DEFAULT_MAXSIZE == numeric_limits<unsigned>::max())cerr <<"auto";
	else cerr<<DEFAULT_MAXSIZE;
	cerr << " )"<<endl;
	cerr << "--adj   : adjacencies (default interval)"<<endl;
	cerr << "--sign -s : sign handling. default: ";
	if( DEFAULT_SGN_HANDLING == DOUBLE ) cerr << "double";
	else if( DEFAULT_SGN_HANDLING == UNSIGN ) cerr << "unsign";
	else{
		cerr << "error: undefined DEFAULT_SGN_HANDLING "<<DEFAULT_SGN_HANDLING<<endl;
		exit(EXIT_FAILURE);
	}
	cerr << endl;

	cerr <<endl<< "output handling"<<endl;
	cerr << "--otype: slits|chars"<<endl;

	cerr << "--plain: output as plain (default: "<<(DEFAULT_NEXUS?"nexus":"plain")<<")"<<endl;
	cerr << "--const: output of constant characters (default: "<< (DEFAULT_CONSTANT?"enabled":"disabled") <<")"<<endl;
	cerr << "--uninf: add|del output of uninformative characters (default: "<<DEFAULT_UNINF<<")"<<endl;

	cerr<<endl<<"weighting of splits and characters"<<endl;
	cerr << "--wfoos unit|linsup|binomial: split weight funtion (default: "<< DEFAULT_WEIGHT_SPLIT <<")"<<endl;
	cerr << "--wfooc unit|prob: character weight funtion (default: "<< DEFAULT_WEIGHT_CHAR <<")"<<endl;


	cerr <<endl<< "functionality to determine a compatible subset"<<endl;
	cerr << "--greedy: determine greedy max weight compatible subset"<<endl;
	cerr << "--ilp   : determine max weight compatible subset "<<endl;
	cerr << "--ilplim: time limit for the ilp solver in milliseconds"<<endl;



	exit( EXIT_FAILURE );
}

//#include <algorithm>
//#include <getopt.h>
//#include <iostream>
//#include <limits.h>
//#include <numeric>
//#include <iterator>
//#include <vector>
//
//#include "genom.hpp"
//#include "common.hpp"
//#include "io.hpp"
//#include "tree.hpp"
//
//#define DEFAULT_SGN_HANDLING "double"	// default sign handling option
//
//using namespace std;
//
//enum CharInf { INFORMATIVE, UNINFORMATIVE, CONSTANT };
//
///**
// * characters .. 01 information for each character
// * chr_idx
// */
//void characters2dot( const vector<string> &nmap, const vector<set<set<int> > > &splits, const vector<unsigned> &split_cnt,
//		const vector<int> &character_sidx, const vector<vector<int> > &characters, const map< vector<int>, int> &chr_idx, const int &nchar );
//
///**
// * compute the weight for each character
// * @param[in] n the length of the genomes
// * @param[in] chr_idx mapping from the gene clusters to the index in the character vector
// * @param[in] weightfoo weighting function
// * @return for each character a weight
// */
//vector<double> character_weight( int n, const map<vector<int>, int > &chr_idx, const string &weightfoo );
//
///**
// * find all conflicting pairs of splits
// * @param[in] characters binary characters found for the species
// */
//void conflict_pairs( const vector<vector<int> > &characters );
//
//
//
///**
// * determine and check command line options
// * @param[in] argc number of arguments
// * @param[in] argv the arguments
// * @param[out] fname the given filename
// * @param[out] circular circularity of the genomes
//// * @param[out] unique output the 'unified' set of character states
// * @param[out] deldup allow deletions and duplicates in the dataset
// * @param[out] asset handle characters as set
// * @param[out] constant keep also constant characters
// * @param[out] directed / undirected
// * @param[out] uninformative keep also uninformative characters
// * @param[out] maxsize maximum size of the considered characters
// * @param[out] otype output type
// * @param[out] frominterval get characters from pairwise intervals (common, strong, irreducible)
// * @param[out] weightfoo weighting function
// * @param[out] outint output also intervals when doing the comparison with a tree
// * @param[out] signhand sign handling method
// */
//void getoptions( int argc, char *argv[], string &fname, int &circular, int directed,
////		bool &unique,
//		bool &deldup, bool &asset, bool &constant, bool &uninformative, bool &ordered, unsigned &maxsize,
//		string &otype, string &frominterval, string &weigthfoo, string &splitweightfoo, bool &outint,
//		string &signhand);
//
///**
// * remove uninformative characters
// * @param[in,out] characters characters[i][j]=x: character no. i is x in genome[j], where x is in {0,1}
// * @param[out] informative [i]=1 iff character[i] is informative
// */
//void informative_characters( const vector<vector<int> > &characters, vector<CharInf> &informative);
//
///**
// * remove uninformative splits
// * @param[in,out] splits the splits
// * @param[in] m size of the base set
// */
//void informative_splits( set<set<set<int> > > &splits, unsigned m);
///**
// * init the characters
// * @param[in] genomes the input genomes
// * @param[out] characters characters[i][j]=x: character no. i is x in genome[j], where x is in {0,1}
//  * @param[out] chr_idx mapping from character (set of elements) to index
// * @param[in] asset handle characters as sets (default multiset)
// * @param[in] maxsize maximal number of genes per character (0 = unlimited)
// * @param[in] ordered use the order information of the sets
// * @param[in] circular iff 1 treat genomes as circular, else linear
// */
//void init_characters(const vector<genom> &genomes, vector<vector<int> > &characters,
//		map< vector<int>, int> &chr_idx, bool asset, bool ordered, unsigned maxsize, int circular  );
//
///**
// * init the character vector with generalized gene adjacencies
// * @param[in] genomes the genomes for which the generalized gene adjacencies
// * 	should be computed
// * @param[out] characters the binary characters, one entry for each found set
// * 	of elements;  characters[i][j]=x: character no. i is x in genome[j],
// * 	where x is in {0,1}
// * @param[out] chr_idx mapping from set of elements to index in the characters
// * 	vector
// * @param[in] ordered use the order information of the sets
// * @param[in] maxsize the maximum allowed difference of gene positions, i.e.
// * 	1: usual gene adjacencies, 2: one gap allowed ...
// * @param[in] circular iff 1 treat genomes as circular, else linear
// */
////
//// *******************************************************************************************
//
//void init_characters_generalized_adjacency(const vector<genom > &genomes, vector<vector<int> > &characters,
//		map< vector<int>, int> &chr_idx, bool ordered, unsigned maxsize, int circular );
//
///**
// * initialise characters from pairwise intervals (common, irreducible common, or strong common)
// * @param[in] genomes the input genomes
// * @param[out] characters characters[i][j]=x: character no. i is x in genome[j], where x is in {0,1}
// * @param[out] chr_idx mapping from character (set of elements) to index
// * @param[in] maxsize maximal number of genes per character (0 = unlimited)
// * @param[in] frominterval get intervals from strong, irreducible, or common intervals
// */
//void init_characters_pairwise_intervals( const vector<genom > &genomes, vector<vector<int> > &characters,
//		map< vector<int>, int> &chr_idx, unsigned maxsize, string frominterval );
//
///**
// * precompute weights of gene cluster of size l as the inverse of the probability to observe a cluster
// * of size l in a genome of size n (which is \binom{n}{l})
// * @param[in] n genome length
// * @return the weights stored in a vector of length n+1, element 0 is set to 0
// */
//void precompute_weights_prob( unsigned n );
//
///**
// * find the most similar split in a given split system and output the size of the difference
// * @param[in] s a split
// * @param[in] ss a aplit system
// * @return the minimal difference between s and a split in ss
// */
//unsigned similar_split( const set<set<int> > s, const set<set<set<int> > > ss );
//
///**
// * compute the difference between two splits. let A and B be two splits and A' and B' be the
// * part of the bipartition containing 1,. then the difference is given by the symmetric difference
// * of A' and B'
// * @param[in] s1 a split
// * @param[in] s2 another split
// * @param difference
// */
//unsigned split_difference(const set<set<int> > s1, const set<set<int> > s2);
//void split_differencexx(const set<set<int> > s1, const set<set<int> > s2);
//
//
///**
// * get the splits defined by the binary characters
// * @param[in] characters characters[i][j]=x: character no. j is x in genome[i], where x is in {0,1}
// * @param[out] informative [i]=1 iff character[i] is informative, ignored if empty
// * @param[out] splits the splits found in the data
// * @param[out] character_sidx the index of the split for each of the characters
// * @param[out] split_cnt for each split the count how often it was found
// */
//void splits_from_characters( const vector<vector<int> > &characters, const vector<CharInf> &informative,
//		vector<set<set<int> > > &splits, vector<unsigned> &character_sidx, vector<unsigned> &split_cnt);
//
//
///**
// * compute the weights of splits, given characterweights and an association of characters to the splits
// * @param[in] splitcnt the number of spits
// * @param[in] character_sidx mapping from character index to split index
// * @param[in] character_weights the weights of the characters
// */
//vector<double> split_weight( int splitcnt, const vector<unsigned> &character_sidx, const vector<double> &character_weights);
//
//vector<double> split_weight_per_length( unsigned m, int splitcnt, const map< vector<int>, int> &chr_idx, const vector<unsigned> &character_sidx, const vector<double> &character_weights);
//
///**
// * print usage information and exit
// */
//void usage(void);
//
///**
// * do the work
// */
//int main( int argc, char *argv[] ){
//	bool deldup = false,	// allow deletions and duplications in the data
//		constant = false, 		// keep constant characters	(zero 1/0)
//		uninformative = false,	// keep uninformative characters (one 1/0)
//		asset = false,		// treat enumerated intervals as sets
//		ordered = false,	// use the order information from the gene 'sets'
//		outint = false,		// output also intervals when doing the comparison with a tree
//		directed = false;
//	int circular = 1,	// circulatity
//		nchar = 0;
//
//	map< vector<int>, int> chr_idx;	// mapping from the found gene sets to an index in the array storing the binary characters
//	string gfname,			// filename of the genome file
//		otype = "",
//		frominterval="",
//		weightfoo="1",		// weighting function
//		splitweightfoo = "all",
//		sgnhand = DEFAULT_SGN_HANDLING; // sign handling option
//	unsigned maxsize = 0;	// maximum size of the considered clusters
//	vector<genom> genomes;	// the genomes
//	vector<string> names,	// names of the species
//		nmap; 				// name map
//	vector<vector<int> > characters;// the binary characters
//	vector<double> character_weights;
//	vector<CharInf> chr_inf;			// stores for each character if its informative
//
//		// the defined splits
//	vector<set<set<int> > > splits;		// the splits found in the data
//	vector<unsigned> character_sidx,	// the index of the split for each of the characters
//		split_cnt;						// for each split the count how often it was found
//	vector<double> split_weights;
//
//		// get and check parameters
//	getoptions( argc, argv, gfname, circular, directed, deldup, asset, constant, uninformative,
//			ordered, maxsize, otype, frominterval, weightfoo, splitweightfoo,
//			outint, sgnhand );
//
//		// read the genomes and the trees
//	read_genomes(gfname, genomes, names, circular, nmap, deldup, deldup);
//
////	for(unsigned i=0; i<genomes.size(); i++){
////		cerr << ">"<<names[i]<<endl;
////		cerr << genomes[i]<<endl;
////	}
////	cerr << genomes.size()<<endl;
//
////	n = genomes[0].size();
//
//	// make the genomes unsigned
//	// - as usual -> genomes of length 2n + 2 (and construct new name mapping)
//	// - or by forgetting the signs
//	if( sgnhand == "double" ){
//		double_genomes(genomes, &nmap, directed);
//	}else if( sgnhand == "unsign" ){
//		unsign_genomes( genomes, nmap, directed );
//	}else{
//		cerr << "unknown sign handling function"<<endl;
//		usage();
//	}
//	cerr<<"#"<< genomes.size() << " genomes "<<genomes[0].size()<<endl;
//
//	// initialise the characters from the genomes
//	if( frominterval == "" ){
//		init_characters(genomes, characters, chr_idx, asset, ordered, maxsize, circular );
//	}else if( frominterval == "ga" ){
//		init_characters_generalized_adjacency(genomes, characters, chr_idx, ordered, maxsize, circular );
//	}else{
//		if( deldup ){
//			cerr << "error: character computation from pairwise intervals"<<endl;
//			cerr << "       only possible when no duplicates and deletions are allowed"<<endl;
//			usage();
//		}
//		init_characters_pairwise_intervals(genomes, characters, chr_idx, maxsize, frominterval );
//	}
//
//	if(weightfoo != "1" && weightfoo != "binom"){
//		cerr << "unknown weighting function"<<endl;
//		usage();
//	}
//	character_weights = character_weight(genomes[0].size(), chr_idx, weightfoo);
//
//	nchar = characters.size();
//
//	cerr << "#initial     characters: "<<characters.size()<<endl;
//
//	unsigned cnchar = 0,
//		unchar = 0;
//	nchar = 0;
//	informative_characters( characters, chr_inf );
//	for( unsigned i=0; i<chr_inf.size(); i++ ){
//		if( chr_inf[i] == INFORMATIVE ) nchar++;
//		else if( chr_inf[i] == UNINFORMATIVE ) unchar++;
//		else if( chr_inf[i] == CONSTANT ) cnchar++;
//	}
//	cerr << "#informative characters  : "<< nchar <<endl;
//	cerr << "#uninformative characters: "<< unchar <<endl;
//	cerr << "#constant characters     : "<< cnchar <<endl;
//
//	if( uninformative )
//		nchar+=unchar;
//	if(constant)
//		nchar += cnchar;
//
////	for( unsigned i=0; i<characters.size(); i++ ){
////		if( chr_inf[i] == 0 ){
////			continue;
////		}
////		for( unsigned j=0; j<characters[i].size(); j++ )
////			cout << characters[i][j]<<" ";
////		cout << endl;
////	}
////	return 1;
//
//	splits_from_characters(characters, chr_inf, splits, character_sidx, split_cnt);
//
//	if(splitweightfoo != "all" && splitweightfoo != "pl"){
//		cerr << "unknown split weighting function"<<endl;
//		usage();
//	}
//
//	if(splitweightfoo=="all")
//		split_weights = split_weight( splits.size() , character_sidx, character_weights );
//	else if(splitweightfoo == "pl")
//		split_weights = split_weight_per_length( genomes.size(), splits.size(), chr_idx, character_sidx, character_weights);
//
//	if( otype != "chars" ){
//		cerr << "#                splits: "<<splits.size()<<endl;
//	}
//
//	if(otype == "chars" || otype == "splits"){
//		cout << "#nexus"<<endl;
//		cout << "begin taxa;"<<endl;
//		cout << "dimensions ntax="<< genomes.size() <<";"<<endl;
//		cout << "TAXLABELS"<<endl;
//		for( unsigned i=0; i<names.size(); i++ ){
//			cout <<"["<< i+1 <<"]\t'"<<names[i]<<"'"<<endl;
//		}
//		cout << ";"<<endl;
//		cout << "end; [TAXLABELS]"<<endl;
//		cout << endl;
//	}
//
//	if(otype == "chars"){
//		cout << "begin characters;"<<endl;
//		cout << "dimensions nchar="<< nchar<<";"<<endl;
//		cout << "format datatype=standard labels=no;"<<endl;
//	//	cout << "CHARSTATELABELS "<<endl;
//	//		"character-number [character-name]"
//	//	        , ...;]
//		cout << "matrix"<<endl;
//		for( unsigned i=0; i<characters[0].size(); i++ ){
//			cout << "["<<i+1<<"] ";
//			for( unsigned j=0; j<characters.size(); j++ ){
//				if( chr_inf[j] == UNINFORMATIVE && !uninformative ){
//					continue;
//				}
//				if ( chr_inf[j] == CONSTANT && !constant ){
//					continue;
//				}
//				cout <<characters[j][i];
//			}
//			cout << endl;
//		}
//		cout << ";"<<endl;
//		cout << "END; [CHARACTERS]"<<endl;
//	}else if( otype == "splits" ){
//	//	conflict_pairs(characters);
//
//		cout << "BEGIN Splits;"<<endl;
//		cout << "DIMENSIONS ntax="<<genomes.size()<<" nsplits="<<splits.size()<<";"<<endl;
//		cout << "FORMAT labels=no weights=yes confidences=no intervals=no;"<<endl;
//	//	PROPERTIES fit=-1.0 cyclic;
//	//	CYCLE 1 3 4 5 6 2;
//		cout << "MATRIX"<<endl;
//
//		for( unsigned i=0; i<splits.size(); i++ ){
//			cout << "["<<i+1<<"]\t";
////			cout << (float)split_cnt[i]/(float)nchar << "\t";
//			cout << split_weights[i]<<"\t";
//			for( set<int>::iterator it = splits[i].begin()->begin(); it!=splits[i].begin()->end(); it++ ){
//				cout << *it + 1<<" ";
//			}
//			cout << ","<<endl;
//		}
//		cout << ";"<<endl;
//		cout << "END; [Splits]"<<endl;
//
////		characters2dot(nmap, splits, split_cnt, character_sidx, characters, chr_idx, nchar);
//		// code for printing the gene sets that contribute to the splits
////		for( unsigned i=0; i<splits.size(); i++ ){
////			cerr << endl;
////			cerr << "Split ["<<i+1<<"]\t";
//////			cout << (float)split_cnt[i]/(float)nchar << "\t";
////			cerr << split_weights[i]<<"\t";
////
////			for( set<int>::iterator it = splits[i].begin()->begin(); it!=splits[i].begin()->end(); it++ ){
////				cerr << *it + 1<<" ";
////			}
////			cerr << endl;
////		}
//	}else{		// otype = name of a file containing a tree -> analyse, i.e. compare computed splits with the splits in the tree
//		ifstream tfile;
//		phy<genom> *bt;
//		set<set<set<int> > > osplits;
//		string line;
//
//		tfile.open (otype.c_str(), ifstream::in);
//		if(!tfile.is_open()){ cout << "could not open treefile: "<< otype << endl; usage();}
//		while(tfile){getline(tfile, line);if(line[0] != '('){continue;} break;}
//		tfile.close();
//
//		bt = new phy<genom>(line, false, false);
//		osplits = bt->bipartitions(names);
//		delete bt;
//
//		if( !uninformative && !constant ){
//			informative_splits( osplits, genomes.size());
//		}
//
////		copy(names.begin(), names.end(), ostream_iterator<string>(cout, " ")); cout << endl;
////		for( set<set<set<int> > >::iterator it = osplits.begin(); it!=osplits.end(); it++ ){
////			for( set<set<int> >::iterator jt = it->begin(); jt!=it->end(); jt++ ){
////				copy( jt->begin(), jt->end(), ostream_iterator<int>(cout, " ") ); cout << ",";
////			}
////			cout << endl;
////		}
//
//		// code for printing the gene sets that contribute to the splits
//		for( unsigned i=0; i<splits.size(); i++ ){
//			cout << "# Split "<<i+1<<"\t";
////			cout << (float)split_cnt[i]/(float)nchar << "\t";
//			cout << split_weights[i]<<"\t";
//
//			if( osplits.find( splits[i] ) != osplits.end() ){cout<<"1\t";}else{cout <<"0\t";}
//			for( set<int>::iterator it = splits[i].begin()->begin(); it!=splits[i].begin()->end(); it++ ){
//				cout << names[*it]<<" ";
//			}
//			cout << endl;
//
//			if( outint ){
//				for( map< vector<int>, int>::iterator it = chr_idx.begin(); it != chr_idx.end(); it++){
//					int ci = it->second;
//					if( character_sidx[ci] != i )
//						continue;
//					if( osplits.find( splits[i] ) != osplits.end() ){
//						cout << "# + ";
//					}else{
//						cout << "# - ";
//					}
//					cout << character_weights[ci] <<" ";
//					cout << it->first.size() <<" ";
//
//					copy( characters[ci].begin(), characters[ci].end(), ostream_iterator<int>(cout, "") );
//					cout << " {";
//					for(unsigned j=0; j<it->first.size(); j++ ){
//						cout << nmap[it->first[j]]<<" ";
//					}
//					cout <<"}"<< endl;
//				}
//			}
//		}
//
///*	code for printing for each split if it is 'correct' or not and the sum of the weigths of the correct and 'wrong'
//		vector<float> wcorr, wwrng;
//		for( unsigned i=0; i<splits.size(); i++ ){
//			if( osplits.find( splits[i] ) != osplits.end() ){
//				wcorr.push_back( split_weights[i] );
//				cout << "C "<<wcorr.back()<<endl;
//			}else{
//				wwrng.push_back( split_weights[i] );
//				cout << "W "<<wwrng.back()<<endl;
//			}
//		}
//
//		while( wcorr.size() + wwrng.size() < 3 ){
//			wwrng.push_back(0.0);
//		}
////		copy(wcorr.begin(), wcorr.end(), ostream_iterator<float>(cout," ")); cout << endl<<endl;;
////		copy(wwrng.begin(), wwrng.end(), ostream_iterator<float>(cout," ")); cout<<endl;
//		cout << accumulate(wcorr.begin(), wcorr.end(), 0.0);cout <<" ";
//		cout << accumulate(wwrng.begin(), wwrng.end(), 0.0);cout <<endl;*/
//
////		for( unsigned i=0; i<splits.size(); i++ ){
////			cout << similar_split( splits[i], osplits )<<" "<<split_weights[i]<<endl;
////		}
//
////		for( unsigned i=0; i<splits.size(); i++ ){
////			for( unsigned j=i+1; j<splits.size(); j++ ){
////				split_differencexx(splits[i], splits[j]);
////			}
////		}
//
//	}
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
////
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//void characters2dot( const vector<string> &nmap, const vector<set<set<int> > > &splits, const vector<unsigned> &split_cnt,
//		const vector<int> &character_sidx, const vector<vector<int> > &characters, const map< vector<int>, int> &chr_idx, const int &nchar ){
//
//	vector< vector<int> > lchar; // local copy of the characters contained in a split
//	vector<vector<bool> > inc;
//
//	// code for printing the gene sets that contribute to the splits
//	for( unsigned i=0; i<splits.size(); i++ ){
//        ofstream dotfile;
//        stringstream fname;
//        fname << "split"<<i+1<<".dot";
//        dotfile.open (fname.str().c_str());
//
//		cerr << "Split ["<<i+1<<"]\t"<<(float)split_cnt[i]/(float)nchar<<"\t";
//		for( set<int>::iterator it = splits[i].begin()->begin(); it!=splits[i].begin()->end(); it++ ){
//			cerr << *it + 1<<" ";
//		}
//		cerr << endl;
//
//		dotfile<< "digraph G {"<<endl;
//			// store characters and print node sets
//
//
//		dotfile<<"subgraph cluster0 {"<<endl;
//
//		dotfile<< "label = \"";
//		bool namewritten=false;
//
//		for( map< vector<int>, int>::const_iterator it = chr_idx.begin(); it != chr_idx.end(); it++){
//			unsigned ci = it->second;
//			if( (unsigned)character_sidx[ci] != i ) continue;
//			if( characters[ci][0] == 1 ) continue;
//
//			if( !namewritten ){
//				copy( characters[ci].begin(), characters[ci].end(), ostream_iterator<int>(dotfile, "") );
//				dotfile<< "\";"<<endl;
//				namewritten = true;
//			}
//			lchar.push_back(it->first);
//			dotfile << lchar.size()-1 << " [shape=box,label=\"";
//			for(unsigned j=0; j<it->first.size(); j++ ){
//				dotfile << nmap[it->first[j]]<<"";
//			}
//			dotfile<<"\"];"<<endl;
//		}
//		if(!namewritten) dotfile << "\""<<endl;
//		dotfile<<"}"<<endl;
//
//		dotfile << "subgraph cluster1 {"<<endl;
//		dotfile<< "label = \"";
//		namewritten=false;
//		for( map< vector<int>, int>::const_iterator it = chr_idx.begin(); it != chr_idx.end(); it++){
//			unsigned ci = it->second;
//			if( (unsigned)character_sidx[ci] != i ) continue;
//			if( characters[ci][0] == 0 ) continue;
//
//			if( !namewritten ){
//				copy( characters[ci].begin(), characters[ci].end(), ostream_iterator<int>(dotfile, "") );
//				dotfile<< "\";"<<endl;
//				namewritten = true;
//			}
//			lchar.push_back(it->first);
//			dotfile << lchar.size()-1 << " [shape=box,label=\"";
//			for(unsigned j=0; j<it->first.size(); j++ ){
//				dotfile << nmap[it->first[j]]<<"";
//			}
//			dotfile<<"\"];"<<endl;
//		}
//		if(!namewritten) dotfile << "\""<<endl;
//		dotfile<<"}"<<endl;
//			// compute and store edges
//		inc = vector<vector<bool> >(lchar.size(), vector<bool>( lchar.size(), false ));
//		for( unsigned j=0; j<lchar.size(); j++ ){
//			for( unsigned k=0; k<lchar.size(); k++ ){
//				if(j == k)
//					continue;
//					// k in j ?
//				if( includes( lchar[j].begin(), lchar[j].end(), lchar[k].begin(), lchar[k].end() ) ){
////					copy(lchar[j].begin(), lchar[j].end(), ostream_iterator<int>(cout,",")); cout << " > ";
////					copy(lchar[k].begin(), lchar[k].end(), ostream_iterator<int>(cout,",")); cout << endl;
//					inc[j][k] = true;
//				}
//			}
//		}
//		for( unsigned j=0; j<lchar.size(); j++ ){
//			for( unsigned k=0; k<lchar.size(); k++ ){
//				if( inc[j][k] ){
//					bool accept = true;
//					for(unsigned l=0; l<lchar.size(); l++){
//						if( l==j || l==k )
//							continue;
//						if( inc[j][l] && inc[l][k]  ){
//							accept = false;
//							break;
//						}
//					}
//					// PRINT EDGE
//					if(accept)
//						dotfile << j <<" -> "<<k<<";"<<endl;
//				}
//
//			}
//		}
//
//		dotfile << "}";
//
//
//		lchar.clear();
//		inc.clear();
//        dotfile.close();
//	}
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
////
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//vector<double> character_weight( int n, const map<vector<int>, int > &chr_idx, const string &weightfoo ){
//
//	if(weightfoo == "1"){
//		return vector<double>( chr_idx.size(), 1.0 );
//	}else{
//		vector<double> weights = vector<double>( chr_idx.size() ), // storing the weight of each cluster
//			weightn;				// stroring the weight of a cluster of size n
//		vector<double> psclrw;	// for storing the n-th row of pascals triangle
//
//			// compute the n-th row of pascals triangle
//			// and the weights for each cluster size
//		psclrw = pascal_row( n );
//
//		weightn = vector<double>( psclrw.size(), 0 );
//		for( unsigned s=1; s<psclrw.size(); s++ ){
//			weightn[s] = 1.0/ ((double)(n- s + 1) / (double)psclrw[ s ]) ;
//			cerr << "s/n"<<s<<"/"<<n<<" : "<< (n- s + 1) <<"/"<<psclrw[ s ]<<"="<<((double)(n- s + 1) / (double)psclrw[ s ])<<endl;
//		}
//
//			// assign the weights of each cluster depending on the cluster sizes
//		for( map<vector<int>, int >::const_iterator it = chr_idx.begin(); it!=chr_idx.end(); it++ ){
//			weights[it->second] = weightn[ it->first.size() ];
//		}
//		return weights;
//	}
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
////
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//void conflict_pairs( const vector<vector<int> > &characters ){
//	int conflict = 0,
//		noconflict = 0,
//		min = std::numeric_limits<int>::max(),
//		minidx = std::numeric_limits<int>::max();
//	vector<int> states(4),
//		scc(characters.size(), 0),	// conflict count for species
//		ccc(characters[0].size(), 0); // conflict count for each character
//	map<int,int> compidx;
//	vector<vector<int> > comp;
//
//	int statecnt = 0;
//
//	// characters[i][j]=x: character no. j is x in genome[i], where x is in {0,1}
//	for( unsigned i=0; i<characters[0].size(); i++){
//		for( unsigned j=i+1; j<characters[0].size(); j++){
//			for( unsigned k=0; k<characters.size(); k++ ){
////				cout << characters[k][i] << characters[k][j]<<endl;
//				states[2*characters[k][i] + characters[k][j]]++;
//			}
//			for( unsigned k=0; k<states.size(); k++ ){
//				if(states[ k ] > 0){
//					statecnt++;
//				}
//				if( states[ k ] < min ){
//					min = states[k];
//					minidx = k;
//				}
//			}
//			for( unsigned k=0; k<characters.size(); k++ ){
//				if(2*characters[k][i] + characters[k][j] == minidx){
//					scc[k]++;
//				}
//			}
//			min = std::numeric_limits<int>::max();
//			minidx = std::numeric_limits<int>::max();
//
////			sort( states.begin(), states.end() );
//
//
//			if(statecnt == 4){	// incompatible
//				ccc[i]++;
//				ccc[j]++;
//
//				if( compidx.find( i ) != compidx.end() && compidx.find( j ) == compidx.end()  ){
//					comp[ compidx[i] ].push_back( j );
//					compidx[j] = compidx[i];
//				}else if( compidx.find( i ) == compidx.end() && compidx.find( j ) != compidx.end()  ){
//					comp[ compidx[j] ].push_back(i);
//					compidx[i] = compidx[j];
//				}
//				else if( compidx.find( i ) == compidx.end() && compidx.find( j ) == compidx.end()  ){
//					compidx[i] = compidx[j] = comp.size();
//					comp.push_back( vector<int>() );
//					comp.back().push_back( i );
//					comp.back().push_back( j );
//				}
//				else if( compidx[i] != compidx[j] ) {
////					cout << "join ("<< compidx[i]<<")"; copy( comp[compidx[i]].begin(), comp[compidx[i]].end(), ostream_iterator<int>(cout," "));cout <<endl;
////					cout << "with ("<< compidx[j]<<")"; copy( comp[compidx[j]].begin(), comp[compidx[j]].end(), ostream_iterator<int>(cout," "));cout <<endl;
//					int ci = compidx[i],
//						cj = compidx[j];
//					comp[ ci ].insert( comp[ ci ].end(), comp[ cj ].begin(), comp[ cj ].end()  );
//					for( unsigned l=0; l<comp[ci].size(); l++ ){
//						compidx[ comp[ci][l] ] = ci;
//					}
//					comp[cj].clear();
////					cout << "---> "; copy( comp[ci].begin(), comp[ci].end(), ostream_iterator<int>(cout," "));cout <<endl;
////					cout << "     "; copy( comp[cj].begin(), comp[cj].end(), ostream_iterator<int>(cout," "));cout <<endl;
//
//				}
//				conflict++;
////				cout << "cnt4 "; copy(states.begin(), states.end(), ostream_iterator<int>(cout, " ")); cout << endl;
//			}else{
//				noconflict++;
//			}
//			states.assign(4,0);
//			statecnt = 0;
//		}
//	}
//
//	for(unsigned i=0; i<scc.size(); i++){
//		cout <<"scc "<< i<<" "<<scc[i]<<endl;
//	}
//	for(unsigned i=0; i<ccc.size(); i++){
//		cout <<"ccc "<< i<<" "<<ccc[i]<<endl;
//	}
//
//	cout << conflict<<" conflicting and "<< noconflict<<" non conflicting pairs"<<endl;
//	for( unsigned i=0; i<comp.size(); i++ ){
//		if(comp[i].size()>0){
//			cout << "conflict component "<<comp[i].size()<<endl;
//		}
//	}
//}
//
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
////
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//void getoptions( int argc, char *argv[], string &fname, int &circular, int directed,
////		bool &unique,
//		bool &deldup, bool &asset, bool &constant, bool &uninformative, bool &ordered, unsigned &maxsize,
//		string &otype, string &frominterval, string &weightfoo, string &splitweightfoo,
//		bool &outint, string &signhand){
//
//	int c;
//
//	while (1) {
//		static struct option long_options[] = {
//			{"deldup",	no_argument,		0, 'd'},
//			{"directed",   no_argument,	0, 'e'},
//			{"file",    required_argument,	0, 'f'},
//			{"help",    no_argument,        0, 'h'},
//			{"interval",required_argument,	0, 'i'},
//			{"linear",	no_argument, 		0, 'l'},
//			{"otype",	required_argument,	0,	'o'},
//			{"ordered", no_argument,		0, 'r'},
//			{"asset",	no_argument,		0, 's'},
//			{"sign", required_argument,	0,	't'},
//			{"uninf",   no_argument,		0, 'u'},
//			{"const",   no_argument,		0, 'x'},
//			{"weight",  required_argument,	0, 'w'},
//			{"splitweight",  required_argument,	0, 'v'},
//			{"oint", 	no_argument,		0, 'z'},
//            {0, 0, 0, 0}
//		};
//
//        int option_index = 0;			// getopt_long stores the option index here.
//        c = getopt_long (argc, argv, "df:hi:lm:o:s",long_options, &option_index);
//
//        /* Detect the end of the options. */
//        if (c == -1)
//        	break;
//
//        switch (c){
//			case 0: // If this option set a flag, do nothing else now.
//				if (long_options[option_index].flag != 0)
//					break;
//				cout << "option "<< long_options[option_index].name;
//				if (optarg)
//					cout << " with arg " << optarg;
//				cout << endl;
//				break;
//			case 'd':
//				deldup = true;
//				break;
//			case 'e':
//				directed = false;
//				break;
//			case 'f':
//				fname = optarg;
//				break;
//			case 'h':
//				usage();
//				break;
//			case 'i':
//				frominterval = optarg;
//				break;
//			case 'l':
//				circular = false;
//				break;
//			case 'm':
//				maxsize = atoi(optarg);
//				break;
//			case 'o':
//				otype = optarg;
//				break;
//			case 'r':
//				ordered = true;
//				break;
//			case 't':
//				signhand = optarg;
//				break;
//			case 'u':
//				uninformative = true;
//				break;
//			case 'w':
//				weightfoo = optarg;
//				break;
//			case 'v':
//				splitweightfoo = optarg;
//				break;
//			case 's':
//				asset = true;
//				break;
//			case 'x':
//				constant = true;
//				break;
//			case 'z':
//				outint = true;
//				break;
//
//			case '?':
//				cerr << "?"<<endl;
//				break; /* getopt_long already printed an error message. */
//			default:
//				usage();
//        }
//	}
//
//	/* Print any remaining command line arguments (not options). */
//    if (optind < argc){
//    	cerr << "non-option ARGV-elements: ";
//    	while (optind < argc)
//    		cerr << argv[optind++]<<" ";
//    	cerr << endl;
//	}
//
//		// check parameters
//	if( fname == "" ){
//		cerr << "no genome file given ! "<<endl;
//		usage();
//	}
//
//	if( asset && ordered ){
//		cerr << "error: gene sets can not be treated as sets and ordered"<<endl;
//		usage();
//	}
//
////    if( otype != "splits" && otype != "chars"  ){
////    	cerr << "error: no or invalid output type given"<<endl;
////    	usage();
////    }
//
//
//}
//
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
////
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//void informative_characters( const vector<vector<int> > &characters, vector<CharInf> &informative){
//	unsigned c = 0; 	// 1-counter
//	informative = vector<CharInf>( characters.size(), INFORMATIVE );
//	for( unsigned i=0; i<characters.size(); i++ ){
//		c = accumulate(characters[i].begin(), characters[i].end(), 0, plus<int>());
//		if( c == 0 || c == characters[0].size() ){
//			informative[i] = CONSTANT;
//		}else if( c == 1 || c==characters[0].size()-1 ){
//			informative[i] = UNINFORMATIVE;
//		}
//	}
//}
//
//void informative_splits( set<set<set<int> > > &splits, unsigned m){
//	set<set<set<int> > >::iterator it=splits.begin();
//	while(it != splits.end()){
//		if(it->begin()->size()<2 || it->begin()->size() > m-2 )
//			splits.erase(it++);
//		else
//			++it;
//	}
//}
//
//// *******************************************************************************************
//// init the character vector
//// *******************************************************************************************
//void init_characters(const vector<genom > &genomes, vector<vector<int> > &characters,
//		map< vector<int>, int> &chr_idx, bool asset, bool ordered, unsigned maxsize, int circular ){
//
//	vector<int> tmpset;
//	vector<int>::iterator new_end;
//
////	unsigned n = 0;
////	n = genomes[0].size();
//
//	chr_idx.clear();
//	characters.clear();
//
//	for(unsigned i=0; i<genomes.size(); i++){
////		characters.push_back( vector<int>(idx + 1) );
//
////		cout << "==============="<<endl;
////		copy(genomes[i].begin(), genomes[i].end(), ostream_iterator<int>(cout, " "));
////		cout <<endl;
//
//		for(unsigned j=0; j<genomes[i].size(); j++){
//			tmpset.clear();
//
//			unsigned k=j;
//			while( true ){
//				if( maxsize != 0 && k >= j+maxsize ){
//					break;
//				}
//				if( (circular==0 && k>=genomes[i].size()) || (circular!=0 && k>=genomes[i].size() && j==k%genomes[i].size()) ){
//					break;
//				}
//
////			for(unsigned k=j; k<genomes[i].size() && ( maxsize == 0 || k < j+maxsize ) ; k++){
////				cout << "k "<<k<<" s "<<genomes[i].size()<<" % "<<k%genomes[i].size()<<endl;
//				tmpset.push_back(genomes[i][k%genomes[i].size()]);
//
//				if( tmpset.size() == 1 ){
//					k++;
//					continue;
//				}
//
////				// this part excludes characters standing for a single gene (consisting of 2x-1, 2x for x \in 1..n)
////				// its commented out because in this way absence and presence of single genes can be coded
////				else if( tmpset.size() == 2 && ((genomes[i][k%genomes[i].size()]+1)/2 == (genomes[i][(k-1)%genomes[i].size()]+1)/2)){
////					k++;
////					continue;
////				}
////				// maybe include; but with different gene content it seems to make more sense without this option
////				else if(tmpset.size() == genomes[i].size()){
////					k++;
////					continue;
////				}
//
//				if( ! ordered ){
//					sort(tmpset.begin(), tmpset.end());
//
//					// delete duplicate elements if handled as set
//					// .. not this only works if the array is sorted
//					if(asset){
//						new_end = unique(tmpset.begin(), tmpset.end());
//						tmpset.erase( new_end, tmpset.end() );
//					}
//				}
//
//				copy(tmpset.begin(), tmpset.end(), ostream_iterator<int>(cerr," "));cerr << endl;
//
//					// if the set is not yet in the map -> insert
//				if( chr_idx.find(tmpset) == chr_idx.end() ){
//					chr_idx[tmpset] = characters.size();
//					characters.push_back( vector<int>(genomes.size(), 0) );
//				}
//				characters[chr_idx[tmpset]][ i ] = 1;
//				k++;
// 			}
//		}
//	}
//}
//
//// *******************************************************************************************
//// init the character vector with generalized gene adjacencies
//// *******************************************************************************************
//
//void init_characters_generalized_adjacency(const vector<genom > &genomes, vector<vector<int> > &characters,
//		map< vector<int>, int> &chr_idx, bool ordered, unsigned maxsize, int circular ){
//
//	vector<int> tmpset(2, 0);
//
//	chr_idx.clear();
//	characters.clear();
//
//	for(unsigned i=0; i<genomes.size(); i++){
//		cout << genomes[i]<<endl;
//		for(unsigned j=0; j<genomes[i].size(); j++){
//			tmpset[0] = genomes[i][j];
//			for( unsigned k=1; k<=maxsize; k++ ){
//				if( circular==0 && (j+k)>=genomes[i].size() )
//					break;
//				tmpset[1] = genomes[i][j+k];
////				cout << tmpset[0]<<","<<tmpset[1]<<endl;
//				if( ! ordered ){
//					sort(tmpset.begin(), tmpset.end());
//				}
//
//					// if the set is not yet in the map -> insert
//				if( chr_idx.find(tmpset) == chr_idx.end() ){
//					chr_idx[tmpset] = characters.size();
//					characters.push_back( vector<int>(genomes.size(), 0) );
//				}
//				characters[chr_idx[tmpset]][ i ] = 1;
//				k++;
//			}
//		}
//	}
//}
//
//// *******************************************************************************************
//// init the character vector with common, irreducible, or strong intervals of all pairs of
//// genomes
//// *******************************************************************************************
//
//void init_characters_pairwise_intervals( const vector<genom > &genomes, vector<vector<int> > &characters,
//		map< vector<int>, int> &chr_idx, unsigned maxsize, string frominterval ){
//
//	int n;
//	vector<pair<int,int> > ii;
//	vector<int> tmpset;
//	vector<int>::iterator new_end;
//
//
//	n = genomes[0].size();
//
//	for( unsigned i=0; i<genomes.size(); i++ ){
//		for( unsigned j=i+1; j<genomes.size(); j++ ){
////			cout<< ">"<<endl;for(unsigned k=0; k<genomes[i].size(); k++) cout << genomes[i][k]<<" "; cout<<endl;
////			cout<< ">"<<endl;for(unsigned k=0; k<genomes[j].size(); k++) cout << genomes[j][k]<<" "; cout<<endl;
//
//			if( frominterval == "irreducible" ){
//				irreducible_intervals( genomes[i], genomes[j], n,  ii);
//			}else if( frominterval == "common" ){
//				common_intervals(genomes[i], genomes[j], 0, 0, 0, ii);
//			}else if( frominterval == "strong" ){
//				ii = vector<pair<int,int> >(2*n);
//				strong_intervals(genomes[i], genomes[j], n, ii, true, false);
//			}else{
//				cerr << "error: unknow interval type: "<<frominterval<<endl;
//				usage();
//			}
////			cout << ii.size() <<" intervals" <<endl;
//
//			for(unsigned k=0; k<ii.size(); k++){
////				cout<< "ii "<<k<<" ("<<ii[k].first<<","<<ii[k].second<<")"<<endl;
//				for( int l=ii[k].first; l<=ii[k].second; l++ ){
////					cout << genomes[i][l-1]<<" ";
//					tmpset.push_back( genomes[i][l] );
//				}
////				cout << endl;
//
//				if( maxsize == 0 || tmpset.size() <= maxsize ){
//
//					sort(tmpset.begin(), tmpset.end());
//
//					if( chr_idx.find(tmpset) == chr_idx.end() ){
//						chr_idx[tmpset] = characters.size();
//						characters.push_back( vector<int>(genomes.size(), 0) );
//					}
//					characters[chr_idx[tmpset]][ i ] = 1;
//					characters[chr_idx[tmpset]][ j ] = 1;
//				}
//				tmpset.clear();
//			}
//			ii.clear();
//		}
//	}
//}
//
//
//
//unsigned similar_split( const set<set<int> > s, const set<set<set<int> > > ss ){
//	unsigned m = std::numeric_limits<unsigned>::max(),
//		t = 0;
//	for( set<set<set<int> > >::const_iterator it=ss.begin(); it!=ss.end(); it++ ){
//		t = split_difference( s, *it );
//		if(t < m){
//			m = t;
//		}
//	}
//	return m;
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//// get the difference between two splits A and B
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//unsigned split_difference(const set<set<int> > s1, const set<set<int> > s2){
//	set<int> s1nf, s2nf, 	// split normal form
//		sd;					// symmetric difference
//
//	for( set<set<int> >::const_iterator it=s1.begin(); it!=s1.end(); it++ ){
//		if( it->find(1) != it->end() ){
//			s1nf = *it;
//			break;
//		}
//	}
//	for( set<set<int> >::const_iterator it=s2.begin(); it!=s2.end(); it++ ){
//		if( it->find(1) != it->end() ){
//			s2nf = *it;
//			break;
//		}
//	}
//
//	insert_iterator<set<int> > ii(sd, sd.begin());
//	set_symmetric_difference(s1nf.begin(), s1nf.end(), s2nf.begin(), s2nf.end(), ii);
//
//	return sd.size();
//}
//
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//// get the difference between two splits A and B
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//void split_differencexx(const set<set<int> > s1, const set<set<int> > s2){
//	set<int> s1nf, s2nf, 	// split normal form
//		sd, se;					// set differences
//
//	for( set<set<int> >::const_iterator it=s1.begin(); it!=s1.end(); it++ ){
//		if( it->find(1) != it->end() ){
//			s1nf = *it;
//			break;
//		}
//	}
//	for( set<set<int> >::const_iterator it=s2.begin(); it!=s2.end(); it++ ){
//		if( it->find(1) != it->end() ){
//			s2nf = *it;
//			break;
//		}
//	}
//
////	copy(s1nf.begin(), s1nf.end(), ostream_iterator<int>(cout," "));cout << endl;
////	copy(s2nf.begin(), s2nf.end(), ostream_iterator<int>(cout," "));cout << endl;
//
//	insert_iterator<set<int> > ii(sd, sd.begin());
//	set_difference(s1nf.begin(), s1nf.end(), s2nf.begin(), s2nf.end(), ii);
//	insert_iterator<set<int> > ij(se, se.begin());
//	set_difference(s2nf.begin(), s2nf.end(), s1nf.begin(), s1nf.end(), ij);
//
//	if( sd.size() == 0 || se.size() == 0 ){
////		cout<< "compatible"<<endl;
//	}{
////		cout<< "NOT compatible"<<endl;
//		if( sd.size() <=se.size()){
//			copy(sd.begin(), sd.end(), ostream_iterator<int>(cout,"\n"));
//		}
//
//		if( sd.size()>=se.size()){
//			copy(se.begin(), se.end(), ostream_iterator<int>(cout,"\n"));
//		}
//	}
//}
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//// characters[i][j] character j presence/absence in species i
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//void splits_from_characters( const vector<vector<int> > &characters, const vector<CharInf> &informative,
//		vector<set<set<int> > > &splits, vector<unsigned> &character_sidx, vector<unsigned> &split_cnt){
//
//	set<set<int> > s; 	// temporary split
//	set<int> z, o;		// temporary sets for split construction (store 0/1 states)
//
//	map< set<set<int> >, int >	split_idx; 	// mapping from the splits to an index in the vector splits
//	unsigned idx;
//
//	splits.clear();
//	character_sidx.clear();
//	character_sidx = vector<unsigned>( characters.size(), std::numeric_limits<int>::max() );
//	split_cnt.clear();
//
////	set<vector<int> > cs;
////	vector<int> c;
////	vector<vector<int> > nc( characters.size(), vector<int>() );
//
//
//	for( unsigned i=0; i<characters.size(); i++ ){
//		if( informative.size() > 0 && informative[i] == INFORMATIVE )
//			continue;
//
//		for(unsigned j=0; j<characters[i].size(); j++){
//			if( characters[i][j] == 0 ){
//				z.insert( j );
//			}else if( characters[i][j] == 1 ){
//				o.insert( j );
//			}else{
//				cerr << "splits_from_characters: error: found character state !in {0,1}"<<endl;
//				exit(0);
//			}
//		}
//		s.insert(z);
//		s.insert(o);
//
//		if( split_idx.find(s) == split_idx.end() ){
//			split_idx[s] = splits.size();
//			splits.push_back(s);
//			split_cnt.push_back(0);
//		}
//		idx = split_idx[s];
//		split_cnt[idx]++;
//		character_sidx[i] = idx;
//
//		o.clear();
//		z.clear();
//		s.clear();
//	}
//
//	split_idx.clear();
////	characters = nc;
//}
//
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//// compute the weight of a split as the number of intervals supporting the split
//// divided by the number of characters at all
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//vector<double> split_weight( int splitcnt, const vector<unsigned> &character_sidx, const vector<double> &character_weights){
//	vector<double> split_weights(splitcnt, 0);
//	double sum = 0;
//
//	for( unsigned i=0; i<character_sidx.size(); i++ ){
//		if( character_sidx[i] == std::numeric_limits<int>::max() )
//			continue;
//
//		split_weights[ character_sidx[i] ] += character_weights[i];
//		sum += character_weights[i];
//	}
//
//	for( unsigned i=0; i<split_weights.size(); i++ ){
//		split_weights[i] /= sum;
//	}
//	return split_weights;
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
////
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//vector<double> split_weight_per_length( unsigned m, int splitcnt, const map< vector<int>, int> &chr_idx, const vector<unsigned> &character_sidx, const vector<double> &character_weights){
//	map<int, vector<double> > swpl;	// swpl[l][i]: weight of the intervals of length l supporting split i
//	vector<double> split_weights;	// a weight for each split
//	unsigned maxidx;	// maximum element index
//	double max;
//	double sum = 0;
//
//	split_weights = vector<double>(splitcnt, 0);
//
//	// compute the sum of the weigths of the intervals of the different length supporting the splits
//	for( map<vector<int>, int >::const_iterator it=chr_idx.begin(); it!=chr_idx.end(); it++){
//		if( character_sidx[ it->second ] == std::numeric_limits<int>::max() )	// skip non-informative characters
//			continue;
//
//		if( swpl.find( it->first.size() ) == swpl.end() ){
//			swpl[it->first.size()] = vector<double>( splitcnt, 0.0 );
//		}
//		swpl[it->first.size()][character_sidx[ it->second ]] += character_weights[ it->second ];
//	}
//
//	for( map<int,vector<double> >::iterator it=swpl.begin(); it != swpl.end(); it++ ){
//		for( unsigned i=0; i<m-3; i++ ){
//			max = 0;
//			maxidx = std::numeric_limits<unsigned>::max();
//			for( unsigned j=0; j<it->second.size(); j++ ){
//				if(it->second[j] > max){
//					max = it->second[j];
//					maxidx = j;
//				}
//			}
//			if(maxidx == std::numeric_limits< unsigned >::max())
//				break;
//
////			cout << it->first << " ";copy(it->second.begin(), it->second.end(), ostream_iterator<double>(cout," ")); cout << " -> "<<maxidx<< endl;
//
//			split_weights[maxidx]++;
//			it->second[maxidx] = 0.0;
//		}
//	}
//
//	for(unsigned i=0; i<split_weights.size(); i++){
//		sum += split_weights[i];
//	}
//	for(unsigned i=0; i<split_weights.size(); i++){
//		split_weights[i] /= sum;
//	}
//
//
//	return split_weights;
//}
//
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
////
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//void usage(void){
//	cerr << "usage: ichar -f FILE [OPTIONS]"<<endl;
//	cerr << "input options: "<<endl;
//	cerr << " --linear -l : consider genomes as linear"<<endl;
//	cerr << " --deldup -d : allow deletions and duplicates in the input"<<endl;
//	cerr << " --sign      : sign handling (default: "<<DEFAULT_SGN_HANDLING<<")"<<endl;
//	cerr << "         double: e->2i-1,2i; -e->2|i|,2|i|-1"<<endl;
//	cerr << "         unsign: remove signs"<<endl;
//	cerr << "output options"<<endl;
//	cerr << " --otype -o : set output type to 'splits', 'chars',"<<endl;
//	cerr << "              or the name of a phylip tree file (for comparison)"<<endl;
//	cerr << " --oint     : if the output shows a comparison also print intervals"<<endl;
//	cerr << "              corresponding to the splits"<<endl;
//	cerr << "character generation:"<<endl;
//	cerr << " --interval -i : specify gene set generation method"<<endl;
//	cerr << "     irreducible: use common intervals of pairs of genomes"<<endl;
//	cerr << "     common     : use irreducible common intervals of pairs of genomes"<<endl;
//	cerr << "     strong     : use strong common intervals of pairs of genomes"<<endl;
//	cerr << "     ga         : generalized gene adjacencies"<<endl;
//	cerr << "     (note: common, irreducible, and strong only work for equal gene content"<<endl;
//	cerr << "      wo duplicates), if unsecified all intervals are enumerated)"<<endl;
//	cerr << "  !note: this returns no trivial splits"<<endl;
//	cerr << " -m MAX : maximal size of the characters (default: no restriction)"<<endl;
//	cerr << "                  for ga this specifies the allowed distance between pairs"<<endl;
//	cerr << " --set -s      : handle characters as sets (each element is counted only once)"<<endl;
//	cerr << " --ordered     : handle characters as ordered"<<endl;
//	cerr << "other options"<<endl;
//	cerr << " --uninf       : print also uninformative characters"<<endl;
//	cerr << " --weight FOO  : weight intervals with funtion FOO (1 or binom)"<<endl;
//	cerr << " --splitweight FOO  : weight splits with funtion FOO (all or pl)"<<endl;
//	exit(0);
//}

