#include <cstring>
#include <getopt.h>

#include "distmat.hpp"
#include "dstnc_bp.hpp"
#include "dstnc_com.hpp"
#include "dstnc_coml.hpp"
#include "dstnc_crex.hpp"
#include "dstnc_inv.hpp"
#include "version.hpp"

#define DEFAULT_CIRCULAR 1	// default circular
#define DEFAULT_INTERVAL false	// # intervals as distance
#define DEFAULT_BP false
#define DEFAULT_COMMON false
#define DEFAULT_CONSERVED false
#define DEFAULT_CREX 1
#define DEFAULT_NEXUS false
#define DEFAULT_HEADER false
#define DEFAULT_LIST false
#define DEFAULT_PAIRPD 0
#define DEFAULT_SGN_HANDLING UNSIGN	// default sign handling option
#define DEFAULT_DIRECTED false  // default for directed:false (undirected:true)
#define DEFAULT_LENWEIGT false  // default for weight intervals by length:

using namespace std;

/**
 * get command line options
 * @param[in,out] lenw weight intrevals by length
 */
void getoptions( int argc, char *argv[], string &fname,
		bool &bp, bool &interval, char &circular, bool &directed,
		bool &common, int &pair_pd, bool &conserved,
		bool &list, bool &headers, bool &nexus, SgnHandType & signhand,
		bool &lenw, int &crex);

/**
 * print some usage information and exit the program with 0
 */
void usage();

/**
 * here the work is done :-)
 */
int main(int argc, char *argv[]) {
	vector<vector<unsigned> > dist_mat;
	vector<genom> genomes;
	vector<string> names;
	string filename;
	bool directed = DEFAULT_DIRECTED,
		list = DEFAULT_LIST,	// print list instead of matrix
		headers = DEFAULT_HEADER,	// print row&line names in the distance matrix
		interval = DEFAULT_INTERVAL,	// print number of intervals
		bp = DEFAULT_BP,
		common=DEFAULT_COMMON,		// common intervals
//		irreducible=false,
		conserved=DEFAULT_CONSERVED,
		allowdel = false,
//		duplicates=false,	// remove duplicate species
//		printdup = false,	// print the names of the removed species
		nexus = DEFAULT_NEXUS,		// print NEXUS distance matrix
		lenw = DEFAULT_LENWEIGT;
	int 
		crex=DEFAULT_CREX,
		pair_pd = DEFAULT_PAIRPD;			// pairwise perfect distance
//		sign=0,
//		normalize_to;
	unsigned n = 0;
	char circular = DEFAULT_CIRCULAR;
//	hdata hd;
	SgnHandType sgnhand = DEFAULT_SGN_HANDLING; // sign handling option
	map<string, bool> name_used;
	vector<string> nmap, lnmap;
	
	double avg=0,
		var = 0;
	
	getoptions( argc, argv, filename, bp, interval, circular, directed,
			common, pair_pd, conserved, list, headers, nexus, sgnhand, lenw, crex);

		// check if a filename was specified
	if (!filename.size()) {		// check if filename specified
		cerr << "Error: no input file specified" << endl;
		usage();
	}
	if (interval && !(common || conserved)){
		cerr << "Error: -m or -n must be choosen with -i"<<endl;
		usage();
	}

	if( bp ){
		allowdel = true;
	}

		// read the file
//	read_taxa(filename, G, Gnames, circular, normalize_to);

	read_genomes(filename, genomes, names, circular, nmap, allowdel);
	
	// make the genomes unsigned
	// - as usual -> http://grimm.ucsd.edu/MGR/genomes of length 2n + 2 (and construct new name mapping)
	// - or by forgetting the signs
	if( sgnhand == DOUBLE ){
		lnmap = double_nmap(nmap, false );
		double_genomes(genomes, NULL, false);
//		double_genomes(genomes, NULL, directed);
	}
	if( directed  ){
		for(unsigned i=0; i<genomes.size(); i++){
			genomes[i].chromosom.insert(genomes[i].chromosom.begin(), genomes[i].size()+1);
			genomes[i].push_back( genomes[i].size()+1 );
			genomes[i].set_nmap(NULL);
		}
	}

//	cerr << genomes<<endl;

	if(genomes.size() == 0){
		cerr << "Error: no genomes found in the input file " << endl;
		usage();
	}
//	init_data(hd, G[0].size(), G[0].getCircular());
	

//		// remove duplicate entries
//	if(duplicates){
//		vector<bool> remove(G.size(), false);
//		vector<genom> filtered;
//		vector<string> filtered_names;
//		if(printdup)
//			cerr << "removing duplicates"<<endl;
//		for(unsigned i=0; i<G.size(); i++){
//			if(remove[i])
//				continue;
//
//			if(printdup)
//				cerr << Gnames[i] <<" -> ";
//			for(unsigned j=i+1; j<G.size(); j++){
//				if(G[i].distance(G[j], hd) == 0){
//					if(printdup)
//						cerr << Gnames[j]<<" ";
//					remove[j]=true;
//				}
//			}
//			if(printdup)
//				cerr << endl;
//		}
//
//		for(unsigned i=0; i<remove.size(); i++){
//			if(!remove[i]){
//				filtered.push_back(G[i]);
//				filtered_names.push_back(Gnames[i]);
//			}
//		}
//		G = filtered;
//		Gnames = filtered_names;
//	}
	//~ for(unsigned i=0; i<G.size(); i++){
		//~ cout << ">"<<Gnames[i]<<endl<<G[i]<<endl;
	//~ }
	//~ return 0;

//	int conserved, int common, int pair_pd, int sign, int interval, int bp, int circular,

	// determine max genome size
	for(unsigned i=0; i<genomes.size(); i++){
		n = max(n, genomes[i].size());
	}

	dstnc *dst = NULL;
	if( interval ){
		if( common ){
			if( lenw ){
				dst = new dstnc_coml( n, circular);
			}else{
				dst = new dstnc_com( n, circular);
			}

		}
		else if(conserved){cerr <<"conserved interval distance not implemented"<<endl;exit(EXIT_FAILURE);}
	}else if(crex == 1){
		dst = new dstnc_crex(n, false, false, 1);
	}else{
		if(common){cerr <<"conserved interval preserving reversal distance not implemented"<<endl; exit(EXIT_FAILURE);}
		else if(conserved){cerr <<"common interval preserving reversal distance not implemented"<<endl; exit(EXIT_FAILURE);}
		else if (bp){dst = new dstnc_bp( n, circular, !circular);}
		else{dst = new dstnc_inv( n, circular );}
	}
	
	cerr <<*dst <<endl;

		// compute the distance matrix
	dist_mat = distance_matrix( genomes, dst );
	
	if( list ){
		for(unsigned i=0; i<dist_mat.size(); i++){
			for( unsigned j=0; j<dist_mat.size(); j++ ){
				cout << names[i]<<" "<<names[j]<<" "<<dist_mat[i][j]<<endl;
			}
		}
	}else{
		if(headers){
			for(unsigned i=0; i<genomes.size(); i++){
				cout << "\t" << names[i];
			}
			cout << endl;
		}

			// print nexus start
		if(nexus){
			cout << "#NEXUS"<<endl;

			cout << "BEGIN Taxa;"<<endl;
			cout << "DIMENSIONS ntax="<<genomes.size()<<";"<<endl;
			cout << "TAXLABELS"<<endl;
			for(unsigned i=0; i<names.size(); i++)
				cout << "["<<i<<"] '"<<names[i]<<"'"<<endl;
			cout << ";"<<endl;
			cout << "END;"<<endl;


			cout << "[!user defined distances]"<<endl;
			cout << "Begin distances;"<<endl;
			cout << "Dimensions ntax="<<genomes.size()<<";"<<endl;
			cout << "format Triangle=Both;"<<endl;
			cout << "matrix"<<endl;
		}
	
			// print distance matrix
		if(!headers){
		    cout << genomes.size()<<endl;
		}
		for(unsigned i=0; i<dist_mat.size(); i++){
			if(nexus){
				cout <<"["<<i<<"] '" <<names[i]<< "' ";
			}
			for(unsigned k=0; k<10; k++ ){
				if( k < names[i].size() ){
					cout <<names[i][k];
				}else{
					cout << " ";
				}
			}
	//		for(unsigned j=0; j<dist_mat[i].size(); j++){
	//			printf("%d %d %d\n", i, j, dist_mat[i][j]);
	//			avg += dist_mat[i][j];
	//		}
			for(unsigned j=0; j<dist_mat[i].size(); j++){
				if(nexus){
				    cout << " ";
				}else{
				    cout << "\t";
				}
				printf("%2d.0 ", dist_mat[i][j]);
				avg += dist_mat[i][j];
			}
			cout << endl;
		}

			// nexus end
		if(nexus){
			cout << ";"<<endl<<"end;"<<endl;
		}else{
			avg /= (dist_mat.size()*dist_mat.size()- dist_mat.size());
			for(unsigned i=0; i<dist_mat.size(); i++){
				for(unsigned j=0; j<dist_mat[i].size(); j++){
					if(i!=j)
						var += pow(dist_mat[i][j]-avg, 2);
				}
			}
			var = sqrt(var / ( (dist_mat.size()*dist_mat.size()- dist_mat.size()) -1));
	//		cout<< G.size()<<" spec n "<< G[0].size()<<" " << "avg, var: "<<avg<<" , "<<var<<endl;
		}
	}
	return EXIT_SUCCESS;
}

void getoptions( int argc, char *argv[], string &fname,
		bool &bp, bool &interval, char &circular, bool &directed,
		bool &common, int &pair_pd, bool &conserved,
		bool &list, bool &headers, bool &nexus, SgnHandType & signhand,
		bool &lenw, int &crex){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"crex",    no_argument,	0, 'c'},
			{"file",    required_argument, 0, 'f'},
			{"help",    no_argument,	0, 'h'},
			{"lindir",  no_argument,	0, 'l'},
			{"linund",  no_argument,	0, 'u'},
			{"lw",    no_argument,	0, 'w'},
			{"sign",    required_argument,	0, 's'},
			{"list",    no_argument,	0, 't'},
			{"nexus",    no_argument,	0, 'x'},
			{"header",    no_argument,	0, 'y'},
			{"version",    no_argument,	0, 'v'},
			{0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "bcf:irmMnxv",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;

				case 'b': {bp = true; crex = 0; break;}
				case 'c': {crex = 1; break;}
				case 'f': {fname = optarg;break;}
				case 'i': {interval = true; crex = 0; break;}
				case 'l': {circular = false; directed = true; break;}
				case 'u': {circular = false; directed = false; break;}
				case 'm': {common = true; pair_pd = 1; break;}
				case 'M': {common = true; pair_pd = 0; break;}
				case 'n': {conserved = true;break;}
				case 's':
					if( strcmp ( optarg, "double" ) == 0 ){
						signhand = DOUBLE;
					}else if( strcmp ( optarg, "unsign" ) == 0 ){
						signhand = UNSIGN;
					}else{
						cerr << "unknown sign handling function: "<<optarg<<endl;
						usage();
					}
					break;
				case 't': {list = true; break;}
                                case 'v': {cout << PROJECT_VERSION_MAJOR << "." << PROJECT_VERSION_MINOR << "." << PROJECT_VERSION_PATCH << std::endl; exit(EXIT_SUCCESS);}
				case 'w': {lenw=true; break;}
				case 'x': {nexus=true; break;}
				case 'y': {headers=true; break;}
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
        if (optind < argc){
            cerr << "non-option ARGV-elements: ";
            while (optind < argc)
                cerr << argv[optind++]<<" ";
            cerr << endl;
	    exit(EXIT_FAILURE);
	}

		// check parameters
	if( fname == "" ){
		cerr << "no file given"<<endl;
		usage();
	}
}

void usage() {
	cerr << "usage :"<<endl;
	cerr << "distmat -f inputfile [OPTIONS]"<<endl;

	cerr << "input options:"<<endl;
	cerr << " --lindir : treat data as linear directed genomes (default circular)"<<endl;
	cerr << " --linund : treat data as linear undirected genomes (default circular)"<<endl;
	cerr << "--sign -s : sign handling. default: "<<endl;
	if( DEFAULT_SGN_HANDLING == DOUBLE ) cerr << "double";
	else if( DEFAULT_SGN_HANDLING == UNSIGN ) cerr << "unsign";
	else{
		cerr << "error: undefined DEFAULT_SGN_HANDLING "<<DEFAULT_SGN_HANDLING<<endl;
		exit(EXIT_FAILURE);
	}
	// cerr << "   -d: remove duplicates gene orders from the data"<<endl;
	// cerr << "   -D: -d AND print names of removed duplicate gene orders"<<endl;
	cerr << "general options:"<<endl;
	cerr << "   --crex: compute CREx distance"<<endl;
	cerr << "   -b: get number of breakpoints"<<endl;
	cerr << "   -i: get number of intervals (-n|-m must be specified)"<<endl;
	cerr << "interval options"<<endl;
	cerr << "   -n: use conserved intervals"<<endl;
	cerr << "   -m: use common intervals"<<endl;
	cerr << "   -M: use global instead of pairwise perfect distance"<<endl;
	cerr << "--lw : weight intervals by length "<<endl;
//	cerr << "common interval options "<<endl;
//	cerr << "   -s : use sign info for common intervals"<<endl;
//	cerr << "   -r : only irreducible common intervals"<<endl;
	cerr << "output options:"<<endl;
	cerr << " --header: print column headers in the distance matrix"<<endl;
	cerr << " --nexus : print nexus format distance matrix"<<endl;
	cerr << " --list  : print list format instead of matrix"<<endl;
	cerr << " --version  : print version and exit"<<endl;

	exit( EXIT_FAILURE );
}
