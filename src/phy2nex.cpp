/** @file phy2nex.cpp
 * reads a phylip tree file and transforms it to nexus format
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <regex.h>

#include "bintree.hpp"

using namespace std;

void usage();

int main(int argc, char *argv[]){
	bintree *bt;
	dstnc *dst = NULL;
	int status;				// status of re matches
	ifstream ifile;			// input file
	regex_t 	re_newick;		// newick tree line
	regmatch_t match[8]; 
	string fname,			// input file name
		line	;				// a line of the input
	unsigned pos;
	vector<string> nwks,		// the trees found in the input
		names;				// names of the species
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'f': {i++; fname = argv[i]; break;}
			default:{
				cout << "Error: unknown option "<<argv[i]<<endl;
				usage();
			}
		}
	}
		// prepare regular expressions
	status = regcomp(&re_newick,"^\\(",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "parse_tree_results: Could not compile regex pattern re_newick."<<endl;
		exit(-1);
	}
		// open the input file
	ifile.open(fname.c_str());
	if(!ifile.is_open()){
		cout << "unable to open "<<fname<<endl;
		exit(1);
	}
		
		// read file line by line
	while(ifile){
		getline( ifile, line );
		
		if(regexec (&re_newick, &line[0], 1, &match[0], 0)==0){
			nwks.push_back( line );
		}
	}

	if(nwks.size() == 0){
		cout << "no trees found"<<endl;
		exit(1);
	}
	
	bt = init(nwks[0], dst);
	leaf_names(bt, names);
	free_tree(bt);
	
	for(unsigned i=0; i<names.size(); i++){
		do{
			pos = names[i].find("_");
			if(pos != string::npos){
				names[i].erase(names[i].begin()+pos);
			}else{
				break;
			}
		}while(1);
	}
	for(unsigned i=0; i<nwks.size(); i++){
		do{
			pos = nwks[i].find("_");
			if(pos != string::npos){
				nwks[i].erase(nwks[i].begin()+pos);
			}else{
				break;
			}
		}while(1);
	}
	
	cout << "#NEXUS"<<endl<<endl;
	cout << "BEGIN taxa;"<<endl;
	cout << "DIMENSIONS ntax="<<names.size()<<";"<<endl;
	cout << "TAXLABELS"<<endl;
	for(unsigned i=0; i<names.size(); i++){
		cout << "["<< i+1<<"] '"<<names[i]<<"'"<<endl;
	}
	cout << ";"<<endl;
	cout << "END [taxa];"<<endl;
	cout << "BEGIN trees;"<<endl;
	for(unsigned i=0; i<nwks.size(); i++){
		cout << "utree t"<<i<<"="<<nwks[i]<<endl;	
	}
	cout << "END [trees];"<<endl;
	
	names.clear();
	nwks.clear();
	regfree(&re_newick);
	ifile.close();
}

void usage(){
	exit(1);
}
