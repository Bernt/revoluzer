/**
 * program to ...
 * @author: M. Bernt
 */

#include <algorithm>
#include <getopt.h>
#include <iostream>
#include <iterator>
#include <queue>
#include <set>

#include "crex.hpp"
#include "costfoo.hpp"
#include "genom.hpp"
#include "helpers.hpp"
#include "io.hpp"
#include "rrrmtrand.hpp"
#include "version.hpp"

//#define DEBUG_CREX

// #define DOT_COMPONENTS
#define ASSET

#ifdef DOT_COMPONENTS
#include <fstream>
#include <iomanip>
#endif//DOT_COMPONENTS

using namespace std;

#ifdef DOT_COMPONENTS
void initgraph(unsigned n, vector<vector<int> > &graph, vector<vector<unsigned> > &comp, map<unsigned, unsigned> &comp_idx){
	graph = vector<vector<int> >(n, vector<int>(n, 0));
	for( unsigned i=0; i<n; i++){
		comp.push_back( vector<unsigned>(1, i) );
		comp_idx[i]=i;
	}
}

void connect(unsigned i, unsigned j, int val, vector<vector<int> > &graph, vector<vector<unsigned> > &comp, map<unsigned, unsigned> &comp_idx){

	graph[i][j] = val;

	// both in a component
	if( comp_idx[i] != comp_idx[j] ){
		unsigned cj = comp_idx[j];
		comp[ comp_idx[i] ].insert(comp[ comp_idx[i] ].end(), comp[ cj ].begin(), comp[ cj ].end() );
		comp.erase( comp.begin()+cj );
		for( unsigned i=0; i<comp.size(); i++ ){
			for( unsigned j=0; j<comp[i].size(); j++ ){
				comp_idx[ comp[i][j] ] = i;
			}
		}
	}
}
#endif

/**
 * get program parameters
 * @param[in] argc argument count
 * @param[in] argv arguments
 * @param[out] fname filename
 * @param[out] circluar circularity
 * @param[out] mkalt create alternatives for transpositions and inverse transpositions
 * @param[out] bps compute breakpoint scenario for the given perm
 * @param[out] crexone compute crex1 scenario for the given perm
 * @param[out] crex add breakpoint scenario as alternative for prime nodes
 * @param[out] maxalt maximum number of alternatives constructed for prime nodes (inv+tdrl, bp)
 * @param[out] randint generate TDRLs from random interval
 * @param[out] n genome length
 * @param[out] r no of repeats
 * @param[out] k number of events
 * @param[out] d number of affected elements
 * @param[out] wI weight of inversions
 * @param[out] wT weight of transpositions
 * @param[out] wiT weight of inverse transpositions
 * @param[out] wTDRL weight of TDRLs
 * @param[out] mindist minimum allowed distance
 * @param[out] maxdist maximum allowed distance
 */
void getoptions(int argc, char *argv[], string &fname, int &circular, bool &mkalt,
		bool &bps, bool &crexone, bool &crexbps, unsigned &maxalt, bool &randint,
		int &n, int &r, int &k, unsigned &d, float &wI, float &wT, float &wiT, float &wTDRL,
		unsigned &mindist, unsigned &maxdist);

/**
 * print usage info and exit
 */
void usage(unsigned exit_code);

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int main(int argc, char *argv[]){
	bool bps = false,		// compute rearrangements with breakpoint method from [ZhaoBourque07]
		crexone = false,	// use crex1 to compute rearrangements
		crexbps = true,		// use breakpoint method for prime nodes in crex
		mkalt = false,		// generate alternatives
		randint = true;		// do random tdrls only affect a randomly choosen interval
	costfoo_by_type *cst; 	// cost function for crex2
//	costfoo_equi *ecst;
	int circular = true,	// circular genomes ?
		n = 0, 				// length of the genomes
		k = 0,				// number of events
		r = 1;				// number of repetitions
	unsigned maxszen = 1,	// maximal number of reversals for combined reversal+tdrl scenarios
		d = std::numeric_limits< unsigned >::max(), 		// maximal number of affected elements per random rearrangement in the random scenario
		mindist = 0,
		maxdist = std::numeric_limits<unsigned>::max();
	string filename;		// input filename
	vector<genom> genomes; 	// the genomes
	vector<float> prob(4,1);		// propability of a reversal, transposition, reverse transposition, and tdrl
	vector<string> names, 	// names of the genomes
		nmap;				// names of the elements
	vector<vector<string> > tax;

	// get and check the parameters
	// get and check the parameters
	getoptions( argc, argv, filename, circular, mkalt, bps, crexone, crexbps, maxszen, randint,
			n, r, k, d, prob[0], prob[1], prob[2], prob[3], mindist, maxdist);

	cst = new costfoo_by_type();
	cst->set( REVNM, prob[0] );
	cst->set( TRANM, prob[1] );
	cst->set( RTRANM, prob[2] );
	cst->set( TDRLNM, prob[3] );

//	ecst = new costfoo_equi();

	// compute probabilities from the weights
	for(unsigned i=0; i<prob.size(); i++){
		prob[i] = 1.0/prob[i];
	}


	if(filename == ""){
		double srecall = 0.0, sprecision = 0.0,
			recall = 0.0, precision = 0.0,
			pness = 0.0;	// 'preservedness'
		unsigned pnodes=0,
				rec, rand, cut;
		genom id,
			tgt;
		rrrmt *cut_sce = NULL;
		vector< rrrmt* > rec_sce(2);
		vector< string > mnames(2);
		vector<set<rrrmt*,HDereferenceLess> > rec_set(2);
		set<rrrmt*,HDereferenceLess> //rand_set,
			cut_set;
		vector<genom> trace;	// trace of the simulation
		vector<pair<int, int> > comint;

		if(n < 2){cerr << "error: genome must have length > 1"<<endl; usage(1);}
		if(k < 1){cerr << "error: k must be greater 0"<<endl; usage(1);}
		if(prob.size() != 4){cerr<<"error: not all probabilities given"<<endl; usage(1);}

		init_rng( 42 );
		mnames[0] = "random";
		mnames[1] = "crex";
//		mnames[2] = "crex2-132";
//		mnames[3] = "crex2-111";
//		mnames[1] = "bp";

		id = genom(n, 0);

		for (int i=0; i<r; i++){
//			cout << i<<endl;
			tgt = genom(n, 0);

			// get a random scenario consisting of k random rearrangements,
			// where each rearrangement does not affect more then d elements
			// tdrls are choosen according to the randint variable
//			rand_sce = new unordered( k, prob, tgt, trace, randint, d);
//			tmp_sce = rand_sce->simplify();
//			delete rand_sce;
//			rand_sce = tmp_sce;

			rec_sce[0] = new rrrmt_rand_sit(k, prob, tgt, trace, 2, 2);
//			rec_sce[0]->getrrrmt(rec_set[0]);

//			cerr << tgt << endl;

//			common_intervals(trace[0], trace.back(), 0, 1, 0, comint);
//			pness = comint.size();
//			comint.clear();
//
//			itnode* iroot;
//			list<itnode *> pnode;
//
//			interval_tree(tgt, id, n, &iroot);
//			interval_tree_primenodes(iroot, pnode);
//			pnodes = pnode.size();
//			interval_tree_free(iroot);
//			pnode.clear();

//			rec_sce[1] = new crex(id, tgt, crexbps, maxszen, mkalt);
//			rec_sce[1]->getrrrmt(rec_set[1]);

			rec_sce[1] = new crex2(id, tgt, true, cst, false, mkalt );//TODO oriented?
//			rec_sce[1]->getrrrmt(rec_set[1]);

//			rec_sce[1] = new crex2(id, tgt, true, cst, true, mkalt );//TODO oriented?
//			rec_sce[1]->getrrrmt(rec_set[1]);

			//			rec_sce[1] = new bpscenario(id, tgt, maxszen, false);
//			rec_sce[1]->getrrrmt(rec_set[1]);

//			for( unsigned m1=0; m1<rec_sce.size(); m1++ ){
//				for( unsigned m2=0; m2<rec_sce.size(); m2++ ){
//					if( m1 == m2 ){
//						continue;
//					}
//					cut_sce = rec_sce[m1]->intersect( rec_sce[m2] );
//					set_intersection( rec_set[m1].begin(), rec_set[m1].end(), rec_set[m2].begin(), rec_set[m2].end(), insert_iterator<set<rrrmt*,HDereferenceLess> >(cut_set,cut_set.begin()), HDereferenceLess() );
//#ifdef ASSET
//					cut = cut_set.size();
//					rec = rec_set[m1].size();
//					rand = rec_set[m2].size();
//#else
//					cut = cut_sce->length(ALTMIN);
//					rec = rec_sce[m1]->length(ALTMIN);
//					if( rec < cut ){
//						rec = rec_sce[m1]->length(ALTMAX);
//					}
//					rand = rec_sce[m2]->length(ALTMIN);
//
//#endif//ASSET
//
//#ifdef DEBUG_CREX
//					cout << "=r="<<i<<"============================================"<<endl;
//					cout << ">id"<<endl<<id<<endl;
//					cout << ">tgt"<<endl<<tgt << endl;
//					cout << "scenario1 "; rec_sce[m1]->output(cout, 1, 0); cout << endl;
//					cout << "   set1: ";
//					for( set<rrrmt*,HDereferenceLess>::iterator it=rec_set[m1].begin(); it!=rec_set[m1].end(); it++ )
//						cout << *(*it);
//					cout <<endl<< "scenario2 "; rec_sce[m2]->output(cout, 1, 0); cout << endl;
//					cout << "   set2: ";
//					for( set<rrrmt*,HDereferenceLess>::iterator it=rec_set[m2].begin(); it!=rec_set[m2].end(); it++ )
//						cout << *(*it);
//					cout <<endl<< "   cut scenario "; cut_sce->output(cout, 1, 0); cout<< endl;
//					cout << "   cut set: ";
//					for( set<rrrmt*,HDereferenceLess>::iterator it=cut_set.begin(); it!=cut_set.end(); it++ )
//						cout << *(*it);
//
//					cout << "cut "<<cut<< " rand "<<rand<< " rec "<<rec<<endl;
//#endif//DEBUG_CREX
//
//					if( rand == 0 && cut == 0 && rec == 0 ){
//						precision = 1.0;
//					}else if( rec == 0 ){
//						precision = 0.0;
//					}else{
//						precision = (double)cut/(double)rec;
//					}
//
//					if( rand == 0 && cut == 0 && rec == 0 ){
//						recall = 1.0;
//					}else if( rand == 0 ){
//						recall = 0.0;
//					}else{
//						recall = (double)cut/(double)rand;
//					}
//
//					srecall += recall;
//					sprecision += precision;
//					cout << mnames[m1]<< " "<< mnames[m2] <<" "<< n<<" "<< k<<" "<<d <<" "<< pness << " " << pnodes<< " "<< recall << " "<<precision<<" "<< rec<< " "<<rec_sce[m1]->length(ALTMIN)<< " "<<rec_sce[m2]->length(ALTMIN)  <<endl;
//					cut_set.clear();// do not delete cut rrrmt pointers, they are just copies
//				}
//			}
//
			for(unsigned j=0; j<rec_sce.size(); j++){
				for( set<rrrmt*,HDereferenceLess>::iterator it=rec_set[j].begin(); it!=rec_set[j].end(); it++ ){delete (*it);}rec_set[j].clear();
				delete rec_sce[j];
			}
		}

		sprecision /= r;
		srecall /= r;

//		cout << ssen*100 <<" "<<sspe*100<<endl;
	}
		// if some file was specified -> process the data in the file
	else{
		vector<vector<rrrmt *> > cm;	// pairwise crex scenarios

		read_taxonomy(filename, tax);
		read_genomes(filename, genomes, names, circular, nmap, true);	// read the genom file
		if(genomes.size() == 0){
			cout << "no genomes in the file"<<endl;
			usage(1);
		}

//		for(unsigned i=0; i<nmap.size(); i++)
//			cerr << i<<" " <<nmap[i]<<endl;

//		for(unsigned i=0; i<genomes.size(); i++){
//			cout << ">"<<names[i]<<endl;
//			cout << genomes[i] <<endl;
//		}

		n = genomes[0].size();
//		init_data(hd, n, circular);

		cm = vector<vector<rrrmt*> >(genomes.size(), vector<rrrmt*>(genomes.size(), NULL));
		for( unsigned i=0; i<genomes.size(); i++ ){
			for(unsigned j=0; j<genomes.size(); j++ ){

//				itnode *tin;
//				list<itnode*> pnodes;
//				interval_tree(genomes[i], genomes[j], genomes[i].size(), &tin);
//				interval_tree_primenodes(tin, pnodes);
//
//				if( pnodes.size() ){
////					cerr << "prime"<<endl;
//					continue;
//				}else{
////					cerr << "li	near "<< names[i]<< " "<<names[j]<<endl;
//				}
//				interval_tree_free(tin);

				if(i==j){
					continue;
				}

				if(bps){
					cm[i][j] = new bpscenario(genomes[i], genomes[j], maxszen, false);
				}else if( crexone ){
//					vector<string> szenarios;
//					compare( genomes[i], genomes[j], nmap, 1, szenarios, maxszen);
					cm[i][j] = new crex(genomes[i], genomes[j], crexbps, maxszen, mkalt);
				}else{
					cm[i][j] = new crex2(genomes[i], genomes[j], true, cst, false, mkalt);//TODO oriented?
				}

#ifndef DOT_COMPONENTS
//				if( cm[i][j]->length(ALTMIN) == 1 ){
//				cout << names[i]<<" "<<names[j]<<" ";
//				copy(tax[i].begin(), tax[i].end(), ostream_iterator<string>(cout," "));cout<< endl;
//				copy(tax[j].begin(), tax[j].end(), ostream_iterator<string>(cout," "));cout<< endl;
//				cm[i][j]->output(cout, 0, 1); cout<<endl;
//				cout << cm[i][j]->length(ALTMIN)<<endl;
//				cout << "---------------------------------"<<endl;
//				}
#endif//DOT_COMPONENTS
			}
		}

#ifdef DOT_COMPONENTS
		vector<unsigned> mno, mnoo,	// minimum outgoing distance
			mni,mnii;				// minimum ingoing  distance
		vector<vector<set<rrrmt*,HDereferenceLess> > > cm_set;	// rrrmt sets for all pairs
		vector<vector<bool> > linear,							// is the sit linear (for all pairs)
			symm;
		vector<vector<int> > graph;
		// maximum connected components of the graph
		vector<vector<unsigned> > comp;
		map<unsigned, unsigned> comp_idx;

//		read_taxonomy(filename, tax);
		if( tax.size() != 0 && tax.size() != names.size() ){
			cerr << "unequal number of tax and names "<<endl;
			exit( EXIT_FAILURE );
		}

		cm_set = vector<vector<set<rrrmt*,HDereferenceLess> > >(genomes.size(), vector<set<rrrmt*,HDereferenceLess> >(genomes.size()));
		linear = vector<vector<bool> >(genomes.size(), vector<bool>(genomes.size(), false));
		symm = vector<vector<bool> >(genomes.size(), vector<bool>(genomes.size(), false));

		// determine
		// - rearrangement sets
		// - symmetric pairs
		// - and pairs with linear sit
		for( unsigned i=0; i<genomes.size(); i++ ){
			for(unsigned j=i+1; j<genomes.size(); j++ ){

				cm[i][j]->getrrrmt(cm_set[i][j], ALTMIN);

//				cout << names[i]<<" -> "<<names[j]<<endl;
//				cout <<*(cm[i][j])<<endl;
//				for( set<rrrmt*,HDereferenceLess>::iterator it=cm_set[i][j].begin(); it!=cm_set[i][j].end(); it++ )
//					cout << *(*it) << endl;
//				cout <<"============"<<endl;

				cm[j][i]->getrrrmt(cm_set[j][i], ALTMIN);

				itnode *iroot;
				list<itnode *> pnodes;
				interval_tree(genomes[i], genomes[j], n, &iroot);
				interval_tree_primenodes(iroot, pnodes);

				if( pnodes.size() == 0 ){
					linear[i][j] = linear[j][i] = true;
//					cout << names[i]<<" "<<names[j]<<" "<< cm[i][j]->size( ALTMIN )<<" "<< cm[j][i]->size( ALTMIN )<<endl;
				}
				interval_tree_free(iroot);
				pnodes.clear();

				set<rrrmt*,HDereferenceLess> unn;
				set_union(cm_set[i][j].begin(), cm_set[i][j].end(), cm_set[j][i].begin(), cm_set[j][i].end(), insert_iterator<set<rrrmt*,HDereferenceLess> >(unn,unn.begin()));
				if( cm_set[i][j].size() == cm_set[j][i].size() && cm_set[i][j].size()==unn.size() ){
//					unn.clear();
					symm[i][j] = symm[j][i] = true;
//					continue;
				}
				unn.clear();

//				print_name(names[i], cout, false);
//				for( set<rrrmt*,DereferenceLess>::iterator it = cm_set[i][j].begin(); it!=cm_set[i][j].end(); it++ ){
//					(*it)->output(0,1,"");
//				}
//				print_name(names[j], cout, false);
//				cout << endl;

			}
		}

		// init empty graph (separat components)
		initgraph(genomes.size(), graph, comp, comp_idx);

		mni = vector<unsigned>(genomes.size(), std::numeric_limits< unsigned >::max());
		mno = vector<unsigned>(genomes.size(), std::numeric_limits< unsigned >::max());
		mnii = vector<unsigned>(genomes.size(), std::numeric_limits< unsigned >::max());
		mnoo = vector<unsigned>(genomes.size(), std::numeric_limits< unsigned >::max());

		for(unsigned i=0; i<genomes.size(); i++){
			for(unsigned j=0; j<genomes.size(); j++){
				if( i==j )
					continue;
				if( linear[i][j] ){
					mno[i] = min( mno[i], (unsigned)cm_set[i][j].size() );
					mni[j] = min( mni[j], (unsigned)cm_set[i][j].size() );
				}
				mnoo[i] = min( mnoo[i], (unsigned)cm_set[i][j].size() );
				mnii[j] = min( mnii[j], (unsigned)cm_set[i][j].size() );
			}
		}

//		copy( mni.begin(), mni.end(), ostream_iterator<unsigned>(cout," ") ); cout << endl;
//		copy( mno.begin(), mno.end(), ostream_iterator<unsigned>(cout," ") ); cout << endl;
//		copy( mnii.begin(), mnii.end(), ostream_iterator<unsigned>(cout," ") ); cout << endl;
//		copy( mnoo.begin(), mnoo.end(), ostream_iterator<unsigned>(cout," ") ); cout << endl;
//		for( unsigned i=0; i<genomes.size(); i++ ){
//			print_name(names[i], cout, false);cout << " : "<<mni[i]<<","<<mno[i]<<endl;
//		}

		// determine graph
		for( unsigned i=0; i<genomes.size(); i++ ){
			for(unsigned j=0; j<genomes.size(); j++ ){
				// i is not connected to itself
				if( i==j){
					continue;
				}

				if(!linear[i][j]){
					continue;
				}
				// i is not connected to j if
				// - i->j >= j->i
				// - the is a shorter outgoing edge from i && there is a shorther ingoing edge to j
				if(
					cm_set[i][j].size() <= cm_set[j][i].size()
					&& cm_set[i][j].size() == mno[ i ]
//					&& cm_set[i][j].size() == mni[j]
					){
					connect(i, j, 1, graph, comp, comp_idx);
					if( symm[i][j] ){
						connect(j, i, 1, graph, comp, comp_idx);
					}
				}
			}
		}

		// connect single tdrl nodes
		for( unsigned i=0; i<genomes.size(); i++ ){
			for(unsigned j=0; j<genomes.size(); j++){
				unsigned tdlcnt = 0;
				for( set<rrrmt*,HDereferenceLess>::iterator it=cm_set[j][i].begin(); it!=cm_set[j][i].end(); it++ ){
					if((*it)->typestrg(1) == "TDL")
						tdlcnt++;
				}

				if(
						tdlcnt == 1 &&
//						cm_set[j][i].size() == 1 &&
						cm_set[j][i].size() == mnii[ i ] &&
						comp_idx[i]!=comp_idx[j]
					){
					connect(j, i, 1, graph, comp, comp_idx);
				}
			}
		}

		map<rrrmt*, int, HDereferenceLess> tdrlidx, ridx, tidx, rtidx;
		int idx=0;

		ofstream tex;
		tex.open( "out.tex" );

		for( unsigned i=0; i<comp.size(); i++ ){
			if( comp[i].size() == 0 ){
				cout << "size 0 component "<<endl;
				continue;
			}
			if( comp[i].size() <= 1 ){
				cout << "size 1 component "; print_name(names[comp[i][0]], cout, false);
//				cout << " : "; copy(tax[comp[i][0]].begin(), tax[comp[i][0]].end(), ostream_iterator<string>(cout," "));
				cout << endl;
				continue;
			}
			cout << "size "<< comp[i].size() <<" component "<< endl;

			ofstream dot;
			dot.open( ("crex-graph"+int2string(i)+".dot").c_str() );

			tex << "\\begin{figure}"<<endl;

			dot << "digraph G{"<<endl;
//			dot << "splines=true;"<<endl;
//			dot << "epsilon=0.01;"<<endl;
//			dot << "overlap=scale;"<<endl;
			for( unsigned j=0; j<comp[i].size(); j++ ){
				print_name(names[comp[i][j]], dot, false);
				dot<< "[";
//				dot<< "[shape=circle,style=filled ";
//				if( find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Chordata" ) != tax[comp[i][j]].end() ){
//					dot <<", fillcolor=\"#003266\"";
//				}else if( find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Echinodermata" ) != tax[comp[i][j]].end() ||
//						find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Hemichordata" ) != tax[comp[i][j]].end() ||
//						find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Xenoturbellida" ) != tax[comp[i][j]].end()){
//					dot <<", fillcolor=\"#0169c9\"";
//				}else if( find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Arthropoda" ) != tax[comp[i][j]].end() ){
//					if( find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Hexapoda" ) != tax[comp[i][j]].end() ){
//						dot <<", fillcolor=\"#ff8600\"";
//					}else if( find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Crustacea" ) != tax[comp[i][j]].end() ){
//						dot <<", fillcolor=\"#ff4900\"";
//					}else{
//						dot <<", fillcolor=\"#c86b1a\"";
//					}
//				}else if( find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Mollusca" ) != tax[comp[i][j]].end() ||
//						find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Annelida" ) != tax[comp[i][j]].end() ||
//						find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Brachiopoda" ) != tax[comp[i][j]].end()||
//						find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Echiura" ) != tax[comp[i][j]].end()||
//						find( tax[comp[i][j]].begin(), tax[comp[i][j]].end(), "Sipuncula" ) != tax[comp[i][j]].end()){
//
//					dot <<", fillcolor=yellow";
//				}
//				else{
//				}
				dot << "color=\"black\", fontname=Helvetica, fontsize=18, label=\"";
//				dot << ", color=\"black\", fontname=Helvetica, fontsize=18, width=0.3, height=0.3, fixedsize=true, label=\"";
				print_name(names[comp[i][j]], dot, true);
				dot << "\"];" <<endl;
			}

			for( unsigned j=0; j<comp[i].size(); j++ ){
				for( unsigned k=0; k<comp[i].size(); k++ ){

					if( graph[comp[i][j]][comp[i][k]] == 0 ){
						continue;
					}

					if( symm[comp[i][j]][comp[i][k]] && comp[i][j] > comp[i][k] ){
						continue;
					}


					unsigned rcnt=0, rtcnt=0, tcnt=0, tdlcnt=0,
						r = 0, g = 0, b = 0;
					for( set<rrrmt*,HDereferenceLess>::iterator it=cm_set[ comp[i][j] ][ comp[i][k] ].begin(); it!=cm_set[comp[i][j]][comp[i][k]].end(); it++ ){
						string tpe = (*it)->typestrg(1);
						if(tpe == "TDL"){
							tdlcnt++;
						}if(tpe=="R"){
							rcnt++;
						}if(tpe=="T"){
							tcnt++;
						}if(tpe=="rT"){
							rtcnt++;
						}
					}
					r = tdlcnt;
					g = rcnt+rtcnt;
					b = tcnt+rtcnt;
					print_name(names[comp[i][j]], dot, false);
					dot <<" -> ";
					print_name(names[comp[i][k]], dot, false);
					dot <<" [";
					if(graph[comp[i][j]][comp[i][k]] == 1){
						dot << "style=solid";
					}else if(graph[comp[i][j]][comp[i][k]] == 2){
						dot << "style=dashed";
					}
					dot << ", color=\"#";
					dot << hex << setfill('0') << setw(2) << (unsigned)(255*(double)r/(r+g+b));
					dot << hex << setfill('0') << setw(2) << (unsigned)(255*(double)g/(r+g+b));
					dot << hex << setfill('0') << setw(2) << (unsigned)(255*(double)b/(r+g+b));
					dot << dec;
					dot << "\"";
//					dot << ", len="<< cm_set[comp[i][j]][comp[i][k]].size();
					dot <<", penwidth=3.0"; // <<3.0*cm_set[comp[i][j]][comp[i][k]].size();
//					dot <<", label=\"(" << rcnt<<","<<tcnt<<","<<rtcnt<<","<<tdlcnt <<")\"";
					dot << ", fontname=Helvetica, fontsize=16";

					dot <<", label=\"";
					for( set<rrrmt*,HDereferenceLess>::iterator it=cm_set[ comp[i][j] ][ comp[i][k] ].begin(); it!=cm_set[comp[i][j]][comp[i][k]].end(); it++ ){
						string tpe = (*it)->typestrg(1);
						dot << tpe;
						if(tpe == "TDL"){
							if( tdrlidx.find(*it) == tdrlidx.end() ){tdrlidx[*it]=tdrlidx.size();}idx=tdrlidx[*it];
						}if(tpe=="R"){
							if( ridx.find(*it) == ridx.end() ){ridx[*it]=ridx.size();}idx=ridx[*it];
						}if(tpe=="T"){
							if( tidx.find(*it) == tidx.end() ){tidx[*it]=tidx.size();}idx=tidx[*it];
						}if(tpe=="rT"){
							if( rtidx.find(*it) == rtidx.end() ){rtidx[*it]=rtidx.size();}idx=rtidx[*it];
						}
						dot<<idx;
					}
					dot << "\"";
					if( symm[comp[i][j]][comp[i][k]] ){
						dot << ", dir=both";
					}

					dot <<"];"<< endl;


					// print gene order comparison file
					stringstream tmp,tmq;
					print_name(names[comp[i][j]], tmp, false);
					print_name(names[comp[i][k]], tmq, false);

//					ofstream goc;
//					goc.open( (tmp.str()+"-"+tmq.str()+".fas").c_str() );
//					goc << ">";print_name(names[comp[i][j]], goc, false);
//					goc << endl<<genomes[comp[i][j]]<<endl;
//					goc << ">";print_name(names[comp[i][k]], goc, false);
//					goc << endl<<genomes[comp[i][k]]<<endl;
//					goc.close();

//					tex << "\\begin{tabular}{ll}"<<endl;
//					for(unsigned l=0; l<cm_set[ comp[i][j] ][ comp[i][k] ].size(); l++){
//						tex << " &"<<endl;
//						tex << "\\includegraphics[width=.9\\textwidth]{rex/figures/nets/rrrmt/"<<tmp.str()<<"-"<<tmq.str()<<"-"<<l<<"-A.eps}\\\\" << endl;
////						tex << "\\begin{tgtfig}"<<endl;
////						tex << " &"<<endl;
////						tex << "\\includegraphics[width=.9\\textwidth]{rex/figures/nets/rrrmt/"<<tmp.str()<<"-"<<tmq.str()<<"-"<<l<<"-B.eps}\\\\" << endl;
////						tex << "\\end{tgtfig}"<<endl;
//					}
//					tex << "\\end{tabular}"<<endl;
//					vector<rrrmt*> tmpr;
//					genom tmpg = genomes[comp[i][j]],
//							tmqg = genomes[comp[i][j]];
//					cm[comp[i][j]][comp[i][k]]->getrrrmt(tmpr, ALTMIN);
//					for(unsigned l=0; l<tmpr.size(); l++){
//						string tpe = (tmpr[l])->typestrg(1);
//						if(tpe == "TDL")idx=tdrlidx[tmpr[l]];
//						if(tpe=="R")idx=ridx[tmpr[l]];
//						if(tpe=="T")idx=tidx[tmpr[l]];
//						if(tpe=="rT")idx=rtidx[tmpr[l]];
//
//						(tmpr[l])->apply(tmqg);
////
//						ofstream goc;
//						goc.open( (tmp.str()+"-"+tmq.str()+"-"+int2string(l)+"-"+tpe+int2string(idx)+".fas").c_str() );
////						goc.open( (tmp.str()+"-"+tmq.str()+"-"+int2string(l)+".fas").c_str() );
//						goc << ">";print_name(names[comp[i][j]], goc, false);
//						goc << endl<<tmpg<<endl;
//						goc << ">";print_name(names[comp[i][k]], goc, false);
//						goc << endl<<tmqg<<endl;
//						goc.close();
//						tmpg = tmqg;
//					}
//					for( unsigned l=0; l<tmpr.size(); l++ ){
//						delete tmpr[l];
//					}
//					tmpr.clear();
				}
			}
			dot << "}"<<endl;
			dot.close();

			tex << "\\includegraphics[width=\\textwidth]{crex-graph"<<i<<".ps}"<<endl;
			tex << "\\caption{crex-graph"<<i<<".dot}"<<endl;
			tex << "\\end{figure}"<<endl;
		}
		tex << "\\begin{tabular}{ll}"<<endl;
		for( map<rrrmt*, int, HDereferenceLess>::iterator it=tdrlidx.begin(); it!=tdrlidx.end(); it++ ){tex<<"TDRL"<<it->second<<" & "; (it->first)->output(tex,0,1,""); tex <<" \\\\"<<endl;}
		for( map<rrrmt*, int, HDereferenceLess>::iterator it=ridx.begin(); it!=ridx.end(); it++ ){tex<<"R"<<it->second<<" & "; (it->first)->output(tex,0,1,""); tex <<" \\\\"<<endl;}
		for( map<rrrmt*, int, HDereferenceLess>::iterator it=tidx.begin(); it!=tidx.end(); it++ ){tex<<"T"<<it->second<<" & "; (it->first)->output(tex,0,1,""); tex <<" \\\\"<<endl;}
		for( map<rrrmt*, int, HDereferenceLess>::iterator it=rtidx.begin(); it!=rtidx.end(); it++ ){tex<<"rT"<<it->second<<" & "; (it->first)->output(tex,0,1,""); tex<<"\\\\"<<endl;}
		tex << "\\end{tabular}"<<endl;
		tex.close();

#endif//DOT_COMPONENTS
		// cout << cm.size()<<endl;
		//cout << "fin"<<endl;

		for(unsigned i=0; i<cm.size(); i++){
			for(unsigned j=0; j<cm[i].size(); j++){
				if( cm[i][j] == NULL ){
					continue;

				}
				if( cm[i][j]->length(ALTMIN) > maxdist || cm[i][j]->length(ALTMIN) < mindist ){
					continue;
				}

				if( i>j && cm[i][j]->length(ALTMIN) == cm[j][i]->length(ALTMIN) ){
					continue;
				}

				set<rrrmt*, HDereferenceLess> rset;
				cm[i][j]->getrrrmt( rset );

				cout << names[i]<< "\t"<< names[j]<<"\t";
				set<rrrmt*, HDereferenceLess>::iterator it = rset.begin();
				if( rset.size()>0 ){
					(*it)->output( 0, 1 );
					it++;
				}
				cout << "\t";

				// cout << endl << genomes[i]<<endl<<genomes[j]<<endl;
				vector<int> bp = genomes[i].breakpoints(genomes[j]);
				for( unsigned k=0; k<bp.size(); k++ ){
					print_element( genomes[i][bp[k]], cout, +1, "", &nmap );
					cout<<"~";
					print_element( genomes[i][bp[k]+1], cout, +1, "", &nmap );
					if(k<bp.size()-1)
						cout<<",";
				}
				cout << endl;
				while(it != rset.end()){
					cout <<"\t\t";
					(*it)->output( 0, 1 );
					cout << "\t"<<endl;
					it++;
				}

//				cout << genomes[i]<<endl;
//				cout << genomes[j]<<endl;
			}
		}
		for(unsigned i=0; i<cm.size(); i++){
			for(unsigned j=0; j<cm[i].size(); j++){
				delete cm[i][j];
			}
		}
		cm.clear();
	}
	delete cst;

	return 0;
}
void getoptions(int argc, char *argv[], string &fname, int &circular, bool &mkalt,
		bool &bps, bool &crexone, bool &crexbps, unsigned &maxalt, bool &randint,
		int &n, int &r, int &k, unsigned &d, float &wI, float &wT, float &wiT, float &wTDRL,
		unsigned &mindist, unsigned &maxdist){

	int c;

	while (1) {
		static struct option long_options[] = {
			// {"noalt",   no_argument, 0, 'a'},
			{"bp",      no_argument, 0, 'b'},
			{"crex1",   no_argument, 0, 'c'},
			{"affect",    required_argument, 0, 'd'},
			{"file",    required_argument, 0, 'f'},
			{"help",   no_argument, 0, 'h'},
			{"events",  required_argument, 0, 'k'},
			{"linear",  no_argument, 0, 'l'},
			// {"primxalt",required_argument, 0, 'm'},
			{"length", required_argument, 0, 'n'},
			{"prinobp", no_argument, 0, 'o'},
			{"repeat", required_argument, 0, 'r'},
			{"version", no_argument, 0, 'v'},
			{"comp", no_argument, 0, 'z'},
			{"wI", required_argument, 0, 'I'},
			{"wT", required_argument, 0, 'T'},
			{"wiT", required_argument, 0, 'N'},
			{"wTDRL", required_argument, 0, 'D'},
			{"mind", required_argument, 0, 'Y'},
			{"maxd", required_argument, 0, 'X'},
			{0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:lv", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;
        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'a':
				mkalt = false;
				break;
			case 'b':
				bps = true;
				break;
			case 'c':
				crexone = true;
				break;
			case 'd':
				d = atoi(optarg);
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage(0);
				break;
			case 'k':
				k = atoi( optarg );
				break;
			case 'l':
				circular = false;
				break;
			case 'm':
				maxalt = atoi( optarg );
				break;
			case 'n':
				n = atoi( optarg );
				break;
			case 'o':
				crexbps = false;
				break;
			case 'r':
				r = atoi(optarg);
				break;
			case 'v':
				cout << PROJECT_VERSION_MAJOR << "." << PROJECT_VERSION_MINOR << "." << PROJECT_VERSION_PATCH << std::endl;
				exit(EXIT_SUCCESS);
			case 'z':
				randint = false;
				break;
			case 'I':
				wI = atof( optarg );
				break;
			case 'T':
				wT = atof( optarg );
				break;
			case 'N':
				wiT = atof( optarg );
				break;
			case 'D':
				wTDRL = atof( optarg );
				break;
			case 'X':
				maxdist = atoi( optarg );
				break;
			case 'Y':
				mindist = atoi( optarg );
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage(0);
        }
	}

	if( crexone and bps ){
		cerr << "the options --crex1 and --bp must not be set simultaneously"<<endl;
		exit( EXIT_FAILURE );
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(unsigned exit_code){
	cout << "usage"<<endl;
	cout << "mode 1: process a gene orders in a file"<<endl;
	cout << "  call: crex -f filename [OPTIONS]"<<endl;
	cout << "mode 2: process random rearrangement scenario"<<endl;
	cout << "        and compare with reconstruction"<<endl;
	cout << "  call: crex [OPTIONS]"<<endl;
	cout << endl;
	cout << "general options"<<endl;
	cout << "--linear -l: handle genomes as linear (default: circular)"<<endl;
	cout << "--bp   : compute with breakpoint scenario [ZhaoBourque07] (default CREx2)"<<endl;
	cout << "--crex1: compute with CREx1 (default CREx2)"<<endl;
	cout << endl;
	cout << "CREx2 options"<<endl;
	cout << "--wI WEIGHT: weight of an inversion"<<endl;
	cout << "--wT WEIGHT:weight of a transposition"<<endl;
	cout << "--wiT WEIGHT: weight of an inverse transposition"<<endl;
	cout << "--wTDRL WEIGHT: weight of a TDRL"<<endl;
	cout << "--noalt: don't compute alternatives for linear nodes"<<endl;
	cout << ""<<endl;
	cout << endl;
	cout << "CREx1 options"<<endl;
	// cout << "--noalt: don't compute alternatives for T+iT"<<endl;
	cout << "--prinobp: don't construct breakpoint scenario for prime nodes"<<endl;
	// cout << "--primxalt: maximal number of alternatives for prime nodes (I+TDRL, bp) (default: 2)"<<endl;
	cout << endl;
	cerr << " --version  : print version and exit"<<endl;

	cout << "mode 1 option"<<endl;
	cout << "--file   -f: specify a filename "<<endl;
	cout << endl;
	cout << "mode 2 options"<<endl;
	cout << "--length: length of the generated gene orders"<<endl;
	cout << "--events: number of random events to apply"<<endl;
	cout << "--repeat: number of repetitions"<<endl;
	cout << "--affect: max number of elements affected by a rearrangement"<<endl;
	cout << "--comp: TDRLs affect complete gene order (default: randomly chosen interval)"<<endl;
	cout << "--wI, --wT, --wiT, --wTDRL: to set rearrangement weights (see CREx2 options) "<<endl;
	cout << endl;

	exit(exit_code);
}
