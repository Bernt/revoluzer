#include <algorithm>
#include <iostream>
#include <list>
#include <limits>
#include <map>
#ifdef THREADED
#include <pthread.h>
#endif//THREADED
#include <unistd.h>		// for gethostname
#include <vector>

#include "conserved.hpp"
#include "dstnc_inv.hpp"
#include "genom.hpp"
#include "helpers.hpp"
#include "io.hpp"


#ifdef USEMPI
#include "mpi.h"
#include "mpi_helpers.hpp"
//~ #include "mpe.h"
#endif//USEMPI

using namespace std;




#ifdef THREADED
/**
 * data structure for transfering data to a merge thread
 */
typedef struct merge_thread_data_struct{
	map<genom, vector<bool> > *used_ptr;	/*! pointer to the global used map*/
	map<genom,bool> *lused_ptr; 			/*! pointer to the local used map*/
	vector<vector<genom> > *front_ptr; 	/*! pointer to the fronts*/
	unsigned *step_ptr, 				/*! pointer to the step number*/
		*max_hit_score_ptr,			/*! maximum number of fronts visited a genome */
		*lSvCnt_ptr, 					/*! number of saved accesses by the local used map*/
		*lAddCnt_ptr, 					/*! number of accesses to the used map*/
		*svCnt_ptr, 					/*! number of senseless accesses to the global used map (front moves in loops ?)*/
		*addCnt_ptr;					/*! number of reasonable accesses to the global used map (merge and additions of the genomes)*/
	vector<bool> *done_ptr;				/*! pointer to a vector which is true for each front which was moved*/

	vector<pair<double, unsigned> > *usedSize_ptr;

	pthread_mutex_t *front_writelock_mtx_ptr,	/*! main thread doesn't write to the fronts if this mutex is available*/
		*finish_mtx_ptr,					/*! main thread is finished if this mutex is available */
		*front_written_sig_mtx_ptr;			/*! mutex for the front_written_sig signal*/
	pthread_cond_t *front_written_sig_ptr;		/*! signal for "something was written to a front" */
	double *merge_time_ptr;					/*! time used for merging*/
} merge_thread_data_t;

/**
 * data structure for transfering data to a front expansion thread
 */
typedef struct getbest_thread_data_struct{
	vector<genom> *g_ptr,
		*root_pack_ptr;
	vector<vector<genom> > *newfront_ptr;
	unsigned *frontidx_ptr;
	unsigned *reduction_ptr,
		*init_mode_ptr;
	unsigned *locSvUsed_ptr,
		*locAddUsed_ptr;

	pthread_mutex_t *front_writelock_mtx_ptr,	/*! main thread doesn't write to the fronts if this mutex is available*/
		*front_written_sig_mtx_ptr;			/*! mutex for the front_written_sig signal*/
	pthread_cond_t *front_written_sig_ptr;		/*! signal for "something was written to a front" */
}getbest_thread_data_t;

#endif//THREADED


/**
 * backtrack the fronts
 *@param[in,out] front the fronts to resore if empty
 *@param[in,out] cuttings the genomes cutted in former steps
 *@param[in] cut_frontsize number of genomes to restore (if not so much are in cuttings then 1)
 *@param[out] done done[i] will be true if front[i] was backtracked
 *@return the number of nonempty or backtracked fronts
 */
unsigned backtrack(
		vector<vector<genom> > &front,
		vector<vector<genom> > &cuttings,
		unsigned cut_frontsize,
		vector<bool> &done);


/**
 * cutfronts: selects from each front maxSize elements with minimal score.
 * the score is the sum of the distances between the front elements and the
 * according origins and the sum of the front element tripel distances.
 *@param[in] fronts the front genomes
 *@param[in] G_orig the origins
 *@param[in] cut_size maximal size of the new fronts
 *@param[in] maxTrials test maximal maxTrials Elements per front (default std::numeric_limits< int >::max())
 *@param[in,out] lowerbound the lowest possible score (will be adjusted in the function)
 *@param[in] verbose if true print some more output (default false)
 *@return true if ???
 */
int cutFronts(
		vector<vector<genom> > &fronts,
		vector<vector<genom> > &cuttings,
		const vector<genom> &G_orig,
		unsigned cut_size,
		unsigned &lowerbound,
		dstnc_inv *rdist,
		unsigned maxTrials = std::numeric_limits< int >::max(),
		bool verbose = false);


#ifdef THREADED
/**
 *
 */
void * getbest_reversals_front_threaded(void * threadarg);
#endif//THREADED

/**
 *@todo DOKU
 *@param[in] G_orig the origin genomes (the input to the program)
 *@param[in] front a front (or a part)
 *@param[in] frontIdx the index of the front (which origin)
 *@param[in,out] lused local used map
 *@param[in] reduction force a minimal distance reduction of ...
 *@param[in] init_mode 0: union, 1: try to maximise, 2: intersection
 *@param[in,out] locSvUsed number of saved accesess by the local used map lused
 *@param[in,out] locAddUsed number of added alements to the local used map
 *@param[in] verbose
 *@return the expanded front
 */
vector<genom> getBestReversalsFront(
	const vector<genom> &G_orig,
	const vector<genom > &front,
	const unsigned &frontIdx,
	map<genom,bool> &lused,
	unsigned reduction,
	unsigned init_mode,
	unsigned &locSvUsed, unsigned &locAddUsed,
	dstnc_inv *rdist,
	bool verbose=false);

#ifdef THREADED
/**
 *@param[in,out] threadarg a pointer to a void casted merge_thread_data_struct
 *@return currently nothing
 */
void * merge_used_threaded(void * threadarg);
#endif//THREADED

/**
 *@param[in,out] used global used map
 *@param[in,out] lused local used map
 *@param[in,out] front fronts
 *@param[in,out] max_meeting_size the maximal number of fronts visited a genome (1,2 or 3)
 *@param[in,out] lSvCnt number of saved accesses by the local used map
 *@param[in,out] lAddCnt number of accesses to the used map
 *@param[in,out] svCnt number of senseless accesses to the global used map (front moves in loops ?)
 *@param[in,out] addCnt number of reasonable accesses to the global used map (merge and additions of the genomes)
 *@param[in,out] merge_time the time needed for the function is added on the given value
 *@param[in,out] usedSize the pair of the size of the used map and the current time is appended to the vector
 *@return true for each front which was moved
 */
vector<bool> mergeUsed(map<genom, vector<bool> > &used,
	map<genom,bool> &lused,
	vector<vector<genom> > &front,
	unsigned &max_meeting_size,
	unsigned &lSvCnt,
	unsigned &lAddCnt,
	unsigned &svCnt,
	unsigned &addCnt,
	double &merge_time,
	vector<pair<double, unsigned> > &usedSize);





void usage();

/**
 * dumps all interresting genomes
 *@param[in] step step of the algorithm
 *@param[in] idx index of the current genome
 *@param[in] dir directory to dump in
 *@param[in] G_orig origin genomes
 *@param[in] G current genomes
 *@param[in] traces the traces
 *@param[in] used used nodes
 *@param[in] bt the reconstructed tree
 *@param[in] delids delete identical genomes before printing
 */
void dumpdata(unsigned step,
		unsigned idx,
		string dir,
		vector<genom> G_orig,
		vector<vector<genom> > &traces,
		map<genom, vector<bool> > &used,
		dstnc_inv *rdist,
		bool delids=true);

/**
 *
 */
int getFronts(vector<vector<genom> > &fronts, 
					vector<genom> &G_orig, 
					vector<genom> &G,
					bool intersect,
					dstnc_inv *rdist){

		// clear the old fronts
	for(unsigned i=0; i<fronts.size(); i++){
		fronts[i].clear();
		//~ cout << G[i]<<endl;
	}

	for(unsigned i=0; i<G.size(); i++){
		map< genom, int > score;
		int maxScore = 1;

		for(unsigned j=0; j<G.size(); j++){
			if(i!=j){
				vector< pair<int, int> > pr;
				//~ cout << "i "<<i<<" j "<<j<< " "<<G[i].distance(G[j])<<endl;
				pr = G[i].getReversalsSameCycle(G[j]);
				for(unsigned k=0; k<pr.size(); k++){
					genom temp=G[i];
					reverse(temp, pr[k]);
					score[temp]++;
				}
			}
		}
		for(map<genom, int>::iterator it=score.begin(); it!=score.end(); it++){
			if(!intersect)
				fronts[i].push_back(it->first);
			else{
				if(it->second == maxScore){
				//~ if(it->second == 2){
					fronts[i].push_back(it->first);
				}
				if(it->second > maxScore){
					maxScore =  it->second;
					fronts[i].clear();
					fronts[i].push_back(it->first);
				}
			}
		}
	}

	return 1;
}

/**
 * get the 'best' reversals from the genome with index idx to the others
 *@param used the genomes used so far 
 *@param G_orig the original vector of genomes (their origins)
 *@param G the vector of genomes
 *@param idx the index (in the vec) of the genome to work on
 *@param onlyCycles only use preserving reversals which are on the same cycle
 *@param doit do the reversal or just try
 *@param cup if true -> take preserving reversals to at least one genome; else take pr to most other genomes
 */
int getBestReversals(map<genom, unsigned> &used, 
						vector<genom> &G_orig, 
						vector<genom> &G, 
						vector<genom> &front, 
						unsigned idx,
						bool doit,
						bool cup,
						dstnc_inv *rdist){

	int distanceSum = 0,
		origDistance = 0,
		maxScore = 1,
		maxDelta = 0;				// distance reduction
	map< pair<int, int>, int > score;
	//~ map< genom, int > score;
	vector< pair<int, int> > bestReversals;
	vector< pair<int, int> > tempReversals;
	//~ vector<genom> candidates,
		//~ tempReversals;

	origDistance = rdist->calc(G[idx],G_orig[idx]);
	for(unsigned j=0; j<G.size(); j++){
		if(idx!=j){
			distanceSum += rdist->calc(G[idx],G_orig[j]);
		}
	}

		// get the preserving reversals (on the same cycle)
	for(unsigned j=0; j<G.size(); j++){
		if(idx!=j){
			vector< pair<int, int> > pr;
			pr = G[idx].getReversalsSameCycle(G[j]);
			for(unsigned k=0; k<pr.size(); k++){
				score[pr[k]]++;
			}
		}
	}

		// get the best (preserving to the most other permutations)
	for(map<pair<int, int>, int>::iterator it=score.begin(); it!=score.end(); it++){
		if(cup)
			bestReversals.push_back(it->first);
		else{
			if(it->second == maxScore){
				bestReversals.push_back(it->first);
			}
			if(it->second > maxScore){
				maxScore =  it->second;
				bestReversals.clear();
				bestReversals.push_back(it->first);
			}
		}
	}

		// get the reversals which reduce the distance most
	for(unsigned j=0; j<bestReversals.size(); j++){
		int delta = distanceSum;
		genom temp=G[idx];
		reverse(temp, bestReversals[j]);
		for(unsigned k=0; k<G.size(); k++){
			if(idx!=k){
				delta -= rdist->calc(temp,G_orig[k]);
			}
		}
		if(delta == maxDelta)
			tempReversals.push_back(bestReversals[j]);
		if(delta > maxDelta){
			maxDelta =  delta;
			tempReversals.clear();
			tempReversals.push_back(bestReversals[j]);
		}
	}
	if(tempReversals.size() || !doit)
		bestReversals = tempReversals;
	tempReversals.clear();

		// filter the reversals which not lead farther away
	for(unsigned j=0; j<bestReversals.size(); j++){
		genom temp=G[idx];
		reverse(temp, bestReversals[j]);
		if(rdist->calc(temp,G_orig[idx]) - origDistance){
		//~ if(temp.intervalDistance(G_orig[idx]) - origDistance){
			tempReversals.push_back(bestReversals[j]);
		}
	}
	if(tempReversals.size() || !doit)
		bestReversals = tempReversals;
	tempReversals.clear();

	if(doit && bestReversals.size()>1){ // get the reversals which result in the most preserving reversals
		int maxNext=0;
		for(unsigned j=0; j<bestReversals.size(); j++){
			int curNext=0;
			vector<genom> trial = G;
			reverse(trial[idx], bestReversals[j]);
			curNext = getBestReversals(used ,G_orig, trial, front, idx, false, cup, rdist);

			if(curNext == maxNext)
				tempReversals.push_back(bestReversals[j]);
			if(curNext > maxNext){
				maxNext = curNext;
				tempReversals.clear();
				tempReversals.push_back(bestReversals[j]);
			}
		}
		if(tempReversals.size() || !doit)
			bestReversals = tempReversals;
		tempReversals.clear();
		// cout << "resulting "<<bestReversals.size() << endl;
	}

	int done = 0;
	for(unsigned j=0; j<bestReversals.size(); j++){
		genom temp = G[idx];					// temporary genome to 'try around'
		reverse(temp, bestReversals[j]);

		if(used[temp]){						// this genome got visited already
			if(used[temp] == idx+1){		// by the same genome ?? -> then this reversal is crap
				continue;
			}else{								// or an other -> then the reversal is just perfect because it merges 2
				if(doit){
					G.clear();
					G.push_back(temp);
					done = 1;
					break;
				}else{
					tempReversals.push_back(bestReversals[j]);
					//~ if(!maxDelta) maxDelta = 1;
					//~ maxDelta *= 1000;
				}
			}
		}else{									// this is a genome what wasn't visited -> this would be ok
			if(doit){
				G[idx] = temp;
				used[G[idx]] = idx+1;
				done = 1;
				break;
			}else{
				tempReversals.push_back(bestReversals[j]);
			}
		}
	}
	if(doit){
		if(!done){
			return -1;
		}else{
			return 1;
		}
	}
	else{
		//~ cout << tab << "RETURN "<<tempReversals.size()*maxDelta<<endl;
	}
	return(tempReversals.size()*maxDelta);
}



//////////////////////////////////////////////////////////////////////////////////////////////////

/**
 */
void getHiscoreGenomes(vector<genom> &result_g, 
	vector<unsigned> &result_c, 
	const vector<genom> &G, 
	unsigned m, 
	unsigned n, 
	unsigned k, 
	char circ,
	bool bio_data,
	dstnc_inv *rdist,
	string dumpfile=""){

	genom id = genom(n, circ);
	int perfect_score = (unsigned) ceil((double)(rdist->calc(G[0],G[1]) + rdist->calc(G[0],G[2]) + rdist->calc(G[1],G[2]) )/2.0);
cout << perfect_score<<endl;
	vector<vector<genom> > results_sorted(4); // 0 : all, 1,2,3 : all results with x hits

	results_sorted[0] = result_g;

	for(unsigned i=0; i<result_g.size(); i++){
		results_sorted[result_c[i]].push_back(result_g[i]);
	}
	for(unsigned i=0; i< results_sorted.size(); i++){
		vector<int> score_mindos(4), score_maxdos(4), score_minis(4), score_maxis(4),
			bestScore_mindos(4, std::numeric_limits< int >::max()), bestScore_maxdos(4, std::numeric_limits< int >::max()), bestScore_minis(4, std::numeric_limits< int >::max()), bestScore_maxis(4, std::numeric_limits< int >::max());
		
			// get avg & max diameter & size of the sets with diff hitcounters
		if(i>1){
			unsigned avg_diameter = 0,
				max_diameter = 0,
				pair_cnt=0;
			for(unsigned j=0; j<results_sorted[i].size(); j++){
				for(unsigned k=j+1; k<results_sorted[i].size(); k++){
					unsigned d=rdist->calc(results_sorted[i][j],results_sorted[i][k]);
					if(d > max_diameter){
						max_diameter = d;
					}
					avg_diameter += d;
					pair_cnt++;
				}
			}
			cout << "SET "<<i<<" SIZE "<< results_sorted[i].size()<< " AVG_D "<<(double)avg_diameter/pair_cnt<<" MAX_D "<<max_diameter<<endl;
		}else{
			cout << "SET "<<i<<" SIZE "<< results_sorted[i].size()<<endl;
		}
		
		
		for(unsigned j=0; j<results_sorted[i].size(); j++){
			int score = 0, dos = 0, is = 0, idos = 0;
			
			score = rdist->calcsum(results_sorted[i][j],G);
			is = intervalDistance(results_sorted[i][j], G);
			if(!bio_data){
				//~ score -= id.distance(G);
				dos = rdist->calc(results_sorted[i][j],id);
				idos = intervalDistance(id, results_sorted[i][j]);
			}

			score_mindos[0] = score; score_mindos[1] = dos; 	score_mindos[2] = is; score_mindos[3] = idos;
			score_maxdos[0] = score; score_maxdos[1] = -1*dos;score_maxdos[2] = is; score_maxdos[3] = idos;
			score_minis[0] = score; 	score_minis[1] = is; 	score_minis[2] = dos; score_minis[3] = idos;
			score_maxis[0] = score;	score_maxis[1] = -1*is;	score_maxis[2] = dos; score_maxis[3] = idos;
			
			if(!bio_data){
				if(score_mindos < bestScore_mindos){
					bestScore_mindos = score_mindos;
				}
				if(score_maxdos < bestScore_maxdos){
					bestScore_maxdos = score_maxdos;
				}
			}
			if(score_minis < bestScore_minis){
				bestScore_minis = score_minis;
			}
			if(score_maxis < bestScore_maxis){
				bestScore_maxis = score_maxis;
			}
		}
		if(results_sorted[i].size()>0){
			if(!bio_data){
				cout <<"MINS+MINDOS "<< i <<"X"<<endl;
				cout << m <<" "<<n<<" "<<k <<" ";
					cout<< bestScore_mindos[1] <<" ";
					cout<< bestScore_mindos[0] <<" ";
					cout<< bestScore_mindos[3]<<" ";
					cout<< bestScore_mindos[2]<<" ";
					cout << bestScore_mindos[0] - perfect_score<<endl;
				
				cout <<"MINS+MAXDOS "<< i <<"X"<<endl;
				cout << m <<" "<<n<<" "<<k <<" ";
					cout <<-1*bestScore_maxdos[1]<<" ";
					cout<< bestScore_maxdos[0] <<" ";
					cout << bestScore_maxdos[3]<<" ";
					cout<< bestScore_maxdos[2]<<" ";
					cout << bestScore_maxdos[0] - perfect_score<<endl;
			}
			cout <<"MINS+MINIS "<< i <<"X"<<endl;
			cout << m <<" "<<n<<" ";
				if(!bio_data)cout <<k <<" ";
				if(!bio_data) cout<< bestScore_minis[2] <<" ";
				cout<< bestScore_minis[0] <<" ";
				if(!bio_data) cout<< bestScore_minis[3] <<" ";
				cout<< bestScore_minis[1]<<" ";
				cout << bestScore_minis[0] - perfect_score<<endl;
	
			cout <<"MINS+MAXIS "<< i <<"X"<<endl;
			cout << m <<" "<<n<<" ";
				if(!bio_data)cout << k <<" ";
				if(!bio_data) cout<< bestScore_maxis[2] <<" ";
				cout<< bestScore_maxis[0] <<" ";
				if(!bio_data) cout<< bestScore_maxis[3] <<" ";
				cout<< -1*bestScore_maxis[1]<<" ";
				cout << bestScore_maxis[0] - perfect_score<<endl;
		}
	}
	

	//~ vector<genom> r_g;
	//~ vector<unsigned> r_c;

	//~ for(unsigned i=0; i<result_g.size(); i++){
		//~ int score = result_g[i].distance(G),
			//~ dos = result_g[i].distance(id),
			//~ is =  result_g[i].intervalDistance(G);

		//~ if(result_c[i] > 1){
			//~ cout <<"#"<< result_c[i] << "x - ";
			//~ cout << m <<" "<<n<<" "<< k <<" "
			//~ <<dos <<" "<< score - perfect_score <<" "
			//~ <<id.intervalDistance(result_g[i]) <<" "<<is <<endl;
			//~ if(result_c[i]==2){
				//~ two.push_back(result_g[i]);
			//~ }
			//~ if(result_c[i]==2){
				//~ three.push_back(result_g[i]);
			//~ }

			
		//~ }

		//~ score_mindos[0] = score; 		score_maxdos[0] = score; 	score_minis[0] = score; 		score_maxis[0] = score;
		//~ score_mindos[1] = dos; 		score_maxdos[1] = -1*dos; 	score_minis[1] = is; 		score_maxis[1] = -1*is;
		//~ score_mindos[2] = result_c[i]; score_maxdos[2] = result_c[i];score_minis[2] = result_c[i]; score_maxis[2] = result_c[i];
		//~ score_mindos[3] = i; 		score_maxdos[3] = i; 		score_minis[3] = i; 		score_maxis[3] = i;

		//~ if(score_mindos < bestScore_mindos){
			//~ bestScore_mindos = score_mindos;
		//~ }
		//~ if(score_maxdos < bestScore_maxdos){
			//~ bestScore_maxdos = score_maxdos;
		//~ }
		//~ if(score_minis < bestScore_minis){
			//~ bestScore_minis = score_minis;
		//~ }
		//~ if(score_maxis < bestScore_maxis){
			//~ bestScore_maxis = score_maxis;
		//~ }
	//~ }
	//~ if(bestScore_mindos[3] != std::numeric_limits< int >::max()) r_g.push_back(result_g[bestScore_mindos[3]]);
	//~ if(bestScore_maxdos[3] != std::numeric_limits< int >::max()) r_g.push_back(result_g[bestScore_maxdos[3]]);
	//~ if(bestScore_minis[3] != std::numeric_limits< int >::max()) r_g.push_back(result_g[bestScore_minis[3]]);
	//~ if(bestScore_maxis[3] != std::numeric_limits< int >::max()) r_g.push_back(result_g[bestScore_maxis[3]]);
	
	//~ if(bestScore_mindos[3] != std::numeric_limits< int >::max()) r_c.push_back(result_c[bestScore_mindos[3]]);
	//~ if(bestScore_maxdos[3] != std::numeric_limits< int >::max()) r_c.push_back(result_c[bestScore_maxdos[3]]);
	//~ if(bestScore_minis[3] != std::numeric_limits< int >::max()) r_c.push_back(result_c[bestScore_minis[3]]);
	//~ if(bestScore_maxis[3] != std::numeric_limits< int >::max()) r_c.push_back(result_c[bestScore_maxis[3]]);
	
	//~ result_g.clear();
	//~ result_c.clear();
	
	//~ result_g = r_g;
	//~ result_c = r_c;
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]){
	char circ = 0;				// curcular genomes
	unsigned 
		m = 0,				// count of genomes 
		n = 0,				// length of genomes
		d = 0, 				// distance from idendity
		step = 0,				// stepcounter
		r_start = 0,			// length of the reversals which generated the set
		r_end = 0,
		reduction=1,			// force a minimal distance reduction 
		init_mode=0,			// candidate set init mode (0: union, 1:try to maximise, 2:intersection)
		cut_frontsize = 0,		// cut fronts bigger than max_front_size back to ...
		max_front_size = 1000,	// maximum size of a front; if bigger -> cut
		lowerbound = 0,		// theoretical minimal tripel score
#ifdef USEMPI
		frontidx = 0,			// index of the current front
		startidx = 0,			// index of the current startElement (in the current front)
		endidx = 0,
		packetsize = 0,		// current size of a package = number of genomes
#endif
		split_quot = 2,		// quotient of the work split
		min_packetsize = 2,		// minimum size of a package
		verbosity = 0,			// verbosity level (0: print nothing; 1: print something about the program state, 
									// 2: print additional statistics)
		max_hit_score = 1,	// the maximal number of fronts visited a genome (1,2 or three)
		min_hit_score = 1;	// the minimal number of fronts visited a genome (1,2 or three) that should be reached

	bool konstant_packagesize=false,
		sort_front = false,		// sort the fronts before cutting
		bio_data = false;

	string filename;				// input filename
	vector<genom> origin;			// origins = input genomes
	vector<string> names;			// names of the input genomes
	vector<vector<genom> > front,		// fronts
		newFront;					// temporary local storage
	vector<vector<genom> > cuttings;	// cutted fronts elements (for each origin)
	map<genom, vector<bool> > used;	// used map
	map<genom, bool> lused;			// local used map

	vector<vector<int> > tempBuffer;	//@todo rename
	vector<unsigned> uBuffer;
	vector<bool> done;

	genom id;						// identity genome (for the computation of the results)	
	dstnc_inv *rdist;						// memory for the genom functions

	int proc_rank = 0,		// rank of the processor
		proc_size = 0;		// number of processors
	int curProc=1;			// a processor number for determination of the next active node
	
	vector<unsigned> tp(3,0);		// one workpacket: current front index, start index in the current front and number of elements
	vector<vector<unsigned> > packet;	// all work packets of one step

#ifdef THREADED
		// ++++++++++ THREAD VARIABLES ++++++++++ 
	vector<genom> root_package;
	unsigned root_package_frontindex;
	pthread_t merge_thread,				// merge thread
		//~ expand_thread
		;
	pthread_mutex_t front_writelock_mtx,	// writelock mutex for the fronts
		finish_mtx,					// signal for the end of the main thread
		front_written_sig_mtx;
	pthread_cond_t front_written_sig;		// condition variable signaling that a front was updated
	merge_thread_data_t merge_thread_data;	// data structure for the merge thread
	getbest_thread_data_t getbest_thread_data;
#endif//THREADED
	
		// ++++++++++ STATISTICS VARIABLES ++++++++++ 
	double 
#ifdef USEMPI
		program_time = 0,
		result_time = 0,
		result_time_trans = 0,
		expand_time = 0,
		expand_calc_time = 0,
		cut_time = 0,
		trans_time_em = 0,
		trans_time_cb = 0,
		trans_time_cg = 0,
		startTime = 0,		
#endif
		merge_time = 0;
	unsigned 
#ifdef USEMPI
		nbSendElements = 0,
		nbRecvElements = 0,
		packagesToSend = 0,
		packagesToSendG = 0,
		packagesToRecv = 0
#endif
		nblSavedAccess = 0,	// nb of known accesses to the loc used map
		nblAccesses = 0,	// nb of new accesses to the loc used map (additions)
		nbSavedAccess = 0,	// nb of known accesses to the glob used map
		nbAccesses = 0,	// nb of new accesses to the glob used map (additions)
		maxLUsedSize = 0;	// maximal size of the local used maps
;
	vector<pair<double, unsigned> > receivedPackages;
	vector<pair<double, unsigned> > usedSize;
	
	char ack = 1;			// acknowledge signal
#ifdef USEMPI
	MPI::Init(argc, argv);
	program_time -= MPI::Wtime();
	startTime = MPI::Wtime();
	proc_rank = MPI::COMM_WORLD.Get_rank();
	proc_size = MPI::COMM_WORLD.Get_size();
#else
	proc_rank = 0;
	proc_size = 1;
#endif//USEMPI

	if(proc_size < 1){
		cout << "There are no Processors available. Exiting ..."<<endl;
		exit(1);
	}
		// the random number generator is initialized (differently) on each processor 
	init_rng(42+proc_rank);

		// get the parameters
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'b': {bio_data = true; break;}
			case 'c': {i++; cut_frontsize = atoi(argv[i]); break;}
			case 'C': {circ = 1; break;}
			case 'f': {i++; filename = argv[i]; break;}
			case 'h': {i++; min_hit_score = atoi(argv[i]); break;}
			case 'i': {i++; init_mode = atoi(argv[i]); break;}
			case 'k': {konstant_packagesize=true; break;}
			case 'm': {i++; max_front_size = atoi(argv[i]); break;}
			case 'q': {i++; split_quot = atoi(argv[i]); break;}
			case 'p': {i++; min_packetsize = atoi(argv[i]); break;}
			case 's': {i++; sort_front = true; break;}
			case 'r': {i++; reduction = atoi(argv[i]); break;}
			case 'v': {i++; verbosity = atoi(argv[i]); break;}
			default:{
				cout << "unknown parameter "<<argv[i]<<endl;
				usage();
				break;
			}
		}
	}

	if(proc_rank == 0){
		if (!filename.size()){		// check if filename specified
			cout << "no input file specified" << endl;
			usage();
		}else{
			read_taxa(filename.c_str(), origin, names, circ);	// read the genom file
			parseFilename(filename.c_str(), m, n, d, r_start, r_end);
		}
		n = origin[0].size();

			// init helping memory for root node
		rdist = new dstnc_inv(n, circ);
		
		m = origin.size();
		
		if(cut_frontsize == 0){
			cut_frontsize = proc_size * min_packetsize ;
			cout << " cut size" <<cut_frontsize<<endl;
		}
			// init the lowerbound for the tripel score 
			// and mark all origin permutations as used
		for(unsigned i=0; i<origin.size(); i++){
			lowerbound += rdist->calc(origin[i],origin[(i+1)%origin.size()]);
			mark(used[origin[i]],i,true);			// set all initial genomes as used
		}
		
		tempBuffer.resize(proc_size);
		uBuffer.resize(proc_size);

#ifdef THREADED
			// init merge pthread mutexes and condition variables
		pthread_mutex_init(&front_writelock_mtx, NULL);
		pthread_mutex_init(&finish_mtx, NULL);
		pthread_mutex_init(&front_written_sig_mtx, NULL);
		pthread_cond_init (&front_written_sig, NULL);
			// init merge thread data structure
		merge_thread_data.used_ptr = &used;
		merge_thread_data.lused_ptr = &lused;
		merge_thread_data.front_ptr = &newFront;
		merge_thread_data.step_ptr = &step;
		merge_thread_data.max_hit_score_ptr = &max_hit_score;
		merge_thread_data.lSvCnt_ptr = &nblSavedAccess;
		merge_thread_data.lAddCnt_ptr = &nblAccesses;
		merge_thread_data.svCnt_ptr = &nbSavedAccess;
		merge_thread_data.addCnt_ptr = &nbAccesses;
		merge_thread_data.done_ptr = &done;
		merge_thread_data.usedSize_ptr = &usedSize;
		merge_thread_data.front_writelock_mtx_ptr = &front_writelock_mtx;
		merge_thread_data.finish_mtx_ptr = &finish_mtx;
		merge_thread_data.front_written_sig_mtx_ptr = &front_written_sig_mtx;
		merge_thread_data.front_written_sig_ptr = &front_written_sig;
		merge_thread_data.merge_time_ptr = &merge_time;

		getbest_thread_data.g_ptr = &origin;
		getbest_thread_data.root_pack_ptr = &root_package;
		getbest_thread_data.newfront_ptr = &newFront;
		getbest_thread_data.frontidx_ptr = &root_package_frontindex;
		getbest_thread_data.reduction_ptr = &reduction;
		getbest_thread_data.init_mode_ptr = &init_mode;
		getbest_thread_data.locSvUsed_ptr  = &nblSavedAccess;
		getbest_thread_data.locAddUsed_ptr = &nblAccesses;
		getbest_thread_data.front_writelock_mtx_ptr = &front_writelock_mtx;
		getbest_thread_data.front_written_sig_mtx_ptr = &front_written_sig_mtx;
		getbest_thread_data.front_written_sig_ptr = &front_written_sig;
#endif//THREADED

	}else{
		tempBuffer.resize(1);
		uBuffer.resize(1);
	}
#ifdef USEMPI
	// bcast all important data to the other procs
	MPI::COMM_WORLD.Bcast( &n, 1, MPI::UNSIGNED, 0);
	MPI::COMM_WORLD.Bcast( &m, 1, MPI::UNSIGNED, 0);
	MPI::COMM_WORLD.Bcast( &d, 1, MPI::UNSIGNED, 0);
	MPI::COMM_WORLD.Bcast( &circ, 1, MPI::CHAR, 0);
	MPI::COMM_WORLD.Bcast( &cut_frontsize, 1, MPI::UNSIGNED, 0);
	MPI::COMM_WORLD.Bcast( &lowerbound, 1, MPI::UNSIGNED, 0);
	BcastGenomeVector(origin, circ, tempBuffer[0], n);
#endif//USEMPI
	
		// init helping memory for all remaining nodes
	if(proc_rank != 0){
		rdist = new dstnc_inv(n, circ);
	}
	
	front.resize(m);
	newFront.resize(m);
	cuttings.resize(m);

	if(proc_rank == 0){
		for(unsigned i=0; i<m; i++){
			front[i].push_back(origin[i]);
		}
	}
//~ int ev_beg = MPE_Log_get_event_number();
//~ int ev_end = MPE_Log_get_event_number();
//~ MPE_Describe_state(ev_beg, ev_end, "expansion", "blue");	

//~ int ev_beg1 = MPE_Log_get_event_number();
//~ int ev_end1 = MPE_Log_get_event_number();
//~ MPE_Describe_state(ev_beg1, ev_end1, "merge", "green1");

//~ int ev_beg2 = MPE_Log_get_event_number();
//~ int ev_end2 = MPE_Log_get_event_number();
//~ MPE_Describe_state(ev_beg2, ev_end2, "cut", "orange1");

//~ int ev_beg3 = MPE_Log_get_event_number();
//~ int ev_end3 = MPE_Log_get_event_number();
//~ MPE_Describe_state(ev_beg3, ev_end3, "master", "cyan1");

	// ******************************************************************* //
	// *************************** MAINLOOP ****************************** //
	// ******************************************************************* //
	do {
//~ MPE_Log_event(ev_beg, step, (char*)0);
		vector<genom> tempFront;				// temp storage, too
		vector<unsigned> size(proc_size, std::numeric_limits< int >::max());			// buffer for receiving the size of the incomming result-genome-vector (per proc because irecv)
		vector<unsigned> procCurFront(proc_size, std::numeric_limits< int >::max()); 	// which processor is doing what currently

		lused.clear();
		packet.clear();

		if(verbosity&1 && proc_rank==0){
			cout << "#m"<<m<<"n"<<n<<"k"<<d<<" step "<<step;
		}
		receivedPackages.clear();
		done.clear();
		done.insert(done.begin(), m, false);
#ifdef THREADED
		root_package.clear();
#endif//THREADED
#ifdef USEMPI
			// broadcast the fronts to all processors
		//~ trans_time_eb -= MPI::Wtime();
		//~ for(unsigned i=0; i< front.size(); i++){
			//~ BcastGenomeVector(front[i], tempBuffer, n);
		//~ }
		//~ trans_time_eb += MPI::Wtime();
		expand_time -= MPI::Wtime();
#endif//USEMPI
		// *************************** FRONT-EXPANSION *************************** //

		//~ if(proc_rank==0){
			//~ frontidx = 0;
			//~ startidx = 0;
			//~ if(front[frontidx].size() / split_quot < 1){
				//~ packetsize = front[frontidx].size() / proc_size;
			//~ }else{
				//~ packetsize = split_quot;
			//~ }
			//~ if(packetsize==0)
				//~ packetsize=1;
			//~ cout << "cps "<<packetsize<<endl;
				//~ // while the current front Index is valid
			//~ while(frontidx < front.size()){
				//cout << "cur Front "<<frontidx<<endl;
				//~ vector<bool> idle(proc_size, 0);
				//~ idle[0] = true;
					//~ // determine the first processor without work
				//~ for(curProc = 1; curProc < proc_size; curProc++){
					//~ if(recvRequest[curProc-1].Test())
						//~ idle[curProc] = true;
				//~ }

				//~ for(unsigned curProc=1; curProc<idle.size(); curProc++){
					//~ if(idle[curProc]){
						//~ tp[0] = frontidx; tp[1] = startidx; tp[2] = packetsize;
						//packet[curProc-1][0] = frontidx; packet[curProc-1][1] = startidx; packet[curProc-1][2] = packetsize;
						//~ trans_time_em1 -= MPI::Wtime();
						//~ MPI::COMM_WORLD.Send(&tp[0], 3, MPI::UNSIGNED, curProc, 0);
						//~ trans_time_em1 += MPI::Wtime();
						//~ trans_time_em2 -= MPI::Wtime();
						//~ recvRequest[curProc-1] = MPI::COMM_WORLD.Irecv(&ack, 1, MPI::CHAR, curProc, 0);
						//~ trans_time_em2 += MPI::Wtime();
						//~ startidx += packetsize;
						//~ idle[0] = false;
					//~ }
					//~ if(startidx >= front[frontidx].size()){
						//~ break;
					//~ }
					
				//~ }
				//~ if(idle[0] && startidx < front[frontidx].size()){
					//~ tempFront.clear();
					//~ expand_calc_time -= MPI::Wtime();
					//~ tempFront = getBestReversalsFront(origin, front, frontidx, startidx, startidx+1, lused, reduction, true, cup, nbFrontElements);
					//~ newFront[frontidx].insert(newFront[frontidx].end(), tempFront.begin(), tempFront.end());
					//~ expand_calc_time += MPI::Wtime();
					//~ startidx += 1;
				//~ }

					//~ // check if the startidx is greater than the size of the current front
				//~ if(startidx >= front[frontidx].size()){
					//~ startidx = 0;
					//~ frontidx++;
					
					//~ if(front[frontidx].size() / split_quot < 1){
						//~ packetsize = front[frontidx].size() / proc_size;
					//~ }else{
						//~ packetsize = split_quot;
					//~ }
					//~ if(packetsize==0)
						//~ packetsize=1;
					//~ if(frontidx < front.size()) cout << "cps "<<packetsize<<endl;
				//~ }
			//~ }
				//~ // if all work is done send abort signal (frontIndex greater than the number of fronts) to all processors
			//~ expand_wait_time -= MPI::Wtime();
			
			//~ for(int i=1; i<proc_size; i++){
				//~ tp[0] = frontidx;
				//~ trans_time_em1 -= MPI::Wtime();
				//MPI::COMM_WORLD.Send(&tp, 3, MPI::UNSIGNED, i, 0);
				//~ MPI::COMM_WORLD.Isend(&tp, 3, MPI::UNSIGNED,i, 0);
				//~ trans_time_em1 += MPI::Wtime();
				
				//~ trans_time_em2 -= MPI::Wtime();
				//recvRequest[i-1] = 
				//~ MPI::COMM_WORLD.Irecv(&ack, 1, MPI::CHAR, i, 0);
				//~ trans_time_em2 += MPI::Wtime();
			//~ }

			//~ expand_wait_time += MPI::Wtime();
		//~ }

#ifdef USEMPI
		if(proc_size > 1){
			if(!konstant_packagesize){
				if(proc_rank == 0){
					vector< MPI::Request > recvRequest(proc_size, MPI::REQUEST_NULL),
						recvRequestGenome(proc_size, MPI::REQUEST_NULL),
						sendRequest(proc_size, MPI::REQUEST_NULL);
					int readyProcs=0;
	
					frontidx = 0;
					startidx = 0;
					packetsize = 0;
					curProc = 1;
					packagesToSend = 0;
#ifdef THREADED
					pthread_mutex_lock(&finish_mtx);
					pthread_create(&merge_thread, NULL, merge_used_threaded, (void *) &merge_thread_data);
#endif//THREADED
						// compute the initial packet size (= (sum of the front sizes)/(quot*proc_size) )
					for(unsigned i=0; i<front.size(); i++){
						packetsize += front[i].size();
					}
					packetsize /= ( split_quot *(proc_size-1));
					
					if(packetsize<min_packetsize){
						packetsize = min_packetsize;
					}
					while(frontidx < front.size()){
						tp[0] = frontidx; 
						tp[1] = startidx; 
						
						if(startidx+packetsize >= front[frontidx].size()){
							tp[2] = front[frontidx].size()-startidx;
						}else{
							tp[2] = packetsize;
						}
	
						packagesToSend += tp[2];
						packet.push_back(tp);
						//~ cout << "pack "<<tp[0]<<","<<tp[1]<<","<<tp[2]<<endl;
						startidx += packetsize;
		
						if(startidx >= front[frontidx].size()){
							startidx = 0;
							frontidx++;
							//~ continue;
						}
						curProc++;
						if(!(curProc<proc_size)){
							curProc=1;
							packetsize /= 2;
							if(packetsize < min_packetsize){
								packetsize = min_packetsize;
							}
//~ #ifdef THREADED
							//~ else if(root_package.size() == 0){
								//~ root_package_frontindex = frontidx;
								//~ if(startidx+packetsize < front[frontidx].size()){
									//~ root_package.insert(root_package.begin(), front[frontidx].begin()+startidx, front[frontidx].begin()+startidx+packetsize);
									//~ front[frontidx].erase(front[frontidx].begin()+startidx, front[frontidx].begin()+startidx+packetsize);
									
								//~ }else{
									//~ root_package.insert(root_package.begin(), front[frontidx].begin()+startidx, front[frontidx].end());
									//~ front[frontidx].erase(front[frontidx].begin()+startidx, front[frontidx].end());
									//~ startidx = 0;
									//~ frontidx++;
								//~ }
							//~ }
//~ #endif//THREADED
						}
					}
//~ #ifdef THREADED
					//~ pthread_create(&expand_thread, NULL, getbest_reversals_front_threaded, (void *) &getbest_thread_data);
//~ #endif//THREADED

					packagesToSendG = packagesToSend;
					packagesToRecv=0;
					
					if(verbosity&2)
						cout << "step"<<step<<" "<<MPI::Wtime()-startTime<<" #send "<<packagesToSend<<" %send "<<(double)packagesToSend/packagesToSendG<<endl;
					
					
					receivedPackages.push_back(pair<double, unsigned>(MPI::Wtime()-startTime, 0 ));
						// send the packages start with a random choosen one then check in cyclic order if they are ready to get new packages
					curProc = (ask_rng()%(proc_size-1))+1;
					for(unsigned i=0; i<packet.size(); ){
						trans_time_em -= MPI::Wtime();
						if(size[curProc] != std::numeric_limits< int >::max() && recvRequest[curProc].Test()){
							recvRequestGenome[curProc] = RecvGenomeVector(tempBuffer[curProc], size[curProc], tempFront, circ,  n, curProc, false, 1);
							size[curProc] = std::numeric_limits< int >::max();
						}
						if(size[curProc] == std::numeric_limits< int >::max() && recvRequestGenome[curProc].Test()){
							if(procCurFront[curProc]!=std::numeric_limits< int >::max()){
								RecvGenomeVector(tempBuffer[curProc], size[curProc], tempFront, circ,  n, curProc, false, 2);
#ifdef THREADED
								pthread_mutex_lock(&front_writelock_mtx);
#endif//THREADED
								newFront[procCurFront[curProc]].insert(newFront[procCurFront[curProc]].end(), tempFront.begin(), tempFront.end());
#ifdef THREADED
								pthread_mutex_unlock(&front_writelock_mtx);
								pthread_mutex_lock(&front_written_sig_mtx);
								pthread_cond_signal(&front_written_sig);
								pthread_mutex_unlock(&front_written_sig_mtx);
#endif//THREADED
								if(verbosity&2){
									nbRecvElements += tempFront.size();
									receivedPackages.push_back(pair<double, unsigned>(MPI::Wtime()-startTime, packagesToRecv+tempFront.size() ));
								}
								packagesToRecv += tempFront.size();
								tempFront.clear();
							}
							MPI::COMM_WORLD.Isend(&packet[i][0], 1, MPI::UNSIGNED, curProc, 5);
						
							SendGenomeVector(front[packet[i][0]], packet[i][1], packet[i][1]+packet[i][2],
								tempBuffer[curProc], uBuffer[curProc], n, curProc, sendRequest[curProc]);
							procCurFront[curProc] = packet[i][0];
							
							if(verbosity&2){
								nbSendElements += packet[i][2];
								packagesToSend -= packet[i][2];
								cout << "step"<<step<<" "<<MPI::Wtime()-startTime<<" #send "<<packagesToSend<<" %send "<<(double)packagesToSend/packagesToSendG<<endl;
							}
							size[curProc]--;
							recvRequest[curProc] = RecvGenomeVector(tempBuffer[curProc], size[curProc], tempFront, circ,  n, curProc, false, 0);
							i++;
							if(!(i<packet.size())){
								trans_time_em += MPI::Wtime();
								break;
							}
						}
						trans_time_em += MPI::Wtime();
							
							// calc the next proc (next or first)
						curProc = (curProc<(proc_size-1)) ? curProc+1 : 1;
					}
						// if all work is done send abort signal (frontIndex greater than the number of fronts) to all processors
					trans_time_em -= MPI::Wtime();
	
						// send abort signal (nonsense front index) to all processes
					tp[0] = front.size();
					for(int i=1; i<proc_size; i++){
						MPI::COMM_WORLD.Isend(&tp[0], 1, MPI::UNSIGNED,i, 5);
					}
						// collect missing results
					while(readyProcs < proc_size-1){
						readyProcs = 0;
						for(int curProc=1; curProc<proc_size; curProc++){
								// if the node never sended something -> ready
							if(procCurFront[curProc]==std::numeric_limits< int >::max()){
								readyProcs++;
							}
								// if something was sended -> complete the receive 
							else{
								if(size[curProc] != std::numeric_limits< int >::max() && recvRequest[curProc].Test()){
									recvRequestGenome[curProc] = RecvGenomeVector(tempBuffer[curProc], size[curProc], tempFront, circ, n, curProc, false, 1);
									size[curProc] = std::numeric_limits< int >::max();
								}
								if(size[curProc] == std::numeric_limits< int >::max() && recvRequestGenome[curProc].Test()){
									RecvGenomeVector(tempBuffer[curProc], size[curProc], tempFront, circ, n, curProc, false, 2);
#ifdef THREADED
									pthread_mutex_lock(&front_writelock_mtx);
#endif//THREADED
									newFront[procCurFront[curProc]].insert(newFront[procCurFront[curProc]].end(), tempFront.begin(), tempFront.end());
#ifdef THREADED
									pthread_mutex_unlock(&front_writelock_mtx);
									pthread_mutex_lock(&front_written_sig_mtx);
									pthread_cond_signal(&front_written_sig);
									pthread_mutex_unlock(&front_written_sig_mtx);
#endif//THREADED
									if(verbosity&2){
										nbRecvElements += tempFront.size();
										receivedPackages.push_back(pair<double, unsigned>(MPI::Wtime()-startTime, packagesToRecv+tempFront.size()));
										packagesToRecv += tempFront.size();
									}
									tempFront.clear();
									procCurFront[curProc]=std::numeric_limits< int >::max();
								}
							}
						}
					}
					trans_time_em += MPI::Wtime();
	
#ifdef THREADED
					//~ pthread_join(expand_thread, NULL);
					pthread_mutex_unlock(&finish_mtx);
					pthread_mutex_lock(&front_written_sig_mtx);
					pthread_cond_signal(&front_written_sig);
					pthread_mutex_unlock(&front_written_sig_mtx);
					pthread_join(merge_thread, NULL);
#endif//THREADED

					if(verbosity&2){
						for( unsigned i=0; i<receivedPackages.size(); i++){
							cout << "step"<<step<<" "<<receivedPackages[i].first <<" #recv "<<receivedPackages[i].second <<" %recv "<<(double)receivedPackages[i].second/packagesToRecv<<endl;
						}
					}
				}
		
				else{
					MPI::Request recvRequest(MPI::REQUEST_NULL),
						sendRequest(MPI::REQUEST_NULL);
					bool cont = true;
					while(cont){
						vector<genom> pack;
							// receive a front index ; if valid receive startIndex and do the work; if not valid 
							// set abort flag
						MPI::COMM_WORLD.Recv(&frontidx, 1, MPI::UNSIGNED, 0, 5);
						if(frontidx < m){
								// receive the work package 
							RecvGenomeVector(tempBuffer[0], size[0], pack, circ, n, 0, true);
								// compute results
							tempFront.clear();
							expand_calc_time -= MPI::Wtime();
							tempFront = getBestReversalsFront(origin, pack, frontidx, lused, reduction, init_mode, nblSavedAccess, nblAccesses);
								// send back the results
							expand_calc_time += MPI::Wtime();
							if(verbosity&2){
								nbRecvElements += pack.size();
								nbSendElements += tempFront.size();
							}
							SendGenomeVector(tempFront, 0, tempFront.size(), tempBuffer[0], uBuffer[0], n, 0, sendRequest);
						}else{
							cont = false;
						}
					}
					
					recvRequest.Wait();
					sendRequest.Wait();
				}
			}// end !konstant_packagesize
			else{
				vector< MPI::Request > sendRequest(proc_size, MPI::REQUEST_NULL);
				if(proc_rank==0){
					for(int i=0; i< (int)front.size(); i++){
						for(int j=1; j<proc_size; j++){
							startidx = (front[i].size() / (proc_size-1))*(j-1);
							endidx = startidx + (front[i].size() / (proc_size-1));
							if(j ==  proc_size-1){
								endidx += (front[i].size() % (proc_size-1));
							}
							//~ cout << "front "<<i<<" "<<startidx<<"-"<<endidx<<"->"<<j<<endl;
							SendGenomeVector(front[i], startidx, endidx, tempBuffer[j], uBuffer[j], n, j, sendRequest[j]);
							if(verbosity&2){
								nbSendElements += (endidx - startidx);
							}
						}
						for(int j=1; j<proc_size; j++)
							sendRequest[j].Wait();
					}
				}else{
					for(unsigned i=0; i<front.size(); i++){
						front[i].clear();
						RecvGenomeVector(tempBuffer[0], size[0], front[i], circ, n, 0, true);
						if(verbosity&2){
							nbRecvElements += front[i].size();
						}
					}
					for(unsigned i=0; i<front.size(); i++){
						newFront[i].clear();
						expand_calc_time -= MPI::Wtime();
						newFront[i] = getBestReversalsFront(origin, front[i], i, lused, reduction, init_mode, nblSavedAccess, nblAccesses);
						expand_calc_time += MPI::Wtime();
					}
				}
					// gather the work 
				for(unsigned i=0; i< newFront.size(); i++){
					if(verbosity&2)
						nbSendElements += newFront[i].size();
					GathervGenomeVector(newFront[i], circ, tempBuffer[0], n);
					if(verbosity&2 && proc_rank == 0)
						nbRecvElements += newFront[i].size();
				}
			}// end konstant_packagesize
		}else{
			for(unsigned i=0; i<front.size(); i++){
				newFront[i]  = getBestReversalsFront(origin, front[i], i, lused, reduction, init_mode, nblSavedAccess, nblAccesses);
				front[i] = newFront[i];
			}
			lused.clear();
		}
#else
		for(unsigned i=0; i<front.size(); i++){
			newFront[i] = getBestReversalsFront(origin, front[i], i, lused, reduction, init_mode, nblSavedAccess, nblAccesses, rdist, false);
			lused.clear();
		}
		ask_rng();	// exactly the same number of rng accesses are needed to get the same results
#endif//USEMPI


#ifdef USEMPI
		expand_time += MPI::Wtime();
//~ MPE_Log_event(ev_end, 0, (char*)0);
//~ MPE_Log_event(ev_beg1, step, (char*)0);
			//~ // gather the work 
		//~ trans_time_eg -= MPI::Wtime();
		//~ for(unsigned i=0; i< newFront.size(); i++){
			//~ nbNewFrontElements+= newFront[i].size();
			
			//~ GathervGenomeVector(newFront[i], circ, tempBuffer[0], n);
		//~ }
		//~ trans_time_eg += MPI::Wtime();
#endif//USEMPI
		

		// *************************** MERGING *************************** //

		if(proc_rank == 0){
			front = newFront;
#ifndef THREADED
			done = mergeUsed(used, lused, front, max_hit_score, nblSavedAccess, nblAccesses, nbSavedAccess, nbAccesses, merge_time, usedSize);
#else
			if(proc_size==1 || konstant_packagesize){
				ask_rng();	// exactly the same number of rng accesses are needed to get the same results
				done = mergeUsed(used, lused, front, max_hit_score, nblSavedAccess, nblAccesses, nbSavedAccess, nbAccesses, merge_time, usedSize);
			}
#endif//THREADED
			usedSize.clear();
			
			if(max_hit_score < min_hit_score){
				bool back=false;
				
				for(unsigned i=0; i<front.size(); i++){
					if(front[i].size()==0){
						back = true;
						break;
					}
				}
				if(back){
					backtrack(front,  cuttings, cut_frontsize, done);
				}
			}

				// check if anything was done
			if(sum(done) <= 0){
				step = 0;
			}else{
				step++;
				//~ if(sum(done)<m)
					//~ backtrack(front,  cuttings, cut_frontsize, done);
				
			}
		}
		for(unsigned i=0; i< newFront.size(); i++){
			newFront[i].clear();
		}
		tempFront.clear();

		if(verbosity&1 && proc_rank==0){
			cout << " -> ("<<front[0].size()<<","<<front[1].size()<<","<<front[2].size()<<")" ;
		}

			// bcast if / if not anything was done (if not then all procs break the loop and quit)
#ifdef USEMPI
		//~ trans_time -= MPI::Wtime();
		MPI::COMM_WORLD.Bcast( &step, 1, MPI::UNSIGNED, 0);
		//~ trans_time += MPI::Wtime();
#endif//USEMPI
		if(step == 0){
			break;
		}

		// *************************** CUT-BACK *************************** //
			// update the lowerbound
		lowerbound -= 3;

			// check if a cutstep is needed
		if(proc_rank==0){
//~ #ifdef USEMPI
			//~ cut_time -= MPI::Wtime();
//~ #endif//USEMPI
			ack = 0;
			for(unsigned i=0; i<front.size(); i++){
				if(front[i].size() > max_front_size){
					ack = 1;
					break;
				}
			}
			//~ if(ack==1){
				//~ newFront = cutFronts(front, origin, cut_frontsize, lowerbound, max_front_size*5);
				//~ for(unsigned i=0; i<front.size(); i++){
					//~ if(front[i].size() > max_front_size){
						//~ front[i] = newFront[i];
					//~ }
				//~ }
				//~ cout << " cut ~> ("<<front[0].size()<<","<<front[1].size()<<","<<front[2].size()<<")";
			//~ }
//~ #ifdef USEMPI
			//~ cut_time += MPI::Wtime();
//~ #endif//USEMPI
		}
//~ MPE_Log_event(ev_end1, 0, (char*)0);
//~ MPE_Log_event(ev_beg2, step, (char*)0);


#ifdef USEMPI
		//~ trans_time -= MPI::Wtime();
		MPI::COMM_WORLD.Bcast( &ack, 1, MPI::CHAR, 0);
		//~ trans_time += MPI::Wtime();
#endif//USEMPI
		if(ack == 1){
			unsigned procCutSize = 0;
				// let proc 0 sort the fronts if wished
			if(proc_rank == 0 && sort_front){
				for(unsigned i=0; i< front.size(); i++){
					sort(front[i].begin(), front[i].end());
				}
			}
#ifdef USEMPI
			trans_time_cb -= MPI::Wtime();
			for(unsigned i=0; i< front.size(); i++){
				BcastGenomeVector(front[i], circ, tempBuffer[0], n);
			}
			trans_time_cb += MPI::Wtime();
			cut_time -= MPI::Wtime();
#endif//USEMPI
			procCutSize = cut_frontsize / proc_size;
			if(cut_frontsize % proc_size){
				for(int i=0; i<(int)cut_frontsize % proc_size; i++){
					if((i%proc_size) == proc_rank){
						procCutSize++;
					}
				}
			}

			cutFronts(front, cuttings, origin, procCutSize, lowerbound, rdist, max_front_size*5/proc_size);
#ifdef USEMPI
			cut_time += MPI::Wtime();
			trans_time_cg -= MPI::Wtime();
			for(unsigned i=0; i< front.size(); i++){
				GathervGenomeVector(newFront[i], circ, tempBuffer[0], n);
			}
			trans_time_cg += MPI::Wtime();
#endif//USEMPI
			unsigned dupl = 0;
			if(proc_rank==0){
				for(unsigned i=0; i<front.size(); i++){
						// look for duplicates
					for(unsigned j=0; j<front[i].size(); j++){
						for(unsigned k=j+1; k<front[i].size(); k++){
							if(front[i][j] == front[i][k])
								dupl++;
							break;
						}
					}
				}
					
			}
			if(verbosity&1 && proc_rank==0){
				cout << " cut ~> ("<<front[0].size()<<","<<front[1].size()<<","<<front[2].size()<<")";
				cout << " D "<<dupl;
				cout << " [";
				for(unsigned i=0; i<cuttings.size(); i++){
					cout << cuttings[i].size()<<" ";
				}
				cout << "] M " <<max_hit_score;
			}
		}
			
		if(verbosity&1 && proc_rank==0){
			cout << endl;
		}
		if(lused.size() > maxLUsedSize)
			maxLUsedSize = lused.size();
		
//~ MPE_Log_event(ev_end2, 0, (char*)0);


	}while (step > 0); // MAINLOOPEND
	
	if(proc_rank==0)
		cout << endl;
	//~ if(proc_rank==0){
		//~ vector<vector<genom> > blub;
		//~ dumpdata(step,	1, "tmp/", origin, blub, used, true);
	//~ }

	// ******************************************************************* //
	// *********************** ANALYSE RESULTS  ********************** //
	// ******************************************************************* //
#ifdef USEMPI
	result_time -= MPI::Wtime();
	result_time_trans -= MPI::Wtime();
#endif//USEMPI
	vector<genom> result_g;
	vector<unsigned> result_c;
#ifdef USEMPI
	vector<MPI::Request> sendRequest(proc_size, MPI::REQUEST_NULL),
		sendRequest2(proc_size, MPI::REQUEST_NULL),
		sendRequest3(proc_size, MPI::REQUEST_NULL);
	MPI::Request req;
#endif//USEMPI
	if(proc_rank == 0){
		vector<vector<genom> > usedMapPart_g(proc_size);
		vector<vector<unsigned> > usedMapPart_c(proc_size);
		vector<unsigned> usedMapPart_cSizes(proc_size);
		
		curProc = 0;
			// reserve some memory in the result vectors - to prevent permanent reallocation in the next loop
		for(int i=0; i<proc_size; i++){
			usedMapPart_g[i].reserve(used.size() / proc_size);
			usedMapPart_c[i].reserve(used.size() / proc_size);
		}
			// divide the used map
		for(map<genom, vector<bool> >::iterator it=used.begin(); it!=used.end(); it++){
			usedMapPart_g[curProc].push_back(it->first);
			usedMapPart_c[curProc].push_back(sum(it->second));
			curProc++;
			curProc %= proc_size;
		}
#ifdef USEMPI
			// send the two vectors
		for(int i=1; i<proc_size; i++){
			SendGenomeVector(usedMapPart_g[i], 0, usedMapPart_g[i].size(), tempBuffer[i], uBuffer[i], n, i, sendRequest[i]);
			usedMapPart_cSizes[i] = usedMapPart_c[i].size();
			sendRequest2[i] =  MPI::COMM_WORLD.Isend(&usedMapPart_cSizes[i], 1, MPI::UNSIGNED, i, 3);
			if(usedMapPart_cSizes[i]>0)
				sendRequest3[i] = MPI::COMM_WORLD.Isend(&usedMapPart_c[i][0], usedMapPart_c[i].size(), MPI::UNSIGNED, i, 4);
		}
#endif//USEMPI
		result_g = usedMapPart_g[0];
		result_c = usedMapPart_c[0];
#ifdef USEMPI
		for(unsigned i=0; i<usedMapPart_g.size(); i++){
			sendRequest[i].Wait();
			usedMapPart_g[i].clear();
			sendRequest2[i].Wait();
			usedMapPart_cSizes.clear();
			sendRequest3[i].Wait();
			usedMapPart_c[i].clear();
		}
#endif//USEMPI
		usedMapPart_g.clear();
		usedMapPart_c.clear();
	}
#ifdef USEMPI
	else{
		RecvGenomeVector(tempBuffer[0], packetsize, result_g, circ, n, 0, true);
		
		MPI::COMM_WORLD.Recv(&packetsize, 1, MPI::UNSIGNED, 0, 3);
		if(packetsize>0){
			result_c.resize(packetsize);
			MPI::COMM_WORLD.Recv(&result_c[0], packetsize, MPI::UNSIGNED, 0, 4);
		}
	}
	result_time_trans += MPI::Wtime();
	getHiscoreGenomes(result_g, result_c, origin, m, n, d, circ, bio_data);
	GathervGenomeVector(result_g, circ, tempBuffer[0], n);
	GathervVector(result_c, proc_size, proc_rank);
#endif//USEMPI
	if(proc_rank == 0){
		//~ id =genom(n, circ);
		getHiscoreGenomes(result_g, result_c, origin, m, n, d, circ, bio_data, rdist);
		//~ unsigned perfect_score = (unsigned) ceil((double)(origin[0].distance(origin[1]) + origin[0].distance(origin[2]) + origin[1].distance(origin[2]) )/2.0);
		//~ cout <<"MINS+MINDOS"<< result_c[0] <<"X"<<endl;
		//~ cout << m <<" "<<n<<" "<< d <<" "
			//~ <<id.distance(result_g[0]) <<" "<< result_g[0].distance(origin) - perfect_score<<" "
			//~ <<id.intervalDistance(result_g[0]) <<" "<<result_g[0].intervalDistance(origin) <<endl;
		//~ cout <<"MINS+MAXDOS"<< result_c[1] <<"X"<<endl;
		//~ cout << m <<" "<<n<<" "<< d <<" "
			//~ <<id.distance(result_g[1]) <<" "<< result_g[1].distance(origin) - perfect_score<<" "
			//~ <<id.intervalDistance(result_g[1]) <<" "<<result_g[1].intervalDistance(origin) <<endl;
		//~ cout <<"MINS+MINIS"<< result_c[2] <<"X"<<endl;
		//~ cout << m <<" "<<n<<" "<< d <<" "
			//~ <<id.distance(result_g[2]) <<" "<< result_g[2].distance(origin) - perfect_score<<" "
			//~ <<id.intervalDistance(result_g[2]) <<" "<<result_g[2].intervalDistance(origin) <<endl;
		//~ cout <<"MINS+MAXIS"<< result_c[3] <<"X"<<endl;
		//~ cout << m <<" "<<n<<" "<< d <<" "
			//~ <<id.distance(result_g[3]) <<" "<< result_g[3].distance(origin) - perfect_score<<" "
			//~ <<id.intervalDistance(result_g[3]) <<" "<<result_g[3].intervalDistance(origin) <<endl;
	}
#ifdef THREADED
	if(proc_rank==0){
		pthread_mutex_destroy(&front_writelock_mtx);
		pthread_mutex_destroy(&finish_mtx);
		pthread_mutex_destroy(&front_written_sig_mtx);
		pthread_cond_destroy(&front_written_sig);
	}
#endif//THREADED

#ifdef USEMPI
	result_time += MPI::Wtime();
	program_time += MPI::Wtime();
#endif//USEMPI
	//~ if(proc_rank == 0){
			//~ // nr, sd, dos
		//~ boost::tuple<int, int, int, genom>	score_mindos, score_maxdos, score_minis,score_maxis;
		//~ boost::tuple<int, int, int, genom> bestScore_mindos(std::numeric_limits< int >::max(),std::numeric_limits< int >::max(),std::numeric_limits< int >::max(), genom()),
			//~ bestScore_maxdos(std::numeric_limits< int >::max(),std::numeric_limits< int >::max(),std::numeric_limits< int >::max(), genom()),
			//~ bestScore_minis(std::numeric_limits< int >::max(),std::numeric_limits< int >::max(),std::numeric_limits< int >::max(), genom()),
			//~ bestScore_maxis(std::numeric_limits< int >::max(),std::numeric_limits< int >::max(),std::numeric_limits< int >::max(), genom());
	
		//~ for(map<genom, vector<bool> >::iterator it=used.begin(); it != used.end(); it++){
			//~ int nr = sum(it->second);
			
			//~ if(nr > 0){
				//~ genom t = it->first;
				
				//~ int s = t.distance(origin),
					//~ // dos = 0;	
					//~ dos = id.distance(t);
				//~ //!@todo nr immer 0 da oftmals bessere ergebnisse mit kleinerer nr.
				//~ score_mindos = boost::tuple<int, int, int, genom>(s, dos, nr, t);
				//~ score_maxdos = boost::tuple<int, int, int, genom>(s, -1*dos, nr, t);
				//~ score_minis = boost::tuple<int, int, int, genom>(s, t.intervalDistance(origin), nr, t);
				//~ score_maxis = boost::tuple<int, int, int, genom>(s, -1*t.intervalDistance(origin), nr, t);
				//~ if(nr > 1){
					//~ cout <<"#"<< nr << "x - ";
					//~ cout << m <<" "<<n<<" "<< d <<" "
						//~ <<dos <<" "<< s - id.distance(origin) <<" "
						//~ <<id.intervalDistance(t) <<" "<<t.intervalDistance(origin) <<endl;
				//~ }
				//~ // if(score == bestScore){
					//~ //scoretable.push_back(boost::tuple<int, int, int, genom>(nr, sd, dos, t));
				//~ // }else 
				//~ if(score_mindos < bestScore_mindos){
					//~ bestScore_mindos = score_mindos;
				//~ }
				//~ if(score_maxdos < bestScore_maxdos){
					//~ bestScore_maxdos = score_maxdos;
				//~ }
				//~ if(score_minis < bestScore_minis){
					//~ bestScore_minis = score_minis;
				//~ }
				//~ if(score_maxis < bestScore_maxis){
					//~ bestScore_maxis = score_maxis;
				//~ }
			//~ }
		//~ }
		//~ id = genom(n, circ);
	
		//~ genom t = boost::get<3>(bestScore_mindos);
		//~ cout <<"MINS+MINDOS"<< boost::get<2>(bestScore_mindos) <<"X"<<endl;
		//~ cout << m <<" "<<n<<" "<< d <<" "
			//~ <<id.distance(t) <<" "<< t.distance(origin) - id.distance(origin)<<" "
			//~ <<id.intervalDistance(t) <<" "<<t.intervalDistance(origin) - id.intervalDistance(origin) <<endl;
		//~ t = boost::get<3>(bestScore_maxdos);
		//~ cout <<"MINS+MAXDOS"<< boost::get<2>(bestScore_mindos) <<"X"<<endl;
		//~ cout << m <<" "<<n<<" "<< d <<" "
			//~ <<id.distance(t) <<" "<< t.distance(origin) - id.distance(origin)<<" "
			//~ <<id.intervalDistance(t) <<" "<<t.intervalDistance(origin) - id.intervalDistance(origin) <<endl;
		//~ t = boost::get<3>(bestScore_minis);
		//~ cout <<"MINS+MINIS"<< boost::get<2>(bestScore_mindos) <<"X"<<endl;
		//~ cout << m <<" "<<n<<" "<< d <<" "
			//~ <<id.distance(t) <<" "<< t.distance(origin) - id.distance(origin)<<" "
			//~ <<id.intervalDistance(t) <<" "<<t.intervalDistance(origin) - id.intervalDistance(origin) <<endl;
		//~ t = boost::get<3>(bestScore_maxis);
		//~ cout <<"MINS+MAXIS"<< boost::get<2>(bestScore_mindos) <<"X"<<endl;
		//~ cout << m <<" "<<n<<" "<< d <<" "
			//~ <<id.distance(t) <<" "<< t.distance(origin) - id.distance(origin)<<" "
			//~ <<id.intervalDistance(t) <<" "<<t.intervalDistance(origin) - id.intervalDistance(origin) <<endl;
	//~ }
	
#ifdef USEMPI
	if(verbosity&1){
		cout << "max_hit_score "<<max_hit_score<<endl;
	}

	if(verbosity&2){

		char hostname[100];
		int hostname_len=100;
	
		double expand_time_g = 0,
			expand_time_max = 0,
			expand_calc_time_g = 0,
			expand_calc_time_max = 0,
			merge_time_g = 0,
			cut_time_g = 0,
			cut_time_max = 0,
			program_time_g = 0,
			program_time_max = 0,
			result_time_g = 0,
			result_time_max = 0,
			result_time_trans_g = 0,
			result_time_trans_max = 0,
			trans_time_em_g = 0,
			trans_time_cb_g = 0,
			trans_time_cb_max = 0,
			trans_time_cg_g = 0,
			trans_time_cg_max = 0;
		unsigned nbSendElements_g = 0,
			nbRecvElements_g = 0,
			nblSavedAccess_g = 0,
			nblAccesses_g = 0;
			
		MPI::COMM_WORLD.Barrier();
		gethostname(hostname, hostname_len);
		
		MPI::COMM_WORLD.Reduce(&expand_time, &expand_time_g, 1, MPI::DOUBLE, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&expand_time, &expand_time_max, 1, MPI::DOUBLE, MPI::MAX, 0);
		MPI::COMM_WORLD.Reduce(&expand_calc_time, &expand_calc_time_g, 1, MPI::DOUBLE, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&expand_calc_time, &expand_calc_time_max, 1, MPI::DOUBLE, MPI::MAX, 0);
		if(proc_rank == 0) merge_time_g = merge_time;
		MPI::COMM_WORLD.Reduce(&cut_time, &cut_time_g, 1, MPI::DOUBLE, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&cut_time, &cut_time_max, 1, MPI::DOUBLE, MPI::MAX, 0);
		MPI::COMM_WORLD.Reduce(&program_time, &program_time_g, 1, MPI::DOUBLE, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&program_time, &program_time_max, 1, MPI::DOUBLE, MPI::MAX, 0);
		MPI::COMM_WORLD.Reduce(&result_time, &result_time_g, 1, MPI::DOUBLE, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&result_time, &result_time_max, 1, MPI::DOUBLE, MPI::MAX, 0);
		MPI::COMM_WORLD.Reduce(&result_time_trans, &result_time_trans_g, 1, MPI::DOUBLE, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&result_time_trans, &result_time_trans_max, 1, MPI::DOUBLE, MPI::MAX, 0);
		if(proc_rank == 0) trans_time_em_g = trans_time_em;
		MPI::COMM_WORLD.Reduce(&trans_time_cb, &trans_time_cb_g, 1, MPI::DOUBLE, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&trans_time_cb, &trans_time_cb_max, 1, MPI::DOUBLE, MPI::MAX, 0);
		MPI::COMM_WORLD.Reduce(&trans_time_cg, &trans_time_cg_g, 1, MPI::DOUBLE, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&trans_time_cg, &trans_time_cg_max, 1, MPI::DOUBLE, MPI::MAX, 0);
		MPI::COMM_WORLD.Reduce(&nbSendElements, &nbSendElements_g, 1, MPI::UNSIGNED, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&nbRecvElements, &nbRecvElements_g, 1, MPI::UNSIGNED, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&nblSavedAccess, &nblSavedAccess_g, 1, MPI::UNSIGNED, MPI::SUM, 0);
		MPI::COMM_WORLD.Reduce(&nblAccesses, &nblAccesses_g, 1, MPI::UNSIGNED, MPI::SUM, 0);
		
		if(proc_rank == 0){
			cout <<"rank\t"<< proc_rank << "\thost\t"<< hostname 
				<<"\tnbSend\t"<<nbSendElements<<"\tnbRecv\t"<<nbRecvElements
				<<"\tnblSvAc\t"<<nblSavedAccess <<"\tnblAcce\t"<<nblAccesses
				<<"\tmLuS\t"<< maxLUsedSize 
				<<"\tet\t"<<expand_time<<"\tect\t"<<expand_calc_time
				<<"\tmt\t"<<merge_time<<"\tct\t"<<cut_time
				<<"\tttcb\t"<<trans_time_cb<<"\tttcg\t"<<trans_time_cg
				<<"\trt\t"<<result_time <<"\trtt\t"<< result_time_trans<< "\tpt\t"<< program_time<<endl;
			for(int i=1; i<proc_size; i++){
				MPI::COMM_WORLD.Recv(&proc_rank, 1, MPI::INT, i, 0);
				MPI::COMM_WORLD.Recv(&hostname, 100, MPI::CHAR, i, 1);
				MPI::COMM_WORLD.Recv(&expand_time, 1, MPI::DOUBLE, i, 3);
				MPI::COMM_WORLD.Recv(&expand_calc_time, 1, MPI::DOUBLE, i, 4);
				MPI::COMM_WORLD.Recv(&merge_time, 1, MPI::DOUBLE, i, 6);
				MPI::COMM_WORLD.Recv(&cut_time, 1, MPI::DOUBLE, i, 7);
				MPI::COMM_WORLD.Recv(&program_time, 1, MPI::DOUBLE, i, 8);
				MPI::COMM_WORLD.Recv(&result_time, 1, MPI::DOUBLE, i, 11);
				MPI::COMM_WORLD.Recv(&result_time_trans, 1, MPI::DOUBLE, i, 12);
				MPI::COMM_WORLD.Recv(&trans_time_cb, 1, MPI::DOUBLE, i, 14);
				MPI::COMM_WORLD.Recv(&trans_time_cg, 1, MPI::DOUBLE, i, 15);
				MPI::COMM_WORLD.Recv(&nbSendElements, 1, MPI::UNSIGNED, i, 16);
				MPI::COMM_WORLD.Recv(&nbRecvElements, 1, MPI::UNSIGNED, i, 17);
				MPI::COMM_WORLD.Recv(&nblSavedAccess, 1, MPI::UNSIGNED, i, 18);
				MPI::COMM_WORLD.Recv(&nblAccesses, 1, MPI::UNSIGNED, i, 19);
				MPI::COMM_WORLD.Recv(&maxLUsedSize, 1, MPI::UNSIGNED, i, 20);
				cout <<"rank\t"<< proc_rank << "\thost\t"<< hostname 
					<<"\tnbSend\t"<<nbSendElements<<"\tnbRecv\t"<<nbRecvElements
					<<"\tnblSvAc\t"<<nblSavedAccess <<"\tnblAcce\t"<<nblAccesses
					<<"\tmLuS\t"<<maxLUsedSize 
					<<"\tet\t"<<expand_time<<"\tect\t"<<expand_calc_time
					<<"\tmt\t"<<merge_time<<"\tct\t"<<cut_time
					<<"\tttcb\t"<<trans_time_cb<<"\tttcg\t"<<trans_time_cg
					<<"\trt\t"<<result_time <<"\trtt\t"<<result_time_trans<< "\tpt\t"<< program_time<<endl;
			}
			if(proc_size>1 && !konstant_packagesize){
				cout <<"SUM\t"	
					<<"\tnbSend\t"<<nbSendElements_g
					<<"\tnbRecv\t"<<nbRecvElements_g
					<<"\tnblSvAc\t"<<nblSavedAccess_g
					<<"\tnblAcce\t"<<nblAccesses_g
					<<"\tnbSvAc\t"<<nbSavedAccess <<"\tnbAcce\t"<<nbAccesses
					<<"\tused\t"<<used.size()
					<<"\tet_avg\t"<<expand_time_g/proc_size
					<<"\tet_max\t"<<expand_time_max
					<<"\tect_avg\t"<<expand_calc_time_g/(proc_size-1)
					<<"\tect_max\t"<<expand_calc_time_max
					<<"\tmt\t"<<merge_time_g
					<<"\tct_avg\t"<<cut_time_g/proc_size
					<<"\tct_max\t"<<cut_time_max
					<<"\tttem\t"<<trans_time_em_g
					<<"\tttcb_avg\t"<<trans_time_cb_g/proc_size
					<<"\tttcb_max\t"<<trans_time_cb_max
					<<"\tttcg_avg\t"<<trans_time_cg_g/proc_size
					<<"\tttcg_max\t"<<trans_time_cg_max
					<<"\trt_avg\t"<<result_time_g/proc_size
					<<"\trt_max\t"<<result_time_max
					<<"\trtt_avg\t"<<result_time_trans_g/proc_size
					<<"\trtt_max\t"<<result_time_trans_max			
					<<"\ttime_avg\t"<<program_time_g/proc_size
					<<"\ttime_max\t"<<program_time_max<<endl;
			}else{
				cout <<"SUM\t"	
					<<"\tnbSend\t"<<nbSendElements_g
					<<"\tnbRecv\t"<<nbRecvElements_g
					<<"\tnblSvAc\t"<<nblSavedAccess_g
					<<"\tnblAcce\t"<<nblAccesses_g
					<<"\tnbSvAc\t"<<nbSavedAccess <<"\tnbAcce\t"<<nbAccesses
					<<"\tused\t"<<used.size()
					<<"\tet_avg\t"<<expand_time_g/proc_size
					<<"\tet_max\t"<<expand_time_max
					<<"\tect_avg\t"<<expand_calc_time_g/proc_size
					<<"\tect_max\t"<<expand_calc_time_max
					<<"\tmt\t"<<merge_time_g
					<<"\tct_avg\t"<<cut_time_g/proc_size
					<<"\tct_max\t"<<cut_time_max
					<<"\tttem\t"<<trans_time_em_g
					<<"\tttcb_avg\t"<<trans_time_cb_g/proc_size
					<<"\tttcb_max\t"<<trans_time_cb_max
					<<"\tttcg_avg\t"<<trans_time_cg_g/proc_size
					<<"\tttcg_max\t"<<trans_time_cg_max
					<<"\trt_avg\t"<<result_time_g/proc_size
					<<"\trt_max\t"<<result_time_max
					<<"\trtt_avg\t"<<result_time_trans_g/proc_size
					<<"\trtt_max\t"<<result_time_trans_max			
					<<"\ttime_avg\t"<<program_time_g/proc_size
					<<"\ttime_max\t"<<program_time_max<<endl;
			}
		}else{
			MPI::COMM_WORLD.Send(&proc_rank, 1, MPI::INT, 0, 0);
			MPI::COMM_WORLD.Send(&hostname, 100, MPI::CHAR, 0, 1);
			MPI::COMM_WORLD.Send(&expand_time, 1, MPI::DOUBLE, 0, 3);
			MPI::COMM_WORLD.Send(&expand_calc_time, 1, MPI::DOUBLE, 0, 4);
			MPI::COMM_WORLD.Send(&merge_time, 1, MPI::DOUBLE, 0, 6);
			MPI::COMM_WORLD.Send(&cut_time, 1, MPI::DOUBLE, 0, 7);
			MPI::COMM_WORLD.Send(&program_time, 1, MPI::DOUBLE, 0, 8);
			MPI::COMM_WORLD.Send(&result_time, 1, MPI::DOUBLE, 0, 11);
			MPI::COMM_WORLD.Send(&result_time_trans, 1, MPI::DOUBLE, 0, 12);
			MPI::COMM_WORLD.Send(&trans_time_cb, 1, MPI::DOUBLE, 0, 14);
			MPI::COMM_WORLD.Send(&trans_time_cg, 1, MPI::DOUBLE, 0, 15);
			MPI::COMM_WORLD.Send(&nbSendElements, 1, MPI::UNSIGNED, 0, 16);
			MPI::COMM_WORLD.Send(&nbRecvElements, 1, MPI::UNSIGNED, 0, 17);
			MPI::COMM_WORLD.Send(&nblSavedAccess, 1, MPI::UNSIGNED, 0, 18);
			MPI::COMM_WORLD.Send(&nblAccesses, 1, MPI::UNSIGNED, 0, 19);
			MPI::COMM_WORLD.Send(&maxLUsedSize, 1, MPI::UNSIGNED, 0, 20);
		}
	}
	MPI::Finalize();
#endif//USEMPI
	return 0;
}

void dumpdata(unsigned step,
		unsigned idx,
		string dir,
		vector<genom> G_orig,
		vector<vector<genom> > &traces,
		map<genom, vector<bool> > &used,
		dstnc_inv *rdist,
		bool delids){

	vector<genom> o;
	vector<string> names;
	string fname=dir+"step_"+int2string(step, 100)+"_idx_"+int2string(idx,10)+".dmp";
	ofstream file;
	genom id = genom( G_orig[0].size(), false);
	bool zero;

	file.open(fname.c_str());
	if(!file.is_open()){
		cout << "dumpfile "<<fname <<" could not get opened."<<endl;
		exit(1);
	}

	//~ o.push_back(id);
	//~ names.push_back("ID");

	for(unsigned i=0; i< G_orig.size(); i++){
		zero = false;
		//!@todo get the name from anywhere else
		//~ string name= "O"+int2string(i)+G_orig[i].getName();
		string name= "O"+int2string(i);

		//!@todo set name anywhere else
		//~ G_orig[i].setName(name);
		for(unsigned j=0; j<o.size(); j++){
			if(rdist->calc(G_orig[i],o[j]) == 0){
				zero = true;
				break;
			}
		}
		if(!zero){
			o.push_back(G_orig[i]);
			names.push_back(name);
		}
	}

	for(unsigned i=0; i< traces.size(); i++){
		for(unsigned j=0; j<traces[i].size(); j++){
			zero = false;
			string name = "t"+int2string(i)+"_"+int2string(j);
			//!@todo set the name from anywhere else
			//~ traces[i][j].setName(name);
			for(unsigned k=0; k<o.size(); k++){
				if(rdist->calc(traces[i][j],o[k]) == 0){
					zero = true;
					break;
				}
			}
			if(!zero){
				o.push_back(traces[i][j]);
				names.push_back(name);
			}
		}
	}
	unsigned nr = 0;
	for(map<genom, vector<bool> >::iterator it=used.begin(); it!=used.end(); it++){
		cout << sum(it->second)<<endl;
		if(sum(it->second)>1){
			string name = int2string(sum(it->second))+int2string(nr);
			o.push_back(it->first);
			names.push_back(name);
			nr++;
		}
	}

	cerr << "ns "<<names.size()<<" os "<<o.size()<<endl;
	file << "\t";
	for(unsigned i=0; i<o.size(); i++){
		file << names[i] << "\t";
	}file << endl;

	for(unsigned i=0; i<o.size(); i++){
		file << names[i]<< "\t";
		for(unsigned j=0; j<o.size(); j++){
			file << rdist->calc(o[i],o[j])<<"\t";
		}
		file << endl;
	}
	file.close();
	return;
}


void usage() {
	cout << "revolver -f inputfile [-C] [-r UINT] [-u] [-m UINT] [-c UINT] [-s UINT]  [-v 0|1|2]"<<endl;
	cout << "\t-f: the inputfile"<<endl;
	cout << "\t-C: treat input as circular genomes (default: linear)"<<endl;
	cout << "\t-i: init mode 0:union, 1:maximise, 2:intersection (default: 0)"<<endl;
	cout << "\t-r: force a minimal distance reduction  (default: 1)"<<endl;
	cout << "\t-m: maximum size of a front (default: 1000)"<<endl;
	cout << "\t-c: nb of elements in a front after cut (default: #proc*minpackagesize)"<<endl;
	cout << "\t-q: split quotient (default: 2)"<<endl;
	cout << "\t-p: minimum size of a package (default: 2)"<<endl;
	cout << "\t-s: sort the fronts before cutting (default: off)"<<endl;
	cout << "\t-u: verbosity level (default: 0)"<<endl;
#ifdef USEMPI	
	MPI::Finalize();
#endif//USEMPI
	exit(0);
}






unsigned backtrack(
		vector<vector<genom> > &front,
		vector<vector<genom> > &cuttings,
		unsigned cut_frontsize,
		vector<bool> &done){

	unsigned front_ok = 0;
	unsigned restore_size=0;
	//~ cout << "backtrack ";
	for(unsigned i=0; i<front.size(); i++){
		if(front[i].size() > 0){
			front_ok++;
			continue;
		}

		if(cuttings[i].size()>0){
			if(cuttings[i].size()>cut_frontsize)
				restore_size = cut_frontsize;
			else
				restore_size = 1;

			front_ok++;
			front[i].insert(front[i].end(), cuttings[i].end()-restore_size, cuttings[i].end());
			cuttings[i].erase(cuttings[i].end()-restore_size, cuttings[i].end());
			mark(done, i, true);
		}
	}
	//~ cout << " B> ("<<front[0].size()<<","<<front[1].size()<<","<<front[2].size()<<")" ;

	return front_ok;
}

//////////////////////////////////////////////////////////////////////////////////////////////////

int cutFronts(
		vector<vector<genom> > &fronts,
		vector<vector<genom> > &cuttings,
		const vector<genom> &G_orig,
		unsigned cut_size,
		unsigned &lowerbound,
		dstnc_inv *rdist,
		unsigned maxTrials,
		bool verbose){


	unsigned ipp,					// counter
		score,					// to store a score value
		step = 0,					// trial counter
		lowerboundReached=0,		// number of fronts with indices belonging to a tripel with a score equal to the lower bound
		prod = 1;					// product of the front sizes (= numer of possible [unordered] tripels )

	vector<unsigned> current_index(fronts.size(), 0);	// the indices of the current tripel
	vector<vector<bool> > included_indices;			// the indices already in the best list are marked (for each front)

	vector<list<pair<unsigned,unsigned> > > best(fronts.size());	// the best found indices (for each front)
	vector<unsigned> min_score(fronts.size(), 0);				// the best score of each best list (for each front)

	pair<unsigned,unsigned> entry;				// an entry of the best list; only for temporary usage
	list<pair<unsigned,unsigned> >::iterator it;

	//~ vector<vector<genom> > newFront(fronts.size());	// the new fronts


	if(verbose){
		cout << "#START cutting Fronts"<<endl;
		cout << "#\tcut back to "<<cut_size<<endl;
		cout << "#\ttrials "<<maxTrials<<endl;
		cout << "#\ttheoretical lower bound "<<lowerbound<<endl;
	}

		// check if all fronts are nonempty (if one is empty return empty fronts)
		// init for each front an bool vector of the length of the corresponding front
		// 	wich should store if an particular index was alread choosen
		// compute the product of the frontsizes, i.e. the number of possible tripels (for ease unordered tripels)
	for(unsigned i=0; i<fronts.size(); i++){


		if(fronts[i].size() == 0){
			for(unsigned j=0; j<fronts.size(); j++){
				cuttings[j].insert(cuttings[j].end(), fronts[j].begin(), fronts[j].end());
				fronts[j].clear();
			}

			if(verbose)
				cout << "#\tone front is empty aborting"<<endl;
			//!@todo maybe it would be better to leave the fronts as they are
			return 0;
		}

		included_indices.push_back(vector<bool>(fronts[i].size(), false));
		prod *= fronts[i].size();
	}
		// if the function should make more trials then tripels are possible -> adjust the number of trials
	if(maxTrials > prod){
		maxTrials = prod;
		if(verbose) cout << "#\treset maxTrials to "<<prod<<endl;
		//!@todo possibly try to get all tripels in this case
	}

	while(lowerboundReached < fronts.size() && step < maxTrials){
		lowerboundReached = 0;
			// update the current indices (get a random index for each front)
		for(unsigned i=0; i<fronts.size(); i++){
			current_index[i] = ask_rng() % fronts[i].size();
		}

		// calculate the score of the current tripel
		score = 0;
		for(unsigned i=0; i<fronts.size(); i++){
			ipp = (i+1) % fronts.size();
			score += rdist->calc(G_orig[i],fronts[i][current_index[i]]) +
				rdist->calc(fronts[i][current_index[i]],fronts[ipp][current_index[ipp]]);
		}

			// now check for each front if the corresponding index is usefull
		for(unsigned i=0; i<fronts.size(); i++){
				// if the list of the best indices if 'full' (i.e., the maximal size is reached)
				// and the worst value in the list is the lowerbound (-> all values in the list have reached the bound)
				// then we can simply continue and forget this index
			if(best[i].size() >= cut_size && min_score[i] <= lowerbound){
				lowerboundReached++;
				continue;
			}

				// if this index is alrady in the best list -> continue
			if(included_indices[i][current_index[i]]){
				continue;
			}
				// if the list of the best indices isn't full or the score is better than the best known score
				// then add the index to the best list
			if(best[i].size() < cut_size || score < min_score[i]){
					// construct the scoretable entry
				entry.first = score;
				entry.second= current_index[i];

					// insert at the correct position
				for(it=best[i].begin(); it!=best[i].end(); it++){
					//~ cout << "try "<<endl;
					if(score >= it->first){
						//~ cout << "insert"<<endl;
						break;
					}
				}
				best[i].insert(it, entry);
				included_indices[i][entry.second] = true;

					// if the best list is now to full -> cutback to cut_size
				if(best[i].size() > cut_size){
					entry = best[i].front();
					included_indices[i][entry.second]=false;
					best[i].pop_front();
				}

					// update the best known value in the best list
				entry = best[i].front();
				min_score[i] = entry.first;
					// check maximum against lowerbound
				if(best[i].size() >= cut_size && min_score[i] <= lowerbound){
					lowerboundReached++;
					if (verbose) cout << "#\tlower bound reached "<<i<<endl;
				}
			}
		}
		//~ cout << "step "<<step<<endl;
		step ++;
	}
	//~ cout << " LB "<<lowerboundReached;

		// update the lowerbound
	lowerbound=std::numeric_limits< int >::max();
	for(unsigned i=0; i<best.size(); i++){
		entry = best[i].back();
		if(entry.first < lowerbound)
			lowerbound = entry.first;
	}

	if(verbose) {
		cout << "#\t"<<step<<" steps needed"<<endl;
		cout << "#\tnew lower bound "<<lowerbound<<endl;
		cout << "#\tcomputed the tripels"<<endl;
	}

	for (unsigned i=0; i<fronts.size(); i++){
		unsigned lb = 0,
			ub = included_indices[i].size()-1;

		//~ cout << "i "<<i<<endl;
		//~ cout << best[i].size()<<endl;;
		//~ for(unsigned j=0; j<included_indices[i].size(); j++){
			//~ cout <<  included_indices[i][j]<<" ";
		//~ }
		//~ cout << endl;

		while (lb < ub){
			if(included_indices[i][lb] == false && included_indices[i][ub] == true){
				included_indices[i][lb] = true;
				included_indices[i][ub] = false;
				swap(fronts[i][lb], fronts[i][ub]);
			}
			if (included_indices[i][lb] == true)
				lb++;
			if (included_indices[i][ub] == false)
				ub--;
		}

		if(included_indices[i][lb] == true){
			lb++;
		}

		//~ cout << "--> i "<<i<<endl;
		//~ for(unsigned j=0; j<included_indices[i].size(); j++){
			//~ cout <<  included_indices[i][j]<<" ";
			//~ if(j==lb) cout << "LB";
		//~ }

		if(lb < included_indices[i].size()){
			cuttings[i].insert(cuttings[i].end(), fronts[i].begin()+lb, fronts[i].end());
			fronts[i].erase(fronts[i].begin()+lb, fronts[i].end());
		}
	}

	if(verbose) cout << "#END cutting Fronts"<<endl;

	return 1;

		// calculate the distances from the origins to the members of the according
		// front for later reuse
	//~ G_origDists.resize(G_orig.size());
	//~ for(unsigned i=0; i<fronts.size(); i++){
		//~ for(unsigned j=0; j<fronts[i].size(); j++){
			//~ G_origDists[i].push_back(G_orig[i].distance(fronts[i][j]));
		//~ }
	//~ }

		// calculate the distances from all elements in one front to all elements
		// in the next front (front_0->front_1,...,front_n->front_0) for later reuse.
		// because we want to calc the sum of a tripel it suffices to calculate the
		// distances between the trace and it's successor
	//~ frontDists.resize(fronts.size());
	//~ for(unsigned i=0; i<	fronts.size(); i++){
		//~ ipp = (i+1) % fronts.size();
		//~ frontDists[i].resize(fronts[i].size());
		//~ for(unsigned j=0; j<frontDists[i].size(); j++){
			//~ frontDists[i][j].resize(fronts[ipp].size());
		//~ }
		//~ for(unsigned j=0; j<fronts[i].size(); j++){
			//~ for(unsigned k=0; k<fronts[ipp].size(); k++){
				//~ frontDists[i][j][k] = fronts[i][j].distance(fronts[ipp][k]);
			//~ }
		//~ }
	//~ }
	/*
	if(verbose) cout << "\tprecomputed the distances"<<endl;
	for(unsigned i=0; i<fronts[0].size();i++){
		if(verbose) cout << "\r"<<i<<"/"<<fronts[0].size() <<endl;
		for(unsigned j=0; j<fronts[1].size(); j++){
			for(unsigned k=0; k<fronts[2].size(); k++){
				score =
					G_orig[0].distance(fronts[0][i])+
					G_orig[1].distance(fronts[1][j])+
					G_orig[2].distance(fronts[2][k])+
					fronts[0][i].distance(fronts[1][j])+
					fronts[1][j].distance(fronts[2][k])+
					fronts[2][k].distance(fronts[0][i]);

					//~ G_origDists[0][i]+G_origDists[1][j]+G_origDists[2][k]+
					//~ frontDists[0][i][j]+frontDists[1][j][k]+frontDists[2][k][i];
				//~ histogramm[score]++;
					// if the besttripels list isn't full or the score is better
					// than the worst of the list insert the current tripel in the list
				for(unsigned l=0; l<best.size(); l++){
					entry.first = score;

					switch (l){	//!@todo make dynamic
						case 0: {entry.second = i; break;}
						case 1: {entry.second = j; break;}
						case 2: {entry.second = k; break;}
					}

					if(!(included_indices[l][entry.second]) && ( best[l].size() < cut_size || (score < min_score[l] && score > minScore[l]))){
						//~ if(l == 1) cout << "score " << score<<endl;

						for(it=best[l].begin(); it!=best[l].end(); it++){
							if(score >= it->first){
								break;
							}
						}

						best[l].insert(it, entry);
						included_indices[l][entry.second] = true;

						//~ if(l==1){
							//~ for(it=best[l].begin(); it!=best[l].end(); it++)
								//~ cout << "("<<it->first << ","<<it->second<<") ";
							//~ cout << endl;
						//~ }
						entry = best[l].back();
						minScore[l] = entry.first;

						entry = best[l].front();
						min_score[l] = entry.first;

						if(best[l].size() > cut_size){
							entry = best[l].front();
							best[l].pop_front();
							included_indices[l][entry.second] = false;
						}
					}
				}
			}
		}
	}
	for(map<unsigned, unsigned>::iterator it=histogramm.begin(); it!=histogramm.end(); it++){
		cout << it->first << " - "<<it->second<<endl;
	}

	if(verbose) cout << "\tcomputed the tripels"<<endl;
	for(unsigned i=0; i< best.size(); i++){
		for(it=best[i].begin(); it!=best[i].end(); it++){
			newFront[i].push_back(fronts[i][it->second]);
		}
	}
	fronts = newFront;

	if(verbose) cout << "END cutting Fronts"<<endl;

	return;*/
}

//////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef THREADED
void * getbest_reversals_front_threaded(void * threadarg){
	getbest_thread_data_t *data = (getbest_thread_data_t *) threadarg;
	vector<genom> tempFront;
	map<genom,bool> lused;


	tempFront = getBestReversalsFront(*(data->g_ptr), *(data->root_pack_ptr), *(data->frontidx_ptr), lused, *(data->reduction_ptr), *(data->init_mode_ptr), *(data->locSvUsed_ptr), *(data->locAddUsed_ptr));

	pthread_mutex_lock(data->front_writelock_mtx_ptr);
	(*(data->newfront_ptr))[*(data->frontidx_ptr)].insert((*(data->newfront_ptr))[*(data->frontidx_ptr)].end(), tempFront.begin(), tempFront.end());
	pthread_mutex_unlock(data->front_writelock_mtx_ptr);

	pthread_mutex_lock(data->front_written_sig_mtx_ptr);
	pthread_cond_signal(data->front_written_sig_ptr);
	pthread_mutex_unlock(data->front_written_sig_mtx_ptr);

	pthread_exit((void* ) NULL);
}
#endif//THREADED

//////////////////////////////////////////////////////////////////////////////////////////////////

vector<genom> getBestReversalsFront(
						const vector<genom> &G_orig,
						const vector<genom > &front,
						const unsigned &frontIdx,
						map<genom,bool> &lused,
						unsigned reduction,
						unsigned init_mode,
						unsigned &locSvUsed, unsigned &locAddUsed,
						dstnc_inv *rdist,
						bool verbose){
	vector<genom> newFront;
	vector<genom> candidates,
		tempCandidates;
	vector< pair<int, int> > pr;
	vector<bool> done;
	map<pair<int, int>, int > score;
	//~ map<genom, bool> lused;

	if(verbose) cout << "START getBestReversalsFront front"<< frontIdx<<endl;
	for(unsigned j=0; j<front.size(); j++){
		int distanceSum = 0,
			origDistance = 0,
			delta,
			maxDelta = reduction,
			maxScore = init_mode;

		if(verbose) cout << "front "<<frontIdx<<" element "<<j<<" : "<<front[j].size()<<endl;

		origDistance = rdist->calc(front[j],G_orig[frontIdx]);
		for(unsigned k=0; k<G_orig.size(); k++){
			if(k!=frontIdx){
				distanceSum += rdist->calc(front[j],G_orig[k]);
			}
		}

		for(unsigned k=0; k<G_orig.size(); k++){
			if(k!=frontIdx){
				pr = front[j].getReversalsSameUnorientedComponentAndSameCycle(G_orig[k]);
				//~ pr = front[j].getReversalsSameCycle(G_orig[k]);
				for(unsigned m=0; m<pr.size(); m++){
					score[pr[m]]++;
				}
				pr.clear();
				//~ for(unsigned l=0; l<front[k].size(); l++){
					//~ pr = front[frontIdx][j].getReversalsSameCycle(front[k][l]);
					//~ for(unsigned m=0; m<pr.size(); m++){
						//~ score[pr[m]]++;
					//~ }
					//~ pr.clear();
				//~ }
			}
		}

		if(verbose)
			cout << "front "<<frontIdx<<" element "<<j<< "\tscore "<<score.size()<<endl;
		for(map<pair<int, int>, int >::iterator it=score.begin(); it!=score.end(); it++){
			genom temp = front[j];
			reverse(temp, it->first);

			if(init_mode == 0){
				candidates.push_back(temp);
			}else{
				if(it->second == maxScore){
					candidates.push_back(temp);
				}
				if(it->second > maxScore){
					maxScore =  it->second;
					candidates.clear();
					candidates.push_back(temp);
				}
			}
		}

		if(verbose)
			cout << "front "<<frontIdx<<" element "<<j<< "\t"<<candidates.size() <<" candidates"<<endl;

		for(unsigned k=0; k<candidates.size(); k++){
			delta = distanceSum;

			for(unsigned l=0; l<G_orig.size(); l++){
				if(l!=frontIdx){
					delta -= rdist->calc(candidates[k],G_orig[l]);
				}
			}
			if(delta >= maxDelta)
				tempCandidates.push_back(candidates[k]);
			// if(delta > maxDelta){
				// maxDelta =  delta;
				// tempCandidates.clear();
				// tempCandidates.push_back(candidates[k]);
			// }
		}
		// if(tempCandidates.size() || !doit)
			candidates = tempCandidates;
		tempCandidates.clear();

		if(verbose)
			cout << "\t"<<candidates.size() <<" cands after fwf"<<endl;

			// filter the reversals which not lead farther away
		for(unsigned k=0; k<candidates.size(); k++){
			if((rdist->calc(candidates[k],G_orig[frontIdx]) - origDistance)>=0){
				tempCandidates.push_back(candidates[k]);
			}
		}
		// if(tempCandidates.size() || !doit)
			candidates = tempCandidates;
		tempCandidates.clear();

		if(verbose)
			cout << "\t"<<candidates.size() <<" cands after bwf"<<" p "<<endl;

		for(unsigned k=0; k<candidates.size(); k++){
			bool *lu_temp = &(lused[candidates[k]]);
			if(*lu_temp == true){
				locSvUsed++;
				continue;
			}else{
				locAddUsed++;
				newFront.push_back(candidates[k]);
				*lu_temp = true;
			}
		}

		candidates.clear();
		score.clear();
	}

	//~ lused.clear();
	if(verbose)
		cout << "END getBestReversalsFront "<<endl;
	return newFront;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef THREADED
void * merge_used_threaded(void * threadarg){
	bool cont = true;
	merge_thread_data_t *data = (merge_thread_data_t *) threadarg;
	vector<vector<genom> > loc_frontpart_copy((*(data->front_ptr)).size()),
		newFront((*(data->front_ptr)).size());
	if(data->done_ptr == NULL){
		cout << "NULL"<<endl;
	}
	do{
		if(pthread_mutex_trylock(data->finish_mtx_ptr) == 0){
			cont = false;
			pthread_mutex_unlock(data->finish_mtx_ptr);
		}
		if(cont == true){
			pthread_mutex_lock(data->front_written_sig_mtx_ptr);
			pthread_cond_wait(data->front_written_sig_ptr, data->front_written_sig_mtx_ptr);
			pthread_mutex_unlock(data->front_written_sig_mtx_ptr);
			pthread_mutex_lock(data->front_writelock_mtx_ptr);
		}
#ifdef USEMPI
			(*(data->merge_time_ptr)) -= MPI::Wtime();
#endif//USEMPI
			for(unsigned i=0; i < loc_frontpart_copy.size(); i++){
				loc_frontpart_copy[i] = (*(data->front_ptr))[i];
				(*(data->front_ptr))[i].clear();
			}
#ifdef USEMPI
			(*(data->merge_time_ptr)) += MPI::Wtime();
#endif//USEMPI
		if(cont == true){
			pthread_mutex_unlock(data->front_writelock_mtx_ptr);
		}
#ifdef USEMPI
			(*(data->merge_time_ptr)) -= MPI::Wtime();
#endif//USEMPI
		for(unsigned i=0; i<loc_frontpart_copy.size(); i++){
			for(unsigned j=0; j<loc_frontpart_copy[i].size(); j++){
				vector<bool> *u_temp;
				bool *lu_temp = &((*(data->lused_ptr))[loc_frontpart_copy[i][j]]);

				if(*lu_temp){
					(*(data->lSvCnt_ptr))++;
					continue;
				}else{
					(*lu_temp) = true;
				}

				u_temp = &((*(data->used_ptr))[loc_frontpart_copy[i][j]]);

				(*(data->lAddCnt_ptr))++;

				if(sum(*u_temp) > 0){					// this genome got visited already
					if(isMarked(*u_temp, i)){			// by the same genome ?? -> then this reversal is crap
						(*(data->svCnt_ptr))++;
						continue;
					}else{							// or an other -> then the reversal is just perfect because it merges 2
						(*(data->addCnt_ptr))++;
						mark(*u_temp, i, true);
						mark(*(data->done_ptr), i, true);
						if(sum(*u_temp)>*(data->max_hit_score_ptr)){
							*(data->max_hit_score_ptr) = sum(*u_temp);
						}

					}
				}else{								// this is a genome what wasn't visited -> this would be ok
					(*(data->addCnt_ptr))++;
					newFront[i].push_back(loc_frontpart_copy[i][j]);
					mark(*u_temp, i, true);
					mark(*(data->done_ptr), i, true);
				}
			}
		}
#ifdef USEMPI
		(*(data->usedSize_ptr)).push_back(pair<double, unsigned>(MPI::Wtime(), (*(data->used_ptr)).size() ));
		(*(data->merge_time_ptr)) += MPI::Wtime();
#endif//USEMPI

	}while(cont);

	(*(data->front_ptr)) = newFront;
	pthread_exit((void* ) NULL);
}
#endif//THREADED
//////////////////////////////////////////////////////////////////////////////////////////////////

vector<bool> mergeUsed(map<genom, vector<bool> > &used,
		map<genom,bool> &lused,
		vector<vector<genom> > &front,
		unsigned &max_hit_score,
		unsigned &lSvCnt,
		unsigned &lAddCnt,
		unsigned &svCnt,
		unsigned &addCnt,
		double &merge_time,
		vector<pair<double, unsigned> > &usedSize){



	merge_time -= get_time();

	vector<bool> done(front.size(), false);			// stores for each front if some action was done
	vector<vector<genom> > newFront(front.size());	// the new fronts

	for(unsigned i=0; i<front.size(); i++){
		for(unsigned j=0; j<front[i].size(); j++){

			vector<bool> *u_temp;
			bool *lu_temp = &(lused[front[i][j]]);

				// first check against the local used map, because the front may include duplicates
				// -> the time for accessing the huge global used map is saved
			if(*lu_temp){
				lSvCnt++;
				continue;
			}else{
				(*lu_temp)=true;
			}

			lAddCnt++;
				// get the entry of the global used map (will be created if not there yet)
			u_temp = &(used[front[i][j]]);
			if(sum(*u_temp) > 0){					// this genome got visited already
				if(isMarked(*u_temp, i)){			// by the same genome ?? -> then this reversal is crap
					svCnt++;
					continue;
				}else{							// or an other -> then the reversal is just perfect because it merges 2
					addCnt++;
					mark(*u_temp, i, true);
					mark(done, i, true);
					if(sum(*u_temp)>max_hit_score){
						max_hit_score = sum(*u_temp);
					}
				}
			}else{								// this is a genome what wasn't visited -> this would be ok
				addCnt++;
				newFront[i].push_back(front[i][j]);
				mark(*u_temp, i, true);
				mark(done, i, true);
			}
		}
		usedSize.push_back(pair<double, unsigned>(get_time(), used.size() ));
	}

	front = newFront;
	merge_time += get_time();
	return done;
}
