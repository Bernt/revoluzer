/**
 * program to get a one step rearrangement network based on CREx
 * @author: M. Bernt
 */

#include <algorithm>
#include <getopt.h>
#include <iostream>
#include <iterator>
#include <set>

#include "crex.hpp"
#include "counter.hpp"
#include "genom.hpp"
#include "helpers.hpp"
#include "io.hpp"

#define DEFAULT_CIRCULAR true	// default circularity:true (linear:false)
#define DEFAULT_CREXBPS true	// use breakpoint szenarios for prime nodes
#define DEFAULT_MAXSZEN 2	// maximum number of alternatives for prime nodes
#define DEFAULT_ALTERNATIVES false // compute alternative szenarios
#define DEFAULT_MAXITER 2	// maximum number of alternatives for prime nodes


using namespace std;

/**
 * read command line options
 * @param[in] argc number of arguments
 * @param[in] argv the arguments
 * @param[out] fname input filename
 * @param[in,out] circular the circularity of the genomes
 * @param[in,out] crexbps use breakpoint szenarios for prime nodes
 * @param[in,out] maxszen maximum number of alternatives for prime nodes
 * @param[in,out] mkalt compute alternative szenarios
 */
void getoptions( int argc, char *argv[], string &fname, bool &circular,
		bool &crexbps, unsigned &maxszen, bool &mkalt);

/**
 * print usage info and exit
 */
void usage();

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int main(int argc, char *argv[]){

	bool circular = DEFAULT_CIRCULAR,	// treat genomes as circular
		crexbps = DEFAULT_CREXBPS,	// use breakpoint szenarios for prime nodes
		mkalt = DEFAULT_ALTERNATIVES;	// compute alternative szenarios
	set<genom> nw,nnw;
	string fname; 	// file name for input
	unsigned maxszen = DEFAULT_MAXSZEN, 	// maximum number of alternatives for prime nodes
			maxiter = DEFAULT_MAXITER,
			iter = 0;
	vector<genom> genomes;
	vector<string> names, 	// the names of the input genomes
		nmap; 				// the names of the genes

	getoptions( argc, argv, fname, circular, crexbps, maxszen, mkalt );

	// read the genomes from the file
	read_genomes(fname, genomes, names, circular, nmap);
	nnw.insert(genomes.begin(), genomes.end());
	cerr << nnw.size()<<endl;


	do{
		nw = nnw;
		for( set<genom>::iterator it=nw.begin(); it!=nw.end(); it++ ){
			for( set<genom>::iterator jt=nw.begin(); jt!=nw.end(); jt++ ){
				rrrmt *sce;
				genom tg = *it;
				sce = new crex(*it, *jt, crexbps, maxszen, mkalt);
				cout << *sce<<endl;
				sce->apply( tg, nnw );
				delete sce;
			}
		}
		cerr << "old "<<nnw.size()<<" new "<<nw.size()<<endl;
		iter++;
	}while( iter < maxiter && nnw.size() > nw.size() );

//	genomes = ingenomes;
//	compcnt = genomes.size();
//	components = vector<unsigned>(genomes.size(), 0);
//	for(unsigned i=0; i<genomes.size(); i++){
//		components[i] = i;
//	}
//	do{
//		cerr << "###################"<<endl;
//		cerr << "# nrex dist="<<maxdist<<" "<<genomes.size()<< " genomes "<<compcnt << " components"<<endl;
//		spanning_network( genomes, components, compcnt, szenarios, distances, maxdist, crexbps, maxszen, mkalt );
//		if( compcnt > 1 ){
//			maxdist++;
//		};
//
//		cerr << "# => "<<genomes.size()<< " genomes "<<compcnt << " components"<<endl;
//	}while( compcnt > 1 );


//	ofstream dotfile;
//	dotfile.open ("test.dot");
//
//
////	output nodes
//	dotfile << "digraph G{"<<endl;
//	for( unsigned i=0; i<genomes.size(); i++ ){
//		stringstream  name;
//		if( i<names.size() ){
//			name << names[i];
//		}else{
//			name << "A"<<i;
//		}
//		dotfile << i<<" [shape=box, label=\""<<name.str()<<"\"];"<<endl;
////		cerr << ">"<<name.str()<<endl;
////		cerr << genomes[i]<<endl;
//
//	}
//
////	dotfile << "    graph [rankdir=LR nodesep=0]"<<endl;
////	dotfile << "{ rank = same; ";
////	for( unsigned i=0; i<names.size(); i++){
////		dotfile << "\""<<i<<"\";"<<endl;
////	}
////	dotfile << "}"<<endl;
//
//
//	set<rrrmt*, HDereferenceLess> rset;
//	map<rrrmt*, string, HDereferenceLess> ridx;
//	for( unsigned i=0; i<genomes.size(); i++ ){
//		for( unsigned j=0; j<genomes.size(); j++ ){
//			if( szenarios.find( make_pair(i,j) ) == szenarios.end() ){
//				continue;
//			}
//			szenarios[make_pair(i,j)]->getrrrmt( rset, ALTALL );
//		}
//	}
//
//
//	ridx = getrrrmtidx( rset );
////	cerr << "rearrangement index"<<endl;
////	for(map<rrrmt*, string, HDereferenceLess>::iterator it=ridx.begin(); it!=ridx.end(); it++){
////		it->first->output(cerr, 0, 1 );
////		cerr << " -> "<<it->second<<endl;
////	}
//
//	// output edges
//	for( unsigned i=0; i<genomes.size(); i++ ){
//		for( unsigned j=i+1; j<genomes.size(); j++ ){
//			vector<rrrmt*> r;
//			unsigned minlen;
//			string dir;
//			pair<unsigned,unsigned> ij=make_pair(i,j),
//				ji=make_pair(j,i);
//
//			if( distances.find(ij)!=distances.end() && (distances.find(ji)==distances.end() || distances[ij] < distances[ji]) ){
//				szenarios[make_pair(i,j)]->getrrrmt( r, ALTALL );
//				minlen = distances[ij];
//				dir="forward";
//			}else if( distances.find(ji)!=distances.end() && (distances.find(ij)==distances.end() || distances[ji] < distances[ij]) ){
//				szenarios[make_pair(j,i)]->getrrrmt( r, ALTALL );
//				minlen = distances[ji];
//				dir="back";
//			}
//			else if( distances.find(ij)!=distances.end() &&  distances.find(ji)!=distances.end() && distances[ij] == distances[ji] ){
//				szenarios[make_pair(i,j)]->getrrrmt( r, ALTALL );
//				minlen = distances[ij];
//				dir="both";
//			}else{
//				continue;
//			}
//
////			cerr<< *(r[0])<<endl;
//
//
//			dotfile <<i<< "->"<<j<<" [dir="<<dir<<endl;
//			dotfile<<", minlen="<<minlen<<endl;
//			dotfile << ", label=\"";
//			for( unsigned k=0; k<r.size(); k++ ){
//				dotfile <<ridx[r[k]];
//				delete r[k];
//			}
//			dotfile<<"\""<<endl;
//			dotfile << "]"<<endl;
//			r.clear();
//		}
//	}
//	dotfile << "}"<<endl;
//	dotfile.close();
//	// free memory
//	for( map<pair<unsigned,unsigned>, rrrmt* >::iterator it=szenarios.begin();
//			it!=szenarios.end(); it++ ){
//		delete it->second;
//	}

	return EXIT_SUCCESS;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


void getoptions( int argc, char *argv[], string &fname, bool &circular,
		bool &crexbps, unsigned &maxszen, bool &mkalt ){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"file",    required_argument, 0, 'f'},
			{"help",    no_argument,       0, 'h'},
            {0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "af:hlm:o",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'a':
				mkalt = false;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case 'l':
				circular = false;
				break;
			case 'm':
				maxszen = atoi(optarg);
				break;
			case 'o':
				crexbps = false;
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
				break;
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cout << "usage"<<endl;
	cout << "general options"<<endl;
	cout << " -a: don't compute alternatives"<<endl;
//	cout << " -b: compute breakpoint scenario [ZhaoBourque07]"<<endl;
	cout << " -o: don't use breakpoint scenario for prime nodes"<<endl;
	cout << " -m: maximal number of reversals for reversal+tdrl scenarios (default: 2)"<<endl;
	cout << "process a file: osrex -f file"<<endl;
	cout << " -f: specify a filename "<<endl;
	cout << " -l: handle genomes as linear (default: circular)"<<endl;
	exit(EXIT_FAILURE);
}
