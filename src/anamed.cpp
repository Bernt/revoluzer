/**
 * analyse a set of medians
 * author: Matthias Bernt
 */


#include <iostream>
#include <limits>
#include <vector>

#include "caprara.hpp"
#include "common.hpp"
#include "dstnc_inv.hpp"
#include "genom.hpp"
#include "io.hpp"
#include "median.hpp"


using namespace std;


/**
 * analyse a set of medians, i.e. compute:
 * - the diameter of the median set
 * - the number of components
 * - the minimal and maximal distance of the components. where the distance between two components is the minimal distance between genomes in the two components
 *
 * @param[in] medians the set of medians to analyse
 * @param[in] genomes the input genomes (needed for perfect distance)
 * @param[in] common user perfect reversal distance
 * @param[out] diam the diameter
 * @param[out] cmpcnt the number of components
 * @param[out] mincmpdist the minimal distance between two components
 * @param[out] maxcmpdist the maximal distance between two components
 * @param[in] hd helping memory
 */
void analyse_medians( const vector<genom> &medians, const vector<genom> &genomes, int common, int &diam, int &cmpcnt, int &mincmpdist, int &maxcmpdist, hdata &hd  );

/**
 * print some usage information and exit
 */
void usage();


/**
 * here the work is done :-)
 */
int main(int argc, char *argv[]) {
	double avgdts = 0.0,	// average distance to solution
		avgpdts = 0.0,
		avgscore = 0.0,
		avgpscore = 0.0,
		avgmci = 0.0;
	genom id; 					// idendity
	mediansolver *imp,
		*pimp;
	dstnc_inv *rdist;
	dstnc_pinv *prdist;

	int common = 0,				// use perfect reversal distance
		diam = std::numeric_limits< int >::max(), 		// the diameter
		cmpcnt = std::numeric_limits< int >::max(), 		// the number of components
		mincmpdist = std::numeric_limits< int >::max(), 	// the minimal distance between two components
		maxcmpdist = std::numeric_limits< int >::max(), 	// the maximal distance between two components
		pdiam = std::numeric_limits< int >::max(), pcmpcnt = std::numeric_limits< int >::max(), pmincmpdist = std::numeric_limits< int >::max(), pmaxcmpdist = std::numeric_limits< int >::max(),
		lb = std::numeric_limits< int >::max(), 			// lower bound of the median problem
//		lscore = std::numeric_limits< int >::max(),		// for sanity checks
		score = std::numeric_limits< int >::max(), 		// score of the solution
		minscore = std::numeric_limits< int >::max(),
		maxscore = 0,
		dts = std::numeric_limits< int >::max(),			// distance to solution
		mindts = std::numeric_limits< int >::max(), 		// minimal ..
		maxdts = 0,		 		// .. and maximal distance to solution

		pscore = std::numeric_limits< int >::max(), 		//
		minpscore = std::numeric_limits< int >::max(),
		maxpscore = 0,
		pdts = std::numeric_limits< int >::max(),			//
		minpdts = std::numeric_limits< int >::max(), 		//
		maxpdts = 0,		 	//

		ci = 0,					// number of common intervals of the input genomes
		mci = 0,				// number of common intervals of the input + median
		minmci = std::numeric_limits< int >::max(),
		maxmci = 0,

		circ = std::numeric_limits< int >::max(), 		// circumference (reversal distance)
		spd = std::numeric_limits< int >::max(),			// sum of pairwise differences (reversal distance)
		pcirc = std::numeric_limits< int >::max(), 		// circumference (perfect reversal distance)
		pspd = std::numeric_limits< int >::max();			// sum of pairwise differences (perfect reversal distance)
	vector<genom> genomes,	// the genomes
		medians;			// and their medians
	vector<string> names,	// the names of the genomes
		mnames;				// and of their medians
	string gfname,	// filename of the genomfile
		mfname;		// filename of the medianfile
	char circular = 1;
//	hdata hd;


	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'c': {common = 1; break; }
			case 'l': {circular=0; break; }
			case 'f': {i++;gfname = argv[i];break;}
			case 'm': {i++;mfname = argv[i];break;}
			default:{			// unknown parameter
				cerr << "unknown option: "<<argv[i]<<endl;
				usage();
				break;
			}
		}
	}
	if (gfname == "" || mfname == ""){
		cerr << "error: filenames of median- and genom file must be given"<<endl;
		usage();
	}

//	read_taxa(gfname, genomes, names, circular);
	vector<string> nmap;
	read_genomes(gfname, genomes, names, circular, nmap);
	if(genomes.size() == 0){
		cerr << "error: no genomes found in the input file "<<gfname << endl;
		usage();
	}
	read_genomes(mfname, medians, mnames, circular, nmap);
	if(medians.size() == 0){
		cerr << "error: no genomes found in the input file "<<mfname << endl;
		usage();
	}

	imp = new rmp( genomes[0].size(), circular );
	pimp = new rmp_common( genomes[0].size(), circular, TCIP, 0);

	rdist = new dstnc_inv( genomes[0].size(), circular );
	prdist = new dstnc_pinv( genomes[0].size() );
	//	init_data(hd, genomes[0].size(), circular);

	id = genom(genomes[0].size(), circular);

	if(common){
		lb = pimp->lower_bound(genomes);
	}else{
		lb = imp->lower_bound(genomes);
	}

	circ = imp->circumference(genomes);
	spd = imp->spd(genomes);
	pcirc = pimp->circumference(genomes);
	pspd = pimp->spd(genomes);

	ci = common_intervals_diff(genomes, genomes[0], genomes[0].size(), circular, 0, 0);

//	analyse_medians(medians, genomes, 0, diam, cmpcnt, mincmpdist, maxcmpdist, hd);
//	analyse_medians(medians, genomes, 1, pdiam, pcmpcnt, pmincmpdist, pmaxcmpdist, hd);

//	TODO: perfect reversal distance
	for(unsigned i=0; i<medians.size(); i++){
		dts = rdist->calc( medians[i], id );
		avgdts += dts;
		mindts = min( dts, mindts );
		maxdts = max( dts, maxdts );

		pdts = prdist->calc(medians[i], id, genomes);
		avgpdts += pdts;
		minpdts = min( pdts, minpdts );
		maxpdts = max( pdts, maxpdts );

		score = rdist->calcsum( medians[i], genomes );
		avgscore += score;
		minscore = min(score, minscore);
		maxscore = max(score, maxscore);

		pscore = prdist->calcsum(medians[i], genomes);
		avgpscore += pscore;
		minpscore = min(pscore, minpscore);
		maxpscore = max(pscore, maxpscore);

		mci = common_intervals_diff(genomes, medians[i], genomes[0].size(), circular, 0, 1);
		avgmci += mci;
		minmci = min(mci, minmci);
		maxmci = max(mci, maxmci);

//		        1        2           3               4           5
		cout <<"MED " << circ<<" "<< pcirc  << " "<< spd<< " "<< pspd<<" "
//		        6          7            8              9
		     << dts<<" "<< pdts<<" " << score << " "<< pscore <<" "
//              10            11
		     << mci << " " << ci << " "
//              12
		     << lb << endl;
	}

	avgdts /= medians.size();
	avgpdts /= medians.size();
	avgscore /= medians.size();
	avgpscore /= medians.size();
	avgmci /= medians.size();

//          1       2            3              4           5			circumference and spd
	cout <<"SET "<< circ <<" "<< pcirc << " "<< spd <<" "<< pspd<< " "
//	        6             7              8              9				distance to solution score
		 << mindts<<" "<< avgdts <<" "<< maxdts<< " "<< score <<" "
//		    10             11              12              13			preserving dts
		 << minpdts<<" "<< avgpdts <<" "<< maxpdts<< " "<< pscore <<" "
//		    14             15              16             17			delta ci & ci
		 << minmci <<" "<< avgmci << " "<< maxmci << " "<<ci<<" "
//          18															medians
	     << medians.size()<<" "
//          19             20             21                 22			median properties
		 << diam << " " << cmpcnt <<" "<< mincmpdist <<" " <<maxcmpdist<<" "
//		    23              24              25                  26
		 << pdiam << " " << pcmpcnt <<" "<< pmincmpdist <<" " <<pmaxcmpdist<<endl;

	delete rdist;
	delete prdist;
}
//                        1                      2
// 1   2   3 4 5 6 7 8 9  0 1 2 3  4 5 6 7   8 9 0 1          2 3 4 5          6
// SET 20 20 0 0 0 0 0 15 0 0 0 15 0 0 0 293 1 0 1 2147483647 0 0 1 2147483647 0


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


void analyse_medians( const vector<genom> &medians, const vector<genom> &genomes, int common, int &diam, int &cmpcnt, int &mincmpdist, int &maxcmpdist){

//	cerr << "reimplement analyse_medians in median.cpp !!!"<<endl;
//	exit(EXIT_FAILURE);
	int s = 0, l = 0;			// temporary vars;
	vector<int> comp;			// the connected components
	vector<vector<int> > cmpdist, // shortest distances between the components
		mdm;	// distance matrix of the medians

	dstnc_inv *rd = new dstnc_inv( genomes[0].size(), false );
	dstnc_pinv *prd = new dstnc_pinv( genomes[0].size() );
		// initialise the returns
	diam = 0;
	cmpcnt = 0;
	mincmpdist = std::numeric_limits< int >::max();
	maxcmpdist = 0;

		// get the distance matrix
	mdm = vector<vector<int> >(medians.size(), vector<int>(medians.size(), 0));
	for(unsigned i=0; i<medians.size(); i++){
		for(unsigned j=i+1; j<medians.size(); j++){
			if (common == 0){
				mdm[i][j] = mdm[j][i] = rd->calc(medians[i], medians[j]);
			}else{
				mdm[i][j] = mdm[j][i] = prd->calc(medians[i], medians[j], genomes );
			}

		}
	}

		// first get the connected componenents
	comp = vector<int>(medians.size(), std::numeric_limits< int >::max());
	for(unsigned i=0; i<medians.size(); i++){
		for(unsigned j=i+1; j<medians.size(); j++){
			if(  mdm[i][j] == 1){
				if( comp[i] == std::numeric_limits< int >::max() && comp[j] == std::numeric_limits< int >::max() ){
					comp[i] = comp[j] = cmpcnt;
					cmpcnt++;
				}else if( comp[i] == std::numeric_limits< int >::max() && comp[j] != std::numeric_limits< int >::max() ){
					comp[i] = comp[j];
				}else if( comp[i] != std::numeric_limits< int >::max() && comp[j] == std::numeric_limits< int >::max() ){
					comp[j] = comp[i];
				}else if( comp[i] != std::numeric_limits< int >::max() && comp[j] != std::numeric_limits< int >::max() && comp[i] != comp[j] ){

					if (comp[i] < comp[j]){
						s = comp[i];
						l = comp[j];
					}else{
						s = comp[j];
						l = comp[i];
					}

					for( unsigned k=0; k<comp.size(); k++ ){
						if( comp[k] == l){
							comp[k] = s;
						}else if( comp[k] != std::numeric_limits< int >::max() && comp[k] > l ){
							comp[k]--;
						}
					}
					cmpcnt--;
				}
#ifdef DEBUG_ANAMED
				cout << "cmpcnt: "<<cmpcnt << endl;
				for(unsigned j=0; j<comp.size(); j++){
					if(comp[j] == std::numeric_limits< int >::max())
						cout << "- ";
					else
						cout << comp[j]<<" ";
				}
				cout << endl;
#endif//DEBUG_ANAMED
			}
		}
	}

	for(unsigned i=0; i<comp.size(); i++){
		if(comp[i] == std::numeric_limits< int >::max()){
			comp[i] = cmpcnt;
			cmpcnt++;
		}
	}

#ifdef DEBUG_ANAMED
	cout << "cmpcnt: "<<cmpcnt << endl;
	cout << "comp  : ";copy(comp.begin(), comp.end(), ostream_iterator<int>(cout, " ")); cout << endl;
#endif//DEBUG_ANAMED

	diam = 0;
	cmpdist = vector<vector<int> >(cmpcnt, vector<int>(cmpcnt, std::numeric_limits< int >::max()) );
	for(unsigned i=0; i<medians.size(); i++){
		for(unsigned j=i+1; j<medians.size(); j++){

			if(mdm[i][j] > diam){
				diam = mdm[i][j];
			}
			if( mdm[i][j] < cmpdist[ comp[i] ][ comp[j] ] ){
				cmpdist[ comp[i] ][ comp[j] ] = cmpdist[ comp[j] ][ comp[i] ]= mdm[i][j];
			}
		}
	}

#ifdef DEBUG_ANAMED
	for(unsigned k=0; k<cmpdist.size(); k++){
		for(unsigned l=0; l<cmpdist[k].size(); l++){
			cout << cmpdist[k][l] << " ";
		}
		cout << endl;
	}
#endif//DEBUG_ANAMED
	for(unsigned k=0; k<cmpdist.size(); k++){
		for(unsigned l=k+1; l<cmpdist[k].size(); l++){

			if(mincmpdist > cmpdist[k][l])
				mincmpdist = cmpdist[k][l];
			if(maxcmpdist < cmpdist[k][l])
				maxcmpdist = cmpdist[k][l];
		}
	}
	if (cmpcnt <= 1){
		mincmpdist = maxcmpdist = 0;
	}
	delete rd;
	return;
}


void usage(void){
	cerr << "usage: anamed -f gfile -m mfile [-c] [-l]"<<endl;
	cerr << "  gfile: a file containing genomes"<<endl;
	cerr << "  mfile: a file containing the medians of the genomes"<<endl;
	cerr << "  -c: use perfect reversal distance"<<endl;
	cerr << "  -l: specifies to handle the genomes as linear"<<endl;
	cerr << "it outputs the following: "<<endl;
	cerr << "- for each median a line starting with 'MED' containing "<<endl;
	cerr << "  the circumference, sum of pairwise differences, " << endl;
	cerr << "  distance to solution, score, lower bound" << endl;
	cerr << "- for the dataset a line starting with 'SET' containing the " << endl;
	cerr << "  circumference, sum of pairwise differences, number of medians, " << endl;
	cerr << "  diameter, number of components, minimal and maximal distance, " << endl;
	cerr << "  between two components, min avg and max distance to solution, "<<endl;
	cerr << "  score, and lower bound"<<endl;
	exit(1);
}
