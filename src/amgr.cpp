#include <getopt.h>
#include <iostream>

#include "amgr.hpp"
#include "bintree.hpp"
#include "distmat.hpp"
#include "caprara.hpp"
#include "rtmedian.hpp"
#include "io.hpp"

using namespace std;

void getoptions( int argc, char *argv[],
		int &allmed, int &all_medians_deepest, int &bounding, int &cache, int &circular, string &fname,
		int &greedy, double &greedyness, string &solverstr, int &verbose,
		vector<int> &pick, vector<int> &pick_level);

/**
 * usage : print usage information
 */
void usage();

/**
 *
 */
int main(int argc, char *argv[]){
//	hdata hd;
	double greedyness = 1.0;
	int cached = 1,			// use a cache for median solver
		circular = 0,		// assume circular genomes ?
		all_medians = 0,	// use all medians from capraras algorithm
		all_medians_deepest = 1, // compute all medians on the last level
		greedy = 0,			// greedy branch and bound (only take the best)
		all_triples = 0,	// use all equally good triples and pairs
		random = 0,			// select random smallest pair / triple
		tree_score = 0, 	// here the score of the partial trees is stored
		best_tree_score = INT_MAX, // the score of the best found tree
		solution_cnt = 0,	// number of found trees
		lb = 0,				// lower bound of the search (abort if a tree with score
							// 	<= lower_bound is found (for testing))
		bounding = 0,		// apply bound ? with best treescore 1 , per level 2
		depth = 0,			// recursion depth
		m = 0,				// number of input genomes
		n = 0,				// ... and their length
		verbose = 0;
	map<vector<pair<unsigned, unsigned> >, vector< vector<genom> >  > solutions;
	mediansolver *solver = NULL;
	string solverstr,
		filename;	// input file name

	dstnc *dst = NULL;

	vector<int>	pick,		// selection method;
		picklevel;			// selection level
	int combine = COMBINE_UNION; 			//

	vector<bool> included_genomes; // genomes already included in the tree
	vector<genom> genomes;	// the input genomes
	vector<string> names, 	// the names of the genomes
		nmap;				// names of the genes
	vector<pair<unsigned, unsigned> > edges;	// edges of the reconstructed tree
	vector<pair<unsigned, unsigned> > progress;
	vector<unsigned > choices;
	vector<vector<unsigned> > dist_mat;	// distance matrix
	vector<int> partial_bound;		// best score of partial trees for each depth

	getoptions( argc, argv, all_medians, all_medians_deepest, bounding,
			cached, circular, filename, greedy, greedyness, solverstr,
			verbose, pick, picklevel );

//		// get and check parameters
//	for (int i = 1; i < argc; i++) {
//		switch (argv[i][1]) {
//			case 'a': {all_medians = 1; break;}
//			case 'b': {bounding = 1; break;}
//			case 'B': {bounding = 2; break;}
//			case 'c': {circular = 1; break;}
//			case 'd': {all_medians_deepest = 0; break;}
//			case 'f': {i++; filename = argv[i]; break;}
//			case 'g': {greedy = 1; i++; greedyness_percent = atoi( argv[i] ); break;}
//			case 'i': {combine = COMBINE_INTERSECTION; break;}
//			case 'l': {i++; lb = atoi(argv[i]); break;}
///*			case 'n': {conserved = 1; break;}
//			case 'm': {common = 1; pair_pd = 1; break;}
//			case 'M': {common = 1; pair_pd = 0; break;}
//			case 'o': {use_tcip = 1; common = 1; pair_pd = 0; break;}*/
//
//			case 'p': {i++; pick.push_back(atoi( argv[i] )); i++; picklevel.push_back(atoi(argv[i])); break;}
//			case 'r': {random = 1;break;}
////			case 's': {sign = 1; break;}
//			case 't': {all_triples = 1; break;}
//			case 'v': {verbose = 1; break;}
//			default:{
//				cout << "unknown parameter "<<argv[i]<<endl;
//				usage();
//			}
//		}
//	}

		// read the input
//	read_taxa(filename, genomes, names, circular);	// read the genom file
	read_genomes(filename, genomes, names, circular, nmap);

	m = genomes.size();
	if( m < 3 ){
		cerr << "error: less than three genomes in the input" << endl;
		usage();
	}
	n = genomes[0].size();
	if( n < 2 ){
		cerr << "error: genomes have length less than 2"<<endl;
		usage();
	}

	init_rng();
//	init_data(hd, n, circular);

	if(solverstr == "rmp"){
		solver = new rmp( n, circular );
	}else if( solverstr == "rtmedian" ){
		solver = new rtmedian( n, argc, argv );
	}else{
		cerr << "error: unknown median solver "<<solverstr<<endl;
		usage();
	}

	if( cached ){
		solver = new mediancache(solver);
	}

	dst = solver->get_dstncfoo();

	if( verbose ){
		cerr << "mediansolver :"<< *solver <<" using "; if( all_medians ){cerr<<"all medians"; if(all_medians_deepest){cerr<<" (also on the deepest level)";} cerr << endl;}else{cerr <<"only one median"<<endl;}
		cerr << "bounding     :"; if(bounding ==0){cerr << "no bounding";}else if(bounding == 1){cerr << "best known tree score";}else if(bounding ==2){cerr<<"level bounding";}else{cerr <<"unknown method";} cerr <<endl;
		if(greedy){
			cerr << "greedy: "<<(int)(100*greedyness)-100<<"%"<<endl;
		}else{
			cerr << "not greedy"<<endl;
		}
		if(pick.size()>0)
			cerr << "median filter: "<<endl;
		for(unsigned i=0; i<pick.size(); i++){
			cerr << "     "<<pick[i] << " - "<<picklevel[i]<<endl;
		}
		cerr << "read genomes from "<<filename<<endl;
		cerr << "     found "<<m<<" genomes of length "<<n<<endl;
		cerr << "     treat them as "; if(circular){cerr << "circular";}else{cerr << "linear";}cerr << endl;

	}


	included_genomes = vector<bool>(2*m, false);		// 2n-2 genomes in the complete tree
	dist_mat = vector<vector<unsigned> >(2*m-1, vector<unsigned>(2*m-1, 0) );
	distance_matrix(genomes, solver->get_dstncfoo(), dist_mat);
	progress = vector<pair<unsigned, unsigned> >( m - 2 );
	choices = vector<unsigned>(m-2, 0);
	partial_bound = vector<int>(m, INT_MAX);


		// start ...
	amgr(genomes, m, names,
		solver,
		all_triples, all_medians, all_medians_deepest,
		pick, picklevel, combine, random, greedy, greedyness, dist_mat, edges,
		included_genomes,
//		median_cache, median_cache_score, median_cache_medians, median_cache_allmeds,
		tree_score, best_tree_score, lb,
		bounding, partial_bound, depth, progress, choices, solutions,
		solution_cnt, verbose);

	//~ cout << "choices ";
	//~ for (unsigned i=0; i<progress.size(); i++){
		//~ cout << progress[i].second<<" ";
	//~ }
	//~ cout << endl;

	delete solver;

//	cout << "FIN "<<solutions.size()<<" solutions"<<endl;

	for(map<vector<pair<unsigned, unsigned> >, vector< vector<genom> >  >::iterator it=solutions.begin(); it!= solutions.end(); it++){
		edges = it->first;
		//~ for(unsigned i=0; i< edges.size(); i++)
			//~ cout << edges[i].first <<"," <<edges[i].second<<" ";
		//~ cout <<endl;

		bintree *bt = init(edges, (it->second)[0], names, dst);
		cout <<best_tree_score <<" ";
		cout << newick(bt,1,1,1)<<endl;

//		cout << " lower bound "<<lower_bound(bt, hd)<<endl;

		//~ vector<vector<genom> > trips;
		//~ vector<vector<string> > trips_names;
		//~ vector<genom> meds;
		//~ vector<string> meds_names;

		//~ triples(bt, trips, trips_names, meds, meds_names);
		//~ cout << "skew ";
		//~ for(unsigned i=0; i<trips.size(); i++){
			//~ int s = INT_MAX, l = 0;
			//~ for(unsigned j=0; j<trips[i].size(); j++){
				//~ int d = meds[i].distance(trips[i][j], hd);
				//~ if(d<s) s = d;
				//~ if(d>l) l = d;
			//~ }
			//~ cout << l - s<<" ";
		//~ }
		//~ cout << endl;
		free_tree(bt);

		//~ for(unsigned i=0; i<(it->second).size(); i++){
			//~ cout << "configurations "<< i <<endl;
			//~ for(unsigned j=0; j<(it->second)[i].size(); j++){
				//~ if(j<names.size())
					//~ cout << ">"<<names[j]<<endl;
				//~ else
					//~ cout << ">"<< j <<endl;

				//~ cout << (it->second)[i][j] <<endl;
			//~ }
		//~ }
		//~ cout << "--------"<<endl;
	}

//	free_data(hd);

	solutions.clear();

	return 1;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void getoptions( int argc, char *argv[],
		int &allmed, int &all_medians_deepest, int &bounding, int &cache, int &circular, string &fname,
		int &greedy, double &greedyness, string &solverstr, int &verbose, vector<int> &pick, vector<int> &picklevel ){
	int c;
	// get and check parameters
//for (int i = 1; i < argc; i++) {
//	switch (argv[i][1]) {
//		case 'd': {all_medians_deepest = 0; break;}

//		case 'g': {greedy = 1; i++; greedyness_percent = atoi( argv[i] ); break;}
//		case 'i': {combine = COMBINE_INTERSECTION; break;}
//		case 'l': {i++; lb = atoi(argv[i]); break;}
//
//		case 'p': {i++; pick.push_back(atoi( argv[i] )); i++; picklevel.push_back(atoi(argv[i])); break;}
//		case 'r': {random = 1;break;}
//		case 't': {all_triples = 1; break;}
//		case 'v': {verbose = 1; break;}
//		default:{
//			cout << "unknown parameter "<<argv[i]<<endl;
//			usage();
//		}
//	}
//}

	allmed = 1;
	all_medians_deepest = 0;
	bounding = 2;
	cache = 1;
	circular = 1;
	greedy = 1;
	greedyness = 1.0;
	solverstr = "rmp";

	while (1) {
		static struct option long_options[] = {
			{"deepmed", no_argument,       0, 'D'},
            {"help",    no_argument,       0, 'h'},
            {"notgreedy",  no_argument,    0, 'G'},
            {"nocache", no_argument,       &cache, 0},
            {"verbose", optional_argument, 0, 'v'},

            {"nearest", optional_argument, 0, 'N'},
            {"longest", optional_argument, 0, 'L'},
            {"shortest", optional_argument, 0, 'S'},
            {"skewest", optional_argument, 0, 'T'},
            {"random", optional_argument, 0, 'R'},
            {0, 0, 0, 0}
		};

		int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "b:f:g:hlos:v",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'b':
				bounding = atoi(optarg);
				break;
			case 'D':
				all_medians_deepest = 1;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'g':
				greedyness = 1.0 + ((double)atoi( optarg )/100.0);
				break;
			case 'G':
				greedy = 0;
				break;
			case 'h':
				usage();
				break;
			case 'L':
				pick.push_back( SELECT_MAX_LONGESTEDGE );
				if(optarg){picklevel.push_back(atoi(optarg));}
				else{picklevel.push_back(0);}
				break;
			case 'N':
				pick.push_back( SELECT_NEAREST );
				if(optarg){picklevel.push_back(atoi(optarg));}
				else{picklevel.push_back(0);}
				break;
			case 'o':
				allmed = 0;
				break;
			case 'l':
				circular = 0;
				break;
			case 'R':
				pick.push_back( SELECT_RANDOM );
				cerr << optarg << endl;
				if(optarg){picklevel.push_back(atoi(optarg));}
				else{picklevel.push_back(0);}
				break;
			case 's':
				solverstr = optarg;
				break;
			case 'S':
				pick.push_back( SELECT_MIN_SHORTESTEDGE );
				if(optarg){picklevel.push_back(atoi(optarg));}
				else{picklevel.push_back(0);}
				break;
			case 'T':
				pick.push_back( SELECT_MIN_SKEW );
				if(optarg){picklevel.push_back(atoi(optarg));}
				else{picklevel.push_back(1);}
				break;
			case 'v':
				if(optarg)
					verbose += atoi(optarg);
				else
					verbose = 1;
				break;
			case '?':
				cout << "?"<<endl;
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	printf ("non-option ARGV-elements: ");
    	while (optind < argc)
    		printf ("%s ", argv[optind++]);
    	putchar ('\n');
	}

    if( bounding < 0 || bounding > 2 ){
    	cerr << "error: unknown bounding mode"<<endl;
    	usage();
    }

    if( fname == "" ){
    	cerr << "error: a filename must be specified"<<endl;
    	usage();
    }
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cout << "amgr -f filename -s solver [OPTIONS]" << endl;

	cout << "data options"<<endl;
	cout << "  -f FNAME : read data from file FNAME"<<endl;
	cout << "  -l : handle genomes as linear (default: circular)"<<endl;

	cout << "median computation options"<<endl;
	cout << "  -s : median solver to use"<<endl;
	cout << "       rtmedian: reversal+transposition median from M.Bader"<<endl;
	cout << "  -o : use only one median from the median solver (default: all)"<<endl;
	cout << "  --nochache : don't use median cache"<<endl;
	cout << "  --deepmed: compute all medians on the deepest recursion (default: false)"<<endl;

	cout << "median filter"<<endl;
	cout << "  --nearest [PER] : pick medians near unconnected nodes"<<endl;
	cout << "  --longest [PER] : pick medians with longest longest edge"<<endl;
	cout << "  --shortest [PER]: pick medians with shortest shortest edge"<<endl;
	cout << "  --skewest [PER] : pick medians with maximal skew"<<endl;
	cout << "    allow PER% deviation from the best value (default: 0)"<<endl;
	cout << "  --random [NMB]  :"<<endl;
	cout << "    select NMB random medians (default: 1)"<<endl;

//	cout << "-p mode level "<<endl;
//	cout << "   mode "<<SELECT_NEAREST <<": pick medians near unconnected nodes (*)"<<endl;
//	cout << "   mode "<<SELECT_CENTRAL <<": pick medians central in the median set (+)"<<endl;
//	cout << "   mode "<<SELECT_MIN_SKEW <<": pick medians with minimal skew (*)"<<endl;
//	cout << "   mode "<<SELECT_MIN_LONGESTEDGE <<": pick medians with shortest longest edge (*)"<<endl;
//	cout << "   mode "<<SELECT_MIN_SHORTESTEDGE<<": pick medians with shortest shortest edge (*)"<<endl;
//	cout << "   * : selection is made over all medians from all triples"<<endl;
//	cout << "   + : selection is made over all medians from one triple"<<endl;
//	cout << "   level is percentage of difference allowed from optimum "<<endl;
//	cout << "   mode "<<SELECT_RANDOM <<": select random medians"<<endl;
//	cout << "   mode "<<SELECT_RANDOM_PC <<": select random medians per component"<<endl;
//	cout << "   mode "<<SELECT_LOOKFORWARD<< ": do a one step look forward"<<endl;
//	cout << "   level is the number of max. selected medians"<<endl;

	cout << "triple selection options"<<endl;
	cout << "  -g GRD : allow triples with score <= bestsol + GRD% (default: 0)"<<endl;
	cout << "  --notgreedy: not greedy mode (for testing only)"<<endl;

	cout << "tree search options"<<endl;
	cout << "  -b : select bounding mode (default: 2)"<<endl;
	cout << "     0: no bounding"<<endl;
	cout << "     1: bound with best known tree score"<<endl;
	cout << "     2: hard bounding, i.e. bound on each level"<<endl;

	cout << "other options"<<endl;
	cout << "  -h --help   : print this message"<<endl;
	cout << "  -v --verbose: print progress information"<<endl;

//	cout << "-p mode level "<<endl;
//	cout << "   mode "<<SELECT_NEAREST <<": pick medians near unconnected nodes (*)"<<endl;
//	cout << "   mode "<<SELECT_CENTRAL <<": pick medians central in the median set (+)"<<endl;
//	cout << "   mode "<<SELECT_MIN_SKEW <<": pick medians with minimal skew (*)"<<endl;
//	cout << "   mode "<<SELECT_MIN_LONGESTEDGE <<": pick medians with shortest longest edge (*)"<<endl;
//	cout << "   mode "<<SELECT_MIN_SHORTESTEDGE<<": pick medians with shortest shortest edge (*)"<<endl;
//	cout << "   * : selection is made over all medians from all triples"<<endl;
//	cout << "   + : selection is made over all medians from one triple"<<endl;
//	cout << "   level is percentage of difference allowed from optimum "<<endl;
//	cout << "   mode "<<SELECT_RANDOM <<": select random medians"<<endl;
//	cout << "   mode "<<SELECT_RANDOM_PC <<": select random medians per component"<<endl;
//	cout << "   mode "<<SELECT_LOOKFORWARD<< ": do a one step look forward"<<endl;
//	cout << "   level is the number of max. selected medians"<<endl;
//	cout << "-i set filter combination mode to intersection"<<endl;
//	cout << "-r : select pair / triple randomly"<<endl;
//	cout << "-t : try all equally good triples"<<endl;
//	cout << "-l lb : stop if a tree of score <= lb is found (default 0)"<<endl;
//	cout << "-v : print progress information"<<endl;
	exit(0);
}
