/*
 * deldup.cpp
 *
 * treat deleted and duplicated genes in gene orders
 *
 *  Created on: Dec 2, 2009
 *      Author: maze
 */
#include "io.hpp"
#include "dstnc_bp.hpp"
#include "dstnc_com.hpp"
#include "dstnc_inv.hpp"

#include <algorithm>
#include <getopt.h>
#include <iostream>
#include <iterator>
#include <limits>
#include <string>
#include <sstream>


//#define DEBUG
#define DEFAULT_INIT "maxspecies" // default initialisation strategy
#define DEFAULT_SEL "min" // default selection strategy
#define DEFAULT_DST "com" // default selection strategy

using namespace std;

/**
 * equalise gene content of g with respect to the genomes in refgenomes which
 * have the gene set ref
 * @param[in] g the gene order to equalize
 * @param[in] refgenomes the reference genomes
 * @param[in] ref the reference gene set (sorted array)
 * @param[in] select strategy for selection of equalised genomes
 * @param[in] dst a distance funtion
 * @return a genome with reference gene set
 */
genom equalize( genom &g, const vector<genom> &refgenomes, const vector<unsigned> &ref, const string &select, dstnc *dst );

/**
 * get command line options
 * @param[out] fname file name
 * @param[out] init initialisation strategy
 * @param[out] select selection strategy
 * @param[out] distance function
 */
void getoptions( int argc, char *argv[], string &fname, string &init,
		string &select, string &dist);

/**
 * get a largest gene set of the genomes which do not have duplicates
 * @param[in] genomes the considered genomes
 * @param[in] init initialisation method
 * @param[out] base the base gene set (sorted array)
 * @param[out] besegenomes the genomes which have the base gene set
 */
void init_gset( const vector<genom> &genomes,  string init, vector<unsigned> &base,
	vector<unsigned> &basegenomes);

/**
 * print usage info and quit
 */
void usage(void);

/**
 * here the work is done :-)
 */
int main(int argc, char *argv[]) {

	int circular = 0;
	string fname,				// filename
		distdesc = DEFAULT_DST,	// distance foo description used for selection
		init = DEFAULT_INIT,	// initialisation strategy
		select = DEFAULT_SEL; // default selection strategy
	vector<genom> genomes;
	vector<string> names,
		nmap;
	dstnc *dst = NULL;			// distance foo used for selection

	vector<unsigned> ref,			// reference gene set
		 refidx;	// indices of the given gene orders which have
					// the reference gene set
	vector<genom> refgenomes,
		nrefgenomes;

	getoptions(argc, argv, fname, init, select, distdesc);
	if( fname == "" ){
		cerr << "no file name specified"<<endl;
		usage();
	}

	// @todo default normto = 1 does not work if 1 not included in all
	read_genomes(fname,  genomes, names, circular, nmap, true, true );

	if( distdesc == "rev" ){
		dst = new dstnc_inv( genomes[0].size(), circular );
	}else if( distdesc == "com" ){
		dst = new dstnc_com( genomes[0].size(), circular );
	}else if( distdesc == "bp" ){
		dst = new dstnc_bp( genomes[0].size(), circular, !circular );
	}

	for( unsigned i=0; i<genomes.size(); i++ ){
		for( unsigned j=1; j<genomes[i].size(); ){
			if( genomes[i][j-1] == genomes[i][j] ){
				cerr << names[i]<<" delete tandem ";
				print_element( genomes[i][j-1], cerr, 0, "", &nmap );cerr <<endl;
				genomes[i].chromosom.erase(genomes[i].chromosom.begin()+j);
			}else{
				j++;
			}
		}
		if( circular != 0 && genomes[i][0] == genomes[i][genomes[i].size()-1] ){
			cerr << names[i]<< " delete tandem ";
			print_element( genomes[i][0], cerr, 0, "", &nmap );cerr <<endl;
			genomes[i].chromosom.erase(genomes[i].chromosom.begin()+genomes[i].size()-1);
		}
	}

	// determine the reference gene set
	init_gset(genomes, init, ref, refidx);

	// construct the bipartition of gene orders
	// - which have the reference gene set
	// - and those which do not
	for(unsigned i=0, j=0; i<genomes.size(); i++){
		if( j < refidx.size() && refidx[j] == i ){	// gene order is in reference genomes
			refgenomes.push_back(genomes[i]);
			j++;
		}else{					// gene order is NOT in reference genomes
			nrefgenomes.push_back(genomes[i]);
		}
//		cout << "> "<<names[i]<<endl;
//		cout << genomes[i]<<endl;
	}

//	exit(1);

//	cout << "ref genomes"<<endl;
//	cout << refgenomes<<endl;
//
//	cout << "nref genomes"<<endl;
//	cout << nrefgenomes<<endl;

	// equalise each of the gene orders with non reference gene set
	for(unsigned i=0, j=0; i<genomes.size(); i++){
		if( j < refidx.size() && refidx[j] == i ){	// gene order is in reference genomes
			j++;
			continue;
		}
		// gene order is NOT in reference genomes
		cerr << names[i]<<endl;
//		genom tmp;
		genomes[i] = equalize( genomes[i], refgenomes, ref, select, dst);

	}

	for(unsigned i=0; i<genomes.size(); i++){
		cout << "> "<<names[i]<<endl;
		cout << genomes[i]<<endl;
	}

	delete dst;
	return EXIT_SUCCESS ;
}


void check_max( genom &g, const vector<genom> &refgenomes, dstnc *dst, const string &select, unsigned &bestd, genom &bestg ){

	unsigned dd = 0;
	if( select == "min" ){
		dd =  dst->calcmin(g, refgenomes);
	}else if( select == "sum" ){
		dd =  dst->calcsum(g, refgenomes);
	}

	// check for minimum
	if( dd < bestd ){
		bestd = dd;
		bestg = g;
	}

	return;
}

// ****************************************************************************

void equalize_mis( genom &g, const vector<genom> &refgenomes, vector<unsigned> &mis, dstnc *dst, const string &select, unsigned &bestd, genom &bestg){

//	cout << "equalize missing "<<mis.size()<<endl;
	if( mis.size() == 0 ){
		bestg = g;
	}
	vector<int> rename( refgenomes[0].size()+1 );

	for( unsigned i=0; i<mis.size(); i++ ){
		cerr << "inserted  "; for( unsigned j=0; j<i; j++ ){print_element( mis[j], cerr, 0, "", refgenomes[0].get_nmap() ); cerr << " ";} cerr <<endl;
		cerr << "inserting "; print_element( mis[i], cerr, 0, "", refgenomes[0].get_nmap() );cerr <<endl;
		cerr << "     left "; for( unsigned j=i+1; j<mis.size(); j++ ){print_element( mis[j], cerr, 0, "", refgenomes[0].get_nmap() ); cerr << " ";} cerr <<endl;

		// generate a copy of the reference genomes without the genes starting at i+1
		vector<genom> rgcp = refgenomes;
		for(unsigned j=0; j<rgcp.size(); j++){
//			cerr <<" thinning " << rgcp[j] <<endl;
			for(unsigned k=0; k<rgcp[j].size(); ){
				if( find( mis.begin()+i+1, mis.end(), abs(rgcp[j][k]) ) == mis.end() ){
					k++;
				}else{
					rgcp[j].chromosom.erase(rgcp[j].chromosom.begin()+k);
				}
			}
//				cerr <<"       -> " << rgcp[j] <<endl;
		}

		// make reference genomes permutations
		fill(rename.begin(), rename.end(), 0);
		for( unsigned j=0; j<rgcp[0].size(); j++ ){
			rename[ abs(rgcp[0][j]) ] = (j+1);
		}
		for( unsigned j=0; j<rgcp.size(); j++ ){
			for(unsigned k=0; k<rgcp[i].size(); k++){
				if(rgcp[j][k]<0){
					rgcp[j][k] = -1*rename[abs(rgcp[j][k])];
				}else{
					rgcp[j][k] = rename[rgcp[j][k]];
				}
			}
		}

		genom gcp = g;
		for(unsigned j=0; j<gcp.size(); j++){
			if(gcp[j]<0){
				gcp[j] = -1*rename[abs(gcp[j])];
			}else{
				gcp[j] = rename[gcp[j]];
			}
		}

		cerr << "reference "<<endl;
		cerr << rgcp << endl;

		cerr << "target "<<gcp << endl;

		dstnc *ldst = dst->clone();
		ldst->adapt( gcp.size()+1 );

		int m = rename[ mis[i] ];

		genom lbestg;
		unsigned lbestd = std::numeric_limits< unsigned >::max();
		// insert missing gene i at all possible positions
		for( unsigned j=0; j<=g.size(); j++ ){
			gcp.chromosom.insert(gcp.chromosom.begin()+j, m);
			cerr <<"check "<<gcp<<endl;
			check_max( gcp, rgcp, ldst, select, lbestd, lbestg );
			cerr << lbestd<<endl;
			gcp.chromosom.erase(gcp.chromosom.begin()+j);


			// @todo orientierung bei unsigned distance measures?
			gcp.chromosom.insert(gcp.chromosom.begin()+j, -m);
//			cout <<"check "<<gcp<<endl;
			check_max( gcp, rgcp, ldst, select, lbestd, lbestg );
			gcp.chromosom.erase(gcp.chromosom.begin()+j);
		}

		g = lbestg;
		for( unsigned j=0; j<g.size(); j++ ){
			for( unsigned k=1; k<rename.size(); k++ ){
				if( rename[k] == abs(g[j]) ){
					if( g[j] < 0 ){
						g[j] = -k;
					}else{
						g[j] = k;
					}
					break;
				}
			}
		}
	}
	bestg = g;
//	mis.clear();
	if(mis.size()>0)
		exit(1);
}

void equalize_red( genom &g, const vector<genom> &refgenomes,
		vector<unsigned> &mis, vector<unsigned> &red, dstnc *dst, const string &select,
		unsigned &bestd, genom &bestg){

	if( red.size() == 0 ){
//		cout <<"redundant genes removed : "<<endl<< g<<endl;
		equalize_mis( g, refgenomes, mis, dst, select, bestd, bestg);
		return;
	}

	unsigned d = red.back();
	red.pop_back();

#ifdef DEBUG
	cout << "treat "<<d<<" -> delete all but one copy"<<endl;
#endif//DEBUG

	unsigned nocc = 0,
			occ = 0;
	vector<int> nchr,
		chr;

	chr = g.getChromosom();

	// count number of occurences
	for( unsigned i=0; i<g.size(); i++ )
		if( (unsigned)abs(g[i]) == d )
			nocc++;

	// try all possibilities to keep one of the occurences
	for(unsigned o=0; o<nocc; o++){
		occ=0;
		nchr.clear();
		for( unsigned i=0; i<g.size(); i++ ){
			if( (unsigned)abs(g[i]) != d or o == occ ){
				nchr.push_back(g[i]);
			}
			if( (unsigned)abs(g[i]) == d )
				occ++;
		}

		g.setChromosom(nchr);
		equalize_red( g, refgenomes, mis, red, dst, select, bestd, bestg );
//			cout << g<<endl;
		g.setChromosom(chr);
	}
	red.push_back( d );
}


// ****************************************************************************

genom equalize( genom &g, const vector<genom> &refgenomes,
		const vector<unsigned> &ref, const string &select, dstnc *dst ){

	vector<unsigned> gs;
	vector<unsigned> mis, red;	// missing and redundant genes

	// determine missing and redundant genes
	gs = g.genset();
	set_difference(ref.begin(), ref.end(), gs.begin(), gs.end(), back_inserter(mis));
	set_difference(gs.begin(), gs.end(), ref.begin(), ref.end(), back_inserter(red));

	cerr << "treating :"<<endl<<g<<endl;
	if(mis.size() > 0){
		cerr << "    missing: ";
		for(unsigned i=0; i<mis.size(); i++){
			print_element( mis[i], cerr, 0, "", refgenomes[0].get_nmap() ); cerr << " ";
		}
		cerr << endl;
	}
	if(red.size() > 0){
		cerr << "    redundant: ";
		for(unsigned i=0; i<red.size(); i++){
			print_element( red[i], cerr, 0, "", refgenomes[0].get_nmap() ); cerr << " ";
		}
		cerr << endl;
	}

	return g;

	genom bestg;
	unsigned bestd = std::numeric_limits< unsigned >::max();

	equalize_red( g, refgenomes, mis, red, dst, select, bestd, bestg );

	for(unsigned i=0; i<bestg.size(); i++){
		unsigned cnt = count(gs.begin(), gs.end(), abs(bestg[i]));
		if( cnt >= 1){
			print_element( bestg[i], cerr, 0, "", refgenomes[0].get_nmap() );
		}else{
			stringstream tmpss;
			print_element( bestg[i], tmpss, 0, "", refgenomes[0].get_nmap() );
			for(unsigned j=0; j< tmpss.str().size(); j++){
				cerr << " ";
			}
		}
		cerr << " ";
	}
	cerr << endl;

	for(unsigned i=0; i<bestg.size(); i++){
		unsigned cnt = count(gs.begin(), gs.end(), abs(bestg[i]));
		if( cnt <= 1){
			print_element( bestg[i], cerr, 0, "", refgenomes[0].get_nmap() );
		}else{
			stringstream tmpss;
			print_element( bestg[i], tmpss, 0, "", refgenomes[0].get_nmap() );
			for(unsigned j=0; j< tmpss.str().size(); j++){
				cerr << " ";
			}
		}
		cerr << " ";
	}
	cerr << endl<<endl;;

//	cout << "final "<<bestg.size() <<" : " << bestg << endl;

	return bestg;
}



// ****************************************************************************

void getoptions( int argc, char *argv[], string &fname, string &init, string &select, string &dist){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"init",   required_argument,  0, 'i'},
			{"help",   no_argument,       0, 'h'},
			{"select", required_argument,  0, 's'},
			{"distance", required_argument,  0, 'd'},
            {0, 0, 0, 0}
		};

        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "d:f:hi:s:",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'd':
				dist = optarg;
				break;
			case 'i':
				init = optarg;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case 's':
				select = optarg;
				break;
			case '?':
				usage();
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

	if( init != "maxgenes" && init != "maxspecies" && init != "user" ){
		cerr << "unknown initialisation strategy"<<endl;
		usage();
	}
	if( dist != "rev" && dist != "com" && dist != "bp" ){
		cerr << "unknown distance funtion"<<endl;
		usage();
	}
	if( select != "min" && select != "sum" ){
		cerr << "unknown selection strategy"<<endl;
		usage();
	}
}


// ****************************************************************************

void init_gset( const vector<genom> &genomes, string init, vector<unsigned> &base,
		vector<unsigned> &basegenomes){

	map<vector<unsigned>, unsigned > gsetidx;	// a mapping from the gene sets to an index in gset and gsetgenome
	vector<vector<unsigned> > gset;			// the gene sets found in the given genomes
	vector<vector<unsigned> > gsetgenome;	// mapping from the gene sets to the genomes which have it

//	map<set<unsigned>, unsigned > gsetidx; 	// for each gene set the number of
//											// times it is present in the genomes
//	vector<set<unsigned> > gset;
//	vector<unsigned> gsetcnt;

	vector<unsigned> gs;	// set of elements of a genome

	// get the gene sets from the duplication free gene orders
	for( unsigned i=0; i<genomes.size(); i++ ){
		if( genomes[i].hasduplicates() ){
			continue;
		}

		gs = genomes[i].genset();
		if( gsetidx.find( gs ) == gsetidx.end() ){
			gsetidx[gs] = gset.size();
//			gsetcnt.push_back(0);
			gset.push_back(gs);
			gsetgenome.push_back(vector<unsigned>());
		}
//		gsetcnt[ gsetidx[gs] ]++;
		gsetgenome[ gsetidx[gs] ].push_back(i);
	}

	cerr << "gene sets: "<<endl;
	for(unsigned i=0; i<gset.size(); i++){
		cerr << ""<<i<<") size "<<gset[i].size()<<endl;
		cerr << "\t";
		for(unsigned j=0; j<gset[i].size(); j++){
			print_element( gset[i][j], cerr, 0, "", genomes[0].get_nmap() );
			if( j<gset[i].size()-1)
				cerr << ",";
		}
		cerr << endl;

		cerr << "\tfound "<<gsetgenome[i].size()<<" times in: ";copy(gsetgenome[i].begin(), gsetgenome[i].end(), ostream_iterator<unsigned>(cerr,",")); cerr << endl;
	}

	unsigned best = 0;
	unsigned max = 0;
	for( unsigned i=0; i<gset.size(); i++ ){
		if( init == "maxspecies" ){	// choose gene set shared by the most genomes
			if( gsetgenome[i].size() > max ){
				max = gsetgenome[i].size();
				best = i;
			}
		}else if( init=="maxgenes" ){	// choose the largest gene set
			if( gset[i].size() > max ){
				max = gset[i].size();
				best = i;
			}
		}
	}
	if( init == "user" ){	// let the user choose
		for( unsigned i=0; i<gset.size(); i++ ){
			cout << i << ") "<< gset[i].size()<<" genes; found in "<<gsetgenome[i].size()<<" gene orders"<<endl<<"\t";
			for(unsigned j=0; j<gset[i].size(); j++){
				print_element( gset[i][j], cout, 0, "", genomes[0].get_nmap() ); cout << " ";
			}
			cout << endl;
		}
		do{
			cerr << "Your choice ["<<0<<":"<<gset.size()-1<<"]: ";
			cin >> best;
			if( best >= gset.size() )
				cerr << "invalid choice!"<<endl;
		}while( best >= gset.size() );
	}

	cerr << "chosen gene set "<<best<<endl<<endl;

	base = gset[best];
	basegenomes = gsetgenome[best];
}

// ****************************************************************************

void usage(void){
	cerr << "deldup"<<endl;
	cerr << " -f --file FNA: filename"<<endl;
	cerr << " -d --distance DST: distance funtion used for selection (default: DST="<<DEFAULT_DST<<")"<<endl;
	cerr << "            com: number of common intervals"<<endl;
	cerr << "            rev: reversal distance"<<endl;
	cerr << "            bp : breakpoint distance"<<endl;
	cerr << " -i --init INI: initialisation strategy (default: INI="<<DEFAULT_INIT<<")"<<endl;
	cerr << "            maxspecies: take the gene set common to the most duplication free gene orders"<<endl;
	cerr << "            maxgenes  : take the gene set duplication free gene order with the most genes"<<endl;
	cerr << "            user      : let the user choose between the alternatives"<<endl;
	cerr << " -s --select SEL: selection strategy (default: SEL="<<DEFAULT_SEL<<")"<<endl;
	cerr << "            min: minimum"<<endl;
	cerr << "            sum: sum"<<endl;
	exit(EXIT_FAILURE);
}
