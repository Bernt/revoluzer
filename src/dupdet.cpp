/*
 * dupdet.cpp
 *
 * detect duplicated gene clusters in gene orders
 *
 *  Created on: Mar 1, 2011
 *      Author: maze
 */

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <iterator>
#include <string>

#include "io.hpp"
#include "geneset.hpp"


#define DEFAULT_MAXSIZE 100000// default maximum set size
#define DEFAULT_ADJACENCY false	// default for the use of specialized adjacency methods
#define DEFAULT_CIRCULAR true	// default circularity:true (linear:false)
#define DEFAULT_DIRECTED false  // default for directed:false (undirected:true)
#define DEFAULT_SGN_HANDLING DOUBLE	// default sign handling option

using namespace std;

void ppmultiset( multiset<int> x, vector<string> *nmap ){
	for( multiset<int>::const_iterator it=x.begin(); it!=x.end(); it++ ){
		print_element( *it, cout, 1, "", nmap );cout <<" ";
	}
}

void ppgenom( genom g ){
	map<int, int> cnt;
	for( unsigned i=0; i<g.size(); i++ ){
		if( cnt.find( abs(g[i]) ) == cnt.end() ){
			cnt[ abs(g[i]) ]=1;
		}else{
			cnt[ abs(g[i]) ]++;
		}
	}

	for( unsigned i=0; i<g.size(); i++ ){
		print_element( g[i], cout, 1, "", g.get_nmap() );
		if( cnt[abs(g[i])]>1 ){
			cout << "*";
		}
		cout <<" ";
	}
	cout << endl;
}

/**
 * check for duplicated clusters and allow for differences in the gene content
 */
void dup_check2( genom g, bool circular ){
	multiset<int> tmpset, tmpset2,
		maxset, isct;

	for( unsigned i=0; i<g.size(); i++ ){
		tmpset.clear();

		unsigned j=i;
		while( true ){
			if( ( !circular && j>=g.size()) ||
				(  circular && j>=g.size() && i<=j%g.size())
				){
				break;
			}
			tmpset.insert(g[j%g.size()]);

//			cout << i<<" " <<j<<" -> "; copy( tmpset.begin(), tmpset.end(), ostream_iterator<int>(cout," " ) );cout << endl;
			j++;


			unsigned k=j,
					maxsize = 0,
					maxk = 0;
			maxset.clear();
			isct.clear();
			tmpset2.clear();
			while( true ){
				if( ( !circular && k>=g.size()) ||
					(  circular && k>=g.size() && i<=k%g.size())
					){
					break;
				}
				tmpset2.insert(g[k%g.size()]);

//				cout <<"\t" <<j<<" " <<k<<" -> "; copy( tmpset2.begin(), tmpset2.end(), ostream_iterator<int>(cout," " ) );cout << endl;
				set_intersection( tmpset.begin(), tmpset.end(), tmpset2.begin(), tmpset2.end(), insert_iterator<multiset<int> >(isct,isct.begin()) );

				if( tmpset2.find(g[i]) != tmpset2.end() && tmpset2.find(g[(j-1)%g.size()]) != tmpset2.end() &&
					tmpset.find(g[j%g.size()]) != tmpset.end() && tmpset.find(g[k%g.size()]) != tmpset.end() ){

//					cout << "candidate "<<endl;
					if( ((float)isct.size())/tmpset2.size() > 0.5 && isct.size() > maxsize ){
						maxsize = isct.size();
						maxset = tmpset2;
						maxk = k;
//						cout << "update"<< <<endl;
					}
				}


				isct.clear();
				k++;
			}

			if( maxsize > 0 && ((float)maxsize)/tmpset.size() > 0.5 ){
				ppmultiset( tmpset, g.get_nmap() ); cout << "\t";
				ppmultiset( maxset, g.get_nmap() ); cout << "\t"<<"["<< i <<","<<j%g.size()<<","<<maxk%g.size()<<"] sizes "<<maxsize<<" "<< tmpset.size()<< endl;
			}
		}
	}

}

void dup_check( genom g, bool circular ){
	multiset<int> tmpset,
			pcpy;

	map<multiset<int>, unsigned > copies;

	for( unsigned j=0; j<g.size(); j++ ){
		tmpset.clear();

		unsigned k=j;
		while( true ){
			// linear  : k is iterated to the end of the genome
			// circular: k is iterated  until it reaches j again in a
			// 		circular fashion
			if( k>=g.size()
//				( !circular && k>=g.size()) ||
//				(  circular && k>=g.size() && j<=k%g.size())
				){
				break;
			}

			// append the next element
			tmpset.insert(g[k%g.size()]);

//			cout << j<<" " <<k<<" -> "; copy( tmpset.begin(), tmpset.end(), ostream_iterator<int>(cout," " ) );cout << endl;
			// enumerate following sets of the same size
			unsigned l=k+1;
			unsigned ccnt = 0;
			while( true ){
				if( ( !circular && (l+tmpset.size()-1)>=g.size())||
						(  circular && (l+tmpset.size()-1)>=g.size() && j<=(l+tmpset.size()-1)%g.size()) )
						{
					break;
				}else{

				}
				pcpy.clear();
				for( unsigned m=l; m<l+tmpset.size(); m++ ){
					pcpy.insert( g[m%g.size()] ) ;
				}
//				cout << "check "; copy( tmpset.begin(), tmpset.end(), ostream_iterator<int>(cout," " ) );
//				cout << "vs "<<l<<" "<<l+tmpset.size()-1<<" -> "; copy( pcpy.begin(), pcpy.end(), ostream_iterator<int>(cout," " ) );cout << endl;

				if( tmpset == pcpy ){
//					cout << l+tmpset.size()-1<< " "<<g.size()<<" " <<j<<" "<<(l+tmpset.size()-1)%g.size()<<endl;
//					cout << "duplicate "; copy( tmpset.begin(), tmpset.end(), ostream_iterator<int>(cout," " ) );
//					cout <<" vs "; copy( pcpy.begin(), pcpy.end(), ostream_iterator<int>(cout," " ) );cout << endl;
					ccnt ++;
				}
				else{
					break;
				}
				l+=tmpset.size();
//				l++;

			}
			if( ccnt > 0  ){
				if( copies.find( tmpset )== copies.end() || (copies.find( tmpset )!= copies.end() && copies[ tmpset ] < ccnt) )
					copies[ tmpset ] = ccnt;
			}

			k++;
		}

	}

//	cout << "copies"<<endl;
	for( map<multiset<int>, unsigned >::const_iterator it=copies.begin(); it!=copies.end(); it++ ){
		for( multiset<int>::const_iterator jt =it->first.begin(); jt != it->first.end(); jt++  ){
			print_element( *jt, cout, 1, "", g.get_nmap()); cout << " ";
		}
		cout << " "<<it->second<<"x"<< endl;
	}
}

/**
 * read command line options
 * @param[in] argc number of arguments
 * @param[in] argv the arguments
 * @param[out] fname input filename
 * @param[in,out] circular the circularity of the genomes
 * @param[in,out] signhand the sing handling function
 * @param[in,out] adjacency use adjacencies
 * @param[in,out] directed the directed (vs undirected) genome model
 * @param[in,out] maxsize maximum set size
 */
void getoptions( int argc, char *argv[], string &fname, bool &circular,
		SgnHandType &signhand, bool &adjacency,
		bool &directed, unsigned &maxsize );

/**
 * print usage and exit
 */
void usage();

int main( int argc, char *argv[] ){
	bool adjacencies = DEFAULT_ADJACENCY,	// adjacency mode
			circular = DEFAULT_CIRCULAR,		// circularity of the genomes
			directed = DEFAULT_DIRECTED;
	string fname;
	SgnHandType sgnhand = DEFAULT_SGN_HANDLING; // sign handling option
	unsigned maxsize = DEFAULT_MAXSIZE; 	// maximum set size
	vector<genom> genomes, sgenomes;	// the gene orders
	vector<string> names,	// names of the species
		nmap, lnmap;	    // names of the genes



	// get command line options
	getoptions( argc, argv, fname, circular, sgnhand, adjacencies,
			directed, maxsize);

	// read the genomes from the file (never rotate)
	read_genomes(fname, genomes, names, 0, nmap, true, true);
	// make the genomes unsigned
	// - as usual -> genomes of length 2n + 2 (and construct new name mapping)
	// - or by forgetting the signs
	sgenomes = genomes;
	if( sgnhand == DOUBLE ){
		lnmap = double_nmap(nmap, directed );
		double_genomes(genomes, &lnmap, directed);
//		double_genomes(genomes, NULL, directed);
	}else if( sgnhand == UNSIGN ){
		lnmap = nmap;	// no long nmap. but the var is needed in both cases
		unsign_genomes( genomes, &nmap, directed );
	}



	for( unsigned i=0; i<genomes.size(); i++ ){
		cout << "> "<<names[i]<<endl;
//		cout << sgenomes[i]<<endl;
		ppgenom(sgenomes[i]);
		dup_check2( genomes[i], circular );
//		break;
	}



	return EXIT_SUCCESS;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void getoptions( int argc, char *argv[], string &fname, bool &circular,
		SgnHandType & signhand, bool &adjacencies,
		bool &directed, unsigned &maxsize ){
	int c,
		cb = (circular)?1:0,	// circular int
		db = (directed)?1:0;	// directed int

	while (1) {
		static struct option long_options[] = {
			{"adj",     no_argument,       0, 'a'},
			{"file",    required_argument, 0, 'f'},
			{"help",    no_argument,       0, 'h'},
			{"lindir",  no_argument,	   0, 'l'},
			{"linund",  no_argument,	   0, 'u'},
			{"sign",    required_argument, 0, 's'},
            {0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:hs:",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'a':
				adjacencies = true;
				maxsize = 2;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case 'l':
				circular = false; directed = true;
				break;
			case 'm':
				maxsize = atoi( optarg );
				break;
			case 'u':
				circular = false; directed = false;
				break;
			case 's':
				if( strcmp ( optarg, "double" ) == 0 ){
					signhand = DOUBLE;
				}else if( strcmp ( optarg, "unsign" ) == 0 ){
					signhand = UNSIGN;
				}else{
					cerr << "unknown sign handling function: "<<optarg<<endl;
					usage();
				}
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

    if( maxsize < 2 ){
    	cerr << "error: maxsize < 2"<<endl;
    	usage();
    }

	// set the adjacency bool varible
    if( adjacencies && maxsize != 2 ){
		cerr << "error: adjacency mode incompatible with maxsize "<<maxsize<<endl;
		usage();
	}

    if( circular && directed ){
    	cerr << "circular directed not implemented"<<endl;
    	exit(EXIT_FAILURE);
    }

		// check parameters
	if( fname == "" ){
		cerr << "no file given"<<endl;
		usage();
	}
}

void usage(){
	cerr << "usage not implemented"<<endl;
	exit(EXIT_FAILURE);
}
