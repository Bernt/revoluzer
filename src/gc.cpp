/** @file gc.cpp
 * 'garbage collector'; purpose: collect the results of cim and
 * mgrResultparser it adds score difference and distance of solution
 * for all k; the input should come from stdin
 */

#include <cmath>
#include <iostream>
#include <iomanip>
#include <map>
//~ #include <boost/regex.hpp>
#include <regex.h>
#include <vector>
#include <stdlib.h>

using namespace std;

void usage(){
	cout << "usage: gc -m 3|4 [-v][-h]"<<endl;
	cout << "-m multiplicator"<<endl;
	cout << "-v print additional variance infos"<<endl;
	cout << "-h print this message"<<endl;
	exit(1);

}

int main(int argc, char *argv[]){
	char in[256];
	regex_t re_result,
		re_result_ext;
	bool print_variance = false;
	regmatch_t match[11];
	int m=0, n=0, k=0, r_start=0, r_end=0,
		mult=0,
		results = 0,
		status;
	double dos=0, sd=0, 			// distance of solution and score difference
		dos_id=0, sd_id=0,		// distance of solution and score difference in interval distance
		psd=0;
	string s;						// string for temporary usage
	map<int, int> counter,
		dos_map,
		sd_map;
	map<int, int> indices;
	vector<vector<double> > dos_vec,
		sd_vec,
		dos_id_vec,
		sd_id_vec,
		psd_vec;

	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'h': {usage();}
			case 'm': {i++; mult = atoi(argv[i]); break;}
			case 'v': {print_variance = true; break;}
		}
	}

	if(mult < 3){
		usage();
	}
	status = regcomp(&re_result,"^(-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+)$",REG_EXTENDED);
	if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_result."<<endl;
		exit(-1);
	}
	status = regcomp(&re_result_ext,"^(-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+) (-{0,1}[[:digit:]]+)$",REG_EXTENDED);
	if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_result_ext."<<endl;
		exit(-1);
	}

	while(!cin.eof()){
		string ts;
		cin.getline(in, 255);
		s = in;
		if(regexec (&re_result, &s[0], 9, &match[0], 0)==0){
			results++;
			ts.assign(s.begin()+match[1].rm_so, s.begin()+match[1].rm_eo);
			m = atoi(ts.c_str());
			ts.assign(s.begin()+match[2].rm_so, s.begin()+match[2].rm_eo);
			n = atoi(ts.c_str());
			ts.assign(s.begin()+match[3].rm_so, s.begin()+match[3].rm_eo);
			k = atoi(ts.c_str());
			ts.assign(s.begin()+match[4].rm_so, s.begin()+match[4].rm_eo);
			dos = atof(ts.c_str());
			ts.assign(s.begin()+match[5].rm_so, s.begin()+match[5].rm_eo);
			sd = atof(ts.c_str());
			ts.assign(s.begin()+match[6].rm_so, s.begin()+match[6].rm_eo);
			dos_id = atof(ts.c_str());
			ts.assign(s.begin()+match[7].rm_so, s.begin()+match[7].rm_eo);
			sd_id = atof(ts.c_str());
			ts.assign(s.begin()+match[8].rm_so, s.begin()+match[8].rm_eo);
			psd = atof(ts.c_str());

			if(indices[k]==0){
				indices[k] = dos_vec.size()+1;
				dos_vec.push_back(vector<double>());
				sd_vec.push_back(vector<double>());
				dos_id_vec.push_back(vector<double>());
				sd_id_vec.push_back(vector<double>());
				psd_vec.push_back(vector<double>());
			}

			dos_vec[indices[k]-1].push_back(dos);
			sd_vec[indices[k]-1].push_back(sd);
			dos_id_vec[indices[k]-1].push_back(dos_id);
			sd_id_vec[indices[k]-1].push_back(sd_id);
			psd_vec[indices[k]-1].push_back(psd);
		}
		if(regexec (&re_result_ext, &s[0], 11, &match[0], 0)==0){
			results++;
			ts.assign(s.begin()+match[1].rm_so, s.begin()+match[1].rm_eo);
			m = atoi(ts.c_str());
			ts.assign(s.begin()+match[2].rm_so, s.begin()+match[2].rm_eo);
			n = atoi(ts.c_str());
			ts.assign(s.begin()+match[3].rm_so, s.begin()+match[3].rm_eo);
			k = atoi(ts.c_str());
			ts.assign(s.begin()+match[4].rm_so, s.begin()+match[4].rm_eo);
			r_start = atoi(ts.c_str());
			ts.assign(s.begin()+match[5].rm_so, s.begin()+match[5].rm_eo);
			r_end = atoi(ts.c_str());
			ts.assign(s.begin()+match[6].rm_so, s.begin()+match[6].rm_eo);
			dos = atof(ts.c_str());
			ts.assign(s.begin()+match[7].rm_so, s.begin()+match[7].rm_eo);
			sd = atof(ts.c_str());
			ts.assign(s.begin()+match[8].rm_so, s.begin()+match[8].rm_eo);
			dos_id = atof(ts.c_str());
			ts.assign(s.begin()+match[9].rm_so, s.begin()+match[9].rm_eo);
			sd_id = atof(ts.c_str());
			ts.assign(s.begin()+match[10].rm_so, s.begin()+match[10].rm_eo);
			psd = atof(ts.c_str());

			if(indices[k]==0){
				indices[k] = dos_vec.size()+1;
				dos_vec.push_back(vector<double>());
				sd_vec.push_back(vector<double>());
				dos_id_vec.push_back(vector<double>());
				sd_id_vec.push_back(vector<double>());
				psd_vec.push_back(vector<double>());
			}

			dos_vec[indices[k]-1].push_back(dos);
			sd_vec[indices[k]-1].push_back(sd);
			dos_id_vec[indices[k]-1].push_back(dos_id);
			sd_id_vec[indices[k]-1].push_back(sd_id);
			psd_vec[indices[k]-1].push_back(psd);

		}

	}
	cout << "# medians of "<<results << " results " << endl;
	cout << "# dos: distance of solution (to the identity)"<<endl;
	cout << "# sd : score difference (to 3*k)"<<endl;
	cout << "# psd: perfect score difference"<<endl;
	cout << "# id : distance in intervaldistance"<<endl;
	if(print_variance){
		cout << "# ratio, avg_dos, var_dos, avg_sd, var_sd, avg_dos_id, var_dos_id, avg_sd_id, var_sd_id, avg_psd, var_psd"<<endl;
	}else{
		cout << "# ratio, avg_dos, avg_sd, avg_dos_id, avg_sd_id, avg_psd"<<endl;
	}
	for(map<int, int >::iterator it=indices.begin(); it!=indices.end(); it++){
		double dos_avg = 0,		//averages
			sd_avg = 0,
			dos_id_avg  = 0,
			sd_id_avg = 0,
			psd_avg = 0,
			dos_var = 0,		// variances
			sd_var = 0,
			sd_id_var = 0,
			dos_id_var = 0,
			psd_var = 0;

		k = it->first;
		//~ cout << "k= "<<k<<endl;
		//~ cout << "elements "<< dos_vec[it->second-1].size()<<endl;
			// calc averages
		for(unsigned i=0; i<dos_vec[it->second-1].size(); i++ ){
			dos_avg += dos_vec[it->second-1][i];
			sd_avg += sd_vec[it->second-1][i];
			dos_id_avg += dos_id_vec[it->second-1][i];
			sd_id_avg += sd_id_vec[it->second-1][i];
			psd_avg += psd_vec[it->second-1][i];
		}
		dos_avg /= dos_vec[it->second-1].size();
		sd_avg  /= sd_vec[it->second-1].size();
		dos_id_avg /= dos_id_vec[it->second-1].size();
		sd_id_avg  /= sd_id_vec[it->second-1].size();
		psd_avg  /= psd_vec[it->second-1].size();
		cout <<fixed << setprecision(3)<<((double)mult*k)/n << "\t";

		if(r_end!=0){
			cout << r_start << "\t";
		}

		if(print_variance){
			for(unsigned i=0; i<dos_vec[it->second-1].size(); i++ ){
				dos_var 		+= pow(dos_vec[it->second-1][i] - dos_avg, 2);
				sd_var 		+= pow(sd_vec[it->second-1][i] - sd_avg, 2);
				dos_id_var 	+= pow(dos_id_vec[it->second-1][i] - dos_id_avg, 2);
				sd_id_var 	+= pow(sd_id_vec[it->second-1][i] - sd_id_avg, 2);
				psd_var 		+= pow(psd_vec[it->second-1][i] - psd_avg, 2);
			}

			dos_var 		= sqrt(dos_var 	/ (dos_vec[it->second-1].size()-1));
			sd_var		= sqrt(sd_var 	/ (sd_vec[it->second-1].size()-1));
			dos_id_var 	= sqrt(dos_id_var/ (dos_id_vec[it->second-1].size()-1));
			sd_id_var 	= sqrt(sd_id_var / (sd_id_vec[it->second-1].size()-1));
			psd_var 		= sqrt(psd_var / (psd_vec[it->second-1].size()-1));

			cout << dos_avg 		<<"\t"<< dos_var <<"\t"
				<< sd_avg 		<<"\t"<< sd_var <<"\t"
				<< dos_id_avg 	<<"\t"<< dos_id_var <<"\t"
				<< sd_id_avg	<<"\t"<< sd_id_var <<"\t"
				<< psd_avg 	<<"\t"<< psd_var<<endl;

		}else{
			cout	<< dos_avg 		<<"\t"
				<< sd_avg 		<<"\t"
				<< dos_id_avg 	<<"\t"
				<< sd_id_avg	<<"\t"
				<< psd_avg<<endl;
		}
	}

	return 1;
}
