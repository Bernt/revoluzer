/** @file grappaResultParser.cpp
 * program to parse the output of grappa to extract the relevant data for test:
 * m, n, k, score difference and distance of solution
 * there is a program gc which takes this output in stdin and outputs a file
 * where score-difference and distance of solution are added for every k
 */

#include <algorithm>
#include <iostream>
#include <fstream>
#include <limits>
#include <vector>
#include <regex.h>

#include "bintree.hpp"
#include "conserved.hpp"
#include "dstnc_inv.hpp"
#include "common_old.hpp"
#include "genom.hpp"
#include "helpers.hpp"

using namespace std;


/**
 * parse the results of given in the file fname and outputs the interesting properties
 *@param[in] fname the file to parse
 *@param[in] realdata if true the data is not simulated (and the filename does not include informations about the simulation parameters)
 *@return 1
 */
int parse_median_results(string fname, bool realdata);

/**
 * parse the results of given in the file fname and outputs the interesting properties
 *@param[in] fname the file to parse
 *@param[in] circ circular genomes
 *@return 1
 */
int parse_tree_results(string fname, int circ, int best);

/**
 *
*/
void pairwise_triple_check(vector<genom> &triple, vector<string> &triple_names,
	genom &med, string &med_name, int circ){

	vector<vector<genom> > pairs(3);
	vector<vector<string> > pairs_names(3);

	pairs[0].push_back(triple[0]);pairs[0].push_back(triple[1]);
	pairs[1].push_back(triple[1]);pairs[1].push_back(triple[2]);
	pairs[2].push_back(triple[2]);pairs[2].push_back(triple[0]);

	pairs_names[0].push_back(triple_names[0]);pairs_names[0].push_back(triple_names[1]);
	pairs_names[1].push_back(triple_names[1]);pairs_names[1].push_back(triple_names[2]);
	pairs_names[2].push_back(triple_names[2]);pairs_names[2].push_back(triple_names[0]);

	for(unsigned i=0; i< pairs.size(); i++){
		vector<pair<unsigned, unsigned> > ci_pre_con, ci_pre_com, ci_pre_coms,
			ci_dif_con, ci_dif_com, ci_dif_coms;
		int dcon = 0,
			dcom = 0,
			dcoms = 0;

		dcom = changes_common_intervals(pairs[i], med, ci_pre_com, ci_dif_com, 0, circ);
		dcoms = changes_common_intervals(pairs[i], med, ci_pre_coms, ci_dif_coms, 1, circ);
		dcon = changes_conserved_intervals(pairs[i], med, ci_pre_con, ci_dif_con, circ);


		if(dcom != 0 || dcoms != 0 || dcon != 0){
			cout << "pair ";
			for(unsigned j=0; j<pairs_names[i].size(); j++)
				cout << pairs_names[i][j] << " ";
			cout << " + "<<med_name[i]<<endl;

			cout << "idx"<< endl;
			for(unsigned j=0; j<med.size(); j++){
				cout.width(3);
				cout << j << " ";
			}
			cout << endl;

			for(unsigned j=0; j<pairs_names[i].size(); j++){
				cout <<">"<<pairs_names[i][j]<<endl << pairs[i][j] << endl;
			}
			cout<<">"<<med_name<<endl<< med<<endl;

			if(dcom != 0)
				cout << "pairdcom " << dcom <<" "<< output(ci_dif_com) << endl;
			if(dcoms != 0)
				cout << "pairdcoms "<< dcoms <<" "<< output(ci_dif_coms)<< endl;
			if(dcon != 0)
				cout << "pairdcon "<< dcon <<" "<< output(ci_dif_con) << endl;
		}
	}
}

void analyse_tree(bintree *bt, int circ){
	vector<genom> meds;
	vector<string> meds_names;
	vector<vector<genom> > trips;
	vector<vector<string> > trips_names;

	print_newick(bt, 1, 1);
	cout << endl;

	triples(bt, trips, trips_names, meds, meds_names);

	for(unsigned i=0; i<meds.size(); i++){
		vector<pair<unsigned, unsigned> > ci_pre_con, ci_pre_com, ci_pre_coms,
			ci_dif_con, ci_dif_com, ci_dif_coms;
		int dcon = 0,
			dcom = 0,
			dcoms = 0;

		dcom = changes_common_intervals(trips[i], meds[i], ci_pre_com, ci_dif_com, 0, circ);
		dcoms = changes_common_intervals(trips[i], meds[i], ci_pre_coms, ci_dif_coms, 1, circ);
		dcon = changes_conserved_intervals(trips[i], meds[i], ci_pre_con, ci_dif_con, circ);


		if(dcom != 0 || dcoms != 0 || dcon != 0){
			cout << "triple ";
			for(unsigned j=0; j<trips_names[i].size(); j++)
				cout << trips_names[i][j] << " ";
			cout << " + "<<meds_names[i]<<endl;

			cout << "idx"<< endl;
			for(unsigned j=0; j<meds[i].size(); j++){
				cout.width(3);
				cout << j << " ";
			}
			cout << endl;

			for(unsigned j=0; j<trips_names[i].size(); j++){
				cout <<">"<<trips_names[i][j]<<endl << trips[i][j] << endl;
			}
			cout<<">"<<meds_names[i]<<endl<< meds[i]<<endl;

			if(dcom != 0)
				cout << "dcom " << dcom <<" "<< output(ci_dif_com) << endl;
			if(dcoms != 0)
				cout << "dcoms "<< dcoms <<" "<< output(ci_dif_coms)<< endl;
			if(dcon != 0)
				cout << "dcon "<< dcon <<" "<< output(ci_dif_con) << endl;
		}

		//~ pairwise_triple_check(trips[i], trips_names[i], meds[i], meds_names[i], hd);
	}

	trips.clear(); trips_names.clear();
	meds.clear(); meds_names.clear();
	return;
}

/**
 * print usage information
 */
void usage();

int main(int argc, char *argv[]){
	string	fname;	// filename of grappa output file
	bool realdata=false,
		tree = false;
	int circ = false;
	int best = 0;

	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'b': {i++; best = atoi(argv[i]);break;}
			case 'c': {circ = 1; break;}
			case 'f': {i++; fname = argv[i]; break;}
			case 'r': {realdata = true;	break;}
			case 'T': {tree = true;	break;}
			default:{
				cout << "Error: unknown option "<<argv[i]<<endl;
				usage();

			}
		}
	}

		// check args
	if(fname.size()==0){
		cout << "Error: no fname given"<<endl;
		usage();
	}

	if(tree)
		parse_tree_results(fname, circ, best);
	else
		parse_median_results(fname, realdata);


	return 1;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int parse_median_results(string fname, bool realdata){
	string line,			// lines of the outputfile
		s;				// string for temporary usage
	int status;
	regex_t re_fname,
		re_fname_ext,
		re_genome,
		re_gene,
		re_end,
		re_dataset_begin,
		re_nrofgenomes,
		re_nrofgenes;
	ifstream grappa_output;	// grappa outpufile
	regmatch_t match[8];
	genom	g;				// genome for temporary usage
	vector<vector<genom> > G(1);	// the genomes inputed to grappa
	vector<genom> solution;
	unsigned m = 0,			// # of genomes
		n = 0,			// # of genes per genome
		e = 0, 			// the experiment number
		k = 0,			// distance
		cur_solution=0,
		r_start=0, r_end=0,
		linenr = 0;
	char circ = 0;
	bool clear_end = true;

	dstnc *rdist = NULL;

		// open file
	grappa_output.open(fname.c_str());
	if(!grappa_output.is_open()){
		cout << "unable to open "<<fname<<endl;
		exit(0);
	}

		// compile the regular expressions
		// regex for the inputfile line like <TR><TD>Input:</TD> <TD>sets/m03_n30/m3_n30_k1_0</TD></TR>
	status = regcomp(&re_fname,"^Input:.*([[:digit:]]+)_m([[:digit:]]+)_n([[:digit:]]+)_k([[:digit:]]+)$",REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_fname."<<endl;
		exit(-1);
	}
	status = regcomp(&re_fname_ext,"^Input:.*([[:digit:]]+)_m([[:digit:]]+)_n([[:digit:]]+)_k([[:digit:]]+)_w([[:digit:]]+)-([[:digit:]]+)$",REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_fname_ext."<<endl;
		exit(-1);
	}
		// regex for the line before genome genome like <b><a NAME="Genome3"></a>A3</b> <br>
	status = regcomp(&re_genome,"^Genome[[:space:]]*(-*[[:digit:]]):(.*)$",REG_ICASE|REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_ancestral_genome."<<endl;
		exit(-1);
	}
		// regex for 1 gene
	status = regcomp(&re_gene,"^([[:space:]]*-{0,1}[[:digit:]]+)",REG_ICASE|REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_gene."<<endl;
		exit(-1);
	}
	status = regcomp(&re_end,"^End time.*$",REG_ICASE|REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_end."<<endl;
		exit(-1);
	}
	status = regcomp(&re_dataset_begin,"^#",REG_ICASE|REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_dataset_begin."<<endl;
		exit(-1);
	}
	status = regcomp(&re_nrofgenomes,"Number of Genomes:[[:space:]*]([[:digit:]*])",REG_ICASE|REG_EXTENDED);
		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_nrofgenomes."<<endl;
		exit(-1);
	}
	status = regcomp(&re_nrofgenes,"Number of Genes:[[:space:]]+([[:digit:]]+)",REG_ICASE|REG_EXTENDED);		if( status != 0 ) {
		cout << "read_taxa: Could not compile regex pattern re_nrofgenes."<<endl;
		exit(-1);
	}


		// read the file linewise
	while(grappa_output){
		getline(grappa_output, line);
		if(regexec (&re_dataset_begin, &line[0], 1, &match[0], 0)==0){
			if(linenr>0 && !clear_end){
				for(unsigned i=0; i<G.size(); i++){
					G[i].clear();
				}
				G.clear();
				G.push_back(vector<genom>());
				solution.clear();
				cur_solution = 0;
				cout <<"error"<<endl;
			}
			clear_end = false;
		}
		if(regexec (&re_fname, &line[0], 5, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			e = atoi(s.c_str());
			s.assign(line.begin()+match[2].rm_so, line.begin()+match[2].rm_eo);
			m = atoi(s.c_str());
			s.assign(line.begin()+match[3].rm_so, line.begin()+match[3].rm_eo);
			n = atoi(s.c_str());
			s.assign(line.begin()+match[4].rm_so, line.begin()+match[4].rm_eo);
			k = atoi(s.c_str());
			//~ cout << "->m "<<m<<" n "<<n << " k "<<k<< " e "<<e<<endl;
		}
		if(regexec (&re_fname_ext, &line[0], 7, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			e = atoi(s.c_str());
			s.assign(line.begin()+match[2].rm_so, line.begin()+match[2].rm_eo);
			m = atoi(s.c_str());
			s.assign(line.begin()+match[3].rm_so, line.begin()+match[3].rm_eo);
			n = atoi(s.c_str());
			s.assign(line.begin()+match[4].rm_so, line.begin()+match[4].rm_eo);
			k = atoi(s.c_str());
			s.assign(line.begin()+match[5].rm_so, line.begin()+match[5].rm_eo);
			r_start = atoi(s.c_str());
			s.assign(line.begin()+match[6].rm_so, line.begin()+match[6].rm_eo);
			r_end = atoi(s.c_str());
			//~ cout << "->m "<<m<<" n "<<n << " k "<<k<< " e "<<e<<endl;
		}

			// if realdata -> get m from grappafile
		if(realdata && regexec (&re_nrofgenomes, &line[0], 2, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			m = atoi(s.c_str());
		}
			// if realdata -> get n from grappafile
		if(realdata && regexec (&re_nrofgenes, &line[0], 2, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			n = atoi(s.c_str());

			rdist = new dstnc_inv(n, circ);
		}
		if(regexec (&re_genome, &line[0], 3, &match[0], 0)==0){
			g.clear();
			s.assign(line.begin()+match[2].rm_so, line.begin()+match[2].rm_eo);	// in s is an 'A' if the genome is the ancestral
			while(regexec (&re_gene, &s[0], 1, &match[0], 0)==0){
				string gene_str;
				gene_str.assign(s.begin()+match[0].rm_so, s.begin()+match[0].rm_eo);
				s = s.substr(match[0].rm_eo - match[0].rm_so ,s.length());
				g.push_back(string2int(gene_str));
			}

			g.setCircular(circ);

			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			if(string2int(s)>0){
				G[cur_solution].push_back(g);
			}else{
				solution.push_back(g);
			}
			if(G[cur_solution].size()==m && solution.size()==cur_solution+1){
				G.push_back(vector<genom>());
				cur_solution++;
			}
		}

		//~ cout << "m "<<m<<" n "<<n << " k "<<k<< " e "<<e<<" as "<< a.size()<<" Gs "<< G.size()<<endl;
			// if all data is complete then output it and reset the data

		if(regexec (&re_end, &line[0], 1, &match[0], 0)==0){
			int sd,
				isd,
				dos,
				idos,
				min_sd = numeric_limits< int >::max(),
				min_isd = numeric_limits< int >::max(),
				min_dos = numeric_limits< int >::max(),
				min_idos = numeric_limits< int >::max(),
				ps,
				psd,
				min_psd = numeric_limits< int >::max();


			for(unsigned i=0; i<G.size();i++){
				if(G[i].size() != m || solution[i].size() == 0){
					G[i].clear();
					continue;
				}

				genom id(n, circ);

				dos = rdist->calc(solution[i],id);
				if(realdata)
					sd = rdist->calcsum(solution[i], G[i]);
				else
					sd = rdist->calcsum(solution[i],G[i]) - rdist->calcsum(id, G[i]);
				idos = intervalDistance(solution[i], id) ;
				isd = intervalDistance(solution[i], G[i]);
				ps = (int)ceil((double)(rdist->calc(G[i][0],G[i][1]) + rdist->calc(G[i][0],G[i][2]) + rdist->calc(G[i][1],G[i][2]) )/2.0);
				psd = rdist->calcsum(solution[i], G[i]) - ps;

				if(sd<min_sd){
					min_dos = dos;
					min_sd = sd;
					min_idos = idos;
					min_isd = isd;
					min_psd = psd;
				}
				if(sd==min_sd){
					if(dos<min_dos)
						min_dos = dos;
					if(idos<min_idos)
						min_idos = idos;
					if(isd<min_isd)
						min_isd = isd;
					if(psd<min_psd)
						min_psd = psd;
				}
				for(unsigned k=0; k<2; k++){
					for(unsigned j=0; j<n; j++){
						vector<int> chr = solution[i].getChromosom();
						rotate(chr.begin(), chr.end()-1,chr.end());
						solution[i].setChromosom(chr);

						dos = rdist->calc(solution[i],id) ;
						if(realdata)
							sd = rdist->calcsum(solution[i],G[i]);
						else
							sd = rdist->calcsum(solution[i],G[i]) - rdist->calcsum(id,G[i]);
						idos = intervalDistance(solution[i], id) ;
						isd = intervalDistance(solution[i], G[i]);
						ps = (int)ceil((double)(rdist->calc(G[i][0],G[i][1]) + rdist->calc(G[i][0],G[i][2]) + rdist->calc(G[i][1],G[i][2]) )/2.0);
						psd = rdist->calcsum(solution[i],G[i]) - ps;

						if(sd<min_sd){
							min_dos = dos;
							min_sd = sd;
							min_idos = idos;
							min_isd = isd;
							min_psd = psd;
						}
						if(sd==min_sd){
							if(dos<min_dos)
								min_dos = dos;
							if(idos<min_idos)
								min_idos = idos;
							if(isd<min_isd)
								min_isd = isd;
							if(psd<min_psd)
								min_psd = psd;
						}
					}
					vector<int> chr = solution[i].getChromosom();
					reverse(chr.begin(), chr.end());
					for(unsigned j=0; j< chr.size(); j++){
						chr[j] *= -1;
					}
					solution[i].setChromosom(chr);
				}
				G[i].clear();
			}
			if(realdata){
				cout << m <<" "<<n<<" "<< min_sd <<" "<< min_psd<<" "
					<<min_isd<<endl;
			}else{
				cout << m <<" "<< n <<" "<< k << " ";
				if(r_end!=0){
					cout << r_start <<" "<<r_end<<" ";
				}
				cout << min_dos <<" "<< min_sd << " "
					<< min_idos <<" "<< min_isd<< " "<<min_psd <<endl;
			}
			k = m = n = r_start = r_end = 0;

			G.clear();
			G.push_back(vector<genom>());
			solution.clear();
			cur_solution = 0;
			clear_end = true;
		}
		linenr++;
	}
	if(!clear_end){
		cout << "error"<<endl;
	}
	grappa_output.close();
	regfree(&re_fname);
	regfree(&re_genome);
	regfree(&re_gene);
	regfree(&re_end);
	if(rdist != NULL){delete rdist;}
	return 1;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int parse_tree_results(string fname, int circ, int best){
	bool start=false;
	genom g;				// temporary buffer for reading
	ifstream grappa_output;	// grappa outpufile
	int status,				// status of re matches
		n,					// nr of genes
		score,				// score of a tree
		best_score=numeric_limits< int >::max();	// smalles score found so far
	regex_t re_dataset_end,
		re_gene,
		re_genome,	// genome line
		re_end_of_nj,	// marks the real start of data
		re_newick,		// newick tree line
		re_nrofgenes;	// nr of genes
	regmatch_t match[8];
	string line,
		nwk,
		s;			//temp string to store the matches
	unsigned cnt = 0;
	vector<bintree *> besttrees;	// the best trees in the file
	vector<genom> genomes;	// genomes in preorder
	vector<string> names;	// names (the numbers in GRAPPA output) in preorder
		// open the files
	grappa_output.open(fname.c_str());
	if(!grappa_output.is_open()){
		cout << "unable to open "<<fname<<endl;
		exit(0);
	}
	dstnc *rdist = NULL;

		// compile some regexes
	status = regcomp(&re_end_of_nj,"Number of neighbor-joining trees evaluated",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "parse_tree_results: Could not compile regex pattern re_end_of_nj."<<endl;
		exit(-1);
	}

	status = regcomp(&re_newick,"^\\(",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "parse_tree_results: Could not compile regex pattern re_newick."<<endl;
		exit(-1);
	}
		// regex for the line before genome genome like <b><a NAME="Genome3"></a>A3</b> <br>
	status = regcomp(&re_genome,"^Genome[[:space:]]*(-*[[:digit:]]+):(.*)$",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "parse_tree_results: Could not compile regex pattern re_ancestral_genome."<<endl;
		exit(-1);
	}

		// regex for 1 gene
	status = regcomp(&re_gene,"^([[:space:]]*-{0,1}[[:digit:]]+)",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "parse_tree_results: Could not compile regex pattern re_gene."<<endl;
		exit(-1);
	}
	status = regcomp(&re_dataset_end,"^inversion score[=[:space:]]*([[:digit:]]+)$",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "parse_tree_results: Could not compile regex pattern re_dataset_end."<<endl;
		exit(-1);
	}
	status = regcomp(&re_nrofgenes,"Number of Genes:[[:space:]]+([[:digit:]]+)",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "parse_tree_results: Could not compile regex pattern re_nrofgenes."<<endl;
		exit(-1);
		rdist = new dstnc_inv( n, circ );
	}

	while(grappa_output){
		getline(grappa_output, line);

		if(regexec (&re_nrofgenes, &line[0], 2, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			n = atoi(s.c_str());
//			init_data(hd, n, circ);
		}

		if(regexec (&re_end_of_nj, &line[0], 1, &match[0], 0)==0){
			start = true;
			continue;
		}
		if(!start)
			continue;

		if(regexec (&re_newick, &line[0], 1, &match[0], 0)==0){
			nwk = line;
		}
		if(regexec (&re_genome, &line[0], 3, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			names.push_back(s);

			g.clear();
			s.assign(line.begin()+match[2].rm_so, line.begin()+match[2].rm_eo);	// in s is an 'A' if the genome is the ancestral
			while(regexec (&re_gene, &s[0], 1, &match[0], 0)==0){
				string gene_str;
				gene_str.assign(s.begin()+match[0].rm_so, s.begin()+match[0].rm_eo);
				s = s.substr(match[0].rm_eo - match[0].rm_so ,s.length());
				g.push_back(string2int(gene_str));
			}
			g.setCircular(circ);
			genomes.push_back(g);
		}
		if(regexec (&re_dataset_end, &line[0], 2, &match[0], 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			score = atoi(s.c_str());

			if(best != 0){
				if(best == score){
					reverse(genomes.begin(), genomes.end());
					reverse(names.begin(), names.end());

					analyse_tree(init(nwk, genomes, names, rdist), circ);
					cnt ++;
					best_score = score;
				}
			}
			else{
				if(score <= best_score){
					if(score < best_score){
						for(unsigned i=0; i<besttrees.size(); i++)
							free(besttrees[i]);
						besttrees.clear();
						best_score = score;
						cnt = 0;
					}
					reverse(genomes.begin(), genomes.end());
					reverse(names.begin(), names.end());

					cnt++;
					besttrees.push_back( init(nwk, genomes, names, rdist) );
				}
			}
			genomes.clear();
			names.clear();

		}
	}
	regfree(&re_nrofgenes);
	regfree(&re_gene);
	regfree(&re_genome);
	regfree(&re_newick);
	regfree(&re_end_of_nj);

	cout << "found "<<cnt<<" tree(s) with score "<<best_score<<endl;

	if(best == 0){
		for(unsigned i=0; i<besttrees.size(); i++){
			analyse_tree(besttrees[i], circ);
		}

		for(unsigned i=0; i<besttrees.size(); i++)
			free(besttrees[i]);
		besttrees.clear();
	}

		// close files
	grappa_output.close();

	if(rdist != NULL){delete rdist;}
	return 1;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cout << "grappaResultparser [-r] -f file" <<endl;
	cout << "the file includes output of a median solver" <<endl;
	cout << "-r : biological data" <<endl;
	cout << "grappaResultparser -T [-c] -f file -i ifile" <<endl;
	cout << "-T : parse the output tree"<<endl;
	cout << "-c : circular genomes"<<endl;
	cout << "-n : count destroyed conserved intervals"<<endl;
	cout << "-m : count destroyed common intervals"<<endl;
	cout << "-s : use sign for common intervals"<<endl;
	exit(0);
}
