/*
 * Construct Median network from given binary data
 *
 *  Created on: Nov 15, 2010
 *      Author: maze
 */

#include <algorithm>
#include <getopt.h>
#include <fstream>
#include <iostream>
#include <regex.h>
#include <stdlib.h>

#include "chrctr/ccc.hpp"
#include "chrctr/chrctr.hpp"
#include "chrctr/mednw.hpp"


using namespace std;

/**
 * extract command line parameters
 * @param[in] argc number
 * @param[in] argv parameters
 * @param[out] fname file name
 * @param[out] ext the extension of fname
 */
void getoptions( int argc, char *argv[], string &fname, string &ext );

/**
 * read boolean character information given in a file
 * @param[in] fname file name
 * @return list of characters
 */
vector<chrctr<unsigned> > readboolfile( const string &fname );

/**
 * read boolean character information given in a nexus file
 * @param[in] fname file name
 * @return list of characters
 */
vector<chrctr<unsigned> > readnexboolfile( const string &fname );

/**
 * print usage info and exit
 */
void usage();

/**
 * work
 */
int main(int argc, char *argv[]){
	ccc<unsigned> *c;				// consistency checker
	mednwe<unsigned> *mn;			// median network
	string fname, ext;				// filename and extension
	vector<chrctr<unsigned> > data;	// characters

	// get command line options
	getoptions( argc, argv, fname, ext );

	if( ext == "nex" ){
		data = readnexboolfile(fname);
	}else{
		data = readboolfile(fname);
	}

	c = new ccc_null<unsigned>();
	mn = new mednwe<unsigned>( data, c, true );

	for( list<mednwnde<unsigned> >::const_iterator it=mn->begin(); it!=mn->end(); it++){
		vector<bool> s;
		s = it->getstates();
		copy( s.begin(), s.end(), ostream_iterator<bool>(cout,"") );cout<<endl;
	}

	delete mn;
	/**
		// code for computing a median network from binary data file
		mednw<unsigned> mn;
		ccc_adj cc( 0, false );
		vector<chrctr<unsigned> > data;	// the binary characters
		mn = mednw<unsigned>( data, cc );
	*/

	/**
		// code for computing a median network from binary data file
		mednw<unsigned> mn;
		ccc_adj cc( 0, false );
		vector<chrctr<unsigned> > data;	// the binary characters
		data = readboolfile(fname);
	//	data = readnexboolfile(fname);
		mn = mednw<unsigned>( data, cc );
	*/


	return EXIT_SUCCESS;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void getoptions( int argc, char *argv[], string &fname, string &ext ){
	int c;

	while (1) {
		static struct option long_options[] = {
			{"file",    required_argument, 0, 'f'},
			{"help",    no_argument, 0, 'h'},
            {0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:h",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

		// check parameters
	if( fname == "" ){
		cerr << "no file given"<<endl;
		usage();
	}

	int dot = fname.find_last_of(".");
	ext = string( fname.begin()+dot+1, fname.end() );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

vector<chrctr<unsigned> > readboolfile( const string &fname ){
	ifstream file;		// input file handle
	string line;		// a line of the input
	vector<vector<bool> > cm;	// character matrix
	unsigned m=std::numeric_limits< unsigned >::max(),
		i = 0;

	// open file
	file.open(fname.c_str(), ios::in);
	if(!file.is_open()){
		cerr << "readboolfile: could not read: "<< fname <<endl;
		exit(EXIT_FAILURE);
	}

	getline(file, line);
	while(file){
//		cout << line<<endl;
//		matrix.push_back( vector<bool>() );
//		for( unsigned i=0; i<line.size(); i++ ){
//			if( line[i] == '0' ){
//				matrix.back().push_back( false );
//			}else if( line[i] == '1' ){
//				matrix.back().push_back( true);
//			}else{
//				cerr << "non binary character in binary data file found"<<endl;
//				exit(EXIT_FAILURE);
//			}
//		}
		if( line.size() > 0 && ( m==std::numeric_limits< unsigned >::max() || line.size() == m ) ){
			cm.push_back( vector<bool>( line.size(), false ) );
			for( unsigned j=0; j<line.size(); j++ ){
				if( line[j] == '0' ){
					cm[i][j] = false;
				}else if( line[j] == '1' ){
					cm[i][j] = true;
				}else{
					cerr << "non binary character '"<<line[j]<<"' at line "<<i+1<<endl;
					exit(EXIT_FAILURE);
				}
			}
			m = line.size();
		}else if( line.size()>0 && (m!=std::numeric_limits< unsigned >::max() && line.size() != m) ){
			cerr << "binary sequences of unequal length found"<<endl;
			exit(EXIT_FAILURE);
		}
		getline(file, line);
		i++;
	}
	file.close();

	// construct characters
	vector<chrctr<unsigned> > chars;
	vector<chrctr<unsigned> >::iterator ic;
	chrctr<unsigned> tmp;

//	for( unsigned i=0; i<cm.size(); i++ ){
//		copy(cm[i].begin(), cm[i].end(), ostream_iterator<bool>(cerr,"")); cerr<<endl;
//	}
	for( unsigned i=0; i<cm[0].size(); i++ ){
		tmp = chrctr<unsigned>( cm, i );

		ic = find( chars.begin(), chars.end(), tmp );
		if(ic == chars.end()){
			tmp.addobservation( i, cm[0][i] );
			chars.push_back( tmp );
		}else{
			ic->addobservation( i, cm[0][i] );
		}
	}

	return chars;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

vector<chrctr<unsigned> > readnexboolfile( const string &fname ){

	int status;
	ifstream file;		// input file handle
	string line;		// a line of the input
	vector<vector<bool> > cm; 	// character matrix
	unsigned m=std::numeric_limits< unsigned >::max();

	string s;
	regex_t rec, ren;
	regmatch_t match[2];

	status = regcomp(&rec,"^\\[[[:digit:]]+\\][[:space:]]+([01]+)[[:space:]]*$",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "readnexboolfile: could not compile regex pattern."<<endl;
		exit(EXIT_FAILURE);
	}
	status = regcomp(&ren,"^dimensions nchar=([[:digit:]]+);[[:space:]]*$",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "readnexboolfile: could not compile regex pattern."<<endl;
		exit(EXIT_FAILURE);
	}


	// open file
	file.open(fname.c_str(), ios::in);
	if(!file.is_open()){
		cerr << "readnexboolfile: could not read: "<< fname <<endl;
		exit(EXIT_FAILURE);
	}

	getline(file, line);
	if( line != "#nexus" ){
		cerr << fname <<" seems to be no nexus file"<<endl;
		exit(EXIT_FAILURE);
	}
	while(file){
		if(regexec (&ren, line.c_str(), 2, match, 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			m = atoi (s.c_str());
		}
		if(regexec (&rec, line.c_str(), 2, match, 0)==0){
			s.assign(line.begin()+match[1].rm_so, line.begin()+match[1].rm_eo);
			if( s.size() != m ){
				cerr << "binary sequences of wrong length found"<<endl;
				exit(EXIT_FAILURE);
			}

			cm.push_back( vector<bool>( s.size(), false ) );
			for( unsigned j=0; j<s.size(); j++ ){
				(cm.back())[j] = ( s[j] == '0' ) ? false : true;
			}
		}
		getline(file, line);
	}
	file.close();

	// construct characters
	vector<chrctr<unsigned> > chars;
	vector<chrctr<unsigned> >::iterator ic;
	chrctr<unsigned> tmp;
	for( unsigned i=0; i<cm[0].size(); i++ ){
		tmp = chrctr<unsigned>( cm, i );

		ic = find( chars.begin(), chars.end(), tmp );
		if(ic == chars.end()){
			tmp.addobservation( i, cm[0][i] );
			chars.push_back( tmp );
		}else{
			ic->addobservation( i, cm[0][i] );
		}
	}

	regfree(&rec);
	regfree(&ren);
	return chars;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cerr << "usage:"<<endl;
	cerr << "mednw -f file"<<endl;
	exit( EXIT_FAILURE );

}
