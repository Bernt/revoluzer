#include <iostream>

#include "common.hpp"
#include "conserved.hpp"
#include "caprara.hpp"
#include "genom.hpp"
#include "io.hpp"
#include "helpers.hpp"

using namespace std;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int main(int argc, char *argv[]){
	bool horizontal = false;
	int circular = 0,			// circular genomes
		n=0;
	itnode *tree_root;
	mpqnode *mpqroot;				// the root of the pqtree of the input genomes
	string filename;			// input filename
	vector<genom> genomes;		// input genomes
	vector<string> names,		// names of the input genomes
		nmap;

		// get the parameters
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'c': {circular = 1; break;}
			case 'f': {i++; filename = argv[i]; break;}
			case 'h': {horizontal = true; break;}
			default:{
				cout << "unknown parameter "<<argv[i]<<endl;
				usage();
			}
		}
	}

	if(filename.size()==0){
		cerr << "error: no filename given ! "<<endl;
		usage();
	}

	read_genomes(filename, genomes, names, circular, nmap);	// read the genom file

//	copy(nmap.begin(), nmap.end(), ostream_iterator<string>(cerr," "));cerr<<endl;

	if(genomes.size() == 0){
		cerr << "error: no genomes found in "<<filename<<endl;
		usage();
	}

	n = genomes[0].size();

//	cout << genomes << endl;

	if(genomes.size() == 2){
		interval_tree(genomes[0], genomes[1], n, &tree_root);
		//interval_tree_print(tree_root, genomes[0], cout);
		interval_tree_print_dot(tree_root, genomes[0], cout, nmap);
		interval_tree_free(tree_root);
	}else if(genomes.size() > 2 ){
		mpqroot = mpqtree_construct(genomes, n);
		mpqtree_print_dot(mpqroot, genomes[0], genomes.size(), cout, horizontal);
		//mpqtree_print(mpqroot, genomes.size());
		mpqtree_free(mpqroot);
	}

	genomes.clear();
	names.clear();
	nmap.clear();

	return 0;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cout << "pqplot -f filename [-c] [-h]" << endl;
	cout << "-c circular genomes"<<endl;
	cout << "-h print signs horizontally (only useful for m>2)"<<endl;
	exit(0);
}
