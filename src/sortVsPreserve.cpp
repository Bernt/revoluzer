/** @file sortVsPreserve.cpp
 * program for calculation how much soting, neutral and preserving reversals
 * are available at average
 */
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <math.h>

#include "conserved.hpp"
#include "dstnc_inv.hpp"
#include "genom.hpp"
#include "helpers.hpp"
#include "io.hpp"
#include "rearrangements.hpp"
#include "rrrmtrand.hpp"

using namespace std;

int sortVsPreserve(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist);
int medianSortVsPreserve(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist);
int inclusion(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist);
int merging(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist);
int intervalVsReversalDistance(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist);
int reversalsVsReversalDistance(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist);
// int condensed_uncondensed(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist);

/**
 * print some usage info
 */
void usage(void){
	cout << "sortVsPreserve -[c|v|m|i|h] [-u] -d <directory> -n <length> -e <exps>"<<endl;
	// cout << "-c nr of sorting revs for condensed and uncondensed" << endl;
	cout << "-d directory for the output files (with a '/' at the end! )"<<endl;
	cout << "-v sorting vs median m=2"<<endl;
	cout << "-m sorting vs median m=3"<<endl;
	cout << "-i percentual inclusion of sorting and median m=2"<<endl;
	cout << "-h count the hurdle mergings"<<endl;
	cout << "-r interval vs reversaldistance"<<endl;
	cout << "-s reversals vs reversaldistance"<<endl;
	cout << "-n length of the genomes"<<endl;
	cout << "-e number of experiments"<<endl;
	cout << "-u unsigned genomes (cur. only for -v & -i)"<<endl;
	exit(1);
}

/**
 * main starts one of the function in dependance from the parameters
 */
int main(int argc, char *argv[]) {
	
	int (*foo)(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist) = NULL;
	int n=0,
		e=0;
	bool unsig=false;
	dstnc_inv *rdist = NULL;
	string dir="",
		input_file="";
	vector<genom> datasets;
	vector<string> names;
	
	for(int i=1; i<argc; i++){
		switch(argv[i][1]){
			case 'v':{
				foo = sortVsPreserve;
				break;
			}
			case 'm':{
				foo = medianSortVsPreserve;
				break;
			}
			case 'i':{
				foo = inclusion;
				break;
			}
			case 'h':{
				foo = merging;
				break;
			}
			case 'r':{
				foo = intervalVsReversalDistance;
				break;
			}
			case 's':{
				foo = reversalsVsReversalDistance;
				break;
			}
/*			case 'c':{
				foo = condensed_uncondensed;
				break;
			}
*/
			case 'd':{
				i++;
				if(i<argc)
					dir = argv[i];
				break;
			}
			case 'e':{
				i++;
				if(i<argc)
					e=atoi(argv[i]);
				break;
			}
			case 'n':{
				i++;
				if(i<argc)
					n=atoi(argv[i]);
				break;
			}
			case 'u':{
				unsig = true;
				break;
			}
			case 'f':{
				i++;
				input_file = argv[i];
				break;
			}
			default:{
				cout << "unrecognized option "<< argv[i]<<endl;
			}
		}
	}
	if(foo==NULL){
		cout <<"error: nothing to do"<<endl;
		usage();
	}
	if(dir==""){
		cout <<"error: no directory given"<<endl;
		usage();
	}
	if(!n){
		cout <<"error: no length given"<<endl;
		usage();
	}
	init_rng();				// init random number generator
	if(input_file != ""){
		vector<genom> G;
		genom temp;
		
		read_taxa(input_file, G, names, 0);	// read the genom file
		
		rdist = new dstnc_inv(G[0].size(), 0);
		for(unsigned i=0; i<G.size(); i++){
			for(unsigned j=i+1; j<G.size(); j++){
				if(rdist->calc(G[i],G[j])>0){
					temp = G[i];
					temp.identify_g(G[j]);
					datasets.push_back(temp);
				}
			}
		}
		n = datasets[0].size();
		unsig = true;
	}
	//~ else{
		//~ genom temp;
		
	if(rdist == NULL){
		rdist = new dstnc_inv(n,0);
	}

		//~ if(unsig){
			//~ for(int i=0; i<e; i++){
				//~ int dist=(ask_rng()%(n-1))+1;
				//~ temp = genom(n, dist, 0, n, true, true, 0, unsig);
				//~ datasets.push_back(temp);
			//~ }
		//~ }else{
			//~ for(int dist=n/10; dist<=n; dist+= n/10){
				//~ for(int i=0; i<e; i++){
					//~ temp = genom(n, dist,0, n, false, false, 0, unsig);
					//~ datasets.push_back(temp);
				//~ }
			//~ }
		//~ }
	//~ }

	//~ if(datasets.size()==0){
		//~ cout <<"error: no number of experiments given"<<endl;
		//~ usage();
	//~ }
	delete rdist;
	return ((*foo)(dir, e, n, unsig, datasets, rdist));
}

/**
 * function to make some statistics for the setinclusion of the sets of reversals which are 
 * sorting, neutral, preserving and preserving on the same cycle. this is done by generating 
 * two random sequences in all possible distances (to the idendity) and testing which 
 * reversals are are sorting this is done multiple times and the average is outputed 
 *@param dir outputfilename
 *@param exp number of experiments
 *@param length length of the tested sequences
 */
int inclusion(string dir, int exp, int length, bool unsig, vector<genom> &data, dstnc_inv *rdist){
	genom id(length, false);
	vector<pair<int,int> > reversals = id.getReversals();	// get all possible reversals
	ofstream per_c, per_cUc;
	string filename;
	unsigned avg_distance=0;
	
	filename = dir+"inclusion_cyc_n"+int2string(length)+".dat";
	per_c.open(filename.c_str());
	if(!per_c.is_open()){
		cout << "could not open : "<<filename <<endl;
		exit(1);
	}

	filename = dir+"inclusion_cUc_n"+int2string(length)+".dat";
	per_cUc.open(filename.c_str());
	if(!per_cUc.is_open()){
		cout << "could not open : "<<filename <<endl;
		exit(1);
	}
	

	map<unsigned, float> cCupSAvg,
		cCupNAvg,
		cCutSAvg,
		cCutNAvg,
		cDifNAvg,
		nDifcAvg,
		cDifSAvg,
		sDifcAvg,
	
		cUcCupSAvg,
		cUcCupNAvg,
		cUcCutSAvg,
		cUcCutNAvg,
		cUcDifNAvg,
		nDifcUcAvg,
		cUcDifSAvg,
		sDifcUcAvg;
	map<unsigned, unsigned> count;
	int dist;	
		
	for(unsigned i=0; i<data.size(); i++){
		//~ cout << "computing "<<i+1<<"/"<<data.size()<<"\r";
		//~ cout.flush();
		
		if(unsig){
			dist = 0;
			avg_distance+=rdist->calc(id,data[i]);
		}else{
			dist = rdist->calc(id,data[i]);
		}
		count[dist]++;
		vector<pair<int,int> > cycleReversals,
			cycleUCompReversals,
			sortingReversals,
			neutralReversals,
			nabReversals,
			interestingReversals;
		
		//~ nabReversals = r.getReversals_nonAdjacencyBreaking(id);
		cycleReversals = data[i].getReversalsSameCycle(id);
		cycleUCompReversals = data[i].getReversalsSameUnorientedComponent(id);
		cycleUCompReversals.insert(cycleUCompReversals.end(), cycleReversals.begin(), cycleReversals.end());

			// get the sorting reversals
		for(unsigned j=0; j< reversals.size(); j++){
			genom temp = data[i];
			reverse(temp, reversals[j]);
			if(rdist->calc(temp,id) <rdist->calc(data[i],id))
				sortingReversals.push_back(reversals[j]);
		}
			// get the neutral reversals
		for(unsigned j=0; j< reversals.size(); j++){
			genom temp = data[i];
			reverse(temp, reversals[j]);
			if(rdist->calc(temp,id) <= rdist->calc(data[i],id))
				neutralReversals.push_back(reversals[j]);
		}
			// make alle pairs the same ordering (for conparing)
		for (unsigned j=0; j<cycleReversals.size(); j++){
			if(cycleReversals[j].first > cycleReversals[j].second)
				swap(cycleReversals[j].first,cycleReversals[j].second);
		}
		for (unsigned j=0; j<cycleUCompReversals.size(); j++){
			if(cycleUCompReversals[j].first > cycleUCompReversals[j].second)
				swap(cycleUCompReversals[j].first,cycleUCompReversals[j].second);
		}
		for (unsigned j=0; j<sortingReversals.size(); j++){
			if(sortingReversals[j].first > sortingReversals[j].second)
				swap(sortingReversals[j].first,sortingReversals[j].second);
		}
		for (unsigned j=0; j<neutralReversals.size(); j++){
			if(neutralReversals[j].first > neutralReversals[j].second)
				swap(neutralReversals[j].first, neutralReversals[j].second);
		}

			// sort everything
		sort (cycleReversals.begin(), cycleReversals.end());
		sort (cycleUCompReversals.begin(), cycleUCompReversals.end());
		sort (sortingReversals.begin(), sortingReversals.end());
		sort (neutralReversals.begin(), neutralReversals.end());

		vector<pair<int,int> > 
			cCupS,cCupN,cCutS,cCutN,cDifN,nDifc,cDifS,sDifc,
			cUcCupS,cUcCupN,cUcCutS,cUcCutN,cUcDifN,nDifcUc,cUcDifS,sDifcUc;

		set_union(cycleReversals.begin(), cycleReversals.end(),
					sortingReversals.begin(), sortingReversals.end(), back_inserter(cCupS));
		set_union(cycleReversals.begin(), cycleReversals.end(),
					neutralReversals.begin(), neutralReversals.end(), back_inserter(cCupN));

		set_intersection(cycleReversals.begin(), cycleReversals.end(),
					sortingReversals.begin(), sortingReversals.end(), back_inserter(cCutS));
		set_intersection(cycleReversals.begin(), cycleReversals.end(),
					neutralReversals.begin(), neutralReversals.end(), back_inserter(cCutN));

		set_difference(cycleReversals.begin(), cycleReversals.end(),
							cCutN.begin(), cCutN.end(), back_inserter(cDifN));
		set_difference(neutralReversals.begin(), neutralReversals.end(),
							cCutN.begin(), cCutN.end(), back_inserter(nDifc));

		set_difference(cycleReversals.begin(), cycleReversals.end(),
							cCutS.begin(), cCutS.end(), back_inserter(cDifS));
		set_difference(sortingReversals.begin(), sortingReversals.end(),
							cCutS.begin(), cCutS.end(), back_inserter(sDifc));


		set_union(cycleUCompReversals.begin(), cycleUCompReversals.end(),
					sortingReversals.begin(), sortingReversals.end(), back_inserter(cUcCupS));
		set_union(cycleUCompReversals.begin(), cycleUCompReversals.end(),
					neutralReversals.begin(), neutralReversals.end(), back_inserter(cUcCupN));

		set_intersection(cycleUCompReversals.begin(), cycleUCompReversals.end(),
					sortingReversals.begin(), sortingReversals.end(), back_inserter(cUcCutS));
		set_intersection(cycleUCompReversals.begin(), cycleUCompReversals.end(),
					neutralReversals.begin(), neutralReversals.end(), back_inserter(cUcCutN));

		set_difference(cycleUCompReversals.begin(), cycleUCompReversals.end(),
							cUcCutN.begin(), cUcCutN.end(), back_inserter(cUcDifN));
		set_difference(neutralReversals.begin(), neutralReversals.end(),
							cUcCutN.begin(), cUcCutN.end(), back_inserter(nDifcUc));

		set_difference(cycleUCompReversals.begin(), cycleUCompReversals.end(),
							cUcCutS.begin(), cUcCutS.end(), back_inserter(cUcDifS));
		set_difference(sortingReversals.begin(), sortingReversals.end(),
							cUcCutS.begin(), cUcCutS.end(), back_inserter(sDifcUc));


		cCupSAvg[dist] += cCupS.size();
		cCupNAvg[dist] += cCupN.size();
		cCutSAvg[dist] += cCutS.size();
		cCutNAvg[dist] += cCutN.size();
		cDifNAvg[dist] += cDifN.size();
		nDifcAvg[dist] += nDifc.size();
		cDifSAvg[dist] += cDifS.size();
		sDifcAvg[dist] += sDifc.size();
		
		cUcCupSAvg[dist] += cUcCupS.size();
		cUcCupNAvg[dist] += cUcCupN.size();
		cUcCutSAvg[dist] += cUcCutS.size();
		cUcCutNAvg[dist] += cUcCutN.size();
		cUcDifNAvg[dist] += cUcDifN.size();
		nDifcUcAvg[dist] += nDifcUc.size();
		cUcDifSAvg[dist] += cUcDifS.size();
		sDifcUcAvg[dist] += sDifcUc.size();
	}

	avg_distance/=data.size();
	per_c << "# S:sorting; N:neutral; C: on the same cycle"<<endl;
	per_c << "# length of the genomes : "<<length << endl;
	per_c << "# number of experiments : "<<data.size() << endl;
	per_c << "# avg distance : "<<avg_distance<< endl;
	per_c << "# average values in persent of:"<<endl;
	per_c << "# d \tS\\C \tS&C \tC\\S \tN\\C \tN&C \tC\\N "<<endl;

	per_cUc << "# S:sorting; N:neutral; CUC: on the same cycle or same unoriented component"<<endl;
	per_cUc << "# length of the genomes : "<<length << endl;
	per_cUc << "# number of experiments : "<<data.size()<< endl;
	per_cUc << "# avg distance : "<<avg_distance<< endl;
	per_cUc << "# average values in persent of:"<<endl;
	per_cUc << "# d \tS\\CUC \tS&CUC \tCUC\\S \tN\\CUC \tN&CUC \tCUC\\N "<<endl;
	
	
	for(map<unsigned,float>::iterator it=cCupSAvg.begin(); it!=cCupSAvg.end(); it++){
			// get the average
		cCupSAvg[it->first] /= count[it->first],
		cCupNAvg[it->first] /= count[it->first],
		cCutSAvg[it->first] /= count[it->first],
		cCutNAvg[it->first] /= count[it->first],
		cDifNAvg[it->first] /= count[it->first],
		nDifcAvg[it->first] /= count[it->first],
		cDifSAvg[it->first] /= count[it->first],
		sDifcAvg[it->first] /= count[it->first];

		cUcCupSAvg[it->first] /= count[it->first],
		cUcCupNAvg[it->first] /= count[it->first],
		cUcCutSAvg[it->first] /= count[it->first],
		cUcCutNAvg[it->first] /= count[it->first],
		cUcDifNAvg[it->first] /= count[it->first],
		nDifcUcAvg[it->first] /= count[it->first],
		cUcDifSAvg[it->first] /= count[it->first],
		sDifcUcAvg[it->first] /= count[it->first];

		per_c   << it->first 	<< "\t" << sDifcAvg[it->first]*100/cCupSAvg[it->first] 
						<< "\t" << (cCutSAvg[it->first]+sDifcAvg[it->first])*100/cCupSAvg[it->first]
						<< "\t" << (sDifcAvg[it->first]+cCutSAvg[it->first]+cDifSAvg[it->first])*100/cCupSAvg[it->first]
						
						<< "\t" << nDifcAvg[it->first]*100/cCupNAvg[it->first] 
						<< "\t" << (nDifcAvg[it->first]+cCutNAvg[it->first])*100/cCupNAvg[it->first]
						<< "\t" << (nDifcAvg[it->first]+cCutNAvg[it->first]+cDifNAvg[it->first])*100/cCupNAvg[it->first]<<endl;

		per_cUc   <<it->first 	<< "\t" << sDifcUcAvg[it->first]*100/cUcCupSAvg[it->first] 
						<< "\t" << (cUcCutSAvg[it->first]+sDifcUcAvg[it->first])*100/cUcCupSAvg[it->first]
						<< "\t" << (sDifcUcAvg[it->first]+cUcCutSAvg[it->first]+cUcDifSAvg[it->first])*100/cUcCupSAvg[it->first]
						<< "\t" << nDifcUcAvg[it->first]*100/cUcCupNAvg[it->first] 
						<< "\t" << (nDifcUcAvg[it->first]+cUcCutNAvg[it->first])*100/cUcCupNAvg[it->first]
						<< "\t" << (nDifcUcAvg[it->first]+cUcCutNAvg[it->first]+cUcDifNAvg[it->first])*100/cUcCupNAvg[it->first]<<endl;
	}
	per_c.close();
	per_cUc.close();
	cout << endl;
	return 1;

	//~ for(int dist=length/10; dist<=length; dist+= length/10){
		//~ float cCupSAvg = 0,
			//~ cCupNAvg = 0,
			//~ cCutSAvg = 0,
			//~ cCutNAvg = 0,
			//~ cDifNAvg = 0,
			//~ nDifcAvg = 0,
			//~ cDifSAvg = 0,
			//~ sDifcAvg = 0,
		
			//~ cUcCupSAvg = 0,
			//~ cUcCupNAvg = 0,
			//~ cUcCutSAvg = 0,
			//~ cUcCutNAvg = 0,
			//~ cUcDifNAvg = 0,
			//~ nDifcUcAvg = 0,
			//~ cUcDifSAvg = 0,
			//~ sDifcUcAvg = 0;
		
		//~ for (int e=0; e< exp; e++){
			//~ cout << "LENGTH "<<length<< " dist "<<dist <<" Exp "<<e<<"\r";
			//~ cout.flush();

			//~ genom r;
			//~ if(unsig){
				//~ r = genom("r", length, dist,0, length, true, false, 0, unsig);
			//~ }else{
				//~ r = genom("r", length, dist,0, length, false, false, 0, unsig);
			//~ }

			//~ vector<pair<int,int> > cycleReversals,
				//~ cycleUCompReversals,
				//~ sortingReversals,
				//~ neutralReversals,
				//~ nabReversals;
			
			//nabReversals = r.getReversals_nonAdjacencyBreaking(id);
			//~ cycleReversals = r.getReversalsSameCycle(id);
			//~ cycleUCompReversals = r.getReversalsSameUnorientedComponent(id);
			//~ cycleUCompReversals.insert(cycleUCompReversals.end(), cycleReversals.begin(), cycleReversals.end());

				//~ // get the sorting reversals
			//~ for(unsigned i=0; i< reversals.size(); i++){
				//~ genom temp = r;
				//~ temp.reverse(reversals[i]);
				//~ if(temp.distance(id) < dist)
					//~ sortingReversals.push_back(reversals[i]);
			//~ }
				//~ // get the neutral reversals
			//~ for(unsigned i=0; i< reversals.size(); i++){
				//~ genom temp = r;
				//~ temp.reverse(reversals[i]);
				//~ if(temp.distance(id) <= dist)
					//~ neutralReversals.push_back(reversals[i]);
			//~ }

				//~ // make alle pairs the same ordering (for conparing)
			//~ for (unsigned i=0; i<cycleReversals.size(); i++)
				//~ if(cycleReversals[i].first > cycleReversals[i].second)
					//~ swap(cycleReversals[i].first,cycleReversals[i].second);
			//~ for (unsigned i=0; i<cycleUCompReversals.size(); i++)
				//~ if(cycleUCompReversals[i].first > cycleUCompReversals[i].second)
					//~ swap(cycleUCompReversals[i].first,cycleUCompReversals[i].second);
			//~ for (unsigned i=0; i<sortingReversals.size(); i++)
				//~ if(sortingReversals[i].first > sortingReversals[i].second)
					//~ swap(sortingReversals[i].first,sortingReversals[i].second);
			//~ for (unsigned i=0; i<neutralReversals.size(); i++)
				//~ if(neutralReversals[i].first > neutralReversals[i].second)
					//~ swap(neutralReversals[i].first, neutralReversals[i].second);

				//~ // sort everything
			//~ sort (cycleReversals.begin(), cycleReversals.end());
			//~ sort (cycleUCompReversals.begin(), cycleUCompReversals.end());
			//~ sort (sortingReversals.begin(), sortingReversals.end());
			//~ sort (neutralReversals.begin(), neutralReversals.end());

			//~ vector<pair<int,int> > 
				//~ cCupS,cCupN,cCutS,cCutN,cDifN,nDifc,cDifS,sDifc,
				//~ cUcCupS,cUcCupN,cUcCutS,cUcCutN,cUcDifN,nDifcUc,cUcDifS,sDifcUc;

			//~ set_union(cycleReversals.begin(), cycleReversals.end(),
						//~ sortingReversals.begin(), sortingReversals.end(), back_inserter(cCupS));
			//~ set_union(cycleReversals.begin(), cycleReversals.end(),
						//~ neutralReversals.begin(), neutralReversals.end(), back_inserter(cCupN));

			//~ set_intersection(cycleReversals.begin(), cycleReversals.end(),
						//~ sortingReversals.begin(), sortingReversals.end(), back_inserter(cCutS));
			//~ set_intersection(cycleReversals.begin(), cycleReversals.end(),
						//~ neutralReversals.begin(), neutralReversals.end(), back_inserter(cCutN));

			//~ set_difference(cycleReversals.begin(), cycleReversals.end(),
								//~ cCutN.begin(), cCutN.end(), back_inserter(cDifN));
			//~ set_difference(neutralReversals.begin(), neutralReversals.end(),
								//~ cCutN.begin(), cCutN.end(), back_inserter(nDifc));

			//~ set_difference(cycleReversals.begin(), cycleReversals.end(),
								//~ cCutS.begin(), cCutS.end(), back_inserter(cDifS));
			//~ set_difference(sortingReversals.begin(), sortingReversals.end(),
								//~ cCutS.begin(), cCutS.end(), back_inserter(sDifc));


			//~ set_union(cycleUCompReversals.begin(), cycleUCompReversals.end(),
						//~ sortingReversals.begin(), sortingReversals.end(), back_inserter(cUcCupS));
			//~ set_union(cycleUCompReversals.begin(), cycleUCompReversals.end(),
						//~ neutralReversals.begin(), neutralReversals.end(), back_inserter(cUcCupN));

			//~ set_intersection(cycleUCompReversals.begin(), cycleUCompReversals.end(),
						//~ sortingReversals.begin(), sortingReversals.end(), back_inserter(cUcCutS));
			//~ set_intersection(cycleUCompReversals.begin(), cycleUCompReversals.end(),
						//~ neutralReversals.begin(), neutralReversals.end(), back_inserter(cUcCutN));

			//~ set_difference(cycleUCompReversals.begin(), cycleUCompReversals.end(),
								//~ cUcCutN.begin(), cUcCutN.end(), back_inserter(cUcDifN));
			//~ set_difference(neutralReversals.begin(), neutralReversals.end(),
								//~ cUcCutN.begin(), cUcCutN.end(), back_inserter(nDifcUc));

			//~ set_difference(cycleUCompReversals.begin(), cycleUCompReversals.end(),
								//~ cUcCutS.begin(), cUcCutS.end(), back_inserter(cUcDifS));
			//~ set_difference(sortingReversals.begin(), sortingReversals.end(),
								//~ cUcCutS.begin(), cUcCutS.end(), back_inserter(sDifcUc));

			//~ cCupSAvg += cCupS.size();
			//~ cCupNAvg += cCupN.size();
			//~ cCutSAvg += cCutS.size();
			//~ cCutNAvg += cCutN.size();
			//~ cDifNAvg += cDifN.size();
			//~ nDifcAvg += nDifc.size();
			//~ cDifSAvg += cDifS.size();
			//~ sDifcAvg += sDifc.size();
			
			//~ cUcCupSAvg += cUcCupS.size();
			//~ cUcCupNAvg += cUcCupN.size();
			//~ cUcCutSAvg += cUcCutS.size();
			//~ cUcCutNAvg += cUcCutN.size();
			//~ cUcDifNAvg += cUcDifN.size();
			//~ nDifcUcAvg += nDifcUc.size();
			//~ cUcDifSAvg += cUcDifS.size();
			//~ sDifcUcAvg += sDifcUc.size();

		//~ }
			//~ // get the average
		//~ cCupSAvg /= exp,
		//~ cCupNAvg /= exp,
		//~ cCutSAvg /= exp,
		//~ cCutNAvg /= exp,
		//~ cDifNAvg /= exp,
		//~ nDifcAvg /= exp,
		//~ cDifSAvg /= exp,
		//~ sDifcAvg /= exp;

		//~ cUcCupSAvg /= exp,
		//~ cUcCupNAvg /= exp,
		//~ cUcCutSAvg /= exp,
		//~ cUcCutNAvg /= exp,
		//~ cUcDifNAvg /= exp,
		//~ nDifcUcAvg /= exp,
		//~ cUcDifSAvg /= exp,
		//~ sDifcUcAvg /= exp;

			//~ // output S\PC(because it's the smallest ); then add pc Cut s; and the 100 (%)
		//~ per_c   << dist 	<< "\t" << sDifcAvg*100/cCupSAvg 
						//~ << "\t" << (cCutSAvg+sDifcAvg)*100/cCupSAvg
						//~ << "\t" << (sDifcAvg+cCutSAvg+cDifSAvg)*100/cCupSAvg
						//~ << "\t" << nDifcAvg*100/cCupNAvg 
						//~ << "\t" << (nDifcAvg+cCutNAvg)*100/cCupNAvg
						//~ << "\t" << (nDifcAvg+cCutNAvg+cDifNAvg)*100/cCupNAvg<<endl;

		//~ per_cUc   << dist 	<< "\t" << sDifcUcAvg*100/cUcCupSAvg 
						//~ << "\t" << (cUcCutSAvg+sDifcUcAvg)*100/cUcCupSAvg
						//~ << "\t" << (sDifcUcAvg+cUcCutSAvg+cUcDifSAvg)*100/cUcCupSAvg
						//~ << "\t" << nDifcUcAvg*100/cUcCupNAvg 
						//~ << "\t" << (nDifcUcAvg+cUcCutNAvg)*100/cUcCupNAvg
						//~ << "\t" << (nDifcUcAvg+cUcCutNAvg+cUcDifNAvg)*100/cUcCupNAvg<<endl;

		/*per << dist << "\t" << sDifPcAvg*100/pcCupSAvg 
						<< "\t" << pcCutSAvg*100/pcCupSAvg 
						<< "\t" << pcDifSAvg*100/pcCupSAvg
						<< "\t" << nDifPcAvg*100/pcCupNAvg 
						<< "\t" << pcCutNAvg*100/pcCupNAvg 
						<< "\t" << pcDifNAvg*100/pcCupNAvg << endl;*/
	//~ }
	//~ per_c.close();
	//~ per_cUc.close();
	//~ cout << endl;
	//~ return 1;
}

/**
 * function to make some statistics for the amount of reversals which are sorting, neutral,
 * preserving and preserving on the same cycle. this is done by generating two random sequences
 * in all possible distances (to the idendity) and testing which reversals are are sorting ... to both
 * this is done multiple times and the average is outputed
 *@param dir outputfilename
 *@param e number of experiments
 *@param n length of the tested sequences
 */
int medianSortVsPreserve(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist){
	unsigned cycle_cnt;
	int trials = 1000;		// number of trials
	vector<int> lengthVec;
	ofstream abs,
		per;

	vector<float> prob(4,0);
	prob[0] = 100;
	vector<genom> trace;
	unordered *rand_sce;

	lengthVec.push_back(10);
	lengthVec.push_back(100);
	
	for(unsigned l=0; l<lengthVec.size(); l++){
		int length = lengthVec[l];				// length of the genomes
		genom id(length, false);
		vector<pair<int,int> > reversals = id.getReversals();	// get all possible reversals
		unsigned max = reversals.size();	// maximal possible count of reversals = (n over 2) binomial coefficient
		string filename;
		string filename_p;

		filename = dir+"sortVsPreserve_n"+int2string(length)+".dat";
		filename_p = dir+"sortVsPreserve_n"+int2string(length)+"_per.dat";

		abs.open(filename.c_str());
		if(!abs.is_open()){
			cout << "could not open : "<<filename <<endl;
			exit(1);
		}
		per.open(filename_p.c_str());
		if(!per.is_open()){
			cout << "could not open : "<<filename <<endl;
			exit(1);
		}
		cout << "write in "<<filename<<" and "<<filename_p<<endl;		
		
		abs << "# rms := root mean square"<<endl;
		abs << "# average, avg+rms and avg-rms numbers of sorting-, neutral-, preserving- and preserving (on the same sycle) reversals"<<endl;
		abs << "# to two permutation together as absolute values"<<endl;
		abs << "# length of the genomes : "<<length << endl;
		abs << "# number of experiments : "<<trials << endl;
		abs << "# d \ts_avg \ts_avg+ \ts_avg- \tn_avg \tn_avg+ \tn_avg- \tp_avg \tp_avg+ \tp_avg- \tpc_avg \tpc_avg+ \tpc_avg"<<endl;
		
		per << "# average, avg+rms and avg-rms numbers of sorting-, neutral-, preserving- and preserving (on the same sycle) reversals"<<endl;
		per << "# to two permutation together in percent from the maximum "<<endl;
		per << "# length of the genomes : "<<length << endl;
		per << "# number of experiments : "<<trials << endl;
		per << "# d \ts_avg \ts_avg+ \ts_avg- \tn_avg \tn_avg+ \tn_avg- \tp_avg \tp_avg+ \tp_avg- \tpc_avg \tpc_avg+ \tpc_avg"<<endl;

		
		for(unsigned dist=0; dist<=(unsigned)length; dist+= length/10){
			vector<float> sorting(trials),
				neutral(trials),
				preserving(trials),
				preservingCycle(trials);

			float sortingAvg = 0, sortingVar =0,
				preservingAvg = 0, neutralVar =0,
				neutralAvg = 0, preservingVar =0,
				preservingCycleAvg = 0, preservingCycleVar=0;

			cout << "LENGTH "<<length<< " dist "<<dist <<"\r";
			cout.flush();
			
			for (int j=0; j<trials; j++){

				genom r1(length, 0);
				genom r2(length, 0);

				rand_sce = new rrrmt_rand( dist, prob, r1, trace, true, std::numeric_limits< unsigned >::max());
				delete rand_sce;
				rand_sce = new rrrmt_rand( dist, prob, r2, trace, true, std::numeric_limits< unsigned >::max() );
				delete rand_sce;
//				genom r1(length, dist,0, length, false, false, 0, false, hd);
//				genom r2(length, dist,0, length, false, false, 0, false, hd);
				
				vector<pair<int,int> > preservingReversals,
					preservingReversals2,
					preservingReversalsCut,
					preservingCycleReversals,
					preservingCycleReversals2,
					preservingCycleReversalsCut,
					sortingReversals,
					sortingReversalsCut,
					neutralReversals,
					neutralReversalsCut;

				vector<int> pi;
				vector<int> cycs,
					cycs2;
				
				pi = id.identify(r1);
				cycle_cnt = cycles(pi, id.size(), cycs),
				pi = id.identify(r2);
				cycle_cnt = cycles(pi, id.size(), cycs2);
					// get the preserving reversals
				preservingReversals  = getPreservingReversals(id, r1);
				preservingReversals2 = getPreservingReversals(id, r2);
				sort(preservingReversals.begin(), preservingReversals.end());
				sort(preservingReversals2.begin(), preservingReversals2.end());
				set_intersection(	preservingReversals.begin(), 	preservingReversals.end(), 
										preservingReversals2.begin(), preservingReversals2.end(),
										back_inserter(preservingReversalsCut));
				
					// get the preserving reversals which are on the same cycle
				for(unsigned i=0; i<preservingReversals.size(); i++){
					if(cycs[preservingReversals[i].first] && cycs[preservingReversals[i].first] == cycs[preservingReversals[i].second+1])
						preservingCycleReversals.push_back(preservingReversals[i]);
				}
				for(unsigned i=0; i<preservingReversals2.size(); i++){
					if(cycs2[preservingReversals2[i].first] && cycs2[preservingReversals2[i].first] == cycs2[preservingReversals2[i].second+1])
						preservingCycleReversals2.push_back(preservingReversals2[i]);
				}
				sort(preservingCycleReversals.begin(), preservingCycleReversals.end());
				sort(preservingCycleReversals2.begin(), preservingCycleReversals2.end());
				set_intersection(preservingCycleReversals.begin(), preservingCycleReversals.end(),
										preservingCycleReversals2.begin(), preservingCycleReversals2.end(),
										back_inserter(preservingCycleReversalsCut));
				
					// get the sorting reversals
				for(unsigned i=0; i< reversals.size(); i++){
					genom temp = id;
					reverse(temp, reversals[i]);
					if(rdist->calc(temp,r1) <dist && rdist->calc(temp,r2) < dist)
						sortingReversalsCut.push_back(reversals[i]);
				}
					// get the neutral reversals
				for(unsigned i=0; i< reversals.size(); i++){
					genom temp = id;
					reverse(temp, reversals[i]);
					if(rdist->calc(temp,r1) <= dist && rdist->calc(temp,r2) <= dist)
						neutralReversalsCut.push_back(reversals[i]);
				}

					// store the result sizes
				sorting[j] = sortingReversalsCut.size();
				neutral[j] = neutralReversalsCut.size();
				preserving[j] = preservingReversalsCut.size();
				preservingCycle[j] = preservingCycleReversalsCut.size();

					// sum for median
				sortingAvg += sortingReversalsCut.size();
				neutralAvg += neutralReversalsCut.size();
				preservingAvg += preservingReversalsCut.size();
				preservingCycleAvg += preservingCycleReversalsCut.size();
			}

				// get the median
			sortingAvg /= trials;
			neutralAvg /= trials;
			preservingAvg /= trials;
			preservingCycleAvg /= trials;
			
				// get the variance
			for(int i=0; i<trials; i++){
				sortingVar 				+= pow(sorting[i]-sortingAvg, 2);
				neutralVar 				+= pow(sorting[i]-neutralAvg, 2);
				preservingVar 			+= pow(sorting[i]-preservingAvg, 2);
				preservingCycleVar 	+= pow(sorting[i]-preservingCycleAvg, 2);
			}
 			sortingVar = sqrt(sortingVar / (trials-1));
 			neutralVar = sqrt(neutralVar / (trials-1));
 			preservingVar = sqrt(preservingVar / (trials-1));
 			preservingCycleVar = sqrt(preservingCycleVar / (trials-1));

				// output the absolute values
			abs 	<< dist 	<<"\t"<<sortingAvg 			<<"\t"<<sortingAvg+sortingVar			<<"\t"<<sortingAvg-sortingVar
								<<"\t"<<neutralAvg			<<"\t"<<neutralAvg+neutralVar			<<"\t"<<neutralAvg-neutralVar
								<<"\t"<<preservingAvg		<<"\t"<<preservingAvg+preservingVar		<<"\t"<<preservingAvg-preservingVar
								<<"\t"<<preservingCycleAvg	<<"\t"<<preservingCycleAvg+preservingCycleVar	<<"\t"<<preservingCycleAvg-preservingCycleVar <<endl;
				// output the percentage
			per << dist 	<<"\t"<<sortingAvg*100 / max <<"\t"<<(sortingAvg+sortingVar)*100 / max <<"\t"<<(sortingAvg-sortingVar)*100 / max 
								<<"\t"<<neutralAvg*100 / max <<"\t"<<(neutralAvg+neutralVar)*100 / max <<"\t"<<(neutralAvg-neutralVar)*100 / max 
								<<"\t"<<preservingAvg*100 / max <<"\t"<<(preservingAvg+preservingVar)*100 / max<<"\t"<<(preservingAvg-preservingVar)*100 / max
								<<"\t"<<preservingCycleAvg *100 / max<<"\t"<<(preservingCycleAvg+preservingCycleVar)*100 / max<<"\t"<<(preservingCycleAvg-preservingCycleVar)*100 / max<<endl;

		}
		abs.close();
		per.close();
	}
	cout << endl;
	return 1;
}


/**
 * function to make some statistics for the amount of reversals which are sorting, neutral,
 * preserving and preserving on the same cycle. this is done by generating random sequence
 * in all possible distances (to the idendity) and testing which reversals are are sorting ...
 * this is done multiple times and the medians are outputted results are 
 *@param dir outputfilename
 *@param e number of experiments
 *@param n length of the tested sequences
 */
int sortVsPreserve(string dir, int e, int n, bool unsig, vector<genom> &data, dstnc_inv *rdist){
	ofstream abs,			// files
		per;
	genom id(n, false);			// init the idendity permutation
	vector<pair<int,int> > reversals = id.getReversals();	// get all possible reversals
	unsigned max = reversals.size();			// maximal possible count of reversals = (n over 2) binomial coefficient
	string filename;								
	string filename_p;
	unsigned avg_distance=0;
		// init the filenames 
	filename = dir+"sortVsPreserve_n"+int2string(n)+".dat";
	filename_p = dir+"sortVsPreserve_n"+int2string(n)+"_per.dat";
	abs.open(filename.c_str());
	if(!abs.is_open()){
		cout << "could not open : "<<filename <<endl;
		exit(1);
	}
	per.open(filename_p.c_str());
	if(!per.is_open()){
		cout << "could not open : "<<filename <<endl;
		exit(1);
	}
	
	map<int, vector<float> > sorting,
		neutral,
		preserving,
		cycle,
		cycleUComp,
		nab,
		proper;

	for(unsigned i=0; i<data.size(); i++){
		cout << "computing "<<i+1<<"/"<<data.size()<<"\r";
		cout.flush();
		
		int dist;
		if(unsig){
			dist = 0;
			avg_distance+=rdist->calc(id,data[i]);
		}else{
			dist = rdist->calc(id,data[i]);
		}
		//~ cout << data[i]<<endl;

		vector<pair<int,int> > cycleReversals,
			cycleUCompReversals,
			sortingReversals,
			neutralReversals,
			nabReversals,
			preservingReversals,
			properReversals;

		preservingReversals = getPreservingReversals(data[i], id);
		nabReversals = data[i].getReversals_nonAdjacencyBreaking(id);
		cycleReversals = data[i].getReversalsSameCycle(id);
		cycleUCompReversals = data[i].getReversalsSameUnorientedComponent(id);
		cycleUCompReversals.insert(cycleUCompReversals.end(), cycleReversals.begin(), cycleReversals.end());
		
			// get the sorting reversals
		for(unsigned j=0; j< reversals.size(); j++){
			genom temp = data[i];
			reverse(temp, reversals[j]);
			if(rdist->calc(temp,id) < rdist->calc(data[i],id))
				sortingReversals.push_back(reversals[j]);
		}

			// get the neutral reversals
		for(unsigned j=0; j< reversals.size(); j++){
			genom temp = data[i];
			reverse(temp, reversals[j]);
			if(rdist->calc(temp,id) <= rdist->calc(data[i],id))
				neutralReversals.push_back(reversals[j]);
		}
		
			// get the proper reversals
		for(unsigned j=0; j< preservingReversals.size(); j++){
			genom temp = data[i];
			reverse(temp, preservingReversals[j]);
			if(rdist->calc(temp,id) <= rdist->calc(data[i],id))
				properReversals.push_back(reversals[j]);
		}

		sorting[dist].push_back(sortingReversals.size());
		neutral[dist].push_back(neutralReversals.size());
		preserving[dist].push_back(preservingReversals.size());
		cycle[dist].push_back(cycleReversals.size());
		cycleUComp[dist].push_back(cycleUCompReversals.size());
		nab[dist].push_back(nabReversals.size());
		proper[dist].push_back(properReversals.size());

	}


	avg_distance/=data.size();
	abs << "# rms := root mean square"<<endl;
	abs << "# average, rms, min and max numbers of "<<endl;
	abs << "# sorting-, favourable-, preserving-, proper-, same cycle-, samecycleandsameunorientedcomponent and nonadjacencybreaking reversals"<<endl;
	abs << "# in percent from the maximum "<<endl;
	abs << "# length of the genomes : "<<n << endl;
	abs << "# number of experiments : "<<data.size() << endl;
	abs << "# avg distance : "<<avg_distance<< endl;
	abs << "# 1 \t2 \t3 \t4 \t5 \t6 \t7 \t8 \t9 \t10 \t11 \t12 \t13 \t14 \t15 \t16 \t17 \t18 \t19 \t20 \t21 \t22 \t23 \t24 \t25 \t26 "<<endl;
	abs << "# d \ts_avg \ts_rms \ts_min \ts_max \tn_avg \tn_rms \tn_min \tn_max \tp_avg \tp_rms \tp_min \tp_max \tpr_avg \tpr_rms \tpr_min \tpr_max \tc_avg \tc_rms \tc_min \tc_max \tcuc_avg \tcuc_rms \tcuc_min \tcuc_max \tnab_avg \tnab_rms \tnab_min \tnab_max "<<endl;	
	
	per << "# rms := root mean square"<<endl;
	per << "# average, rms, min and max numbers of "<<endl;
	per << "# sorting-, favourable-, preserving-, proper-, same cycle-, samecycleandsameunorientedcomponent and nonadjacencybreaking reversals"<<endl;
	per << "# in percent from the maximum "<<endl;
	per << "# length of the genomes : "<<n << endl;
	per << "# number of experiments : "<<data.size() << endl;
	per << "# avg distance : "<<avg_distance<< endl;
	per << "# 1 \t2 \t3 \t4 \t5 \t6 \t7 \t8 \t9 \t10 \t11 \t12 \t13 \t14 \t15 \t16 \t17 \t18 \t19 \t20 \t21 \t22 \t23 \t24 \t25 \t26 "<<endl;
	per << "# d \ts_avg \ts_rms \ts_min \ts_max \tn_avg \tn_rms \tn_min \tn_max \tp_avg \tp_rms \tp_min \tp_max \tpr_avg \tpr_rms \tpr_min \tpr_max \tc_avg \tc_rms \tc_min \tc_max \tcuc_avg \tcuc_rms \tcuc_min \tcuc_max \tnab_avg \tnab_rms \tnab_min \tnab_max "<<endl;
	cout << endl;	
	int nr = 0;
	for(map<int, vector<float> >::iterator it=sorting.begin(); it!=sorting.end();it++){
		cout << "collecting "<< nr+1<<"/"<<sorting.size()<<"\r";
		nr++;
		cout.flush();
		float sortingAvg = 0, sortingVar =0,
			preservingAvg = 0, neutralVar =0,
			neutralAvg = 0, preservingVar =0,
			cycleAvg = 0, cycleVar=0,
			cycleUCompAvg = 0, cycleUCompVar=0,
			nabAvg = 0, nabVar=0,
			properAvg = 0, properVar = 0,
			sortingMin = 1000000, sortingMax = 0,
			preservingMin = 1000000, preservingMax = 0,
			neutralMin = 1000000, neutralMax = 0,
			cycleMin = 1000000, cycleMax = 0,
			cycleUCompMin = 1000000, cycleUCompMax = 0,
			nabMin = 1000000, nabMax = 0,
			properMin = 1000000, properMax = 0;
		unsigned runs = sorting[it->first].size();
		for(unsigned i=0; i< runs; i++){
				// summation
			sortingAvg += sorting[it->first][i];
			preservingAvg += preserving[it->first][i];
			neutralAvg += neutral[it->first][i];
			cycleAvg += cycle[it->first][i];
			cycleUCompAvg += cycleUComp[it->first][i];
			nabAvg += nab[it->first][i];
			properAvg += proper[it->first][i];

				// update min / max
			sortingMax = (sorting[it->first][i]>sortingMax) ? sorting[it->first][i] : sortingMax;
			neutralMax = (neutral[it->first][i]>neutralMax) ? neutral[it->first][i] : neutralMax;
			preservingMax = (preserving[it->first][i]>preservingMax) ? preserving[it->first][i] : preservingMax;
			cycleMax = (cycle[it->first][i]>cycleMax) ? cycle[it->first][i] : cycleMax;
			cycleUCompMax = (cycleUComp[it->first][i]>cycleUCompMax) ? cycleUComp[it->first][i] : cycleUCompMax;
			nabMax = (nab[it->first][i]>nabMax) ? nab[it->first][i] : nabMax;
			properMax = (proper[it->first][i]>properMax) ? proper[it->first][i] : properMax;
			
			sortingMin = (sorting[it->first][i]<sortingMin) ? sorting[it->first][i] : sortingMin;
			neutralMin = (neutral[it->first][i]<neutralMin) ? neutral[it->first][i] : neutralMin;
			preservingMin = (preserving[it->first][i]<preservingMin) ? preserving[it->first][i] : preservingMin;
			cycleMin = (cycleUComp[it->first][i]<cycleMin) ? cycleUComp[it->first][i] : cycleMin;
			cycleUCompMin = (cycleUComp[it->first][i]<cycleUCompMin) ? cycleUComp[it->first][i] : cycleUCompMin;
			nabMin = (nab[it->first][i]<nabMin) ? nab[it->first][i] : nabMin;
			properMin = (proper[it->first][i]<properMin) ? proper[it->first][i] : properMin;
		}
		
			// get the median
		sortingAvg /= runs;
		neutralAvg /= runs;
		preservingAvg /= runs;
		cycleAvg /= runs;
		cycleUCompAvg /= runs;
		nabAvg /= runs;
		properAvg /= runs;

			// get the variance
		for(unsigned i=0; i< runs; i++){
			sortingVar 		+= pow(sorting[it->first][i]-sortingAvg, 2);
			neutralVar 		+= pow(neutral[it->first][i]-neutralAvg, 2);
			preservingVar 	+= pow(preserving[it->first][i]-preservingAvg, 2);
			cycleVar 		+= pow(cycle[it->first][i]-cycleAvg, 2);
			cycleUCompVar 	+= pow(cycleUComp[it->first][i]-cycleUCompAvg, 2);
			nabVar 			+= pow(nab[it->first][i]-nabAvg, 2);
			properVar		+= pow(proper[it->first][i]-properAvg, 2);
		}
		
		sortingVar = sqrt(sortingVar / (runs-1));
		neutralVar = sqrt(neutralVar / (runs-1));
		preservingVar = sqrt(preservingVar / (runs-1));
		cycleVar = sqrt(cycleVar / (runs-1));
		cycleUCompVar = sqrt(cycleUCompVar / (runs-1));
		nabVar = sqrt(nabVar / (runs-1));
		properVar = sqrt(properVar / (runs-1));
			// output the absolute values
		abs 	<< it->first 	
				<<"\t"<<sortingAvg 		<<"\t"<<sortingVar		<<"\t"<<sortingMin 		<< "\t" << sortingMax
				<<"\t"<<neutralAvg		<<"\t"<<neutralVar		<<"\t"<<neutralMin 		<< "\t" << neutralMax
				<<"\t"<<preservingAvg	<<"\t"<<preservingVar	<<"\t"<<preservingMin 	<< "\t" << preservingMax
				<<"\t"<<properAvg		<<"\t"<<properVar		<<"\t"<<properMin 		<< "\t" << properMax
				<<"\t"<<cycleAvg		<<"\t"<<cycleVar		<<"\t"<<cycleMin 		<< "\t" << cycleMax
				<<"\t"<<cycleUCompAvg	<<"\t"<<cycleUCompVar	<<"\t"<<cycleUCompMin 	<< "\t" << cycleUCompMax
				<<"\t"<<nabAvg 		<<"\t"<<nabVar			<<"\t"<<nabMin 		<< "\t" << nabMax <<endl; 
			// output the percentage
		per  << it->first 	
			<<"\t"<<sortingAvg*100 / max	<<"\t"<<sortingVar*100 / max 	<<"\t"<< sortingMin*100 / max 	<< "\t" << sortingMax*100 / max
			<<"\t"<<neutralAvg*100 / max 	<<"\t"<<neutralVar*100 / max 	<<"\t"<< neutralMin*100 / max 	<< "\t" << neutralMax*100 / max
			<<"\t"<<preservingAvg*100 / max <<"\t"<<preservingVar*100 / max	<<"\t"<< preservingMin*100 / max<< "\t" << preservingMax*100 / max
			<<"\t"<<properAvg*100 / max 	<<"\t"<<properVar*100 / max		<<"\t"<< properMin*100 / max 	<< "\t" << properMax*100 / max
			<<"\t"<<cycleAvg *100 / max		<<"\t"<<cycleVar*100 / max		<<"\t"<< cycleMin*100 / max 	<< "\t" << cycleMax*100 / max
			<<"\t"<<cycleUCompAvg*100 / max	<<"\t"<<cycleUCompVar*100 / max	<<"\t"<< cycleUCompMin*100 / max<< "\t" << cycleUCompMax*100 / max
			<<"\t"<<nabAvg*100 / max 		<<"\t"<<nabVar*100 / max 		<<"\t"<< nabMin *100 / max		<< "\t" << nabMax*100 / max <<endl;

	}
	
	abs.close();
	per.close();
	cout << endl;
	return 1;
}

/**
 * function for counting the hurdle mergings possible at one permutation 
 * they are equal to the sorting and not preserving reversals
 *@param dir outputfilename
 *@param exp number of experiments
 *@param length length of the tested sequences
 *@param length length of the tested sequences
 */
int merging(string dir, int exp, int length, bool unsig, vector<genom> &data, dstnc_inv *rdist){
	unordered *rand_sce;
	vector<genom> trace;
	vector<float> prob(4,0);
	prob[0]=100;

	for (int l=0; l<length; l+=length/10){
		int found_cnt = 0,
			notfound_cnt = 0;
		genom id(l, false);			// init the idendity permutation	
		vector<pair<int,int> > reversals = id.getReversals();	// get all possible reversals		
		
		for (int e=0; e<exp; e++){
			int d = 0;
			vector<pair<int,int> > preservingReversals,
					sortingReversals;


//			genom r(l, 0, 0, length, false, true, 0, false);
			genom r( length, 0 );
			rand_sce = new rrrmt_rand( l, prob, r, trace, true, std::numeric_limits< unsigned >::max() );
			delete rand_sce;



			d = rdist->calc(r,id);
				// get the preserving
			preservingReversals = getPreservingReversals(r, id);
			vector< vector<int> > ci = getMaximalChains(r, id);
			
			//~ cout << "-------------"<<endl;
			//~ for (unsigned i=0; i<ci.size(); i++){
				//~ for(unsigned j=0; j<ci[i].size(); j++){
					//~ cout << ci[i][j] << " ";
				//~ }
				//~ cout << endl;
			//~ }

				// get the sorting
			for(unsigned i=0; i< reversals.size(); i++){
				genom temp = r;
				reverse(temp, reversals[i]);
				if(rdist->calc(temp,id) < (unsigned)d)
					sortingReversals.push_back(reversals[i]);
			}
			
			
			for (unsigned i=0; i<sortingReversals.size(); i++){
				pair<int,int> revsorting = sortingReversals[i];
				
				swap (revsorting.first, revsorting.second);
				if(find(preservingReversals.begin(), preservingReversals.end(), sortingReversals[i])== preservingReversals.end() ||
					find(preservingReversals.begin(), preservingReversals.end(), revsorting)== preservingReversals.end())
					found_cnt++;
				else 
					notfound_cnt++;
			}
			
		}
		cout << l << "\t"<< (float)found_cnt/exp<<"\t"<< (float)notfound_cnt/exp<< "\t"<<((float)found_cnt/exp) / ((float)notfound_cnt/exp) << endl;
	}
	
	return 1;
}

/**
 * function for comparison of interval & reversal distance
 *@param dir outputfilename
 *@param exp number of experiments
 *@param length length of the tested sequences
 */
int intervalVsReversalDistance(string dir, int exp, int length, bool unsig, vector<genom> &data, dstnc_inv *rdist){
	genom id(length, false);			// init the idendity permutation
	for(int dist=0; dist<=length; dist++){
		vector<float> idistances(exp);
		float idist =0, 
			idistVar=0,
			idistMin=1000000,
			idistMax=0;

		for (int j=0; j<exp; j++){
			genom r(length, false);
			//~ genom r("r", length, dist,0, 1, false, false, false);
			r.evolve(dist, 0);
			
			idistances[j] = intervalDistance(id, r);
			idist += idistances[j];
			
			if(idistances[j]<idistMin)
				idistMin = idistances[j];
			if(idistances[j]>idistMax)
				idistMax = idistances[j];
			
			//~ cout << id.distance(r) <<" " << id.intervalDistance(r)<<endl;
		}
		idist = idist/ exp;
		for(int i=0; i<exp; i++){
			idistVar = pow(idistances[i]-idist, 2);
		}
		idistVar = sqrt(idistVar / (exp-1));
		cout << dist <<" "<< idist << " "<< idistVar<< " "<<idistMin<<" "<< idistMax<<endl;
	}
	return 1;
}


/**
 * function for comparison of number of real inversions & reversal distance
 *@param dir outputfilename
 *@param exp number of experiments
 *@param length length of the tested sequences
 */
int reversalsVsReversalDistance(string dir, int exp, int length, bool unsig, vector<genom> &data, dstnc_inv *rdist){

	genom id(length, false);			// init the idendity permutation
	//~ unsigned rdist = 0,
			//~ prdist = 0;
	//~ for (unsigned i=0; i<data.size(); i++){
		//~ cout << id.distance(data[i])<<" "<<id.preserving_reversal_distance(data[i])<<endl;
		//~ rdist += id.distance(data[i]);
		//~ prdist += id.preserving_reversal_distance(data[i]);
	//~ }

	//~ cout << rdist<<" "<<prdist<<endl;
	//~ cout << data.size()<<endl;
	for(int dist=0; dist<=length; dist+=length/10){
		unsigned diff_cnt = 0;
		float rdist = 0,
			prdist = 0;

		for (int j=0; j<exp; j++){

			genom r(length, false);
			//~ genom r("r", length, dist,0, 1, false, false, false);
			r.evolve(dist, 0);

			//~ r.distance(id);
			//~ cout << r.distance(id)<< " - "<<r.preserving_reversal_distance(id)<<endl;
			//~ if(r.distance(id)!=r.preserving_reversal_distance(id))
				//~ diff_cnt++;
			
			//~ rdist += r.distance(id);
			printf("reversalsVsReversalDistance: a lazy guy commented out preserving_reversal_distance .. \n");
			printf("           so.. this is not usable until some body reenables this\n");
//			prdist += preserving_reversal_distance(r, id, 0);
			//~ cout << id.distance(r) <<" " << id.intervalDistance(r)<<endl;
		}
		rdist /= exp;
		prdist /= exp;
		// cout << (float)dist/length <<" "<< (float)(idist - dist)/length<<endl;
		cout << (float)dist/length <<" "<< (float)rdist /length<<" "<< (float)prdist /length<<" > " <<diff_cnt<<endl;

	}
	return 1;
}

/**
 * compare the number of sorting reversals for condensed and uncondensed genomes
 */
/*
int condensed_uncondensed(string dir, int e, int n, bool unsig, vector<genom> &data, hdata &hd){
	vector<genom> cond(3, genom(n, 0)), 
		uncond(3, genom(n, 0));
	hdata hd_cond;
	vector< pair<int, int> > revs,
		revs_cond;
	int *condense_succ, 	// data for condensation
		*condense_decode;
	int dist_before, dist_after;
	map<int, vector<float> > sorting_cond, 
		sorting_uncond;
	float s;

	genom id(n, false);
	revs = id.getReversals();
	
	for(unsigned i=0; i<data.size(); i++){
		cerr <<"\r"<< i+1 <<"/"<<data.size();
		cout.flush();
		
		cond[0] = uncond[0] = id;
		cond[1] = uncond[1] = data[i];
		cond[2] = uncond[2] = id;
		condense_succ = ( int * ) malloc ( ( 2 * n + 1 ) * sizeof ( int ) );
		if ( condense_succ == ( int * ) NULL )
			fprintf ( stderr, "ERROR: condense_succ NULL\n" );
		condense_decode = ( int * ) malloc ( ( 2 * n + 1 ) * sizeof ( int ) );
		if ( condense_decode == ( int * ) NULL )
			fprintf ( stderr, "ERROR: condense_decode NULL\n" );

		condense (cond, 0, condense_succ, condense_decode, 1);
		init_data(hd, cond[0].size(), 0);
		
		dist_before = cond[0].distance(cond[1], hd);
		s = 0;
		if(dist_before>0){
			revs_cond = cond[0].getReversals();
			
			for(unsigned j=0; j<revs_cond.size(); j++){
				genom temp = cond[0];
				reverse(temp, revs_cond[j]);
				dist_after = temp.distance(cond[1], hd);
				
				if(dist_after < dist_before){
					s+=1;
				}
			}
			sorting_cond[dist_before].push_back(s);

			revs_cond.clear();
			s = 0;
			dist_before = uncond[0].distance(uncond[1], hd);
			for(unsigned j=0; j<revs.size(); j++){
				genom temp = uncond[0];
				reverse(temp, revs[j]);
				dist_after = temp.distance(uncond[1], hd);
				
				if(dist_after < dist_before){
					s += 1;
				}
			}
			sorting_uncond[dist_before].push_back(s);
		}
		
		
		free_data(hd);
		free(condense_succ);
		free(condense_decode);
	}
	cout<<endl;
	float sorting_cond_avg = 0,
		sorting_uncond_avg = 0;
	
	for(map<int, vector<float> >::iterator it=sorting_cond.begin(); it!=sorting_cond.end();it++){
		unsigned diff_instances = 0;
		for(unsigned i=0; i<it->second.size(); i++){
			sorting_cond_avg += it->second[i];
			if(it->second[i] !=sorting_uncond[it->first][i]) {
				diff_instances++;
			}
		}
		
		for(unsigned i=0; i<sorting_uncond[it->first].size(); i++)
			sorting_uncond_avg += sorting_uncond[it->first][i];
	
		cout << it->first
			<< " " << diff_instances
			<< " " << sorting_cond_avg / it->second.size()
			<< " " << sorting_uncond_avg / sorting_uncond[it->first].size()<< endl;
	}
	
	
	return 1;
}*/
