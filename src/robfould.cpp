#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#include "tree.hpp"

using namespace std;

void usage();

int main(int argc, char *argv[]){
//	dstnc *dst = NULL;
	ifstream file;
	string line;
	vector<phy<unsigned> > trees;
	vector<string> fnames;

		// get and check parameters
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'f': {i++; fnames.push_back( argv[i] ); break;}
			case 'h': {usage(); break;}
			default:{
				fnames.push_back( argv[i] );
				break;
			}
		}
	}

	if (fnames.size() == 0){
		usage();
	}

	for(unsigned i=0; i<fnames.size(); i++){
		file.open (fnames[i].c_str(), ifstream::in);
		if(!file.is_open()){
			cout << "could not open "<< fnames[i]<<endl;
			usage();
		}

	    while ( file.good() ){
			getline(file, line);
			if(line[0] == '('){
//				 cout << line << endl;
				trees.push_back( phy<unsigned>(line) );
//				cout << "# "<< trees.back()<<endl;
				// break;
			}
	    }
		file.close();
		file.clear();
	}

	for(unsigned i=1; i<trees.size(); i++){
		pair<unsigned,unsigned> ds = trees[0].splitdiff(trees[i]);

		cout << (ds.first+ds.second) / 2.0<<" " ;
		cout << ds.first <<" " <<ds.second <<endl;
	}
}

void usage(){
	cout << "robfould [-f filename]*"<<endl;
	exit(0);
}
