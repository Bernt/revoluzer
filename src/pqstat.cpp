/** @file pqstat.cpp
 * program to compute properties of pqtrees of pairs or triples
 * of genomes in a file ( number of p nodes, number of prime-prime edges )
 */

#include <iostream>
#include <limits>

#include "common.hpp"
#include "genom.hpp"
#include "helpers.hpp"
#include "io.hpp"

using namespace std;

/**
 * print statistics of a given interval tree
 * columns:
 * - has a prime node or not (1/0)
 * - number of prime nodes
 * - number of prime prime edges
 * @param[in] p the pnode
 */
void pnode_stats(itnode *itnode);

/**
 * print usage information
 */
void usage();

int main(int argc, char *argv[]) {
	bool triples = false,	// look at all triples of the input genomes
		pairs = false;		// look at all pairs of the input genomes
	itnode *iroot;
	int n,				// length of the genomes
		circular = 0;	// circular genomes (1) / linear (0)
	string fname;		// input file name
	vector<genom> genomes,	// the genomes in the file
		triple;
	vector<string> names,	// the names of the genomes in the file
		nmap;				// names of the element

	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'c': {circular = 1; break;}
			case 'f': {i++; fname = argv[i]; break;}
			case 'p': {pairs = true; break;}
			case 't': {triples = true; break;}
			default:{
				cout << "unknown parameter "<<argv[i]<<endl;
				usage();
			}
		}
	}


	if(fname == ""){
		cout << "no file given"<<endl;
		usage();
	}

	read_genomes(fname, genomes, names, circular, nmap);	// read the genom file
	if(genomes.size() == 0){
		cout << "no genomes in the file"<<endl;
		usage();
	}

//	cout << "# inst_pnodes inst_pcomps nrrmp nrcomb inst_minlen inst_avglen inst_maxlen ";
//	cout << "inst_mindeg inst_avgdeg inst_max_degree inst_ppedges comp_mindeg comp_avgdeg comp_max_degree comp_ppedges "<<endl;
//  cout << "# depth: maximal depth of the tree"<<endl;
//	cout << "# pnodes: # p nodes"<<endl;
//	cout << "# pcomp : # p components"<<endl;
//	cout << "# nrrmp : # of rmps to solve"<<endl;
//	cout << "# nrcomb: # of combinations to test"<<endl;
//	cout << "# deg   : p degree (number of p childs + p parent)"<<endl;
//	cout << "# len   : size of an rmp, i.e. number of p childs "<<endl;
//	cout << "# ppedges : number of ppedges "<<endl;
//	cout << "# inst .. per instance"<<endl;
//	cout << "# comp .. per component"<<endl;

	n = genomes[0].size();

	if( pairs ){	// check all pairs
		vector<int> haslnode(genomes.size(), 0); // mark if a genome has at least one comp. with a linear node
//		unsigned acc = 0;
		list<itnode *> pnode;
		for(unsigned i=0; i<genomes.size(); i++){
			for(unsigned j=i+1; j<genomes.size(); j++){
				interval_tree(genomes[i], genomes[j], n, &iroot);
//				cout << interval_tree_depth( iroot ) <<" ";
//				cout << names[i] << " "<<names[j]<<endl;
//				pnode_stats(iroot);
				interval_tree_primenodes(iroot, pnode);
				if( pnode.size() == 0){
					haslnode[i]++;
					haslnode[j]++;
				}
				pnode.clear();
				interval_tree_free(iroot);
			}
		}
//		for( unsigned i=0; i<haslnode.size(); i++ ){
//			if( haslnode[i] )
//				acc++;
//		}
//		cerr <<"lnodeacc "<< 100*(float)acc/(float)genomes.size()<<endl;
		for( unsigned i=0; i<haslnode.size(); i++ )
			cout << haslnode[i]<<endl;
	}else if(triples){ 				// check all triples
		for(unsigned i=0; i<genomes.size(); i++){
			for(unsigned j=i+1; j<genomes.size(); j++){
				for(unsigned k=j+1; k<genomes.size(); k++){

					triple.push_back( genomes[i] );
					triple.push_back( genomes[j] );
					triple.push_back( genomes[k] );

					interval_tree(triple, n, &iroot);
					pnode_stats(iroot);
					interval_tree_free(iroot);
					triple.clear();
				}
			}
		}
	}else{			// use the complete data
		interval_tree(genomes, n, &iroot);
		pnode_stats(iroot);
		interval_tree_free(iroot);
	}

	return 1;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// p: triple instances for which the tree has at least one prime node;
// q: number of prime nodes;
// c: number of prime node subtrees;
// -: min, avg, max number of prime nodes in the primenode subtrees
//    min, avg, max pnode degree
//
// r: number of oRMPs to be solved;
// t: number of 3-sign combinations
// l: maximal number of children of the prime nodes;
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void pnode_stats(itnode *p){
	list<itnode *> pnode,			// the pnodes of a interval tree
		lnode;
	vector<vector<itnode *> > pcomp; // the components of pnodes
	double avg_len = 0,				// average length of a pnode (nr of childs)
		avg_deg = 0;				// average pnode degree of a pnode
	int deg = 0,					// degree of a pnode (nr of connected pnodes)
		min_deg = std::numeric_limits< int >::max(), 			// min degree
		max_deg = 0,				// max degree
		ppedges = 0,				// number of pp edges
		min_len = std::numeric_limits< int >::max(),			// min ..
		max_len = 0,				// .. and max of pnode length
		nr_rmp = 0,					// number of median problems to solve
		nr_comb = 0;				// number of possible combinations
	vector<double> cavg_deg;		// the same for each component
	vector<int> cmin_deg, cmax_deg, // ..
		cppedges; 					// ..

	interval_tree_linearnodes(p, lnode);
	interval_tree_primenodes(p, pnode);
	interval_tree_pnode_components(p, pcomp);

		// iterate over the pnodes and compute
		// - min, avg, and max pnode degree
		// - min, avg, and max oRMP length
	for( list<itnode *>::iterator it = pnode.begin(); it!=pnode.end(); it++ ){
			// get the pnode degree of the current node
		deg = 0;
		for (unsigned j=0; j<(*it)->children.size(); j++){
			if((*it)->children[j]->type == PRI)
				deg++;
		}
		if( (*it)->parent != NULL && (*it)->parent->type != LIN )
			deg++;

		avg_deg += deg;
		min_deg = min(min_deg, deg);
		max_deg = max(max_deg, deg);

		if( (*it)->parent != NULL && (*it)->parent->type != LIN)
			ppedges++;

			// length of the oRMPs
		avg_len += (*it)->children.size();
		min_len = min((int)(*it)->children.size(), min_len);
		max_len = max((int)(*it)->children.size(), max_len);
	}
	if(pnode.size() > 0){
		avg_deg /= pnode.size();
		avg_len /= pnode.size();
	}else{
		avg_deg = 0;
		avg_len = 0;
		min_deg = 0;
		min_len = 0;
	}

		// for each component:
		// - min, avg, and max pnode degree
		// - number of oRMPs to solve
		// - number of P-P edges
		// - number of combinations to test
	cavg_deg = vector<double>(pcomp.size(), 0);
	cmin_deg = vector<int>(pcomp.size(), std::numeric_limits< int >::max());
	cmax_deg = vector<int>(pcomp.size(), 0);
	cppedges = vector<int>(pcomp.size(), 0);
	for(unsigned k=0; k<pcomp.size(); k++){
		for (unsigned i=0; i<pcomp[k].size(); i++){
			deg = 0;
			for (unsigned j=0; j<pcomp[k][i]->children.size(); j++){
				if(pcomp[k][i]->children[j]->type == PRI)
					deg++;
			}
			if( pcomp[k][i]->parent != NULL && pcomp[k][i]->parent->type != LIN )
				deg++;

			cavg_deg[k] += deg;
			cmin_deg[k] = min(cmin_deg[k], deg);
			cmax_deg[k] = max(cmax_deg[k], deg);

			nr_rmp += pow( 4, deg );

			if( pcomp[k][i]->parent != NULL && pcomp[k][i]->parent->type != LIN){
				cppedges[k]++;
			}
		}
		if(pcomp[k].size() > 0)
			cavg_deg[k] /= pcomp[k].size();
		else
			cavg_deg[k] = 0;


		nr_comb += pow(4, cppedges[k]);
	}
//	" "<< lnode.size()
//	        1                       2                    3              4
	cout << pnode.size() << " "<< pcomp.size() << " "<<nr_rmp << " "<<nr_comb;
//	              5             6             7
	cout << " "<< min_len<<" "<<avg_len<<" "<<max_len;
//	              8             9             10
	cout << " "<< min_deg<<" "<<avg_deg<<" "<<max_deg<<" "<<ppedges;

	for(unsigned k=0; k<pcomp.size(); k++){
		cout << " "<<cmin_deg[k]<<" "<<cavg_deg[k]<<" "<<cmax_deg[k]<<" "<<cppedges[k];
	}
	cout << endl;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cout << "get statistics for the pqtree of the input genomes"<<endl;
	cout << "usage pqstat -f file [-c] [-t] [-p]"<<endl;
	cout << "  -c: handle genomes as circular (default: linear)"<<endl;
	cout << "  -t: look at the pqtrees of all triples (default: off)"<<endl;
	cout << "  -p: look at the pqtrees of all pairs (default: off)"<<endl;
	exit(1);
}
