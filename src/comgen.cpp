/*
 * comgen.cpp
 *
 * remove genes that are not common to all given gene orders
 *
 *  Created on: Apr 19, 2012
 *      Author: maze
 */
#include <algorithm>
#include <cstdlib>
#include <getopt.h>
#include <iostream>

#include "io.hpp"

using namespace std;

/**
 * read command line options
 * @param[in] argc number of arguments
 * @param[in] argv the arguments
 * @param[out] fname input genome filename
 * @param[in,out] circular the circularity of the genomes
 */
void getoptions( int argc, char *argv[], string &fname, bool &circular);

/**
 * print usage information and exit
 */
void usage();

int main(int argc, char *argv[]) {
	bool circular = true;
	string fname;
	vector<genom> genomes;
	map<int,unsigned> genecnt;
	vector<string> names,
		nmap;

	// get and check parameters
	getoptions( argc, argv, fname, circular);


	// read the genomes (no duplicates allowed)
	read_genomes(fname, genomes, names, circular, nmap, true, false);

	// determine the number of times each gene appears
	for( unsigned i=0; i<genomes.size(); i++ ){
		for( unsigned j=0; j<genomes[i].size(); j++ ){
			if( genecnt.find(abs(genomes[i][j])) == genecnt.end()){
				genecnt[ abs(genomes[i][j]) ] = 0;
			}
			genecnt[ abs(genomes[i][j]) ]++;
		}
	}

	// remove genes appearing not in all genomes from the count map
	for( map<int,unsigned>::iterator it = genecnt.begin(); it!=genecnt.end(); ){
		if( it->second == genomes.size() ){
			cerr << "keep "; print_element(it->first, cerr, 1, "", &nmap);cerr<< endl;
			++it;

		}else{
			cerr << "delete "; print_element(it->first, cerr, 1, "", &nmap);cerr<< endl;
			genecnt.erase( it++ );
		}
	}

	// remove genes appearing not in the count map from the genomes
	for( unsigned i=0; i<genomes.size(); i++ ){
		vector<int> chr = genomes[i].getChromosom();
		for(vector<int>::iterator it=chr.begin(); it!=chr.end(); ){
			if( genecnt.find( abs(*it) ) == genecnt.end() ){
				chr.erase( it++ );
			}else{
				++it;
			}
		}
		genomes[i].setChromosom(chr);

		cout << ">"<<names[i]<<endl;
		cout << genomes[i]<<endl;
	}



}

void getoptions( int argc, char *argv[], string &fname, bool &circular){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"file",    required_argument, 0, 'f'},
            {0, 0, 0, 0}
		};
        int option_index = 0;	// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:l",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case 'l':
				circular = false;;
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
				break;
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}

		// check parameters
	if(fname == ""){
		cerr << "no genome file given ! "<<endl;
		usage();
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cerr << "comgen -f FILE [OPTIONS]"<<endl;
	cerr << "-l: assume linear genomes"<<endl;
	exit( EXIT_FAILURE );
}
