/*
 * test.cpp
 *
 * I file I use for tests.
 *
 *  Created on: May 17, 2010
 *      Author: maze
 */

#include "helpers.hpp"
#include "io.hpp"
#include "costfoo.hpp"
#include "crex.hpp"
#include "genom.hpp"
#include "geneset.hpp"

#include <algorithm>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <limits>
#include <limits.h>

using namespace std;

int main(int argc, char *argv[]){
	vector<genom> genomes, ogenomes;
	vector<string> names,
		nmap;
	vector<vector<int> > scenarios;

	read_genomes(argv[1], genomes, names, true, nmap, false);

	ogenomes = genomes;
// 	cout << genomes[0]<<endl;
//	cout << genomes[1]<<endl;

	for(unsigned i=0; i<genomes[1].size(); i++){
		cout << "\""; print_element(genomes[1][i], cout, 1, "", &nmap); cout<<"\",";
	}
cout << endl;
	for(int n=0; n<(int)genomes[0].size(); n++){
		vector<int> inv = genomes[0].inverse( false);

		// cout << tdrl_distance( genomes[0], genomes[1] )<<endl;
		scenarios =  tdrl_sorting_all( genomes[0], genomes[1] );
		if( scenarios.size() != 1 ){
			cerr << "!= 1 scenario"<<endl;
			exit(EXIT_FAILURE);
		}

//			cout << ogenomes[1]<<endl;

		int d = scenarios[0][abs(genomes[0][inv[abs(ogenomes[1][0])]])];
		for( int p=0; p<genomes[0].size(); p++ ){
			char c;
			int l = nmap[abs(ogenomes[1][p])].length();
			if( scenarios[0][abs(genomes[0][inv[abs(ogenomes[1][p])]])] == 0 ){
				c='0';
			}else{
				c='1';
			}
			if( ogenomes[1][p] < 0 )
				l++;


			print_element( ogenomes[1][p], cout, 1, "", &nmap ); cout << " ";
			print_element( genomes[0][0], cout, 1, "", &nmap ); cout << " ";
			cout<< c << endl;

//			for(unsigned x=0; x<l; x++)
//				cout << c;
// 			cout << " ";
//			cout << c<<" ";
			// print_tdrl( scenarios[i], &nmap);
		}
//		print_element( genomes[0][0], cout, 1, "", &nmap );
//		cout <<endl;

		genomes[0].normalize(genomes[1][1]);
		genomes[1].normalize(genomes[1][1]);
	}

/*	for(int n=0; n<(int)genomes[0].size(); n++){
		for( int m=0; m<(int)genomes[0].size(); m++){


			genomes[0].normalize(ogenomes[0][n]);
			genomes[1].normalize(ogenomes[1][m]);

//			cout << genomes[0]<<endl;
//			cout << genomes[1]<<endl;
			vector<int> inv = genomes[0].inverse( false);

			if( tdrl_distance( genomes[0], genomes[1]) > 1 ){
				continue;
			}
			scenarios =  tdrl_sorting_all( genomes[0], genomes[1] );
			if( scenarios.size() != 1 ){
				cerr << "!= 1 scenario"<<endl;
				exit(0);
			}

	//			cout << ogenomes[1]<<endl;

			int d = scenarios[0][abs(genomes[0][inv[abs(ogenomes[1][0])]])];

			for( int p=0; p<genomes[0].size(); p++ ){
				char c;
				int l = nmap[abs(ogenomes[1][p])].length();
				if( d-scenarios[0][abs(genomes[0][inv[abs(ogenomes[1][p])]])] == 0 ){
					c='0';
				}else{
					c='1';
				}
				if( ogenomes[1][p] < 0 )
					l++;

				for(unsigned x=0; x<l; x++)
					cout << c;
				cout << " ";
				// print_tdrl( scenarios[i], &nmap);
			}
//			print_element( ogenomes[0][n], cout, 1, "", &nmap );
//			cout << " - ";
//			print_element( ogenomes[1][m], cout, 1, "", &nmap );
			cout <<endl;

		}
	}
*/

	return EXIT_SUCCESS;
}
