/**
 * program to search for 'interesting' tdrls in a dataset
 * i.e. positive or negative prime permutations with #chains = 2^x
 * @author Matthias Bernt
 */

#include <algorithm>
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <math.h>

#include "common.hpp"
#include "dstnc_com.hpp"
#include "distmat.hpp"
#include "genom.hpp"
#include "helpers.hpp"
#include "io.hpp"
#include "tdl.hpp"
#include "tdlmedian.hpp"
#include "dstnc_rt.hpp"



using namespace std;

ofstream dot;



/**
 * count the number of alternations (select in 1st/2nd copy) of a given tdrl in g
 */
unsigned tdrl_alternation_cnt( const vector<int> &tdrl, const genom &g ){
	unsigned alt=0;
	vector<int> ginv = g.inverse(),
		gtdrl( tdrl.size()-1, INT_MAX );

	for( unsigned i=1; i<tdrl.size(); i++ ){
		gtdrl[ abs(ginv[i]) ] = tdrl[ i ];
	}
	for( unsigned i=1; i<gtdrl.size(); i++){
		if( gtdrl[i-1] != gtdrl[i] ){
			alt++;
		}
	}
	return alt;
}

/**
 * count the number of alternations (select in 1st/2nd copy) of a given tdrl scenario in g
 */
unsigned tdrl_alternation_cnt( const vector<vector<int> > &tdrls, const genom &g ){
	genom tmpg = g;
	unsigned alt=0;


	for( unsigned i=0; i<tdrls.size(); i++ ){
		alt += tdrl_alternation_cnt(tdrls[i], tmpg);
		tdrl_apply(tmpg, tdrls[i]);
	}
	return alt;
}

/**
 * print some usage information and quit
 */
void usage(void);

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void getoptions( int argc, char *argv[], string &fname, int &circular,
		bool &pq){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"file",	required_argument,	0, 'f'},
			{"help",    no_argument,    0, 'h'},
			{"linear",	no_argument,	0, 'l'},
			{"pq",		no_argument,	0, 'p'},
            {0, 0, 0, 0}
		};

        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:hlp",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'h':
				usage();
				break;
			case 'l':
				circular = 0;
				break;
			case 'p':
				pq = true;
				break;

			case '?':
				cerr << "?"<<endl;
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

	/* Print any remaining command line arguments (not options). */
    if (optind < argc){
    	cerr << "non-option ARGV-elements: ";
    	while (optind < argc)
    		cerr << argv[optind++]<<" ";
    	cerr << endl;
	}
}



/**
 * @param[in] x a genome
 * @param[in] y a genome
 * @param[in] xydist some distance from x to y
 * @param[in] xmind smallest distance from x to any other genome
 * @param[in] ymind smallest distance from y to any other genome
 */
void analyse1(const genom &x, const genom &y, string xname, string yname){
		// variables for TDRL median solver
//	vector<genom> mp, medians;
//	int mediancnt,tdlmedianscore;

	int cxy, tdrldxy,	// chains, tdrl dist from x to y
		cyx, tdrldyx, 	// chains, tdrl dist from y to x
		trnspd,			// transposition distance
		mediancnt, tdlmedianscore;
//	int *xptr,
//		*yptr;
	tdl_bbmedian *tdlsolver;
	vector<genom> mp, medians;
//	weightedbb::WeightedBB *weightedbb;
	dstnc *tpdfoo;
	dstnc *rdist;

//	print_name( xname, cout);
//	cout <<" ";
//	print_name( yname, cout);
//	cout << " ";

	cxy = tdrl_chaincnt( x, y );
	tdrldxy = tdrl_distance( cxy );

	cyx = tdrl_chaincnt( y, x );
	tdrldyx = tdrl_distance( cyx );

	if( min( tdrldxy, tdrldyx) > 3 ){
		return;
		cout << "# "<<cxy << " " << tdrldxy <<"\t";
		cout << "# "<<cyx << " " << tdrldyx <<"\t"<<endl;
		return;
	}

	cout << cxy << " " << tdrldxy <<"\t";
	cout << cyx << " " << tdrldyx <<"\t";

//	xptr = x.get_pointer();
//	yptr = y.get_pointer();

//	weightedbb = new weightedbb::WeightedBB();
//	weightedbb->setHeapLimit(250000000 / x.size());
//	weightedbb->setWeights(1, 1, 0);
//	weightedbb->setPermutations(x.size(), xptr, yptr);
//	weightedbb->sort();
//	trnspd = weightedbb->getDistance();
//	delete weightedbb;
	rdist = new dstnc_inv( x.size(), false );
	cout << rdist->calc(x,y) <<" ";
	delete rdist;

	tpdfoo = new dstnc_rt(x.size(), false, 1, 1, 1);
	trnspd = tpdfoo->calc( x, y );
	delete tpdfoo;


	cout << trnspd << " ";


	tdlsolver = new tdl_bbmedian();
//	if( ppow(tdrldxy)-cxy == 0 || ppow(tdrldyx)-cyx == 0 ){
//		mp.push_back(x);
//		mp.push_back(y);
//		tdlsolver->solve(mp, true, NO_BOUND, NO_BOUND, mediancnt, tdlmedianscore, medians);
//		mp.clear();
//	}else{
		tdlmedianscore = INT_MAX;
		mediancnt = INT_MAX;
//	}

	delete tdlsolver;
	cout << tdlmedianscore<< " "<< mediancnt<<" "<<x.size()<<" ";

	if( tdrldxy <= tdrldyx ){
		print_name(xname, dot,false);
		dot << " -> ";
		print_name(yname, dot,false);
		dot << " [label=\""<< cxy <<"\"];"<<endl;
	}
	if( tdrldyx <= tdrldxy ){
		print_name(yname, dot,false);
		dot << " -> ";
		print_name(xname, dot,false);
		dot << " [label=\""<< cyx <<"\"];"<<endl;
	}

	print_name(xname, cout);cout << " ";
	print_name(yname, cout);cout << " ";
	cout << endl;
}

void its1(const vector<genom> &genomes, const vector<string> &names, bool pq, int n){
	genom qperm,
		g,
		id;
	itnode *iroot = NULL;
	list<itnode *> nodes;
	vector<vector<unsigned> > distmat;	// distance matrix
	vector<int> mind;				// minimal distance for each perm


////			    genomes, int conserved, int common, int pair_pd, int sign, int interval, int bp, int circular, hdata &hd, vector<vector<int> > &dist_mat
//	distance_matrix(genomes, 0,             1,          0,           0,        1,            0,      0,            hd,                             distmat);
	dstnc *dist;
	dist = new dstnc_com( n, true );
	distmat = distance_matrix( genomes, dist );
//	distmat = vector<vector<int> >(genomes.size(), vector<int>(genomes.size(), 1));
	mind = vector<int>(genomes.size(), INT_MAX);
	for( unsigned i=0; i<genomes.size(); i++ ){
		for(unsigned j=0; j<genomes.size(); j++){
			if( i==j )
				continue;
			if( distmat[i][j] > 0 && distmat[i][j] < mind[i] )
				mind[i] = distmat[i][j];
		}
	}

	for( unsigned i=0; i<genomes.size(); i++ ){
		for(unsigned j=i+1; j<genomes.size(); j++){
			if( genomes[i] == genomes[j] ){
				continue;
			}

			g = genomes[j].identify_g(genomes[i]);
			id = genom( g.size(), 0 );

			if ( !pq ){
				if( !g.isneg() and !g.ispos() ){
					continue;
				}
				if( g.isneg() ){
					reverse(g, 0, g.size()-1);
				}

				if( distmat[i][j] > mind[i] || distmat[i][j] > mind[j] ){
					continue;
				}

				analyse1( g, id, names[i], names[j] );
			}
			else{
				interval_tree(genomes[i], genomes[j], n, &iroot);
				interval_tree_primenodes(iroot, nodes);
//				cout << "pnodes "<<nodes.size()<<endl;
				for( list<itnode*>::iterator it=nodes.begin(); it!=nodes.end(); it++ ){
					quotient_permutation(*it, g, qperm, 0, true);
					id = genom( qperm.size(), 0 );

					if( !qperm.ispos() && !qperm.isneg() )
						continue;
					if( qperm.isneg() ){
						reverse(qperm, 0, qperm.size()-1);
					}
					analyse1( qperm, id, names[i], names[j] );
				}
				interval_tree_free(iroot);
				nodes.clear();
				iroot = NULL;
			}
		}
	}
}

// *******************************************************************************************
// init the intervals
// *******************************************************************************************
void init_characters(const vector<genom > &genomes, vector<set<int> > &characters,
		map< vector<int>, int> &chr_idx ){

	vector<int> tmpset;
	vector<int>::iterator new_end;

	chr_idx.clear();
	characters.clear();

	for(unsigned i=0; i<genomes.size(); i++){

		for(unsigned j=0; j<genomes[i].size(); j++){
			tmpset.clear();

			unsigned k=j;
			while( true ){
				if( k>=genomes[i].size() && j==k%genomes[i].size() ){
					break;
				}

				tmpset.push_back( abs( genomes[i][k%genomes[i].size()] ) );

					// exclude trivial sets of size 1
//				if(tmpset.size() < 2){
//					k++;
//					continue;
//				}

				sort(tmpset.begin(), tmpset.end());

					// if the set is not yet in the map -> insert
				if( chr_idx.find(tmpset) == chr_idx.end() ){
					chr_idx[tmpset] = characters.size();
					characters.push_back( set<int>() );
				}
				characters[chr_idx[tmpset]].insert(i);
				k++;
 			}
		}
	}
}

void its2(const vector<genom> &genomes, const vector<string> &names, int n){
	genom qperm, id,				// quotient permutation and corresponding identity
		g,							// identified genome
		f,cf;							// a front of a node
	list<itnode *> pnode;			// the prime nodes of a sit
	map< vector<int>, int> itvidx; 	// mapping from intervals to index in the itvprs vector
	unsigned pnodecnt = 0,			// number of checked primenodes
		negpnodecnt = 0,			// number of checked all negative primenodes
		pospnodecnt = 0,			// number of checked all positive primenodes
		pospnodeneeecnt = 0,
		pospnodeeeecnt = 0;
	vector<string> *nmap;
	itnode* sit; 					// strong interval trees for all pairs
	vector<set<int> > itv; 			// interval presence itvprs[i]: interval i present in the genomes in the set
	map<vector<int>, set<set<int> > > pitv;	// intervals which are a prime node in at least one pairwise comparison and the pairs

	nmap = genomes[0].get_nmap();

	// get the intervals of the permutations and store which permutation has which interval
	cerr << "enumerating intervals"<<endl;
	init_characters(genomes, itv, itvidx );

	cerr << "searching prime nodes of interest"<<endl;

	for( int i=0; i<genomes.size(); i++ ){
		for( unsigned j=i+1; j<genomes.size(); j++ ){
			interval_tree(genomes[i], genomes[j], n, &sit);

			// get prime nodes
			interval_tree_primenodes(sit, pnode);
			if( pnode.size() == 0 )
				continue;

			pnodecnt += pnode.size();
			g = genomes[j].identify_g(genomes[i]);

			// iterate over prime nodes with pos / neg quotient permutation
			// - get the front of the prime node
			// - determine the set of species which have the interval corresponding to the prime node
			//   and all intervals corresponding to the children
			// - get the sub-genomes corresponding to the interval
			// - get the
			for( list<itnode*>::iterator it=pnode.begin(); it!=pnode.end(); it++ ){
				quotient_permutation(*it, g, qperm, 0, true);
				id = genom( qperm.size(), 0 );

				if( !qperm.ispos() && !qperm.isneg() )
					continue;

				int cxy, cyx, tdrldxy, tdrldyx;
				cxy = tdrl_chaincnt( qperm, id );
				tdrldxy = tdrl_distance( cxy );
				cyx = tdrl_chaincnt( id, qperm );
				tdrldyx = tdrl_distance( cyx );

//				if( tdrldxy != 1 && tdrldyx != 1 )
//					continue;

				if( qperm.ispos() ){
					pospnodecnt++;
				}if( qperm.isneg() ){
					negpnodecnt++;
					reverse(qperm, 0, qperm.size()-1);
				}


				set<int> iset,
					tmpset;
//				itnode *tmpsit1, *tmpsit2;
				vector<genom> subgenomes;
				vector<string> subnames;
				bool eee = false;	// easier explanation exists?

				// get the species which have this interval
				front(*it, genomes[i], f);
				f.set_nmap( genomes[i].get_nmap() );
				for( unsigned c=0; c<f.chromosom.size(); c++ ){
					if( f.chromosom[c] < 0 ){
						f.chromosom[c]*=-1;
					}
				}
				sort(f.chromosom.begin(), f.chromosom.end());
				iset = itv[itvidx[ f.chromosom ]];

				// intersect with the sets of species which have the child intervals
//				for( unsigned c=0; c<(*it)->children.size(); c++ ){
//					front((*it)->children[c], genomes[i], cf);
//					for( unsigned d=0; d<cf.chromosom.size(); d++ ){
//						if( cf.chromosom[d] < 0 ){
//							cf.chromosom[d]*=-1;
//						}
//					}
//					sort(cf.chromosom.begin(), cf.chromosom.end());
//					set_intersection(iset.begin(), iset.end(),
//							itv[itvidx[ cf.chromosom ]].begin(), itv[itvidx[ cf.chromosom ]].end(),
//							insert_iterator<set<int> >(tmpset, tmpset.begin()));
//					iset = tmpset;
//					tmpset.clear();
//					cf.clear();
//				}

				// get the subgenomes (the intervals) of all species of interest
				set<int> elements = set<int>( f.chromosom.begin(), f.chromosom.end() );

				if( iset.find(i) == iset.end() ){
					cerr << i<< " not in iset"<<endl;
					exit(EXIT_FAILURE);
				}

				if( iset.find(j) == iset.end() ){
					cerr << j<< " not in iset"<<endl;
					exit(EXIT_FAILURE);
				}

				for( set<int>::iterator jt=iset.begin(); jt!=iset.end(); jt++ ){
					genom tmpg;
					pair<int,int> iv = genomes[*jt].interval( elements );
					for( int k=iv.first; k<=iv.second; k++)
						tmpg.push_back( genomes[*jt][k]);
					tmpg.set_nmap( genomes[i].get_nmap() );

					if( *jt == i || *jt == j ){
						subgenomes.insert( subgenomes.begin(), tmpg );
						subnames.insert( subnames.begin(), names[*jt] );
					}else{
						subgenomes.push_back( tmpg );
						subnames.push_back( names[*jt] );
					}
				}

				vector<pair<int, int> > ci, ci0, ci1;
				common_intervals(subgenomes[0], subgenomes[1], 0, 0, 0, ci);
				for( unsigned k=2; k<subgenomes.size(); k++ ){
					common_intervals(subgenomes[0], subgenomes[k], 0, 0, 0, ci0);
					common_intervals(subgenomes[1], subgenomes[k], 0, 0, 0, ci1);
					if( ci0.size() > ci.size() && ci1.size() > ci.size() ){
						cerr << ">"; print_name(subnames[0], cerr, false); cerr<<endl<< subgenomes[0]<<endl;
						cerr << ">"; print_name(subnames[1], cerr, false); cerr<<endl<< subgenomes[1]<<endl;
						cerr << ">"; print_name(subnames[k], cerr, false); cerr<<endl<< subgenomes[k]<<endl;
						cerr << "---------------------------------------------------"<<endl;
						eee = true;
						break;
					}
				}


//				cout << i<<" "<<j<<endl;
//				copy(iset.begin(), iset.end(), ostream_iterator<int>(cout, " ")); cout << endl;


				// check if there exists an easier explanation
				// i.e. on of the pairs (of subgenomes) has a smaller number of children
//				identify(subgenomes, hd);
//				for( unsigned k=0; k<subgenomes.size(); k++ ){
//					for( unsigned l=k+1; l<subgenomes.size(); l++ ){
//						if( subgenomes[k] == subgenomes[l] )
//							continue;
//						interval_tree(subgenomes[k], subgenomes[l], subgenomes[0].size(), &tmpsit);
//						if( tmpsit->children.size() < (*it)->children.size() ){
//							eee = true;
////							cerr << tmpsit->children.size() <<"<"<< (*it)->children.size()<<endl;
//						}
//						interval_tree_free(tmpsit);
//					}
//				}

				if( !eee ){
					subgenomes.erase( subgenomes.begin()+2, subgenomes.end() );
					subnames.erase( subnames.begin()+2, subnames.end() );

					cout << "---------------------------------------"<<endl;
					cout << ">"; print_name(subnames[0], cerr, false);cout<<endl;
					cout << subgenomes[0]<<endl;
					cout << ">"; print_name(subnames[1], cerr, false);cout<<endl;
					cout << subgenomes[1]<<endl;
//					its1(subgenomes, subnames, true, subgenomes[0].size());
					its1(subgenomes, subnames, false, subgenomes[0].size());

//					if( pitv.find(f.chromosom) == pitv.end() ){
//						pitv[f.chromosom] = set<set<int> >();
//					}
//					pitv[f.chromosom].insert(iset);
					pospnodeneeecnt++;
				}else{
					pospnodeeeecnt++;
				}
				f.clear();
			}
			pnode.clear();
		}
	}

//	cout << "found " << pitv.size() <<" clusters of interest: "<<endl;
//
//	for( map<vector<int>, set<set<int> > >::iterator it = pitv.begin(); it!= pitv.end(); it++ ){
//		set<int> elements = set<int>( it->first.begin(), it->first.end() );
//		cout << "======================"<<endl;
//		cout << it->second.size()<<" x ";
//		for( unsigned i=0; i<it->first.size(); i++ ){
////			cout << it->first[i]<<" ";
//			cout << (*nmap)[ it->first[i] ]<<" ";
//		}
//		cout << endl;
//		for( set<set<int> >::iterator jt=it->second.begin(); jt!=it->second.end(); jt++ ){
////			cout << "\t";copy( jt->begin(), jt->end(), ostream_iterator<int>(cout, " ") );cout << endl;
//			pair<int,int> iv;
//			vector<genom> subgenomes;
//			vector<string> subnames;
//			for( set<int>::const_iterator kt = jt->begin(); kt!= jt->end(); kt++ ){
//				iv = genomes[*kt].interval( elements );
////				cout << ">"<<names[*kt]<<endl;
//				subgenomes.push_back( genom() );
//				for( int i=iv.first; i<=iv.second; i++ ){
////					print_element( genomes[*kt][i], cout, 0, "", nmap );cout << " ";
//					subgenomes.back().chromosom.push_back( genomes[*kt][i] );
//				}
//				subgenomes.back().set_nmap(nmap);
//				subnames.push_back( names[*kt] );
//			}
//			unique( subgenomes, subnames, false );
//			for(unsigned i=0; i<subgenomes.size(); i++){
////				cout << ">"; print_name(subnames[i], cout); cout<<endl;
//				cout << ">"<< subnames[i]<<endl;
//				cout << subgenomes[i]<<endl;
//			}
//			identify(subgenomes, hd);
//			its1(subgenomes, subnames, true, subgenomes[0].size());
//			cout << "-----"<<endl;
//		}
//		cout << endl;

		//		set<int> elements = set<int>( it->first.begin(), it->first.end() );
//		for( set<int>::iterator jt = itv[itvidx[ it->first ]].begin(); jt!=itv[itvidx[ it->first ]].end(); jt++){
//			pair<int,int> iv = genomes[*jt].interval( elements);
//			cout << ">"<<names[*jt]<<endl;;
//			for( unsigned j=iv.first; j<=iv.second; j++)
//				cout << (*nmap)[ abs(genomes[*jt][j]) ]<<" ";
//			cout << endl;
//		}
//	}

	cout << "#    genomes      "<<genomes.size()<<endl;
	cout << "#    genome pairs "<<(genomes.size() * genomes.size()-1)/2 << endl;
	cout << "#          pnodes "<<pnodecnt<<endl;
	cout << "# positive pnodes "<<pospnodecnt<<endl;
	cout << "# negative pnodes "<<negpnodecnt<<endl;
	cout << "# .. with easier explanation "<< pospnodeeeecnt<<endl;
	cout << "# .. without easier explanation "<< pospnodeneeecnt<<endl;
	cout << "# .. .. in "<<pitv.size()<<" intervals"<<endl;
}

/**
 * do what the program is intended to do ..
 */
int main(int argc, char *argv[]){
	bool pq = false;

	int circular = 1;	// circular genomes
	unsigned n = 0,
		m = 0;
	string gfname; 			// genome filename
	vector<genom> genomes;

	vector<string> nmap,
		names;

	getoptions(argc, argv, gfname, circular, pq);

	dot.open("/tmp/its.dot", ios_base::out);
	dot << "digraph G{"<<endl;

		// read the genomes
	if(gfname == ""){
		cout << "no file given"<<endl;
		usage();
	}
	read_genomes(gfname, genomes, names, circular, nmap);
	m = genomes.size();
	if(m == 0){
		cerr << "no genomes in the file "<< gfname<<endl;
		usage();
	}
	n = genomes[0].size();
	if(n < 2){
		cerr << "genomes are of length < 2" << endl;
		usage();
	}

	its1( genomes, names, pq, n );
//	its2(genomes, names, n);
	dot << "}"<<endl;
	dot.close();
	return EXIT_SUCCESS;	// everything OK .. here you have the answer
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// print some usage info and quit
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cerr << "usage unimplemented "<<endl;
	exit(EXIT_FAILURE);
}
