/**
 * author: Matthias Bernt
 * coauthors: D.Merkle, M. Middendorf
 */

#include <iostream>
#include <iterator>
#include <vector>

#include "genom.hpp"
#include "common.hpp"
#include "io.hpp"

using namespace std;

void usage();

int main(int argc, char *argv[]){
	int ci = 0,
		circ = 0, 
		pq = 0,
		strong = 0, 
		n;
	itnode *pqroot;
	string fname;
	vector<int> r, 
		l;
	vector<genom> genomes;
	vector<pair<int,int> > ints;
	vector<string> names,
		nmap;

		// get the parameters
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'c': {circ = 1; break;}
			case 'f': {i++; fname = argv[i]; break;}
			case 'i': {ci = 1; break;}
			case 'p': {pq = 1; break;}
			case 's': {strong = 1; break;}
			default:{
				cout << "unknown parameter "<<argv[i]<<endl;
//				usage();
			}
		}
	}

	if(fname.size()==0){
		cout << "no filename given ! "<<endl;
		usage();
	}
	
	if( ci == 0 && pq == 0 && strong == 0 ){
		cout << "nothing to do: specify at least one of -i, -s, or -p"<<endl;
		usage();
	}
	
//	read_taxa(fname, genomes, names, circ);	// read the genom file
	read_genomes(fname, genomes, names, circ, nmap);	// read the genom file
	cout << genomes << endl;
	copy(nmap.begin(), nmap.end(), ostream_iterator<string>(cout, " "));cout <<endl;
	if ( genomes.size() == 0){
		cout << "no genomes found in the file "<<endl;
		exit(1);
	}
	
	n = genomes[0].size();
	r = vector<int>( n+1, 0 );
	l = vector<int>( n+1, 0 ); 
	
	if(pq == 1){
		cout << "pq tree"<<endl;
		cout << "======="<<endl;
		interval_tree(genomes[0], genomes[1], n, &pqroot);
		interval_tree_print(pqroot, genomes[0], cout);
		interval_tree_free(pqroot);
	}
	if(strong == 1){
		cout << "strong common intervals"<<endl;
		cout << "======================="<<endl;
		ints = vector<pair<int,int> >(2*n, pair<int,int>());
		strong_intervals( genomes, n, r, l, ints);
		for(unsigned i=0; i<ints.size(); i++){
			cout << "["<<ints[i].first <<","<<ints[i].second <<"] ";
		}
		cout << endl;
		ints.clear();
	}
	if(ci == 1){
		cout << "common intervals"<<endl;
		cout << "================"<<endl;
		common_intervals(genomes, circ, 0, 0, ints);
		for(unsigned i=0; i<ints.size(); i++){
			cout << "["<<ints[i].first <<","<<ints[i].second <<"] ";
		}
		cout << endl;
		ints.clear();
	}

	r.clear();
	l.clear();
	genomes.clear();
	names.clear();

	return 0;
}

void usage(){
	cout << "program to determine the (strong) common intervals / pq tree"<<endl;
	cout << "usage: intervals -f filename [-c] [-s [-p] [-i]"<<endl;
	cout << "   -c: circular genomes (default: linear)"<<endl;
	cout << "   -i: print common intervals"<<endl;
	cout << "   -s: print strong common intervals"<<endl;
	cout << "   -p: print pq tree"<<endl;
	exit(1);
}
