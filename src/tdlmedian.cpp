 /**
 * program for computing tdl medians
 * @author: M.Bernt
 */

#include "genom.hpp"
#include "io.hpp"
#include "tdl.hpp"
#include "tdlmedian.hpp"
#include <algorithm>
#include <iostream>
#include <string>

using namespace std;

/**
 * print some usage info and exit
 */
void usage();

/**
 *
 */
int main( int argc, char *argv[] ){
	bool bf = false,
		allmed = false;		// get all medians
	int circular = 0,		// circular genomes ?
		n = 0,
		mediancnt,
		score;
	mediansolver *solver;
	string fname;			// input filename
	vector<genom> genomes, 	// the genomes
		median;				// the medians
	vector<string> names, 	// names of the genomes
		nmap;				// names of the elements

		// get and check the parameters
	for (int i = 1; i < argc; i++) {
		switch (argv[i][1]) {
			case 'a': {allmed = true; break;}
			case 'b': {bf = true; break;}
			case 'c': {circular = 1; break;}
			case 'f': {
				if(i+1 >= argc){cerr << "error: no filename specified with -f "<<endl;usage();}
				fname = argv[++i]; break;
			}case 'n': {
				if(i+1 >= argc){cerr << "error: no length specified -n "<<endl;usage();}
				n = atoi(argv[++i]); break;
			}
			default:{
				cout << "unknown parameter "<<argv[i]<<endl;
				usage();
			}
		}
	}

	if( bf ){
		solver = new tdl_bfmedian();
	}else{
		solver = new tdl_bbmedian();
	}

	if( fname == "" && n < 2 ){
		cerr << "error: no input file and no n>2 given" <<endl;
		usage();
	}

	if( fname != "" ){
		read_genomes(fname, genomes, names, circular, nmap);	// read the genom file
//		read_taxa(fname, genomes, names, circular);

		if(genomes.size() != 2){
			cout << "error: the input file must contain 2 genomes .. "<<genomes.size()<<" found" <<endl;
			usage();
		}

		solver->solve(genomes, true, 0, std::numeric_limits< int >::max(), mediancnt, score, median );

		for( unsigned i=0; i<median.size(); i++ ){
			cout << median[i]<< " : " << tdrl_distance(median[i], genomes[0])+ tdrl_distance(median[i], genomes[1])<<endl;
		}
	}else{
		genomes.push_back( genom(n, false) );
		genomes.push_back( genom(n, false) );

		next_permutation( genomes[1].chromosom.begin(), genomes[1].chromosom.end() );
		do{
			solver->solve(genomes, allmed, 0, std::numeric_limits< int >::max(), mediancnt, score, median );
			cout << "x "<<median.size() <<" "<<score<<endl;
			for( unsigned i=0; i<median.size(); i++ ){
				cout << median[i]<< " : " << tdrl_distance(median[i], genomes[0])+ tdrl_distance(median[i], genomes[1])<<endl;

			}
		}while(next_permutation( genomes[1].chromosom.begin(), genomes[1].chromosom.end() ));
	}

	delete solver;
	return EXIT_SUCCESS;
}

void usage(){
	cerr << "tdlmedian -f fname [-c]"<<endl;
	exit(1);
}
