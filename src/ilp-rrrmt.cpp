/**
 * program to ...
 * @author: M. Bernt
 */

#include <getopt.h>
#include <iostream>

#include "ilp-rrrmt.hpp"

using namespace std;


/**
 * get program parameters
 * @param[in] argc argument count
 * @param[in] argv arguments
 * @param[out] fname filename
 * @param[out] circluar circularity
 * @param[out] mkalt create alternatives for transpositions and inverse transpositions
 * @param[out] bps compute breakpoint scenario for the given perm
 * @param[out] crexone compute crex1 scenario for the given perm
 * @param[out] crex add breakpoint scenario as alternative for prime nodes
 * @param[out] maxalt maximum number of alternatives constructed for prime nodes (inv+tdrl, bp)
 * @param[out] randint generate TDRLs from random interval
 * @param[out] n genome length
 * @param[out] r no of repeats
 * @param[out] k number of events
 * @param[out] d number of affected elements
 * @param[out] wI weight of inversions
 * @param[out] wT weight of transpositions
 * @param[out] wiT weight of inverse transpositions
 * @param[out] wTDRL weight of TDRLs
 */
void getoptions(int argc, char *argv[], string &fname, int &circular, float &wI, float &wT, float &wiT, float &wTDRL);

/**
 * print usage info and exit
 */
void usage();

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int main(int argc, char *argv[]){
	costfoo_by_type *cst; 	// cost function for crex2
	int circular = true;	// circular genomes ?
	string filename;		// input filename
	vector<genom> genomes; 	// the genomes
	vector<string> names, 	// names of the genomes
		nmap;				// names of the elements
	vector<float> prob(4,1);		// propability of a reversal, transposition, reverse transposition, and tdrl
	rrrmt *sce;


	// get and check the parameters
	// get and check the parameters
	getoptions( argc, argv, filename, circular, prob[0], prob[1], prob[2], prob[3]);

	cst = new costfoo_by_type();
	cst->set( REVNM, prob[0] );
	cst->set( TRANM, prob[1] );
	cst->set( RTRANM, prob[2] );
	cst->set( TDRLNM, prob[3] );

//	cst = new costfoo_equi();

	read_genomes(filename, genomes, names, circular, nmap, true);	// read the genom file
	if(genomes.size() == 0){
		cout << "no genomes in the file"<<endl;
		usage();
	}

	sce = new ilp_rrrmt(genomes[0], genomes[1], cst);

	delete sce;
	delete cst;

	return 0;
}
void getoptions(int argc, char *argv[], string &fname, int &circular,
		float &wI, float &wT, float &wiT, float &wTDRL){

	int c;

	while (1) {
		static struct option long_options[] = {
			{"file",    required_argument, 0, 'f'},
			{"linear",  no_argument, 0, 'l'},
			{"wI", required_argument, 0, 'I'},
			{"wT", required_argument, 0, 'T'},
			{"wiT", required_argument, 0, 'N'},
			{"wTDRL", required_argument, 0, 'D'},
			{0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:l", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'f':
				fname = optarg;
				break;
			case 'l':
				circular = false;
				break;
			case 'I':
				wI = atof( optarg );
				break;
			case 'T':
				wT = atof( optarg );
				break;
			case 'N':
				wiT = atof( optarg );
				break;
			case 'D':
				wTDRL = atof( optarg );
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void usage(){
	cout << "usage"<<endl;
	cout << "mode 1: process a gene orders in a file"<<endl;
	cout << "  call: crex -f filename [OPTIONS]"<<endl;
	cout << "mode 2: process random rearrangement scenario"<<endl;
	cout << "        and compare with reconstruction"<<endl;
	cout << "  call: crex [OPTIONS]"<<endl;
	cout << endl;
	cout << "general options"<<endl;
	cout << "--linear -l: handle genomes as linear (default: circular)"<<endl;
	cout << "--bp   : compute with breakpoint scenario [ZhaoBourque07] (default CREx2)"<<endl;
	cout << "--crex1: compute with CREx1 (default CREx2)"<<endl;
	cout << endl;
	cout << "CREx2 options"<<endl;
	cout << "--wI WEIGHT: weight of an inversion"<<endl;
	cout << "--wT WEIGHT:weight of a transposition"<<endl;
	cout << "--wiT WEIGHT: weight of an inverse transposition"<<endl;
	cout << "--wTDRL WEIGHT: weight of a TDRL"<<endl;
	cout << "--noalt: don't compute alternatives for linear nodes"<<endl;
	cout << ""<<endl;
	cout << endl;
	cout << "CREx1 options"<<endl;
	cout << "--noalt: don't compute alternatives for T+iT"<<endl;
	cout << "--prinobp: don't construct breakpoint scenario for prime nodes"<<endl;
	cout << "--primxalt: maximal number of alternatives for prime nodes (I+TDRL, bp) (default: 2)"<<endl;
	cout << endl;


	cout << "mode 1 option"<<endl;
	cout << "--file   -f: specify a filename "<<endl;
	cout << endl;
	cout << "mode 2 options"<<endl;
	cout << "--length: length of the generated gene orders"<<endl;
	cout << "--events: number of random events to apply"<<endl;
	cout << "--repeat: number of repetitions"<<endl;
	cout << "--affect: max number of elements affected by a rearrangement"<<endl;
	cout << "--comp: TDRLs affect complete gene order (default: randomly chosen interval)"<<endl;
	cout << "--wI, --wT, --wiT, --wTDRL: to set rearrangement weights (see CREx2 options) "<<endl;
	cout << endl;

	exit(1);
}
