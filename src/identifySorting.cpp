#include <algorithm>
#include <iostream>

#include "common.hpp"
#include "conserved.hpp"
#include "helpers.hpp"
#include "io.hpp"
using namespace std;

int main(int argc, char *argv[]) {
	dstnc *rdist = NULL;
	unsigned length = 0,				// length of the genomes
		dist = 0,
		cycle_cnt = 0;
	string filename = "test";
	vector<genom> G;
	vector<string> names;

	init_rng();			// init random number generator

	read_taxa(filename, G, names, 0);
	rdist = new dstnc_inv(G[0].size(), false);

	for (unsigned i = 0; i < G.size(); i++) {
		length = G[i].size();
		genom id(length, false);
		vector<pair<int, int> > reversals = id.getReversals(),
			ei;

		cout << G[i] << endl;
		cout << id << endl;
		vector<pair<int, int> > preservingReversals,
			preservingCycleReversals,
			sortingReversals,
			neutralReversals,
			nabReversals;
		vector<int> cycs,
			pi;

		ei = vector<pair<int,int> > (length+1);
		cycs = vector<int> (length+1, 0);

		pi = id.identify(G[i]);
		cycle_cnt = cycles(pi, length, cycs);
		preservingReversals = getPreservingReversals(G[i], id);
		nabReversals = G[i].getReversals_nonAdjacencyBreaking(id);
		vector< vector< int > > ci = getMaximalChains(G[i], id);
		dist = rdist->calc(id,G[i]);

		cout << "DISTANCE "<<dist<<endl;
		cout << "PRESERVING R"<<endl;
		for (unsigned j=0; j< preservingReversals.size(); j++){
			cout << preservingReversals[j].first<<","<<preservingReversals[j].second<<endl;
			if(cycs[preservingReversals[j].first] && cycs[preservingReversals[j].first] == cycs[preservingReversals[j].second+1])
				preservingCycleReversals.push_back(preservingReversals[j]);
		}

		cout << "elementary reversals "<<endl;

		elementary_intervals(pi, length, ei);

		for(unsigned j=0; j<ei.size(); j++)
			cout << "\t"<<ei[i].first<<","<<ei[i].second<<endl;

		cout << "cycles: ";
		for (unsigned j=0; j<cycs.size(); j++)
			cout << " " <<cycs[j];
		cout << endl;

		cout << "conserved intervals"<<endl;
		for (unsigned j=0; j<ci.size(); j++){
			for(unsigned k=0; k<ci[j].size(); k++)
				cout << ci[j][k] << ",";
			cout << endl;
		}

		for (unsigned j = 0; j < reversals.size(); j++) {
			genom temp = G[i];
			reverse(temp, reversals[j]);
			if (rdist->calc(temp,id) < (int) dist)
				sortingReversals.push_back(reversals[j]);
			if (rdist->calc(temp,id) <= (int) dist)
				neutralReversals.push_back(reversals[j]);
		}
		cout << "preserving (cycle) sorting neutral reversals"<<endl;
		for (unsigned j = 0; j < reversals.size(); j++) {
			cout << reversals[j].first<<","<<reversals[j].second<<" ";

			if(find(preservingReversals.begin(), preservingReversals.end(), reversals[j]) == preservingReversals.end())
				cout << "  ";
			else
				cout << "P ";

			if(find(preservingCycleReversals.begin(), preservingCycleReversals.end(), reversals[j]) == preservingCycleReversals.end())
				cout << "   ";
			else
				cout << "PC ";

			if(find(neutralReversals.begin(), neutralReversals.end(), reversals[j]) == neutralReversals.end())
				cout << "  ";
			else
				cout << "N ";


			if(find(sortingReversals.begin(), sortingReversals.end(), reversals[j]) == sortingReversals.end())
				cout << "  ";
			else
				cout << "S ";

			if(find(nabReversals.begin(), nabReversals.end(), reversals[j]) == nabReversals.end())
				cout << "  ";
			else
				cout << "B ";

			cout << endl;

		}

	}
	delete rdist;
}
