/**
 * filename: solution.cpp
 * author: Martin Bader
 * begin: 25.06.2007
 * last change: 01.10.2008
 *
 * Implementation of solution.h
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
  
#include "solution.h"

#include <stdlib.h>
#include "medianPools.h"

#include <iostream>

namespace median
{
using namespace std;


/***************************************************************************
 * public methods
 ***************************************************************************/
 
Solution::Solution()
{
    m_edges1 = NULL;  
    m_edges2 = NULL;  
    m_edges3 = NULL;  
    m_median = NULL;
    m_even1 = NULL; 
    m_even2 = NULL; 
    m_even3 = NULL; 
    m_steps = 0;
    m_lb = 0;
}         


Solution::Solution(int f_size, bool f_even)
{
    m_edges1 = new int[f_size];  
    m_edges2 = new int[f_size];  
    m_edges3 = new int[f_size];  
    m_median = new int[f_size];
    if (f_even)
    {
        m_even1 = new bool[f_size];
        m_even2 = new bool[f_size];
        m_even3 = new bool[f_size];
    }
    else
    {
        m_even1 = NULL;
        m_even2 = NULL;
        m_even3 = NULL;
    }
    m_steps = 0;
    m_lb = 0;
}


Solution::~Solution()
{
    delete[] m_edges1;
    delete[] m_edges2;
    delete[] m_edges3;
    delete[] m_median;
    if (m_even1 != NULL)
    {
        delete[] m_even1;
        delete[] m_even2;
        delete[] m_even3;
    }
}


/***************************************************************************
 * debug methods
 ***************************************************************************/

void Solution::dump(int f_size)
{
    cout << "***** dumping solution " << (long)this << " ******\n";
    cout << "steps: " << m_steps << "   lb: " << m_lb << endl;
    cout << "m_edges1\n";
    for (int i = 0; i < f_size; i++)
        cout << m_edges1[i] << " ";
    cout << endl;
    cout << "m_edges2\n";
    for (int i = 0; i < f_size; i++)
        cout << m_edges2[i] << " ";
    cout << endl;
    cout << "m_edges3\n";
    for (int i = 0; i < f_size; i++)
        cout << m_edges3[i] << " ";
    cout << endl;
    cout << "m_median\n";
    for (int i = 0; i < f_size; i++)
        cout << m_median[i] << " ";
    cout << endl;
    if (m_even1 != NULL)
    {
        cout << "m_even1\n";
        for (int i = 0; i < f_size; i++)
            cout << (m_even1[i]? "e " : "o ");
        cout << endl;    
        cout << "m_even2\n";
        for (int i = 0; i < f_size; i++)
            cout << (m_even2[i]? "e " : "o ");
        cout << endl;    
        cout << "m_even3\n";
        for (int i = 0; i < f_size; i++)
            cout << (m_even3[i]? "e " : "o ");
        cout << endl;    
    }
    cout << "***********************\n";    
}


/***************************************************************************/

} // end namespace median
