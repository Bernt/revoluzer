/**
 * filename: start.cpp
 * author: Martin Bader
 * begin: 25.06.2007
 * last change: 23.08.2010
 *
 * Start file. This is a small user interface.
 * 
 * Copyright (C) 2007-2010  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
  
#include "median.h"

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <vector>


using namespace std;
using namespace median;


/**
 * Reads permutations from a file. The permutations will be written to
 * f_perm1, f_perm2, f_perm3, which are passed uninitialized. The genome 
 * names are written to f_name1, f_name2, f_name3, which are also passed
 * uninitialized. The permutation size will be returned (on error -1)
 * So far, only minor error handling implemented
 * @param f_filename  name of the file
 * @param f_perm1  first permutation will be written here
 * @param f_perm2  second permutation will be written here
 * @param f_perm3  third permutation will be written here
 * @param f_name1  name of the first genome
 * @param f_name2  name of the second genome
 * @param f_name3  name of the third genome
 * @return  the permutation size
 */  
int readFile(const char* f_filename, int* &f_perm1, int* &f_perm2, int* &f_perm3,
  char* &f_name1, char* &f_name2, char* &f_name3)
{
    ifstream input;
    char buffer[256];
    vector<int> tmpPerm;
    int stringsize;     
    int permsize;      
    
    input.open(f_filename);
    while (input.peek() == '#')          // line is a comment
        input.ignore(1024, '\n');
    if (input.peek() != '>')             // wrong format
    {
        input.close();
        return -1;    
    }
    input.ignore(1, ' ');
    input.getline(buffer, 256);
    stringsize = input.gcount();
    f_name1 = new char[stringsize];
    strncpy(f_name1, buffer, stringsize);
    while (input.peek() == '#')          // line is a comment
        input.ignore(1024, '\n');
    permsize = 0;
    while (input.peek() == ' ')
        input.ignore(1);
    do
    {
        tmpPerm.push_back(0);             // increment vector size
        for (stringsize = 0; (input.peek() != ' ') && (input.peek() != '\n'); stringsize++)
            input >> buffer[stringsize];
        buffer[stringsize] = 0;    
        tmpPerm[permsize++] = atoi(buffer);
        while (input.peek() == ' ')
            input.ignore(1);
    } while (input.peek() != '\n');
    input.ignore(1);
    f_perm1 = new int[permsize];
    for (int i = 0; i < permsize; i++)
        f_perm1[i] = tmpPerm[i];
    while (input.peek() == '#')          // line is a comment
        input.ignore(1024, '\n');
    if (input.peek() != '>')             // wrong format
    {
        input.close();
        delete[] f_perm1; f_perm1 = NULL;
        delete[] f_name1; f_name1 = NULL;
        return -1;    
    }
    input.ignore(1, ' ');
    input.getline(buffer, 256);
    stringsize = input.gcount();
    f_name2 = new char[stringsize];
    strncpy(f_name2, buffer, stringsize);
    while (input.peek() == '#')          // line is a comment
        input.ignore(1024, '\n');
    f_perm2 = new int[permsize];    
    for (int i = 0; i < permsize; i++)
        input >> f_perm2[i];
    input.ignore(256, '\n');        
    while (input.peek() == '#')          // line is a comment
        input.ignore(1024, '\n');
    if (input.peek() != '>')             // wrong format
    {
        input.close();
        delete[] f_perm1; f_perm1 = NULL;
        delete[] f_name1; f_name1 = NULL;
        delete[] f_perm2; f_perm2 = NULL;
        delete[] f_name2; f_name2 = NULL;
        return -1;    
    }
    input.ignore(1, ' ');
    input.getline(buffer, 256);
    stringsize = input.gcount();
    f_name3 = new char[stringsize];
    strncpy(f_name3, buffer, stringsize);
    while (input.peek() == '#')          // line is a comment
        input.ignore(1024, '\n');
    f_perm3 = new int[permsize];    
    for (int i = 0; i < permsize; i++)
        input >> f_perm3[i];    
    input.close();
    return permsize;    
} 


int main(int f_argc, char** f_argv)
{
    int* perm1 = NULL;
    int* perm2 = NULL;
    int* perm3 = NULL;
    int* result = NULL;
    char* name1 = NULL;
    char* name2 = NULL;
    char* name3 = NULL;
    int size;
    Median* solver = new Median();   // the median solver
    int resultweight;
    int bestweight;                  // store resultweight for comparing with cutoff value
    
    cout << "*****************************************************************\n";
    cout << "median v2.3.0  Copyright (C) 2007-2010  Martin Bader\n";
    cout << "This program comes with ABSOLUTELY NO WARRANTY.\n";
    cout << "This is free software, and you are welcome to redistribute it\n";
    cout << "under certain conditions.\n";
    cout << "For details about the licence, read the file gpl.txt\n";
    cout << "*****************************************************************\n";
    if (f_argc != 7)
    {
        cout << "Usage: median infile wr wt distMode exact all\n";
        cout << "  wr, wt: integer weights for reversals and (inverted) transpositions,\n";
        cout << "  distmode: 0: reversals only; 1: transpositions only; 2: weighted reversal and transposition distance\n";
        cout << "  exact: 0 if pairwise distances are evaluated approximately, 1 otherwise\n";
        cout << "  all: 0 gives only one optimal solution, 1 gives all optimal solutions\n"; 
        return 0;
    }
    cout << "reading file...\n";
    size = readFile(f_argv[1], perm1, perm2, perm3, name1, name2, name3);
    cout << "done\n";
    result = new int[size];    
    if ((f_argv[4][0] != '0') && (f_argv[5][0] == '1'))       // weighted branch and bound needed
//        solver->init(size, atoi(f_argv[2]), atoi(f_argv[3]), atoi(f_argv[4]), (f_argv[5][0] == '1'), 25000000 / size, 25000000 / size);
        solver->init(size, atoi(f_argv[2]), atoi(f_argv[3]), atoi(f_argv[4]), (f_argv[5][0] == '1'), 60000000 / size, 15000000 / size);
    else    
//        solver->init(size, atoi(f_argv[2]), atoi(f_argv[3]), atoi(f_argv[4]), (f_argv[5][0] == '1'), 50000000 / size); 
        solver->init(size, atoi(f_argv[2]), atoi(f_argv[3]), atoi(f_argv[4]), (f_argv[5][0] == '1'), 75000000 / size); 
    if (f_argv[6][0] == '0')    // only one solution seeked        
    {        
        // use this for only one median    
        resultweight = solver->solve(perm1, perm2, perm3, result);
        cout << "resulting permutation:\n";
        for (int i = 0; i < size; i++)
            cout << result[i] << " ";
        cout << endl;  
        cout << "weight: " << resultweight << endl;
        cout << "heap peak: " << solver->getHeapPeak() << endl;
        if (solver->getLeastCutoff() < resultweight)
            cout << "A solution of weight " << solver->getLeastCutoff() << " might have been overlooked\n";
    }
    else                        // all optimal solutions seeked
    {  
        resultweight = solver->getFirstSolution(perm1, perm2, perm3, result);
        bestweight = resultweight;
        while (resultweight >= 0)
        {
            cout << "resulting permutation:\n";
            for (int i = 0; i < size; i++)
                cout << result[i] << " ";
            cout << endl;  
            cout << "weight: " << resultweight << endl;
            cout << "heap peak: " << solver->getHeapPeak() << endl;
            resultweight = solver->getNextSolution(result);  
        }
        if (solver->getLeastCutoff() <= bestweight)
            cout << "Some solutions of weight " << solver->getLeastCutoff() << " might have been overlooked\n";
    }
    delete[] perm1;
    delete[] perm2;
    delete[] perm3;
    if (name1 != NULL)
        delete[] name1;
    if (name2 != NULL)
        delete[] name2;
    if (name3 != NULL)
        delete[] name3;
    delete[] result;
    delete solver;
    return 0;    
}
