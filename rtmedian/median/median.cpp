/**
 * filename: median.cpp
 * author: Martin Bader
 * begin: 25.06.2007
 * last change: 20.08.2010
 *
 * Implementation of median.h
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
  
#include "median.h"

#include <iostream>

#include <algorithm>
#include <stdlib.h>
#include "medianPools.h"
#include "minswrt.h"
#include "solution.h"

namespace median
{
using namespace std;


/***************************************************************************
 * public methods
 ***************************************************************************/
 
Median::Median()
{
    m_size = 0;
    m_input = new const int*[3];
    m_wr = 1;
    m_wt = 2;
    m_distMode = 2;
    m_upperBound = INT_MAX;
    m_solutionPool = NULL;
    m_heap.reserve(4096);
    m_heapPeak = 0;
    m_heapMax = 2000000;
    m_overlooked = INT_MAX - 3;
    m_boolArray2 = NULL;
    m_cycle12 = NULL;
    m_cycle13 = NULL;
    m_cycle23 = NULL;
    m_perm = NULL;
    m_d1 = -1;
    m_d2 = -1;
    m_d3 = -1;
    m_nodeOrder = NULL;
    m_minswrt.setGreedy(1);
#if DEBUG_MEDIAN    
    // init debug vars    
    m_perm1 = NULL;
    m_perm2 = NULL;
    m_perm3 = NULL;
#endif    
}         


Median::~Median()
{
    delete[] m_input;
    delete m_solutionPool;
    delete[] m_boolArray2;
    delete[] m_cycle12;
    delete[] m_cycle13;
    delete[] m_cycle23;
    delete[] m_perm;
    delete[] m_nodeOrder;
#if DEBUG_MEDIAN    
    delete[] m_perm1;
    delete[] m_perm2;
    delete[] m_perm3;
#endif    
}


void Median::init(int f_size, int f_wr, int f_wt, int f_distMode, bool f_exact, unsigned int f_heapMax, int f_pairwiseHeapMax)
{
    // if there is any old data, clear it
    if (m_solutionPool != NULL)
        delete m_solutionPool;
    if (m_boolArray2 != NULL)
        delete[] m_boolArray2;
    if (m_cycle12 != NULL)
        delete[] m_cycle12;    
    if (m_cycle13 != NULL)
        delete[] m_cycle13;    
    if (m_cycle23 != NULL)
        delete[] m_cycle23;
    if (m_perm != NULL)
        delete[] m_perm; 
    if (m_nodeOrder != NULL)
        delete[] m_nodeOrder;               
    m_size = f_size;
    m_wr = f_wr;
    m_wt = f_wt;
    m_distMode = f_distMode;
    m_exact = f_exact;
    m_solutionPool = new SolutionPool();
    m_solutionPool->init(f_size, (f_distMode > 0));
    m_boolArray2 = new bool[f_size * 2];
    m_cycle12 = new int[f_size * 2];
    m_cycle13 = new int[f_size * 2];
    m_cycle23 = new int[f_size * 2];
    m_perm = new int[f_size];
    m_d1 = -1;
    m_d2 = -1;
    m_d3 = -1;
    m_nodeOrder = new int[f_size * 2];
    if (m_distMode == 0)           // reversals only
    {
        m_revdist.init(f_size);    
    }
    else if (m_distMode == 1)      // transpositions only
    {
        m_minswrt.setWeights(f_wt, f_wt);
        m_minswrt.setPermutationSize(f_size);
        if (m_exact)
        {
            m_weightedbb.setWeights(f_wr, f_wt, 0);
            m_weightedbb.setHeapLimit(f_pairwiseHeapMax);
        }
    }
    else                           // weighted r/t distance
    {
        m_minswrt.setWeights(f_wr, f_wt);
        m_minswrt.setPermutationSize(f_size);
        if (m_exact)
            m_weightedbb.setWeights(f_wr, f_wt, 1);
    }
    m_heapMax = f_heapMax;
}


int Median::solve(const int* f_perm1, const int* f_perm2, const int* f_perm3, 
  int* f_result, int f_upperBound)
{
    int weight;
    
#if DEBUG_MEDIAN    
    m_numExpansions = 0;
#endif
    weight = solveWithoutClean(f_perm1, f_perm2, f_perm3, f_result, f_upperBound);
    cleanHeap();
#if DEBUG_MEDIAN    
    cout << m_numExpansions << " expansions calculated\n";
#endif
    return weight;
}  


int Median::getFirstSolution(const int* f_perm1, const int* f_perm2, const int* f_perm3, 
  int* f_result, int f_upperBound)
{
    return solveWithoutClean(f_perm1, f_perm2, f_perm3, f_result, f_upperBound);
}
      

int Median::getNextSolution(int* f_result)
{
    Solution* sol = m_solutionPool->getSolution();  // the actual partial solution
    int node;                                       // a node in a MBG
    int resultweight;                               // weight of the median
    int bestweight;                                 // best possible weight

    if (m_heap.size() == 0)    // heap empty! should never happen with proper use...
    {
        m_d1 = -1;
        m_d2 = -1;
        m_d3 = -1;
        return -1;
    }
    bestweight = m_heap[0]->m_lb;         
    // remove heap top element (we already used this solution), heapify
    m_solutionPool->freeSolution(m_heap[0]);
    m_heap[0] = m_heap[m_heap.size()-1];
    m_heap.pop_back();
    heapifyDown();
    if ((m_heap.size() == 0) || (m_heap[0]->m_lb > bestweight))   // no more optimal medians
    {
        m_d1 = -1;
        m_d2 = -1;
        m_d3 = -1;
        return -1;
    } 
    // find next solution
    while (m_heap.size() > 0)
    {
        if (m_heap[0]->m_steps == m_size)
        {
            if (verifyWeight())
                break;
        }
        else
        {
            if (m_distMode == 0)
                expandRev();               // includes heapify
            else 
                expand();                  // includes heapify
        }
    }
    if ((m_heap.size() == 0) || (m_heap[0]->m_lb > bestweight))          // no solution within upper bound
    {
        m_d1 = -1;
        m_d2 = -1;
        m_d3 = -1;
        return -1;
    }    
    // the optimal complete solution is now the heap top element
    // build resulting permutation by following hamiltonian cycle
    sol = m_heap[0];
    f_result[0] = 1;
    node = 1;
    for (int i = 1; i < m_size; i++)
    {
        node = sol->m_median[node];
        if (node & 1)                      // connected to right point
        {
            f_result[i] = -((node + 1) / 2);
            node--;
        } 
        else                                // connected to left point
        {
            f_result[i] = (node + 2) / 2;
            node++;
        }
    }
    if (m_distMode == 0)            
        resultweight = m_wr * sol->m_lb / 2;
    else    
        resultweight = sol->m_lb / 4;
    return resultweight;
}  
    

void Median::getMedianDist(int& f_d1, int& f_d2, int& f_d3)
{
    f_d1 = m_d1;    
    f_d2 = m_d2;    
    f_d3 = m_d3;    
}


void Median::cleanHeap()
{
    // clean up the heap
    for (unsigned int i = 0; i < m_heap.size(); i++)
        m_solutionPool->freeSolution(m_heap[i]);
    m_heap.clear();
}


int Median::getLeastCutoff()
{
    if (m_distMode == 0)    
        return (m_overlooked + 1) / 2;
    else
        return (m_overlooked + 3) / 4;
}
 
 
int Median::pairDist(const int* f_perm0, const int* f_perm1, int f_mode)
{
    int lb;
    int dist0 = 0;   // forward distance    
    int dist1 = 0;   // reverse distance
    const vector<minswrt::Operation>* sorting;      // sorting sequence 
    
    if (m_distMode == 0)
        return m_revdist.getDistance(f_perm0, f_perm1);
    else
    {
        m_minswrt.setTarget(f_perm1);
        m_minswrt.setOrigin(f_perm0);
        lb = (int)(m_minswrt.getLowerBound() + 0.01f);
        if (f_mode == 1)
            return lb;
        m_minswrt.sort();
        sorting = m_minswrt.getResult();
        for (unsigned int i = 0; i < sorting->size(); i++)
            dist0 += ((*sorting)[i].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
        if (dist0 > lb)
        {
            m_minswrt.setTarget(f_perm0);
            m_minswrt.setOrigin(f_perm1);
            m_minswrt.sort();
            sorting = m_minswrt.getResult();
            for (unsigned int i = 0; i < sorting->size(); i++)
                dist1 += ((*sorting)[i].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
            if (dist1 < dist0)
                dist0 = dist1;
        }
        if ((dist0 > lb) && m_exact && (f_mode == 0))
        {
            m_weightedbb.setPermutations(m_size, f_perm0, f_perm1);
            m_weightedbb.sort(dist0);
            dist1 = m_weightedbb.getDistance();
            if (dist1 < dist0)
                dist0 = dist1;
        }
        return dist0;
    }
}
 


/***************************************************************************
 * private methods
 ***************************************************************************/

int Median::solveWithoutClean(const int* f_perm1, const int* f_perm2, const int* f_perm3, 
  int* f_result, int f_upperBound)
{
    Solution* sol = m_solutionPool->getSolution();  // the actual partial solution
    int node1, node2;                               // two nodes in a MBG
    int resultweight;                               // weight of the median
    int d12, d13, d23;                              // pairwise distances for upper bound
    int lb;                                         // lower bound for weighted distance
    const vector<minswrt::Operation>* sorting;      // sorting sequence 
    
    // init some members
    m_input[0] = f_perm1;    
    m_input[1] = f_perm2;    
    m_input[2] = f_perm3;
    // set the upper bound
    m_upperBound = f_upperBound;  
    if (m_distMode == 0)
    {
        d12 = m_revdist.getDistance(f_perm1, f_perm2);
        d13 = m_revdist.getDistance(f_perm1, f_perm3);
        d23 = m_revdist.getDistance(f_perm2, f_perm3);
        if (d12 + d13 < m_upperBound)
            m_upperBound = d12 + d13;
        if (d12 + d23 < m_upperBound)
            m_upperBound = d12 + d23;
        if (d13 + d23 < m_upperBound)
            m_upperBound = d13 + d23;
        m_upperBound *= 2;    
    }
    else
    {
        m_minswrt.setTarget(f_perm2);
        m_minswrt.setOrigin(f_perm1);
        lb = (int)(m_minswrt.getLowerBound() + 0.01f);
        d12 = 0;
        m_minswrt.sort();
        sorting = m_minswrt.getResult();
        for (unsigned int i = 0; i < sorting->size(); i++)
            d12 += ((*sorting)[i].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
        if (d12 > lb)
        {
            m_minswrt.setTarget(f_perm1);
            m_minswrt.setOrigin(f_perm2);
            lb = 0;
            m_minswrt.sort();
            sorting = m_minswrt.getResult();
            for (unsigned int i = 0; i < sorting->size(); i++)
                lb += ((*sorting)[i].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
            if (lb < d12)
                d12 = lb;
        }
        m_minswrt.setTarget(f_perm3);
        m_minswrt.setOrigin(f_perm1);
        lb = (int)(m_minswrt.getLowerBound() + 0.01f);
        d13 = 0;
        m_minswrt.sort();
        sorting = m_minswrt.getResult();
        for (unsigned int i = 0; i < sorting->size(); i++)
            d13 += ((*sorting)[i].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
        if (d13 > lb)
        {
            m_minswrt.setTarget(f_perm1);
            m_minswrt.setOrigin(f_perm3);
            lb = 0;
            m_minswrt.sort();
            sorting = m_minswrt.getResult();
            for (unsigned int i = 0; i < sorting->size(); i++)
                lb += ((*sorting)[i].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
            if (lb < d13)
                d13 = lb;
        }
        m_minswrt.setTarget(f_perm3);
        m_minswrt.setOrigin(f_perm2);
        lb = (int)(m_minswrt.getLowerBound() + 0.01f);
        d23 = 0;
        m_minswrt.sort();
        sorting = m_minswrt.getResult();
        for (unsigned int i = 0; i < sorting->size(); i++)
            d23 += ((*sorting)[i].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
        if (d23 > lb)
        {
            m_minswrt.setTarget(f_perm2);
            m_minswrt.setOrigin(f_perm3);
            lb = 0;
            m_minswrt.sort();
            sorting = m_minswrt.getResult();
            for (unsigned int i = 0; i < sorting->size(); i++)
                lb += ((*sorting)[i].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
            if (lb < d23)
                d23 = lb;
        }
        if (d12 + d13 < m_upperBound)
            m_upperBound = d12 + d13;
        if (d12 + d23 < m_upperBound)
            m_upperBound = d12 + d23;
        if (d13 + d23 < m_upperBound)
            m_upperBound = d13 + d23;
        m_upperBound *= 4;    
    }
    // create initial partial solution        
    for (int i = 1; i < m_size; i++)
    {
        node1 = (f_perm1[i-1] > 0? 2 * f_perm1[i-1] - 1 : -2 * f_perm1[i-1] - 2);   
        node2 = (f_perm1[i] > 0? 2 * f_perm1[i] - 2 : -2 * f_perm1[i] - 1);
        sol->m_edges1[node1] = node2; 
        sol->m_edges1[node2] = node1;
    } 
    node1 = (f_perm1[m_size-1] > 0? 2 * f_perm1[m_size-1] - 1 : -2 * f_perm1[m_size-1] - 2);   
    node2 = (f_perm1[0] > 0? 2 * f_perm1[0] - 2 : -2 * f_perm1[0] - 1);   
    sol->m_edges1[node1] = node2; 
    sol->m_edges1[node2] = node1;
    for (int i = 1; i < m_size; i++)
    {
        node1 = (f_perm2[i-1] > 0? 2 * f_perm2[i-1] - 1 : -2 * f_perm2[i-1] - 2);   
        node2 = (f_perm2[i] > 0? 2 * f_perm2[i] - 2 : -2 * f_perm2[i] - 1);   
        sol->m_edges2[node1] = node2; 
        sol->m_edges2[node2] = node1;
    } 
    node1 = (f_perm2[m_size-1] > 0? 2 * f_perm2[m_size-1] - 1 : -2 * f_perm2[m_size-1] - 2);   
    node2 = (f_perm2[0] > 0? 2 * f_perm2[0] - 2 : -2 * f_perm2[0] - 1);   
    sol->m_edges2[node1] = node2; 
    sol->m_edges2[node2] = node1;
    for (int i = 1; i < m_size; i++)
    {
        node1 = (f_perm3[i-1] > 0? 2 * f_perm3[i-1] - 1 : -2 * f_perm3[i-1] - 2);   
        node2 = (f_perm3[i] > 0? 2 * f_perm3[i] - 2 : -2 * f_perm3[i] - 1);   
        sol->m_edges3[node1] = node2; 
        sol->m_edges3[node2] = node1;
    } 
    node1 = (f_perm3[m_size-1] > 0? 2 * f_perm3[m_size-1] - 1 : -2 * f_perm3[m_size-1] - 2);   
    node2 = (f_perm3[0] > 0? 2 * f_perm3[0] - 2 : -2 * f_perm3[0] - 1);   
    sol->m_edges3[node1] = node2; 
    sol->m_edges3[node2] = node1;
    for (int i = 0; i < 2 * m_size; i++)
        sol->m_median[i] = -1;          // node not yet connected
    if (m_distMode == 0)
    {
        sol->m_steps = 0;
        calcLowerBoundRev(sol);
    }
    else
    {    
        for (int i = 0; i < 2 * m_size; i++)
            sol->m_even1[i] = true;
        for (int i = 0; i < 2 * m_size; i++)
            sol->m_even2[i] = true;
        for (int i = 0; i < 2 * m_size; i++)
            sol->m_even3[i] = true;
        sol->m_steps = 0;
        calcLowerBound(sol);
    }
    if (m_distMode == 0)
        createNodeOrderRev(sol);
    else
        createNodeOrder(sol);
#if DEBUG_MEDIAN        
    m_perm1 = new int[2 * m_size];
    for (int i = 0; i < 2 * m_size; i++)
        m_perm1[i] = sol->m_edges1[i];
    m_perm2 = new int[2 * m_size];
    for (int i = 0; i < 2 * m_size; i++)
        m_perm2[i] = sol->m_edges2[i];
    m_perm3 = new int[2 * m_size];
    for (int i = 0; i < 2 * m_size; i++)
        m_perm3[i] = sol->m_edges3[i];
#endif    
    m_heap.push_back(sol);
    m_heapPeak = 1;
    m_overlooked = INT_MAX - 3;
    while (m_heap.size() > 0)
    {
        if (m_heap[0]->m_steps == m_size)
        {
            if (verifyWeight())
                break;
        }
        else if (m_distMode == 0)
            expandRev();
         else
            expand();                  // includes heapify
    }
    if (m_heap.size() == 0)            // no solution within upper bound
    {
        m_d1 = -1;
        m_d2 = -1;
        m_d3 = -1;
        return -1;
    }    
    // the optimal complete solution is now the heap top element
    // build resulting permutation by following hamiltonian cycle
    sol = m_heap[0];
    f_result[0] = 1;
    node1 = 1;
    for (int i = 1; i < m_size; i++)
    {
        node1 = sol->m_median[node1];
        if (node1 & 1)                      // connected to right point
        {
            f_result[i] = -((node1 + 1) / 2);
            node1--;
        } 
        else                                // connected to left point
        {
            f_result[i] = (node1 + 2) / 2;
            node1++;
        }
    }
    if (m_distMode == 0)            
        resultweight = m_wr * sol->m_lb / 2;
    else    
        resultweight = sol->m_lb / 4;
    return resultweight;
}

 
void Median::calcLowerBound(Solution* f_sol)
{
    unsigned int even = 0;    // number of even cycles
    unsigned int odd = 0;     // number of odd cycles
    int pos;         // the actual position
    bool length;              // length of the actual cycle (0: odd, 1: even) 
    
    // perm1 vs perm2
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;    // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (m_boolArray2[i])    // already visited
            continue;
        pos = i;    
        length = true;
        do
        {
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges1[pos];
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges2[pos];
            length = !length;
        } while (pos != i);
        length? even++ : odd++; 
    }
    // perm1 vs perm3
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;    // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (m_boolArray2[i])    // already visited
            continue;
        pos = i;    
        length = true;
        do
        {
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges1[pos];
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges3[pos];
            length = !length;
        } while (pos != i);
        length? even++ : odd++; 
    }
    // perm2 vs perm3
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;    // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (m_boolArray2[i])    // already visited
            continue;
        pos = i;    
        length = true;
        do
        {
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges2[pos];
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges3[pos];
            length = !length;
        } while (pos != i);
        length? even++ : odd++; 
    }
    f_sol->m_lb = (3 * m_size - odd) * m_wt - (m_wt - m_wr) * 2 * even; 
}

      
void Median::calcLowerBoundRev(Solution* f_sol)
{
    unsigned int cycles = 0;    // number of cycles
    int pos;                    // the actual position
    
    // perm1 vs perm2
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;    // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (m_boolArray2[i])    // already visited
            continue;
        pos = i;    
        do
        {
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges1[pos];
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges2[pos];
        } while (pos != i);
        cycles++;
    }
    // perm1 vs perm3
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;    // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (m_boolArray2[i])    // already visited
            continue;
        pos = i;    
        do
        {
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges1[pos];
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges3[pos];
        } while (pos != i);
        cycles++;
    }
    // perm2 vs perm3
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;    // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (m_boolArray2[i])    // already visited
            continue;
        pos = i;    
        do
        {
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges2[pos];
            m_boolArray2[pos] = true; 
            pos = f_sol->m_edges3[pos];
        } while (pos != i);
        cycles++;
    }
    f_sol->m_lb = 3 * m_size - cycles; 
}

      
void Median::createNodeOrder(Solution* f_sol)
{
    pair<int, int>* scores = new pair<int, int>[2 * m_size];    // array which
                // contains for each node its score (first) and its index (second)
    int node1;        // first node of a cycle
    int pos;          // a position in a cycle
    int deltaLb;      // change of the lower bound due to an extension (+ fixed bias)
    int* node2Cycle12 = new int[2 * m_size];      // maps each node to a cycle index between perm1 and perm2
    int* node2Cycle13 = new int[2 * m_size];      // maps each node to a cycle index between perm1 and perm3
    int* node2Cycle23 = new int[2 * m_size];      // maps each node to a cycle index between perm2 and perm3
    int* cyclelength12 = new int[m_size];         // the length of the cycles between perm1 and perm2
    int* cyclelength13 = new int[m_size];         // the length of the cycles between perm1 and perm3
    int* cyclelength23 = new int[m_size];         // the length of the cycles between perm2 and perm3
                
    // init some data
    for (int i = 0; i < 2 * m_size; ++i)
    {
        scores[i].first = 0;                
        scores[i].second = i;
        m_cycle12[i] = 0;
        m_cycle13[i] = 0;
        m_cycle23[i] = 0;
    }
    // precalculate cycle information 
    // find cycles between perm1 and perm2
    node1 = 0;
    for (int i = 0; true; ++i)
    {
        while ((node1 < 2 * m_size) && (m_cycle12[node1] != 0))
            node1++;
        if (node1 == 2 * m_size)
            break;    
        pos = node1;
        do
        {
            m_cycle12[f_sol->m_edges1[pos]] = m_cycle12[pos] + 1;
            pos = f_sol->m_edges1[pos];
            node2Cycle12[pos] = i;
            m_cycle12[f_sol->m_edges2[pos]] = m_cycle12[pos] + 1;
            pos = f_sol->m_edges2[pos];        
            node2Cycle12[pos] = i;
        } while (pos != node1);
        cyclelength12[i] = m_cycle12[pos];    
    }
    // find cycles between perm1 and perm3
    node1 = 0;
    for (int i = 0; true; ++i)
    {
        while ((node1 < 2 * m_size) && (m_cycle13[node1] != 0))
            node1++;
        if (node1 == 2 * m_size)
            break;    
        pos = node1;
        do
        {
            m_cycle13[f_sol->m_edges1[pos]] = m_cycle13[pos] + 1;
            pos = f_sol->m_edges1[pos];
            node2Cycle13[pos] = i;
            m_cycle13[f_sol->m_edges3[pos]] = m_cycle13[pos] + 1;
            pos = f_sol->m_edges3[pos];        
            node2Cycle13[pos] = i;
        } while (pos != node1);
        cyclelength13[i] = m_cycle13[pos];    
    }
    // find cycles between perm2 and perm3
    node1 = 0;
    for (int i = 0; true; ++i)
    {
        while ((node1 < 2 * m_size) && (m_cycle23[node1] != 0))
            node1++;
        if (node1 == 2 * m_size)
            break;    
        pos = node1;
        do
        {
            m_cycle23[f_sol->m_edges2[pos]] = m_cycle23[pos] + 1;
            pos = f_sol->m_edges2[pos];
            node2Cycle23[pos] = i;
            m_cycle23[f_sol->m_edges3[pos]] = m_cycle23[pos] + 1;
            pos = f_sol->m_edges3[pos];        
            node2Cycle23[pos] = i;
        } while (pos != node1);
        cyclelength23[i] = m_cycle23[pos];    
    }
    // calculate the scores; the score of a node is the sum of the lower bounds of its extensions
    // -> calculate the score of all extensions
    for (node1 = 0; node1 < 2 * m_size; ++node1)
    {
        // check scores for expansion node1 - i
        for (int i = node1 + 1; i < m_size * 2; (m_distMode == 1? i += 2 : i++))
        {
            if ((node1 ^ i) == 1)                      // short cycle -> not a permutation matching
                continue;
            deltaLb = 0;
            // do not add the basic change for step +=1, node -= 1, as its always the same
            // edges parallel to contracted edge
            if (f_sol->m_edges1[node1] == i)
                deltaLb -= 2 * m_wt;
            if (f_sol->m_edges2[node1] == i)
                deltaLb -= 2 * m_wt;
            if (f_sol->m_edges3[node1] == i)
                deltaLb -= 2 * m_wt;
            // pairwise distances: perm1 vs perm2
            if ((f_sol->m_edges1[node1] == i) && (f_sol->m_edges2[node1] == i))   // both edges parallel to m
                deltaLb += m_wt;
            else if (f_sol->m_edges1[node1] == i)                               // one edge (1) parallel to m
            {
                if (!f_sol->m_even1[node1])                                     // else no change in lb
                    deltaLb += (cyclelength12[node2Cycle12[node1]] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
            } 
            else if (f_sol->m_edges2[node1] == i)                               // one edge (2) parallel to m
            {
                if (!f_sol->m_even2[node1])                                     // else no change in lb
                    deltaLb += (cyclelength12[node2Cycle12[node1]] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
            } 
            else if (node2Cycle12[i] != node2Cycle12[node1])              // merge cycles
            {
                if (cyclelength12[node2Cycle12[node1]] & 2)
                    deltaLb += m_wt;
                else if (cyclelength12[node2Cycle12[i]] & 2)
                    deltaLb += m_wt;
                else
                    deltaLb += 3 * m_wt - 4 * m_wr;
            }
            else if ((m_cycle12[node1] - m_cycle12[i]) % 2 == 0)              // cycle changes sign
                deltaLb += (cyclelength12[node2Cycle12[node1]] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
            else if (!(cyclelength12[node2Cycle12[node1]] & 2) || (!(abs(m_cycle12[i] - m_cycle12[node1]) & 2)))
                deltaLb -= m_wt;                                         // split in two cycles
            else 
                deltaLb += 4 * m_wr - 3 * m_wt;                              // split in two cycles
            // pairwise distances: perm1 vs perm3
            if ((f_sol->m_edges1[node1] == i) && (f_sol->m_edges3[node1] == i))   // both edges parallel to m
                deltaLb += m_wt;
            else if (f_sol->m_edges1[node1] == i)                               // one edge (1) parallel to m
            {
                if (!f_sol->m_even1[node1])                                     // else no change in lb
                    deltaLb += (cyclelength13[node2Cycle13[node1]] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
            } 
            else if (f_sol->m_edges3[node1] == i)                               // one edge (3) parallel to m
            {
                if (!f_sol->m_even3[node1])                                     // else no change in lb
                    deltaLb += (cyclelength13[node2Cycle13[node1]] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
            } 
            else if (node2Cycle13[i] != node2Cycle13[node1])              // merge cycles
            {
                if (cyclelength13[node2Cycle13[node1]] & 2)
                    deltaLb += m_wt;
                else if (cyclelength13[node2Cycle13[i]] & 2)
                    deltaLb += m_wt;
                else                                                          
                    deltaLb += 3 * m_wt - 4 * m_wr;
            }
            else if ((m_cycle13[node1] - m_cycle13[i]) % 2 == 0)              // cycle changes sign
                deltaLb += (cyclelength13[node2Cycle13[node1]] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
            else if (!(cyclelength13[node2Cycle13[node1]] & 2) || (!(abs(m_cycle13[i] - m_cycle13[node1]) & 2))) 
                deltaLb -= m_wt;                                         // split in two cycles
            else 
                deltaLb += 4 * m_wr - 3 * m_wt;                              // split in two cycles
            // pairwise distances: perm2 vs perm3
            if ((f_sol->m_edges2[node1] == i) && (f_sol->m_edges3[node1] == i))   // both edges parallel to m
                deltaLb += m_wt;
            else if (f_sol->m_edges2[node1] == i)                               // one edge (2) parallel to m
            {
                if (!f_sol->m_even2[node1])                                     // else no change in lb
                    deltaLb += (cyclelength23[node2Cycle23[node1]] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
            } 
            else if (f_sol->m_edges3[node1] == i)                               // one edge (3) parallel to m
            {
                if (!f_sol->m_even3[node1])                                     // else no change in lb
                    deltaLb += (cyclelength23[node2Cycle23[node1]] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
            } 
            else if (node2Cycle23[i] != node2Cycle23[node1])              // merge cycles
            {
                if (cyclelength23[node2Cycle23[node1]] & 2)
                    deltaLb += m_wt;
                else if (cyclelength23[node2Cycle23[i]] & 2)
                    deltaLb += m_wt;
                else                                                          
                    deltaLb += 3 * m_wt - 4 * m_wr;
            }
            else if ((m_cycle23[node1] - m_cycle23[i]) % 2 == 0)              // cycle changes sign
                deltaLb += (cyclelength23[node2Cycle23[node1]] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
            else if (!(cyclelength23[node2Cycle23[node1]] & 2) || (!(abs(m_cycle23[i] - m_cycle23[node1]) & 2))) 
                deltaLb -= m_wt;                                         // split in two cycles
            else 
                deltaLb += 4 * m_wr - 3 * m_wt;                              // split in two cycles
            // add to the two score entries
            scores[node1].first += deltaLb;
            scores[i].first += deltaLb;
        }
    }
    // sort the scores
    sort(scores, &(scores[2 * m_size]));
    // inverse copy the result
    for (int i = 0; i < 2 * m_size; ++i)
        m_nodeOrder[i] = scores[2 * m_size - 1 - i].second;
    // clean up    
    delete[] scores;
    delete[] node2Cycle12;
    delete[] node2Cycle13;
    delete[] node2Cycle23;
    delete[] cyclelength12;
    delete[] cyclelength13;
    delete[] cyclelength23;
}

      
void Median::createNodeOrderRev(Solution* f_sol)
{
    pair<int, int>* scores = new pair<int, int>[2 * m_size];    // array which
                // contains for each node its score (first) and its index (second)
    int node1;        // first node of a cycle
    int pos;          // a position in a cycle
    int deltaLb;      // change of the lower bound due to an extension (+ fixed bias)
    int* node2Cycle12 = new int[2 * m_size];      // maps each node to a cycle index between perm1 and perm2
    int* node2Cycle13 = new int[2 * m_size];      // maps each node to a cycle index between perm1 and perm3
    int* node2Cycle23 = new int[2 * m_size];      // maps each node to a cycle index between perm2 and perm3
                
    // init some data
    for (int i = 0; i < 2 * m_size; ++i)
    {
        scores[i].first = 0;                
        scores[i].second = i;
        m_cycle12[i] = 0;
        m_cycle13[i] = 0;
        m_cycle23[i] = 0;
    }
    // precalculate cycle information 
    // find cycles between perm1 and perm2
    node1 = 0;
    for (int i = 0; true; ++i)
    {
        while ((node1 < 2 * m_size) && (m_cycle12[node1] != 0))
            node1++;
        if (node1 == 2 * m_size)
            break;    
        pos = node1;
        do
        {
            m_cycle12[f_sol->m_edges1[pos]] = m_cycle12[pos] + 1;
            pos = f_sol->m_edges1[pos];
            node2Cycle12[pos] = i;
            m_cycle12[f_sol->m_edges2[pos]] = m_cycle12[pos] + 1;
            pos = f_sol->m_edges2[pos];        
            node2Cycle12[pos] = i;
        } while (pos != node1);
    }
    // find cycles between perm1 and perm3
    node1 = 0;
    for (int i = 0; true; ++i)
    {
        while ((node1 < 2 * m_size) && (m_cycle13[node1] != 0))
            node1++;
        if (node1 == 2 * m_size)
            break;    
        pos = node1;
        do
        {
            m_cycle13[f_sol->m_edges1[pos]] = m_cycle13[pos] + 1;
            pos = f_sol->m_edges1[pos];
            node2Cycle13[pos] = i;
            m_cycle13[f_sol->m_edges3[pos]] = m_cycle13[pos] + 1;
            pos = f_sol->m_edges3[pos];        
            node2Cycle13[pos] = i;
        } while (pos != node1);
    }
    // find cycles between perm2 and perm3
    node1 = 0;
    for (int i = 0; true; ++i)
    {
        while ((node1 < 2 * m_size) && (m_cycle23[node1] != 0))
            node1++;
        if (node1 == 2 * m_size)
            break;    
        pos = node1;
        do
        {
            m_cycle23[f_sol->m_edges2[pos]] = m_cycle23[pos] + 1;
            pos = f_sol->m_edges2[pos];
            node2Cycle23[pos] = i;
            m_cycle23[f_sol->m_edges3[pos]] = m_cycle23[pos] + 1;
            pos = f_sol->m_edges3[pos];        
            node2Cycle23[pos] = i;
        } while (pos != node1);
    }
    // calculate the scores; the score of a node is the sum of the lower bounds of its extensions
    // -> calculate the score of all extensions
    for (node1 = 0; node1 < 2 * m_size; ++node1)
    {
        // check scores for expansion node1 - i
        for (int i = node1 + 1; i < m_size * 2; (m_distMode == 1? i += 2 : i++))
        {
            if ((node1 ^ i) == 1)                      // short cycle -> not a permutation matching
                continue;
            deltaLb = 0;
            // do not add the basic change for step +=1, node -= 1, as its always the same
            // edges parallel to contracted edge
            if (f_sol->m_edges1[node1] == i)
                deltaLb -= 2;
            if (f_sol->m_edges2[node1] == i)
                deltaLb -= 2;
            if (f_sol->m_edges3[node1] == i)
                deltaLb -= 2;
            // pairwise distances: perm1 vs perm2
            if ((f_sol->m_edges1[node1] == i) && (f_sol->m_edges2[node1] == i))   // both edges parallel to m
                deltaLb += 1;
            else if ((f_sol->m_edges1[node1] == i) || (f_sol->m_edges2[node1] == i));  // one edge parallel to m -> no change in score
            else if (node2Cycle12[i] != node2Cycle12[node1])              // merge cycles
                deltaLb += 1;
            else if ((m_cycle12[node1] - m_cycle12[i]) % 2 != 0)              // split in two cycles
                deltaLb -= 1;
            // pairwise distances: perm1 vs perm3
            if ((f_sol->m_edges1[node1] == i) && (f_sol->m_edges3[node1] == i))   // both edges parallel to m
                deltaLb += 1;
            else if ((f_sol->m_edges1[node1] == i) || (f_sol->m_edges3[node1] == i));  // one edge parallel to m -> no change in score
            else if (node2Cycle13[i] != node2Cycle13[node1])              // merge cycles
                deltaLb += 1;
            else if ((m_cycle13[node1] - m_cycle13[i]) % 2 != 0)              // split in two cycles
                deltaLb -= 1;
            // pairwise distances: perm2 vs perm3
            if ((f_sol->m_edges2[node1] == i) && (f_sol->m_edges3[node1] == i))   // both edges parallel to m
                deltaLb += 1;
            else if ((f_sol->m_edges1[node1] == i) || (f_sol->m_edges2[node1] == i));  // one edge parallel to m -> no change in score
            else if (node2Cycle23[i] != node2Cycle23[node1])              // merge cycles
                deltaLb += 1;
            else if ((m_cycle23[node1] - m_cycle23[i]) % 2 != 0)              // split in two cycles
                deltaLb -= 1;
            // add to the two score entries
            scores[node1].first += deltaLb;
            scores[i].first += deltaLb;
        }
    }
    // sort the scores
    sort(scores, &(scores[2 * m_size]));
    // inverse copy the result
    for (int i = 0; i < 2 * m_size; ++i)
        m_nodeOrder[i] = scores[2 * m_size - 1 - i].second;
    // clean up    
    delete[] scores;
    delete[] node2Cycle12;
    delete[] node2Cycle13;
    delete[] node2Cycle23;
}


void Median::expand()
{
    Solution* exp = m_heap[0];        // solution to expand
    int node1;                        // node to expand
    int nodeA, nodeB;                 // neighbors of contracted edge
    Solution* sol;                    // a new solution
    int pos;                          // position in the cycle
    bool length;                      // length of a cycle (0: odd, 1: even)
    bool down = true;                 // use a heapify downwards for the first expansion

#if DEBUG_MEDIAN    
    m_numExpansions++;
#endif
    // find node for expanding
    node1 = exp->m_steps;             // previous nodes are always expanded
    while (exp->m_median[m_nodeOrder[node1]] != -1)
        node1++;
    node1 = m_nodeOrder[node1];
    // for the score calculation, we need the cycles of the pairwise comparison
    // that include node1. Nodes set to 0 are not in the cycle
    pos = node1;    
    // genome 1 vs genome 2
    for (int i = 0; i < m_size * 2; i++)
        m_cycle12[i] = 0;
    do
    {
        m_cycle12[exp->m_edges1[pos]]= (exp->m_even1[pos]?         
          m_cycle12[pos] + 1 : m_cycle12[pos] + 3);
        pos = exp->m_edges1[pos];
        m_cycle12[exp->m_edges2[pos]]= (exp->m_even2[pos]?         
          m_cycle12[pos] + 1 : m_cycle12[pos] + 3);
        pos = exp->m_edges2[pos];        
    } while (pos != node1);    
    // genome 1 vs genome 3
    for (int i = 0; i < m_size * 2; i++)
        m_cycle13[i] = 0;
    do
    {
        m_cycle13[exp->m_edges1[pos]]= (exp->m_even1[pos]?         
          m_cycle13[pos] + 1 : m_cycle13[pos] + 3);
        pos = exp->m_edges1[pos];
        m_cycle13[exp->m_edges3[pos]]= (exp->m_even3[pos]?         
          m_cycle13[pos] + 1 : m_cycle13[pos] + 3);
        pos = exp->m_edges3[pos];        
    } while (pos != node1);    
    // genome 2 vs genome 3
    for (int i = 0; i < m_size * 2; i++)
        m_cycle23[i] = 0;
    do
    {
        m_cycle23[exp->m_edges2[pos]]= (exp->m_even2[pos]?         
          m_cycle23[pos] + 1 : m_cycle23[pos] + 3);
        pos = exp->m_edges2[pos];
        m_cycle23[exp->m_edges3[pos]]= (exp->m_even3[pos]?         
          m_cycle23[pos] + 1 : m_cycle23[pos] + 3);
        pos = exp->m_edges3[pos];        
    } while (pos != node1);   
    // try all possible expansions
    for (int i = (m_distMode == 1? 1 - (node1 & 1) : 0); i < 2 * m_size; (m_distMode == 1? i += 2 : ++i))
    {
        if (i == node1)
            continue;
        if (exp->m_median[i] != -1)    // node already used
            continue;
        if (shortCycle(exp->m_median, node1, i))   // not a permutation matching
            continue;
        // create new partial solution    
        sol = m_solutionPool->getSolution();
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_edges1[j] = exp->m_edges1[j];
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_edges2[j] = exp->m_edges2[j];
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_edges3[j] = exp->m_edges3[j];
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_median[j] = exp->m_median[j];
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_even1[j] = exp->m_even1[j];
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_even2[j] = exp->m_even2[j];
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_even3[j] = exp->m_even3[j];
        // contract edge
        sol->m_median[node1] = i;
        sol->m_median[i] = node1;
        nodeA = sol->m_edges1[node1];
        nodeB = sol->m_edges1[i];
        sol->m_edges1[nodeA] = nodeB;
        sol->m_edges1[nodeB] = nodeA;
        sol->m_even1[nodeA] ^= sol->m_even1[nodeB];
        sol->m_even1[nodeB] = sol->m_even1[nodeA];
        nodeA = sol->m_edges2[node1];
        nodeB = sol->m_edges2[i];
        sol->m_edges2[nodeA] = nodeB;
        sol->m_edges2[nodeB] = nodeA;
        sol->m_even2[nodeA] ^= sol->m_even2[nodeB];
        sol->m_even2[nodeB] = sol->m_even2[nodeA];
        nodeA = sol->m_edges3[node1];
        nodeB = sol->m_edges3[i];
        sol->m_edges3[nodeA] = nodeB;
        sol->m_edges3[nodeB] = nodeA;
        sol->m_even3[nodeA] ^= sol->m_even3[nodeB];
        sol->m_even3[nodeB] = sol->m_even3[nodeA];
        sol->m_steps = exp->m_steps + 1;
        // update the score:
        // step += 1, n -= 1
        sol->m_lb = exp->m_lb + 3 * m_wt;
        // edges parallel to contracted edge
        if (exp->m_edges1[node1] == i)
            sol->m_lb -= (exp->m_even1[node1]? 2 * m_wt : 4 * (m_wt - m_wr)); 
        if (exp->m_edges2[node1] == i)
            sol->m_lb -= (exp->m_even2[node1]? 2 * m_wt : 4 * (m_wt - m_wr)); 
        if (exp->m_edges3[node1] == i)
            sol->m_lb -= (exp->m_even3[node1]? 2 * m_wt : 4 * (m_wt - m_wr));
        // pairwise distances: perm1 vs perm2
        if ((exp->m_edges1[node1] == i) && (exp->m_edges2[node1] == i))   // both edges parallel to m
            sol->m_lb += (exp->m_even1[node1] == exp->m_even2[node1]? m_wt : 2 * (m_wt - m_wr));
        else if (exp->m_edges1[node1] == i)                               // one edge (1) parallel to m
        {
            if (!exp->m_even1[node1])                                     // else no change in lb
                sol->m_lb += (m_cycle12[node1] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
        } 
        else if (exp->m_edges2[node1] == i)                               // one edge (2) parallel to m
        {
            if (!exp->m_even2[node1])                                     // else no change in lb
                sol->m_lb += (m_cycle12[node1] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
        } 
        else if (m_cycle12[i] == 0)                                       // merge cycles
        {
            if (m_cycle12[node1] & 2)
                sol->m_lb += m_wt;
            else                                                          // get length of second cycle
            {
                length = true;
                pos = i;
                do
                {
                    if (!exp->m_even1[pos])
                        length = !length;
                    pos = exp->m_edges1[pos];
                    if (exp->m_even2[pos])
                        length = !length;
                    pos = exp->m_edges2[pos];                    
                } while (pos != i);
                sol->m_lb += (length? 3 * m_wt - 4 * m_wr : m_wt);
            }
        }
        else if (m_cycle12[exp->m_edges2[i]] < m_cycle12[exp->m_edges1[i]])  // cycle changes sign
            sol->m_lb += (m_cycle12[node1] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
        else if ((!(m_cycle12[node1] & 2)) ||  (!(m_cycle12[i] & 2)))        // split in two cycles
            sol->m_lb -= m_wt;
        else 
            sol->m_lb += 4 * m_wr - 3 * m_wt;                              // split in two cycles
        // pairwise distances: perm1 vs perm3
        if ((exp->m_edges1[node1] == i) && (exp->m_edges3[node1] == i))   // both edges parallel to m
            sol->m_lb += (exp->m_even1[node1] == exp->m_even3[node1]? m_wt : 2 * (m_wt - m_wr));
        else if (exp->m_edges1[node1] == i)                               // one edge (1) parallel to m
        {
            if (!exp->m_even1[node1])                                     // else no change in lb
                sol->m_lb += (m_cycle13[node1] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
        } 
        else if (exp->m_edges3[node1] == i)                               // one edge (3) parallel to m
        {
            if (!exp->m_even3[node1])                                     // else no change in lb
                sol->m_lb += (m_cycle13[node1] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
        } 
        else if (m_cycle13[i] == 0)                                       // merge cycles
        {
            if (m_cycle13[node1] & 2)
                sol->m_lb += m_wt;
            else                                                          // get length of second cycle
            {
                length = true;
                pos = i;
                do
                {
                    if (!exp->m_even1[pos])
                        length = !length;
                    pos = exp->m_edges1[pos];
                    if (exp->m_even3[pos])
                        length = !length;
                    pos = exp->m_edges3[pos];                    
                } while (pos != i);
                sol->m_lb += (length? 3 * m_wt - 4 * m_wr : m_wt);
            }
        }
        else if (m_cycle13[exp->m_edges3[i]] < m_cycle13[exp->m_edges1[i]])  // cycle changes sign
            sol->m_lb += (m_cycle13[node1] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
        else if ((!(m_cycle13[node1] & 2)) ||  (!(m_cycle13[i] & 2)))        // split in two cycles
            sol->m_lb -= m_wt;
        else 
            sol->m_lb += 4 * m_wr - 3 * m_wt;                              // split in two cycles
        // pairwise distances: perm2 vs perm3
        if ((exp->m_edges2[node1] == i) && (exp->m_edges3[node1] == i))   // both edges parallel to m
            sol->m_lb += (exp->m_even2[node1] == exp->m_even3[node1]? m_wt : 2 * (m_wt - m_wr));
        else if (exp->m_edges2[node1] == i)                               // one edge (2) parallel to m
        {
            if (!exp->m_even2[node1])                                     // else no change in lb
                sol->m_lb += (m_cycle23[node1] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
        } 
        else if (exp->m_edges3[node1] == i)                               // one edge (3) parallel to m
        {
            if (!exp->m_even3[node1])                                     // else no change in lb
                sol->m_lb += (m_cycle23[node1] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
        } 
        else if (m_cycle23[i] == 0)                                       // merge cycles
        {
            if (m_cycle23[node1] & 2)
                sol->m_lb += m_wt;
            else                                                          // get length of second cycle
            {
                length = true;
                pos = i;
                do
                {
                    if (!exp->m_even2[pos])
                        length = !length;
                    pos = exp->m_edges2[pos];
                    if (exp->m_even3[pos])
                        length = !length;
                    pos = exp->m_edges3[pos];                    
                } while (pos != i);
                sol->m_lb += (length? 3 * m_wt - 4 * m_wr : m_wt);
            }
        }
        else if (m_cycle23[exp->m_edges3[i]] < m_cycle23[exp->m_edges2[i]])  // cycle changes sign
            sol->m_lb += (m_cycle23[node1] & 2? 2 * m_wr - m_wt : m_wt - 2 * m_wr);
        else if ((!(m_cycle23[node1] & 2)) ||  (!(m_cycle23[i] & 2)))        // split in two cycles
            sol->m_lb -= m_wt;
        else 
            sol->m_lb += 4 * m_wr - 3 * m_wt;                              // split in two cycles
#if DEBUG_MEDIAN
        // !!DEBUG
        int tmp = calcLowerBoundFromScratch(sol);        
        if (sol->m_lb != tmp)
        {
            cout << "ERROR!! lower bound mismatch! (" << sol->m_lb << " vs. " << tmp << ")\n";
            sol->dump(2 * m_size);
        }            
        if (sol->m_lb < exp->m_lb)
            cout << "ERROR!! exp -> sol " << exp->m_lb << " -> " << sol->m_lb << endl;
#endif                
        // add new solution to heap
        if (sol->m_lb <= m_upperBound)
        {
            if (down)
            {
                m_heap[0] = sol;
                heapifyDown();
                down = false;
            }
            else
            {
                m_heap.push_back(sol);
                heapifyUp();
                if (m_heap.size() > m_heapPeak)
                    m_heapPeak++; 
            }  
            if (m_heap.size() > m_heapMax)
            {
                if (m_heap[m_heap.size()-1]->m_lb < m_overlooked)
                    m_overlooked = m_heap[m_heap.size()-1]->m_lb;
                m_solutionPool->freeSolution(m_heap[m_heap.size() - 1]);
                m_heap.pop_back();
            }
#if DEBUG_MEDIAN
//            if (!isHeap())
//                cout << "ERROR!! heap condition violated!\n";
#endif                
        }
        else
            m_solutionPool->freeSolution(sol);
    }    
    // remove old solution
    m_solutionPool->freeSolution(exp);    
    // if all expansions are below the upper bound, we must fill the heap top
    if (down)
    {
        m_heap[0] = m_heap[m_heap.size()-1];
        m_heap.pop_back();
        heapifyDown();    
#if DEBUG_MEDIAN
//        if (!isHeap())
//            cout << "ERROR!! heap condition violated!\n";
#endif                
    }    
}    


void Median::expandRev()
{
    Solution* exp = m_heap[0];        // solution to expand
    int node1;                        // node to expand
    int nodeA, nodeB;                 // neighbors of contracted edge
    Solution* sol;                    // a new solution
    int pos;                          // position in the cycle
    bool down = true;                 // use a heapify downwards for the first expansion

    // find node for expanding
    node1 = exp->m_steps;             // previous nodes are always expanded
    while (exp->m_median[m_nodeOrder[node1]] != -1)
        node1++;
    node1 = m_nodeOrder[node1];
    // for the score calculation, we need the cycles of the pairwise comparison
    // that include node1. Nodes set to 0 are not in the cycle
    pos = node1;    
    // genome 1 vs genome 2
    for (int i = 0; i < m_size * 2; i++)
        m_cycle12[i] = 0;
    do
    {
        m_cycle12[exp->m_edges1[pos]] = m_cycle12[pos] + 1;
        pos = exp->m_edges1[pos];
        m_cycle12[exp->m_edges2[pos]] = m_cycle12[pos] + 1;
        pos = exp->m_edges2[pos];        
    } while (pos != node1);    
    // genome 1 vs genome 3
    for (int i = 0; i < m_size * 2; i++)
        m_cycle13[i] = 0;
    do
    {
        m_cycle13[exp->m_edges1[pos]] = m_cycle13[pos] + 1;
        pos = exp->m_edges1[pos];
        m_cycle13[exp->m_edges3[pos]] = m_cycle13[pos] + 1;
        pos = exp->m_edges3[pos];        
    } while (pos != node1);    
    // genome 2 vs genome 3
    for (int i = 0; i < m_size * 2; i++)
        m_cycle23[i] = 0;
    do
    {
        m_cycle23[exp->m_edges2[pos]] = m_cycle23[pos] + 1;
        pos = exp->m_edges2[pos];
        m_cycle23[exp->m_edges3[pos]] = m_cycle23[pos] + 1;
        pos = exp->m_edges3[pos];        
    } while (pos != node1);   
    // try all possible expansions
    for (int i = 0; i < m_size * 2; i++)
    {
        if (i == node1)
            continue;
        if (exp->m_median[i] != -1)    // node already used
            continue;
        if (shortCycle(exp->m_median, node1, i))   // not a permutation matching
            continue;
        // create new partial solution    
        sol = m_solutionPool->getSolution();
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_edges1[j] = exp->m_edges1[j];
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_edges2[j] = exp->m_edges2[j];
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_edges3[j] = exp->m_edges3[j];
        for (int j = 0; j < 2 * m_size; j++)
            sol->m_median[j] = exp->m_median[j];
        // contract edge
        sol->m_median[node1] = i;
        sol->m_median[i] = node1;
        nodeA = sol->m_edges1[node1];
        nodeB = sol->m_edges1[i];
        sol->m_edges1[nodeA] = nodeB;
        sol->m_edges1[nodeB] = nodeA;
        nodeA = sol->m_edges2[node1];
        nodeB = sol->m_edges2[i];
        sol->m_edges2[nodeA] = nodeB;
        sol->m_edges2[nodeB] = nodeA;
        nodeA = sol->m_edges3[node1];
        nodeB = sol->m_edges3[i];
        sol->m_edges3[nodeA] = nodeB;
        sol->m_edges3[nodeB] = nodeA;
        sol->m_steps = exp->m_steps + 1;
        // update the score:
        // step += 1, n -= 1
        sol->m_lb = exp->m_lb + 3;
        // edges parallel to contracted edge
        if (exp->m_edges1[node1] == i)
            sol->m_lb -= 2; 
        if (exp->m_edges2[node1] == i)
            sol->m_lb -= 2; 
        if (exp->m_edges3[node1] == i)
            sol->m_lb -= 2;
        // pairwise distances: perm1 vs perm2
        if ((exp->m_edges1[node1] == i) && (exp->m_edges2[node1] == i))   // both edges parallel to m
            sol->m_lb += 1;
        else if ((exp->m_edges1[node1] == i) || (exp->m_edges2[node1] == i)); // one edge  parallel to m -> no change in score
        else if (m_cycle12[i] == 0)                                       // merge cycles
            sol->m_lb += 1;
        else if (m_cycle12[exp->m_edges2[i]] > m_cycle12[exp->m_edges1[i]])  // split in two cycles
            sol->m_lb -= 1;
        // pairwise distances: perm1 vs perm3
        if ((exp->m_edges1[node1] == i) && (exp->m_edges3[node1] == i))   // both edges parallel to m
            sol->m_lb += 1;
        else if ((exp->m_edges1[node1] == i) || (exp->m_edges3[node1] == i)); // one edge  parallel to m -> no change in score
        else if (m_cycle13[i] == 0)                                       // merge cycles
            sol->m_lb += 1;
        else if (m_cycle13[exp->m_edges3[i]] > m_cycle13[exp->m_edges1[i]])  // split in two cycles
            sol->m_lb -= 1;
        // pairwise distances: perm2 vs perm3
        if ((exp->m_edges2[node1] == i) && (exp->m_edges3[node1] == i))   // both edges parallel to m
            sol->m_lb += 1;
        else if ((exp->m_edges2[node1] == i) || (exp->m_edges3[node1] == i)); // one edge  parallel to m -> no change in score
        else if (m_cycle23[i] == 0)                                       // merge cycles
            sol->m_lb += 1;
        else if (m_cycle23[exp->m_edges3[i]] > m_cycle23[exp->m_edges2[i]])  // split in two cycles
            sol->m_lb -= 1;
#if DEBUG_MEDIAN      
        // !!DEBUG
        int tmp = calcLowerBoundFromScratchRev(sol);        
        if (sol->m_lb != tmp)
        {
            cout << "ERROR!! lower bound mismatch! (" << sol->m_lb << " vs. " << tmp << ")\n";
            sol->dump(2 * m_size);
        }            
        if (sol->m_lb < exp->m_lb)
            cout << "ERROR!! exp -> sol " << exp->m_lb << " -> " << sol->m_lb << endl;
#endif
        // add new solution to heap
        if (sol->m_lb <= m_upperBound)
        {
            if (down)
            {
                m_heap[0] = sol;
                heapifyDown();
                down = false;
            }
            else
            {
                m_heap.push_back(sol);
                heapifyUp();
                if (m_heap.size() > m_heapPeak)
                    m_heapPeak++; 
            }  
            if (m_heap.size() > m_heapMax)
            {
                if (m_heap[m_heap.size()-1]->m_lb < m_overlooked)
                    m_overlooked = m_heap[m_heap.size()-1]->m_lb;
                m_solutionPool->freeSolution(m_heap[m_heap.size() - 1]);
                m_heap.pop_back();
            }
#if DEBUG_MEDIAN      
//            if (!isHeap())
//                cout << "ERROR!! heap condition violated!\n";
#endif
        }
        else
            m_solutionPool->freeSolution(sol);
    }    
    // remove old solution
    m_solutionPool->freeSolution(exp);
    // if all expansions are below the upper bound, we must fill the heap top
    if (down)
    {
        m_heap[0] = m_heap[m_heap.size()-1];
        m_heap.pop_back();
        heapifyDown();    
#if DEBUG_MEDIAN      
//        if (!isHeap())
//            cout << "ERROR!! heap condition violated!\n";
#endif
    }    
}    


bool Median::shortCycle(const int* f_edges, int f_node1, int f_node2)
{
    int pos = f_node1;     // position in the graph
    
    // follow H
    pos & 1? pos-- : pos++;
    if ((pos == f_node2) && (m_size > 1))  // short cycle
        return true;
    for (int i = 2; i < m_size; i++)
    {
        if (f_edges[pos] == -1)    // we are on a path, not a cycle
            return false;        
        // follow f_edges
        pos = f_edges[pos];
        // follow H
        pos & 1? pos-- : pos++;
        if (pos == f_node2)        // short cycle
            return true;
    }
    return false;    
}


void Median::heapifyDown()
{
    Solution* swap;                        // solution for swapping 
    unsigned int pos = 0;                  // actual position in the heap
    unsigned int child = 1;                // child of actual element
    
    while (child < m_heap.size())
    {
        // get smaller child
        if ((child + 1 < m_heap.size()) && ((m_heap[child+1]->m_lb < m_heap[child]->m_lb) 
          || ((m_heap[child+1]->m_lb == m_heap[child]->m_lb) && (m_heap[child+1]->m_steps > m_heap[child]->m_steps))))
            child++;
        // compare with child
        if ((m_heap[child]->m_lb < m_heap[pos]->m_lb) || (((m_heap[child]->m_lb == m_heap[pos]->m_lb)) 
          && (m_heap[child]->m_steps > m_heap[pos]->m_steps)))       
        {
            // swap
            swap = m_heap[pos];
            m_heap[pos] = m_heap[child];
            m_heap[child] = swap;
            pos = child;
            child = 2 * pos + 1;
        }
        else          // heap condition fulfilled
            return;
    }
}
    
    
void Median::heapifyUp()
{
    Solution* swap;                        // solution for swapping 
    unsigned int pos = m_heap.size() - 1;  // actual position in the heap
    unsigned int parent;                   // parent of pos
    
    while (pos != 0)
    {
        parent = (pos - 1) / 2;
        // compare with parent 
        if ((m_heap[pos]->m_lb < m_heap[parent]->m_lb) || (((m_heap[pos]->m_lb == m_heap[parent]->m_lb)) 
          && (m_heap[pos]->m_steps > m_heap[parent]->m_steps)))       
        {
            // swap
            swap = m_heap[pos];
            m_heap[pos] = m_heap[parent];
            m_heap[parent] = swap;
            pos = parent;
        }
        else          // heap condition fulfilled
            return;
    }  
}        


bool Median::verifyWeight()
{
    Solution* sol = m_heap[0];        // solution to verifiy
    int pos;                          // position in the cycle
    int lb1, lb2, lb3;                // lower bound for exact comparisons
    int d1, d2, d3;                   // distances for exact comparison
    int dtmp;                         // temporary distance for exact comparison    
    const vector<minswrt::Operation>* sorting; // sorting sequence
    int diff;                         // remaining difference to predicted bound 
         
    // create the median
    m_perm[0] = 1;
    pos = 1;
    for (int i = 1; i < m_size; i++)
    {
        pos = sol->m_median[pos];
        if (pos & 1)                      // connected to right point
        {
            m_perm[i] = -((pos + 1) / 2);
            pos--;
        } 
        else                                // connected to left point
        {
            m_perm[i] = (pos + 2) / 2;
            pos++;
        }
    }
    // calculate the distances
    if (m_distMode == 0)
    {
        d1 = m_revdist.getDistance(m_input[0], m_perm);
        d2 = m_revdist.getDistance(m_input[1], m_perm);
        d3 = m_revdist.getDistance(m_input[2], m_perm);
        if (sol->m_lb / 2 < d1 + d2 + d3)            
        {
            sol->m_lb = 2 * (d1 + d2 + d3);
            heapifyDown();
            return false;
        }     
        else
        {
            sol->m_lb = 2 * (d1 + d2 + d3);
            m_d1 = d1;
            m_d2 = d2;
            m_d3 = d3;
            return true;
        }
    }
    else
    {
        m_minswrt.setTarget(m_perm);
        m_minswrt.setOrigin(m_input[0]);
        lb1 = (int)(m_minswrt.getLowerBound() + 0.01f);
        d1 = 0;
        m_minswrt.sort();
        sorting = m_minswrt.getResult();
        for (unsigned int j = 0; j < sorting->size(); j++)
            d1 += ((*sorting)[j].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
        m_minswrt.setOrigin(m_input[1]);
        lb2 = (int)(m_minswrt.getLowerBound() + 0.01f);
        d2 = 0;
        m_minswrt.sort();
        sorting = m_minswrt.getResult();
        for (unsigned int j = 0; j < sorting->size(); j++)
            d2 += ((*sorting)[j].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
        m_minswrt.setOrigin(m_input[2]);
        lb3 = (int)(m_minswrt.getLowerBound() + 0.01f);
        d3 = 0;
        m_minswrt.sort();
        sorting = m_minswrt.getResult();
        for (unsigned int j = 0; j < sorting->size(); j++)
            d3 += ((*sorting)[j].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
        m_minswrt.setOrigin(m_perm);
        if (d1 != lb1)    // try to optimize
        {
            m_minswrt.setTarget(m_input[0]);    
            dtmp = 0;
            m_minswrt.sort();
            sorting = m_minswrt.getResult();
            for (unsigned int j = 0; j < sorting->size(); j++)
                dtmp += ((*sorting)[j].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
            if (dtmp < d1)
                d1 = dtmp;
        }    
        if (d2 != lb2)    // try to optimize
        {
            m_minswrt.setTarget(m_input[1]);    
            dtmp = 0;
            m_minswrt.sort();
            sorting = m_minswrt.getResult();
            for (unsigned int j = 0; j < sorting->size(); j++)
                dtmp += ((*sorting)[j].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
            if (dtmp < d2)
                d2 = dtmp;
        }    
        if (d3 != lb3)    // try to optimize
        {
            m_minswrt.setTarget(m_input[2]);    
            dtmp = 0;
            m_minswrt.sort();
            sorting = m_minswrt.getResult();
            for (unsigned int j = 0; j < sorting->size(); j++)
                dtmp += ((*sorting)[j].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt);
            if (dtmp < d3)
                d3 = dtmp;
        }     
        diff = sol->m_lb / 4 - (lb1 + lb2 + lb3);
        if (!m_exact)
        { 
            if (sol->m_lb / 4 < d1 + d2 + d3)
            {
                sol->m_lb = 4 * (d1 + d2 + d3);
                heapifyDown();
                return false;
            }     
            else
            {
                sol->m_lb = 4 * (d1 + d2 + d3);
                m_d1 = d1;
                m_d2 = d2;
                m_d3 = d3;
                return true;
            }
        }
        if (d1 != lb1)      // further optimize
        {
            m_weightedbb.setPermutations(m_size, m_perm, m_input[0]);
            m_weightedbb.sort(lb1 + diff);
            d1 = m_weightedbb.getDistance();
            if (d1 == -1)        // no solution found
            {
                sol->m_lb += 4;
                heapifyDown();
                return false;
            }
            else if (d1 != -2)
                diff -= (d1 - lb1);
        }
        if (d2 != lb2)      // further optimize
        {
            m_weightedbb.setPermutations(m_size, m_perm, m_input[1]);
            m_weightedbb.sort(lb2 + diff);
            d2 = m_weightedbb.getDistance();
            if (d2 == -1)        // no solution found
            {
                sol->m_lb += 4;
                heapifyDown();
                return false;
            }
            else if (d2 != -2)
                diff -= (d2 - lb2);
        }
        if (d3 != lb3)      // further optimize
        {
            m_weightedbb.setPermutations(m_size, m_perm, m_input[2]);
            m_weightedbb.sort(lb3 + diff);
            d3 = m_weightedbb.getDistance();
            if (d3 == -1)        // no solution found
            {
                sol->m_lb += 4;
                heapifyDown();
                return false;
            }
            else if (d3 != -2)
                diff -= (d3 - lb3);
        }
        if ((d1 == -2) || (d2 == -2) || (d3 == -2))   // cannot calc this solution exactly -> remove
        {
            if (sol->m_lb < m_overlooked)
                m_overlooked = sol->m_lb;
            m_solutionPool->freeSolution(sol);
            m_heap[0] = m_heap[m_heap.size()-1];
            heapifyDown();
            return false;    
        }
        sol->m_lb = 4 * (d1 + d2 + d3);
        m_d1 = d1;
        m_d2 = d2;
        m_d3 = d3;
        return true;
    }
}


/***************************************************************************
 * debugging methods
 ***************************************************************************/
#if DEBUG_MEDIAN

int Median::calcLowerBoundFromScratch(Solution* f_sol)
{
    int score;         // the resulting score
    int pos;           // position in the cycle
    int length;

        
    // basic value: 3 n wt + 6 step wt = (size + steps) * 3 wt
    score =  (m_size + f_sol->m_steps) * 3 * m_wt;
    // cycles of perm1 and median
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;        // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (f_sol->m_median[i] == -1)    // not in a cycle
            continue;                    
        if (m_boolArray2[i])             // already visited
            continue;
        // follow the path
        length = 0;
        pos = i;
        do
        {
            if (m_boolArray2[pos])              // not a cycle but a path
            {
                length = -1;
                break;
            }
            m_boolArray2[pos] = true;
            pos = f_sol->m_median[pos];
            m_boolArray2[pos] = true;
            pos = m_perm1[pos];
            if (f_sol->m_median[pos] == -1)   // not a cycle but a path
            {
                length = -1;
                break;
            }
            else
                length++;
        } while (pos != i);
        if (length > 0) 
        {
            if (length & 1)                   // odd cycle
                score -= 2 * m_wt;
            else                              // even cycle
                score -= 4 * (m_wt - m_wr);
        }                
    } 
    // cycles of perm2 and median
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;        // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (f_sol->m_median[i] == -1)    // not in a cycle
            continue;                    
        if (m_boolArray2[i])             // already visited
            continue;
        // follow the path
        length = 0;
        pos = i;
        do
        {
            if (m_boolArray2[pos])              // not a cycle but a path
            {
                length = -1;
                break;
            }
            m_boolArray2[pos] = true;
            pos = f_sol->m_median[pos];
            m_boolArray2[pos] = true;
            pos = m_perm2[pos];
            if (f_sol->m_median[pos] == -1)   // not a cycle but a path
            {
                length = -1;
                break;
            }
            else
                length++;
        } while (pos != i);
        if (length > 0) 
        {
            if (length & 1)                   // odd cycle
                score -= 2 * m_wt;
            else                              // even cycle
                score -= 4 * (m_wt - m_wr);
        }                
    } 
    // cycles of perm3 and median
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;        // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (f_sol->m_median[i] == -1)    // not in a cycle
            continue;                    
        if (m_boolArray2[i])             // already visited
            continue;
        // follow the path
        length = 0;
        pos = i;
        do
        {
            if (m_boolArray2[pos])              // not a cycle but a path
            {
                length = -1;
                break;
            }
            m_boolArray2[pos] = true;
            pos = f_sol->m_median[pos];
            m_boolArray2[pos] = true;
            pos = m_perm3[pos];
            if (f_sol->m_median[pos] == -1)   // not a cycle but a path
            {
                length = -1;
                break;
            }
            else
                length++;
        } while (pos != i);
        if (length > 0) 
        {
            if (length & 1)                   // odd cycle
                score -= 2 * m_wt;
            else                              // even cycle
                score -= 4 * (m_wt - m_wr);
        }        
    } 
    // cycles between perm1 and perm2
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;              // use as visited array
    for (int i = 0; i < 2 * m_size; i++)    
    {
        if (m_boolArray2[i])
            continue;                         // already visited
        if (f_sol->m_median[i] != -1)         // edge contracted
            continue;
        // follow the cycle
        length = 0;
        pos = i;
        do
        {
            m_boolArray2[pos] = true;
            if (f_sol->m_even1[pos])
                length += 1;
            else
                length += 3;
            pos = f_sol->m_edges1[pos];    
            m_boolArray2[pos] = true;
            if (f_sol->m_even2[pos])
                length += 1;
            else
                length += 3;
            pos = f_sol->m_edges2[pos];    
        } while (pos != i);
        if ((length / 2) & 1)                 // odd cycle
            score -= m_wt;
        else
            score -= 2 * (m_wt - m_wr);
    }
//    cout << score << " ";
    // cycles between perm1 and perm3
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;              // use as visited array
    for (int i = 0; i < 2 * m_size; i++)    
    {
        if (m_boolArray2[i])
            continue;                         // already visited
        if (f_sol->m_median[i] != -1)         // edge contracted
            continue;
        // follow the cycle
        length = 0;
        pos = i;
        do
        {
            m_boolArray2[pos] = true;
            if (f_sol->m_even1[pos])
                length += 1;
            else
                length += 3;
            pos = f_sol->m_edges1[pos];    
            m_boolArray2[pos] = true;
            if (f_sol->m_even3[pos])
                length += 1;
            else
                length += 3;
            pos = f_sol->m_edges3[pos];    
        } while (pos != i);
        if ((length / 2) & 1)                 // odd cycle
            score -= m_wt;
        else
            score -= 2 * (m_wt - m_wr);
    }
    // cycles between perm2 and perm3
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;              // use as visited array
    for (int i = 0; i < 2 * m_size; i++)    
    {
        if (m_boolArray2[i])
            continue;                         // already visited
        if (f_sol->m_median[i] != -1)         // edge contracted
            continue;
        // follow the cycle
        length = 0;
        pos = i;
        do
        {
            m_boolArray2[pos] = true;
            if (f_sol->m_even2[pos])
                length += 1;
            else
                length += 3;
            pos = f_sol->m_edges2[pos];    
            m_boolArray2[pos] = true;
            if (f_sol->m_even3[pos])
                length += 1;
            else
                length += 3;
            pos = f_sol->m_edges3[pos];    
        } while (pos != i);
        if ((length / 2) & 1)                 // odd cycle
            score -= m_wt;
        else
            score -= 2 * (m_wt - m_wr);
    }
    return score;    
}   


int Median::calcLowerBoundFromScratchRev(Solution* f_sol)
{
    int score;         // the resulting score
    int pos;           // position in the cycle
    bool isCycle;      // true if we are on a cycle

    // basic value: 3 n + 6 step = (size + steps) * 3
    score =  (m_size + f_sol->m_steps) * 3;
    // cycles of perm1 and median
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;        // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (f_sol->m_median[i] == -1)    // not in a cycle
            continue;                    
        if (m_boolArray2[i])             // already visited
            continue;
        // follow the path
        isCycle = true;
        pos = i;
        do
        {
            if (m_boolArray2[pos])              // not a cycle but a path
            {
                isCycle = false;
                break;
            }
            m_boolArray2[pos] = true;
            pos = f_sol->m_median[pos];
            m_boolArray2[pos] = true;
            pos = m_perm1[pos];
            if (f_sol->m_median[pos] == -1)   // not a cycle but a path
            {
                isCycle = false;
                break;
            }
        } while (pos != i);
        if (isCycle) 
            score -= 2;
    } 
//    cout << score << " ";
    // cycles of perm2 and median
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;        // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (f_sol->m_median[i] == -1)    // not in a cycle
            continue;                    
        if (m_boolArray2[i])             // already visited
            continue;
        // follow the path
        isCycle = true;
        pos = i;
        do
        {
            if (m_boolArray2[pos])              // not a cycle but a path
            {
                isCycle = false;
                break;
            }
            m_boolArray2[pos] = true;
            pos = f_sol->m_median[pos];
            m_boolArray2[pos] = true;
            pos = m_perm2[pos];
            if (f_sol->m_median[pos] == -1)   // not a cycle but a path
            {
                isCycle = false;
                break;
            }
        } while (pos != i);
        if (isCycle) 
            score -= 2;
    } 
    // cycles of perm3 and median
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;        // use as visited array
    for (int i = 0; i < 2 * m_size; i++)
    {
        if (f_sol->m_median[i] == -1)    // not in a cycle
            continue;                    
        if (m_boolArray2[i])             // already visited
            continue;
        // follow the path
        isCycle = true;
        pos = i;
        do
        {
            if (m_boolArray2[pos])              // not a cycle but a path
            {
                isCycle = false;
                break;
            }
            m_boolArray2[pos] = true;
            pos = f_sol->m_median[pos];
            m_boolArray2[pos] = true;
            pos = m_perm3[pos];
            if (f_sol->m_median[pos] == -1)   // not a cycle but a path
            {
                isCycle = false;
                break;
            }
        } while (pos != i);
        if (isCycle) 
            score -= 2;
    } 
//    cout << score << " ";
    // cycles between perm1 and perm2
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;              // use as visited array
    for (int i = 0; i < 2 * m_size; i++)    
    {
        if (m_boolArray2[i])
            continue;                         // already visited
        if (f_sol->m_median[i] != -1)         // edge contracted
            continue;
        // follow the cycle
        pos = i;
        do
        {
            m_boolArray2[pos] = true;
            pos = f_sol->m_edges1[pos];    
            m_boolArray2[pos] = true;
            pos = f_sol->m_edges2[pos];    
        } while (pos != i);
        score -= 1;
    }
    // cycles between perm1 and perm3
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;              // use as visited array
    for (int i = 0; i < 2 * m_size; i++)    
    {
        if (m_boolArray2[i])
            continue;                         // already visited
        if (f_sol->m_median[i] != -1)         // edge contracted
            continue;
        // follow the cycle
        pos = i;
        do
        {
            m_boolArray2[pos] = true;
            pos = f_sol->m_edges1[pos];    
            m_boolArray2[pos] = true;
            pos = f_sol->m_edges3[pos];    
        } while (pos != i);
        score -= 1;
    }
//    cout << score << " ";
    // cycles between perm2 and perm3
    for (int i = 0; i < 2 * m_size; i++)
        m_boolArray2[i] = false;              // use as visited array
    for (int i = 0; i < 2 * m_size; i++)    
    {
        if (m_boolArray2[i])
            continue;                         // already visited
        if (f_sol->m_median[i] != -1)         // edge contracted
            continue;
        // follow the cycle
        pos = i;
        do
        {
            m_boolArray2[pos] = true;
            pos = f_sol->m_edges2[pos];    
            m_boolArray2[pos] = true;
            pos = f_sol->m_edges3[pos];    
        } while (pos != i);
        score -= 1;
    }
    return score;    
}   


bool Median::isHeap()
{
    for (unsigned int i = 0; 2 * i + 1 < m_heap.size(); i++)
    {
        if ((m_heap[i]->m_lb > m_heap[2*i+1]->m_lb)
          || ((m_heap[i]->m_lb == m_heap[2*i+1]->m_lb)
          && (m_heap[i]->m_steps < m_heap[2*i+1]->m_steps)))
            return false;
        if (2 * i + 2 < m_heap.size())
        {      
            if ((m_heap[i]->m_lb > m_heap[2*i+2]->m_lb)
              || ((m_heap[i]->m_lb == m_heap[2*i+2]->m_lb)
              && (m_heap[i]->m_steps < m_heap[2*i+2]->m_steps)))
                return false;
        }
    }

    return true;    
}


bool Median::debug(Solution* f_sol)
{
    return true;
}    


#endif

/***************************************************************************/

} // end namespace median
