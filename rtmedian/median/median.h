/**
 * filename: median.h
 * author: Martin Bader
 * begin: 25.06.2007
 * last change: 20.08.2010
 *
 * Calculates the median between three genomes.
 * Either uses the reversal distance or the weighted reversal/transposition
 * distance. This is a pimped version of Capraras algorithm.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifndef MEDIAN_H_
#define MEDIAN_H_

#include <vector>
#include "minswrt.h"
#include "revDist.h"
#include "weightedbb.h"

#include <fstream>

namespace median
{

#define DEBUG_MEDIAN 0

// class forward declarations
class Solution;
class SolutionPool;


/**
 * The main class.
 */
class Median
{
    /***********************************************************************
     * member variables
     ***********************************************************************/
  private:
    /** Permutation size */
    int m_size;
    
    /** Pointer to the input genomes */
    int const ** m_input;
    
    /** Distance mode: 0: reversal only, 1: transposition only, 2: weighted reversal/
     * transposition distance */
    int m_distMode;
    
    /** TRUE if we are solving the problem exactly, i.e. pairwise distances are calculated
     * by an exact solver. */
    bool m_exact;    
    
    /** Weight of a reversal */
    int m_wr;
    
    /** Weight of an (inverted) transposition */
    int m_wt;
    
    /** An upper bound for the median weight */
    int m_upperBound;
    
    /** Pool for partial solutions */
    SolutionPool* m_solutionPool;
    
    /** The heap of partial solutions */
    std::vector<Solution*> m_heap;
    
    /** The maximum allowed heap size */
    unsigned int m_heapMax; 
     
    /** For statistical purposes: maximum number of solutions on the heap. */
    unsigned int m_heapPeak;
    
    /** By pruning the tree or aborting exact calculations, some solutions 
     * can get lost. This value indicates the weight of the possible best
     * lost solution. */
    int m_overlooked;  
    
    /** A boolean array of size 2 * permSize for multiple purposes; member
     * variable to prevent frequent allocation. */
    bool* m_boolArray2;
    
    /** For score update: a cycle between perm1 and perm2 */
    int* m_cycle12;
                  
    /** For score update: a cycle between perm1 and perm3 */
    int* m_cycle13;
                  
    /** For score update: a cycle between perm2 and perm3 */
    int* m_cycle23;
    
    /** A permutation (needed for exact distance calculation */ 
    int* m_perm;
    
    /** Distance last calculated median - perm1 */
    int m_d1;
    
    /** Distance last calculated median - perm2 */
    int m_d2;
    
    /** Distance last calculated median - perm3 */
    int m_d3;
    
    /** Order in which the nodes should be expanded */
    int* m_nodeOrder;
    
    /** Solver for pairwise distances (weighted reversal/transposition 
     * distance), approximation algorithm */
    minswrt::MinSWRT m_minswrt;
    
    /** Solver for pairwise distances (weighted reversal/transposition 
     * distance), branch n bound algorithm */
    weightedbb::WeightedBB m_weightedbb;
    
    /** Solver for pairwise distances (weighted reversal/transposition 
     * distance) */
    revDist::RevDist m_revdist;
                      
    // !!DEBUG
    /** Original edges of perm1, 2, 3 */
#if DEBUG_MEDIAN
    int* m_perm1;
    int* m_perm2;
    int* m_perm3;
    int m_numExpansions;
#endif    

                  
    /***********************************************************************
     * methods
     ***********************************************************************/     
  public:
    /**
     * Constructor. Please call init() after the call of the constructor.
     */
    Median();
        
    /**
     * Destructor.
     */
    ~Median();
    
    /**
     * Initializes the median solver.
     * @param f_size  the permutation size
     * @param f_wr  weight of a reversal
     * @param f_wt  weight of an (inverted) transposition
     * @param f_distMode  distance mode (0: reversals only; 1: transpositions only; 2: weighted reversal and transposition distance)
     * @param f_exact  TRUE if we want exact solutions (branch and bound for pairwise comparison)
     * @param f_heapMax  maximum allowed number of elements on the heap
     * @param f_pairwiseHeapMax  maximum allowes number of elements on the heap for exact pairwise comparison
     */
    void init(int f_size, int f_wr, int f_wt, int f_mode = 2, bool f_exact = false, unsigned int f_heapMax = 2000000, int f_pairwiseHeapMax = 2000000);

    /**
     * Solves the median problem. The solver must be initialized. f_result 
     * must be initialized with the correct size. The return value is the 
     * weight of the median.
     * If any upper bound for the median weight is known, you can set it with
     * the parameter f_upperBound. This will speed up the calculation. If there
     * is no median within the upper bound, -1 will be returned.
     * @param f_perm1  first permutation
     * @param f_perm2  second permutation
     * @param f_perm3  third permutation
     * @param f_result  the result will be written to here
     * @param f_upperBound  we search only for medians with weight <= this bound
     * @return  the weight of the median
     */
    int solve(const int* f_perm1, const int* f_perm2, const int* f_perm3, 
      int* f_result, int f_upperBound = INT_MAX);

    /**
     * Same as solve, but without clearing the heap in the end. Use this if 
     * you need all medians: first call getFirstSolution, then iterate over
     * getNextSolution until -1 is returned. Then call cleanHeap. 
     * @param f_perm1  first permutation
     * @param f_perm2  second permutation
     * @param f_perm3  third permutation
     * @param f_result  the result will be written to here
     * @param f_upperBound  we search only for medians with weight <= this bound
     * @return  the weight of the median
     */
    int getFirstSolution(const int* f_perm1, const int* f_perm2, const int* f_perm3, 
      int* f_result, int f_upperBound = INT_MAX);
      
    /**
     * Calculates another median with the same weight if there is any, 
     * otherwise returns -1. The new median will be written to f_result which
     * must be initialized.
     * @param f_result  the result will be written to here
     * @return  the weight of the median
     */
    int getNextSolution(int* f_result);  
    
    /**
     * Get the distances from the input permutations to the last created median.
     * Only call this if you have called solve, getFirstSolution, or getNextSolution.
     * The results will be written to f_d1, f_d2, f_d3, which are integer 
     * references.
     * @param f_d1  distance median - perm1
     * @param f_d2  distance median - perm2
     * @param f_d3  distance median - perm3
     */
    void getMedianDist(int& f_d1, int& f_d2, int& f_d3);
    
    /** 
     * Cleans up the heap. Call this after getting several medians with 
     * getFirstResult / getNextResult.
     */
    void cleanHeap();
     
    /**
     * Returns the maximum number of elements in the heap during the last 
     * solving.
     * @return  the heap peak
     */
    inline int getHeapPeak()
    {
        return m_heapPeak;
    }      
    
    /** 
     * Returns the least weight of a solution that might have been lost by some
     * pruning techniques.
     * @return  weight of best possible lost solution
     */
    int getLeastCutoff(); 
    
    /**
     * Returns the pairwise distance between two genomes. f_mode determines
     * what is calculated: 0 is exact (works only if m_exact is true, otherwise
     * returns an upper bound), 1 is lower bound, 2 is an upper bound.
     * @param f_perm0  first permutation
     * @param f_perm1  second permutation
     * @param f_mode  determines if solved exactly(0), as lower bound(1), or as upper bound(2)
     * @return  pairwise genome distance
     */ 
    int pairDist(const int* f_perm0, const int* f_perm1, int f_mode = 1);
     
  protected:
    /**
     * Finds a first solution and does not clean the heap. This is the main
     * part of solve / getFirstSolution. For a more detailed description, 
     * look at the comment of solve.   
     * @param f_perm1  first permutation
     * @param f_perm2  second permutation
     * @param f_perm3  third permutation
     * @param f_result  the result will be written to here
     * @param f_upperBound  we search only for medians with weight <= this bound
     * @return  the weight of the median
     */
    int solveWithoutClean(const int* f_perm1, const int* f_perm2, const int* f_perm3, 
      int* f_result, int f_upperBound = INT_MAX);
  
    /** Calculates the lower bound of a partial solution (from scratch).
     * It is assumed that all edges are of length 1 and the median contains
     * no edges so far. 
     * This is the version for weighted reversal/transposition distance.
     * The result will be written to f_sol->m_lb.
     * @param f_sol  the solution to handle
     */
    void calcLowerBound(Solution* f_sol);   

    /** 
     * Calculates the lower bound of a partial solution (from scratch).
     * It is assumed that all edges are of length 1 and the median contains
     * no edges so far.
     * This is the version for the reversal only distance. 
     * The result will be written to f_sol->m_lb.
     * @param f_sol  the solution to handle
     */
    void calcLowerBoundRev(Solution* f_sol);   

    /** 
     * Calculates in which order the nodes should be expanded. Nodes that 
     * increase the lower bound for many extensions should be preferred. This
     * ordering is calculated w.r.t. a given partial solution. This must be
     * the initial empty solution!
     * @param f_sol  the partial solution used for calculating the ordering
     */
    void createNodeOrder(Solution* f_sol);
        
    /** 
     * Calculates in which order the nodes should be expanded. Nodes that 
     * increase the lower bound for many extensions should be preferred. This
     * ordering is calculated w.r.t. a given partial solution. This must be
     * the initial empty solution!
     * This is the version for the reversal distance
     * @param f_sol  the partial solution used for calculating the ordering
     */
    void createNodeOrderRev(Solution* f_sol);
        
    /**
     * Expands the heap top element. This includes preserving the heap
     * condition. If an optimal solution is found during the expand process,
     * it will be put on the top of the heap and the function will be aborted 
     * (in this case the heap condition might be broken).  
     */
    void expand();
    
    /**
     * Expands the heap top element. This includes preserving the heap
     * condition. If an optimal solution is found during the expand process,
     * it will be put on the top of the heap and the function will be aborted 
     * (in this case the heap condition might be broken).
     * This version is taken for the reversal only distance.  
     */
    void expandRev();
    
    
    /** 
     * Tests if by adding an edge from f_node1 to f_node2, the union of 
     * H and f_edges will have a short cycle. In this case, the result 
     * cannot be a permutaton matching.
     * @param f_edges  the edges to test
     * @param f_node1  start node of new edge
     * @param f_node2  end node of new edge
     * @return  true if there is a short cycle and we cannot expand the
     *          solution to a permutation matching.
     */ 
    bool shortCycle(const int* f_edges, int f_node1, int f_node2);
    
    /**
     * Recreates the heap condition after changing the top element.
     */
    void heapifyDown();
    
    /**
     * Recreates the heap conditon after adding an element at the bottom.
     */
    void heapifyUp();
    
    /**
     * Verifies the weight of a solution by an exact branch and bound algorithm.
     * If the weight could not be verified, it is actualized and the heap 
     * condition will be repaired. The solution to check is the heap peak 
     * element.
     * @return  true if the weight is correct
     */
    bool verifyWeight();
     
    /***********************************************************************
     * debugging methods
     ***********************************************************************/
#if DEBUG_MEDIAN     
    /** 
     * Calculates the lower bound from scratch, i.e. without using previous
     * informations. The calculated lower bound will be returned.
     * @param f_sol  solution of which we shall calculate the lower bound
     * @return  the lower bound
     */          
    int calcLowerBoundFromScratch(Solution* f_sol);   

    /** 
     * Calculates the lower bound from scratch, i.e. without using previous
     * informations. The calculated lower bound will be returned.
     * This version is for the reversal only distance.
     * @param f_sol  solution of which we shall calculate the lower bound
     * @return  the lower bound
     */          
    int calcLowerBoundFromScratchRev(Solution* f_sol);
    
    /** 
     * Checks if the current heap fulfills the heap condition.
     * @return  true if the heap condition is fulfilled
     */
    bool isHeap();
    
    /**
     * Debugging method, functionality may change.
     */
    bool debug(Solution* f_sol);

#endif        
};

/***************************************************************************/

} // end namespace median

#endif /*MEDIAN_H_*/
