/**
 * filename: solution.h
 * author: Martin Bader
 * begin: 25.06.2007
 * last change: 23.08.2007
 *
 * (Partially) solution of the median problem.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifndef SOLUTION_H_
#define SOLUTION_H_


namespace median
{

/**
 * (Partially) solution of the median problem.
 */
class Solution
{
    /***********************************************************************
     * member variables
     ***********************************************************************/
  public:
    /** Edges of first genome */
    int* m_edges1;  
            
    /** Edges of second genome */
    int* m_edges2;  

    /** Edges of third genome */
    int* m_edges3;
    
    /** Current edges of the median */
    int* m_median;
    
    /** Marks edges of the first genome with even length  (after length / 2) */
    bool* m_even1;  

    /** Marks edges of the second genome with even length  (after length / 2) */
    bool* m_even2;  

    /** Marks edges of the third genome with even length  (after length / 2) */
    bool* m_even3;  

    /** already performed steps for this solution */
    int m_steps;
    
    /** lower bound for the solutions weight (including some multipliers) */
    int m_lb;
          
                  
    /***********************************************************************
     * methods
     ***********************************************************************/     
  public:
    /**
     * Standard Constructor. All arrays are set to NULL.
     */
    Solution();
    
    /** 
     * This Constructor sets the size of the graphs and reserves memory for
     * the arrays. Size is the number of nodes, which is twice the number 
     * of elements in the permutations.
     * You can also set if you are interested in the even arrays. Else they
     * won't be initialized.
     * @param f_size  permutation size
     * @param f_even  if true the even arrays are initialized
     */
    Solution(int f_size, bool f_even);
        
    /**
     * Destructor.
     */
    ~Solution();


    /***********************************************************************
     * debug methods
     ***********************************************************************/     
  public:
    /**
     * Dumps the solution to stderr.
     * @param f_size  solution size (number of nodes).
     */
    void dump(int f_size); 
};


/***************************************************************************/

} // end namespace median

#endif /*SOLUTION_H_*/
