/**
 * filename: medianPools.cpp
 * author: Martin Bader
 * begin: 25.06.2007
 * last change: 01.10.2008
 *
 * Implementation of medianPools.h
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
  
#include "medianPools.h"

#include "solution.h"


namespace median
{
using namespace std;


/***************************************************************************
 * public methods
 ***************************************************************************/
 
SolutionPool::SolutionPool()
{
    m_size = 0;
    m_solutions.reserve(1024);
}


SolutionPool::~SolutionPool()
{
    for (unsigned int i = 0; i < m_solutions.size(); i++)
        delete m_solutions[i];
}


void SolutionPool::init(int f_size, bool f_even)
{
    // delete old Solution (if there are any)
    for (unsigned int i = 0; i < m_solutions.size(); i++)
        delete m_solutions[i];
    m_solutions.clear();
    // set the new size    
    m_size = 2 * f_size;
    m_even = f_even;
}
    

Solution* SolutionPool::getSolution()
{
    Solution* result;
    
    if (m_solutions.size() > 0)
    {
        result = m_solutions[m_solutions.size()-1];
        m_solutions.pop_back();
    }
    else
        result = new Solution(m_size, m_even); 
    return result;
}


/***************************************************************************/

} // end namespace median
