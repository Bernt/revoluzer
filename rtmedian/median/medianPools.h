/**
 * filename: medianPools.h
 * author: Martin Bader
 * begin: 25.06.2006
 * last change: 01.10.2008
 *
 * Pools of several data structures, to avoid frequent allocation / freeing
 * of memory.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
  
#ifndef MEDIANPOOLS_H_
#define MEDIANPOOLS_H_

#include <vector>

namespace median
{

// class forward declarations
class Solution;


/**
 * Pool for solutions
 */
class SolutionPool
{
    /***********************************************************************
     * member variables
     ***********************************************************************/
  protected:
    /** size of the solutions; this is the number of nodes, which is twice
     * the number of elements in the permutations. */
    int m_size;
    
    /** If this variable is set to true, the solutons will have the even
     * arrays (needed for weighted distance). */
    bool m_even;
    
    /** the solutions */
    std::vector<Solution*> m_solutions;
          
        
    /***********************************************************************
     * methods
     ***********************************************************************/     
  public:
    /**
     * Constructor.
     */
    SolutionPool();
        
    /**
     * Destructor. 
     */
    ~SolutionPool();
    
    /**
     * Initializes the pool with a given permutation size.
     * Set if the solutions shall have the even arrays
     * @param f_size  the permutation size
     * @param f_even  if true, the solutions will have the even array
     */
    void init(int f_size, bool f_even);
    
    /** 
     * Returns a Solution
     * @return  pointer to a Solution
     */
    Solution* getSolution();
    
    /**
     * Return the Solution back to the pool.
     * @param f_solution  the Solution to give back
     */
    inline void freeSolution(Solution* f_solution)
    {
        m_solutions.push_back(f_solution);
    }
};


/***************************************************************************/

} // end namespace median

#endif /*MEDIANPOOLS_H_*/
