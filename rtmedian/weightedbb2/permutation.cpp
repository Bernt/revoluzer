/**
 * filename: permutation.cpp
 * author: Martin Bader
 * begin: 11.08.2008
 * last change: 19.11.2008
 *
 * Implementation of permutation.h
 * 
 * Copyright (C) 2008  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "permutation.h"

#include <iostream>
#include <stdlib.h>
#include "globals.h"

using namespace std;

namespace weightedbb
{
    
/***************************************************************************
 * public methods
 ***************************************************************************/
 
Permutation::Permutation()
{
    m_size = 0;
    m_perm = NULL;
    m_index = NULL;
    m_hash = 0;
    m_codd = 0;
    m_ceven = 0;  
}


Permutation::Permutation(int f_size, int* f_data)
{
    int value = 1;                             // value of an element
    int posData = 0;                           // position in f_data
    int posPerm = 0;                           // position in m_perm
    bool inv;                                  // true if we must invert f_data                                            
 
    m_size = f_size;
    // mark values to delete
    for (int i = 1; i <= f_size; i++)
        gIntArray0[i] = 0;
    for (int i = 1; i < f_size; i++)
    {
        if ((f_data[i-1] + 1 == f_data[i]) || ((f_data[i-1] == f_size) && (f_data[i] == 1)) || ((f_data[i-1] == -1) && (f_data[i] == -f_size)))
        {
            gIntArray0[abs(f_data[i])] = -1;   // mark value to delete
            m_size--;
        }
    }
    if ((f_data[f_size-1] + 1 == f_data[0]) || ((f_data[f_size-1] == f_size) && (f_data[0] == 1)) || ((f_data[f_size-1] == -1) && (f_data[0] == -f_size)))
    {
        gIntArray0[abs(f_data[0])] = -1;        // mark value to delete
        m_size--;
    }
    // check if we already have the id
    if (m_size == 0)
    {
        m_size = 1;
        m_perm = new int[1];
        m_perm[0] = 1;
        m_index = new int[2];
        m_index[1] = 0;
        calcHash();
        calcCycles();  
        return;
    }
    // create mapping
    for (int i = 1; i <= f_size; i++)
    {
        if (gIntArray0[i] != -1)
            gIntArray0[i] = value++;
    }
    // search starting point
    for (; posData < f_size; posData++)
    {
        if (gIntArray0[abs(f_data[posData])] == 1)
            break;
    }
    inv = (f_data[posData] < 0);
    // create the permutation
    m_perm = new int[m_size];
    m_index = new int[m_size+1];
    for (int i = 0; i < f_size; i++)
    {
        if (f_data[posData] > 0)
        {
            if (gIntArray0[f_data[posData]] != -1)
            {
                m_perm[posPerm] = (inv? -gIntArray0[f_data[posData]] : gIntArray0[f_data[posData]]);
                m_index[abs(m_perm[posPerm])] = (m_perm[posPerm] > 0? posPerm : -posPerm);
                posPerm++;
            }
            inv? (posData == 0? posData = f_size - 1 : posData--) : (posData == f_size - 1? posData = 0: posData++);
        }
        else  
        {
            if (gIntArray0[-f_data[posData]] != -1)
            {
                m_perm[posPerm] = (inv? gIntArray0[-f_data[posData]] : -gIntArray0[-f_data[posData]]);
                m_index[abs(m_perm[posPerm])] = (m_perm[posPerm] > 0? posPerm : -posPerm);
                posPerm++;
            }
            inv? (posData == 0? posData = f_size - 1 : posData--) : (posData == f_size - 1? posData = 0: posData++);
        }
    }
    calcHash();
    calcCycles();  
}


Permutation::~Permutation()
{
    delete[] m_perm;
    delete[] m_index;
}


Permutation* Permutation::performOperation(const Operation& f_op) const
{
    Permutation* result = new Permutation();      // the resulting permutation
    int adj0, adj1, adj2;                         // 1 if the edges become adjacencies, 0 otherwise
    int pos;                                      // position for writing

    // init, create mappings    
    result->m_size = m_size;
    adj0 = 0;
    adj1 = 0;
    adj2 = 0;
    for (int i = 1; i <= m_size; i++)      // gIntArray0 for positiv vars
        gIntArray0[i] = i;
    for (int i = 1; i <= m_size; i++)      // gIntArray1 for negativ vars    
        gIntArray1[i] = -i;
    // check optype    
    if (f_op.m_type == Operation::REVERSAL)       // A B A -> A -B A
    {
        // check for adjacencies
        if (m_perm[f_op.m_edges[0]-1] + 1 == -m_perm[f_op.m_edges[1] - 1])
        {
            adj0 = 1;
            for (int i = (m_perm[f_op.m_edges[0]-1] > 0? m_perm[f_op.m_edges[0]-1] + 1 
              : -m_perm[f_op.m_edges[0]-1]); i <= m_size; i++)
            {
                gIntArray0[i]--;
                gIntArray1[i]++;
            }
            result->m_size--;
        }
        if (f_op.m_edges[1] == m_size)
        {
            if (m_perm[f_op.m_edges[0]] == -m_size)
            {
                adj1 = 1;
                gIntArray0[m_size]--;
                gIntArray1[m_size]++;
                result->m_size--;
            }
        }
        else
        {
            if (-m_perm[f_op.m_edges[0]] + 1 == m_perm[f_op.m_edges[1]])
            {            
                adj1 = 1;
                for (int i = (m_perm[f_op.m_edges[0]] > 0? m_perm[f_op.m_edges[0]] 
                  : m_perm[f_op.m_edges[1]]); i <= m_size; i++)
                {
                    gIntArray0[i]--;
                    gIntArray1[i]++;
                }
                result->m_size--;
            }
        }
        // init arrays
        if (result->m_size == 0)
        {
            result->m_size = 1;
            result->m_perm = new int[1];
            result->m_index = new int[2];
            result->m_perm[0] = 1;
            result->m_index[1] = 0;
            result->calcHash();
            result->calcCycles();    
            return result;
        }
        result->m_perm = new int[result->m_size];
        result->m_index = new int[result->m_size + 1];
        // write first block
        for (int i = 0; i < f_op.m_edges[0] - adj0; i++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[i] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[i]] = i;
            }
            else
            {
                result->m_perm[i] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[i]] = -i;
            }
        }
        // write second block
        pos = f_op.m_edges[0] - adj0;
        for (int i = f_op.m_edges[1] - 1; i >= f_op.m_edges[0] + adj1; i--, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray1[m_perm[i]];
                result->m_index[-result->m_perm[pos]] = -pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray0[-m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
        }
        // write last block
        for (int i = f_op.m_edges[1]; i < m_size; i++, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[pos]] = -pos;
            }
        }
    }
    else if (f_op.m_type == Operation::TRANSPOSITION)         // A B C A -> A C B A
    {
        // check for adjacencies
        if (m_perm[f_op.m_edges[0]-1] + 1 == m_perm[f_op.m_edges[1]])
        {
            adj0 = 1;
            for (int i = (m_perm[f_op.m_edges[1]] > 0? m_perm[f_op.m_edges[1]] 
              : -m_perm[f_op.m_edges[0]-1]); i <= m_size; i++)
            {
                gIntArray0[i]--;
                gIntArray1[i]++;
            }
            result->m_size--;
        }
        if (m_perm[f_op.m_edges[2]-1] + 1 == m_perm[f_op.m_edges[0]])
        {
            adj1 = 1;
            for (int i = (m_perm[f_op.m_edges[0]] > 0? m_perm[f_op.m_edges[0]] 
              : -m_perm[f_op.m_edges[2]-1]); i <= m_size; i++)
            {
                gIntArray0[i]--;
                gIntArray1[i]++;
            }
            result->m_size--;
        }
        if (f_op.m_edges[2] == m_size)
        {
            if (m_perm[f_op.m_edges[1] - 1] == m_size)
            {
                adj2 = 1;
                gIntArray0[m_size]--;
                gIntArray1[m_size]++;
                result->m_size--;
            }
        }
        else
        {
            if (m_perm[f_op.m_edges[1]-1] + 1 == m_perm[f_op.m_edges[2]])
            {
                adj2 = 1;
                for (int i = (m_perm[f_op.m_edges[2]] > 0? m_perm[f_op.m_edges[2]] 
                  : -m_perm[f_op.m_edges[1]-1]); i <= m_size; i++)
                {
                    gIntArray0[i]--;
                    gIntArray1[i]++;
                }
                result->m_size--;
            }
        }
        // init arrays
        if (result->m_size == 0)
        {
            result->m_size = 1;
            result->m_perm = new int[1];
            result->m_index = new int[2];
            result->m_perm[0] = 1;
            result->m_index[1] = 0;
            result->calcHash();
            result->calcCycles();    
            return result;
        }
        result->m_perm = new int[result->m_size];
        result->m_index = new int[result->m_size + 1];
        // write first block
        for (int i = 0; i < f_op.m_edges[0] - adj0; i++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[i] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[i]] = i;
            }
            else
            {
                result->m_perm[i] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[i]] = -i;
            }
        }
        // write second block
        pos = f_op.m_edges[0] - adj0;
        for (int i = f_op.m_edges[1]; i < f_op.m_edges[2] - adj1; i++, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray1[-m_perm[i]];
                result->m_index[-result->m_perm[pos]] = -pos;
            }
        }
        // write third block
        for (int i = f_op.m_edges[0]; i < f_op.m_edges[1] - adj2; i++, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray1[-m_perm[i]];
                result->m_index[-result->m_perm[pos]] = -pos;
            }
        }
        // write last block
        for (int i = f_op.m_edges[2]; i < m_size; i++, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[pos]] = -pos;
            }
        }
    }
    else if (f_op.m_type == Operation::TRANSREVA)         // A B C A -> A C -B A
    {
        // check for adjacencies
        if (m_perm[f_op.m_edges[0]-1] + 1 == m_perm[f_op.m_edges[1]])
        {
            adj0 = 1;
            for (int i = (m_perm[f_op.m_edges[1]] > 0? m_perm[f_op.m_edges[1]] 
              : -m_perm[f_op.m_edges[0]-1]); i <= m_size; i++)
            {
                gIntArray0[i]--;
                gIntArray1[i]++;
            }
            result->m_size--;
        }
        if (m_perm[f_op.m_edges[2]-1] + 1 == -m_perm[f_op.m_edges[1]-1])
        {
            adj1 = 1;
            for (int i = (m_perm[f_op.m_edges[2]-1] > 0? -m_perm[f_op.m_edges[1]-1] 
              : -m_perm[f_op.m_edges[2]-1]); i <= m_size; i++)
            {
                gIntArray0[i]--;
                gIntArray1[i]++;
            }
            result->m_size--;
        }
        if (f_op.m_edges[2] == m_size)
        {
            if (m_perm[f_op.m_edges[0]] == -m_size)
            {
                adj2 = 1;
                gIntArray0[m_size]--;
                gIntArray1[m_size]++;
                result->m_size--;
            }
        }
        else
        {
            if (-m_perm[f_op.m_edges[0]] + 1 == m_perm[f_op.m_edges[2]])
            {
                adj2 = 1;
                for (int i = (m_perm[f_op.m_edges[2]] > 0? m_perm[f_op.m_edges[2]] 
                  : m_perm[f_op.m_edges[0]]); i <= m_size; i++)
                {
                    gIntArray0[i]--;
                    gIntArray1[i]++;
                }
                result->m_size--;
            }
        }
        // init arrays
        if (result->m_size == 0)
        {
            result->m_size = 1;
            result->m_perm = new int[1];
            result->m_index = new int[2];
            result->m_perm[0] = 1;
            result->m_index[1] = 0;
            result->calcHash();
            result->calcCycles();    
            return result;
        }
        result->m_perm = new int[result->m_size];
        result->m_index = new int[result->m_size + 1];
        // write first block
        for (int i = 0; i < f_op.m_edges[0] - adj0; i++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[i] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[i]] = i;
            }
            else
            {
                result->m_perm[i] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[i]] = -i;
            }
        }
        // write second block
        pos = f_op.m_edges[0] - adj0;
        for (int i = f_op.m_edges[1]; i < f_op.m_edges[2] - adj1; i++, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray1[-m_perm[i]];
                result->m_index[-result->m_perm[pos]] = -pos;
            }
        }
        // write third block
        for (int i = f_op.m_edges[1]-1; i >= f_op.m_edges[0] + adj2; i--, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray1[m_perm[i]];
                result->m_index[-result->m_perm[pos]] = -pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray0[-m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
        }
        // write last block
        for (int i = f_op.m_edges[2]; i < m_size; i++, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[pos]] = -pos;
            }
        }
    }
    else if (f_op.m_type == Operation::TRANSREVB)         // A B C A -> A -C B A
    {
        // check for adjacencies
        if (m_perm[f_op.m_edges[0]-1] + 1 == -m_perm[f_op.m_edges[2]-1])
        {
            adj0 = 1;
            for (int i = (m_perm[f_op.m_edges[0]-1] > 0? -m_perm[f_op.m_edges[2]-1] 
              : -m_perm[f_op.m_edges[0]-1]); i <= m_size; i++)
            {
                gIntArray0[i]--;
                gIntArray1[i]++;
            }
            result->m_size--;
        }
        if (-m_perm[f_op.m_edges[1]] + 1 == m_perm[f_op.m_edges[0]])
        {
            adj1 = 1;
            for (int i = (m_perm[f_op.m_edges[0]] > 0? m_perm[f_op.m_edges[0]] 
              : m_perm[f_op.m_edges[1]]); i <= m_size; i++)
            {
                gIntArray0[i]--;
                gIntArray1[i]++;
            }
            result->m_size--;
        }
        if (f_op.m_edges[2] == m_size)
        {
            if (m_perm[f_op.m_edges[1] - 1] == m_size)
            {
                adj2 = 1;
                gIntArray0[m_size]--;
                gIntArray1[m_size]++;
                result->m_size--;
            }
        }
        else
        {
            if (m_perm[f_op.m_edges[1]-1] + 1 == m_perm[f_op.m_edges[2]])
            {
                adj2 = 1;
                for (int i = (m_perm[f_op.m_edges[2]] > 0? m_perm[f_op.m_edges[2]] 
                  : -m_perm[f_op.m_edges[1]-1]); i <= m_size; i++)
                {
                    gIntArray0[i]--;
                    gIntArray1[i]++;
                }
                result->m_size--;
            }
        }
        // init arrays
        if (result->m_size == 0)
        {
            result->m_size = 1;
            result->m_perm = new int[1];
            result->m_index = new int[2];
            result->m_perm[0] = 1;
            result->m_index[1] = 0;
            result->calcHash();
            result->calcCycles();    
            return result;
        }
        result->m_perm = new int[result->m_size];
        result->m_index = new int[result->m_size + 1];
        // write first block
        for (int i = 0; i < f_op.m_edges[0] - adj0; i++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[i] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[i]] = i;
            }
            else
            {
                result->m_perm[i] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[i]] = -i;
            }
        }
        // write second block
        pos = f_op.m_edges[0] - adj0;
        for (int i = f_op.m_edges[2]-1; i >= f_op.m_edges[1] + adj1; i--, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray1[m_perm[i]];
                result->m_index[-result->m_perm[pos]] = -pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray0[-m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
        }
        // write third block
        for (int i = f_op.m_edges[0]; i < f_op.m_edges[1] - adj2; i++, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray1[-m_perm[i]];
                result->m_index[-result->m_perm[pos]] = -pos;
            }
        }
        // write last block
        for (int i = f_op.m_edges[2]; i < m_size; i++, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[pos]] = -pos;
            }
        }
    }
    else if (f_op.m_type == Operation::REVREV)         // A B C A -> A -B -C A
    {
        // check for adjacencies
        if (m_perm[f_op.m_edges[0]-1] + 1 == -m_perm[f_op.m_edges[1]-1])
        {
            adj0 = 1;
            for (int i = (m_perm[f_op.m_edges[0]-1] > 0? -m_perm[f_op.m_edges[1]-1] 
              : -m_perm[f_op.m_edges[0]-1]); i <= m_size; i++)
            {
                gIntArray0[i]--;
                gIntArray1[i]++;
            }
            result->m_size--;
        }
        if (m_perm[f_op.m_edges[0]] - 1 == m_perm[f_op.m_edges[2]-1])
        {
            adj1 = 1;
            for (int i = (m_perm[f_op.m_edges[0]] > 0? m_perm[f_op.m_edges[0]] 
              : -m_perm[f_op.m_edges[2]-1]); i <= m_size; i++)
            {
                gIntArray0[i]--;
                gIntArray1[i]++;
            }
            result->m_size--;
        }
        if (f_op.m_edges[2] == m_size)
        {
            if (m_perm[f_op.m_edges[1]] == -m_size)
            {
                adj2 = 1;
                gIntArray0[m_size]--;
                gIntArray1[m_size]++;
                result->m_size--;
            }
        }
        else
        {
            if (-m_perm[f_op.m_edges[1]] + 1 == m_perm[f_op.m_edges[2]])
            {
                adj2 = 1;
                for (int i = (m_perm[f_op.m_edges[2]] > 0? m_perm[f_op.m_edges[2]] 
                  : m_perm[f_op.m_edges[1]]); i <= m_size; i++)
                {
                    gIntArray0[i]--;
                    gIntArray1[i]++;
                }
                result->m_size--;
            }
        }
        // init arrays
        if (result->m_size == 0)
        {
            result->m_size = 1;
            result->m_perm = new int[1];
            result->m_index = new int[2];
            result->m_perm[0] = 1;
            result->m_index[1] = 0;
            result->calcHash();
            result->calcCycles();    
            return result;
        }
        result->m_perm = new int[result->m_size];
        result->m_index = new int[result->m_size + 1];
        // write first block
        for (int i = 0; i < f_op.m_edges[0] - adj0; i++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[i] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[i]] = i;
            }
            else
            {
                result->m_perm[i] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[i]] = -i;
            }
        }
        // write second block
        pos = f_op.m_edges[0] - adj0;
        for (int i = f_op.m_edges[1]-1; i >= f_op.m_edges[0] + adj1; i--, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray1[m_perm[i]];
                result->m_index[-result->m_perm[pos]] = -pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray0[-m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
        }
        // write third block
        for (int i = f_op.m_edges[2]-1; i >= f_op.m_edges[1] + adj2; i--, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray1[m_perm[i]];
                result->m_index[-result->m_perm[pos]] = -pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray0[-m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
        }
        // write last block
        for (int i = f_op.m_edges[2]; i < m_size; i++, pos++)
        {
            if (m_perm[i] > 0)
            {
                result->m_perm[pos] = gIntArray0[m_perm[i]];
                result->m_index[result->m_perm[pos]] = pos;
            }
            else
            {
                result->m_perm[pos] = gIntArray1[-m_perm[i]]; 
                result->m_index[-result->m_perm[pos]] = -pos;
            }
        }
    }
    result->calcHash();
    result->calcCycles();    
    return result;    
}


int Permutation::getLowerBound(int f_wr, int f_wt)
{
    return m_ceven * f_wr + ((m_size - m_codd) / 2 - m_ceven) * f_wt;    
//    if (f_wt <= 2 * f_wr)
//        return m_ceven * f_wr + ((m_size - m_codd) / 2 - m_ceven) * f_wt;
//    else
//        return (m_size - (m_codd + m_ceven)) * f_wr;
}


bool Permutation::operator==(Permutation& f_perm) const
{
    if ((m_hash != f_perm.m_hash) || (m_size != f_perm.m_size))
        return false;
    for (int i = 0; i < m_size; i++)
    {
        if (m_perm[i] != f_perm.m_perm[i])
            return false;
    }
    return true; 
}


/***************************************************************************
 * private methods
 ***************************************************************************/

void Permutation::calcHash()
{
    m_hash = 0;
    for (int i = 1; i < m_size; i++)
    {
        m_hash += m_perm[i];
        m_hash *= (m_perm[i] - m_perm[i-1]);
    }
    m_hash = abs(m_hash);
    m_hash %= HASHSIZE;
}


void Permutation::calcCycles()
{
    int length;          // length of the current cycle
    bool left;           // true if we are at the left point of an element
    int pos;             // current position
    int element;         // current element
    
    m_codd = 0;
    m_ceven = 0;
    for (int i = 0; i < m_size; i++)   // use as visited array
        gIntArray0[i] = 0;
    for (int i = 0; i < m_size; i++)
    {
        if (gIntArray0[i] == 1)         // already visited
            continue;
        length = 0;
        left = false;
        pos = i;
        while (gIntArray0[pos] == 0)
        {
            length++;
            gIntArray0[pos] = 1;
            // current element - negativ if we are at its lower point
            if (left)
                element = -m_perm[pos];
            else
                element = (pos == 0? m_perm[m_size - 1] : m_perm[pos-1]);
            // target element - negativ if we want to its lower point    
            element = -element - 1;
            if (element == 0)
                element = m_size;
            else if (element == -(m_size + 1))
                element = -1;
            pos = abs(m_index[abs(element)]);
            if ((element > 0) ^ (m_perm[pos] > 0))
                left = false;
            else
            {
                (pos == m_size - 1? pos = 0 : pos++);
                left = true;
            }
        }
        if (length & 1)
            m_codd++;
        else
            m_ceven++;
    }
}


/***************************************************************************
 * static (non-class) methods
 ***************************************************************************/

void printPermutation(int f_size, int* f_perm)
{
    for (int i = 0; i < f_size; i++)
        cout << f_perm[i] << " ";
    cout << endl;
}


void printOperation(Operation* f_op, std::ostream &out)
{
    switch (f_op->m_type)
    {
    case Operation::REVERSAL: out << "     rev(" << f_op->m_edges[0]
      << ", " << f_op->m_edges[1] << ") "; break;
    case Operation::TRANSPOSITION: out << "    t(" << f_op->m_edges[0]
      << ", " << f_op->m_edges[1] << ", " << f_op->m_edges[2]<< ") "; break;
    case Operation::TRANSREVA: out << "tra(" << f_op->m_edges[0]
      << ", " << f_op->m_edges[1] << ", " << f_op->m_edges[2]<< ") "; break;
    case Operation::TRANSREVB: out << "trb(" << f_op->m_edges[0]
      << ", " << f_op->m_edges[1] << ", " << f_op->m_edges[2]<< ") "; break;
    case Operation::REVREV: out << "rr(" << f_op->m_edges[0]
      << ", " << f_op->m_edges[1] << ", " << f_op->m_edges[2]<< ") "; break;
    case Operation::INVTRANS: out << "inv t(" << f_op->m_edges[0]
      << ", " << f_op->m_edges[1] << ", " << f_op->m_edges[2]<< ") "; break;  
    }
}

/***************************************************************************
 * debugging methods
 ***************************************************************************/
 
void Permutation::dump()
{
    cout << "*** dumping Permutation " << this << " ***\n";
    cout << "size: " << m_size << endl;
    cout << "perm:\n";
    for (int i = 0; i < m_size; i++)
        cout << m_perm[i] << " ";
    cout << "\nm_index:\n";
    for (int i = 1; i <= m_size; i++)
        cout << m_index[i] << " ";
    cout << "\nhash: " << m_hash << endl;
    cout << "codd / ceven: " << m_codd << " / " << m_ceven << endl;
    cout << "***************\n";
}

} // end namespace weightedbb
