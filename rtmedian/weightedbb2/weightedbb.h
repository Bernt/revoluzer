/**
 * filename: weightedbb.h
 * author: Martin Bader
 * begin: 11.08.2008
 * last change: 31.03.2010
 *
 * Main file of the branch n bound algorithm for weighted Sorting by 
 * reversals, transpositions, and transreversals.
 * 
 * Copyright (C) 2008-2010  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef WEIGHTEDBB_H_
#define WEIGHTEDBB_H_

#include <climits>
#include <list>
#include <vector>
#include "../minswrt/minswrt.h"

#define DEBUG_WEIGHTEDBB 0

namespace weightedbb
{

// forward declarations
struct Operation;
class Permutation;


/**
 * A candidate for expansion - consists of the perm and the position where to 
 * find in the different queues. 
 */
struct Candidate
{
    /** The permutation object */
    Permutation* m_perm;
    
    /** The lower bound for the whole sequence */
    int m_lbWhole;
    
    /**  The lower bound for the remaining sequence */
    int m_lbRem;
    
    /** Vector position in WeightedBB::m_candidates */
    int m_positionC; 
        
    /** Vector position in WeightedBB::m_queue */
    int m_positionQ;
    
    /** The number of children that are still valid */
    int m_numChildren;
    
    /** The parent candidate */
    Candidate* m_parent;
    
    /** The Operation that led to this candidate */
    Operation* m_op; 
};


/**
 * Struct that maps elements in the internal representation to lists of 
 * elements corresponding to the real permutation 
 */
struct ElementList
{
    /** List size */
    int m_size;
        
    /** List of elements */
    std::list<int> m_elements;   
};

 
/**
 * The main class, contains the branch n bound algorithm.
 */
class WeightedBB
{
    /***********************************************************************
     * member variables
     ***********************************************************************/

  protected:
    /** Size of permutation to sort */
    int m_permSize;
    
    /** Copy of the source permutation */
    int* m_source;
    
    /** Copy of the target permutation */
    int* m_target;
    
    /** The weight for reversals */
    int m_wr;
    
    /** The weight for transpositions and transreversals */
    int m_wt;
    
    /** Distance mode: 0: transposition distance; 1: weighted reversal and transposition distance */
    int m_distMode;    
    
    /** Mapping target -> id */
    int* m_mapping;
    
    /** The queue of permutations to expand */
    std::vector<Candidate*>* m_queue;
    
    /** All candidate permutations, sorted by the lower bound  */
    std::vector<Candidate*>* m_candidates;

    /** The lower bound for the distance */
    int m_lowerBound;
        
    /** The upper bound for the distance */
    int m_upperBound;
    
    /** The calculated distance */
    int m_distance;
    
    /** Elements currently on the heap */
    int m_heapSize;
    
    /** Maximal number of elements on the heap */
    int m_heapMax;
    
    /** Maximal allowed number of elements on the heap */
    int m_heapLimit;
    
    /** The last candidate of the sorting sequence */
    Candidate* m_terminal;
    
    /** Tool for calculating the upper bound. */
    minswrt::MinSWRT m_minswrt;

    /***********************************************************************
     * methods
     ***********************************************************************/

  public:
    /**
     * Constructor.
     */
    WeightedBB();
    
    /**
     * Destructor.
     */
    ~WeightedBB(); 

    /**
     * Sets the heap limit (maximum number of elements allowed on the heap).
     * As the limit is not checked in each step, it is possible that this limit is 
     * exceeded by a small number.
     * @param f_limit  the new heap limit
     */
    inline void setHeapLimit(int f_limit)
    {
        m_heapLimit = f_limit;
    }
    
    /**
     * Sets the permutation to sort. Clears the memory of old data.
     * f_size is the permutation size, and f_source is a int[f_size] array
     * that contains the signed elements. Elements go from 1 to f_size.
     * The algorithm assumes that the target permutation is the ID.
     * @param f_size  size of the permutation
     * @param f_source  permutation data
     */
    void setPermutation(int f_size, const int* f_source);
    
    /** 
     * Sets two permutations. Clears the memory of old data.
     * f_size is the permutation size, f_source and f_target
     * are two int[f_size} arrays that contain the signed elements. Elements
     * go from 1 to f_size.
     * @param f_size  size of the permutation
     * @param f_source  source permutation
     * @param f_target  target permutation
     */
    void setPermutations(int f_size, const int* f_source, const int* f_target);  
    
    /**
     * Sets the weights for reversals and transpositions + transreversals.
     * @param f_wr  weight for reversals
     * @param f_wt  weight for transpositions and transreversals
     * @param f_distMode  0 transposition distance, 1 weighted rev/t distance
     */
    void setWeights(int f_wr, int f_wt, int f_distMode = 1); 
     
    /**
     * Starts the sorting algorithm. Only searches for solutions <= f_maxDist.
     * @param f_maxDist maximum distance to search
     */
    void sort(bool circular, int f_maxDist = INT_MAX);

    /**
     * Returns the lower bound between the input permutations.
     * If the input permutations are not set, -1 will be returned.
     * @return  the lower bound
     */
    int getLowerBound() const;
    
    /**
     * Returns the number of odd and even cycles between the input permutations.
     * The results will be written to f_odd and f_even, which are references
     * of integers. As adjacency cycles are eliminated, it also returns the number
     * of blocks (= number of breakpoints), which will be written to f_blocks. 
     * The return value is true on success, and false if no
     * permutations are set.
     * @param f_odd  the number of odd cylces (return value)
     * @param f_even  the number of even cycles (return value)
     * @return  true if successfull, false otherwise
     */
    bool getNumCycles(int& f_odd, int& f_even, int& f_blocks);
    
    /**
     * Returns the weighted distance of the input permutation. This can only
     * be called aftzer a call of sort.
     * @return  the weighted distance
     */
    int getDistance();
    
    /**
     * Returns the calculated sorting sequence. The result will be written to
     * the parameters f_numOps, f_ops, and f_perms (all data in these parameters
     * will be lost). f_numOps is the number of performed operations, f_ops are 
     * the operations, and f_perms are the permutations on the sorting scenario
     * (all from source to target). I.e. f_perms are f_ops + 1 permutations, 
     * f_perms[0] is the source permutation, f_perms[f_ops] is the target 
     * permutation. f_op[i] is the operation from permutation f_perm[i] to 
     * f_perm[i+1]. All returned values are copies, so make sure to delete them
     * in your program when no longer needed.
     * The return value indicates whether we could return a sorting sequence 
     * or not. You must have performed a successfull call of sort(), or false 
     * will be returned and none of the parameters will be written.
     * @param f_numOps  the number of operations (result will be written here)
     * @param f_ops  the operations (result will be written here)
     * @param f_perms  the permutations on the sorting sequence (result will be written here)
     * @return  true if successfull, false otherwise
     */
    bool getSortingSequence(unsigned int& f_numOps, Operation*& f_ops, int**& f_perms) const;
     
    /**
     * Prints the result to stdout. sort must have been called before this.
     * f_printLevel determins how much information will be written.
     * f_printLevel:
     * 0: only write overall weight
     * 1: write operations
     * 2: write operations + overall weight
     * 3: write operations + permutations
     * 4: write operations + permutations + overall weight
     * @param f_printLevel  the print level
     */
    void printResult(int f_printLevel, std::ostream &out);
    
    /** 
     * Returns the maximum heap size during the last calculation.
     * @return  maximum heap size
     */
    inline int getHeapMax()
    {
        return m_heapMax;
    }
    
  private:
    /** 
     * Expands a candidate
     * @param f_cand  the candidate
     */
    void expand(Candidate* f_cand, bool circular);
    
    /**
     * Applys the operation f_op to the permutation f_perm. The resulting permutation
     * will be added at the correct position in the queues. This also contains a 
     * check against the upper bound and duplicate elimination.
     * @param f_perm  source permutation
     * @param f_op  the operation to apply
     * @param f_weight  weight of sequence m_source -> f_perm
     */
    void applyOperation(Candidate* f_perm, const Operation* f_op, int weight);  
  
    /**
     * Deletes the candidate plus all its ancestors (frees memory).
     * @param f_can  the candidate to delete
     */
    void deleteAncestors(Candidate* f_cand);
    
    /** 
     * Clears all current data.
     */
    void clear();
     
    /***********************************************************************
     * debuging methods
     ***********************************************************************/

#if DEBUG_WEIGHTEDBB

  public:
    /**
     * Debugging method, functionality can change.
     */  
    void debug();
    
#endif

};

} // end namespace weightedbb

#endif /*WEIGHTEDBB_H_*/
