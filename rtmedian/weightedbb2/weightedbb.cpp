/**
 * filename: weightedbb.cpp
 * author: Martin Bader
 * begin: 11.08.2008
 * last change: 31.03.2010
 *
 * Implementation of weightedbb.h
 * 
 * Copyright (C) 2008-2010  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "weightedbb.h"

#include <stack>
#include "globals.h"
#include "permutation.h"


namespace weightedbb
{

/***************************************************************************
 * public methods
 ***************************************************************************/

WeightedBB::WeightedBB()
{
    // init the class
    m_permSize = 0;
    m_source = NULL;
    m_target = NULL;
    m_wr = 1;
    m_wt = 1;
    m_distMode = 1;
    m_mapping = NULL;
    m_queue = NULL;
    m_candidates = new vector<Candidate*>[HASHSIZE];
    m_lowerBound = 0;
    m_upperBound = -1;
    m_distance = -1;
    m_minswrt.setGreedy(1);
    m_heapSize = 0;
    m_heapMax = 0;
    m_heapLimit = INT_MAX;
}


WeightedBB::~WeightedBB()
{
    clear();
    delete[] m_candidates; m_candidates = NULL;  
}


void WeightedBB::setPermutation(int f_size, const int* f_source)
{    
    clear();
    gIntArray0 = new int[f_size+1];
    gIntArray1 = new int[f_size+1];
    gIntArray2 = new int[f_size+1];
    gIntArray3 = new int[f_size+1];
    gIntArray4 = new int[f_size+1];
    gIntArray5 = new int[f_size+1];
    gIntArray6 = new int[f_size+1];
    gBoolArray0 = new bool[f_size+1];
    m_permSize = f_size;
    m_source = new int[f_size];
    m_target = new int[f_size];
    m_mapping = new int[f_size+1];
    for (int i = 0; i < f_size; i++)
        m_source[i] = f_source[i];
    for (int i = 0; i < f_size; i++)
        m_target[i] = i+1;
    for (int i = 1; i <= f_size; i++)
        m_mapping[i] = i;
    m_minswrt.setPermutations(m_permSize, m_source, m_target);    
}


void WeightedBB::setPermutations(int f_size, const int* f_source, const int* f_target)
{
    clear();
    gIntArray0 = new int[f_size+1];
    gIntArray1 = new int[f_size+1];
    gIntArray2 = new int[f_size+1];
    gIntArray3 = new int[f_size+1];
    gIntArray4 = new int[f_size+1];
    gIntArray5 = new int[f_size+1];
    gIntArray6 = new int[f_size+1];
    gBoolArray0 = new bool[f_size+1];
    m_permSize = f_size;
    m_source = new int[f_size];
    m_target = new int[f_size];
    m_mapping = new int[f_size+1]; 
    for (int i = 0; i < f_size; i++)
        f_target[i] > 0? m_mapping[f_target[i]] = i+1 : m_mapping[-f_target[i]] = -(i+1);
    for (int i = 0; i < f_size; i++)
        m_source[i] = (f_source[i] > 0? m_mapping[f_source[i]] : -m_mapping[-f_source[i]]);
    for (int i = 0; i < f_size; i++)
        m_target[i] = i+1; 
    m_minswrt.setPermutations(m_permSize, m_source, m_target);      
}  


void WeightedBB::setWeights(int f_wr, int f_wt, int f_distMode)
{
    m_wr = f_wr;
    m_wt = f_wt;
    m_distMode = f_distMode;
    if (m_distMode == 0)     // transposition distance
    {
        m_wr = f_wt;
        m_wt = f_wt;
        m_minswrt.setWeights(m_wt, m_wt);
    }
    else
    {    
        m_wr = f_wr;
        m_wt = f_wt;
        m_minswrt.setWeights(m_wr, m_wt);
    } 
}


void WeightedBB::sort(bool circular, int f_maxDist)
{
    const vector<minswrt::Operation>* approx;       // sorting sequence of the approximation algorithm
    Permutation* perm;                              // the initial permutation
    Candidate* cand;                                // + additional information
    
    // check if the permutations are set - otherwise return
    if (m_source == NULL)
    {
        cout << "permutations not set - cannot sort\n";
        return;
    }
    // if we have to clear the old queues, do so
    if (m_queue != NULL)
    {
        for (int i = 0; i <= m_upperBound - m_lowerBound; i++)
        {
            for (unsigned int j = 0; j < m_queue[i].size(); j++)
                deleteAncestors(m_queue[i][j]);
            m_queue[i].clear();
        }
        delete[] m_queue;
        for (int i = 0; i < HASHSIZE; i++)
            m_candidates[i].clear();
    } 
    // create the initial permutation      
    perm = new Permutation(m_permSize, m_source);
    m_lowerBound = perm->getLowerBound(m_wr, m_wt);
    cand = new Candidate;
    cand->m_lbRem = m_lowerBound;
    cand->m_lbWhole = m_lowerBound;
    cand->m_perm = perm;
    cand->m_positionC = 0;
    cand->m_positionQ = 0;
    cand->m_numChildren = 0;
    cand->m_parent = NULL;
    cand->m_op = NULL; 
    // calculate the bounds and init the queues
    m_minswrt.sort();
    approx = m_minswrt.getResult();
    m_upperBound = 0;
    for (unsigned int i = 0; i < approx->size(); i++)
    {
//        m_upperBound += ((*approx)[i].m_type == minswrt::Operation::OPERATION_REVERSAL? m_wr : m_wt); 

        if ((*approx)[i].m_type == minswrt::Operation::OPERATION_REVERSAL)
          m_upperBound += m_wr;
        else 
        {
             bool contains_dummy = false;
             for(int j = (*approx)[i].m_edges[0]-1; (j%m_permSize) != (*approx)[i].m_edges[1]-1; j++)
             {
                if(abs(m_source[j%m_permSize]) == m_permSize)
                {
                    contains_dummy = true;
                    break;
                }
             }
             if (((*approx)[i].m_type == minswrt::Operation::OPERATION_TRANSREVERSAL) && contains_dummy)
              m_upperBound += 2 * m_wr;
            else
              m_upperBound += m_wt;
        }
    }
    if (f_maxDist < m_upperBound)
        m_upperBound = f_maxDist;
    if (m_upperBound < m_lowerBound)   // there can be no solution
    {
        cerr << "upperBound < lowerBound " << m_upperBound << " < " << m_lowerBound << endl;
        m_distance = -1;
        return;
    }
    m_queue = new vector<Candidate*>[m_upperBound - m_lowerBound + 1];
    m_queue[0].push_back(cand);    
    m_candidates[perm->m_hash].push_back(cand);
    m_heapSize = 1;
    m_heapMax = 1;
    // expand
    for (int x = 0; x <= m_upperBound - m_lowerBound; x++)     // position in queue array
    {
        while (!m_queue[x].empty())
        {
            cand = m_queue[x][m_queue[x].size()-1];
            if (cand->m_lbRem == 0)                   // solution found
            {
                m_distance = x + m_lowerBound;
                m_terminal = cand; 
                return;
            }
            if (m_heapMax > m_heapLimit)              // more elements on the heap than allowed -> abort
            {
                m_distance = -2;
                return;
            }
            m_queue[x].pop_back();
            m_candidates[cand->m_perm->m_hash][cand->m_positionC] = m_candidates[cand->m_perm->m_hash][m_candidates[cand->m_perm->m_hash].size()-1];
            m_candidates[cand->m_perm->m_hash][cand->m_positionC]->m_positionC = cand->m_positionC;
            m_candidates[cand->m_perm->m_hash].pop_back();
            expand(cand, circular);
            if (cand->m_numChildren == 0)
                deleteAncestors(cand);
        }
    }
    m_distance = -1;
}
    

int WeightedBB::getLowerBound() const
{
    Permutation* perm;                              // the initial permutation
    int result;                                     // this will be returned
        
    // check if the permutations are set - otherwise return
    if (m_source == NULL)
    {
        cout << "permutations not set - cannot calculate a lower bound\n";
        return -1;
    }
    // create the initial permutation      
    perm = new Permutation(m_permSize, m_source);
    result = perm->getLowerBound(m_wr, m_wt);
    delete perm;
    return result;
}
    

bool WeightedBB::getNumCycles(int& f_odd, int& f_even, int& f_blocks)
{
    Permutation* perm;                              // the initial permutation
        
    // check if the permutations are set - otherwise return
    if (m_source == NULL)
    {
        cout << "permutations not set - cannot calculate a lower bound\n";
        return false;
    }
    // create the initial permutation      
    perm = new Permutation(m_permSize, m_source);
    f_odd = perm->m_codd;
    f_even = perm->m_ceven;    
    f_blocks = perm->m_size;
    delete perm;
    return true;
}
    

int WeightedBB::getDistance()
{
    return m_distance;
}

    
bool WeightedBB::getSortingSequence(unsigned int& f_numOps, Operation*& f_ops, int**& f_perms) const
{
    unsigned int opCounter = 0;   // counts the written operations
    stack<Candidate*> seq;    // sequence of candidates
    Candidate* cand;          // A candidate of the sequence
    Permutation* perm = NULL; // its permutation
    int size;                 // its size
    ElementList* blocks;      // the mapping from internal elements to real elements
    int value;                // an elements value
    bool found = false;       // used for searching, indicates that we have found an element
    int firstblock = -1;      // id of the real first block
    int offset = 0;           // position of the real first element in its block
    bool firstori = true;     // true if it has positive orientation
    bool ori = true;          // orientation to use for the current perm
    int counter;              // counts printed elements
    int it;                   // loop iterator
    int swap;                 // swap two variables
    int b0, b1, b2, b3, b4, b5;  // border elements of an operation
    int m[3];                 // merge points (smaller elements)
    int shift;                // shift of the start point
    bool invert;              // true if we must invert the new start point
    int tmp;

    if (m_distance < 0)       // not successfully sorted 
        return false;
    // fill the stack     
    cand = m_terminal;
    f_numOps = 0;
    while (cand != NULL)
    {
        seq.push(cand);
        cand = cand->m_parent;
        f_numOps++;
    }
    f_numOps--;
    // init the result
    f_ops = new Operation[f_numOps];
    f_perms = new int*[f_numOps + 1];
    // invert m_mapping
    for (int i = 1; i <= m_permSize; i++)
    {
        if (m_mapping[i] > 0)
            gIntArray1[m_mapping[i]] = i;
        else
            gIntArray1[-m_mapping[i]] = -i;
    }
    // create the initial mapping....
    size = m_permSize;
    value = 1;
    // mark values to delete
    for (int i = 1; i <= size; i++)
        gIntArray0[i] = 0;
    for (int i = 1; i < m_permSize; i++)
    {
        if ((m_source[i-1] + 1 == m_source[i]) || ((m_source[i-1] == m_permSize) && (m_source[i] == 1)) 
          || ((m_source[i-1] == -1) && (m_source[i] == -m_permSize)))
        {
            gIntArray0[abs(m_source[i])] = -1;   // mark value to delete
            size--;
        }
    }
    if ((m_source[m_permSize-1] + 1 == m_source[0]) || ((m_source[m_permSize-1] == m_permSize) && (m_source[0] == 1)) 
      || ((m_source[m_permSize-1] == -1) && (m_source[0] == -m_permSize)))
    {
        gIntArray0[abs(m_source[0])] = -1;        // mark value to delete
        size--;
    }
    if (size == 0)     // already sorted
    {
        blocks = new ElementList[2];
        blocks[1].m_size = m_permSize;
        for (int i = 1; i <= m_permSize; i++)
            blocks[1].m_elements.push_back(gIntArray1[i]);
        firstblock = 1;
        offset = 0;
        firstori = true;                
    }
    else
    {
        // create mapping
        for (int i = 1; i <= m_permSize; i++)
        {
            if (gIntArray0[i] != -1)
                gIntArray0[i] = value++;
        }
        for (int i = 0; i < m_permSize; i++)    // indicate orientation of elements
        {
            if (m_source[i] > 0)
                gBoolArray0[m_source[i]] = true;
            else
                gBoolArray0[-m_source[i]] = false;
        }
        blocks = new ElementList[size+1];
        for (int i = 0; i < m_permSize; i++)
        {
            if (gIntArray0[abs(m_source[i])] != -1)
            {
                if (m_source[i] > 0)
                {
                    blocks[gIntArray0[m_source[i]]].m_elements.push_back(gIntArray1[m_source[i]]);
                    blocks[gIntArray0[m_source[i]]].m_size = 1;
                    for (int j = (m_source[i]==m_permSize?1:m_source[i]+1); (gIntArray0[j] == -1) && gBoolArray0[j]; (j==m_permSize? j=1 : j++))
                    {
                        blocks[gIntArray0[m_source[i]]].m_elements.push_back(gIntArray1[j]);
                        blocks[gIntArray0[m_source[i]]].m_size++;
                    }
                }
                else
                {
                    blocks[gIntArray0[-m_source[i]]].m_elements.push_front(gIntArray1[-m_source[i]]);
                    blocks[gIntArray0[-m_source[i]]].m_size = 1;
                    for (int j = (m_source[i]==-1?m_permSize:-m_source[i]-1); (gIntArray0[j] == -1) && !gBoolArray0[j]; (j==1? j=m_permSize : j--))
                    {
                        blocks[gIntArray0[-m_source[i]]].m_elements.push_front(gIntArray1[j]);
                        blocks[gIntArray0[-m_source[i]]].m_size++;
                    }
                }
            }
        }
        // check for permutation start
        for (int i = 1; (i <= size) && !found; i++)
        {
            offset = 0;
            for (list<int>::iterator j = blocks[i].m_elements.begin(); j != blocks[i].m_elements.end(); j++, offset++)
            {
                if (*j == m_source[0])
                {
                    firstblock = i;
                    firstori = true;
                    found = true;
                    break;
                }
                else if (*j == -m_source[0])
                {
                    firstblock = i;
                    firstori = false;
                    found = true;
                    break;
                }
            }
        }
    }
    // go down the stack
    while (!seq.empty())
    {
        cand = seq.top();
        if (cand->m_op != NULL)
        {
            // translate the operation
            f_ops[opCounter].m_type = cand->m_op->m_type;
            f_ops[opCounter].m_edges[0] = 0;
            f_ops[opCounter].m_edges[1] = 0;
            f_ops[opCounter].m_edges[2] = 0;
            for (int i = 0; i < cand->m_op->m_edges[0]; i++)
                f_ops[opCounter].m_edges[0] += blocks[abs(perm->m_perm[i])].m_size;
            for (int i = 0; i < cand->m_op->m_edges[1]; i++)
                f_ops[opCounter].m_edges[1] += blocks[abs(perm->m_perm[i])].m_size;
            if (f_ops[opCounter].m_type != Operation::REVERSAL)
            {                    
                for (int i = 0; i < cand->m_op->m_edges[2]; i++)
                    f_ops[opCounter].m_edges[2] += blocks[abs(perm->m_perm[i])].m_size;
            }
            // count shift
            counter = 0;
            for (it = 0; abs(perm->m_perm[it]) != firstblock; it++)
                counter += blocks[abs(perm->m_perm[it])].m_size;
            if (perm->m_perm[it] > 0)
                counter += offset;
            else
                counter += (blocks[firstblock].m_size - offset - 1);
            if (firstori == (perm->m_perm[it] > 0))
            {
                f_ops[opCounter].m_edges[0] -= (counter - 1);
                f_ops[opCounter].m_edges[1] -= (counter - 1);
                f_ops[opCounter].m_edges[2] -= (counter - 1);
            }
            else
            {
                f_ops[opCounter].m_edges[0] = counter - f_ops[opCounter].m_edges[0] + 2;
                f_ops[opCounter].m_edges[1] = counter - f_ops[opCounter].m_edges[1] + 2;
                f_ops[opCounter].m_edges[2] = counter - f_ops[opCounter].m_edges[2] + 2;
            }
            if (f_ops[opCounter].m_edges[0] <= 0)
                f_ops[opCounter].m_edges[0] += m_permSize;
            else if (f_ops[opCounter].m_edges[0] > m_permSize)
                f_ops[opCounter].m_edges[0] -= m_permSize;    
            if (f_ops[opCounter].m_edges[1] <= 0)
                f_ops[opCounter].m_edges[1] += m_permSize;
            else if (f_ops[opCounter].m_edges[1] > m_permSize)
                f_ops[opCounter].m_edges[1] -= m_permSize;    
            if (f_ops[opCounter].m_edges[2] <= 0)
                f_ops[opCounter].m_edges[2] += m_permSize; 
            else if (f_ops[opCounter].m_edges[2] > m_permSize)
                f_ops[opCounter].m_edges[2] -= m_permSize;    
            if ((f_ops[opCounter].m_type == Operation::REVERSAL) 
              && (f_ops[opCounter].m_edges[0] > f_ops[opCounter].m_edges[1]))
            {
                swap = f_ops[opCounter].m_edges[0];
                f_ops[opCounter].m_edges[0] = f_ops[opCounter].m_edges[1];
                f_ops[opCounter].m_edges[1] = swap;
            }
            if (f_ops[opCounter].m_type == Operation::TRANSPOSITION)
            {
                if (firstori != (perm->m_perm[it] > 0))
                {
                    swap = f_ops[opCounter].m_edges[0];
                    f_ops[opCounter].m_edges[0] = f_ops[opCounter].m_edges[1];
                    f_ops[opCounter].m_edges[1] = swap;
                }
                if ((f_ops[opCounter].m_edges[0] > f_ops[opCounter].m_edges[1]) 
                  || (f_ops[opCounter].m_edges[1] > f_ops[opCounter].m_edges[2]))
                {
                    swap = f_ops[opCounter].m_edges[0];
                    f_ops[opCounter].m_edges[0] = f_ops[opCounter].m_edges[2];
                    f_ops[opCounter].m_edges[2] = f_ops[opCounter].m_edges[1];
                    f_ops[opCounter].m_edges[1] = swap;
                } 
                if ((f_ops[opCounter].m_edges[0] > f_ops[opCounter].m_edges[1]) 
                  || (f_ops[opCounter].m_edges[1] > f_ops[opCounter].m_edges[2]))
                {
                    swap = f_ops[opCounter].m_edges[0];
                    f_ops[opCounter].m_edges[0] = f_ops[opCounter].m_edges[2];
                    f_ops[opCounter].m_edges[2] = f_ops[opCounter].m_edges[1];
                    f_ops[opCounter].m_edges[1] = swap;
                } 
            }
            if (f_ops[opCounter].m_type == Operation::TRANSREVA)
            {
                if (firstori != (perm->m_perm[it] > 0))
                {
                    swap = f_ops[opCounter].m_edges[0];
                    f_ops[opCounter].m_edges[0] = f_ops[opCounter].m_edges[1];
                    f_ops[opCounter].m_edges[1] = swap;
                }
            }                    
            if (f_ops[opCounter].m_type == Operation::TRANSREVB)
            {
                if (firstori == (perm->m_perm[it] > 0))
                {
                    swap = f_ops[opCounter].m_edges[0];
                    f_ops[opCounter].m_edges[0] = f_ops[opCounter].m_edges[1];
                    f_ops[opCounter].m_edges[1] = f_ops[opCounter].m_edges[2];
                    f_ops[opCounter].m_edges[2] = swap;
                }
                else
                {
                    swap = f_ops[opCounter].m_edges[0];
                    f_ops[opCounter].m_edges[0] = f_ops[opCounter].m_edges[2];
                    f_ops[opCounter].m_edges[2] = swap;
                }
            }                    
            if (f_ops[opCounter].m_type == Operation::REVREV)
            {
                if (firstori == (perm->m_perm[it] > 0))
                {
                    swap = f_ops[opCounter].m_edges[0];
                    f_ops[opCounter].m_edges[0] = f_ops[opCounter].m_edges[2];
                    f_ops[opCounter].m_edges[2] = f_ops[opCounter].m_edges[1];
                    f_ops[opCounter].m_edges[1] = swap;
                }
                else
                {
                    swap = f_ops[opCounter].m_edges[1];
                    f_ops[opCounter].m_edges[1] = f_ops[opCounter].m_edges[2];
                    f_ops[opCounter].m_edges[2] = swap;
                }
            }             
            if ((f_ops[opCounter].m_type == Operation::TRANSREVA) || (f_ops[opCounter].m_type == Operation::TRANSREVB)
              || (f_ops[opCounter].m_type == Operation::REVREV))
                f_ops[opCounter].m_type = Operation::INVTRANS;      
            // calculate the effects of the operation: does the startpoint change?
            shift = 0;
            if (f_ops[opCounter].m_type == Operation::REVERSAL)
            {
                if (f_ops[opCounter].m_edges[0] == 1)
                {
                    shift = f_ops[opCounter].m_edges[1] - 2;
                    invert = true;
                }
                else
                    invert = false;
            }
            else if (f_ops[opCounter].m_type == Operation::TRANSPOSITION)
            {
                if (f_ops[opCounter].m_edges[0] == 1)
                    shift = f_ops[opCounter].m_edges[1] - 1;
                invert = false;
            }
            else           // any kind of inverted transposition
            {
                if ((f_ops[opCounter].m_edges[0] == 1) || ((f_ops[opCounter].m_edges[1] != 1) 
                  && (f_ops[opCounter].m_edges[0] > f_ops[opCounter].m_edges[1])))      // first point in inverted segment
                {
                    if ((m_permSize  + 1 - f_ops[opCounter].m_edges[0]) % m_permSize 
                      < (m_permSize + f_ops[opCounter].m_edges[2] - f_ops[opCounter].m_edges[1]) % m_permSize)
                    {
                        shift = f_ops[opCounter].m_edges[1] - f_ops[opCounter].m_edges[0];
                        invert = false;
                    }
                    else
                    {        
                        shift = f_ops[opCounter].m_edges[2] + f_ops[opCounter].m_edges[0] - 3;
                        invert = true;
                    }
                }
                else if ((f_ops[opCounter].m_edges[1] == 1) || ((f_ops[opCounter].m_edges[2] != 1) 
                  && (f_ops[opCounter].m_edges[1] > f_ops[opCounter].m_edges[2]))) // first point in moved segment
                {
                    if ((1 - f_ops[opCounter].m_edges[0] + m_permSize) % m_permSize 
                      < (f_ops[opCounter].m_edges[2] - f_ops[opCounter].m_edges[1] + m_permSize) % m_permSize)
                    {
                        shift = f_ops[opCounter].m_edges[1] - f_ops[opCounter].m_edges[0];
                        invert = false;
                    }
                    else
                    {
                        shift = f_ops[opCounter].m_edges[0] + f_ops[opCounter].m_edges[2] - 3;
                        invert = true;
                    }    
                }
                else
                    invert = false;
                if (shift < 0)
                    shift += m_permSize;
                else if (shift >= m_permSize)
                    shift -= m_permSize;     
            }
            tmp = 0;
            while (abs(perm->m_perm[tmp]) != firstblock)
                tmp++;
            ori = ((perm->m_perm[tmp] > 0) == firstori);
            while (shift > 0)
            {
                if (ori == (perm->m_perm[tmp] > 0))
                {
                    if (offset + shift >= blocks[abs(perm->m_perm[tmp])].m_size)
                    {
                        shift -= (blocks[abs(perm->m_perm[tmp])].m_size - offset);
                        if (ori)
                            tmp = (tmp + 1) % size;
                        else
                            tmp = (tmp - 1 + size) % size;    
                        if (ori == (perm->m_perm[tmp] > 0))
                            offset = 0;
                        else
                            offset = blocks[abs(perm->m_perm[tmp])].m_size - 1;
                    }
                    else
                    {
                        offset += shift;
                        shift = 0;    
                    }
                }
                else
                {
                    if (offset < shift)
                    {
                        shift -= (offset + 1);
                        if (ori)
                            tmp = (tmp + 1) % size;
                        else
                            tmp = (tmp - 1 + size) % size;    
                        if (ori == (perm->m_perm[tmp] > 0))
                            offset = 0;
                        else
                            offset = blocks[abs(perm->m_perm[tmp])].m_size - 1;
                    }
                    else
                    {
                        offset -= shift;
                        shift = 0;
                    }
                }
            }
            firstblock = abs(perm->m_perm[tmp]);
            firstori = (ori == (perm->m_perm[tmp] > 0)) ^ invert;
            // calculate the effects of the operation: merging of blocks 
            m[0] = -1;
            m[1] = -1;
            m[2] = -1;
            b0 = (cand->m_op->m_edges[0] == 0? perm->m_perm[perm->m_size-1] : perm->m_perm[cand->m_op->m_edges[0]-1]);
            b1 = perm->m_perm[cand->m_op->m_edges[0] % size];
            b2 = (cand->m_op->m_edges[1] == 0? perm->m_perm[perm->m_size-1] : perm->m_perm[cand->m_op->m_edges[1]-1]);
            b3 = perm->m_perm[cand->m_op->m_edges[1] % size];
            if (cand->m_op->m_type != Operation::REVERSAL)
            {
                b4 = (cand->m_op->m_edges[2] == 0? perm->m_perm[perm->m_size-1] : perm->m_perm[cand->m_op->m_edges[2]-1]);
                b5 = perm->m_perm[cand->m_op->m_edges[2] % size];
            }
            if (cand->m_op->m_type == Operation::REVERSAL)
            {
                if (b0 > 0)
                {
                    if ((b0 % perm->m_size) + 1 == -b2)
                        m[0] = b0;
                }
                else if (b2 > 0)
                {
                    if ((b2 % perm->m_size) + 1 == -b0)
                        m[0] = b2;
                }
                if (b1 < 0)
                {
                    if ((-b1 % perm->m_size) + 1 == b3)
                        m[1] = -b1;
                }
                else if (b3 < 0)
                {
                    if ((-b3 % perm->m_size) + 1 == b1)
                        m[1] = -b3;
                }
            }
            else if (cand->m_op->m_type == Operation::TRANSPOSITION)
            {
                if (b0 > 0)
                {
                    if ((b0 % perm->m_size) + 1 == b3)
                        m[0] = b0;
                }
                else if (b3 < 0)
                {
                    if ((-b3 % perm->m_size) + 1 == -b0)
                        m[0] = -b3;
                }
                if (b2 > 0)
                {
                    if ((b2 % perm->m_size) + 1 == b5)
                        m[1] = b2;
                }
                else if (b5 < 0)
                {
                    if ((-b5 % perm->m_size) + 1 == -b2)
                        m[1] = -b5;
                }
                if (b4 > 0)
                {
                    if ((b4 % perm->m_size) + 1 == b1)
                        m[2] = b4;
                }
                else if (b1 < 0)
                {
                    if ((-b1 % perm->m_size) + 1 == -b4)
                        m[2] = -b1;
                }
            }
            else if (cand->m_op->m_type == Operation::TRANSREVA)
            {
                if (b0 > 0)
                {
                    if ((b0 % perm->m_size) + 1 == b3)
                        m[0] = b0;
                }
                else if (b3 < 0)
                {
                    if ((-b3 % perm->m_size) + 1 == -b0)
                        m[0] = -b3;
                }
                if (b4 > 0)
                {
                    if ((b4 % perm->m_size) + 1 == -b2)
                        m[1] = b4;
                }
                else if (b2 > 0)
                {
                    if ((b2 % perm->m_size) + 1 == -b4)
                        m[1] = b2;
                }
                if (b1 < 0)
                {
                    if ((-b1 % perm->m_size) + 1 == b5)
                        m[2] = -b1;
                }
                else if (b5 < 0)
                {
                    if ((-b5 % perm->m_size) + 1 == b1)
                        m[2] = -b5;
                }
            }
            else if (cand->m_op->m_type == Operation::TRANSREVB)
            {
                if (b0 > 0)
                {
                    if ((b0 % perm->m_size) + 1 == -b4)
                        m[0] = b0;
                }
                else if (b4 > 0)
                {
                    if ((b4 % perm->m_size) + 1 == -b0)
                        m[0] = b4;
                }
                if (b3 < 0)
                {
                    if ((-b3 % perm->m_size) + 1 == b1)
                        m[1] = -b3;
                }
                else if (b1 < 0)
                {
                    if ((-b1 % perm->m_size) + 1 == b3)
                        m[1] = -b1;
                }
                if (b2 > 0)
                {
                    if ((b2 % perm->m_size) + 1 == b5)
                        m[2] = b2;
                }
                else if (b5 < 0)
                {
                    if ((-b5 % perm->m_size) + 1 == -b2)
                        m[2] = -b5;
                }
            }
            else if (cand->m_op->m_type == Operation::REVREV)
            {
                if (b0 > 0)
                {
                    if ((b0 % perm->m_size) + 1 == -b2)
                        m[0] = b0;
                }
                else if (b2 > 0)
                {
                    if ((b2 % perm->m_size) + 1 == -b0)
                        m[0] = b2;
                }
                if (b1 < 0)
                {
                    if ((-b1 % perm->m_size) + 1 == -b4)
                        m[1] = -b1;
                }
                else if (b4 > 0)
                {
                    if ((b4 % perm->m_size) + 1 == b1)
                        m[1] = b4;
                }
                if (b3 < 0)
                {
                    if ((-b3 % perm->m_size) + 1 == b5)
                        m[2] = -b3;
                }
                else if (b5 < 0)
                {
                    if ((-b5 % perm->m_size) + 1 == b3)
                        m[2] = -b5;
                }
            }
            // merge
            for (int i = 0; i < 3; i++)
            {
                if (m[i] < 0)
                    continue;
                if (size == 1)     // never merge all
                    break;
                tmp = (m[i] == size? 1 : m[i] + 1);
                if (tmp == 1)
                {
                    if (firstblock == m[i])
                        firstblock = 1;
                    else if (firstblock == 1)
                        offset += blocks[m[i]].m_size;    
                    blocks[1].m_elements.splice(blocks[1].m_elements.begin(), blocks[m[i]].m_elements);
                    blocks[1].m_size += blocks[m[i]].m_size;
                    
                }
                else
                {
                    if (firstblock == tmp)
                    {
                        firstblock = m[i];
                        offset += blocks[m[i]].m_size;
                    }
                    else if (firstblock > tmp)
                        firstblock--;
                    blocks[m[i]].m_elements.splice(blocks[m[i]].m_elements.end(), blocks[tmp].m_elements);
                    blocks[m[i]].m_size += blocks[tmp].m_size;
                    for (int j = i+1; j < 3; j++)
                    {
                        if (m[j] >= tmp)
                            m[j]--;
                    }
                    for (int j = tmp; j < size; j++)
                    {
                        blocks[j].m_elements = blocks[j+1].m_elements;
                        blocks[j].m_size = blocks[j+1].m_size;
                    }
                }            
                size--;
            }
            opCounter++;
        }
        size = cand->m_perm->m_size;
        perm = cand->m_perm;
        if (size == 0)
        {
            value = 0;
            ori = firstori;
            size = 1;
        }
        else
        {
            for (int i = 0; i < size; i++)
            {
                if (perm->m_perm[i] == firstblock)
                {
                    value = i;
                    ori = firstori;
                    break;
                }
                else if (perm->m_perm[i] == -firstblock)
                {
                    value = i;
                    ori = !firstori;
                    break;
                }
            }
        }
        counter = (firstori? -offset : -blocks[firstblock].m_size + offset + 1);
        f_perms[opCounter] = new int[m_permSize];
        while (counter < m_permSize)    // print exactly this amount of elements
        {
            if (ori == (perm->m_perm[value] > 0))    // run through this block in positive direction
            {
                for (list<int>::iterator i = blocks[abs(perm->m_perm[value])].m_elements.begin(); 
                  (i != blocks[abs(perm->m_perm[value])].m_elements.end()) && (counter < m_permSize); i++, counter++)
                {
                    if (counter >= 0)
                        f_perms[opCounter][counter] = *i;
                }
            }
            else                             // run through this block in negative direction
            {
                for (list<int>::reverse_iterator i = blocks[abs(perm->m_perm[value])].m_elements.rbegin(); 
                  (i != blocks[abs(perm->m_perm[value])].m_elements.rend()) && (counter < m_permSize); i++, counter++)
                {
                    if (counter >= 0)
                        f_perms[opCounter][counter] = -(*i);
                }
            }
            if (ori)
                value = (value+1)%size;
            else
                value = (value+size-1)%size;    
        }
        seq.pop();                
    }
    delete[] blocks;
    return true;    
}


void WeightedBB::printResult(int f_printLevel, std::ostream &out)
{
    stack<Candidate*> seq;    // sequence of candidates
    Candidate* cand;          // A candidate of the sequence
    Permutation* perm = NULL; // its permutation
    int size;                 // its size
    ElementList* blocks;      // the mapping from internal elements to real elements
    int value;                // an elements value
    bool found = false;       // used for searching, indicates that we have found an element
    int firstblock = -1;      // id of the real first block
    int offset = 0;           // position of the real first element in its block
    bool firstori = true;     // true if it has positive orientation
    bool ori = true;          // orientation to use for the current perm
    int counter;              // counts printed elements
    Operation op;             // the current operation
    int it;                   // loop iterator
    int swap;                 // swap two variables
    int b0, b1, b2, b3, b4, b5;  // border elements of an operation
    int m[3];                 // merge points (smaller elements)
    int shift;                // shift of the start point
    bool invert;              // true if we must invert the new start point
    int tmp;

    /**
     * Prints the result to stdout. sort must have been called before this.
     * f_printLevel determins how much information will be written.
     * f_printLevel:
     * 0: only write overall weight
     * 1: write operations
     * 2: write operations + overall weight
     * 3: write operations + permutations
     * 4: write operations + permutations + overall weight
     * @param f_printLevel  the print level
     */
    out << CONSOLE_BOLD;
    if (m_distance == -1)
    {
        out << "no solution within upper bound found\n";
        out << CONSOLE_RESET;
        return;
    }
    else if (m_distance == -2)
    {
        out << "calculation required too much resources -> aborted\n";
        out << CONSOLE_RESET;
        return;
    }
    if (f_printLevel > 0)
    {
        // fill the stack     
        cand = m_terminal;
        while (cand != NULL)
        {
            seq.push(cand);
            cand = cand->m_parent;
        }
        // invert m_mapping
        for (int i = 1; i <= m_permSize; i++)
        {
            if (m_mapping[i] > 0)
                gIntArray1[m_mapping[i]] = i;
            else
                gIntArray1[-m_mapping[i]] = -i;
        }
        // create the initial mapping....
        size = m_permSize;
        value = 1;
        // mark values to delete
        for (int i = 1; i <= size; i++)
            gIntArray0[i] = 0;
        for (int i = 1; i < m_permSize; i++)
        {
            if ((m_source[i-1] + 1 == m_source[i]) || ((m_source[i-1] == m_permSize) && (m_source[i] == 1)) 
              || ((m_source[i-1] == -1) && (m_source[i] == -m_permSize)))
            {
                gIntArray0[abs(m_source[i])] = -1;   // mark value to delete
                size--;
            }
        }
        if ((m_source[m_permSize-1] + 1 == m_source[0]) || ((m_source[m_permSize-1] == m_permSize) && (m_source[0] == 1)) 
          || ((m_source[m_permSize-1] == -1) && (m_source[0] == -m_permSize)))
        {
            gIntArray0[abs(m_source[0])] = -1;        // mark value to delete
            size--;
        }
        if (size == 0)     // already sorted
        {
            blocks = new ElementList[2];
            blocks[1].m_size = m_permSize;
            for (int i = 1; i <= m_permSize; i++)
                blocks[1].m_elements.push_back(gIntArray1[i]);
            firstblock = 1;
            offset = 0;
            firstori = true;                
        }
        else
        {
            // create mapping
            for (int i = 1; i <= m_permSize; i++)
            {
                if (gIntArray0[i] != -1)
                    gIntArray0[i] = value++;
            }
            for (int i = 0; i < m_permSize; i++)    // indicate orientation of elements
            {
                if (m_source[i] > 0)
                    gBoolArray0[m_source[i]] = true;
                else
                    gBoolArray0[-m_source[i]] = false;
            }
            blocks = new ElementList[size+1];
            for (int i = 0; i < m_permSize; i++)
            {
                if (gIntArray0[abs(m_source[i])] != -1)
                {
                    if (m_source[i] > 0)
                    {
                        blocks[gIntArray0[m_source[i]]].m_elements.push_back(gIntArray1[m_source[i]]);
                        blocks[gIntArray0[m_source[i]]].m_size = 1;
                        for (int j = (m_source[i]==m_permSize?1:m_source[i]+1); (gIntArray0[j] == -1) && gBoolArray0[j]; (j==m_permSize? j=1 : j++))
                        {
                            blocks[gIntArray0[m_source[i]]].m_elements.push_back(gIntArray1[j]);
                            blocks[gIntArray0[m_source[i]]].m_size++;
                        }
                    }
                    else
                    {
                        blocks[gIntArray0[-m_source[i]]].m_elements.push_front(gIntArray1[-m_source[i]]);
                        blocks[gIntArray0[-m_source[i]]].m_size = 1;
                        for (int j = (m_source[i]==-1?m_permSize:-m_source[i]-1); (gIntArray0[j] == -1) && !gBoolArray0[j]; (j==1? j=m_permSize : j--))
                        {
                            blocks[gIntArray0[-m_source[i]]].m_elements.push_front(gIntArray1[j]);
                            blocks[gIntArray0[-m_source[i]]].m_size++;
                        }
                    }
                }
            }
            // check for permutation start
            for (int i = 1; (i <= size) && !found; i++)
            {
                offset = 0;
                for (list<int>::iterator j = blocks[i].m_elements.begin(); j != blocks[i].m_elements.end(); j++, offset++)
                {
                    if (*j == m_source[0])
                    {
                        firstblock = i;
                        firstori = true;
                        found = true;
                        break;
                    }
                    else if (*j == -m_source[0])
                    {
                        firstblock = i;
                        firstori = false;
                        found = true;
                        break;
                    }
                }
            }
        }
        // go down the stack
        while (!seq.empty())
        {
            cand = seq.top();
            if (cand->m_op != NULL)
            {
                // translate the operation
                op.m_type = cand->m_op->m_type;
                op.m_edges[0] = 0;
                op.m_edges[1] = 0;
                op.m_edges[2] = 0;
                for (int i = 0; i < cand->m_op->m_edges[0]; i++)
                    op.m_edges[0] += blocks[abs(perm->m_perm[i])].m_size;
                for (int i = 0; i < cand->m_op->m_edges[1]; i++)
                    op.m_edges[1] += blocks[abs(perm->m_perm[i])].m_size;
                if (op.m_type != Operation::REVERSAL)
                {                    
                    for (int i = 0; i < cand->m_op->m_edges[2]; i++)
                        op.m_edges[2] += blocks[abs(perm->m_perm[i])].m_size;
                }
                // count shift
                counter = 0;
                for (it = 0; abs(perm->m_perm[it]) != firstblock; it++)
                    counter += blocks[abs(perm->m_perm[it])].m_size;
                if (perm->m_perm[it] > 0)
                    counter += offset;
                else
                    counter += (blocks[firstblock].m_size - offset - 1);
                if (firstori == (perm->m_perm[it] > 0))
                {
                    op.m_edges[0] -= (counter - 1);
                    op.m_edges[1] -= (counter - 1);
                    op.m_edges[2] -= (counter - 1);
                }
                else
                {
                    op.m_edges[0] = counter - op.m_edges[0] + 2;
                    op.m_edges[1] = counter - op.m_edges[1] + 2;
                    op.m_edges[2] = counter - op.m_edges[2] + 2;
                }
                if (op.m_edges[0] <= 0)
                    op.m_edges[0] += m_permSize;
                else if (op.m_edges[0] > m_permSize)
                    op.m_edges[0] -= m_permSize;    
                if (op.m_edges[1] <= 0)
                    op.m_edges[1] += m_permSize;
                else if (op.m_edges[1] > m_permSize)
                    op.m_edges[1] -= m_permSize;    
                if (op.m_edges[2] <= 0)
                    op.m_edges[2] += m_permSize; 
                else if (op.m_edges[2] > m_permSize)
                    op.m_edges[2] -= m_permSize;    
                if ((op.m_type == Operation::REVERSAL) && (op.m_edges[0] > op.m_edges[1]))
                {
                    swap = op.m_edges[0];
                    op.m_edges[0] = op.m_edges[1];
                    op.m_edges[1] = swap;
                }
                if (op.m_type == Operation::TRANSPOSITION)
                {
                    if (firstori != (perm->m_perm[it] > 0))
                    {
                        swap = op.m_edges[0];
                        op.m_edges[0] = op.m_edges[1];
                        op.m_edges[1] = swap;
                    }
                    if ((op.m_edges[0] > op.m_edges[1]) || (op.m_edges[1] > op.m_edges[2]))
                    {
                        swap = op.m_edges[0];
                        op.m_edges[0] = op.m_edges[2];
                        op.m_edges[2] = op.m_edges[1];
                        op.m_edges[1] = swap;
                    } 
                    if ((op.m_edges[0] > op.m_edges[1]) || (op.m_edges[1] > op.m_edges[2]))
                    {
                        swap = op.m_edges[0];
                        op.m_edges[0] = op.m_edges[2];
                        op.m_edges[2] = op.m_edges[1];
                        op.m_edges[1] = swap;
                    } 
                }
                if (op.m_type == Operation::TRANSREVA)
                {
//                    cout << "TRANSREVA" << op.m_edges[0] << op.m_edges[1] << op.m_edges[2] << "\n";
                    if (firstori != (perm->m_perm[it] > 0))
                    {
                        swap = op.m_edges[0];
                        op.m_edges[0] = op.m_edges[1];
                        op.m_edges[1] = swap;
                    }
                }                    
                if (op.m_type == Operation::TRANSREVB)
                {
//                    cout << "TRANSREVB" << op.m_edges[0] << op.m_edges[1] << op.m_edges[2] << "\n";
                    if (firstori == (perm->m_perm[it] > 0))
                    {
                        swap = op.m_edges[0];
                        op.m_edges[0] = op.m_edges[1];
                        op.m_edges[1] = op.m_edges[2];
                        op.m_edges[2] = swap;
                    }
                    else
                    {
                        swap = op.m_edges[0];
                        op.m_edges[0] = op.m_edges[2];
                        op.m_edges[2] = swap;
                    }
                }                    
                if (op.m_type == Operation::REVREV)
                {
//                    cout << "REVREV" << op.m_edges[0] << op.m_edges[1] << op.m_edges[2] << "\n";
                    if (firstori == (perm->m_perm[it] > 0))
                    {
                        swap = op.m_edges[0];
                        op.m_edges[0] = op.m_edges[2];
                        op.m_edges[2] = op.m_edges[1];
                        op.m_edges[1] = swap;
                    }
                    else
                    {
                        swap = op.m_edges[1];
                        op.m_edges[1] = op.m_edges[2];
                        op.m_edges[2] = swap;
                    }
                } 
                
                if ((op.m_type == Operation::TRANSREVA) || (op.m_type == Operation::TRANSREVB)
                  || (op.m_type == Operation::REVREV))
                    op.m_type = Operation::INVTRANS;     
                out << CONSOLE_COLOR_WHITE;
                printOperation(&op, out);
                // delayed print of the permutation
                if (f_printLevel > 2)
                {
                    if (((op.m_type == Operation::REVERSAL) || (op.m_type == Operation::INVTRANS))
                      && ((op.m_edges[0] == 1) || ((op.m_edges[1] != 1) && (op.m_edges[0] > op.m_edges[1]))))
                        out << CONSOLE_COLOR_RED;
                    else if ((op.m_type == Operation::TRANSPOSITION)
                      && ((op.m_edges[0] == 1) || ((op.m_edges[1] != 1) && (op.m_edges[0] > op.m_edges[1]))))
                        out << CONSOLE_COLOR_CYAN;
                    else if (((op.m_type == Operation::TRANSPOSITION) || (op.m_type == Operation::INVTRANS))
                      && ((op.m_edges[1] == 1) || ((op.m_edges[2] != 1) && (op.m_edges[1] > op.m_edges[2]))))
                        out << CONSOLE_COLOR_GREEN;
                    else
                        out << CONSOLE_COLOR_WHITE;
                    for (int i = 0; i < m_permSize; i++)
                    {
                        if (i + 1 == op.m_edges[0]) 
                        {
                            if ((op.m_type == Operation::REVERSAL) || (op.m_type == Operation::INVTRANS))
                                out << CONSOLE_COLOR_RED;
                            else
                                out << CONSOLE_COLOR_CYAN;
                        }
                        else if (i + 1 == op.m_edges[1]) 
                        {
                            if ((op.m_type == Operation::TRANSPOSITION) || (op.m_type == Operation::INVTRANS))
                                out << CONSOLE_COLOR_GREEN;
                            else
                                out << CONSOLE_COLOR_WHITE;
                        }
                        else if ((i + 1 == op.m_edges[2]) && (op.m_type != Operation::REVERSAL))
                            out << CONSOLE_COLOR_WHITE;
                        out << gIntArray2[i] << " ";
                    }
                }
                out << endl;
                // calculate the effects of the operation: does the startpoint change?
                shift = 0;
                if (op.m_type == Operation::REVERSAL)
                {
                    if (op.m_edges[0] == 1)
                    {
                        shift = op.m_edges[1] - 2;
                        invert = true;
                    }
                    else
                        invert = false;
                }
                else if (op.m_type == Operation::TRANSPOSITION)
                {
                    if (op.m_edges[0] == 1)
                        shift = op.m_edges[1] - 1;
                    invert = false;
                }
                else           // any kind of inverted transposition
                {
                    if ((op.m_edges[0] == 1) || ((op.m_edges[1] != 1) && (op.m_edges[0] > op.m_edges[1])))      // first point in inverted segment
                    {
                        if ((m_permSize  + 1 - op.m_edges[0]) % m_permSize < (m_permSize + op.m_edges[2] - op.m_edges[1]) % m_permSize)
                        {
                            shift = op.m_edges[1] - op.m_edges[0];
                            invert = false;
                        }
                        else
                        {        
                            shift = op.m_edges[2] + op.m_edges[0] - 3;
                            invert = true;
                        }
                    }
                    else if ((op.m_edges[1] == 1) || ((op.m_edges[2] != 1) && (op.m_edges[1] > op.m_edges[2]))) // first point in moved segment
                    {
                        if ((1 - op.m_edges[0] + m_permSize) % m_permSize < (op.m_edges[2] - op.m_edges[1] + m_permSize) % m_permSize)
                        {
                            shift = op.m_edges[1] - op.m_edges[0];
                            invert = false;
                        }
                        else
                        {
                            shift = op.m_edges[0] + op.m_edges[2] - 3;
                            invert = true;
                        }    
                    }
                    else
                        invert = false;
                    if (shift < 0)
                        shift += m_permSize;
                    else if (shift >= m_permSize)
                        shift -= m_permSize;     
                }
                tmp = 0;
                while (abs(perm->m_perm[tmp]) != firstblock)
                    tmp++;
                ori = ((perm->m_perm[tmp] > 0) == firstori);
                while (shift > 0)
                {
                    if (ori == (perm->m_perm[tmp] > 0))
                    {
                        if (offset + shift >= blocks[abs(perm->m_perm[tmp])].m_size)
                        {
                            shift -= (blocks[abs(perm->m_perm[tmp])].m_size - offset);
                            if (ori)
                                tmp = (tmp + 1) % size;
                            else
                                tmp = (tmp - 1 + size) % size;    
                            if (ori == (perm->m_perm[tmp] > 0))
                                offset = 0;
                            else
                                offset = blocks[abs(perm->m_perm[tmp])].m_size - 1;
                        }
                        else
                        {
                            offset += shift;
                            shift = 0;    
                        }
                    }
                    else
                    {
                        if (offset < shift)
                        {
                            shift -= (offset + 1);
                            if (ori)
                                tmp = (tmp + 1) % size;
                            else
                                tmp = (tmp - 1 + size) % size;    
                            if (ori == (perm->m_perm[tmp] > 0))
                                offset = 0;
                            else
                                offset = blocks[abs(perm->m_perm[tmp])].m_size - 1;
                        }
                        else
                        {
                            offset -= shift;
                            shift = 0;
                        }
                    }
                }
                firstblock = abs(perm->m_perm[tmp]);
                firstori = (ori == (perm->m_perm[tmp] > 0)) ^ invert;
                // calculate the effects of the operation: merging of blocks 
                m[0] = -1;
                m[1] = -1;
                m[2] = -1;
                b0 = (cand->m_op->m_edges[0] == 0? perm->m_perm[perm->m_size-1] : perm->m_perm[cand->m_op->m_edges[0]-1]);
                b1 = perm->m_perm[cand->m_op->m_edges[0] % size];
                b2 = (cand->m_op->m_edges[1] == 0? perm->m_perm[perm->m_size-1] : perm->m_perm[cand->m_op->m_edges[1]-1]);
                b3 = perm->m_perm[cand->m_op->m_edges[1] % size];
                if (cand->m_op->m_type != Operation::REVERSAL)
                {
                    b4 = (cand->m_op->m_edges[2] == 0? perm->m_perm[perm->m_size-1] : perm->m_perm[cand->m_op->m_edges[2]-1]);
                    b5 = perm->m_perm[cand->m_op->m_edges[2] % size];
                }
                if (cand->m_op->m_type == Operation::REVERSAL)
                {
                    if (b0 > 0)
                    {
                        if ((b0 % perm->m_size) + 1 == -b2)
                            m[0] = b0;
                    }
                    else if (b2 > 0)
                    {
                        if ((b2 % perm->m_size) + 1 == -b0)
                            m[0] = b2;
                    }
                    if (b1 < 0)
                    {
                        if ((-b1 % perm->m_size) + 1 == b3)
                            m[1] = -b1;
                    }
                    else if (b3 < 0)
                    {
                        if ((-b3 % perm->m_size) + 1 == b1)
                            m[1] = -b3;
                    }
                }
                else if (cand->m_op->m_type == Operation::TRANSPOSITION)
                {
                    if (b0 > 0)
                    {
                        if ((b0 % perm->m_size) + 1 == b3)
                            m[0] = b0;
                    }
                    else if (b3 < 0)
                    {
                        if ((-b3 % perm->m_size) + 1 == -b0)
                            m[0] = -b3;
                    }
                    if (b2 > 0)
                    {
                        if ((b2 % perm->m_size) + 1 == b5)
                            m[1] = b2;
                    }
                    else if (b5 < 0)
                    {
                        if ((-b5 % perm->m_size) + 1 == -b2)
                            m[1] = -b5;
                    }
                    if (b4 > 0)
                    {
                        if ((b4 % perm->m_size) + 1 == b1)
                            m[2] = b4;
                    }
                    else if (b1 < 0)
                    {
                        if ((-b1 % perm->m_size) + 1 == -b4)
                            m[2] = -b1;
                    }
                }
                else if (cand->m_op->m_type == Operation::TRANSREVA)
                {
                    if (b0 > 0)
                    {
                        if ((b0 % perm->m_size) + 1 == b3)
                            m[0] = b0;
                    }
                    else if (b3 < 0)
                    {
                        if ((-b3 % perm->m_size) + 1 == -b0)
                            m[0] = -b3;
                    }
                    if (b4 > 0)
                    {
                        if ((b4 % perm->m_size) + 1 == -b2)
                            m[1] = b4;
                    }
                    else if (b2 > 0)
                    {
                        if ((b2 % perm->m_size) + 1 == -b4)
                            m[1] = b2;
                    }
                    if (b1 < 0)
                    {
                        if ((-b1 % perm->m_size) + 1 == b5)
                            m[2] = -b1;
                    }
                    else if (b5 < 0)
                    {
                        if ((-b5 % perm->m_size) + 1 == b1)
                            m[2] = -b5;
                    }
                }
                else if (cand->m_op->m_type == Operation::TRANSREVB)
                {
                    if (b0 > 0)
                    {
                        if ((b0 % perm->m_size) + 1 == -b4)
                            m[0] = b0;
                    }
                    else if (b4 > 0)
                    {
                        if ((b4 % perm->m_size) + 1 == -b0)
                            m[0] = b4;
                    }
                    if (b3 < 0)
                    {
                        if ((-b3 % perm->m_size) + 1 == b1)
                            m[1] = -b3;
                    }
                    else if (b1 < 0)
                    {
                        if ((-b1 % perm->m_size) + 1 == b3)
                            m[1] = -b1;
                    }
                    if (b2 > 0)
                    {
                        if ((b2 % perm->m_size) + 1 == b5)
                            m[2] = b2;
                    }
                    else if (b5 < 0)
                    {
                        if ((-b5 % perm->m_size) + 1 == -b2)
                            m[2] = -b5;
                    }
                }
                else if (cand->m_op->m_type == Operation::REVREV)
                {
                    if (b0 > 0)
                    {
                        if ((b0 % perm->m_size) + 1 == -b2)
                            m[0] = b0;
                    }
                    else if (b2 > 0)
                    {
                        if ((b2 % perm->m_size) + 1 == -b0)
                            m[0] = b2;
                    }
                    if (b1 < 0)
                    {
                        if ((-b1 % perm->m_size) + 1 == -b4)
                            m[1] = -b1;
                    }
                    else if (b4 > 0)
                    {
                        if ((b4 % perm->m_size) + 1 == b1)
                            m[1] = b4;
                    }
                    if (b3 < 0)
                    {
                        if ((-b3 % perm->m_size) + 1 == b5)
                            m[2] = -b3;
                    }
                    else if (b5 < 0)
                    {
                        if ((-b5 % perm->m_size) + 1 == b3)
                            m[2] = -b5;
                    }
                }
                // merge
                for (int i = 0; i < 3; i++)
                {
                    if (m[i] < 0)
                        continue;
                    if (size == 1)     // never merge all
                        break;
                    tmp = (m[i] == size? 1 : m[i] + 1);
                    if (tmp == 1)
                    {
                        if (firstblock == m[i])
                            firstblock = 1;
                        else if (firstblock == 1)
                            offset += blocks[m[i]].m_size;    
                        blocks[1].m_elements.splice(blocks[1].m_elements.begin(), blocks[m[i]].m_elements);
                        blocks[1].m_size += blocks[m[i]].m_size;
                        
                    }
                    else
                    {
                        if (firstblock == tmp)
                        {
                            firstblock = m[i];
                            offset += blocks[m[i]].m_size;
                        }
                        else if (firstblock > tmp)
                            firstblock--;

                        blocks[m[i]].m_elements.splice(blocks[m[i]].m_elements.end(), blocks[tmp].m_elements);
                        blocks[m[i]].m_size += blocks[tmp].m_size;
                        for (int j = i+1; j < 3; j++)
                        {
                            if (m[j] >= tmp)
                                m[j]--;
                        }
                        for (int j = tmp; j < size; j++)
                        {
                            blocks[j].m_elements = blocks[j+1].m_elements;
                            blocks[j].m_size = blocks[j+1].m_size;
                        }
                    }            
                    size--;
                }
            }
            size = cand->m_perm->m_size;
            perm = cand->m_perm;
            if (f_printLevel > 2)
            {
                if (size == 0)
                {
                    value = 0;
                    ori = firstori;
                    size = 1;
                }
                else
                {
                    for (int i = 0; i < size; i++)
                    {
                        if (perm->m_perm[i] == firstblock)
                        {
                            value = i;
                            ori = firstori;
                            break;
                        }
                        else if (perm->m_perm[i] == -firstblock)
                        {
                            value = i;
                            ori = !firstori;
                            break;
                        }
                    }
                }
                counter = (firstori? -offset : -blocks[firstblock].m_size + offset + 1);
                while (counter < m_permSize)    // print exactly this amount of elements
                {
                    if (ori == (perm->m_perm[value] > 0))    // run through this block in positive direction
                    {
                        for (list<int>::iterator i = blocks[abs(perm->m_perm[value])].m_elements.begin(); 
                          (i != blocks[abs(perm->m_perm[value])].m_elements.end()) && (counter < m_permSize); i++, counter++)
                        {
                            if (counter >= 0)  
                                gIntArray2[counter] = *i;     // delay the output
                        }
                    }
                    else                             // run through this block in negative direction
                    {
                        for (list<int>::reverse_iterator i = blocks[abs(perm->m_perm[value])].m_elements.rbegin(); 
                          (i != blocks[abs(perm->m_perm[value])].m_elements.rend()) && (counter < m_permSize); i++, counter++)
                        {
                            if (counter >= 0)  
                                gIntArray2[counter] = -(*i);  // delay the output
                        }
                    }
                    if (ori)
                        value = (value+1)%size;
                    else
                        value = (value+size-1)%size;    
                }
            }
            seq.pop();                
        }
        // delayed print of the permutation
        if (f_printLevel > 2)
        {
            out << CONSOLE_COLOR_WHITE;
            out << "                  ";
            for (int i = 0; i < m_permSize; i++)
                out << gIntArray2[i] << " ";
            out << endl;
        }
        delete[] blocks;
    }
    if (!(f_printLevel & 1))
        out << "overall weight: " << m_distance << endl;
    out << CONSOLE_RESET;
}
 

/***************************************************************************
 * private methods
 ***************************************************************************/
 
void WeightedBB::expand(Candidate* f_cand, bool circular)
{
/*    cout << "expand";
    for (int i = 0; i < f_cand->m_perm->m_size; ++i)
        cout << " " << f_cand->m_perm->m_perm[i];
    if (f_cand->m_op)
    {        
        if (f_cand->m_op->m_type == Operation::TRANSPOSITION)
            cout << "tp(" << f_cand->m_op->m_edges[0] << ", " << f_cand->m_op->m_edges[1] << ", "<< f_cand->m_op->m_edges[2] << ")\n";
        else if (f_cand->m_op->m_type == Operation::TRANSREVA)
            cout << "tra(" << f_cand->m_op->m_edges[0] << ", " << f_cand->m_op->m_edges[1] << ", "<< f_cand->m_op->m_edges[2] << ")\n";
        else if (f_cand->m_op->m_type == Operation::TRANSREVB)
            cout << "trb(" << f_cand->m_op->m_edges[0] << ", " << f_cand->m_op->m_edges[1] << ", "<< f_cand->m_op->m_edges[2] << ")\n";
        else if (f_cand->m_op->m_type == Operation::REVREV)
            cout << "rr(" << f_cand->m_op->m_edges[0] << ", " << f_cand->m_op->m_edges[1] << ", "<< f_cand->m_op->m_edges[2] << ")\n";
    }*/
    Permutation* perm = f_cand->m_perm;    // permutation to expand
    Operation op;                          // operation to perform
    int weight = f_cand->m_lbWhole - f_cand->m_lbRem;   // so far performed weight

    if (m_distMode == 1)
    {
        // for all reversals
        op.m_type = Operation::REVERSAL;
        for (int i = 0; i < perm->m_size; i++)
        {
            op.m_edges[0] = i;
            for (int j = i+1; j < perm->m_size; j++)
            {
                op.m_edges[1] = j;
                applyOperation(f_cand, &op, weight);
            }
        }
    }
    // for all (inverted) transpositions
    for (int i = 0; i < perm->m_size - 2; i++)
    {
        op.m_edges[0] = i;
        for (int j = i+1; j < perm->m_size - 1; j++)
        {
            op.m_edges[1] = j;
            for (int k = j+1; k < perm->m_size; k++)
            {
                op.m_edges[2] = k;
                op.m_type = Operation::TRANSPOSITION;
                applyOperation(f_cand, &op, weight);
                if (m_distMode == 1)
                {
                    op.m_type = Operation::TRANSREVA;
                    applyOperation(f_cand, &op, weight);
                    op.m_type = Operation::TRANSREVB;
                    applyOperation(f_cand, &op, weight);
                    if( circular ){
                    	op.m_type = Operation::REVREV;
                    	applyOperation(f_cand, &op, weight);
                    }
                }
            }
        }
    }
}


void WeightedBB::applyOperation(Candidate* f_cand, const Operation* f_op, int f_weight)
{
    Permutation* succ = f_cand->m_perm->performOperation(*f_op);   // successor permutation
    int lbRem;                                             // lower bound of the remaining distance
    int lbWhole;                                           // lower bound for the whole sequence
    Candidate* cand;                                       // a candidate
    
    // calc bounds
    lbRem = succ->getLowerBound(m_wr, m_wt);
    lbWhole = f_weight + lbRem + (f_op->m_type == Operation::REVERSAL? m_wr : m_wt);
    if (lbWhole > m_upperBound)        // not good enough
    {
        delete succ;
        return;
    }
    // search if duplicated
    for (unsigned int i = 0; i < m_candidates[succ->m_hash].size(); i++)
    {
        if (*succ == *(m_candidates[succ->m_hash][i]->m_perm))
        {
            cand = m_candidates[succ->m_hash][i];   // use cand as duplicate of succ  
            // check which one to take
            if (lbWhole < cand->m_lbWhole)
            {
                // move to better position in m_queue
                m_queue[cand->m_lbWhole - m_lowerBound][cand->m_positionQ] 
                  = m_queue[cand->m_lbWhole - m_lowerBound][m_queue[cand->m_lbWhole - m_lowerBound].size() - 1];
                m_queue[cand->m_lbWhole - m_lowerBound][cand->m_positionQ]->m_positionQ = cand->m_positionQ;                  
                m_queue[cand->m_lbWhole - m_lowerBound].pop_back();
                cand->m_lbWhole = lbWhole;
                cand->m_positionQ = m_queue[lbWhole - m_lowerBound].size();
                cand->m_parent->m_numChildren--;
                if (cand->m_parent->m_numChildren == 0)
                    deleteAncestors(cand->m_parent);
                cand->m_parent = f_cand;
                cand->m_op->m_type = f_op->m_type;
                cand->m_op->m_edges[0] = f_op->m_edges[0];
                cand->m_op->m_edges[1] = f_op->m_edges[1];
                cand->m_op->m_edges[2] = f_op->m_edges[2];
                f_cand->m_numChildren++; 
                m_queue[lbWhole - m_lowerBound].push_back(cand);
                delete succ;
                return;
            }
            else          // duplicate is better
            {
                delete succ;
                return;
            }
        }
    }
    // new candidate, add 
    cand = new Candidate;
    cand->m_perm = succ;
    cand->m_lbWhole = lbWhole;
    cand->m_lbRem = lbRem;
    cand->m_positionC = m_candidates[succ->m_hash].size(); 
    cand->m_positionQ = m_queue[lbWhole - m_lowerBound].size();
    cand->m_numChildren = 0;
    cand->m_parent = f_cand;
    cand->m_op = new Operation;
    cand->m_op->m_type = f_op->m_type;
    cand->m_op->m_edges[0] = f_op->m_edges[0];
    cand->m_op->m_edges[1] = f_op->m_edges[1];
    cand->m_op->m_edges[2] = f_op->m_edges[2];
    f_cand->m_numChildren++;
    m_candidates[succ->m_hash].push_back(cand);
    m_queue[lbWhole - m_lowerBound].push_back(cand);
    m_heapSize++;
    if (m_heapSize > m_heapMax)
        m_heapMax = m_heapSize;
}
  

void WeightedBB::deleteAncestors(Candidate* f_cand)
{
    Candidate* cand = f_cand;
    Candidate* parent;
    
    do
    {
        parent = cand->m_parent;
        delete cand->m_perm; cand->m_perm = NULL;
        delete cand->m_op; cand->m_op = NULL;
        delete cand; cand = NULL; 
        m_heapSize--;
        cand = parent;
        if (cand == NULL)
            return;
    } while (cand->m_numChildren == 1);
    cand->m_numChildren--;
}


void WeightedBB::clear()
{
    if (m_queue != NULL)
    {
        for (int i = 0; i <= m_upperBound - m_lowerBound; i++)
        {
            for (unsigned int j = 0; j < m_queue[i].size(); j++)
                deleteAncestors(m_queue[i][j]);
            m_queue[i].clear();
        }  
        delete[] m_queue; m_queue = NULL;
    }  
    for (int i = 0; i < HASHSIZE; i++)
        m_candidates[i].clear();
    delete[] m_source; m_source = NULL;
    delete[] m_target; m_target = NULL;
    delete[] m_mapping; m_mapping = NULL;
    delete[] gIntArray0; gIntArray0 = NULL;
    delete[] gIntArray1; gIntArray1 = NULL;
    delete[] gIntArray2; gIntArray2 = NULL;
    delete[] gIntArray3; gIntArray3 = NULL;
    delete[] gIntArray4; gIntArray4 = NULL;
    delete[] gIntArray5; gIntArray5 = NULL;
    delete[] gIntArray6; gIntArray6 = NULL;
    delete[] gBoolArray0; gBoolArray0 = NULL;
    m_lowerBound = 0;
    m_upperBound = -1;
    m_distance = -1;
}


/***************************************************************************
 * debugging methods
 ***************************************************************************/

#if DEBUG_WEIGHTEDBB

void WeightedBB::debug()
{
}
  
#endif

} // end namespace weightedbb
