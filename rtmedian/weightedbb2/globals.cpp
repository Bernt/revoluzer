/**
 * filename: globals.cpp
 * author: Martin Bader
 * begin: 11.08.2008
 * last change: 12.08.2008
 *
 * Implementation of globals.h
 * 
 * Copyright (C) 2008  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "globals.h"


namespace weightedbb
{
    
    int* gIntArray0 = NULL;
    int* gIntArray1 = NULL;
    int* gIntArray2 = NULL;
    int* gIntArray3 = NULL;
    int* gIntArray4 = NULL;
    int* gIntArray5 = NULL;
    int* gIntArray6 = NULL;
    bool* gBoolArray0 = NULL;
         
} // end namespace weightedbb
