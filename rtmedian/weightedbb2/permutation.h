/**
 * filename: permutation.h
 * author: Martin Bader
 * begin: 11.08.2008
 * last change: 19.11.2008
 *
 * The representation of a permutation.
 * 
 * Copyright (C) 2008  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <ostream>

#ifndef WBB_PERMUTATION_H_
#define WBB_PERMUTATION_H_


namespace weightedbb
{
    
/**
 * The different operation types.
 */
struct Operation
{
    /** Type of the operation */
    enum 
    {
        REVERSAL,       // A B A -> A -B A 
        TRANSPOSITION,  // A B C A -> A C B A
        TRANSREVA,      // A B C A -> A C -B A
        TRANSREVB,      // A B C A -> A -C B A
        REVREV,         // A B C A -> A -B -C A
        INVTRANS        // general case of inverted transposition (use for human readable output only)
    } m_type;

    /** The edges of the operation */
    int m_edges[3];
};

     
  
/**
 * This class represents a permutation, with all necessary additional 
 * information.
 */
class Permutation
{
    /***********************************************************************
     * member variables
     ***********************************************************************/
   
  public:  
    /** Size of the permutation */
    int m_size;
    
    /** Permutation data */
    int* m_perm;
    
    /** Inverse permutation - indices of elements */
    int* m_index;
    
    /** Hash value of the permutation */
    long m_hash;
    
    /** Number of odd cycles */
    int m_codd;
    
    /** Number of even cycles */
    int m_ceven;
     
    /***********************************************************************
     * methods
     ***********************************************************************/

  public:
    /**
     * Constructor.
     */
	Permutation();
    
    /**
     * This constructor also computes all memebers. The input data will be 
     * transformed such that it does not contain any breakpoints, and the first
     * element is +1.
     * @param f_size  size of the data array
     * @param f_data  the input data array
     */
     Permutation(int f_size, int* f_data);
    
    /**
     * The destructor.
     */
	~Permutation();
    
    /**
     * Performs an operation.
     * @param f_op  the operation to perform
     * @return  the resulting permutation
     */
    Permutation* performOperation(const Operation& f_op) const;
    
    /**
     * Calculates the lower bound.
     * @param f_wr  weight of reversals
     * @param f_wt  weight of (inverted) transpositions and revrevs
     * @return  the lower bound
     */
    int getLowerBound(int f_wr, int f_wt);
    
    /**
     * Checks if two permutations are equal (that means the elements are equal).
     * @return  true if they are equal
     */
    bool operator==(Permutation& f_perm) const;
    
    /**
     * Direct access to an element.
     * @param f_pos  position of the element
     * @return  the element as reference
     */
    inline int& operator[](int f_pos)
    {
        return m_perm[f_pos];
    }
    
  private:
    /** 
     * Calcualtes the hash value of the permutation and writes it to m_hash. 
     */    
    void calcHash();
    
    /**
     * Calculates the number of even and odd cycles and writes them to ceven, codd.
     */
    void calcCycles();
    
    /***********************************************************************
     * debugging methods
     ***********************************************************************/
     
  public:
    /**
     * Dumps the permutation to stdout
     */
    void dump();       
};


/**
 * Prints a permutation (int array)
 * @param f_size  size of the array
 * @param f_perm  data array
 */
void printPermutation(int f_size, int* f_perm);

/** 
 * Prints an operation.
 * @param f_op  the oepration to print
 */
void printOperation(Operation* f_op, std::ostream &out);

} // end namespace weightedbb
 
#endif 
