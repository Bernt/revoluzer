/**
 * filename: cemetery.cpp
 * author: Martin Bader
 * begin: 19.11.2008
 * last change: 19.11.2008
 *
 * Function cemetery - Functions that are no longer needed.
 * 
 * Copyright (C) 2008  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
/**
 * Calculates the cycles, and also the hurdles. The hurdle definition is a little bit
 * different from SBR, because we also have to take care if cycles are even or odd. In 
 * fact, hurdles are really rare in SBwRT, thus considering hurdles didn't bring any speed improvement.
 */
void Permutation::calcCycles2()
{
    int length;          // length of the current cycle
    bool left;           // true if we are at the left point of an element
    int pos;             // current position
    int element;         // current element
    int stackTop = -1;   // top position of the stacks
    int evenid;          // id of even cycles
    int currend;       // end of current extent
    int ori;             // orientation of current component (0 unoriented, > 0 oriented)
    bool topHurdle;      // true if there is a hurdle at the top of the tree
    int* cycleid = gIntArray0;    // cycleID / visited array
    int* cycleType = gIntArray1;  // cycleID -> type (0 unoriented, 1 oriented, 2 even cycle)
    int* rightmost = gIntArray2;  // cycleID -> rightmost point
    int* extentbegin = gIntArray3;// extend begin stack
    int* extentend = gIntArray4;  // extend end stack
    int* extentori = gIntArray5;  // extent orientation stack (0 unoriented, > 0 oriented)
    int* encHurdles = gIntArray6; // enclosed hurdles stack
    bool* strip = gBoolArray0;    // stack to determine if we have are in a strip of comps as child    
    
    m_codd = 0;
    m_ceven = 0;
    for (int i = 0; i < m_size; i++)   // use as visited array
        cycleid[i] = 0;
    for (int i = 0; i < m_size; i++)
    {
        if (cycleid[i] != 0)         // already visited
            continue;
        cycleType[i+1] = 0;
        rightmost[i+1] = i;
        length = 0;
        left = true;
        pos = i;
        while (cycleid[pos] == 0)
        {
            if (rightmost[i+1] > pos)     // cycle t-oriented
                cycleType[i+1] = 1;
            else 
                rightmost[i+1] = pos;                  
            length++;
            cycleid[pos] = i+1;
            // current element - negativ if we are at its lower point
            if (left)
                element = -m_perm[pos];
            else
            {
                element = (pos == 0? m_perm[m_size - 1] : m_perm[pos-1]);
                cycleType[i+1] = 1;    // cycle r-oriented
            }    
            // target element - negativ if we want to its lower point    
            element = -element - 1;
            if (element == 0)
                element = m_size;
            else if (element == -(m_size + 1))
                element = -1;
            pos = abs(m_index[abs(element)]);
            if ((element > 0) ^ (m_perm[pos] > 0))
                left = false;
            else
            {
                (pos == m_size - 1? pos = 0 : pos++);
                left = true;
            }
        }
        if (length == 1)     // adjacencies are not unoriented
            cycleType[i+1] = 1;
        if (length & 1)
            m_codd++;
        else
        {
            m_ceven++;
            cycleType[i+1] = 2;
        }
    }
    // count hurdles:
    // merge even cycles 
    evenid = m_size + 1;   
    for (int i = 0; i < m_size; i++)
    { 
        if (cycleType[cycleid[i]] != 2)     // not an even cycle
            continue;
        if (cycleid[i] < evenid)             // first even cycle
            evenid = cycleid[i];
        else
        { 
            if (rightmost[cycleid[i]] > rightmost[evenid])   // new rightmost point
                rightmost[evenid] = rightmost[cycleid[i]];
            cycleid[i] = evenid;
        }
    }       
/*    cout << "cycle ids: ";
    for (int i = 0; i < m_size; i++)
        cout << cycleid[i] << " ";
    cout << endl;
    cout << "orientation: ";
    for (int i = 0; i < m_size; i++)
        cout << cycleType[i] << " ";
    cout << endl;
  */  
    
    // find components (adaption of BMY2001)
    pos = m_size;
    topHurdle = false;
    for (int i = 0; i < m_size; i++)
    {
        if (cycleid[i] == i+1)       // cycle begin -> push
        {
            stackTop++;
            extentbegin[stackTop] = i;
            extentend[stackTop] = rightmost[i+1];
            extentori[stackTop] = cycleType[i+1];
            encHurdles[stackTop] = 0;
            strip[stackTop] = false;
        }
        currend = rightmost[cycleid[i]];
        ori = cycleType[cycleid[i]];
        while (extentbegin[stackTop] >= cycleid[i])   // and thus > extentbegin = cycleid[i] - 1
        {
//            cout << "merge\n";
            if (extentend[stackTop] > currend)
                currend = extentend[stackTop];
            ori |= extentori[stackTop];    
            stackTop--;
            // set parent - do i really need that?
        }
        if (currend > extentend[stackTop])
            extentend[stackTop] = currend;
        extentori[stackTop] |= ori;
        if (i == extentend[stackTop])    // component end 
        {
//            cout << "comp: " << extentbegin[stackTop] << "  " << i << endl;
//            cout << "ori: " << extentori[stackTop] << ", enc " << encHurdles[stackTop] << endl;
            if (extentori[stackTop] == 0)    // component unoriented
            {
                if (encHurdles[stackTop] == 0)     // this is a hurdle
                    m_numHurdles++;
                else
                {
                    if (extentbegin[stackTop] < pos)        // includes all so-far found unoriented components
                    {
                        pos = extentbegin[stackTop];
                        topHurdle = (encHurdles[stackTop] == 1);
                    }
                    else
                        topHurdle = false;
                }
                if ((stackTop > 0) && !strip[stackTop-1])
                {
                    encHurdles[stackTop-1]++;
                    strip[stackTop-1] = true;
                }
            }
            else if ((stackTop > 0) && (encHurdles[stackTop] > 0) && !strip[stackTop-1])
            {
                encHurdles[stackTop-1]++;
                strip[stackTop-1] = true;
            }
            stackTop--;
        }
        else            // open new strip (allow to add hurdles)
            strip[stackTop] = false;
    }
    if (topHurdle)
        m_numHurdles++;
}
