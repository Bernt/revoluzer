/**
 * filename: permutation.h
 * author: Martin Bader
 * begin: 22.07.2005
 * last change: 11.08.2008
 *
 * The representation of a permutation.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef PERMUTATION_H_
#define PERMUTATION_H_

#include "config.h"

namespace minswrt
{
    
/**
 * This struct represents a cycle. As we only work on simple permutations,
 * we can only represent short cycles (max length = 3). The position of the 
 * reality-edges must be a cyclic increasing sequence. If the cycle is a 
 * 1-twisted 3-cycle, the twisted edge is the last edge. If the cycle is
 * a 2-twisted 3-cycle, the untwisted edge is the last edge.
 */
struct Cycle
{
    /** Type of the cycle. */
    enum
    {
        CYCLE_INVALID,           // invalid cycle
        CYCLE_ADJACENCY,         // adjacency
        CYCLE_2UNORIENTED,       // r-unoriented 2-cycle
        CYCLE_2ORIENTED,         // r-oriented 2-cycle
        CYCLE_0TWISTED,          // nontwisted 3-cycle 
        CYCLE_1TWISTED,          // 1-twisted 3-cycle 
        CYCLE_2TWISTED,          // 2-twisted 3-cycle 
        CYCLE_3TWISTED,          // 3-twisted 3-cycle 
        CYCLE_LONGCYCLE          // cycle length > 3 (->WARNING! edge data corrupted)
    } m_type;
    
    /** The positions of the reality-edges; this is always a cyclic 
     * increasing sequence. If the cycle is a 1-twisted 3-cycle, the twisted 
     * edge is the last edge. If the cycle is a 2-twisted 3-cycle, the 
     * untwisted edge is the last edge. */
    int m_edges[3];
}; 
 

 
/**
 * This struct represents an operation on the permutation.
 */
struct Operation
{
    /** type of the operation */
    enum 
    {
        OPERATION_REVERSAL,
        OPERATION_TRANSPOSITION,
        OPERATION_TRANSREVERSAL,
        OPERATION_INVALID
    } m_type;
    
    /** involved reality-edges; for reversals and transreversals, the segment 
     * between m_edges[0] and m_edges[1] will be inverted. f_edges must be a 
     * cyclic increasing sequence. */
    int m_edges[3];
};



/**
 * A sequence of operations, including delta sigma, w, delta sigma / w 
 */
struct Sequence
{
    /** the number of operations */
    int m_numOperations;
    
    /** the Operations (maximum 4) */
    Operation m_operations[4];
    
    /** delta sigma of the sequence */
    float m_dsigma;
    
    /** weight of the sequence */
    float m_weight;
    
    /** ratio delta sigma / weight */
    float m_ratio;        
};

 
 
/**
 * This class represents a permutation, with all necessary additional 
 * information.
 */
class Permutation
{
    /***********************************************************************
     * member variables
     ***********************************************************************/
   
  public:  
    /** The data elements. Elements go from 1 to m_size, plus additional 
     * sign. */
    int* m_data;
    
    /** Index of the elements. m_index[i] returns the index in the data array
     * of the unsigned element i. m_index[0] = 0 by definition. */
    int* m_index; 
           
  protected:  
    /** Size of the permutation. */
    int m_size;

  private:
    /** temporary data array for performing operation; avoids frequent 
     * memory allocation */
    int* m_tempData; 
    
     
    /***********************************************************************
     * methods
     ***********************************************************************/

  public:
    /**
     * The constructor. After calling the constructor, don't forget to call
     * init(int f_size).
     */
	Permutation();
    
    /**
     * The destructor.
     */
	~Permutation();
    
    /**
     * Initialises the permutation. f_size is the permutation size. Deletes 
     * any old data, doesn't set new data.
     * @param f_size  the size of the permutation
     */
    void init(int f_size);

    /**
     * Initialises the permutation. f_size is the permutation size. Deletes 
     * any old data, and sets the new data. Size of the new data array must 
     * be f_size.
     * @param f_size  the size of the permutation
     * @param f_data  the elements
     */
    void init(int f_size, const int* f_data);
    
    /**
     * Sets the data of the permutation, and actualises the index array. 
     * f_data must have the size of the permutation.
     */
    void setData(const int* f_data);
     
    /**
     * Returns the permutation size.
     * @return  the permutation size
     */
    inline int size() const
    {
        return m_size;
    }  
  
    /**
     * Searches for a cycle in the permutation that is not an adjacency. The 
     * cycle will be written to f_result. If there are only adjacencies, the 
     * result will have the type CYCLE_INVALID. Use this only for simple 
     * permutations, because the cycle data for long cycles will be 
     * corrupted. 
     * Additional, it is possible to search for cycles that intersect a given
     * interval. For this, f_intersect must be set. If it is set to 1, the 
     * algorithm searches for a cycle intersecting with the interval 
     * (f_a1, f_a2). If it is set to 2, the algorithm searches for a cycle 
     * intersecting with the union (f_a1, f_a2) || (f_b1, f_b2). The intervals
     * must be non-intersecting.
     * Intersecting cycles must have one edge inside the interval, and the other
     * outside the interval, excluding the interval bounds.
     * @param f_result  the result will be written here
     * @param f_intersect  choose intersection level
     * @param f_a1  start of first interval
     * @param f_a2  end of first interval
     * @param f_b1  start of second interval
     * @param f_b2  end of second interval
     */
    void getCycle(Cycle* f_result, int f_intersect = 0, int f_a1 = -1, 
      int f_a2 = -1, int f_b1 = -1, int f_b2 = -1) const;
    
    /**
     * Searches for a cycle with a given reality-edge. The result will be 
     * written to f_cycle, which must be initialized.
     * @param f_edge  index of the reality-edge   
     * @param f_cycle  the resulting cycle
     */
    void getCycleOfEdge(int f_edge, Cycle* f_result);
     
    /**
     * Performs an operation on the permutation.
     * @param f_operation  the operation to perform
     */
    void performOperation(const Operation& f_operation);
    
    /**
     * Prints the permutation on the console.
     */
    void print() const;    


    /***********************************************************************
     * debugging methods
     ***********************************************************************/
     
    /**
     * Prints all info about the permutation to stderr.
     */
    void debugPrint();
    
    /**
     * Checks if the permutation contains a long cycle.
     * @return  true if it has a long cycle
     */
    bool hasLongCycle(); 
    
    /**
     * Checks if the permutation is sorted.
     * @return  true if the permutation is sorted
     */
    bool isSorted();
          
};



/***************************************************************************
 * debugging non-class functions
 ***************************************************************************/

/**
 * Prints a cycle to stderr.
 * @param f_cycle  the cycle to print
 */
void printCycle(const Cycle& f_cycle);


} // end namespace minswrt

#endif 
