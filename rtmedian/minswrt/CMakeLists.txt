ADD_LIBRARY ( minswrt 
	config.cpp
	minswrt.cpp
	permutation.cpp
	tools.cpp )

ADD_EXECUTABLE ( minswrt_main start.cpp )

target_link_libraries( minswrt_main minswrt )
SET_TARGET_PROPERTIES (minswrt_main PROPERTIES OUTPUT_NAME minswrt)
