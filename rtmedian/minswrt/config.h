/**
 * filename: config.h
 * author: Martin Bader
 * begin: 22.07.2005
 * last change: 03.09.2008
 *
 * Some configurations for MinSWRT (namespaces, ...).
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef CONFIG_H_
#define CONFIG_H_

#include <iostream>

using namespace std;

#define DEBUG 0

#endif
