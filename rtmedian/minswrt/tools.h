/**
 * filename: tools.h
 * author: Martin Bader
 * begin: 25.07.2005
 * last change: 11.08.2008
 *
 * Useful functions for MinSWRT.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef TOOLS_H_
#define TOOLS_H_

#include "config.h"

#include "permutation.h"

namespace minswrt
{
    
// define console commandos
#define CONSOLE_RESET "\033[0m"
#define CONSOLE_BOLD  "\033[1m"
#define CONSOLE_HALFBRIGHT "\033[2m"
#define CONSOLE_UNDERSCORE "\033[4m"
#define CONSOLE_BLINK "\033[5m"
#define CONSOLE_REVERSE "\033[7m"
#define CONSOLE_NORMALBRIGHT "\033[22m"
#define CONSOLE_UNDERSCROE_OFF "\033[24m"
#define CONSOLE_BLINK_OFF "\033[25m"
#define CONSOLE_REVERSE_OFF "\033[27m"
#define CONSOLE_COLOR_BLACK "\033[30m"
#define CONSOLE_COLOR_RED "\033[31m"
#define CONSOLE_COLOR_GREEN "\033[32m"
#define CONSOLE_COLOR_BROWN "\033[33m"
#define CONSOLE_COLOR_BLUE "\033[34m"
#define CONSOLE_COLOR_MAGENTA "\033[35m"
#define CONSOLE_COLOR_CYAN "\033[36m"
#define CONSOLE_COLOR_WHITE "\033[37m"
#define CONSOLE_UNDERSCORE_CDEFAULT "\033[38m"
#define CONSOLE_UNDERSCORE_OFF_CDEFAULT "\033[39m"
#define CONSOLE_BACKGROUND_BLACK "\033[40m"
#define CONSOLE_BACKGROUND_RED "\033[41m"
#define CONSOLE_BACKGROUND_GREEN "\033[42m"
#define CONSOLE_BACKGROUND_BROWN "\033[43m"
#define CONSOLE_BACKGROUND_BLUE "\033[44m"
#define CONSOLE_BACKGROUND_MAGENTA "\033[45m"
#define CONSOLE_BACKGROUND_CYAN "\033[46m"
#define CONSOLE_BACKGROUND_WHITE "\033[47m"
#define CONSOLE_BACKGROUND_DEFAULT "\033[49m"


/**
 * Checks if an array of size 3 is cyclic sorted. If two elements are equal,
 * it will return true. If all elements are equal, it will return false.
 * @param f_array  the array
 * @return  true if the array is cyclic sorted
 */
inline bool cyclicSorted(int f_array[3])
{
    return (f_array[0] > f_array[1]) ^ (f_array[1] > f_array[2]) 
      ^ (f_array[2] > f_array[0]);
    
}


/**
 * Checks if two 3-cycles are interleaving.
 * @param f_c  first cycle
 * @param f_d  second cycle
 */
bool interleaving(const Cycle& f_c, const Cycle& f_d);


} // end namespace minswrt

#endif
