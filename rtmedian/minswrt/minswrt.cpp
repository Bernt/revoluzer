/**
 * filename: minswrt.cpp
 * author: Martin Bader
 * begin: 22.07.2005
 * last change: 15.08.2008
 *
 * The main class of Sorting by Weighted Reversals and Transpositions.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "minswrt.h"

#include <list>
#include <stdlib.h>
#include "permutation.h"
#include "tools.h"


namespace minswrt
{
using namespace std;
    
/***************************************************************************
 * public methods
 ***************************************************************************/
 
MinSWRT::MinSWRT()
{
    m_greedy = false;
    m_lookaheadDepth = 2;
    m_lookaheadBranch[0] = 5;
    m_lookaheadBranch[1] = 5;
    m_wr = 2.0f / 3.0f;
    m_wt = 1.0f;
    m_origin = NULL;
    m_target = NULL;
    m_perm = new Permutation(); 
    m_simplePerm = NULL;   
    m_simplePosition = NULL;
    m_dirtyMapping = true;
    m_sequence = new Sequence();
    m_sequence->m_numOperations = 0;
    m_shift = 0;
    m_tempData = NULL;
}


MinSWRT::~MinSWRT()
{
    delete m_perm; m_perm = NULL;             
    delete m_simplePerm; m_simplePerm = NULL;
    delete[] m_simplePosition; m_simplePosition = NULL;
    delete m_sequence; m_sequence = NULL;
    delete[] m_tempData; m_tempData = NULL;
}


void MinSWRT::setLookaheadParams(int f_depth, const int* f_branch)
{
    m_greedy = 2;
    m_lookaheadDepth = f_depth;    
    for (int i = 0; i < m_lookaheadDepth; i++)
        m_lookaheadBranch[i] = f_branch[i];
}


void MinSWRT::setWeights(float f_wr, float f_wt)
{
    m_wr = f_wr;
    m_wt = f_wt;   
}


void MinSWRT::setPermutationSize(int f_size)
{
    m_size = f_size;
    delete[] m_tempData;
    m_tempData = new int[f_size];
}
    

void MinSWRT::setOrigin(const int* f_data)
{
    m_origin = f_data;
    m_dirtyMapping = true;
}
     

void MinSWRT::setTarget(const int* f_data)
{
    m_target = f_data;
    m_dirtyMapping = true;
}
         

void MinSWRT::setPermutations(int f_size, const int* f_origin, const int* f_target)
{
    m_size = f_size;
    m_origin = f_origin;
    m_target = f_target;    
    m_dirtyMapping = true;    
    delete[] m_tempData;
    m_tempData = new int[f_size];
}

              
float MinSWRT::getLowerBound()
{
    bool* visited = new bool[m_size];  // visited reality-edges
    int cyclelength;                   // length of the current cycle
    int pos;                           // position of current reality-edge
    bool left;                   // true if we leave the RE at the left point
    int element;                       // the next element (unsigned)
    int ceven = 0;                     // number of even cycles
    int codd = 0;                      // number of odd cycles
    
    if (m_dirtyMapping)
    {
        mapPermutations();
        delete[] m_simplePosition; 
        m_simplePosition = new int[m_size];
    }
    for (int i = 0; i < m_size; i++)
        visited[i] = false;
    for (int i = 0; i < m_size; i++)
    {
        if (!visited[i])               // new cycle
        {
            cyclelength = 0;
            pos = i;
            left = true;
            do
            {
                visited[pos] = true;
                cyclelength++;
                if (left)
                    element = m_perm->m_data[pos];
                else 
                    element = m_perm->m_data[(pos+1) % m_size];
                if (left ^ (element > 0))   // go to element - 1, upper point
                {
                    left = false;           // left now indicates point of target
                    element = abs(element) - 1;
                    if (element == 0)
                        element = m_size;
                }
                else                        // go to element + 1, lower point
                {
                    left = true;            // left now indicates point of target
                    element = abs(element) + 1;        
                    if (element > m_size)
                        element = 1;
                }
                pos = m_perm->m_index[element];
                if  (!(left ^ (m_perm->m_data[pos] > 0)))            
                {    
                    pos = (pos + m_size - 1) % m_size;
                    left = true;
                }
                else
                    left = false;
            } while (!visited[pos]); 
            if (cyclelength & 1)
                codd++;
            else
                ceven++;    
        }
    }
    delete[] visited;
    return ceven * m_wr + ((m_size - codd) / 2 - ceven) * m_wt;
}
    
    
void MinSWRT::sort()
{
    if (m_dirtyMapping)
    {
        mapPermutations();
        delete[] m_simplePosition; 
        m_simplePosition = new int[m_size];
    }    
    generateSimplePermutation();
    m_sorting.erase(m_sorting.begin(), m_sorting.end());
    m_shift = 0;
    if (m_greedy == 0)
        nodeRoot();
    else if (m_greedy == 1)    
        nodeRootGreedy();
    else if (m_greedy == 2)
    {
        if (m_lookaheadDepth == 0)    // only do pseudo lookahead
            nodeRootGreedy();
        else       
            nodeRootLookahead();
    }
    else
        cerr << "minSWRT::sort: invalid parameter for greedy strategy\n";
}    


void MinSWRT::initStartingSequences()
{
    if (m_dirtyMapping)
    {
        mapPermutations();
        delete[] m_simplePosition; 
        m_simplePosition = new int[m_size];
    }    
    generateSimplePermutation();
}


int MinSWRT::getStartingSequences(Sequence* f_result)
{
    Cycle cycle;
    int counter = 0;
    
    for (int i = 0; i < m_simplePerm->size(); i++)    // for all possible cycles
    {
        m_simplePerm->getCycleOfEdge(i, &cycle);
        // choose node
        switch (cycle.m_type)
        {
        case Cycle::CYCLE_ADJACENCY:
            continue;    
        case Cycle::CYCLE_2ORIENTED: 
            node000(cycle);
            break;
        case Cycle::CYCLE_2UNORIENTED:
            node003(cycle);
            break;
        case Cycle::CYCLE_0TWISTED: 
            node004(cycle);
            break;
        case Cycle::CYCLE_1TWISTED: 
            node005(cycle);
            break;
        case Cycle::CYCLE_2TWISTED: 
            node002(cycle);
            break;
        case Cycle::CYCLE_3TWISTED: 
            node001(cycle);
            break;
        default: 
            cerr << "error in MinSWRT::nodeRootGreedy: Algo found unexpected cycle type\n";
            printCycle(cycle);            
            return counter;
        }
        // copy to result vector
        f_result[counter].m_numOperations = m_sequence->m_numOperations;
        for (int j = 0; j < m_sequence->m_numOperations; j++)
            f_result[counter].m_operations[j] = m_sequence->m_operations[j];                    
        f_result[counter].m_ratio = m_sequence->m_ratio;
        f_result[counter].m_dsigma = m_sequence->m_dsigma;
        f_result[counter].m_weight = m_sequence->m_weight;
        counter++;
    }    
    return counter;    
}
 
 
void MinSWRT::performStartingSequence(Sequence* f_seq)
{
    int order1[3];       // needed to check is something is cyclic sorted
    int order2[3];       // needed to check is something is cyclic sorted
    int delta1, delta2;  // distance to move
    int size = m_simplePerm->size();   // permutation size
    
    for (int i = 0; i < f_seq->m_numOperations; i++)
    {
        m_simplePerm->performOperation(f_seq->m_operations[i]);
        // update operations
        if ((f_seq->m_operations[i].m_type == Operation::OPERATION_REVERSAL)
          || (f_seq->m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = f_seq->m_operations[i].m_edges[0];
            order1[1] = f_seq->m_operations[i].m_edges[1];
            for (int j = i+1; j < f_seq->m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    order1[2] = f_seq->m_operations[j].m_edges[k];
                    if (!cyclicSorted(order1))    // move the edge
                        f_seq->m_operations[j].m_edges[k] = 
                          (f_seq->m_operations[i].m_edges[0] 
                          + f_seq->m_operations[i].m_edges[1] 
                          - f_seq->m_operations[j].m_edges[k] + size) % size;
                }              
            }
        }        
        if ((f_seq->m_operations[i].m_type == Operation::OPERATION_TRANSPOSITION)
          || (f_seq->m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = f_seq->m_operations[i].m_edges[1];
            order1[1] = f_seq->m_operations[i].m_edges[2];
            order2[0] = f_seq->m_operations[i].m_edges[2];
            order2[1] = f_seq->m_operations[i].m_edges[0];
            delta1 = f_seq->m_operations[i].m_edges[0] - f_seq->m_operations[i].m_edges[2]; 
            delta2 = f_seq->m_operations[i].m_edges[2] - f_seq->m_operations[i].m_edges[1]; 
            for (int j = i+1; j < f_seq->m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    if (f_seq->m_operations[j].m_edges[k] == f_seq->m_operations[i].m_edges[2])
                        f_seq->m_operations[j].m_edges[k] = (f_seq->m_operations[j].m_edges[k] 
                          + delta1 - delta2 + size) % size;
                    else
                    {
                        order1[2] = f_seq->m_operations[j].m_edges[k];
                        // move first segment to the back
                        if (!cyclicSorted(order1))    // move the edge
                            f_seq->m_operations[j].m_edges[k] = 
                              (f_seq->m_operations[j].m_edges[k] + delta1 + size) 
                              % size;
                        else 
                        {
                            // move second segment to the front      
                            order2[2] = f_seq->m_operations[j].m_edges[k];
                            if (!cyclicSorted(order2))  // move the edge
                            {
                                f_seq->m_operations[j].m_edges[k] = 
                                  (f_seq->m_operations[j].m_edges[k] - delta2 + size) 
                                  % size;
                            }
                        }
                    }      
                }              
            }
        }
    }    
}
 

void MinSWRT::printResult(int f_printLevel)
{
    float weight = 0.0f;      // overall weight
    int* perm;                // copy of the permutation    
    int blanks;               // blanks to print before an element
    int p[3];                 // the three splitpoints of the operation
    int range1;               // length of firt segment
    int range2;               // length of second segment
    
    cout << CONSOLE_BACKGROUND_BLACK << CONSOLE_COLOR_WHITE 
      << CONSOLE_BOLD << endl << endl;
    if ((f_printLevel == 1) || (f_printLevel == 2))
    {
        for (unsigned int i = 0; i < m_sorting.size(); i++)
        {
            switch (m_sorting[i].m_type)
            {
            case Operation::OPERATION_REVERSAL:
                cout << " r(";
                break;
            case Operation::OPERATION_TRANSPOSITION:
                cout << " t(";
                break;
            case Operation::OPERATION_TRANSREVERSAL:
                cout << "tr(";
                break;
            default: 
                cerr << "WARNING! MinSWRT::printResult found unexpected operation type!";
                break;    
            }
            cout << m_sorting[i].m_edges[0];
            cout << ", " << m_sorting[i].m_edges[1];
            if (m_sorting[i].m_type != Operation::OPERATION_REVERSAL)
                cout << ", " << m_sorting[i].m_edges[2];
            cout << ")\n";
        }    
    }
    else if ((f_printLevel == 3) || (f_printLevel == 4))
    {
        // copy permutation
        perm = new int[m_size];         
        for (int i = 0; i < m_size; i++)
            perm[i] = m_origin[i];
        // print operation    
        for (unsigned int i = 0; i < m_sorting.size(); i++)
        {
            p[0] = m_sorting[i].m_edges[0] - 1;
            p[1] = m_sorting[i].m_edges[1] - 1;
            p[2] = m_sorting[i].m_edges[2] - 1;
            switch (m_sorting[i].m_type)
            {
            case Operation::OPERATION_REVERSAL:
                cout << "     r(";
                break;
            case Operation::OPERATION_TRANSPOSITION:
                cout << " t(";
                break;
            case Operation::OPERATION_TRANSREVERSAL:
                cout << "tr(";
                break;
            default:
                cout << "WARNING! Unexpected operation in MinSWRT::printResult\n";
            } 
            cout << (m_sorting[i].m_edges[0] < 10?" ":"") << m_sorting[i].m_edges[0]     
              << (m_sorting[i].m_edges[1] < 10?",  ":", ") << m_sorting[i].m_edges[1];
            if (m_sorting[i].m_type != Operation::OPERATION_REVERSAL)
                cout << (m_sorting[i].m_edges[2] < 10?",  ":", ") << m_sorting[i].m_edges[2];  
            cout << ")(";
            // print current permutation
            if (p[0] > p[1])            // we are in the first segment
                cout << CONSOLE_COLOR_RED << CONSOLE_BOLD;
            else if ((p[1] > p[2]) && (m_sorting[i].m_type != Operation::OPERATION_REVERSAL))
                                       // we are in the second segment
                cout << CONSOLE_COLOR_GREEN << CONSOLE_BOLD;                
            for (int j = 0; j < m_size; j++)
            {
                if (j == p[0])         // begin of first segment
                    cout << CONSOLE_COLOR_RED << CONSOLE_BOLD;
                else if ((j == p[1]) && (m_sorting[i].m_type != Operation::OPERATION_REVERSAL))
                                       // begin of second segment
                    cout << CONSOLE_COLOR_GREEN << CONSOLE_BOLD;                
                else if (((j == p[1]) && (m_sorting[i].m_type == Operation::OPERATION_REVERSAL))
                  || ((j == p[2]) && (m_sorting[i].m_type != Operation::OPERATION_REVERSAL)))
                    cout << CONSOLE_COLOR_WHITE;// << CONSOLE_NORMALBRIGHT;  
                blanks = (j == 0? 0:1);
                if (perm[j] > 0)
                    blanks++;
                if (abs(perm[j]) < 10)
                    blanks++;
                for (int k = 0; k < blanks; k++)
                    cout << " ";
                cout << perm[j];
            }
            cout << CONSOLE_COLOR_WHITE << ")\n"; // << CONSOLE_NORMALBRIGHT << ")\n";
            // perform the operation
            if ((m_sorting[i].m_type == Operation::OPERATION_REVERSAL) 
              || (m_sorting[i].m_type == Operation::OPERATION_TRANSREVERSAL))
            {  
                for (int j = p[0]; j != p[1]; (j == m_size - 1? j = 0 : j++))
                    m_tempData[j] = perm[j];
                for (int j = p[0]; j != p[1]; (j == m_size - 1? j = 0 : j++))
                    perm[j] = -m_tempData[(p[0] + p[1] - 1 - j + m_size) % m_size];
            }
            if ((m_sorting[i].m_type == Operation::OPERATION_TRANSPOSITION) 
              || (m_sorting[i].m_type == Operation::OPERATION_TRANSREVERSAL))
            {  
                range1 = (p[1] - p[0] + m_size) % m_size;    
                range2 = (p[2] - p[1] + m_size) % m_size; 
                for (int j = 0; j < range1; j++)      // store first segment
                    m_tempData[j] = perm[(p[0] + j) % m_size];
                for (int j = 0; j < range2; j++)      // move second segment to front
                    perm[(p[0] + j) % m_size] = perm[(p[1] + j) % m_size];
                for (int j = 0; j < range1; j++)      // move first segment to back
                    perm[(p[0] + range2 + j) % m_size] = m_tempData[j];
            }
        }        
        // print current permutation
        cout << "              (";
        for (int j = 0; j < m_size; j++)
        {
            blanks = (j == 0? 0:1);
            if (perm[j] > 0)
                blanks++;
            if (abs(perm[j]) < 10)
                blanks++;
            for (int k = 0; k < blanks; k++)
                cout << " ";
            cout << perm[j];
        }
        cout << ")\n";
        delete[] perm;
    }    
    if (!(f_printLevel & 1))
    {
        for (unsigned int i = 0; i < m_sorting.size(); i++)
        {
            if (m_sorting[i].m_type == Operation::OPERATION_REVERSAL)
                weight += m_wr;
            else
                weight += m_wt;
        }
        cout << "weight: " << weight << endl;    
    }
    cout << CONSOLE_RESET << endl;
}

    
/**
 * Returns a pointer to the result vector.
 */
const vector<Operation>* MinSWRT::getResult()
{
    return &m_sorting;
}


/***************************************************************************
 * protected methods
 ***************************************************************************/

void MinSWRT::mapPermutations()
{
    int* index = new int[m_size+1];    // stores index of elements of origin

    for (int i = 0; i < m_size; i++)   // initialize index
        index[abs(m_origin[i])] = i;        
    m_perm->init(m_size);
    for (int i = 0; i < m_size; i++)   // map target to id
    {
        if ((m_target[i] > 0) == (m_origin[index[abs(m_target[i])]] > 0))
            m_perm->m_data[index[abs(m_target[i])]] = i+1;
        else    
            m_perm->m_data[index[abs(m_target[i])]] = -(i+1);
        m_perm->m_index[i+1] = index[abs(m_target[i])];    
    }    
    m_dirtyMapping = false;
    delete[] index;
}


void MinSWRT::generateSimplePermutation()
{
    int size = m_size;                 // size of the current permutation
    int simplesize = size;             // size of the simple permutation
    bool* visited = new bool[3 * m_size / 2];    // visited reality-edges 
                                       // size also ok for simple perm
    int cyclelength;                   // length of the current cycle
    int pos;                           // position of current reality-edge
    bool left;                   // true if we leave the RE at the left point
    int element;                       // the next element (unsigned)
    int splitelement;                  // the new inserted element
    int lastpos;                       // last visited reality-edge
    int lastleft;                      // last value for left
    
    for (int i = 0; i < size; i++)
    {
        visited[i] = false;
        m_simplePosition[i] = i;
    }
    for (int i = 0; i < size; i++)
    {
        if (!visited[i])               // new cycle
        {
            cyclelength = 0;
            pos = i;
            left = true;
            do
            {
                visited[pos] = true;
                cyclelength++;
                if (left)
                    element = m_perm->m_data[pos];
                else 
                    element = m_perm->m_data[(pos+1) % size];
                if (left ^ (element > 0))   // go to element - 1, upper point
                {
                    left = false;           // left now indicates point of target
                    element = abs(element) - 1;
                    if (element == 0)
                        element = size;
                }
                else                        // go to element + 1, lower point
                {
                    left = true;            // left now indicates point of target
                    element = abs(element) + 1;        
                    if (element > size)
                        element = 1;
                }
                pos = m_perm->m_index[element];
                if  (!(left ^ (m_perm->m_data[pos] > 0)))            
                {    
                    pos = (pos + size - 1) % size;
                    left = true;
                }
                else
                    left = false;
            } while (!visited[pos]); 
            if (cyclelength > 3)
                simplesize += (cyclelength / 2 - 1);
        }
    }
    
#if DEBUG    
    cerr << "simplesize: " << simplesize << endl;
#endif
    
    // create simple permutation
    delete m_simplePerm;
    m_simplePerm = new Permutation();
    m_simplePerm->init(simplesize);
    for (int i = 0; i < size; i++)     // copy data from original permutation
    {
        m_simplePerm->m_data[i] = m_perm->m_data[i];
        m_simplePerm->m_index[i+1] = m_perm->m_index[i+1];
    }
    if (size == simplesize)            // no splits to perform
    {
        delete[] visited;
        return;
    }
    // expand cycles
    for (int i = 0; i < simplesize; i++)
        visited[i] = false;
    for (int i = 0; i < simplesize; i++)   // search all reality-edges for long cycles
    {
        if (!visited[i])                   // new cycle
        {
            cyclelength = 0;
            pos = i;
            left = false;
            if (m_simplePerm->m_data[i] > 0)   // precalculate the splitelement
                splitelement = m_simplePerm->m_data[i] + 1; 
            else
                splitelement = m_simplePerm->m_data[i];                     
            do
            {
                visited[pos] = true;
                lastpos = pos;
                lastleft = left;
                cyclelength++;
                if (left)
                    element = m_simplePerm->m_data[pos];
                else 
                    element = m_simplePerm->m_data[(pos+1) % size];
                if (left ^ (element > 0))   // go to element - 1, upper point
                {
                    left = false;           // left now indicates point of target
                    element = abs(element) - 1;
                    if (element == 0)
                        element = size;
                }
                else                        // go to element + 1, lower point
                {
                    left = true;            // left now indicates point of target
                    element = abs(element) + 1;        
                    if (element > size)
                        element = 1;
                }
                pos = m_simplePerm->m_index[element];
                if  (!(left ^ (m_simplePerm->m_data[pos] > 0)))            
                {    
                    pos = (pos + size - 1) % size;
                    left = true;
                }
                else
                    left = false;
            } while (!visited[pos] && (cyclelength < 3)); 
            if ((cyclelength == 3) && !visited[pos])       // split this cycle
            {
                // increment all values >= splitelement, adjust m_index
                for (int j = size; j >= abs(splitelement); j--)
                {
                    if (m_simplePerm->m_data[m_simplePerm->m_index[j]] > 0)
                        m_simplePerm->m_data[m_simplePerm->m_index[j]]++;
                    else
                        m_simplePerm->m_data[m_simplePerm->m_index[j]]--;
                    m_simplePerm->m_index[j+1] = m_simplePerm->m_index[j];   
                }
                // move all to the right
                for (int j = size - 1; j > lastpos; j--)
                {
                    m_simplePerm->m_data[j+1] = m_simplePerm->m_data[j];
                    m_simplePerm->m_index[abs(m_simplePerm->m_data[j])]++;
                    visited[j+1] = visited[j];                    
                }                                
                // fill in splitelement
                if (lastleft)
                {
                    visited[lastpos] = false;
                    visited[lastpos + 1] = true;
                    splitelement *= -1;
                }
                else
                    visited[lastpos+1] = false;
                lastpos++;
                m_simplePerm->m_data[lastpos] = splitelement;
                m_simplePerm->m_index[abs(splitelement)] = lastpos;
                size++;
                for (int j = 0; j < m_size; j++)    // update m_simplePosition
                {
                    if (m_simplePosition[j] >= lastpos)
                        m_simplePosition[j]++;
                }
                if (size == simplesize)
                {
                    delete[] visited;
                    return;
                }
            }    
        }    // end if(!visited[i])
    }
    delete[] visited;    
}        


void MinSWRT::performSequence(Sequence* f_seq)
{
    int order1[3];       // needed to check is something is cyclic sorted
    int order2[3];       // needed to check is something is cyclic sorted
    int delta1, delta2;  // distance to move
    int size = m_simplePerm->size();   // permutation size
    
    for (int i = 0; i <f_seq-> m_numOperations; i++)
    {
        m_simplePerm->performOperation(f_seq->m_operations[i]);
        // update operations
        if ((f_seq->m_operations[i].m_type == Operation::OPERATION_REVERSAL)
          || (f_seq->m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = f_seq->m_operations[i].m_edges[0];
            order1[1] = f_seq->m_operations[i].m_edges[1];
            for (int j = i+1; j < f_seq->m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    order1[2] = f_seq->m_operations[j].m_edges[k];
                    if (!cyclicSorted(order1))    // move the edge
                        f_seq->m_operations[j].m_edges[k] = 
                          (f_seq->m_operations[i].m_edges[0] 
                          + f_seq->m_operations[i].m_edges[1] 
                          - f_seq->m_operations[j].m_edges[k] + size) % size;
                }              
            }
        }        
        if ((f_seq->m_operations[i].m_type == Operation::OPERATION_TRANSPOSITION)
          || (f_seq->m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = f_seq->m_operations[i].m_edges[1];
            order1[1] = f_seq->m_operations[i].m_edges[2];
            order2[0] = f_seq->m_operations[i].m_edges[2];
            order2[1] = f_seq->m_operations[i].m_edges[0];
            delta1 = f_seq->m_operations[i].m_edges[0] - f_seq->m_operations[i].m_edges[2]; 
            delta2 = f_seq->m_operations[i].m_edges[2] - f_seq->m_operations[i].m_edges[1]; 
            for (int j = i+1; j < f_seq->m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    if (f_seq->m_operations[j].m_edges[k] == f_seq->m_operations[i].m_edges[2])
                        f_seq->m_operations[j].m_edges[k] = (f_seq->m_operations[j].m_edges[k] 
                          + delta1 - delta2 + size) % size;
                    else
                    {
                        order1[2] = f_seq->m_operations[j].m_edges[k];
                        // move first segment to the back
                        if (!cyclicSorted(order1))    // move the edge
                            f_seq->m_operations[j].m_edges[k] = 
                              (f_seq->m_operations[j].m_edges[k] + delta1 + size) 
                              % size;
                        else 
                        {
                            // move second segment to the front      
                            order2[2] = f_seq->m_operations[j].m_edges[k];
                            if (!cyclicSorted(order2))  // move the edge
                            {
                                f_seq->m_operations[j].m_edges[k] = 
                                  (f_seq->m_operations[j].m_edges[k] - delta2 + size) 
                                  % size;
                            }
                        }
                    }      
                }              
            }
        }
        mimickOperation(f_seq->m_operations[i]);
    }    
}


void MinSWRT::mimickOperation(Operation&  f_operation)
{
    int p[3];            // the splitpoints in m_perm
    int range;           // length of a segment
    int range2;          // length of a segment
    int simplerange;     // length of a segment in m_simplePerm
    int position;        // position in m_simplePerm

#if 0    // !!DEBUG
    for (int i = 0; i < m_size; i++)
        cerr << m_simplePosition[i] << " ";
    cerr << endl;
        
    switch (f_operation.m_type)
    {
    case Operation::OPERATION_REVERSAL:
        cerr << " r ";
        break;
    case Operation::OPERATION_TRANSPOSITION:
        cerr << " t ";
        break;
    case Operation::OPERATION_TRANSREVERSAL:
        cerr << "tr ";
        break;
    default:
        break;
    }    
    cerr << f_operation.m_edges[0] << " " << f_operation.m_edges[1]
      << " " << f_operation.m_edges[2] << endl;
#endif
      
    p[0] = -1;
    p[1] = -1;
    p[2] = -1;
    // find splitpoints
    for (int j = 0; j < 3; j++)
    {
        for (int i = m_size - 1; i >= 0; i--)
        {
            if (m_simplePosition[i] > f_operation.m_edges[j])
                p[j] = i-1;
        }
        if (p[j] == -1)
            p[j] = m_size - 1;
    }    
    // update m_simplePos
    if ((f_operation.m_type == Operation::OPERATION_REVERSAL)
      || (f_operation.m_type == Operation::OPERATION_TRANSREVERSAL))
    {
        range = (p[1] - p[0] + m_size) % m_size;        
        for (int i = p[1]; i != p[0]; i == 0? i = m_size - 1 : i--)
            m_tempData[i] = m_simplePosition[i];                
        for (int i = 0; i < range; i++)
        {
            position = m_tempData[(p[1]-i+m_size) % m_size];    
            position = (f_operation.m_edges[0] + f_operation.m_edges[1] + 1 - position
              + m_simplePerm->size()) % m_simplePerm->size();            
            m_simplePosition[(p[0]+i+1) % m_size] 
              = position;
        }                
    }      
    if ((f_operation.m_type == Operation::OPERATION_TRANSPOSITION)
      || (f_operation.m_type == Operation::OPERATION_TRANSREVERSAL))
    {
        range = (p[2] - p[1] + m_size) % m_size;
        range2 = (p[0] - p[2] + m_size) % m_size;
        simplerange = (f_operation.m_edges[2] - f_operation.m_edges[1] 
          + m_simplePerm->size()) % m_simplePerm->size();
        for (int i = 0; i < range; i++)
            m_tempData[i] = m_simplePosition[(p[1] + i + 1) % m_size];
        for (int i = 1; i <= range2; i++)
        {
            position = (m_simplePosition[(p[2] + i) % m_size] - simplerange 
              + m_simplePerm->size()) % m_simplePerm->size();
            m_simplePosition[(p[2] + i - range + m_size) % m_size] = position;            
        }                        
        simplerange = (f_operation.m_edges[0] - f_operation.m_edges[2] 
          + m_simplePerm->size()) % m_simplePerm->size();
        for (int i = 0; i < range; i++)
            m_simplePosition[(p[1] + 1 + range2 + i) % m_size] 
              = (m_tempData[i] + simplerange) % m_simplePerm->size();
    }    
    // if the number of dummy elements in the tail segment changes,
    // m_simplePosition becomes shifted; remove this shift
    range = 0;                                // reuse range
    for (int i = 1; i < m_size; i++)
    {
        if (m_simplePosition[i] < m_simplePosition[i-1])
        {
            range = i;
            break;
        }
    }
    for (int i = 0; i < m_size; i++)
        m_tempData[i] = m_simplePosition[(i + range) % m_size];
    for (int i = 0; i < m_size; i++)
        m_simplePosition[i] = m_tempData[i];
    // write the operation
    if (p[0] == p[1])    
        f_operation.m_type = Operation::OPERATION_INVALID;
    if (p[2] == p[0])         // avoid shifting - shift only when segment1 does not exist
        p[2] = p[1];    
    else if ((f_operation.m_type == Operation::OPERATION_TRANSPOSITION)
      && ((p[0] == p[2]) || (p[1] == p[2])))
        f_operation.m_type = Operation::OPERATION_INVALID;
    else if ((f_operation.m_type == Operation::OPERATION_TRANSREVERSAL)
      && ((p[0] == p[2]) || (p[1] == p[2])))
        f_operation.m_type = Operation::OPERATION_REVERSAL; 
    if (f_operation.m_type == Operation::OPERATION_REVERSAL)   // avoid shifting
        p[2] = p[1];
    if (f_operation.m_type != Operation::OPERATION_INVALID)
    {          
        f_operation.m_edges[0] = (p[0] + 1 + m_shift) % m_size + 1;
        f_operation.m_edges[1] = (p[1] + 1 + m_shift) % m_size + 1;
        f_operation.m_edges[2] = (p[2] + 1 + m_shift) % m_size + 1;
        m_sorting.push_back(f_operation);
    }            
    // adjust the offset
    m_shift = (m_shift + p[2] - p[1] + range + m_size) % m_size;
}


void MinSWRT::nodeRoot()
{
    Cycle cycle;
    
    m_simplePerm->getCycle(&cycle);  
    while (cycle.m_type != Cycle::CYCLE_INVALID)
    {
        // choose node
        switch (cycle.m_type)
        {
        case Cycle::CYCLE_2ORIENTED: 
            node000(cycle);
            break;
        case Cycle::CYCLE_2UNORIENTED:
            node003(cycle);
            break;
        case Cycle::CYCLE_0TWISTED: 
            node004(cycle);
            break;
        case Cycle::CYCLE_1TWISTED: 
            node005(cycle);
            break;
        case Cycle::CYCLE_2TWISTED: 
            node002(cycle);
            break;
        case Cycle::CYCLE_3TWISTED: 
            node001(cycle);
            break;
        default: 
            cerr << "error in MinSWRT::nodeRoot: Algo found unexpected cycle type\n";
            printCycle(cycle);            
            return;
        }
        performSequence(m_sequence);          
        m_simplePerm->getCycle(&cycle);            
        
        // !!DEBUG: avoid infinite loop
        if (m_sequence->m_numOperations == 0)
        {
            cycle.m_type = Cycle::CYCLE_INVALID;
            cout << endl << "unexpected behaviour in MinSWRT::nodeRoot\n";
        }
        m_sequence->m_numOperations = 0;    
    }
}


void MinSWRT::nodeRootGreedy()
{
    Cycle cycle;
    Sequence gcSeq;         // golden cage sequence
    
    while (true)
    {
        gcSeq.m_ratio = 0.0f;
        gcSeq.m_numOperations = 0;
        for (int i = 0; i < m_simplePerm->size(); i++)    // for all possible cycles
        {
            m_simplePerm->getCycleOfEdge(i, &cycle);
            // choose node
            switch (cycle.m_type)
            {
            case Cycle::CYCLE_ADJACENCY:
                continue;    
            case Cycle::CYCLE_2ORIENTED: 
                node000(cycle);
                break;
            case Cycle::CYCLE_2UNORIENTED:
                node003(cycle);
                break;
            case Cycle::CYCLE_0TWISTED: 
                node004(cycle);
                break;
            case Cycle::CYCLE_1TWISTED: 
                node005(cycle);
                break;
            case Cycle::CYCLE_2TWISTED: 
                node002(cycle);
                break;
            case Cycle::CYCLE_3TWISTED: 
                node001(cycle);
                break;
            default: 
                cerr << "error in MinSWRT::nodeRootGreedy: Algo found unexpected cycle type\n";
                printCycle(cycle);            
                return;
            }
            if (m_sequence->m_ratio > gcSeq.m_ratio)     // copy to golden cage
            {
                gcSeq.m_numOperations = m_sequence->m_numOperations;
                for (int j = 0; j < m_sequence->m_numOperations; j++)
                    gcSeq.m_operations[j] = m_sequence->m_operations[j];                    
                gcSeq.m_ratio = m_sequence->m_ratio;
                // dsigma and weight are not of interest, so don't copy them
            }
        }    
        if (gcSeq.m_numOperations == 0)     // permutation sorted
            return;
        // perform golden cage sequence
        performSequence(&gcSeq);          
    }    
}


void MinSWRT::nodeRootLookahead()
{
    Cycle cycle;                    // cycle in the RDD
    Sequence* gcSeq = NULL;         // sequence in the golden cage    
    list<Sequence*> seqList;        // list with all actual sequences, sorted by ratio
    list<Sequence*>::iterator it;   // list iterator
    int rank;                       // current rank in the sequence list
    float ratio;                    // overall ratio of the sequence (including LAH)
    
    while (true)
    {
        for (int i = 0; i < m_simplePerm->size(); i++)    // for all possible cycles
        {
            m_simplePerm->getCycleOfEdge(i, &cycle);
            // choose node
            switch (cycle.m_type)
            {
            case Cycle::CYCLE_ADJACENCY:
                continue;    
            case Cycle::CYCLE_2ORIENTED: 
                node000(cycle);
                break;
            case Cycle::CYCLE_2UNORIENTED:
                node003(cycle);
                break;
            case Cycle::CYCLE_0TWISTED: 
                node004(cycle);
                break;
            case Cycle::CYCLE_1TWISTED: 
                node005(cycle);
                break;
            case Cycle::CYCLE_2TWISTED: 
                node002(cycle);
                break;
            case Cycle::CYCLE_3TWISTED: 
                node001(cycle);
                break;
            default: 
                cerr << "error in MinSWRT::nodeRootLookahead: Algo found unexpected cycle type\n";
                printCycle(cycle);            
                return;
            }
            // store sequence
            it = seqList.begin();
            rank = 1;
            while ((it != seqList.end()) && (rank < m_lookaheadBranch[0]) 
              && ((*it)->m_ratio > m_sequence->m_ratio))
            {
                rank++;
                it++;
            }
            if (rank <= m_lookaheadBranch[0])
            {
                seqList.insert(it, m_sequence);    
                m_sequence = new Sequence();            
            }
        }
        // do lookahead for the best sequences of the list
        it = seqList.begin();
        for (int i = 0; (it != seqList.end()) && (i < m_lookaheadBranch[0]); i++, it++)
        {
            ratio = doLookahead(**it, m_lookaheadDepth);
            if ((gcSeq == NULL) || (ratio > gcSeq->m_ratio))
            {
                gcSeq = *it;
                gcSeq->m_ratio = ratio;
            }
        }    
        if (gcSeq == NULL)     // permutation sorted
            return;
        // perform golden cage sequence
        performSequence(gcSeq);          
        // cleaning up
        for (it = seqList.begin(); it != seqList.end(); it++)
            delete *it;            
        gcSeq = NULL;    
        seqList.clear();
    }    
}


float MinSWRT::doLookahead(Sequence f_seq, int f_depth)
{
//    cerr << "\n entering doLookahead\n";
//    cerr << "ratio: " << f_seq.m_ratio << "  dsigma: " << f_seq.m_dsigma << "  weight: " << f_seq.m_weight << endl;
    
    int size = m_simplePerm->size();  // permutation size
    Permutation* tmp;                 // used for swapping permutations
    int order1[3];                    // needed to check is something is cyclic sorted
    int order2[3];                    // needed to check is something is cyclic sorted
    int delta1, delta2;               // distance to move
    Cycle cycle;                      // a cycle in the RDD (starting cycle of case analysis)   
    list<Sequence*> seqList;          // list with all actual sequences, sorted by ratio
    list<Sequence*>::iterator it;     // list iterator
    int rank;                         // current rank in the sequence list
    int branch = 0;                   // branching degree in this recursion depth 
    float ratio;                      // ratio returned from a recursive call
    float result = 0.0f;              // best possible ratio over all sequences
    
    if (f_depth > 1)   // else not needed
        branch = m_lookaheadBranch[m_lookaheadDepth - f_depth + 1];
    // store old permutation
    tmp = m_simplePerm;
    m_simplePerm = new Permutation();
    m_simplePerm->init(size, tmp->m_data);    
    // perform sequence
    for (int i = 0; i < f_seq.m_numOperations; i++)
    {
        m_simplePerm->performOperation(f_seq.m_operations[i]);    
        // update operations
        if ((f_seq.m_operations[i].m_type == Operation::OPERATION_REVERSAL)
          || (f_seq.m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = f_seq.m_operations[i].m_edges[0];
            order1[1] = f_seq.m_operations[i].m_edges[1];
            for (int j = i+1; j < f_seq.m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    order1[2] = f_seq.m_operations[j].m_edges[k];
                    if (!cyclicSorted(order1))    // move the edge
                        f_seq.m_operations[j].m_edges[k] = 
                          (f_seq.m_operations[i].m_edges[0] 
                          + f_seq.m_operations[i].m_edges[1] 
                          - f_seq.m_operations[j].m_edges[k] + size) % size;
                }              
            }
        }        
        if ((f_seq.m_operations[i].m_type == Operation::OPERATION_TRANSPOSITION)
          || (f_seq.m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = f_seq.m_operations[i].m_edges[1];
            order1[1] = f_seq.m_operations[i].m_edges[2];
            order2[0] = f_seq.m_operations[i].m_edges[2];
            order2[1] = f_seq.m_operations[i].m_edges[0];
            delta1 = f_seq.m_operations[i].m_edges[0] - f_seq.m_operations[i].m_edges[2]; 
            delta2 = f_seq.m_operations[i].m_edges[2] - f_seq.m_operations[i].m_edges[1]; 
            for (int j = i+1; j < f_seq.m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    if (f_seq.m_operations[j].m_edges[k] == f_seq.m_operations[i].m_edges[2])
                        f_seq.m_operations[j].m_edges[k] = (f_seq.m_operations[j].m_edges[k] 
                          + delta1 - delta2 + size) % size;
                    else
                    {
                        order1[2] = f_seq.m_operations[j].m_edges[k];
                        // move first segment to the back
                        if (!cyclicSorted(order1))    // move the edge
                            f_seq.m_operations[j].m_edges[k] = 
                              (f_seq.m_operations[j].m_edges[k] + delta1 + size) 
                              % size;
                        else 
                        {
                            // move second segment to the front      
                            order2[2] = f_seq.m_operations[j].m_edges[k];
                            if (!cyclicSorted(order2))  // move the edge
                            {
                                f_seq.m_operations[j].m_edges[k] = 
                                  (f_seq.m_operations[j].m_edges[k] - delta2 + size) 
                                  % size;
                            }
                        }
                    }      
                }              
            }
        }
    }
    // lookahead    
    for (int i = 0; i < m_simplePerm->size(); i++)    // for all possible cycles
    {
        m_simplePerm->getCycleOfEdge(i, &cycle);
        // choose node
        switch (cycle.m_type)
        {
        case Cycle::CYCLE_ADJACENCY:
            continue;    
        case Cycle::CYCLE_2ORIENTED: 
            node000(cycle);
            break;
        case Cycle::CYCLE_2UNORIENTED:
            node003(cycle);
            break;
        case Cycle::CYCLE_0TWISTED: 
            node004(cycle);
            break;
        case Cycle::CYCLE_1TWISTED: 
            node005(cycle);
            break;
        case Cycle::CYCLE_2TWISTED: 
            node002(cycle);
            break;
        case Cycle::CYCLE_3TWISTED: 
            node001(cycle);
            break;
        default: 
            cerr << "error in MinSWRT::doLookahead: Algo found unexpected cycle type\n";
            printCycle(cycle);            
            continue;
        }
        m_sequence->m_weight += f_seq.m_weight;     // dsigma, w, ratio shall be 
        m_sequence->m_dsigma += f_seq.m_dsigma;     // calculated over the whole sequence
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;   
        if (f_depth > 1)
        {
            // store sequence
            it = seqList.begin();
            rank = 1;
            while ((it != seqList.end()) && (rank < branch) && ((*it)->m_ratio > m_sequence->m_ratio))
            {
                rank++;
                it++;
            }
            if (rank <= branch)
            {
                seqList.insert(it, m_sequence);    
                m_sequence = new Sequence();
            }
        }        
        else if (m_sequence->m_ratio > result)
            result = m_sequence->m_ratio;
    }    
    if (f_depth == 1)            // abort recursion
    {
        // clean up memory
        delete m_simplePerm;
        // unswap permutations
        m_simplePerm = tmp;
        // return the result
        if (result < 0.01f)      // sequence sorted
            return f_seq.m_ratio;
        else
            return result;
    }        
    // do lookahead for the best sequences of the list
    it = seqList.begin();
    for (int i = 0; (it != seqList.end()) && (i < branch); i++, it++)
    {
        ratio = doLookahead(**it, f_depth - 1);
        if (ratio > result)
            result = ratio;
    }    
    // clean up memory
    delete m_simplePerm;
    for (it = seqList.begin(); it != seqList.end(); it++)
        delete *it;
    // unswap permutations
    m_simplePerm = tmp;
    // return the result
    if (result < 0.01f)      // sequence sorted
        return f_seq.m_weight;
    else
        return result;
}


void MinSWRT::node000(Cycle& f_c)
{
#if DEBUG    
    // !!DEBUG: cycle info
    cerr << "entering node000" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
#endif
    
    int create = 0;     // new created followup-2moves
    int destroy = 0;    // destroyed followup-2moves
    int size = m_simplePerm->size();
    Cycle d;
    int order1[3];
    int order2[3];
    
    /* c: r-oriented 2-cycle */    
    m_sequence->m_numOperations = 1;
    m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
    m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
    m_sequence->m_dsigma = 2.0f * m_wr / m_wt;
    m_sequence->m_weight = m_wr;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;        
    // small lookahead: check for destroyed / new-created followups
    if (m_greedy < 2)
        return;
    order1[0] = f_c.m_edges[0];
    order1[2] = f_c.m_edges[1];
    order2[0] = f_c.m_edges[0];
    order2[2] = f_c.m_edges[1];
    for (int i = (f_c.m_edges[0] + 1) % size; i != f_c.m_edges[1]; 
      (i+1 == size? i = 0 : i++))
    {
        m_simplePerm->getCycleOfEdge(i, &d);
        switch (d.m_type)
        {
        case Cycle::CYCLE_2UNORIENTED:    
            order1[1] = d.m_edges[1];
            if (!cyclicSorted(order1))   // intersecting
                create++;
            break;
        case Cycle::CYCLE_2ORIENTED:    
            order1[1] = d.m_edges[1];
            if (!cyclicSorted(order1))   // intersecting
                destroy++;
            break;
        case Cycle::CYCLE_1TWISTED:    
            order1[1] = d.m_edges[0];
            order2[1] = d.m_edges[1];
            if (cyclicSorted(order1) ^ cyclicSorted(order2))
                create++;
            break;
        case Cycle::CYCLE_2TWISTED:    
            order1[1] = d.m_edges[0];
            order2[1] = d.m_edges[1];
            if (cyclicSorted(order1) ^ cyclicSorted(order2))
                destroy++;
            break;
        default:
            break;
        }        
    }  
    m_sequence->m_ratio += (float)(create - destroy) * 0.01f / (float)size;
}


void MinSWRT::node001(Cycle& f_c)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node001" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
#endif
      
    int create = 0;     // new created followup-2moves
    int destroy = 0;    // destroyed followup-2moves
    int size = m_simplePerm->size();
    Cycle d;

    /* c: 3-twisted 3-cycle */
    m_sequence->m_numOperations = 1;
    m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
    m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];    
    m_sequence->m_dsigma = 2.0f;
    m_sequence->m_weight = m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    // small lookahead: check for destroyed / new-created followups
    if (m_greedy < 2)
        return;
    for (int i = (f_c.m_edges[0] + 1) % size; i != f_c.m_edges[1]; 
      (i+1 == size? i = 0 : i++))
    {
        m_simplePerm->getCycleOfEdge(i, &d);
        switch (d.m_type)
        {
        case Cycle::CYCLE_0TWISTED:    
        case Cycle::CYCLE_1TWISTED:    
            if (interleaving(f_c, d))
                create++;
            break;
        case Cycle::CYCLE_2TWISTED:    
        case Cycle::CYCLE_3TWISTED:
            if (interleaving(f_c, d))
                destroy++;
            break;
        default:
            break;
        }        
    }  
    m_sequence->m_ratio += (float)(create - destroy) * 0.01f / (float)size;
}


void MinSWRT::node002(Cycle& f_c)
{
#if DEBUG    
    // !!DEBUG: cycle info
    cerr << "entering node002" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
#endif

    int create = 0;     // new created followup-2moves
    int destroy = 0;    // destroyed followup-2moves
    int size = m_simplePerm->size();
    Cycle d;
    int order1[3];
    int order2[3];

    /* c: 2-twisted 3-cycle */
    m_sequence->m_numOperations = 1;
    m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
    m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
    m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];
    m_sequence->m_dsigma = 2.0f;
    m_sequence->m_weight = m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    // small lookahead: check for destroyed / new-created followups
    if (m_greedy < 2)
        return;
    order1[0] = f_c.m_edges[0];
    order1[2] = f_c.m_edges[1];
    order2[0] = f_c.m_edges[0];
    order2[2] = f_c.m_edges[1];
    for (int i = (f_c.m_edges[0] + 1) % size; i != f_c.m_edges[1]; 
      (i+1 == size? i = 0 : i++))
    {
        m_simplePerm->getCycleOfEdge(i, &d);
        switch (d.m_type)
        {
        case Cycle::CYCLE_2UNORIENTED:
            order1[1] = d.m_edges[1];
            if (cyclicSorted(order1))
                create++;
            break;
        case Cycle::CYCLE_2ORIENTED:
            order1[1] = d.m_edges[1];
            if (cyclicSorted(order1))
                destroy++;
            break;
        case Cycle::CYCLE_0TWISTED:    
            if (interleaving(f_c, d))
                create++;
            break;
        case Cycle::CYCLE_1TWISTED:    
            if (interleaving(f_c, d))
                create++;
            else
            {
                order1[1] = d.m_edges[0];
                order2[1] = d.m_edges[1];
                if (cyclicSorted(order1) ^ cyclicSorted(order2))
                    create++;
            }
            break;
        case Cycle::CYCLE_2TWISTED:    
            if (interleaving(f_c, d))
                destroy++;
            else
            {
                order1[1] = d.m_edges[0];
                order2[1] = d.m_edges[1];
                if (cyclicSorted(order1) ^ cyclicSorted(order2))
                    destroy++;
            }
            break;
        case Cycle::CYCLE_3TWISTED:
            if (interleaving(f_c, d))
                destroy++;
            break;
        default:
            break;
        }        
    }  
    m_sequence->m_ratio += (float)(create - destroy) * 0.01f / (float)size;
}


void MinSWRT::node003(Cycle& f_c)
{
#if DEBUG    
    // !!DEBUG: cycle info
    cerr << "entering node003" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
#endif
    
    /* c is r-unoriented 2-cycle -> search intersecting cycle d */
    Cycle d;        // d intersects c
    int order[3];   // needed to check cyclic sorting of three edges

    m_simplePerm->getCycle(&d, 1, f_c.m_edges[0], f_c.m_edges[1]);
    switch (d.m_type)
    {
    case Cycle::CYCLE_2ORIENTED: 
        node000(d);
        break;
    case Cycle::CYCLE_2UNORIENTED:
        node006(f_c, d);
        break;
    case Cycle::CYCLE_0TWISTED: 
        // search for the chord of d not intersected by c
        order[0] = f_c.m_edges[0];
        order[2] = f_c.m_edges[1];
        order[1] = d.m_edges[1];
        if (cyclicSorted(order))        // chord between RE 0 and 1 is not 
        {                               // intersected
            d.m_edges[1] = d.m_edges[0];
            d.m_edges[0] = d.m_edges[2];
            d.m_edges[2] = order[1];            
        }
        else
        {
            order[1] = d.m_edges[2];
            if (cyclicSorted(order))    // chord between RE 0 and 1 is not 
            {                           // intersected
                d.m_edges[2] = d.m_edges[0];
                d.m_edges[0] = d.m_edges[1];
                d.m_edges[1] = order[1];
            }
            else                        // switch RE of c
            {
                f_c.m_edges[0] = f_c.m_edges[1];
                f_c.m_edges[1] = order[0];
            }            
        }                               
        node008(f_c, d);
        break;
    case Cycle::CYCLE_1TWISTED: 
        order[0] = d.m_edges[0];
        order[2] = d.m_edges[1];
        order[1] = f_c.m_edges[0];
        if (cyclicSorted(order))         // c intersect nontwisted chord
        {
            node007(f_c, d);
            break;
        }
        order[1] = f_c.m_edges[1];    
        if (cyclicSorted(order))         // c intersect nontwisted chord
        {                                // switch edges of c
            f_c.m_edges[2] = f_c.m_edges[1];
            f_c.m_edges[1] = f_c.m_edges[0];
            f_c.m_edges[0] = f_c.m_edges[2];
            node007(f_c, d);
            break;
        }
        node005(d);
        break;
    case Cycle::CYCLE_2TWISTED: 
        node002(d);
        break;
    case Cycle::CYCLE_3TWISTED: 
        node001(d);
        break;
    default: 
        cerr << "error in MinSWRT::node003: Algo found unexpected cycle type\n";
        return;
    }  
}


void MinSWRT::node004(Cycle& f_c)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node004" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
#endif
      
    /* c is a nontwisted 3-cycle -> search intersecting cycle d */
    Cycle d;        // d intersects c
    int order[3];   // needed to check cyclic sorting of three edges

    m_simplePerm->getCycle(&d, 1, f_c.m_edges[0], f_c.m_edges[1]);
    switch (d.m_type)
    {
    case Cycle::CYCLE_2ORIENTED: 
        node000(d);
        break;
    case Cycle::CYCLE_2UNORIENTED:
        // search for the non-intersected chord of c
        order[0] = f_c.m_edges[1];
        order[2] = f_c.m_edges[2];
        order[1] = d.m_edges[1];
        if (cyclicSorted(order))        // non-intersected chord between 
        {                               // RE 2 and 0
            f_c.m_edges[2] = f_c.m_edges[0];
            f_c.m_edges[0] = f_c.m_edges[1];
            f_c.m_edges[1] = order[2];
            d.m_edges[1] = d.m_edges[0];
            d.m_edges[0] = order[1];
        }                               // else no shift necessary
        node008(d, f_c);         
        break;
    case Cycle::CYCLE_0TWISTED:         
        if (interleaving(f_c, d))       // check if interleaving    
        {
            node016(f_c, d);                    
            break;
        }
        // search non-intersected chord of d
        order[0] = f_c.m_edges[0];
        order[2] = f_c.m_edges[1];
        order[1] = d.m_edges[1];
        if (cyclicSorted(order))        // non-intersected chord between
        {                               // RE 0 and 1
            d.m_edges[1] = d.m_edges[0];
            d.m_edges[0] = d.m_edges[2];
            d.m_edges[2] = order[1];
            order[1] = d.m_edges[0];    // store edge that is not between RE 0 and 1 of c
        }
        else
        {
            order[1] = d.m_edges[2];
            if (cyclicSorted(order))    // non-intersected chord between
            {                           // RE 2 and 0
                d.m_edges[2] = d.m_edges[0];
                d.m_edges[0] = d.m_edges[1];
                d.m_edges[1] = order[1];
                order[1] = d.m_edges[0];// store edge that is not between RE 0 and 1 of c
            }
        }                               // else no shift necessary
        // search non-intersected edge of c
        order[2] = f_c.m_edges[2];
        if (cyclicSorted(order))        // intersection between RE 1 and 2 of c
        {
            f_c.m_edges[2] = f_c.m_edges[0];
            f_c.m_edges[0] = f_c.m_edges[1];
            f_c.m_edges[1] = order[2];
        }
        node017(f_c, d);             
        break;
    case Cycle::CYCLE_1TWISTED: 
        if (interleaving(f_c, d))       // check if interleaving    
        {
            // search position of twisted edge
            order[0] = f_c.m_edges[0];
            order[2] = f_c.m_edges[1];
            order[1] = d.m_edges[0];
            if (cyclicSorted(order))
            {
                f_c.m_edges[0] = f_c.m_edges[1];
                f_c.m_edges[1] = f_c.m_edges[2];
                f_c.m_edges[2] = order[0];                    
            }
            else
            {
                order[1] = d.m_edges[1];
                if (!cyclicSorted(order))
                {
                    f_c.m_edges[0] = f_c.m_edges[2];
                    f_c.m_edges[2] = f_c.m_edges[1];
                    f_c.m_edges[1] = order[0];                    
                }
            }
            node040(f_c, d);                    
            break;
        }
        order[0] = d.m_edges[0];
        order[2] = d.m_edges[1];
        order[1] = f_c.m_edges[0];
        if (cyclicSorted(order))         // c intersect nontwisted chord
        {
            order[1] = f_c.m_edges[2];
            if (cyclicSorted(order))     // no intersection between c2, c0
            {                            // -> shift c
                f_c.m_edges[2] = f_c.m_edges[0];
                f_c.m_edges[0] = f_c.m_edges[1];
                f_c.m_edges[1] = order[1];
            }            
            node026(d, f_c);             
            break;
        }
        order[1] = f_c.m_edges[1];
        if (cyclicSorted(order))         // c intersect nontwisted chord
        {
            order[1] = f_c.m_edges[2];
            if (!cyclicSorted(order))    // no intersection between c2, c0
            {                            // -> shift c
                f_c.m_edges[2] = f_c.m_edges[0];
                f_c.m_edges[0] = f_c.m_edges[1];
                f_c.m_edges[1] = order[1];
            }            
            node026(d, f_c);             
            break;
        }
/*        
        order[1] = f_c.m_edges[2];
        if (cyclicSorted(order))         // c intersect nontwisted chord
        {
            node026(d, f_c);             
            break;
        }
        */
        node005(d);                      // discard c
        break;
    case Cycle::CYCLE_2TWISTED: 
        node002(d);
        break;
    case Cycle::CYCLE_3TWISTED: 
        node001(d);
        break;
    default: 
        cerr << "error in MinSWRT::node004: Algo found unexpected cycle type\n";
        return;
    }  
}


void MinSWRT::node005(Cycle& f_c)
{
#if DEBUG    
    // !!DEBUG: cycle info
    cerr << "entering node005" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
#endif
      
    /* Node 005: c is a 1-twisted 3-cycle -> intersecting cycle d */
    Cycle d;        // d intersects c
    int order[3];   // needed to check cyclic sorting of three edges

    m_simplePerm->getCycle(&d, 1, f_c.m_edges[0], f_c.m_edges[1]);
    switch (d.m_type)
    {
    case Cycle::CYCLE_2ORIENTED: 
        node000(d);
        break;
    case Cycle::CYCLE_2UNORIENTED:
        node007(d, f_c);
        break;
    case Cycle::CYCLE_0TWISTED: 
        if (interleaving(f_c, d))
        {
            node040(d, f_c);      
            break;
        }
        order[0] = f_c.m_edges[0];
        order[2] = f_c.m_edges[1];
        order[1] = d.m_edges[1];
        if (cyclicSorted(order))         // non-intersected chord between
        {                                // RE 0 and 1
            d.m_edges[1] = d.m_edges[0];
            d.m_edges[0] = d.m_edges[2];
            d.m_edges[2] = order[1];
        }
        else
        {
            order[1] = d.m_edges[2];
            if (cyclicSorted(order))     // non-intersected chord between
            {                            // RE 2 and 0
                d.m_edges[2] = d.m_edges[0];
                d.m_edges[0] = d.m_edges[1];
                d.m_edges[1] = order[1];
            }
        }    // else no shift necessary
        node026(f_c, d);
        break;
    case Cycle::CYCLE_1TWISTED: 
        if (interleaving(f_c, d))
        {
            node024(f_c, d);
            break;
        }
        node025(f_c, d);
        break;
    case Cycle::CYCLE_2TWISTED: 
        node002(d);
        break;
    case Cycle::CYCLE_3TWISTED: 
        node001(d);
        break;
    default: 
        cerr << "error in MinSWRT::node005: Algo found unexpected cycle type\n";
        return;
    }
}


void MinSWRT::node006(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node006" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif
   
    /* c,d are intersecting r-unoriented 2-cycles.*/
    int order[3];   // needed to check cyclic sorting of three edges

    if (3 * m_wr < 2 * m_wt)
    {    
        m_sequence->m_numOperations = 3;
        m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
        m_sequence->m_operations[1].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
        m_sequence->m_dsigma = 4.0f * m_wr / m_wt;
        m_sequence->m_weight = 3.0f * m_wr;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    }
    else
    {
        // edges should be sorted c0 d0 c1 d1
        order[0] = f_c.m_edges[0];
        order[1] = f_d.m_edges[0];
        order[2] = f_c.m_edges[1];
        if (!cyclicSorted(order))
        {
            f_d.m_edges[2] = f_d.m_edges[0];
            f_d.m_edges[0] = f_d.m_edges[1];
            f_d.m_edges[1] = f_d.m_edges[2];
        }
        m_sequence->m_numOperations = 2;
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[0];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
        m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[1];
        m_sequence->m_dsigma = 4.0f * m_wr / m_wt;
        m_sequence->m_weight = 2.0f * m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    }
}

    
void MinSWRT::node007(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node007" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif

    /* c is an r-unoriented 2-cycle, d is a 1-twisted 3-cycle. c intersects 
     * the nontwisted chord of d. The edge of c that lies between the 
     * nontwisted edges of d is m_edge[0].*/
    int order[3];   // needed to check cyclic sorting of three edges

    m_sequence->m_numOperations = 3;
    order[0] = f_d.m_edges[1];
    order[1] = f_c.m_edges[1];
    order[2] = f_d.m_edges[2];
    if (cyclicSorted(order))    // edge sequence d2 d0 c0 d1 c1
    {
        m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[1];
        m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[2];
    }
    else                        // edge sequence d2 c1 d0 c0 d1
    {
        m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[2];
        m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[0];
    }
    m_sequence->m_operations[1].m_type = Operation::OPERATION_REVERSAL;
    m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[1];
    m_sequence->m_operations[2].m_type = Operation::OPERATION_REVERSAL;
    m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[1];
    m_sequence->m_dsigma = 2.0f + 2.0f * m_wr / m_wt;
    m_sequence->m_weight = 3.0f * m_wr;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}
    

void MinSWRT::node008(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node008" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif
    
    /* c is an r-unoriented 2-cycle, d is a nontwisted 3-cycle. c and d are 
     * intersecting. The chord of d not intersected by c is between the 
     * reality-edges m_edges[1] and m_edges[2]. m_edges[0] of c lies between
     * m_edges[0] and m_edges[1] of d. */
    Cycle e;           // intersects open chord of d 
    int order[3];      // needed to check cyclic sorting
    int twisted;       // position of twisted edge
    
    m_simplePerm->getCycle(&e,1, f_d.m_edges[1], f_d.m_edges[2]);
    switch (e.m_type)
    {
    case Cycle::CYCLE_2ORIENTED: 
        node000(e);
        break;
    case Cycle::CYCLE_2UNORIENTED:    
        node009(f_c, f_d, e);
        break;
    case Cycle::CYCLE_0TWISTED: 
        if (interleaving(f_d, e))
        {
            node016(f_d, e);
            break;
        }        
        order[0] = f_d.m_edges[1];
        order[2] = f_d.m_edges[2];
        order[1] = e.m_edges[1];
        if (cyclicSorted(order))          // no intersection between RE 0 and 1
        {
            e.m_edges[1] = e.m_edges[0];
            e.m_edges[0] = e.m_edges[2];
            e.m_edges[2] = order[1];
        }
        else
        {
            order[1] = e.m_edges[2];
            if (cyclicSorted(order))      // no intersection between RE 2 and 0
            {
                e.m_edges[2] = e.m_edges[0];
                e.m_edges[0] = e.m_edges[1];
                e.m_edges[1] = order[1];
            }
        }    // else no shift necessary
        node010(f_c, f_d, e);
        break;
    case Cycle::CYCLE_1TWISTED: 
        order[0] = f_d.m_edges[0];        // search position of twisted edge
        order[2] = f_d.m_edges[1];
        order[1] = e.m_edges[2];
        if (cyclicSorted(order))          
            twisted = 0;
        else
        {
            order[0] = f_d.m_edges[1];
            order[2] = f_d.m_edges[2];
            if (cyclicSorted(order))
                twisted = 1;
            else
                twisted = 2;
        }            
        if (interleaving(f_d, e))
        {
            if (twisted == 0)
            {
                f_d.m_edges[0] = f_d.m_edges[2];
                f_d.m_edges[2] = f_d.m_edges[1];
                f_d.m_edges[1] = order[0];
            }
            else if (twisted == 2)
            {
                f_d.m_edges[2] = f_d.m_edges[0];
                f_d.m_edges[0] = f_d.m_edges[1];
                f_d.m_edges[1] = order[2];
            }
            node040(f_d, e); 
            break;    
        }
        node005(e);
        break;
    case Cycle::CYCLE_2TWISTED: 
        node002(e);
        break;
    case Cycle::CYCLE_3TWISTED: 
        node001(e);
        break;
    default: 
        cerr << "error in MinSWRT::node008: Algo found unexpected cycle type\n";
        return;
    }
     
}


void MinSWRT::node009(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node009" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* c and e are r-unoriented 2-cycles, d is a nontwisted 3-cycle. 
     * c.m_edges[0] lies between the reality-edges m_edges[0] and m_edges[1] 
     * of d, e.m_edges[0] lies between m_edges[1] and m_edges[2] of d. 
     * c.m_edges[1] lies between m_edges[0] and m_edges[2] of d. The position
     * of e.m_edges[1] is arbitrary.*/
    int order1[3];  // needed to check cyclic sorting of three edges
    int order2[3];  // needed to check cyclic sorting of three edges
    
    order1[0] = f_c.m_edges[0];
    order1[2] = f_c.m_edges[1];
    order1[1] = f_e.m_edges[0];
    order2[0] = f_c.m_edges[0];
    order2[2] = f_c.m_edges[1];
    order2[1] = f_e.m_edges[1];
    if (cyclicSorted(order1) ^ cyclicSorted(order2))
        node006(f_c, f_e);
    else
        node012(f_c, f_d, f_e);        
}


void MinSWRT::node010(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node010" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* c is an r-unoriented 2-cycle, d and e are nontwisted 3-cycles.
     * c intersects d (m_edges[0] of c between m_edges[0] and m_edges[1] of 
     * d, m_edges[1] of c between m_edges[2] and m_edges[0] of d). e 
     * intersects d (at least one edge of e between m_edges[1] and m_edges[2]
     * of d, no edge of d between m_edges[1] and m_edges[2] of e). */
    int order[3];   // needed to check cyclic sorting of three edges
    
    order[0] = f_e.m_edges[1];
    order[2] = f_e.m_edges[2];
    order[1] = f_c.m_edges[0];
    if (cyclicSorted(order))
    {
        node011(f_c, f_d, f_e);
        return;
    }
    order[1] = f_c.m_edges[1];
    if (cyclicSorted(order))
    {
        node013(f_c, f_d, f_e);
        return;
    }
    node014(f_c, f_d, f_e);    
}


void MinSWRT::node011(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node011" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* Node 011: c is r-unoriented 2-cycle, d and e are nontwisted 3-cycles. 
     * The configuration looks like this:
     *         
     *           c0
     *       e2      e1
     *     
     *     d1          d0
     * 
     *       e0      c1  
     *           d2               */
    if (m_wr < m_wt)
    {    
        m_sequence->m_numOperations = 3;
        m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[1];
        m_sequence->m_operations[1].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[1];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
        m_sequence->m_dsigma = 2.0f + 2.0f * m_wr / m_wt;
        m_sequence->m_weight = 2.0f * m_wr + m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    }
    else
    {    
        m_sequence->m_numOperations = 3;
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[0];
        m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[2];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
        m_sequence->m_dsigma = 4.0f;
        m_sequence->m_weight = 3.0f * m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    }
}


void MinSWRT::node012(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node012" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* Node 012: c and e are r-unoriented 2-cycles, d is a nontwisted 
     * 3-cycle. c.m_edges[0] lies between the reality-edges m_edges[0] and
     * m_edges[1] of d, c.m_edges[1] lies between the reality-edges 
     * m_edges[2] and m_edges[0] of d, e.m_edges[0] lies between m_edges[1] 
     * and m_edges[2] of d. The two 2-cycles are not intersecting. */
    m_sequence->m_numOperations = 3;
    m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
    m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
    m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
    m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
    m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];
    m_sequence->m_dsigma = 2.0f + 4.0f * m_wr / m_wt;
    m_sequence->m_weight = 3.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}


void MinSWRT::node013(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node013" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* Node 013: c is r-unoriented 2-cycle, d and e are nontwisted 3-cycles. 
     * The configuration looks like this:
     *         
     *           c0
     *       d1      d0
     *     
     *     e0          e2
     * 
     *       d2      c1  
     *           e1        */
    if (m_wr < m_wt)
    {    
        m_sequence->m_numOperations = 3;
        m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[2];
        m_sequence->m_operations[1].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[1];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[1];
        m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[0];     
        m_sequence->m_dsigma = 2.0f + 2.0f * m_wr / m_wt;
        m_sequence->m_weight = 2.0f * m_wr + m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    }
    else
    {    
        m_sequence->m_numOperations = 3;
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[1].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_e.m_edges[2];
        m_sequence->m_operations[1].m_edges[2] = f_e.m_edges[1];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[2];
        m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[1];
        m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[0];
        m_sequence->m_dsigma = 4.0f;
        m_sequence->m_weight = 3.0f * m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    }
}


void MinSWRT::node014(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node014" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* Node 014: c is an r-unoriented 2-cycle, d and e are nontwisted 3-cycles.
     * c intersects d (m_edges[0] of c between m_edges[0] and m_edges[1] of 
     * d, m_edges[1] of c between m_edges[2] and m_edges[0] of d). e 
     * intersects d (at least one edge of e between m_edges[1] and m_edges[2]
     * of d). The chord between m_edges[1] and m_edges[2] of e is neither 
     * intersected by c nor by d. */
    Cycle f;        // intersects chord between RE 1 and 2 of e 
    int order[3];   // needed to check cyclic sorting of three edges
    int cs1, cs2;   // result of a call of cyclicSorted
    
    m_simplePerm->getCycle(&f, 1, f_e.m_edges[1], f_e.m_edges[2]);
    switch (f.m_type)
    {
    case Cycle::CYCLE_2ORIENTED: 
        node000(f);
        break;
    case Cycle::CYCLE_2UNORIENTED:
        // check if c, f are intersecting
        order[0] = f_c.m_edges[1];
        order[2] = f_c.m_edges[0];
        order[1] = f.m_edges[0];
        cs1 = cyclicSorted(order);
        order[1] = f.m_edges[1];
        cs2 = cyclicSorted(order);
        if (cs1 ^ cs2)            // c, f intersecting
        {
            node006(f_c, f);
            break;
        }
        if (cs1)                  // -> e must intersect c -> node012
        {
            f_c.m_edges[0] = f_c.m_edges[1];   // switch c
            f_c.m_edges[1] = order[2];
            node012(f_c, f_e, f);
            break;
        }            
        // check if d, f are intersecting
        order[0] = f_d.m_edges[0];
        order[2] = f_d.m_edges[1];
        cs2 = cyclicSorted(order);
        order[1] = f.m_edges[0];
        cs1 = cyclicSorted(order);
        if (cs1 ^ cs2)           // f intersects d between RE0 and RE1
        {
            // search position of second edge of f
            if (cs1)             
                order[1] = f.m_edges[1];
            order[0] = f_d.m_edges[1];
            order[2] = f_d.m_edges[2];
            if (cyclicSorted(order))
            {
                if (cs1) 
                {
                    f.m_edges[1] = f.m_edges[0];
                    f.m_edges[0] = order[1];
                }
                node012(f_c, f_d, f);
            }
            else if (cs1)
                node011(f, f_d, f_e);
            else
            {
                f.m_edges[0] = f.m_edges[1];
                f.m_edges[1] = order[1];
                node013(f, f_d, f_e);    
            }
            break;                
        }
        order[0] = f_d.m_edges[1];
        order[2] = f_d.m_edges[2];
        cs1 = cyclicSorted(order);
        order[1] = f.m_edges[1];
        cs2 = cyclicSorted(order);
        if (cs1 ^ cs2)           // f intersects d between RE1 and RE2 
        {                        // other RE of f between d.RE2 and d.RE0
            if (cs2) 
            {
                f.m_edges[1] = f.m_edges[0];
                f.m_edges[0] = order[1];
            }
            node012(f_c, f_d, f);                
            break;            
        }
        node015(f_c, f_d, f_e, f);
        break;        
    case Cycle::CYCLE_0TWISTED: 
        if (interleaving(f_d, f))
            node016(f_d, f);
        else if (interleaving(f_e, f))
            node016(f_e, f);        
        else    
        {
            // search position of d.m_edges[0]
            order[0] = f_d.m_edges[1];
            order[2] = f_d.m_edges[2];
            order[1] = f_e.m_edges[0];
            if (cyclicSorted(order))          // shift d
            {
                f_d.m_edges[1] = f_d.m_edges[2];
                f_d.m_edges[2] = f_d.m_edges[0];
                f_d.m_edges[0] = order[0];
            }
            else
            {
                order[0] = f_d.m_edges[2];
                order[2] = f_d.m_edges[0];
                if (cyclicSorted(order))      // shift d
                {
                    f_d.m_edges[2] = f_d.m_edges[1];
                    f_d.m_edges[1] = f_d.m_edges[0];
                    f_d.m_edges[0] = order[0];
                }
            }     // else no shift necessary
            node018(f_d, f_e, f);        
        }
        break;        
    case Cycle::CYCLE_1TWISTED: 
        if (interleaving(f_d, f))
        {
            order[0] = f_d.m_edges[1];
            order[2] = f_d.m_edges[2];
            order[1] = f.m_edges[0];
            if (cyclicSorted(order))              // shift d
            {
                f_d.m_edges[2] = f_d.m_edges[0];
                f_d.m_edges[0] = f_d.m_edges[1];
                f_d.m_edges[1] = order[2];
            }
            else
            {
                order[1] = f.m_edges[1];
                if (cyclicSorted(order))          // shift d
                {
                    f_d.m_edges[2] = f_d.m_edges[1];
                    f_d.m_edges[1] = f_d.m_edges[0];
                    f_d.m_edges[0] = order[2];
                }
            }     // else no shift necessary                    
            node040(f_d, f);
            break;
        }
        if (interleaving(f_e, f))
        {
            order[0] = f_e.m_edges[1];
            order[2] = f_e.m_edges[2];
            order[1] = f.m_edges[0];
            if (cyclicSorted(order))              // shift e
            {
                f_e.m_edges[2] = f_e.m_edges[0];
                f_e.m_edges[0] = f_e.m_edges[1];
                f_e.m_edges[1] = order[2];
            }
            else
            {
                order[1] = f.m_edges[1];
                if (cyclicSorted(order))          // shift d
                {
                    f_e.m_edges[2] = f_e.m_edges[1];
                    f_e.m_edges[1] = f_e.m_edges[0];
                    f_e.m_edges[0] = order[2];
                }
            }     // else no shift necessary                    
            node040(f_e, f);
            break;
        }
        // search separated edge of d
        order[0] = f_d.m_edges[1];
        order[2] = f_d.m_edges[2];
        order[1] = f_e.m_edges[0];
        if (cyclicSorted(order))
        {
            order[2] = f_d.m_edges[0];
            order[1] = f_e.m_edges[1];
            if (cyclicSorted(order))    // separated edge: d.2
            {
                f_d.m_edges[0] = f_d.m_edges[2];
                f_d.m_edges[2] = f_d.m_edges[1];
                f_d.m_edges[1] = order[2];
            }
            else                        // separated edge: d.1
            {
                f_d.m_edges[0] = f_d.m_edges[1];
                f_d.m_edges[1] = f_d.m_edges[2];
                f_d.m_edges[2] = order[2];
            }
        }
        else
        {
            order[2] = f_d.m_edges[0];
            if (cyclicSorted(order))    // separated edge: d.2
            {
                f_d.m_edges[0] = f_d.m_edges[2];
                f_d.m_edges[2] = f_d.m_edges[1];
                f_d.m_edges[1] = order[2];
            }
            else                        // separated edge: d.1
            {
                f_d.m_edges[0] = f_d.m_edges[1];
                f_d.m_edges[1] = f_d.m_edges[2];
                f_d.m_edges[2] = order[2];
            } 
        }        
        node019(f_d, f_e, f);
        break;
    case Cycle::CYCLE_2TWISTED: 
        node002(f);
        break;
    case Cycle::CYCLE_3TWISTED: 
        node001(f);
        break;
    default: 
        cerr << "error in MinSWRT::node014: Algo found unexpected cycle type\n";
        printCycle(f);            
        return;        
    }    
}


void MinSWRT::node015(Cycle& f_c, Cycle& f_d, Cycle& f_e, Cycle& f_f)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node015" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
    cerr << "cycle f:" << endl;
    printCycle(f_f);
#endif
    
    /* c and f are an r-unoriented 2-cycle, d and e are nontwisted 
     * 3-cycles. c intersects d (m_edges[0] of c between m_edges[0] and 
     * m_edges[1] of d, m_edges[1] of c between m_edges[2] and m_edges[0] of 
     * d). e intersects d (at least one edge of e between m_edges[1] and 
     * m_edges[2] of d). The chord between m_edges[1] and m_edges[2] of e is 
     * neither intersected by c nor by d, but intersected by f (f.m_edges[0]
     * between the reality-edges). c and f are not intersecting; if d and f
     * are intersecting, no reality-edge of f is between d.m_edges[1] and 
     * d.m_edges[2]. */
    int order[3];   // needed to check cyclic sorting of three edges
    
    // search position of f.RE0
    order[0] = f_c.m_edges[0];
    order[2] = f_d.m_edges[1];
    order[1] = f_f.m_edges[0];
    if (cyclicSorted(order))        // f.RE0 between c.RE0 and d.RE1
    {
        // search position of f.RE1
        order[1] = f_f.m_edges[1];
        if (!cyclicSorted(order))  // edge sequence d0 c0 e1 f0 e2 d1 e0 d2 f1 c1
        {
#if DEBUG            
            cerr << "node015.1\n";
#endif
            m_sequence->m_numOperations = 3;
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_f.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_f.m_edges[1];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_f.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];
            m_sequence->m_dsigma = 4.0f;
            m_sequence->m_weight = 3.0f * m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }                
        order[2] = f_e.m_edges[1];
        if (cyclicSorted(order))   // edge sequence d0 c0 f1 e1 f0 e2 d1 e0 d2 c1
        {
#if DEBUG            
            cerr << "node015.2\n";
#endif
            m_sequence->m_numOperations = 3;
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];            
            m_sequence->m_dsigma = 4.0f;
            m_sequence->m_weight = 3.0f * m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }                   
#if DEBUG            
        cerr << "node015.3\n";
#endif
        m_sequence->m_numOperations = 4;       // edge sequence d0 c0 e1 f0 e2 f1 d1 e0 d2 c1
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[0].m_edges[0] = f_f.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_f.m_edges[1];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];                    
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[1].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_e.m_edges[2];
        m_sequence->m_operations[1].m_edges[2] = f_e.m_edges[1];            
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[1];            
        m_sequence->m_operations[3].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[3].m_edges[1] = f_f.m_edges[1];
        m_sequence->m_operations[3].m_edges[2] = f_f.m_edges[0];                    
        m_sequence->m_dsigma = 4.0f + 4.0f * m_wr / m_wt;
        m_sequence->m_weight = 4.0f * m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
        return;
    }
    order[0] = f_d.m_edges[2];
    order[2] = f_c.m_edges[1];
    if (cyclicSorted(order))       // f.RE0 between d.RE2 and c.RE1
    {
        // search position of f.RE1
        order[1] = f_f.m_edges[1];
        if (!cyclicSorted(order))  // edge sequence d0 c0 f1 d1 e0 d2 e1 f0 e2 c1
        {
#if DEBUG            
            cerr << "node015.4\n";
#endif
            m_sequence->m_numOperations = 3;
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_f.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_f.m_edges[0];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_f.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];                        
            m_sequence->m_dsigma = 4.0f;
            m_sequence->m_weight = 3.0f * m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }                
        order[0] = f_e.m_edges[2];
        if (cyclicSorted(order))   // edge sequence d0 c0 d1 e0 d2 e1 f0 e2 f1 c1
        {
#if DEBUG            
            cerr << "node015.5\n";
#endif
            m_sequence->m_numOperations = 3;
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];                        
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];                        
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];                                    
            m_sequence->m_dsigma = 4.0f;
            m_sequence->m_weight = 3.0f * m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }                          
#if DEBUG            
        cerr << "node015.6\n";
#endif
        m_sequence->m_numOperations = 4;       // edge sequence d0 c0 d1 e0 d2 f1 e1 f0 e2 c1
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_f.m_edges[1];
        m_sequence->m_operations[0].m_edges[2] = f_f.m_edges[0];                            
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[1].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_e.m_edges[2];
        m_sequence->m_operations[1].m_edges[2] = f_e.m_edges[1];                    
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[1];                    
        m_sequence->m_operations[3].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[3].m_edges[0] = f_f.m_edges[0];
        m_sequence->m_operations[3].m_edges[1] = f_c.m_edges[1];
        m_sequence->m_operations[3].m_edges[2] = f_c.m_edges[0];                    
        m_sequence->m_dsigma = 4.0f + 4.0f * m_wr / m_wt;
        m_sequence->m_weight = 4.0f * m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
        return;
    }                              // f.RE0 between d.RE1 and d.RE2
    // search e.RE0
    order[1] = f_e.m_edges[0];
    if (cyclicSorted(order))       // e.RE0 between d.RE2 and c.RE1
    {
        // position of f.RE1 doesn't matter, same operations
        // edge sequence d0 c0 d1 e1 f0 e2 f1 d2 e0 c1
        //            or d0 c0 d1 f1 e1 f0 e2 d2 e0 c1      
#if DEBUG            
        cerr << "node015.7\n";
#endif
        m_sequence->m_numOperations = 4;
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_f.m_edges[0];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];                                                
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];                            
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];                                
        m_sequence->m_operations[3].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[3].m_edges[0] = f_f.m_edges[0];
        m_sequence->m_operations[3].m_edges[1] = f_f.m_edges[1];
        m_sequence->m_operations[3].m_edges[2] = f_c.m_edges[1];                    
        m_sequence->m_dsigma = 4.0f + 4.0f * m_wr / m_wt;
        m_sequence->m_weight = 4.0f * m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
        return;
    }                              // e.RE0 between c.RE0 and d.RE1
    // position of f.RE1 doesn't really matter - operations are the same
    // edge sequence d0 c0 e0 d1 e1 f0 e2 f1 d2 c1
    //            or d0 c0 e0 d1 f1 e1 f0 e2 d2 c1
#if DEBUG            
    cerr << "node015.8\n";
#endif
    m_sequence->m_numOperations = 4; 
    m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[0].m_edges[1] = f_f.m_edges[0];
    m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];                                                        
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];                            
    m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
    m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
    m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];                                        
    m_sequence->m_operations[3].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[3].m_edges[0] = f_f.m_edges[0];
    m_sequence->m_operations[3].m_edges[1] = f_f.m_edges[1];
    m_sequence->m_operations[3].m_edges[2] = f_c.m_edges[1];                                
    m_sequence->m_dsigma = 4.0f + 4.0f * m_wr / m_wt;
    m_sequence->m_weight = 4.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}


void MinSWRT::node016(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node016" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif

    /* c and d are two interleaving nontwisted 3-cycles */
    m_sequence->m_numOperations = 3;
    m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
    m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];                                
    m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
    m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];                                
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 3.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}

    
void MinSWRT::node017(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node017" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif
    
    /* c and d are two intersecting, non-interleaving nontwisted 3-cycles. 
     * For both cycles, the non-intersected chord is between the 
     * reality-edges m_edges[1] and m_edges[2]. */
    Cycle e;        // intersects open arc of d
    int order[3];   // needed to check cyclic sorting of three edges
    
    m_simplePerm->getCycle(&e, 1, f_d.m_edges[1], f_d.m_edges[2]) ;
    switch (e.m_type)
    {
    case Cycle::CYCLE_2ORIENTED: 
        node000(e);
        break;
    case Cycle::CYCLE_2UNORIENTED:
        order[0] = f_c.m_edges[1];
        order[2] = f_c.m_edges[2];
        order[1] = e.m_edges[1];
        if (cyclicSorted(order))
        {
            order[1] = f_d.m_edges[0];
            order[2] = e.m_edges[0];   
            if (cyclicSorted(order))   
            {
                f_c.m_edges[1] = f_c.m_edges[2];
                f_c.m_edges[2] = f_c.m_edges[0];
                f_c.m_edges[0] = order[0];
                e.m_edges[0] = e.m_edges[1];
                e.m_edges[1] = order[2];
                node013(e, f_c, f_d);
            }
            else
            {
                f_c.m_edges[1] = f_c.m_edges[0];
                f_c.m_edges[0] = f_c.m_edges[2];
                f_c.m_edges[2] = order[0];
                node011(e, f_c, f_d);
            }
            break;
        }
        // find second chord of d intersected by e
        order[0] = f_d.m_edges[0];
        order[2] = f_d.m_edges[1];
        if (cyclicSorted(order))
        {
            f_d.m_edges[0] = f_d.m_edges[1];
            f_d.m_edges[1] = f_d.m_edges[2];
            f_d.m_edges[2] = order[0];
        }
        else
        {
            f_d.m_edges[0] = f_d.m_edges[2];
            f_d.m_edges[2] = f_d.m_edges[1];
            f_d.m_edges[1] = order[0];
            e.m_edges[1] = e.m_edges[0];
            e.m_edges[0] = order[1];
        }
        node014(e, f_d, f_c);
        break;
    case Cycle::CYCLE_0TWISTED: 
        if (interleaving(f_c, e))
        {
            node016(f_c, e);
            break;
        }
        if (interleaving(f_d, e))
        {
            node016(f_d, e);
            break;
        }
        // find position of d.m_edges[0] in cycle c
        order[0] = f_c.m_edges[0];
        order[2] = f_c.m_edges[1];
        order[1] = f_d.m_edges[0];
        if (!cyclicSorted(order))     // d.RE0 between c.RE2 and c.RE0 
        {                             // -> shift c
            f_c.m_edges[0] = f_c.m_edges[2];
            f_c.m_edges[2] = f_c.m_edges[1];
            f_c.m_edges[1] = order[0];
        }
        node018(f_c, f_d, e);
        break;
    case Cycle::CYCLE_1TWISTED: 
        if (interleaving(f_c, e))
        {
            // search position of the twisted edge
            order[0] = f_c.m_edges[0];
            order[2] = f_c.m_edges[1];
            order[1] = e.m_edges[2];
            if (cyclicSorted(order))        // shift c
            {
                f_c.m_edges[0] = f_c.m_edges[2];
                f_c.m_edges[2] = f_c.m_edges[1];
                f_c.m_edges[1] = order[0];
            }
            else
            {
                order[0] = f_c.m_edges[2];
                if (cyclicSorted(order))     // shift c
                {
                    f_c.m_edges[2] = f_c.m_edges[0];
                    f_c.m_edges[0] = f_c.m_edges[1];
                    f_c.m_edges[1] = order[0];
                }
            }
            node040(f_c, e);
            break;
        }
        if (interleaving(f_d, e))
        {
            // search position of the twisted edge
            order[0] = f_d.m_edges[0];
            order[2] = f_d.m_edges[1];
            order[1] = e.m_edges[2];
            if (cyclicSorted(order))        // shift c
            {
                f_d.m_edges[0] = f_d.m_edges[2];
                f_d.m_edges[2] = f_d.m_edges[1];
                f_d.m_edges[1] = order[0];
            }
            else
            {
                order[0] = f_d.m_edges[2];
                if (cyclicSorted(order))     // shift c
                {
                    f_d.m_edges[2] = f_d.m_edges[0];
                    f_d.m_edges[0] = f_d.m_edges[1];
                    f_d.m_edges[1] = order[0];
                }
            }
            node040(f_d, e);
            break;
        }
        node019(f_c, f_d, e);
        break;
    case Cycle::CYCLE_2TWISTED: 
        node002(e);
        break;
    case Cycle::CYCLE_3TWISTED: 
        node001(e);
        break;
    default: 
        cerr << "error in MinSWRT::node017: Algo found unexpected cycle type\n";
        printCycle(e);            
        return;
    }
}    


void MinSWRT::node018(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node018" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* c, d and e are nontwisted 3-cycles. c intersects d (d.m_edges[0] 
     * between c.m_edges[0] and c.m_edges[1], others outside), e intersects d
     * (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]). 
     * There is no pair of interleaving cycles. */
    int order[3];   // needed to check cyclic sorting of three edges

    // get position of RE  0 of e relativ to c
    order[0] = f_c.m_edges[0];
    order[2] = f_c.m_edges[1];
    order[1] = f_e.m_edges[0];
    if (cyclicSorted(order))
    {
        order[1] = f_e.m_edges[1];
        if (!cyclicSorted(order))     // mutually intersecting
        {
            node021(f_c, f_d, f_e);
            return;
        }
        order[1] = f_e.m_edges[2];
        if (!cyclicSorted(order))     // mutually intersecting
        {
            node021(f_c, f_d, f_e);
            return;
        }
        node020(f_c, f_d, f_e);        
        return;;
    }
    order[0] = f_c.m_edges[1];
    order[2] = f_c.m_edges[2];
    if (cyclicSorted(order))
    {
        order[1] = f_e.m_edges[1];
        if (!cyclicSorted(order))     // mutually intersecting
        {
            node021(f_c, f_d, f_e);
            return;;
        }
        order[1] = f_e.m_edges[2];
        if (!cyclicSorted(order))     // mutually intersecting
        {
            node021(f_c, f_d, f_e);
            return;;
        }
        node020(f_c, f_d, f_e);        
        return;
    }
    order[0] = f_c.m_edges[2];
    order[2] = f_c.m_edges[0];
    order[1] = f_e.m_edges[1];
    if (!cyclicSorted(order))         // mutually intersecting
    {
        node021(f_c, f_d, f_e);
        return;
    }
    order[1] = f_e.m_edges[2];
    if (!cyclicSorted(order))     // mutually intersecting
    {
        node021(f_c, f_d, f_e);
        return;
    }
    node020(f_c, f_d, f_e);        
}
    

void MinSWRT::node019(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node019" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* c and d are nontwisted 3-cycles, e is a 1-twisted 3-cycle. There is no 
     * pair of interleaving cycles. c intersects with d (with the chords 
     * between reality-edges 0,1 and 2, 0 of both cycles). e intersects with 
     * d (at least one reality-edge of e between reality-edges 1 and 2 of d).*/
    int order[3];   // needed to check cyclic sorting of three edges

    order[0] = f_c.m_edges[0];     
    order[2] = f_c.m_edges[1];
    order[1] = f_d.m_edges[0];
    if (cyclicSorted(order))       // at least one edge of e between c.2 and c.0
    {
        order[2] = f_c.m_edges[2];
        order[1] = f_e.m_edges[0];
        if (cyclicSorted(order))   // mutually intersecting
        {
            node023(f_c, f_d, f_e);
            return;
        }
        order[1] = f_e.m_edges[1];
        if (cyclicSorted(order))   // mutually intersecting
        {
            node023(f_c, f_d, f_e);
            return;
        }
        order[1] = f_e.m_edges[2];
        if (cyclicSorted(order))   // mutually intersecting
        {
            node023(f_c, f_d, f_e);
            return;
        }
        node022(f_c, f_d, f_e);
    }
    else                           // at least one edge of e between c.0 and c.1
    {
        order[1] = f_c.m_edges[0];
        order[0] = f_e.m_edges[0];
        if (cyclicSorted(order))   // mutually intersecting
        {
            node023(f_c, f_d, f_e);
            return;
        }
        order[0] = f_e.m_edges[1];
        if (cyclicSorted(order))   // mutually intersecting
        {
            node023(f_c, f_d, f_e);
            return;
        }
        order[0] = f_e.m_edges[2];
        if (cyclicSorted(order))   // mutually intersecting
        {
            node023(f_c, f_d, f_e);
            return;
        }
        node022(f_c, f_d, f_e);
    }
}


void MinSWRT::node020(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node020" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* c, d and e are nontwisted 3-cycles. c intersects d 
     * (d.m_edges[0] between c.m_edges[0] and c.m_edges[1], others outside),
     * e intersects d (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]). 
     * There is no pair of interleaving cycles. c and e are
     * not intersecting. */
    int order[3];   // needed to check cyclic sorting of three edges
    bool leftarc;   // true if e0 in arc between c.1 and c.2
    
    m_sequence->m_numOperations = 3;          // for all sequences
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 3.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    // get position of e.0 relativ to c
    order[0] = f_c.m_edges[1];
    order[2] = f_c.m_edges[2];
    order[1] = f_e.m_edges[0];
    leftarc = cyclicSorted(order);
    // get position of e.1 and e.2 relativ to d
    order[2] = f_e.m_edges[1];
    order[0] = f_d.m_edges[2];
    if (cyclicSorted(order))      // e0, e1 between d.1 and d.2
    {
        order[1] = f_e.m_edges[2];
        order[2] = f_d.m_edges[0];
        if (cyclicSorted(order))
        {
            if (leftarc)          // edge sequence c0 d0 c1 d1 e0 e1 d2 e2 c2    
            {
#if DEBUG            
                cerr << "node020.1\n";
#endif
                m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
                m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
                m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
                m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
                return;
            }
            else                  // edge sequence c0 d0 c1 c2 d1 e0 e1 d2 e2    
            {
#if DEBUG            
                cerr << "node020.2\n";
#endif
                m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
                m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[1];
                m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];                
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
                m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];
                return;
            }                     
        }
        else if (leftarc)         // edge sequence c0 d0 c1 e2 d1 e0 e1 d2 c2    
        {
#if DEBUG            
            cerr << "node020.3\n";
#endif
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;            
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];                            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
            return;
        }
        else                      // edge sequence c0 d0 c1 c2 e2 d1 e0 e1 d2
        {
#if DEBUG            
            cerr << "node020.4\n";
#endif
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];                
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];
            return;
        }
    } 
    order[0] = f_d.m_edges[0];
    if (cyclicSorted(order))      // e1 between d2 and d0
    {
        order[1] = f_e.m_edges[2];
        if (cyclicSorted(order))
        {
            if (leftarc)          // edge sequence c0 d0 c1 d1 e2 e0 d2 e1 c2
            {
#if DEBUG            
                cerr << "node020.5\n";
#endif
                m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;            
                m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
                m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[2];
                m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];                            
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
                m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
                return;
            }
            else                  // edge sequence c0 d0 c1 c2 d1 e2 e0 d2 e1
            {
#if DEBUG            
            cerr << "node020.6\n";
#endif
                m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
                m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
                m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];                
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[2];
                m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
                m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];
                return;
            }
        }   
        else if (leftarc)         // edge sequence c0 d0 c1 d1 e0 d2 e1 e2 c2
        {
#if DEBUG            
            cerr << "node020.7\n";
#endif
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;            
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];                            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
            return;
        }
        else                      // edge sequence c0 d0 c1 c2 d1 e0 d2 e1 e2
        {
#if DEBUG            
            cerr << "node020.8\n";
#endif
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];                
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];
            return;
        }
    }                             // e1 between d0 and d1
    order[0] = f_e.m_edges[2];
    order[1] = f_d.m_edges[1];
    if (cyclicSorted(order))
    {    
        if (leftarc)              // edge sequence c0 d0 c1 e1 e2 d1 e0 d2 c2
        {
#if DEBUG            
            cerr << "node020.9\n";
#endif
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;            
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[1];                            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];
            return;
        }
        else                      // edge sequence c0 d0 c1 c2 e1 e2 d1 e0 d2
        {
#if DEBUG            
            cerr << "node020.10\n";
#endif
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];                
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];
            return;
        }
    }
    else if (leftarc)             // edge sequence c0 d0 c1 e1 d1 e2 e0 d2 c2
    {
#if DEBUG            
        cerr << "node020.11\n";
#endif
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;            
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
        m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[2];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];                            
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
        return;
    }
    else                          // edge sequence c0 d0 c1 c2 e1 d1 e2 e0 d2
    {
#if DEBUG            
            cerr << "node020.12\n";
#endif
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
        m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];                
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
        m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];
        return;
    }    
}

      
void MinSWRT::node021(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node021" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* c, d and e are nontwisted 3-cycles. c intersects d 
     * (d.m_edges[0] between c.m_edges[0] and c.m_edges[1], others outside),
     * e intersects d (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]). 
     * There is no pair of interleaving cycles. c and e are intersecting 
     * (so the cycles are mutually intersecting). */     
    int order[3];   // needed to check cyclic sorting of three edges
    
    m_sequence->m_numOperations = 3;                // for all sequences
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 3.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    
    // get position of d1 relativ to c
    order[0] = f_c.m_edges[1];
    order[2] = f_c.m_edges[2];
    order[1] = f_d.m_edges[1];
    if (cyclicSorted(order))            // d1, d2 between c1, c2
    {
        // for all possibilities
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];
        // get position of e1
        order[2] = f_e.m_edges[1];
        order[0] = f_c.m_edges[2];
        if (cyclicSorted(order))        // e1 somewhere between e0 and c2
        { 
#if DEBUG            
            cerr << "node021.1\n";
#endif
            // for all possible edge sequences
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];   
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
            return;
        }
        order[0] = f_c.m_edges[0];
        if (cyclicSorted(order))        // e1 between c2 and c0
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            if (cyclicSorted(order))    // edge sequence c0 d0 c1 d1 e0 d2 c2 e1 e2
            {
#if DEBUG            
                cerr << "node021.2\n";
#endif
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[2];   
                m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
                m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];
                return;
            }
            else                        // edge sequence c0 d0 c1 d1 e2 e0 d2 c2 e1
            {
#if DEBUG            
                cerr << "node021.3\n";
#endif
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];   
                m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
                return;            
            }
        }                               // e1 between somewhere between c0 and c1
        // for all edge sequences
#if DEBUG            
        cerr << "node021.4\n";
#endif
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];   
        m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];
        return;
    }
    else                                // d1, d2 between c2, c0
    {
        // for all sequences
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];
        // get position of e1
        order[2] = f_e.m_edges[1];
        order[0] = f_c.m_edges[0];
        if (cyclicSorted(order))        // e1 somewhere between e0 and c0
        {
#if DEBUG            
            cerr << "node021.5\n";
#endif
            // for all sequences
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[2];   
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
            return;
        }
        order[0] = f_d.m_edges[0];
        if (cyclicSorted(order))        // e1 between c0 and d0
        {            
            // get position of e2
            order[2] = f_e.m_edges[2];
            if (cyclicSorted(order))    // edge sequence c0 e1 e2 d0 c1 c2 d1 e0 d2
            {
#if DEBUG            
                cerr << "node021.6\n";
#endif
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[2];   
                m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
                m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
                return;
            }
            else                        // edge sequence c0 e1 d0 c1 c2 d1 e2 e0 d2
            {
#if DEBUG            
                cerr << "node021.7\n";
#endif
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];   
                m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
                return;
            }                                    
        }
        order[0] = f_c.m_edges[1];
        if (cyclicSorted(order))        // e1 between d0 and c1
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            if (cyclicSorted(order))    // edge sequence c0 d0 e1 e2 c1 c2 d1 e0 d2
            {
#if DEBUG            
                cerr << "node021.8\n";
#endif
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[2];   
                m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
                m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
                return;
            }
            else
            {
#if DEBUG            
                cerr << "node021.9\n";
#endif
                // for both sequences
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];   
                m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
                return;
            }
        }
        else                            // e1 between c1 and c2
        {
#if DEBUG            
            cerr << "node021.10\n";
#endif
            // for all cases
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];   
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];
            return;
        }        
    }     
}


void MinSWRT::node022(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node022" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* c and d are nontwisted 3-cycles, e is a 1-twisted 3-cycle. 
     * There is no pair of interleaving cycles. c intersects with d (with
     * the chords between reality-edges 0,1 and 2, 0 of both cycles). e 
     * intersects with d (at least one reality-edge of e between 
     * reality-edges 1 and 2 of d). c and e are not intersecting. */
    int order[3];         // needed to check cyclic sorting of three edges
    bool alphac1;         // true if first transposition does not act on c1
    int isolated;         // isolated edge of e
    int ex;               // edge of e where the first transposition acts on

    m_sequence->m_numOperations = 3;
    m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
    // search the two edges of c close to e
    order[0] = f_c.m_edges[0];
    order[1] = f_d.m_edges[0];    
    order[2] = f_c.m_edges[1];
    if (cyclicSorted(order))
    {
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
        alphac1 = true;
    }
    else
    {
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
        alphac1 = false;
    }    
    // search edge of e between d1 and d2 + isolated edge of e
    order[0] = f_d.m_edges[1];
    order[2] = f_d.m_edges[2];
    order[1] = f_e.m_edges[0];
    if (cyclicSorted(order))
    {
        ex = 0;
        order[1] = f_e.m_edges[1];
        if (cyclicSorted(order))
            isolated = 2;
        else
        {
            order[1] = f_e.m_edges[2];
            if (cyclicSorted(order))
                isolated = 1;
            else
                isolated = 0;
        }
    }
    else
    {        
        order[1] = f_e.m_edges[1];
        if (cyclicSorted(order))
        {
            ex = 1;
            order[1] = f_e.m_edges[2];
            if (cyclicSorted(order))
                isolated = 0;
            else
                isolated = 1;
        }
        else
        {
            ex = 2;
            isolated = 2;
        }
    }
    m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[ex];
    // second operation: eliminate d
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
    // search third operation
    if (isolated == ex)
    {
        if (ex == 2)        // isolated edge == twisted edge
        {
            if (alphac1)
            {
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
                m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
                m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];
            }
            else
            {
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
                m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
                m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
            }            
        }
        else
        {
            if (alphac1)
            {
                m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
                m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[(ex + 2) % 3];
                if (m_sequence->m_operations[2].m_edges[2] != f_e.m_edges[2])
                    m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                else
                    m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            }
            else
            {
                m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
                m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[ex + 1];     // ex < 2
                if (m_sequence->m_operations[2].m_edges[2] != f_e.m_edges[2])
                    m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                else
                    m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;                
            }            
        }
    }
    else
    {
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];
        if (ex == 0)
        {
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            if (isolated == 1)
            {
                if (alphac1)
                    m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
                else
                    m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
            }
            else  // isolated == 2
            {
                if (alphac1)
                    m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
                else
                    m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
            }
        }
        else      // ex == 1
        {
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];
            if (isolated == 0)
            {
                if (alphac1)
                    m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
                else
                    m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
            }
            else  // isolated == 2
            {
                if (alphac1)
                    m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[2];
                else
                    m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            }
        }
    }
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 3.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}
    

void MinSWRT::node023(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node023" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* Node 023: c and d are nontwisted 3-cycles, e is a 1-twisted 3-cycle. 
     * There is no pair of interleaving cycles. c intersects with d (with
     * the chords between reality-edges 0,1 and 2, 0 of both cycles). e 
     * intersects with d (at least one reality-edge of e between 
     * reality-edges 1 and 2 of d). c and e are intersecting (so the cycles
     * are mutually intersecting). */
    int order[3];         // needed to check cyclic sorting of three edges
    bool mirror = false;  // true if we work with mirrored configuration
    bool alphac1;         // true if first transposition does not act on c1
    int ex;               // edge of e where the first transposition acts on
    bool beta, gamma;     // position of the other two edges of e; true if
                          // between ca, cb

    // search chord through center of the configuration
    order[0] = f_c.m_edges[0];
    order[1] = f_d.m_edges[0];
    order[2] = f_c.m_edges[2];
    alphac1 = cyclicSorted(order);
    // search third edge of first transposition
    order[0] = f_d.m_edges[1];
    order[2] = f_d.m_edges[2];
    order[1] = f_e.m_edges[0];
    if (cyclicSorted(order))
        ex = 0;
    else
    {
        order[1] = f_e.m_edges[1];
        if (cyclicSorted(order))
            ex = 1;
        else
            ex = 2;
    }
    // check if we need to mirror the configuration
    order[1] = f_d.m_edges[0];
    order[0] = f_e.m_edges[(ex + 1) % 3];
    if (cyclicSorted(order))
        mirror = true;
    else
    {
        order[0] = f_e.m_edges[(ex + 2) % 3];
        mirror = cyclicSorted(order);
    }
    if (mirror)
    {
        // mirror configuration
        order[0] = f_c.m_edges[1];   // use as temporary variable
        f_c.m_edges[0] = m_simplePerm->size() - f_c.m_edges[0];
        f_c.m_edges[1] = m_simplePerm->size() - f_c.m_edges[2];
        f_c.m_edges[2] = m_simplePerm->size() - order[0];
        order[0] = f_d.m_edges[1];   // use as temporary variable
        f_d.m_edges[0] = m_simplePerm->size() - f_d.m_edges[0];
        f_d.m_edges[1] = m_simplePerm->size() - f_d.m_edges[2];
        f_d.m_edges[2] = m_simplePerm->size() - order[0];
        order[0] = f_e.m_edges[0];   // use as temporary variable
        f_e.m_edges[0] = m_simplePerm->size() - f_e.m_edges[1];
        f_e.m_edges[1] = m_simplePerm->size() - order[0];
        f_e.m_edges[2] = m_simplePerm->size() - f_e.m_edges[2];
        // solve mirrored configuration
        node023(f_c, f_d, f_e);
        // re-mirror operations
        order[0] = m_sequence->m_operations[0].m_edges[0];  // use as temporary variable
        m_sequence->m_operations[0].m_edges[0] = m_simplePerm->size() - m_sequence->m_operations[0].m_edges[1];
        m_sequence->m_operations[0].m_edges[1] = m_simplePerm->size() - order[0];
        m_sequence->m_operations[0].m_edges[2] = m_simplePerm->size() - m_sequence->m_operations[0].m_edges[2];
        order[0] = m_sequence->m_operations[1].m_edges[0];  // use as temporary variable
        m_sequence->m_operations[1].m_edges[0] = m_simplePerm->size() - m_sequence->m_operations[1].m_edges[1];
        m_sequence->m_operations[1].m_edges[1] = m_simplePerm->size() - order[0];
        m_sequence->m_operations[1].m_edges[2] = m_simplePerm->size() - m_sequence->m_operations[1].m_edges[2];
        order[0] = m_sequence->m_operations[2].m_edges[0];  // use as temporary variable
        m_sequence->m_operations[2].m_edges[0] = m_simplePerm->size() - m_sequence->m_operations[2].m_edges[1];
        m_sequence->m_operations[2].m_edges[1] = m_simplePerm->size() - order[0];
        m_sequence->m_operations[2].m_edges[2] = m_simplePerm->size() - m_sequence->m_operations[2].m_edges[2];        
        return;    
    }    
    // write first operation
    m_sequence->m_numOperations = 3;
    m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
    if (alphac1)
    {
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
    }
    else
    {
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
    }
    m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[ex];
    // second operation: eliminate d
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
    // check position of other edges of e
    order[0] = m_sequence->m_operations[0].m_edges[0];
    order[2] = m_sequence->m_operations[0].m_edges[1];
    order[1] = f_e.m_edges[(ex + 2) % 3];
    beta = cyclicSorted(order);
    order[1] = f_e.m_edges[(ex + 1) % 3];
    gamma = cyclicSorted(order);
    if (beta && gamma)
    {
        // check if before alpha
        if (alphac1)
            order[2] = f_c.m_edges[1];
        else
            order[2] = f_c.m_edges[2];
        if (cyclicSorted(order))   // both before alpha
        {
            m_sequence->m_operations[2].m_edges[0] = order[2];                   // alpha
            m_sequence->m_operations[2].m_edges[1] = m_sequence->m_operations[0].m_edges[0]; // ca
            if (ex == 2)
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];         // gamma
            else    
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[(ex+2) % 3];// beta
        }    
        else                       // both after alpha
        {
            m_sequence->m_operations[2].m_edges[0] = m_sequence->m_operations[0].m_edges[1]; // cb
            m_sequence->m_operations[2].m_edges[1] = order[2];                   // alpha
            if (ex == 2)
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];         // beta
            else    
                m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[ex+1];      // gamma
        }
        if ((ex == 2) || (m_sequence->m_operations[2].m_edges[2] == f_e.m_edges[2]))
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        else
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
    }
    else if (beta)  // beta, !gamma
    {
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        if (ex == 0)
        {
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];             // gamma
            m_sequence->m_operations[2].m_edges[1] = m_sequence->m_operations[0].m_edges[0]; // ca
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];             // beta
        }
        else if (ex == 1)
        {
            m_sequence->m_operations[2].m_edges[0] = m_sequence->m_operations[0].m_edges[0]; // ca
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];             // beta
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];             // gamma
        }
        else      // ex == 2
        {
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];             // beta
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];             // gamma
            m_sequence->m_operations[2].m_edges[2] = m_sequence->m_operations[0].m_edges[0]; // ca
        }        
    }
    else            // gamma, !beta
    {
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        if (ex == 0)
        {
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];             // gamma
            m_sequence->m_operations[2].m_edges[1] = m_sequence->m_operations[0].m_edges[1]; // cb
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];             // beta
        }
        else if (ex == 1)
        {
            m_sequence->m_operations[2].m_edges[0] = m_sequence->m_operations[0].m_edges[1]; // cb
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];             // beta
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];             // gamma
        }
        else     // ex == 2
        {
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];             // beta
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];             // gamma
            m_sequence->m_operations[2].m_edges[2] = m_sequence->m_operations[0].m_edges[1]; // cb
        }
    }
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 3.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}


void MinSWRT::node024(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node024" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif

    /* c and d are two interleaving 1-twisted 3-cycles. */
    int order[3];     // needed to check cyclic sorting of three edges
    bool arc1, arc2;  // true if an edge is in the arc
    
    order[0] = f_c.m_edges[2];
    order[2] = f_d.m_edges[2];
    order[1] = f_c.m_edges[0];
    arc1 = cyclicSorted(order);
    order[1] = f_d.m_edges[0];
    arc2 = !cyclicSorted(order);
    if (arc1 && arc2)
    {
        node036(f_c, f_d);
        return;
    }
    if (arc1)
        node027(f_c, f_d);
    else
        node027(f_d, f_c);
}


void MinSWRT::node025(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node025" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif

    /* c and d are non-interleaving 1-twisted 3-cycles. d intersects 
     * the nontwisted chord of c. */
    int order[3];     // needed to check cyclic sorting of three edges
    bool ord0, ord1;  // true if d0, d1 between c0, c1

    order[0] = f_c.m_edges[0];
    order[2] = f_c.m_edges[1];
    order[1] = f_d.m_edges[0];
    ord0 = cyclicSorted(order);
    order[1] = f_d.m_edges[1];
    ord1 = cyclicSorted(order);
    if (ord0 && !ord1)       // nontwisted chord of d intersected by c
        node028(f_c, f_d);
    else if (!ord0 && ord1)  // nontwisted chord of d intersected by c
        node028(f_d, f_c);
    else
        node029(f_c, f_d);
}


void MinSWRT::node026(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node026" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif
    
    /* c is a 1-twisted 3-cycle, d is a nontwisted 3-cycle. d intersects the 
     * nontwisted chord of c.  The chord of d that is not intersected by c 
     * lies between the reality edges m_edges[1] and m_edges[2]. */
    Cycle e;          // intersects open arc of d
    int order[3];     // needed to check cyclic sorting of three edges
    
    m_simplePerm->getCycle(&e, 1, f_d.m_edges[1], f_d.m_edges[2]);
    switch (e.m_type)
    {
    case Cycle::CYCLE_2ORIENTED: 
        node000(e);
        break;        
    case Cycle::CYCLE_2UNORIENTED:
        node030(f_c, f_d, e);
        break;        
    case Cycle::CYCLE_0TWISTED: 
        if (interleaving(f_d, e))
        {
            node016(f_d, e);
            break;
        }
        if (interleaving(f_c, e))
        {
            order[0] = e.m_edges[1];
            order[2] = e.m_edges[2];
            order[1] = f_c.m_edges[0];
            if (cyclicSorted(order))
            {
                e.m_edges[2] = e.m_edges[1];    
                e.m_edges[1] = e.m_edges[0];    
                e.m_edges[0] = order[2];
            }
            else
            {
                order[1] = f_c.m_edges[1];
                if (cyclicSorted(order))
                {
                    e.m_edges[2] = e.m_edges[0];    
                    e.m_edges[0] = e.m_edges[1];    
                    e.m_edges[1] = order[2];
                }
            }    // else no shift necessary                
            node040(e, f_c); 
            break;    
        }
        // shift e
        order[0] = f_d.m_edges[1];
        order[2] = f_d.m_edges[2];
        order[1] = e.m_edges[1];
        if (cyclicSorted(order))     // isolated edge: e2
        {
            e.m_edges[1] = e.m_edges[0];
            e.m_edges[0] = e.m_edges[2];
            e.m_edges[2] = order[1];   
            order[1] = e.m_edges[0];  // store edge outside d1-d2
        }
        else
        {
            order[1] = e.m_edges[2];
            if (cyclicSorted(order)) // isolated edge: e1
            {
                e.m_edges[2] = e.m_edges[0];
                e.m_edges[0] = e.m_edges[1];
                e.m_edges[1] = order[1];                
                order[1] = e.m_edges[0];  // store edge outside d1-d2                
            }  // else: isolated edge: e0; no shift necessary
            else
                order[1] = e.m_edges[1];  // store edge outside d1-d2                
        }
        // shift d
        order[2] = f_d.m_edges[0];
        if (cyclicSorted(order))      // edge 0-1 of d not intersected by e
        {
            f_d.m_edges[0] = f_d.m_edges[2];    
            f_d.m_edges[2] = f_d.m_edges[1];    
            f_d.m_edges[1] = order[2];    
        }
        else                          // edge 2-0 of d not intersected by e
        {
            f_d.m_edges[0] = f_d.m_edges[1];    
            f_d.m_edges[1] = f_d.m_edges[2];    
            f_d.m_edges[2] = order[2];    
        }                    
        node019(e, f_d, f_c);
        break;        
    case Cycle::CYCLE_1TWISTED: 
        if (interleaving(f_d, e))
        {
            order[0] = f_d.m_edges[1];
            order[2] = f_d.m_edges[2];
            order[1] = e.m_edges[0];
            if (cyclicSorted(order))
            {
                f_d.m_edges[2] = f_d.m_edges[1];    
                f_d.m_edges[1] = f_d.m_edges[0];    
                f_d.m_edges[0] = order[2];
            }
            else
            {
                order[1] = e.m_edges[1];
                if (cyclicSorted(order))
                {
                    f_d.m_edges[2] = f_d.m_edges[0];    
                    f_d.m_edges[0] = f_d.m_edges[1];    
                    f_d.m_edges[1] = order[2];
                }
            }    // else no shift necessary                            
            node040(f_d, e);
            break;    
        }
        if (interleaving(f_c, e))
        {
            node024(f_c, e);
            break;    
        }        
        node031(f_c, f_d, e);
        break;
    case Cycle::CYCLE_2TWISTED: 
        node002(e);
        break;
    case Cycle::CYCLE_3TWISTED: 
        node001(e);
        break;
    default: 
        cerr << "error in MinSWRT::node026: Algo found unexpected cycle type\n";
        printCycle(e);            
        return;
    }    
}


void MinSWRT::node027(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node027" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif

    /* c and d are two 1-twisted 3-cycles that form a 1-twisted pair. The 
     * twist of c is left (counterclockwise after) the twist of d. */
    Cycle e;          // intersects the adjacent arcs
    int order1[3];    // search edges in left arc
    int order2[3];    // search edges in right arc
    int order3[3];    // search edges outside the arcs
    int a1 = -1, a2 = -1, a3 = -1, a4 = -1;   // edge index of e in 
                      // arc 1(left), 2(right), 3(down), 4(up); no edge -> -1
        
    m_simplePerm->getCycle(&e, 2, f_c.m_edges[2], f_d.m_edges[0], 
      f_c.m_edges[1], f_d.m_edges[2]);
    // check trivial cases
    switch (e.m_type)
    {
    case Cycle::CYCLE_2ORIENTED:
        node000(e);
        return;
    case Cycle::CYCLE_2TWISTED:
        node002(e);
        return;
    case Cycle::CYCLE_3TWISTED: 
        node001(e);
        return;
    default:               // no trivial case
        break;
    }  
    m_sequence->m_numOperations = 3;   // for all cases
    // check edge positions of cycle e
    order1[0] = f_c.m_edges[2];
    order1[2] = f_d.m_edges[0];
    order2[0] = f_c.m_edges[1];
    order2[2] = f_d.m_edges[2];
    order3[0] = f_d.m_edges[0];
    order3[2] = f_c.m_edges[1];  
    for (int i = 0; (e.m_type == Cycle::CYCLE_2UNORIENTED? 
      (i < 2) : (i < 3)); i++)
    {
        order1[1] = e.m_edges[i];
        if (cyclicSorted(order1))
            a1 = i;
        else
        {
            order2[1] = e.m_edges[i];
            if (cyclicSorted(order2))
                a2 = i;
            else
            {
                order3[1] = e.m_edges[i];
                if (cyclicSorted(order3))
                    a3 = i;
                else
                    a4 = i;
            }
        }                    
    } 
    // check case: intersection with nontwisted edge
    if (a3 != -1)
    {
        m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
        if (a1 != -1)
        {
            m_sequence->m_operations[0].m_edges[0] = e.m_edges[a1];        
            m_sequence->m_operations[0].m_edges[1] = e.m_edges[a3];        
            // search exact position of intersecting edge
            order3[1] = e.m_edges[a3];
            order3[2] = f_c.m_edges[0];
            if (cyclicSorted(order3))    // edge before c0
            {
                m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
                m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
                m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[0];
                m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
                m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];
            }
            else                        // edge after c0
            {
                m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
                m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[2];
                m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[0];
                m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[1];
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
                m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[2];
                m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[0];
                m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[1];
            }
        }
        else
        {
            m_sequence->m_operations[0].m_edges[0] = e.m_edges[a3];        
            m_sequence->m_operations[0].m_edges[1] = e.m_edges[a2];        
            // search exact position of intersecting edge
            order3[1] = e.m_edges[a3];
            order3[0] = f_d.m_edges[1];
            if (cyclicSorted(order3))    // edge after d1
            {
                m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
                m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[1];
                m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[2];
                m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[0];
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
                m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[0];
                m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[1];      
            }
            else                        // edge before d1
            {
                m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
                m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
                m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
                m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];
                m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
                m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
                m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
                m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];                      
            }
        }
        // calculate ratio: if reversal splits 1-twisted 3-cycle, ratio is better
        if ((e.m_type == Cycle::CYCLE_1TWISTED) 
          && ((e.m_edges[2] == m_sequence->m_operations[0].m_edges[0]) 
           || (e.m_edges[2] == m_sequence->m_operations[0].m_edges[1])))
            m_sequence->m_dsigma = 6.0f - 2.0f * m_wr / m_wt;           
        else
            m_sequence->m_dsigma = 4.0f;
        m_sequence->m_weight = m_wr + 2.0f * m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
        return;
    }
    // check case: e is 2-cycle, intersecting with a twisted edge
    if (e.m_type == Cycle::CYCLE_2UNORIENTED)
    {
        if (a1 != -1)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[1];
        }
        else
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];
        }
        m_sequence->m_operations[2].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[2].m_edges[0] = e.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = e.m_edges[1];
        m_sequence->m_dsigma = 2.0f + 2.0f * m_wr / m_wt;
        m_sequence->m_weight = 2.0f * m_wr + m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
        return;
    }
    // e is t-unoriented 3-cycle
    if (e.m_type == Cycle::CYCLE_0TWISTED)
    {
        m_sequence->m_dsigma = 4.0f;
        m_sequence->m_weight = 3.0f * m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
        if ((a1 != -1) && (a2 != -1))
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = e.m_edges[a2];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = e.m_edges[a4];
            return;    
        }
        if (a1 == 0)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = e.m_edges[2];
            return;    
        }
        if (a1 == 2)    // shift e
        {
            order1[0] = e.m_edges[0];
            e.m_edges[0] = e.m_edges[2];
            e.m_edges[2] = e.m_edges[1];
            e.m_edges[1] = order1[0];
            a1 = 1;            
        }               
        if (a1 == 1)
        {            
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = e.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = e.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = e.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[1];
            return;
        }
        if (a2 == 0)
        {            
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = e.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = e.m_edges[1];
            return;                
        }
        if (a2 == 2)    // shift e
        {
            order1[0] = e.m_edges[0];
            e.m_edges[0] = e.m_edges[2];
            e.m_edges[2] = e.m_edges[1];
            e.m_edges[1] = order1[0];
            a2 = 1;            
        }               // a2 now == 1
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[0].m_edges[0] = e.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[2].m_edges[0] = e.m_edges[1];
        m_sequence->m_operations[2].m_edges[1] = e.m_edges[2];
        m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];
        return;
    }
    if (e.m_type == Cycle::CYCLE_1TWISTED)
    {
        // check if nontwisted chord of e is intersected
        order1[0] = e.m_edges[0];
        order1[2] = e.m_edges[1];
        order1[1] = f_c.m_edges[2];
        order2[0] = e.m_edges[0];
        order2[2] = e.m_edges[1];
        order2[1] = f_c.m_edges[0];
        if (cyclicSorted(order1) ^ cyclicSorted(order2))
        {
            node029(e, f_c);
            return;
        }
        order1[1] = f_d.m_edges[2];
        order2[1] = f_d.m_edges[0];
        if (cyclicSorted(order1) ^ cyclicSorted(order2))
        {
            node029(e, f_d);
            return;
        }
        // four possible configurations
        m_sequence->m_dsigma = 4.0f;
        m_sequence->m_weight = 3.0f * m_wt;
        m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
        if (a1 == 1)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = e.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = e.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[0];            
            return;
        }
        if (a1 == 2)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = e.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = e.m_edges[0];            
            return;    
        }
        if (a2 == 1)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = e.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = e.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];                        
            return;
        }   // a2 == 2
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[0].m_edges[0] = e.m_edges[2];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
        m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
        m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
        m_sequence->m_operations[2].m_edges[2] = e.m_edges[1];            
        return;
    }
    cerr << "error in MinSWRT::node027: Algo found unexpected cycle type\n";
    printCycle(e);            
}


void MinSWRT::node028(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node028" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif

    /* c, d are intersecting, non-interleaving 1-twisted 3-cycles. 
     * Both cycles intersect the nontwisted chord of the other cycle. 
     * Reality-edge m_edges[0] of cycle d lies between reality-edges
     * m_edges[0] and m_edges[1] of cycle c. */
    int order[3];     // needed to check cyclic sorting of three edges

    // for all configurations
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 2.0f * m_wr + m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
    m_sequence->m_numOperations = 3;
    m_sequence->m_operations[2].m_type = Operation::OPERATION_REVERSAL;
    m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[1];
    // find configurations
    order[0] = f_c.m_edges[0];
    order[1] = f_d.m_edges[2];
    order[2] = f_c.m_edges[1];
    if (cyclicSorted(order))          // d2 between c0, c1
    {
        m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[1];
        m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[2];
        order[0] = f_d.m_edges[1];
        order[2] = f_c.m_edges[2];
        if (cyclicSorted(order))      // edge sequence c0 d2 d0 c1 c2 d1
        {
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[1];
            return;    
        }
        else                          // edge sequence c0 d2 d0 c1 d1 c2
        {
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[0];
            return;
        }        
    }
    else
    {
        m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[2];
        m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[0];
        order[0] = f_d.m_edges[0];
        order[2] = f_c.m_edges[2];
        if (cyclicSorted(order))      // edge sequence c0 d0 c1 d1 d2 c2
        {
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[0];
            return;    
        }
        else                          // edge sequence c0 d0 c1 c2 d1 d2
        {
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_edges[2] = f_c.m_edges[1];
            return;
        }        
    }
}


void MinSWRT::node029(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node029" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif
    
    /* c, d are intersecting, non-interleaving 1-twisted 3-cycles.
     * d intersects the nontwisted chord of c, but c doesn't intersect the
     * nontwisted chord of d. */
    Cycle e;          // intersects open arc of d
    int order[3];     // needed to check cyclic sorting of three edges

    m_simplePerm->getCycle(&e, 1, f_d.m_edges[0], f_d.m_edges[1]);
    // check trivial cases
    switch (e.m_type)
    {
    case Cycle::CYCLE_2ORIENTED: 
        node000(e);
        return;
    case Cycle::CYCLE_2UNORIENTED:    
        node007(e, f_d);
        return;
    case Cycle::CYCLE_2TWISTED: 
        node002(e);
        return;
    case Cycle::CYCLE_3TWISTED: 
        node001(e);
        return;
    default: 
        break;
    }
    m_sequence->m_numOperations = 3;
    // search edges intersecting with open chord -> first operation
    m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
    order[0] = f_d.m_edges[0];
    order[2] = f_d.m_edges[1];
    for (int i = 0; i < 3; i++)   // e is 3-cycle
    {
        order[1] = e.m_edges[i];
        if (cyclicSorted(order))
            m_sequence->m_operations[0].m_edges[0] = e.m_edges[i];
        else    
            m_sequence->m_operations[0].m_edges[1] = e.m_edges[i];
    }
    // is d2 in first reversal?
    order[0] = m_sequence->m_operations[0].m_edges[0];
    order[1] = f_d.m_edges[2];
    order[2] = m_sequence->m_operations[0].m_edges[1];
    if (cyclicSorted(order))
    {
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
    }
    else
    {
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];
    }        
    order[0] = f_c.m_edges[0];
    order[2] = f_c.m_edges[1];
    order[1] = f_d.m_edges[2];
    if (cyclicSorted(order)) // twisted edge of d between nontwisted of c
    {
        // are c1, c2 adjacent on the circle?
        order[0] = f_c.m_edges[1];
        order[1] = f_d.m_edges[0];
        order[2] = f_c.m_edges[2];
        if (cyclicSorted(order))    // no, d0, d1 between c1, c2
        {
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];
        }
        else                        // yes, c1, c2 adjacent
        {
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];
        }
    }
    else                     // twisted edge of d not between nontwisted of c
    {
        // are c1, c2 adjacent on the circle?
        order[0] = f_c.m_edges[1];
        order[2] = f_c.m_edges[2];
        if (cyclicSorted(order))    // no, d2 between c1, c2
        {
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];
        }
        else                        // yes, c1, c2 adjacent
        {
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];
        }        
    }
    // check ratio - if first reversal splits a cycle, ratio is higher
    if ((e.m_type == Cycle::CYCLE_1TWISTED) 
      && ((e.m_edges[2] == m_sequence->m_operations[0].m_edges[0]) 
       || (e.m_edges[2] == m_sequence->m_operations[0].m_edges[1])))
        m_sequence->m_dsigma = 6.0f - 2.0f * m_wr / m_wt;   
    else
        m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = m_wr + 2.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;    
}


void MinSWRT::node030(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node030" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* Node 030: c is 1-twisted 3-cycle, d is nontwisted 3-cycle, e is 
     * r-unoriented 2-cycle. d intersects the nontwisted chord of c, c does
     * not intersect the chord between m_edges[1] and m_edges[2] of d. This 
     * chord is intersected by e (with m_edges[0] between the reality-edges. */
    int order[3];     // needed to check cyclic sorting of three edges
    bool e0, e1;      // true if e0, e1 is between c0, c1
    
    order[0] = f_c.m_edges[0];
    order[2] = f_c.m_edges[1];
    order[1] = f_e.m_edges[0];
    e0 = cyclicSorted(order);
    order[1] = f_e.m_edges[1];
    e1 = cyclicSorted(order);
    if (e0 && !e1)
    {
        node007(f_e, f_c);
        return;
    }
    if (!e0 && e1)
    {
        f_e.m_edges[1] = f_e.m_edges[0];
        f_e.m_edges[0] = order[1];
        node007(f_e, f_c);
        return;
    }
    if (e0 && e1)
        node032(f_c, f_d, f_e); 
    else    
        node033(f_c, f_d, f_e); 
}
    

void MinSWRT::node031(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node031" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* Node 031: c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d 
     * intersects the nontwisted chord of c, c does not intersect the chord 
     * between m_edges[1] and m_edges[2] of d. This chord is intersected by 
     * e. There is no pair of interleaving cycles. */
    int order[3];     // needed to check cyclic order of three edges
    bool e0, e1, e2;  // true if e0, e1, e2 is in a given interval
    
    // check intersection between c, e
    order[0] = f_c.m_edges[0];
    order[2] = f_c.m_edges[1];
    order[1] = f_e.m_edges[0];
    e0 = cyclicSorted(order);
    order[1] = f_e.m_edges[1];
    e1 = cyclicSorted(order);
    if (e0 != e1)      // c and e are intersecting
    {
        node035(f_c, f_d, f_e);    
        return;
    }
    order[1] = f_e.m_edges[2];
    e2 = cyclicSorted(order);
    if (e0 != e2)      // c and e are intersecting
    {
        node035(f_c, f_d, f_e);            
        return;
    }
    order[0] = f_c.m_edges[1];
    order[2] = f_c.m_edges[2];
    e2 = cyclicSorted(order);
    order[1] = f_e.m_edges[0];
    e0 = cyclicSorted(order);
    if (e0 != e2)      // c and e are intersecting
    {
        node035(f_c, f_d, f_e);            
        return;
    }
    order[1] = f_e.m_edges[1];
    e1 = cyclicSorted(order);
    if (e0 != e1)      // c and e are intersecting
    {
        node035(f_c, f_d, f_e);            
        return;
    }
    // c and e are not intersecting            
    node034(f_c, f_d, f_e); 
}
    

void MinSWRT::node032(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node032" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* c is 1-twisted 3-cycle, d is nontwisted 3-cycle, e is r-unoriented 
     * 2-cycle. d intersects the nontwisted chord of c. d.m_edges[1] and 
     * d.m_edges[2] lie between c.m_edges[0] and c.m_edges[1]. e intersects d 
     * (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]), but does not 
     * intersect c. */
    int order[3];     // needed to check cyclic order of three edges
    
    // for all configurations
    m_sequence->m_numOperations = 4;
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
    // check position of d0
    order[0] = f_c.m_edges[1];
    order[1] = f_d.m_edges[0];
    order[2] = f_c.m_edges[2];
    if (cyclicSorted(order))        // d0 between c1, c2
    {
        // check position of e1
        order[0] = f_d.m_edges[1];
        order[1] = f_e.m_edges[1];
        if (cyclicSorted(order))    // edge sequence d0 c0 c0 d1 e0 d2 d1 c1
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[3].m_edges[1] = f_e.m_edges[0];
        }
        else                        // edge sequence d0 c2 c0 e1 d1 e0 d2 c1
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[3].m_edges[1] = f_e.m_edges[0];
        }        
    }
    else                            // d0 between c2, c0
    {
        // check position of e1
        order[0] = f_d.m_edges[1];
        order[1] = f_e.m_edges[1];
        if (cyclicSorted(order))    // edge sequence d0 c0 d1 e0 d2 e1 c1 c2
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[3].m_edges[1] = f_e.m_edges[0];
        }
        else                        // edge sequence d0 c0 e1 d1 e0 d2 c1 c2
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[3].m_edges[1] = f_e.m_edges[0];
        }
    }    
    m_sequence->m_dsigma = 4.0f + 2.0f * m_wr / m_wt;
    m_sequence->m_weight = m_wr + 3 * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}
         

void MinSWRT::node033(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node033" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* c is 1-twisted 3-cycle, d is nontwisted 3-cycle, e is r-unoriented 
     * 2-cycle. d intersects the nontwisted chord of c. d.m_edges[1] and 
     * d.m_edges[2] are outside of arc c.m_edges[0], c.m_edges[1]. e 
     * intersects d (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]), but 
     * does not intersect c. */
    int order[3];     // needed to check cyclic order of three edges

    // for all configurations
    m_sequence->m_numOperations = 4;
    // check position of d1, dd2
    order[0] = f_c.m_edges[1];
    order[1] = f_d.m_edges[1];
    order[2] = f_c.m_edges[2];
    if (cyclicSorted(order))        // d1, d2, between c1, c2
    {
        // check position of e1
        order[1] = f_e.m_edges[1];
        order[2] = f_d.m_edges[1];
        if (cyclicSorted(order))   // edge sequence d0 c1 e1 d1 e0 d2 c2 c0
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[3].m_edges[1] = f_e.m_edges[1];            
            m_sequence->m_dsigma = 4.0f + 2.0f * m_wr / m_wt;
            m_sequence->m_weight = m_wr + 3.0f * m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }
        order[2] = f_c.m_edges[2];
        if (cyclicSorted(order))   // edge sequence d0 c1 d1 e0 d2 e1 c2 c0 
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[3].m_edges[1] = f_e.m_edges[1];  
            m_sequence->m_dsigma = 4.0f + 2.0f * m_wr / m_wt;
            m_sequence->m_weight = m_wr + 3.0f * m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }
        else                       // edge sequence d0 c1 d1 e0 d2 c2 e1 c0 
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[2];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[3].m_edges[1] = f_c.m_edges[1];            
            m_sequence->m_dsigma = 4.0f + 2.0f * m_wr / m_wt;
            m_sequence->m_weight = 3.0f * m_wr + m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }
    }
    else                            // d1, d2 between c2, c0
    {
        // check position of e1
        order[1] = f_e.m_edges[1];
        order[2] = f_c.m_edges[2];
        if (cyclicSorted(order))   // edge sequence d0 c1 e1 c2 d1 e0 d2 c0
        {            
            m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[1].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[3].m_edges[1] = f_c.m_edges[1];            
            m_sequence->m_dsigma = 4.0f + 2.0f * m_wr / m_wt;
            m_sequence->m_weight = 3.0f * m_wr + m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }
        order[2] = f_d.m_edges[1];
        if (cyclicSorted(order))   // edge sequence d0 c1 c2 e1 d1 e0 d2 c0 
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[0];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[3].m_edges[1] = f_e.m_edges[1];            
            m_sequence->m_dsigma = 4.0f + 2.0f * m_wr / m_wt;
            m_sequence->m_weight = m_wr + 3.0f * m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }
        else                       // edge sequence d0 c1 c2 e1 d1 e0 d2 c0 
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[1];
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
            m_sequence->m_operations[3].m_type = Operation::OPERATION_REVERSAL;
            m_sequence->m_operations[3].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[3].m_edges[1] = f_e.m_edges[1];            
            m_sequence->m_dsigma = 4.0f + 2.0f * m_wr / m_wt;
            m_sequence->m_weight = m_wr + 3.0f * m_wt;
            m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
            return;
        }        
    }     
}    
   

void MinSWRT::node034(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node034" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d intersects the 
     * nontwisted chord of c, c does not intersect the chord between 
     * m_edges[1] and m_edges[2] of d. This chord is intersected by e. c and 
     * e are not intersecting. */
    int order[3];     // needed to check cyclic order of three edges
    
    // check if d intersects the nontwisted chord of e
    order[0] = f_e.m_edges[0];
    order[2] = f_e.m_edges[1];
    order[1] = f_d.m_edges[1];
    if (cyclicSorted(order))       // d intersects nontwisted chord of e
    {
        node037(f_c, f_d, f_e);
        return;
    }
    order[1] = f_d.m_edges[2];
    if (cyclicSorted(order))       // d intersects nontwisted chord of e
    {
        node037(f_c, f_d, f_e);
        return;
    }
    node038(f_c, f_d, f_e);        
}


void MinSWRT::node035(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node035" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d intersects the 
     * nontwisted chord of c, c does not intersect the chord between 
     * m_edges[1] and m_edges[2] of d. This chord is intersected by e. There 
     * is no pair of interleaving cycles. c and e are intersecting. */
    int order[3];     // needed to check cyclic order of three edges

    // intersects e the nontwisted chord of c?
    order[0] = f_c.m_edges[0];
    order[2] = f_c.m_edges[1];     
    order[1] = f_e.m_edges[0];
    if (cyclicSorted(order))         // e intersects nontwisted chord of c
    {
        node025(f_c, f_e);
        return;    
    }
    order[1] = f_e.m_edges[1];
    if (cyclicSorted(order))         // e intersects nontwisted chord of c
    {
        node025(f_c, f_e);
        return;    
    }
    order[1] = f_e.m_edges[2];
    if (cyclicSorted(order))         // e intersects nontwisted chord of c
    {
        node025(f_c, f_e);
        return;    
    }
    // intersects c the nontwisted chord of e?
    order[0] = f_e.m_edges[0];
    order[2] = f_e.m_edges[1];     
    order[1] = f_c.m_edges[0];
    if (cyclicSorted(order))         // c intersects nontwisted chord of e,
    {                                // but not vice versa
        node029(f_e, f_c);
        return;    
    }
    order[1] = f_c.m_edges[1];
    if (cyclicSorted(order))         // e intersects nontwisted chord of c
    {                                // but not vice versa
        node029(f_e, f_c);
        return;    
    }
    order[1] = f_c.m_edges[2];
    if (cyclicSorted(order))         // e intersects nontwisted chord of c
    {                                // but not vice versa
        node029(f_e, f_c);
        return;    
    }
    node039(f_c, f_d, f_e);        
}


void MinSWRT::node036(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node036" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif

    /* c and d are two interleaving 1-twisted 3-cycles that do not form a 
     * 1-twisted pair. */
    m_sequence->m_numOperations = 3;
    m_sequence->m_operations[0].m_type = Operation::OPERATION_REVERSAL;
    m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
    m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];
    m_sequence->m_operations[2].m_type = Operation::OPERATION_REVERSAL;
    m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 2.0f * m_wr + m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}


void MinSWRT::node037(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node037" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d intersects the 
     * nontwisted chord of c, c does not intersect the chord between 
     * m_edges[1] and m_edges[2] of d. This chord is intersected by a 
     * nontwisted chord of e. c and e are not intersecting. */
    int order[3];     // needed to check cyclic order of three edges
    int separatedc;   // edge of c separated from the others
    int separatede;   // edge of e separated from the others
    bool reverted;    // true if twisted edge is reverted
    
    m_sequence->m_numOperations = 4;
    // find arc with edges from c and e
    order[0] = f_d.m_edges[2];
    order[2] = f_d.m_edges[0];
    order[1] = f_e.m_edges[0];
    if (cyclicSorted(order))        // arc between d2, d0
    {
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[2];
        m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[0];
        m_sequence->m_operations[0].m_edges[2] = f_d.m_edges[1];
        // search separated edge
        order[1] = f_e.m_edges[2];
        if (cyclicSorted(order))
            separatede = 1;
        else
            separatede = 0;
    }
    else
    {
        order[1] = f_e.m_edges[1];
        if (cyclicSorted(order))        // arc between d2, d0
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_d.m_edges[1];
            // search separated edge
            order[1] = f_e.m_edges[2];
            if (cyclicSorted(order))
                separatede = 0;
            else
                separatede = 1;
        }
        else                            // arc between d0, d1
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_d.m_edges[2];
            // search separated edge
            order[0] = f_e.m_edges[1];
            order[2] = f_e.m_edges[2];
            order[1] = f_d.m_edges[0];
            if (cyclicSorted(order))
                separatede = 1;
            else
            {
                order[1] = f_d.m_edges[1];
                if (cyclicSorted(order))
                    separatede = 1;
                else
                    separatede = 0;
            }            
        }
    }
    // check if e2 is reverted in the first move
    order[0] = m_sequence->m_operations[0].m_edges[0];
    order[2] = m_sequence->m_operations[0].m_edges[1];
    order[1] = f_e.m_edges[2];
    reverted = cyclicSorted(order);
    // second move
    if (separatede == 0)
    {
        if (reverted)
        {
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_e.m_edges[1];            
        }
        else
        {
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_e.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[1].m_edges[2] = f_e.m_edges[1];            
        }
    }
    else
    {
        if (reverted)
        {
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_e.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_e.m_edges[0];            
        }
        else
        {
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[1].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_e.m_edges[0];            
        }
    }
    // find separated edge of c
    order[0] = f_c.m_edges[1];
    order[2] = f_c.m_edges[2];
    order[1] = f_d.m_edges[0];
    if (cyclicSorted(order))
        separatedc = 1;
    else
    {
        order[1] = f_d.m_edges[1];
        if (cyclicSorted(order))
            separatedc = 1;
        else
            separatedc = 0;
    }
    // check if c2 is reverted in the first move
    order[0] = m_sequence->m_operations[0].m_edges[0];
    order[2] = m_sequence->m_operations[0].m_edges[1];
    order[1] = f_c.m_edges[2];
    reverted = cyclicSorted(order);
    // third move
    if (separatedc == 0)
    {
        if (reverted ^ (m_sequence->m_operations[0].m_edges[2] == f_d.m_edges[1]) 
          ^ (separatede == 0))
        {
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];            
        }
        else
        {
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];            
        }
    }
    else
    {
        if (reverted ^ (m_sequence->m_operations[0].m_edges[2] == f_d.m_edges[1]) 
          ^ (separatede == 0))
        {
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];            
        }
        else
        {
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];            
        }
    }
    // last move
    if (separatedc == separatede)
    {
        m_sequence->m_operations[3].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[3].m_edges[0] = m_sequence->m_operations[0].m_edges[1];
        m_sequence->m_operations[3].m_edges[1] = m_sequence->m_operations[0].m_edges[0];
        m_sequence->m_operations[3].m_edges[2] = m_sequence->m_operations[0].m_edges[2];
    }
    else
    {
        m_sequence->m_operations[3].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[3].m_edges[0] = m_sequence->m_operations[0].m_edges[0];
        m_sequence->m_operations[3].m_edges[1] = m_sequence->m_operations[0].m_edges[1];
        m_sequence->m_operations[3].m_edges[2] = m_sequence->m_operations[0].m_edges[2];
    }
    m_sequence->m_dsigma = 6.0f;
    m_sequence->m_weight = 4.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}


void MinSWRT::node038(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node038" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d intersects the 
     * nontwisted chord of c, c does not intersect the chord between 
     * m_edges[1] and m_edges[2] of d. This chord is intersected by the two 
     * twisted chords of e. c and e are not intersecting. */
    int order[3];    // needed to check cyclic order of three edges
    int confdc;      // configuration of cycles c and d; 0: d0 c2 c0 d1 d2 c1
                     // 1: d0 c1 c2 d1 d2 c0;  2: d0 c0 d1 d2 c1 c2
                     // 3: d0 c1 d1 d2 c2 c0
    int confde;      // configuration of cycles d and e; 0: d0 d1 e0 e1 d2 e2
                     // 1: d0 d1 e2 d2 e0 e1;  2: d0 e2 d1 e0 e1 d2
                     // 3: d0 e0 e1 d1 e1 d2                     
     
    // configuration of c and d
    order[0] = f_d.m_edges[0];
    order[2] = f_d.m_edges[2];
    order[1] = f_c.m_edges[2];
    if (cyclicSorted(order))    
    {
        order[1] = f_c.m_edges[0];
        if (cyclicSorted(order))    // edge sequence d0 c2 c0 d1 d2 c1
            confdc = 0;
        else                        // edge sequence d0 c1 c2 d1 d2 c0
            confdc = 1;
    }
    else
    {
        order[1] = f_c.m_edges[0];
        if (cyclicSorted(order))    // edge sequence d0 c0 d1 d2 c1 c2
            confdc = 2;
        else                        // edge sequence d0 c1 d1 d2 c2 c0
            confdc = 3;
    }
    // configuration of d and e
    order[0] = f_d.m_edges[1];
    order[2] = f_d.m_edges[2];
    order[1] = f_e.m_edges[2];
    if (cyclicSorted(order))
    {
        order[2] = f_d.m_edges[0];
        order[1] = f_e.m_edges[0];
        if (cyclicSorted(order))    // edge sequence d0 d1 e2 d2 e0 e1
            confde = 1;
        else                        // edge sequence d0 e0 e1 d1 e2 d2
            confde = 3;
    }
    else
    {
        order[2] = f_d.m_edges[0];
        if (cyclicSorted(order))    // edge sequence d0 d1 e0 e1 d2 e2
            confde = 0;
        else                        // edge sequence d0 e2 d1 e0 e1 d2
            confde = 2;
    }
    m_sequence->m_numOperations = 3;
    if (confdc == 0)
    {
        if (confde == 0)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[1];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];            
        }
        else if (confde == 1)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];                        
        }
        else if (confde == 2)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[2];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];                                    
        }
        else    // confde == 3
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[1];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[2];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];                        
        }
    }
    else if (confdc == 1)
    {
        if (confde == 0)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[1];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];                                                
        }
        else if (confde == 1)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];                                                
        }
        else if (confde == 2)
        {            
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[2];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];                        
        }
        else    // confde == 3
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[1];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[2];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];                                                            
        }        
    }
    else if (confdc == 2)
    {        
        if (confde == 0)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];                                                            
        }
        else if (confde == 1)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[0];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];                                                            
        }
        else if (confde == 2)
        {            
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[0];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[1];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];                                                            
        }
        else    // confde == 3
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];                                                            
        }        
    }
    else   // confdc == 3
    {
        if (confde == 0)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];                                                            
        }
        else if (confde == 1)
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[0];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[2];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
            m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[0];                                                            
        }
        else if (confde == 2)
        {            
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[2];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[0];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];                                                            
        }
        else    // confde == 3
        {
            m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[2];
            m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
            m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];            
            m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
            m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
            m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];            
            m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
            m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
            m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
            m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];                                                            
        }                
    }    
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 3.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}


void MinSWRT::node039(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node039" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif

    /* c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d intersects the 
     * nontwisted chord of c, c does not intersect the chord between 
     * m_edges[1] and m_edges[2] of d. This chord is intersected by e. There 
     * is no pair of interleaving cycles. c and e are intersecting with their 
     * twisted chords. */
    int order[3];    // needed to check cyclic order of three edges
    int edgeseq;     // there are 6 possible edge sequences
    
    // getting edge sequence
    order[0] = f_d.m_edges[1];
    order[2] = f_d.m_edges[2];
    order[1] = f_e.m_edges[2];
    if (cyclicSorted(order))      // e2 between d1, d2
    {
        order[1] = f_e.m_edges[0];  
        order[2] = f_d.m_edges[0];
        if (cyclicSorted(order))   
            edgeseq = 0;          // edge sequence d0 c1 d1 e2 d2 c2 e0 e1 c0
        else 
            edgeseq = 1;          // edge sequence d0 c1 e0 e1 c2 d1 e2 d2 c0
    }
    else
    {
        order[2] = f_d.m_edges[0];
        if (cyclicSorted(order))  // e2 between c2, c0
        {
            order[1] = f_e.m_edges[1];
            order[2] = f_d.m_edges[2];
            if (cyclicSorted(order))
                edgeseq = 2;      // edge sequence d0 c1 d1 e0 e1 d2 c2 e2 c0
            else
                edgeseq = 3;      // edge sequence d0 c1 d1 e0 d2 e1 c2 e2 c0 
        }
        else                      // e2 between c1, c2
        {
            order[1] = f_e.m_edges[0];
            order[2] = f_d.m_edges[2];
            if (cyclicSorted(order))
                edgeseq = 4;      // edge sequence d0 c1 e2 c2 d1 e0 e1 d2 c0
            else    
                edgeseq = 5;      // edge sequence d0 c1 e2 c2 e0 d1 e1 d2 c0            
        }
    }    
    m_sequence->m_numOperations = 3;
    switch (edgeseq)
    {
    case 0:
#if DEBUG            
        cerr << "node039.0\n";
#endif
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
        m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[0];
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[1];
        break;
    case 1:
#if DEBUG            
        cerr << "node039.1\n";
#endif
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
        m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSPOSITION;
        m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[1];
        m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[0];
        break;
    case 2:
#if DEBUG            
        cerr << "node039.2\n";
#endif
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[2];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
        m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[1];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
        m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
        break;
    case 3:
#if DEBUG            
        cerr << "node039.3\n";
#endif
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[1];
        m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[0];
        m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_e.m_edges[1];
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];
        break;
    case 4:
#if DEBUG            
        cerr << "node039.4\n";
#endif
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[1] = f_e.m_edges[2];
        m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[0];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
        m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[2];
        break;
    case 5:
#if DEBUG            
        cerr << "node039.5\n";
#endif
        m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[0].m_edges[0] = f_e.m_edges[1];
        m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[0];
        m_sequence->m_operations[0].m_edges[2] = f_e.m_edges[2];
        m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[2].m_edges[0] = f_e.m_edges[0];
        m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[1];
        m_sequence->m_operations[2].m_edges[2] = f_e.m_edges[2];
        break;
    }
    // second move is for four sequences identical
    if (edgeseq == 3)
    {
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[0];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[2];
        m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[1];
    }
    else if (edgeseq == 5)
    {
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[0];
        m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[2];
    }
    else
    {
        m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
        m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[2];
        m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[1];
        m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[0];
    }
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 3.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}


void MinSWRT::node040(Cycle& f_c, Cycle& f_d)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node040" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
#endif

    /* c is a nontwisted 3-cycle, d is a 1-twisted 3-cycle. c and d are 
     * interleaving. The twisted edge of d lies between m_edges[1] and 
     * m_edges[2] of cycle c. */
    m_sequence->m_numOperations = 3;
    m_sequence->m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
    m_sequence->m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[0].m_edges[1] = f_c.m_edges[1];
    m_sequence->m_operations[0].m_edges[2] = f_c.m_edges[2];
    m_sequence->m_operations[1].m_type = Operation::OPERATION_TRANSREVERSAL;
    m_sequence->m_operations[1].m_edges[0] = f_d.m_edges[1];
    m_sequence->m_operations[1].m_edges[1] = f_d.m_edges[0];
    m_sequence->m_operations[1].m_edges[2] = f_d.m_edges[2];
    m_sequence->m_operations[2].m_type = Operation::OPERATION_TRANSREVERSAL;
    m_sequence->m_operations[2].m_edges[0] = f_c.m_edges[0];
    m_sequence->m_operations[2].m_edges[1] = f_c.m_edges[2];
    m_sequence->m_operations[2].m_edges[2] = f_c.m_edges[1];    
    m_sequence->m_dsigma = 4.0f;
    m_sequence->m_weight = 3.0f * m_wt;
    m_sequence->m_ratio = m_sequence->m_dsigma / m_sequence->m_weight;
}
       

/***************************************************************************
 * debugging methods
 ***************************************************************************/

void MinSWRT::debug()
{
    cerr << "simple perm is " << (m_simplePerm->isSorted()?"":"not ") << "sorted\n";
    
    cerr << "n = " << m_size << endl;
/*    cerr << "target:\n";
    for (int i = 0; i < m_size; i++)
        cerr << m_target[i] << " ";
    cerr << endl;        */
//    printResult(4);
    
}

} // end namespace minswrt
