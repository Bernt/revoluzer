/**
 * filename: cemetery.cpp
 * author: Martin Bader
 * begin: 11.08.2005
 * last change: 17.01.2006
 *
 * Cemetery for old functions. Maybe I want to recycle parts of them later, 
 * so don't kick them.
 */

 
/**
 * This function still contains the whole decision tree, down to every
 * possible edge sequence.  
 * Node 021: c, d and e are nontwisted 3-cycles. c intersects d 
 * (d.m_edges[0] between c.m_edges[0] and c.m_edges[1], others outside),
 * e intersects d (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]). 
 * There is no pair of interleaving cycles. c and e are intersecting (so 
 * the cycles are mutually intersecting). 
 * @param f_c  cycle c
 * @param f_d  cycle d
 * @param f_d  cycle e
 */
void MinSWRT::node021(Cycle& f_c, Cycle& f_d, Cycle& f_e)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node021" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
    cerr << "cycle d:" << endl;
    printCycle(f_d);
    cerr << "cycle e:" << endl;
    printCycle(f_e);
#endif
    
    /* c, d and e are nontwisted 3-cycles. c intersects d 
     * (d.m_edges[0] between c.m_edges[0] and c.m_edges[1], others outside),
     * e intersects d (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]). 
     * There is no pair of interleaving cycles. c and e are intersecting 
     * (so the cycles are mutually intersecting). */     
    int order[3];   // needed to check cyclic sorting of three edges
    
    // get position of d1 relativ to c
    order[0] = f_c.m_edges[1];
    order[2] = f_c.m_edges[2];
    order[1] = f_d.m_edges[1];
    if (cyclicSorted(order))            // d1, d2 between c1, c2
    {
        // get position of e1
        order[2] = f_e.m_edges[1];
        order[0] = f_d.m_edges[2];
        if (cyclicSorted(order))        // e1 between e0 and d2
        { 
            // get position of e2
            order[2] = f_e.m_edges[2];
            order[0] = f_c.m_edges[0];
            if (cyclicSorted(order))    // edge sequence c0 d0 c1 d1 e0 e1 d2 c2 e2
            {
                return;
            }
            order[0] = f_d.m_edges[0];
            if (cyclicSorted(order))    // edge sequence c0 e2 d0 c1 d1 e0 e1 d2 c2
            {
                return;
            }                       
            else                        // edge sequence c0 d0 e2 c1 d1 e0 e1 d2 c2
            {
                return;
            }
        }
        order[0] = f_c.m_edges[2];
        if (cyclicSorted(order))        // e1 between d2 and c2
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            order[0] = f_c.m_edges[0];
            if (cyclicSorted(order))    // edge sequence c0 d0 c1 d1 e0 d2 e1 c2 e2
            {
                return;
            }
            else                        // edge sequence c0 e2 d0 c1 d1 e0 d2 e1 c2
            {
                return;
            }
        }
        order[0] = f_c.m_edges[0];
        if (cyclicSorted(order))        // e1 between c2 and c0
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            if (cyclicSorted(order))    // edge sequence c0 d0 c1 d1 e0 d2 c2 e1 e2
            {
                return;
            }
            else                        // edge sequence c0 d0 c1 d1 e2 e0 d2 c2 e1
            {
                return;            
            }
        }
        order[0] = f_d.m_edges[0];
        if (cyclicSorted(order))        // e1 between c0 and d0
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            if (cyclicSorted(order))    // edge sequence c0 e1 e2 d0 c1 d1 e0 d2 c2
            {
                return;
            }
            else                        // edge sequence c0 e1 d0 c1 d1 e2 e0 d2 c2
            {
                return;                        
            }
        }   
        else                            // e1 between d0 and c1
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            order[0] = f_c.m_edges[1];
            if (cyclicSorted(order))    // edge sequence c0 d0 e1 e2 c1 d1 e0 d2 c2
            {
                return;
            }
            order[0] = f_d.m_edges[1];
            if (cyclicSorted(order))    // edge sequence c0 d0 e1 c1 e2 d1 e0 d2 c2
            {
                return;
            }
            else                        // edge sequence c0 d0 e1 c1 d1 e2 e0 d2 c2
            {
                return;
            }            
        }
    }
    else                                // d1, d2 between c2, c0
    {
        // get position of e1
        order[2] = f_e.m_edges[1];
        order[0] = f_d.m_edges[2];
        if (cyclicSorted(order))        // e1 between e0 and d2
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            order[0] = f_d.m_edges[0];
            if (cyclicSorted(order))    // edge sequence c0 e2 d0 c1 c2 d1 e0 e1 d2
            {
                return;
            }
            order[0] = f_c.m_edges[1];
            if (cyclicSorted(order))    // edge sequence c0 d0 e2 c1 c2 d1 e0 e1 d2
            {
                return;
            }
            else                        // edge sequence c0 d0 c1 e2 c2 d1 e0 e1 d2
            {
                return;
            }            
        }
        order[0] = f_c.m_edges[0];
        if (cyclicSorted(order))        // e1 between d2 and c0
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            order[0] = f_d.m_edges[0];
            if (cyclicSorted(order))    // edge sequence c0 e2 d0 c1 c2 d1 e0 d2 e1
            {
                return;
            }
            else                        // edge sequence c0 d0 c1 c2 d1 e2 e0 d2 e1
            {
                return;
            }                        
        }
        order[0] = f_d.m_edges[0];
        if (cyclicSorted(order))        // e1 between c0 and d0
        {            
            // get position of e2
            order[2] = f_e.m_edges[2];
            if (cyclicSorted(order))    // edge sequence c0 e1 e2 d0 c1 c2 d1 e0 d2
            {
                return;
            }
            else                        // edge sequence c0 e1 d0 c1 c2 d1 e2 e0 d2
            {
                return;
            }                                    
        }
        order[0] = f_c.m_edges[1];
        if (cyclicSorted(order))        // e1 between d0 and c1
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            if (cyclicSorted(order))    // edge sequence c0 d0 e1 e2 c1 c2 d1 e0 d2
            {
                return;
            }
            else                        // edge sequence c0 d0 e1 c1 c2 d1 e2 e0 d2
            {
                return;
            }                                                
        }
        else                            // e1 between c1 and c2
        {
            // get position of e2
            order[2] = f_e.m_edges[2];
            order[0] = f_c.m_edges[2];
            if (cyclicSorted(order))    // edge sequence c0 d0 c1 e1 e2 c2 d1 e0 d2
            {
                return;
            }
            order[0] = f_d.m_edges[1];
            if (cyclicSorted(order))    // edge sequence c0 d0 c1 e1 c2 e2 d1 e0 d2
            {
                return;
            }
            else                        // edge sequence c0 d0 c1 e1 c2 d1 e2 e0 d2
            {
                return;
            }
        }        
    }     
}


/**
 * Old version of the function. Depth is set to 1, and the algorithm is 
 * branching into any possible path
 */
void MinSWRT::nodeRootLookahead()
{
    int numOperations;
    Operation operations[4];
    Cycle cycle;
    m_simplePerm->getCycle(&cycle);  
    
    while (true)
    {
        m_gcRatio = 0.0f;
        m_gcNumOperations = 0;
        for (int i = 0; i < m_simplePerm->size(); i++)    // for all possible cycles
        {
            m_simplePerm->getCycleOfEdge(i, &cycle);
            // choose node
            switch (cycle.m_type)
            {
            case Cycle::CYCLE_ADJACENCY:
                m_ratio = 0.0f;
                continue;    
            case Cycle::CYCLE_2ORIENTED: 
                node000(cycle);
                break;
            case Cycle::CYCLE_2UNORIENTED:
                node003(cycle);
                break;
            case Cycle::CYCLE_0TWISTED: 
                node004(cycle);
                break;
            case Cycle::CYCLE_1TWISTED: 
                node005(cycle);
                break;
            case Cycle::CYCLE_2TWISTED: 
                node002(cycle);
                break;
            case Cycle::CYCLE_3TWISTED: 
                node001(cycle);
                break;
            default: 
                cerr << "error in MinSWRT::nodeRootGreedy: Algo found unexpected cycle type\n";
                printCycle(cycle);            
                return;
            }
            // store sequence
            numOperations = m_numOperations;
            for (int j = 0; j < numOperations; j++)
                operations[j] = m_operations[j];
            // do lookahead    
            m_ratio = doLookahead();            
            if (m_ratio > m_gcRatio)     // copy to golden cage
            {
                m_gcNumOperations = numOperations;
                for (int j = 0; j < numOperations; j++)
                    m_gcOperations[j] = operations[j];
                m_gcRatio = m_ratio;
            }
        }    
        if (m_gcNumOperations == 0)     // permutation sorted
            return;
        // backcopy golden cage, and perform sequence
        m_numOperations = m_gcNumOperations;
        for (int i = 0; i < m_numOperations; i++)
            m_operations[i] = m_gcOperations[i];
        performSequence();          
    }    
}


/**
 * Old version of the function. Depth is set to 1, and the algorithm is 
 * branching into any possible path
 */
float MinSWRT::doLookahead()
{
    float weightS1 = 0.0f;            // weight of old sequence
    float weightS2;                   // weight of second sequence
    float dsigmaS1;                   // delta sigma of old sequence
    float ratioBoth;                  // ratio over both sequences
    float result = 0.0f;              // best possible ratio over both sequences
    int order1[3];                    // needed to check is something is cyclic sorted
    int order2[3];                    // needed to check is something is cyclic sorted
    int delta1, delta2;               // distance to move
    int size = m_simplePerm->size();  // permutation size
    Cycle cycle;                      // a cycle in the RDD (starting cycle of case analysis)   
    Permutation* tmp;                 // used for swapping permutations
    
    for (int i = 0; i < m_numOperations; i++)
    {
        if (m_operations[i].m_type == Operation::OPERATION_REVERSAL)
            weightS1 += m_wr;
        else
            weightS1 += m_wt;
    }
    dsigmaS1 = m_ratio * weightS1;
    m_lookaheadPerm->setData(m_simplePerm->m_data);
    
    // perform sequence on m_lookaheadPerm
    for (int i = 0; i < m_numOperations; i++)
    {
        m_lookaheadPerm->performOperation(m_operations[i]);
        // update operations
        if ((m_operations[i].m_type == Operation::OPERATION_REVERSAL)
          || (m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = m_operations[i].m_edges[0];
            order1[1] = m_operations[i].m_edges[1];
            for (int j = i+1; j < m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    order1[2] = m_operations[j].m_edges[k];
                    if (!cyclicSorted(order1))    // move the edge
                        m_operations[j].m_edges[k] = 
                          (m_operations[i].m_edges[0] 
                          + m_operations[i].m_edges[1] 
                          - m_operations[j].m_edges[k] + size) % size;
                }              
            }
        }        
        if ((m_operations[i].m_type == Operation::OPERATION_TRANSPOSITION)
          || (m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = m_operations[i].m_edges[1];
            order1[1] = m_operations[i].m_edges[2];
            order2[0] = m_operations[i].m_edges[2];
            order2[1] = m_operations[i].m_edges[0];
            delta1 = m_operations[i].m_edges[0] - m_operations[i].m_edges[2]; 
            delta2 = m_operations[i].m_edges[2] - m_operations[i].m_edges[1]; 
            for (int j = i+1; j < m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    if (m_operations[j].m_edges[k] == m_operations[i].m_edges[2])
                        m_operations[j].m_edges[k] = (m_operations[j].m_edges[k] 
                          + delta1 - delta2 + size) % size;
                    else
                    {
                        order1[2] = m_operations[j].m_edges[k];
                        // move first segment to the back
                        if (!cyclicSorted(order1))    // move the edge
                            m_operations[j].m_edges[k] = 
                              (m_operations[j].m_edges[k] + delta1 + size) 
                              % size;
                        else 
                        {
                            // move second segment to the front      
                            order2[2] = m_operations[j].m_edges[k];
                            if (!cyclicSorted(order2))  // move the edge
                            {
                                m_operations[j].m_edges[k] = 
                                  (m_operations[j].m_edges[k] - delta2 + size) 
                                  % size;
                            }
                        }
                    }      
                }              
            }
        }
    }    

    // swap permutations
    tmp = m_simplePerm;
    m_simplePerm = m_lookaheadPerm;
    // lookahead    
    for (int i = 0; i < m_simplePerm->size(); i++)    // for all possible cycles
    {
        m_simplePerm->getCycleOfEdge(i, &cycle);
        // choose node
        switch (cycle.m_type)
        {
        case Cycle::CYCLE_ADJACENCY:
            m_ratio = 0.0f;
            continue;    
        case Cycle::CYCLE_2ORIENTED: 
            node000(cycle);
            break;
        case Cycle::CYCLE_2UNORIENTED:
            node003(cycle);
            break;
        case Cycle::CYCLE_0TWISTED: 
            node004(cycle);
            break;
        case Cycle::CYCLE_1TWISTED: 
            node005(cycle);
            break;
        case Cycle::CYCLE_2TWISTED: 
            node002(cycle);
            break;
        case Cycle::CYCLE_3TWISTED: 
            node001(cycle);
            break;
        default: 
            cerr << "error in MinSWRT::doLookahead: Algo found unexpected cycle type\n";
            printCycle(cycle);            
            continue;
        }
        // calculate overall ratio
        weightS2 = 0.0f;
        for (int j = 0; j < m_numOperations; j++)
        {
            if (m_operations[j].m_type == Operation::OPERATION_REVERSAL)
                weightS2 += m_wr;
            else
                weightS2 += m_wt;
        }
        ratioBoth = (dsigmaS1 + m_ratio * weightS2) / (weightS1 + weightS2); 
        // check if better than old result        
        if (ratioBoth > result)
            result = ratioBoth;
    }    
    // unswap permutations
    m_simplePerm = tmp;
    if (result < 0.1f)        // happens when m_lookaheadPerm = id
        result = dsigmaS1 / weightS1;
    return result;    
}


/**
 * Node 0 without lookahead
 */
void MinSWRT::node000(Cycle& f_c)
{
#if DEBUG    
    // !!DEBUG: cycle info
    cerr << "entering node000" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
#endif
    
    /* c: r-oriented 2-cycle */    
    m_numOperations = 1;
    m_operations[0].m_type = Operation::OPERATION_REVERSAL;
    m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_operations[0].m_edges[1] = f_c.m_edges[1];
    m_ratio = 2.0f / m_wt; 
}


/**
 * Node 1 without lookahead
 */
void MinSWRT::node001(Cycle& f_c)
{
#if DEBUG
    // !!DEBUG: cycle info
    cerr << "entering node001" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
#endif
      
    /* c: 3-twisted 3-cycle */
    m_numOperations = 1;
    m_operations[0].m_type = Operation::OPERATION_TRANSPOSITION;
    m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_operations[0].m_edges[1] = f_c.m_edges[1];
    m_operations[0].m_edges[2] = f_c.m_edges[2];
    m_ratio = 2.0f / m_wt; 
}


/**
 * Node 2 without lookahead
 */
void MinSWRT::node002(Cycle& f_c)
{
#if DEBUG    
    // !!DEBUG: cycle info
    cerr << "entering node002" << endl;
    cerr << "cycle c:" << endl;
    printCycle(f_c);
#endif

    /* c: 2-twisted 3-cycle */
    m_numOperations = 1;
    m_operations[0].m_type = Operation::OPERATION_TRANSREVERSAL;
    m_operations[0].m_edges[0] = f_c.m_edges[0];
    m_operations[0].m_edges[1] = f_c.m_edges[1];
    m_operations[0].m_edges[2] = f_c.m_edges[2];
    m_ratio = 2.0f / m_wt; 
}


/**
 * This version applies the sequence that is stored in the class variables
 * m_operations and m_numOperations.
 */
void MinSWRT::performSequence()
{
    int order1[3];       // needed to check is something is cyclic sorted
    int order2[3];       // needed to check is something is cyclic sorted
    int delta1, delta2;  // distance to move
    int size = m_simplePerm->size();   // permutation size
    
#if 0    
    // !!DEBUG: print all operations
    for (int i = 0; i < m_numOperations; i++)
    {
        cerr << "op" << i << ": " << m_operations[i].m_edges[0] << "  " 
          << m_operations[i].m_edges[1] << "  " 
          <<  m_operations[i].m_edges[2] << endl;
    }
#endif
    
    for (int i = 0; i < m_numOperations; i++)
    {
        m_simplePerm->performOperation(m_operations[i]);
        // update operations
        if ((m_operations[i].m_type == Operation::OPERATION_REVERSAL)
          || (m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = m_operations[i].m_edges[0];
            order1[1] = m_operations[i].m_edges[1];
            for (int j = i+1; j < m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    order1[2] = m_operations[j].m_edges[k];
                    if (!cyclicSorted(order1))    // move the edge
                        m_operations[j].m_edges[k] = 
                          (m_operations[i].m_edges[0] 
                          + m_operations[i].m_edges[1] 
                          - m_operations[j].m_edges[k] + size) % size;
                }              
            }
        }        
        if ((m_operations[i].m_type == Operation::OPERATION_TRANSPOSITION)
          || (m_operations[i].m_type == Operation::OPERATION_TRANSREVERSAL))
        {
            order1[0] = m_operations[i].m_edges[1];
            order1[1] = m_operations[i].m_edges[2];
            order2[0] = m_operations[i].m_edges[2];
            order2[1] = m_operations[i].m_edges[0];
            delta1 = m_operations[i].m_edges[0] - m_operations[i].m_edges[2]; 
            delta2 = m_operations[i].m_edges[2] - m_operations[i].m_edges[1]; 
            for (int j = i+1; j < m_numOperations; j++)   
            {
                for (int k = 0; k < 3; k++)
                {
                    if (m_operations[j].m_edges[k] == m_operations[i].m_edges[2])
                        m_operations[j].m_edges[k] = (m_operations[j].m_edges[k] 
                          + delta1 - delta2 + size) % size;
                    else
                    {
                        order1[2] = m_operations[j].m_edges[k];
                        // move first segment to the back
                        if (!cyclicSorted(order1))    // move the edge
                            m_operations[j].m_edges[k] = 
                              (m_operations[j].m_edges[k] + delta1 + size) 
                              % size;
                        else 
                        {
                            // move second segment to the front      
                            order2[2] = m_operations[j].m_edges[k];
                            if (!cyclicSorted(order2))  // move the edge
                            {
                                m_operations[j].m_edges[k] = 
                                  (m_operations[j].m_edges[k] - delta2 + size) 
                                  % size;
                            }
                        }
                    }      
                }              
            }
        }
        
        mimickOperation(m_operations[i]);

#if 0        
        // !!DEBUG: print current permutation and further operations
        cerr << "perm: ";
        for (int j = 0; j < m_simplePerm->size(); j++)
            cerr << m_simplePerm->m_data[j] << " ";        
        cerr << endl;
        for (int k = i+1; k < m_numOperations; k++)
        {
            cerr << "op" << k << ": " << m_operations[k].m_edges[0] << "  " 
              << m_operations[k].m_edges[1] << "  " 
              <<  m_operations[k].m_edges[2] << endl;
        }
#endif

    }    
}



 