/**
 * filename: tools.cpp
 * author: Martin Bader
 * begin: 25.07.2005
 * last change: 11.08.2008
 *
 * Useful functions for MinSWRT.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tools.h"

#include "permutation.h"

namespace minswrt
{

/***************************************************************************
 * debugging non-class functions
 ***************************************************************************/

bool interleaving(const Cycle& f_c, const Cycle& f_d)
{
    int i1, i2;     // edge indexes
    
    // search minimum edges    
    if (f_c.m_edges[0] < f_c.m_edges[1])
    {
        if (f_c.m_edges[0] < f_c.m_edges[2])
            i1 = 0;
        else
            i1 = 2;
    }
    else if (f_c.m_edges[1] < f_c.m_edges[2])
        i1 = 1;
    else
        i1 = 2;
    if (f_d.m_edges[0] < f_d.m_edges[1])
    {
        if (f_d.m_edges[0] < f_d.m_edges[2])
            i2 = 0;
        else
            i2 = 2;
    }
    else if (f_d.m_edges[1] < f_d.m_edges[2])
        i2 = 1;
    else
        i2 = 2;
    // check if interleaving
    if (f_c.m_edges[i1] > f_d.m_edges[i2])
        i2 == 2? i2 = 0 : i2++;    
    else if (f_c.m_edges[(i1 + 2) % 3] > f_d.m_edges[(i2 + 2) % 3])
        return false;        
    for (int i = 0; i < 2; i++)
    {
        if (f_c.m_edges[i1] > f_d.m_edges[i2])
            return false;
        i1 == 2? i1 = 0 : i1++;    
        if (f_d.m_edges[i2] > f_c.m_edges[i1])
            return false;
        i2 == 2? i2 = 0 : i2++;    
    }
    return true;            
}                    

} // end namespace minswrt
