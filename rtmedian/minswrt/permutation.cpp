/**
 * filename: permutation.cpp
 * author: Martin Bader
 * begin: 22.07.2005
 * last change: 15.08.2008
 *
 * The representation of a permutation.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "permutation.h"

#include <stdlib.h>
#include "tools.h"

namespace minswrt
{
using namespace std;    
    
/***************************************************************************
 * public methods
 ***************************************************************************/
 
Permutation::Permutation()
{
    m_size = 0;
    m_data = NULL;
    m_index = NULL;
    m_tempData = NULL;
}


Permutation::~Permutation()
{
    delete[] m_data; m_data = NULL;
    delete[] m_index; m_index = NULL;
    delete[] m_tempData; m_tempData = NULL;
}


void Permutation::init(int f_size)
{
    m_size = f_size;
    delete[] m_data;
    delete[] m_index;
    delete[] m_tempData;
    m_data = new int[f_size];    
    m_index = new int[f_size+1];
    m_index[0] = 0;
    m_tempData = new int[f_size];
}


void Permutation::init(int f_size, const int* f_data)
{
    m_size = f_size;
    delete[] m_data;
    delete[] m_index;
    delete[] m_tempData;
    m_data = new int[f_size];    
    m_index = new int[f_size+1];
    m_index[0] = 0;
    m_tempData = new int[f_size];
    for (int i = 0; i < f_size; i++)
    {
        m_data[i] = f_data[i];       
        m_index[abs(f_data[i])] = i;
    }
}


void Permutation::setData(const int* f_data)
{
    for (int i = 0; i < m_size; i++)
    {
        m_data[i] = f_data[i];       
        m_index[abs(f_data[i])] = i;
    }    
}


void Permutation::getCycle(Cycle* f_result, int f_intersect, 
  int f_a1, int f_a2, int f_b1, int f_b2) const
{
    bool left[4];      // did we leave the last RE to the left?
    int element;       // the next element
    int order1[3];     // needed to check if intersecton is ok
    int order2[3];     // needed to check if intersecton is ok
    
    // adjust bounds
    if (f_intersect == 0)
    {
        f_b2 = m_size;
    }
    else if (f_intersect == 1)
    {
        f_b2 = f_a2;
    }
    if (f_b2 == 0)
        f_b2 = m_size;
    // search cycle    
    for (int i = f_a1 + 1; i != f_b2; i++)
    {        
        if (i == m_size)
            i = 0;
        if (i == f_a2)
        {
            i = (f_b1 + 1) % m_size;
            if (i == f_b2)
                break;
        }
        f_result->m_edges[0] = i;
        left[0] = true;
        for (int j = 0; j < 2;)           // increment in loop
        {
            if (left[j])            
                element = m_data[f_result->m_edges[j]];                
            else 
                element = m_data[(f_result->m_edges[j]+1) % m_size];
            if (left[j] ^ (element > 0))   // go to element - 1, upper point
            {
                j++;                       // increment j
                left[j] = false;         // left now indicates point of target
                element = abs(element) - 1;
                if (element == 0)
                    element = m_size;
            }
            else                           // go to element + 1, lower point
            {
                j++;                       // increment j
                left[j] = true;          // left now indicates point of target
                element = abs(element) + 1;        
                if (element > m_size)
                    element = 1;
            }
            f_result->m_edges[j] = m_index[element];
            if  (!(left[j] ^ (m_data[f_result->m_edges[j]] > 0)))            
            {    
                f_result->m_edges[j] = (f_result->m_edges[j] + m_size - 1) % m_size;
                left[j] = true;
            }
            else
                left[j] = false;
        }

#if 0        
        cerr << "cycle found\n";
        cerr << "edges: " << f_result->m_edges[0] << "  " << f_result->m_edges[1] 
          << "  " << f_result->m_edges[2] << endl;
#endif

        // check intersection; first edge is always in interval
        if (f_intersect == 1)
        {
            order1[0] = f_a1;
            order1[1] = f_result->m_edges[1];
            order1[2] = f_a2;
            if (cyclicSorted(order1))          // second element in interval
            {
                order1[1] = f_result->m_edges[2];
                if (cyclicSorted(order1))      // third element in interval
                    continue;
            }
        }
        else if (f_intersect == 2)
        {
            order1[0] = f_a1;
            order1[1] = f_result->m_edges[1];
            order1[2] = f_a2;
            order2[0] = f_b1;
            order2[1] = f_result->m_edges[1];
            order2[2] = f_b2;
            if (cyclicSorted(order1) || cyclicSorted(order2))  // second in interval
            {
                order1[1] = f_result->m_edges[2];
                order2[1] = f_result->m_edges[2];
                if (cyclicSorted(order1) || cyclicSorted(order2))  // third in interval
                    continue;
            }
        }    
        // check cycle type
        if (f_result->m_edges[0] == f_result->m_edges[1])
            f_result->m_type = Cycle::CYCLE_ADJACENCY;
        else if (f_result->m_edges[0] == f_result->m_edges[2])            
        {
            if (left[0] == left[1])
                f_result->m_type = Cycle::CYCLE_2UNORIENTED;
            else
                f_result->m_type = Cycle::CYCLE_2ORIENTED;
        }        
        else 
        {
            // check if long cycle
            if (left[2])            
                element = m_data[f_result->m_edges[2]];                
            else 
                element = m_data[(f_result->m_edges[2]+1) % m_size];
            if (left[2] ^ (element > 0))   // go to element - 1, upper point
            {
                left[3] = false;           // left now indicates point of target
                element = abs(element) - 1;
                if (element == 0)
                    element = m_size;
            }
            else                           // go to element + 1, lower point
            {
                left[3] = true;            // left now indicates point of target
                element = abs(element) + 1;        
                if (element > m_size)
                    element = 1;
            }
            element = m_index[element];    // use element as edge index (save memory)
            if  (!(left[3] ^ (m_data[element] > 0)))            
                element = (element + m_size - 1) % m_size;
            if (element == f_result->m_edges[0])    // 3-cycle
            {   
                if (left[1] && left[2])    // all RE have same orientation
                {
                    if (cyclicSorted(f_result->m_edges))
                        f_result->m_type = Cycle::CYCLE_3TWISTED;                    
                    else    
                    {
                        f_result->m_type = Cycle::CYCLE_0TWISTED;
                        element = f_result->m_edges[1];     // switch order
                        f_result->m_edges[1] = f_result->m_edges[2];
                        f_result->m_edges[2] = element;
                    }
                }
                else                       // RE with different orientations
                {
                    // move special edge at correct position - use element as 
                    // temporary variable
                    if (!left[1] && !left[2])    // first edge is special edge
                    {    
                        element = f_result->m_edges[0];
                        f_result->m_edges[0] = f_result->m_edges[1];
                        f_result->m_edges[1] = f_result->m_edges[2];
                        f_result->m_edges[2] = element;
                    }
                    else if (left[2])           // second edge is special edge
                    {                           // also invert order
                        element = f_result->m_edges[1];
                        f_result->m_edges[1] = f_result->m_edges[2];
                        f_result->m_edges[2] = element;
                    }
                    else                        // third edge is special edge
                    {                           // invert order
                        element = f_result->m_edges[0];
                        f_result->m_edges[0] = f_result->m_edges[1];
                        f_result->m_edges[1] = element;
                    }
                    if (cyclicSorted(f_result->m_edges))
                        f_result->m_type = Cycle::CYCLE_1TWISTED;
                    else    
                    {
                        f_result->m_type = Cycle::CYCLE_2TWISTED;
                        element = f_result->m_edges[0];    // invert order
                        f_result->m_edges[0] = f_result->m_edges[1];
                        f_result->m_edges[1] = element;
                    }
                }
            }  // end 3-cycle        
            else
                f_result->m_type = Cycle::CYCLE_LONGCYCLE;                            
        }  // end > 2-cycle
        if (f_result->m_type != Cycle::CYCLE_ADJACENCY)
            return;
    }  // end for                  
    f_result->m_type = Cycle::CYCLE_INVALID;     // no cycle found
}


void Permutation::getCycleOfEdge(int f_edge, Cycle* f_result)
{
    bool left[4];      // did we leave the last RE to the left?
    int element;       // the next element
    
    f_result->m_edges[0] = f_edge;
    left[0] = true;
    for (int j = 0; j < 2;)           // increment in loop
    {
        if (left[j])            
            element = m_data[f_result->m_edges[j]];                
        else 
            element = m_data[(f_result->m_edges[j]+1) % m_size];
        if (left[j] ^ (element > 0))   // go to element - 1, upper point
        {
            j++;                       // increment j
            left[j] = false;         // left now indicates point of target
            element = abs(element) - 1;
            if (element == 0)
                element = m_size;
        }
        else                           // go to element + 1, lower point
        {
            j++;                       // increment j
            left[j] = true;          // left now indicates point of target
            element = abs(element) + 1;        
            if (element > m_size)
                element = 1;
        }
        f_result->m_edges[j] = m_index[element];
        if  (!(left[j] ^ (m_data[f_result->m_edges[j]] > 0)))            
        {    
            f_result->m_edges[j] = (f_result->m_edges[j] + m_size - 1) % m_size;
            left[j] = true;
        }
        else
            left[j] = false;
    }

#if 0        
    cerr << "cycle found\n";
    cerr << "edges: " << f_result->m_edges[0] << "  " << f_result->m_edges[1] 
      << "  " << f_result->m_edges[2] << endl;
#endif

    // check cycle type
    if (f_result->m_edges[0] == f_result->m_edges[1])
        f_result->m_type = Cycle::CYCLE_ADJACENCY;
    else if (f_result->m_edges[0] == f_result->m_edges[2])            
    {
        if (left[0] == left[1])
            f_result->m_type = Cycle::CYCLE_2UNORIENTED;
        else
            f_result->m_type = Cycle::CYCLE_2ORIENTED;
    }        
    else 
    {
        // check if long cycle
        if (left[2])            
            element = m_data[f_result->m_edges[2]];                
        else 
            element = m_data[(f_result->m_edges[2]+1) % m_size];
        if (left[2] ^ (element > 0))   // go to element - 1, upper point
        {
            left[3] = false;           // left now indicates point of target
            element = abs(element) - 1;
            if (element == 0)
                element = m_size;
        }
        else                           // go to element + 1, lower point
        {
            left[3] = true;            // left now indicates point of target
            element = abs(element) + 1;        
            if (element > m_size)
                element = 1;
        }
        element = m_index[element];    // use element as edge index (save memory)
        if  (!(left[3] ^ (m_data[element] > 0)))            
            element = (element + m_size - 1) % m_size;
        if (element == f_result->m_edges[0])    // 3-cycle
        {   
            if (left[1] && left[2])    // all RE have same orientation
            {
                if (cyclicSorted(f_result->m_edges))
                    f_result->m_type = Cycle::CYCLE_3TWISTED;                    
                else    
                {
                    f_result->m_type = Cycle::CYCLE_0TWISTED;
                    element = f_result->m_edges[1];     // switch order
                    f_result->m_edges[1] = f_result->m_edges[2];
                    f_result->m_edges[2] = element;
                }
            }
            else                       // RE with different orientations
            {
                // move special edge at correct position - use element as 
                // temporary variable
                if (!left[1] && !left[2])    // first edge is special edge
                {    
                    element = f_result->m_edges[0];
                    f_result->m_edges[0] = f_result->m_edges[1];
                    f_result->m_edges[1] = f_result->m_edges[2];
                    f_result->m_edges[2] = element;
                }
                else if (left[2])           // second edge is special edge
                {                           // also invert order
                    element = f_result->m_edges[1];
                    f_result->m_edges[1] = f_result->m_edges[2];
                    f_result->m_edges[2] = element;
                }
                else                        // third edge is special edge
                {                           // invert order
                    element = f_result->m_edges[0];
                    f_result->m_edges[0] = f_result->m_edges[1];
                    f_result->m_edges[1] = element;
                }
                if (cyclicSorted(f_result->m_edges))
                    f_result->m_type = Cycle::CYCLE_1TWISTED;
                else    
                {
                    f_result->m_type = Cycle::CYCLE_2TWISTED;
                    element = f_result->m_edges[0];    // invert order
                    f_result->m_edges[0] = f_result->m_edges[1];
                    f_result->m_edges[1] = element;
                }
            }
        }  // end 3-cycle        
        else
            f_result->m_type = Cycle::CYCLE_LONGCYCLE;                            
    }  // end > 2-cycle
}


void Permutation::performOperation(const Operation& f_operation)
{
    int pos;    // actual position
    int range;  // length of a segment
    
    if ((f_operation.m_type == Operation::OPERATION_REVERSAL) 
      || (f_operation.m_type == Operation::OPERATION_TRANSREVERSAL))
    {
        for (int i = f_operation.m_edges[1]; 
          i != f_operation.m_edges[0]; (i == 0? i = m_size - 1:i--))
            m_tempData[i] = m_data[i];
        for (int i = 1; i <= (f_operation.m_edges[1] 
          - f_operation.m_edges[0] + m_size) % m_size; i++)
        {
            pos = (f_operation.m_edges[0] + i) % m_size;
            m_data[pos] = -m_tempData[(f_operation.m_edges[1] + m_size + 1 
              - i) % m_size];
            m_index[abs(m_data[pos])] = pos;
        }  
    }
    if ((f_operation.m_type == Operation::OPERATION_TRANSPOSITION) 
      || (f_operation.m_type == Operation::OPERATION_TRANSREVERSAL))
    {
        // store length of first segment to change
        range = (f_operation.m_edges[2] - f_operation.m_edges[1] + m_size) 
          % m_size;
        for (int i = 1; i <= range; i++)    // store segment
            m_tempData[i] = m_data[(f_operation.m_edges[1] + i) % m_size];
        pos = f_operation.m_edges[1];
        for (int i = 0; i < (f_operation.m_edges[0] 
          - f_operation.m_edges[2] + m_size) % m_size; i++)
        {
            pos == m_size - 1?pos = 0 : pos++;
            m_data[pos] = m_data[(pos + range) % m_size];
            m_index[abs(m_data[pos])] = pos;
        }
        for (int i = 1; i <= range; i++)
        {
            pos == m_size - 1?pos = 0 : pos++;
            m_data[pos] = m_tempData[i];
            m_index[abs(m_data[pos])] = pos;
        }
    }
}


void Permutation::print() const
{
    for (int i = 0; i < m_size; i++)
        cout << m_data[i] << "  ";
    cout << endl;
}


/***************************************************************************
 * debugging methods
 ***************************************************************************/

void Permutation::debugPrint()
{
    cerr << "m_size: " << m_size << endl;
    cerr << "m_data: " << endl;
    for (int i = 0; i < m_size; i++)
        cerr << m_data[i] << "  ";
    cerr << endl << "m_index: " << endl;
    for (int i = 1; i <= m_size; i++)
        cerr << m_index[i] << "  ";
    cerr << endl;
}


bool Permutation::hasLongCycle()
{
    bool* visited = new bool[m_size];  // visited edges
    int cyclelength;                   // length of the current cycle
    bool left;                   // true if we leave the RE at the left point
    int element;                       // the next element (unsigned)
    int pos;                           // position in permutation
    
    for (int i = 0; i < m_size; i++)
        visited[i] = false;
    for (int i = 0; i < m_size; i++)
    {
        if (!visited[i])               // new cycle
        {
            cyclelength = 0;
            pos = i;
            left = true;
            do
            {
                visited[pos] = true;
                cyclelength++;
                if (left)
                    element = m_data[pos];
                else 
                    element = m_data[(pos+1) % m_size];
                if (left ^ (element > 0))   // go to element - 1, upper point
                {
                    left = false;           // left now indicates point of target
                    element = abs(element) - 1;
                    if (element == 0)
                        element = m_size;
                }
                else                        // go to element + 1, lower point
                {
                    left = true;            // left now indicates point of target
                    element = abs(element) + 1;        
                    if (element > m_size)
                        element = 1;
                }
                pos = m_index[element];
                if  (!(left ^ (m_data[pos] > 0)))            
                {    
                    pos = (pos + m_size - 1) % m_size;
                    left = true;
                }
                else
                    left = false;
            } while (!visited[pos]); 
            if (cyclelength > 3)
            {
                delete[] visited;
                return true;
            }
        }
    }
    delete[] visited;
    return false;
}


bool Permutation::isSorted()
{
    int lowest = 0;       // lowest element of the permutation
    
    for (int i = 0; i < m_size; i++)
    {
        if ((m_data[i] == 1) || (m_data[i] == -m_size))
        {
            lowest = i;
            break;
        }
    }
    for (int i = 1; i < m_size; i++)
    {
        if (m_data[(lowest + i) % m_size] != m_data[(lowest + i - 1) % m_size] + 1)
            return false;
    }
    return true;
}


/***************************************************************************
 * debugging non-class functions
 ***************************************************************************/

void printCycle(const Cycle& f_cycle)
{
    switch (f_cycle.m_type)
    {
    case Cycle::CYCLE_ADJACENCY: cerr << "adjacency\n"; break;    
    case Cycle::CYCLE_2ORIENTED: cerr << "oriented 2-cycle\n"; break;    
    case Cycle::CYCLE_2UNORIENTED: cerr << "unoriented 2-cycle\n"; break;    
    case Cycle::CYCLE_0TWISTED: cerr << "nontwisted\n"; break;
    case Cycle::CYCLE_1TWISTED: cerr << "1-twisted\n"; break;
    case Cycle::CYCLE_2TWISTED: cerr << "2-twisted\n"; break;
    case Cycle::CYCLE_3TWISTED: cerr << "3-twisted\n"; break;
    case Cycle::CYCLE_LONGCYCLE: cerr << "long cycle\n"; break;    
    case Cycle::CYCLE_INVALID: cerr << "invalid\n"; break;    
    }
    cerr << "edges: " << f_cycle.m_edges[0] << "  " << f_cycle.m_edges[1] << "  "
      << f_cycle.m_edges[2] << endl;
    
}


} // end namespace minswrt
