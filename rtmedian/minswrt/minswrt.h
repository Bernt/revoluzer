/**
 * filename: minswrt.h
 * author: Martin Bader
 * begin: 22.07.2005
 * last change: 11.08.2008
 *
 * The main class of Sorting by Weighted Reversals and Transpositions.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef MINSWRT_H_
#define MINSWRT_H_

#include "config.h"

#include <vector>
#include "permutation.h"

namespace minswrt
{
    
// forward declarations
class Permutation;


/**
 * The main class, contains the whole functionality.
 */
class MinSWRT
{
    /***********************************************************************
     * member variables
     ***********************************************************************/
   
  protected:
    /** Searching strategy: 0 without greedy (default, O(n^2) );  1 greedy 
     * ( O(n^3) );  2 greedy with lookahead */
    int m_greedy;
    
    /** The depth of the lookahead (if used) */
    int m_lookaheadDepth;
    
    /** The lookahead branching parameters (i.th value: branch a[i] best
     * sequences */
    int m_lookaheadBranch[64];
    
    /** Weight of reversals */
    float m_wr;
    
    /** Weight of transpositions and transreversals */
    float m_wt;
    
    /** Size of the permutations (origin and target) */
    int m_size;
     
    /** The origin permutation */
    const int* m_origin;
    
    /** The target permutation. */
    const int* m_target;

    /** The permutation to sort (mapped origin permutation). */
    Permutation* m_perm;
     
    /** The equivalent simple permutation. */
    Permutation* m_simplePerm;
    
    /** Position of the elements of m_perm in m_simplePerm. */
    int* m_simplePosition;    
    
    /** Dirty-flag for the mapping of origin/target to m_perm/id */
    bool m_dirtyMapping;
    
    /** The actual starting sequence (nodes write directly in this) */
    Sequence* m_sequence;
        
    /** A vector of operations that sorts the permutation */
    vector<Operation> m_sorting;
    
    /** While mimicking the sorting, there occurs a cyclic shift of the 
     * permutation. This variable is the actual shift of m_perm. */
    int m_shift; 
                  
  private:
    /** Avoids frequent memory allocation */
    int* m_tempData;
        
    // !!DEBUG
    int twomoves;
    int othermoves;    

    /***********************************************************************
     * methods
     ***********************************************************************/
     
  public:
    /**
     *  The constructor.
     */
    MinSWRT(); 
 
    /**
     * The destructor.
     */
    ~MinSWRT();

    /**
     * Sets the searching strategy of the algorithm: 0: no greedy;  1 greedy
     * without lookahead;  2 greedy with lookahead.
     * The Greedy-algorithm has a running-time of O(n^3), but is 
     * expected to return better sequences than without greedy (non-Greedy 
     * has running time of O(n^2). Use of lookahead increases the running 
     * time to !!TODO but should return even better results.
     * @param f_greedy  searching strategy
     */
    void setGreedy(int f_greedy)
    {
        m_greedy = f_greedy;
    }
    
    /**
     * Sets the lookahead depth and branching parameters. Automatically sets
     * the mode to lookahead. f_depth is the lookahead depth, and must be 
     * >= 0 (0 means a pseudo-lookahead for moves with delta codd = 2). 
     * f_branch is an array with the branching degree for the different 
     * depths (so f_depth[i] = k means that the k best sequences will be 
     * expanded in the i.th recursive step). The array must have a size 
     * >= f_depth.
     * Additional to the lookahead depth, one pseudo-lookahead step 
     * (calculating the change of t/tr-oriented 3-cycles and r-oriented
     * 2-cycles) will be performed when using lookahead.
     * @param f_depth  the depth of the lookahead
     * @param f_branch  array with branching degrees
     */
    void setLookaheadParams(int f_depth, const int* f_branch);
     
    /**
     * Sets the weights w_r (for reversals) and w_t (for transpositions and
     * transreversals). The algorithm provides a 1.5-approximation for 
     * w_r <= w_t <= 2 w_r.
     * @param f_wr  weight of reversals
     * @param f_wt  weight of transpositions and transreversals
     */
    void setWeights(float f_wr, float f_wt);
     
    /**
     * Sets the size of the permutations. You must call this function before
     * calling setOrigin or setTarget.
     * @param f_size  permutation size
     */
    void setPermutationSize(int f_size);
    
    /**
     * Sets the origin permutation. The size of the permutation must be set 
     * with setPermutationSize; the data must be a permutation of the 
     * integers 1 .. f_size, plus additional sign.     
     */
    void setOrigin(const int* f_data); 
     
    /**
     * Sets the target permutation. The size of the permutation must be set 
     * with setPermutationSize; the data must be a permutation of the 
     * integers 1 .. f_size, plus additional sign.     
     */
    void setTarget(const int* f_data); 
         
    /**
     * Sets both the origin and the target permutation. Both must have the 
     * size f_size. f_origin and f_target contain the permutation data. The 
     * data must be a permutation of the integers 1 .. f_size, plus 
     * additional signs.
     * @param f_size  the permutation size
     * @param f_data  the elements
     */
    void setPermutations(int f_size, const int* f_origin, const int* f_target); 
              
    /**
     * Returns a lower bound for the weighted distance.
     * @return  a lower bound for the weighted distance
     */
    float getLowerBound();
    
    /**
     * Performs the sorting algorithm, including all preparing steps
     * (permutation mapping, generating simple permutation. ...).
     */
    void sort(); 
     
    /**
     * Initializes the starting sequences functionality.
     * Call this before the first call of getStartingSequences. 
     */     
    void initStartingSequences();
     
    /**
     * Creates a vector of possible starting sequences. The result
     * will be written to f_result, which must be initialized with
     * the size of the simple permutation. The number of valid sequences
     * in the result will be the returned value.
     * @param f_result  the resulting sequences will be written to here
     * @return  the number of found sequences
     */
    int getStartingSequences(Sequence* f_result); 

    /**
     * Performs a sequence on the simple permutation.
     * @param f_seq  the sequence to perform
     * */
    void performStartingSequence(Sequence* f_seq);        
    
    /**
     * Prints the result to stdout. sort must have been called before this.
     * f_printLevel determins how much information will be written.
     * f_printLevel:
     * 0: only write overall weight
     * 1: write operations
     * 2: write operations + overall weight
     * 3: write operations + permutations
     * 4: write operations + permutations + overall weight
     * @param f_printLevel  the print level
     */
    void printResult(int f_printLevel);
    
    /**
     * Returns a pointer to the result vector.
     * @return  the result vector
     */
    const vector<Operation>* getResult();
          
  protected:
    /**
     * Maps the permutation from origin/target to m_perm/id. After this, 
     * m_perm is correctly initialized.
     */
    void mapPermutations(); 
  
    /**
     * Generates the simple permutation. The permutation must be set.
     */
    void generateSimplePermutation();

    /**
     * Performs a sequence of operations. 
     * @param f_seq  the sequence to perform
     */
    void performSequence(Sequence* f_seq);
    
    /**
     * Mimicks an operation on the original permutation. This just changes 
     * the array m_simplePosition and writes the the operation to the result
     * vector.
     * @param f_operation  the operation to mimick
     */
    void mimickOperation(Operation&  f_operation);
     
    /**
     * Root node of the sorting algorithm; as long as the simple permutation 
     * is not yet sorted, a cycle will be choosen, a known case in the tree
     * will be searched for, and a sequence to solve the case will be 
     * performed.
     */
    void nodeRoot();

    /**
     * Greedy version of the root node; always searches for the best sequence
     * (but this is not an exhaustive search).
     */
    void nodeRootGreedy();
           
    /**
     * Lookahead version of the root node; always searches for the best 
     * sequence, use a lookahead of 1 (but this is not an exhaustive search).
     */
    void nodeRootLookahead();
    
    /**
     * Performs the lookahead for the Sequence f_seq. The return value is the 
     * best ratio delta sigma / w that the lookahead can find if we begin 
     * with the given sequence. f_depth is the used lookahead depth (so if it 
     * is 1 only the next sequences will be considered). Note that this is 
     * not an exhaustive search.
     * The sequence not passed by a reference but by a copy because we have 
     * to perform some changes on it and don't want to have side effects.
     * @param f_seq  the first sequence to perform
     * @param f_depth  depth of the lookahead
     * @return delta sigma / w for the best concatenation of sequences
     */
    float doLookahead(Sequence f_seq, int f_depth);
           
    /**
     * Node 000: c is an r-oriented 2-cycle.
     * @param f_c  cycle c
     */
    void node000(Cycle& f_c);
          
    /**
     * Node 001: c is a 3-twisted 3-cycle.
     * @param f_c  cycle c
     */     
    void node001(Cycle& f_c);
    
    /**
     * Node 002: c is a 2-twisted 3-cycle.
     * @param f_c  cycle c
     */     
    void node002(Cycle& f_c);
    
    /**
     * Node 003: c is an r-unoriented 2-cycle.
     * @param f_c  cycle c
     */
    void node003(Cycle& f_c);
    
    /**
     * Node 004: c is a nontwisted 3-cycle.
     * @param f_c  cycle c
     */
    void node004(Cycle& f_c);
    
    /**
     * Node 005: c is a 1-twisted 3-cycle.
     * @param f_c  cycle c
     */
    void node005(Cycle& f_c);
    
    /**
     * Node 006: c,d are intersecting r-unoriented 2-cycles.
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node006(Cycle& f_c, Cycle& f_d);
    
    /**
     * Node 007: c is an r-unoriented 2-cycle, d is a 1-twisted 3-cycle. c 
     * intersects the nontwisted chord of d. The edge of c that lies between
     * the nontwisted edges of d is m_edge[0].
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node007(Cycle& f_c, Cycle& f_d);
    
    /**
     * Node 008: c is an r-unoriented 2-cycle, d is a nontwisted 3-cycle. c 
     * and d are intersecting. The chord of d not intersected by c is between
     * the reality-edges m_edges[1] and m_edges[2]. m_edges[0] of c lies 
     * between m_edges[0] and m_edges[1] of d. 
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node008(Cycle& f_c, Cycle& f_d);
    
    /**
     * Node 009: c and e are r-unoriented 2-cycles, d is a nontwisted 3-cycle. 
     * c.m_edges[0] lies between the reality-edges m_edges[0] and m_edges[1] 
     * of d, e.m_edges[0] lies between m_edges[1] and m_edges[2] of d. 
     * c.m_edges[1] lies between m_edges[0] and m_edges[2] of d. The position
     * of e.m_edges[1] is arbitrary.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node009(Cycle& f_c, Cycle& f_d, Cycle& f_e);
    
    /**
     * Node 010: c is an r-unoriented 2-cycle, d and e are nontwisted 3-cycles.
     * c intersects d (m_edges[0] of c between m_edges[0] and m_edges[1] of 
     * d, m_edges[1] of c between m_edges[2] and m_edges[0] of d). e 
     * intersects d (m_edges[0] of e between m_edges[1] and m_edges[2]
     * of d, no edge of d between m_edges[1] and m_edges[2] of e).
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node010(Cycle& f_c, Cycle& f_d, Cycle& f_e);
     
    /**
     * Node 011: c is r-unoriented 2-cycle, d and e are nontwisted 3-cycles. 
     * The configuration looks like this:
     *         
     *           c0
     *       e2      e1
     *     
     *     d1          d0
     * 
     *       e0      c1  
     *           d2
     * 
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node011(Cycle& f_c, Cycle& f_d, Cycle& f_e);
     
    /**
     * Node 012: c and e are r-unoriented 2-cycles, d is a nontwisted 
     * 3-cycle. c.m_edges[0] lies between the reality-edges m_edges[0] and
     * m_edges[1] of d, c.m_edges[1] lies between the reality-edges 
     * m_edges[2] and m_edges[0] of d, e.m_edges[0] lies between m_edges[1] 
     * and m_edges[2] of d. The two 2-cycles are not intersecting. 
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node012(Cycle& f_c, Cycle& f_d, Cycle& f_e);
    
    /**
     * Node 013: c is r-unoriented 2-cycle, d and e are nontwisted 3-cycles. 
     * The configuration looks like this:
     *         
     *           c0
     *       d1      d0
     *     
     *     e0          e2
     * 
     *       d2      c1  
     *           e1
     * 
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node013(Cycle& f_c, Cycle& f_d, Cycle& f_e);

    /**
     * Node 014: c is an r-unoriented 2-cycle, d and e are nontwisted 3-cycles.
     * c intersects d (m_edges[0] of c between m_edges[0] and m_edges[1] of 
     * d, m_edges[1] of c between m_edges[2] and m_edges[0] of d). e 
     * intersects d (at least one edge of e between m_edges[1] and m_edges[2]
     * of d). The chord between m_edges[1] and m_edges[2] of e is neither 
     * intersected by c nor by d.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node014(Cycle& f_c, Cycle& f_d, Cycle& f_e); 
     
    /**
     * Node 015: c and f are an r-unoriented 2-cycle, d and e are nontwisted 
     * 3-cycles. c intersects d (m_edges[0] of c between m_edges[0] and 
     * m_edges[1] of d, m_edges[1] of c between m_edges[2] and m_edges[0] of 
     * d). e intersects d (at least one edge of e between m_edges[1] and 
     * m_edges[2] of d). The chord between m_edges[1] and m_edges[2] of e is 
     * neither intersected by c nor by d, but intersected by f (f.m_edges[0]
     * between the reality-edges). c and f are not intersecting; if d and f
     * are intersecting, no reality-edge of f is between d.m_edges[1] and 
     * d.m_edges[2].
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     * @param f_f  cycle f
     */
    void node015(Cycle& f_c, Cycle& f_d, Cycle& f_e, Cycle& f_f); 
     
    /**
     * Node 016: c and d are two interleaving nontwisted 3-cycles.
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node016(Cycle& f_c, Cycle& f_d);
    
    /**
     * Node 017: c and d are two intersecting, non-interleaving nontwisted 
     * 3-cycles. For both cycles, the non-intersected chord is between the 
     * reality-edges m_edges[1] and m_edges[2].
     * and d are intersecting
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node017(Cycle& f_c, Cycle& f_d);
    
    /**
     * Node 018: c, d and e are nontwisted 3-cycles. c intersects d 
     * (d.m_edges[0] between c.m_edges[0] and c.m_edges[1], others outside),
     * e intersects d (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]). 
     * There is no pair of interleaving cycles.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_d  cycle e
     */
    void node018(Cycle& f_c, Cycle& f_d, Cycle& f_e); 
     
    /**
     * Node 019: c and d are nontwisted 3-cycles, e is a 1-twisted 3-cycle. 
     * There is no pair of interleaving cycles. c intersects with d (with
     * the chords between reality-edges 0,1 and 2, 0 of both cycles). e 
     * intersects with d (at least one reality-edge of e between 
     * reality-edges 1 and 2 of d).
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node019(Cycle& f_c, Cycle& f_d, Cycle& f_e);

    /**
     * Node 020: c, d and e are nontwisted 3-cycles. c intersects d 
     * (d.m_edges[0] between c.m_edges[0] and c.m_edges[1], others outside),
     * e intersects d (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]). 
     * There is no pair of interleaving cycles. c and e are not intersecting.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_d  cycle e
     */
    void node020(Cycle& f_c, Cycle& f_d, Cycle& f_e); 

      
    /**
     * Node 021: c, d and e are nontwisted 3-cycles. c intersects d 
     * (d.m_edges[0] between c.m_edges[0] and c.m_edges[1], others outside),
     * e intersects d (e.m_edges[0] between d.m_edges[1] and d.m_edges[2]). 
     * There is no pair of interleaving cycles. c and e are intersecting (so 
     * the cycles are mutually intersecting). 
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_d  cycle e
     */
    void node021(Cycle& f_c, Cycle& f_d, Cycle& f_e); 

    /**
     * Node 022: c and d are nontwisted 3-cycles, e is a 1-twisted 3-cycle. 
     * There is no pair of interleaving cycles. c intersects with d (with
     * the chords between reality-edges 0,1 and 2, 0 of both cycles). e 
     * intersects with d (at least one reality-edge of e between 
     * reality-edges 1 and 2 of d). c and e are not intersecting.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */     
    void node022(Cycle& f_c, Cycle& f_d, Cycle& f_e);
    
    /**
     * Node 023: c and d are nontwisted 3-cycles, e is a 1-twisted 3-cycle. 
     * There is no pair of interleaving cycles. c intersects with d (with
     * the chords between reality-edges 0,1 and 2, 0 of both cycles). e 
     * intersects with d (at least one reality-edge of e between 
     * reality-edges 1 and 2 of d). c and e are intersecting (so the cycles
     * are mutually intersecting).
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node023(Cycle& f_c, Cycle& f_d, Cycle& f_e);

    /**
     * Node 024: c and d are two interleaving 1-twisted 3-cycles.
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node024(Cycle& f_c, Cycle& f_d);
     
    /**
     * Node 025: c and d are non-interleaving 1-twisted 3-cycles. d 
     * intersects the nontwisted chord of c.
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
     void node025(Cycle& f_c, Cycle& f_d);
     
    /**
     * Node 026: c is a 1-twisted 3-cycle, d is a nontwisted 3-cycle. d 
     * intersects the nontwisted chord of c. The chord of d that is not 
     * intersected by c lies between the reality edges m_edges[1] and 
     * m_edges[2].
     * and d are intersecting
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node026(Cycle& f_c, Cycle& f_d);
    
    /**
     * Node 027: c and d are two 1-twisted 3-cycles that form a 1-twisted 
     * pair. The twist of c is left (counterclockwise after) the twist of d.
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node027(Cycle& f_c, Cycle& f_d);
    
    /**
     * Node 028: c, d are intersecting, non-interleaving 1-twisted 3-cycles. 
     * Both cycles intersect the nontwisted chord of the other cycle. 
     * Reality-edge m_edges[0] of cycle d lies between reality-edges
     * m_edges[0] and m_edges[1] of cycle c.
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node028(Cycle& f_c, Cycle& f_d);
    
    /**
     * Node 029: c, d are intersecting, non-interleaving 1-twisted 3-cycles.
     * d intersects the nontwisted chord of c, but c doesn't intersect the
     * nontwisted chord of d
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node029(Cycle& f_c, Cycle& f_d);
     
    /** 
     * Node 030: c is 1-twisted 3-cycle, d is nontwisted 3-cycle, e is 
     * r-unoriented 2-cycle. d intersects the nontwisted chord of c, c does
     * not intersect the chord between m_edges[1] and m_edges[2] of d. This 
     * chord is intersected by e (with m_edges[0] between the reality-edges.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node030(Cycle& f_c, Cycle& f_d, Cycle& f_e);
    
    /**
     * Node 031: c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d 
     * intersects the nontwisted chord of c, c does not intersect the chord 
     * between m_edges[1] and m_edges[2] of d. This chord is intersected by 
     * e. There is no pair of interleaving cycles.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node031(Cycle& f_c, Cycle& f_d, Cycle& f_e);

    /** 
     * Node 032: c is 1-twisted 3-cycle, d is nontwisted 3-cycle, e is 
     * r-unoriented 2-cycle. d intersects the nontwisted chord of c. 
     * d.m_edges[1] and d.m_edges[2] lie between c.m_edges[0] and 
     * c.m_edges[1]. e intersects d (e.m_edges[0] between d.m_edges[1] and 
     * d.m_edges[2]), but does not intersect c.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node032(Cycle& f_c, Cycle& f_d, Cycle& f_e);
         
    /** 
     * Node 033: c is 1-twisted 3-cycle, d is nontwisted 3-cycle, e is 
     * r-unoriented 2-cycle. d intersects the nontwisted chord of c. 
     * d.m_edges[1] and d.m_edges[2] are outside of arc c.m_edges[0],
     * c.m_edges[1]. e intersects d (e.m_edges[0] between d.m_edges[1] and 
     * d.m_edges[2]), but does not intersect c.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node033(Cycle& f_c, Cycle& f_d, Cycle& f_e);

    /**
     * Node 034: c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d 
     * intersects the nontwisted chord of c, c does not intersect the chord 
     * between m_edges[1] and m_edges[2] of d. This chord is intersected by 
     * e. c and e are not intersecting.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node034(Cycle& f_c, Cycle& f_d, Cycle& f_e);

    /**
     * Node 035: c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d 
     * intersects the nontwisted chord of c, c does not intersect the chord 
     * between m_edges[1] and m_edges[2] of d. This chord is intersected by 
     * e. There is no pair of interleaving cycles. c and e are intersecting.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node035(Cycle& f_c, Cycle& f_d, Cycle& f_e);

    /**
     * Node036: c and d are two interleaving 1-twisted 3-cycles that do not
     * form a 1-twisted pair.
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node036(Cycle& f_c, Cycle& f_d);
    
    /**
     * Node 037: c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d 
     * intersects the nontwisted chord of c, c does not intersect the chord 
     * between m_edges[1] and m_edges[2] of d. This chord is intersected by 
     * a nontwisted chord of e. c and e are not intersecting.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node037(Cycle& f_c, Cycle& f_d, Cycle& f_e);

    /**
     * Node 038: c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d 
     * intersects the nontwisted chord of c, c does not intersect the chord 
     * between m_edges[1] and m_edges[2] of d. This chord is intersected by 
     * the two twisted chords of e. c and e are not intersecting.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node038(Cycle& f_c, Cycle& f_d, Cycle& f_e);
         
    /**
     * Node 039: c, e are 1-twisted 3-cycles, d is nontwisted 3-cycle. d 
     * intersects the nontwisted chord of c, c does not intersect the chord 
     * between m_edges[1] and m_edges[2] of d. This chord is intersected by 
     * e. There is no pair of interleaving cycles. c and e are intersecting 
     * with their twisted chords.
     * @param f_c  cycle c
     * @param f_d  cycle d
     * @param f_e  cycle e
     */
    void node039(Cycle& f_c, Cycle& f_d, Cycle& f_e);
    
    /**
     * Node 040: c is a nontwisted 3-cycle, d is a 1-twisted 3-cycle. c and d
     * are interleaving. The twisted edge of d lies between m_edges[1] and 
     * m_edges[2] of cycle c.
     * @param f_c  cycle c
     * @param f_d  cycle d
     */
    void node040(Cycle& f_c, Cycle& f_d);
    
     
    /***********************************************************************
     * debugging methods
     ***********************************************************************/

  public:    
    /**
     * Debugging stuff; changes permanently.
     */
    void debug();     
};

} // end namespace minswrt

#endif 
