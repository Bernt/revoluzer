/**
 * filename: revDist.cpp
 * author: Martin Bader
 * begin: 17.01.2007
 * last change: 17.08.2009
 *
 * Implementation of revDist.h
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
  
#include "revDist.h"

#include <stdlib.h>
#include <iostream>

namespace revDist
{
using namespace std;

/***************************************************************************
 * public methods
 ***************************************************************************/
 
RevDist::RevDist()
{
    m_state = 0;
    m_permSize = 0;
    m_mapped = NULL;
    m_element2Index = NULL;
    m_numCycles = 0;
    m_perm2Cycle = NULL;
    m_components = NULL;;
    m_compStarts = NULL;
    m_compEnds = NULL;
    m_compStack = NULL;
    m_chainStack = NULL;
    m_orientedStack = NULL;
}         


RevDist::~RevDist()
{
    delete[] m_mapped;
    delete[] m_element2Index;
    delete[] m_perm2Cycle;
    delete[] m_components;
    delete[] m_compStarts;
    delete[] m_compEnds;
    delete[] m_compStack;
    delete[] m_chainStack;
    delete[] m_orientedStack;
}


void RevDist::init(int f_size)
{
    m_permSize = f_size;
    m_state = 1;    
    m_mapped = new int[f_size];
    m_element2Index = new int[f_size + 1];
    m_numCycles = 0;
    m_perm2Cycle = new int[f_size];
    m_components = new IntPair[f_size];    
    m_compStarts = new bool[f_size];
    m_compEnds = new bool[f_size];
    m_compStack = new int[f_size];
    m_chainStack = new int[f_size];
    m_orientedStack = new bool[f_size];
}


int RevDist::getDistance(const int* f_perm1, const int* f_perm2)
{
    if (m_state != 1)
    {
        cerr << "RevDist not initialized! Aborting getDistance\n";
        return -1;
    }
    if (m_permSize == 1)   // for this case, some arrays are to small -> handle it as trivial case
        return 0;
    mapPermutations(f_perm1, f_perm2);
    markCycles();
    findComponents();
    return m_permSize - m_numCycles + calcHurdles();  // calcHurdles also calculates the fortress indicator
}


/***************************************************************************
 * public methods
 ***************************************************************************/

void RevDist::mapPermutations(const int* f_perm1, const int* f_perm2)
{
    // use element2Index as mapping
    for (int i = 0; i < m_permSize; i++)
    {
        if (f_perm2[i] > 0)
            m_element2Index[f_perm2[i]] = i+1;
        else
            m_element2Index[-f_perm2[i]] = -(i+1);
    }
    // map perm1
    for (int i = 0; i < m_permSize; i++)
    {
        if (f_perm1[i] > 0)
            m_mapped[i] = m_element2Index[f_perm1[i]];
        else
            m_mapped[i] = -m_element2Index[-f_perm1[i]];
    }
    // set element2Index according to the mapping
    for (int i = 0; i < m_permSize; i++)
        m_element2Index[abs(m_mapped[i])] = i;
/*        
    cerr << "mapped: ";
    for (int i = 0; i < m_permSize; i++)
        cerr << m_mapped[i] << " ";
    cerr << endl << "element2Index: ";
    for (int i = 0; i <= m_permSize; i++)
        cerr << m_element2Index[i] << " ";
    cerr << endl;*/
}

 
void RevDist::markCycles()
{
    int edge;         // the actual RE where we are
    int element;      // next element to find
    bool positiv;     // true if we walk in positiv direction
    
    /** 
     * Gives each cycle a label, and marks for each RE the cycle it belongs
     * to. Stores to each cycle begin- and endpoint. This sets the initial
     * values of m_numCycles, m_perm2Cycle, and m_components.
     */
    m_numCycles = 0;
    for (int i = 0; i < m_permSize; i++)        // mark all RE as not yet visited
         m_perm2Cycle[i] = -1;
    for (int i = 0; i < m_permSize; i++)        // start with each RE
    {     
        if (m_perm2Cycle[i] != -1)              // RE already visited
            continue;
        edge = i;
        positiv = true;
        m_components[m_numCycles].first = i;    
        m_components[m_numCycles].second = i;    
        while (m_perm2Cycle[edge] == -1)        // walk through cycle
        {
            m_perm2Cycle[edge] = m_numCycles; 
            if (edge > m_components[m_numCycles].second)
                m_components[m_numCycles].second = edge;
            // search next element
            if (positiv)                        
            {
                element = m_mapped[edge];
                if (element < 0)
                {
                    element = (element == -m_permSize? 1 : -element + 1);
                    positiv = false;
                }
                else
                {
                    element = (element == 1? m_permSize : element - 1);
                    positiv = true;
                }
            }
            else
            {
                element = (edge == 0? m_mapped[m_permSize - 1] : m_mapped[edge - 1]);
                if (element < 0)
                {
                    element = (element == -1? m_permSize : -element - 1);
                    positiv = true;
                }
                else
                {
                    element = (element == m_permSize? 1 : element + 1);
                    positiv = false;
                }
            }
            edge = m_element2Index[element];
            if (positiv ^ (m_mapped[edge] > 0))       // correct edge
                positiv = false;
            else                                      // walk one edge to the right
            {
                edge = (edge == m_permSize - 1? 0 : edge + 1);
                positiv = true;
            }
        }    
/*        cerr << " (" << m_components[m_numCycles].first << " - " 
          << m_components[m_numCycles].second << endl;*/
        m_numCycles++;
    }            
    
//    cout << "#cycles: " << m_numCycles << endl;
}


void RevDist::findComponents()
{
    int stacksize = 0;           // size of the stack
    int extent;                  // max edge index in component
    
    // init some vars
    for (int i = 0; i < m_permSize; i++)
        m_compStarts[i] = false;
    for (int i = 0; i < m_permSize; i++)
        m_compEnds[i] = false;
    // get for each RE the component
    for (int i = 0; i < m_permSize; i++)
    {
        if (i == m_components[m_perm2Cycle[i]].first)              // start of a cycle
            m_compStack[stacksize++] = m_perm2Cycle[i];           // push cycle
        extent = m_components[m_perm2Cycle[i]].second;
        while (m_components[m_compStack[stacksize-1]].first > m_components[m_perm2Cycle[i]].first)
        {
            if (m_components[m_compStack[stacksize-1]].second > extent)
                extent = m_components[m_compStack[stacksize-1]].second;
            stacksize--;
        }
        if (m_components[m_compStack[stacksize-1]].second < extent)
            m_components[m_compStack[stacksize-1]].second = extent;
        if (i == m_components[m_compStack[stacksize-1]].second)   // component found
        {
            m_compStarts[m_components[m_compStack[stacksize-1]].first] = true;
            m_compEnds[i] = true;
            stacksize--;
        }
    }
}


int RevDist::calcHurdles()
{
    int shortpaths = 0;           // number of short paths
    int longpaths = 0;            // number of long paths
    int highestbranch = m_permSize+1;      // height of the highest branching node
    bool hbiscomp = false;        // true if the highest branch is a component
    int highestunor = m_permSize+1;        // height of the highest unoriented component
    int secondunor = m_permSize+1;         // height of the second highest unoriented component
    int numunor = 0;              // number of unoriented components
    int depth = 0;                // the current depth
    int state;                    // state of a component
    
    m_chainStack[0] = 0;          // the parent chain
    for (int i = 0; i < m_permSize; i++)
    {
        if (m_compStarts[i])      // start of a new component
        {
            depth++;
            m_compStack[depth] = 0;
            m_chainStack[depth] = 0;
            m_orientedStack[depth] = false;
        }
        if (m_mapped[(i+m_permSize-1)%m_permSize] * m_mapped[i] < 0)   // component is oriented
            m_orientedStack[depth] = true;
        if (m_compEnds[i])         // end of a component
        {
            // don't count adjacencies as unoriented components
            if (m_compStarts[i])
                m_orientedStack[depth] = true;
            if (!m_orientedStack[depth])
            {
                numunor++;
                if (depth < highestunor)
                {
                    secondunor = highestunor;
                    highestunor = depth;
                }
                else if (depth < secondunor)
                    secondunor = depth;
            }
            // also end child chain
            if (m_compStack[depth] == 0)
                m_compStack[depth] = m_chainStack[depth];
            else if (m_chainStack[depth] != 0)
            {
                // node becomes branching - report child paths
                if (m_chainStack[depth] == 1)
                    shortpaths++;
                else if (m_chainStack[depth] == 2)
                    longpaths++;
                if (m_compStack[depth] == 1)
                    shortpaths++;
                else if (m_compStack[depth] == 2)
                    longpaths++;
                m_compStack[depth] = 4;
                if (depth <= highestbranch)
                {
                    highestbranch = depth;
                    hbiscomp = true;
                }
            }
            // get the state of this component
            if (!m_orientedStack[depth] && (m_compStack[depth] <= 1))
                state = m_compStack[depth] + 1;
            else 
                state = m_compStack[depth];
            // report to parent chain
            depth--;
            if (m_chainStack[depth] == 0)
            {
                if (state == 4)
                    m_chainStack[depth] = 3;
                else
                    m_chainStack[depth] = state;
            }
            else if (state != 0)    
            {
                // chain becomes branching - report child paths
                if (state == 1)
                    shortpaths++;
                else if (state == 2)
                    longpaths++;
                if (m_chainStack[depth] == 1)
                    shortpaths++;
                else if (m_chainStack[depth] == 2)
                    longpaths++;
                m_chainStack[depth] = 4;
                if (depth < highestbranch)    
                {
                    highestbranch = depth;
                    hbiscomp = false;
                }
            }
            if ((i != m_permSize - 1) && !m_compStarts[i+1])    // also end the parent chain
            {
                if (m_compStack[depth] == 0)
                    m_compStack[depth] = m_chainStack[depth];
                else if (m_chainStack[depth] != 0)
                {
                    // node becomes branching - report child paths
                    if (m_chainStack[depth] == 1)
                        shortpaths++;
                    else if (m_chainStack[depth] == 2)
                        longpaths++;
                    if (m_compStack[depth] == 1)
                        shortpaths++;
                    else if (m_compStack[depth] == 2)
                        longpaths++;
                    m_compStack[depth] = 4;
                    if (depth <= highestbranch)
                    {
                        highestbranch = depth;
                        hbiscomp = true;
                    }
                }
                // create new parent chain
                m_chainStack[depth] = 0;
            }
            if (i == m_permSize - 1)                          // end last chain
            {
                if (m_chainStack[depth] == 1)
                    shortpaths++;
                else if (m_chainStack[depth] == 2)
                    longpaths++;
                else if (m_chainStack[depth] == 4)    
                {
                    highestbranch = depth;
                    hbiscomp = false;
                }
                
            }
        }
    }
    // check root path
    if (highestbranch != m_permSize+1)
    {
        if (!hbiscomp)
            highestbranch++;
        if (highestunor < highestbranch)
        {
            if (secondunor < highestbranch)
                longpaths++;
            else
                shortpaths++;
        }
    }
/*
    cerr << "#unor: " << numunor << endl;        
    cerr << "long: " << longpaths << endl;
    cerr << "short: " << shortpaths << endl;
  */
    if (((longpaths & 1) == 1) && (shortpaths == 0))
        return longpaths + 1;
    else        
        return longpaths + shortpaths;
}


/***************************************************************************/

} // end namespace revDist
