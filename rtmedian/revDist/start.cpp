/**
 * filename: start.cpp
 * author: Martin Bader
 * begin: 17.01.2007
 * last change: 23.08.2010
 *
 * Start file. Small UI.
 * 
 * Copyright (C) 2007-2010  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
  
#include "revDist.h"

#include <fstream>
#include <iostream>
  
using namespace std;

/**
 * Reads a permutation from an input file. The array will be written to 
 * f_origin and f_target (which are references of a pointer), and the 
 * permutation size will be returned.
 * @param f_filename  the name of the file
 * @param f_origin  reference of the origin data pointer
 * @param f_target  reference of the target data pointer
 * @return  permutation size
 */
int readFile(const char* f_filename, int*& f_origin, int*& f_target)
{
    ifstream input;       // file input stream
    int size;             // permutation size
    
    input.open(f_filename);    
    if (!input.is_open())
    {
        cerr << "error while opening the file " << f_filename << endl;
        return -1;
    }
    while (input.peek() == '#')        // line is a comment
        input.ignore(999, '\n');
    input >> size;
    input.ignore(999, '\n');
    f_origin = new int[size];
    f_target = new int[size];
    while (input.peek() == '#')        // line is a comment
        input.ignore(999, '\n');
    for (int i = 0; i < size; i++)
        input >> f_origin[i];
    input.ignore(999, '\n');
    while (input.peek() == '#')        // line is a comment
        input.ignore(999, '\n');
    for (int i = 0; i < size; i++)
        input >> f_target[i];
    input.close();     
    return size;
}


int main(int f_argc, char** f_argv)
{
    revDist::RevDist rd;
    int size = 0;
    int* perm1, *perm2;
    int dist;
    
    cout << "*****************************************************************\n";
    cout << "revDist v1.0.2  Copyright (C) 2007-2010  Martin Bader\n";
    cout << "This program comes with ABSOLUTELY NO WARRANTY.\n";
    cout << "This is free software, and you are welcome to redistribute it\n";
    cout << "under certain conditions.\n";
    cout << "For details about the licence, read the file gpl.txt\n";
    cout << "*****************************************************************\n";
    if (f_argc != 2)
    {
        cout << "Usage: revDist infile\n";
        return 0;
    }
    cout << "reading file\n";
    size = readFile(f_argv[1], perm1, perm2);
    rd.init(size);
    dist = rd.getDistance(perm1, perm2);        
    cout << "dist: " << dist << endl;
    return 0;    
}
