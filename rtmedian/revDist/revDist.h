/**
 * filename: revDist.h
 * author: Martin Bader
 * begin: 17.01.2007
 * last change: 23.08.2007
 *
 * Calculates the reversal distance between two genomes.
 * 
 * Copyright (C) 2007  Martin Bader
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
#ifndef REVDIST_H_
#define REVDIST_H_


namespace revDist
{


/**
 * Pair of integers.
 */
struct IntPair
{
    int first;
    int second;
};


/**
 * The main class.
 */
class RevDist
{
    /***********************************************************************
     * member variables
     ***********************************************************************/
  public:
            
  private:
    /** state of the program; 0: not yet initialized; 1: initialized */
    int m_state;
    
    /** size of the permutations */
    int m_permSize;
    
    /** the mapped permutation 1 (perm 2 is mapped to id) */
    int* m_mapped;
    
    /** indicates the index for each element */
    int* m_element2Index;
     
    /** the number of cycles */
    int m_numCycles;
    
    /** maps each RE to a cylce */
    int* m_perm2Cycle;
    
    /** stores for each component start- and endpoint */
    IntPair* m_components;
    
    /** Indicates for each RE in the permutation wether it is a start point of 
     * a component. */
    bool* m_compStarts;
    
    /** Indicates for each RE in the permutation wether it is a end point of 
     * a component. */
    bool* m_compEnds;
    
    /** stack of component indices */
    int* m_compStack;
    
    /** stack of chains */
    int* m_chainStack;
    
    /** stack indicates which components are oriented */
    bool* m_orientedStack;
    
                  
    /***********************************************************************
     * methods
     ***********************************************************************/     
  public:
    /**
     * Constructor.
     */
    RevDist();
        
    /**
     * Destructor.
     */
    ~RevDist();
    
    /**
     * Initializes the member variables. Call this after the constructor.
     * @param f_size  permutation size
     */
    void init(int f_size); 
    
    /** 
     * Calculates the distance between f_perm1 and f_perm2. Both permutations
     * must be of the size determined by calling init().
     * @param f_perm1  the source permutation
     * @param f_perm2  the target permutation
     * @return  the reversal distance
     */
    int getDistance(const int* f_perm1, const int* f_perm2);
    
  private:
  
    /** 
     * Maps the permutations such that f_perm2 is the id. The mapping of 
     * f_pem1 will be written to m_mapping. This also initializes 
     * m_element2Index
     */
    void mapPermutations(const int* f_perm1, const int* f_perm2);
    
    /** 
     * Gives each cycle a label, and marks for each RE the cycle it belongs
     * to. Stores to each cycle begin- and endpoint. This sets the initial
     * values of m_numCycles, m_perm2Cycle, and m_components.
     */
    void markCycles();
    
    /**
     * Finds all components, and fills their start/endpoints in m_compStarts
     * and m_compEnds.
     */
    void findComponents();

    /**
     * Calculates the number of hurdles (h) + the fortress indicator (f).
     * Returns h + f.
     * @return h + f
     */
    int calcHurdles();
};


/***************************************************************************/

} // end namespace revDist

#endif /*REVDIST_H_*/
