/*
 * test-rrrmt.cpp
 *
 * test aspects of the rearrangement operations
 * 1) apply followed by deapply should give the same as before
 *    this is tested for all: single gene deletions, inversions,
 *    transpositions, inverse transpositions, TDRLs
 *    permutation length is set to N (because of 2^N possible TDRLs
 *    N should not be set to high)
 *  Created on: Nov, 2011
 *      Author: maze
 */

#include <algorithm>
#include <iostream>
#include <stdlib.h>

#include "counter.hpp"
#include "rearrangements.hpp"

#define N 10
#define NRTESTS 10000
using namespace std;

int main( int argc, char *argv[] ){
	int //circular =0,
		n=N,
		k;
	genom g1, g2;
	rrrmt *rs = NULL;
	vector<genom> trace;
	vector<float> prob(4,0);

	init_rng( 42 );
	g2 = genom( n, 0 );
	// single gene indel
	cerr << "test (apply/deapply): single gene deletions .. ";
	for( unsigned i = 0; i<(unsigned)n; i++ ){
		g1 = genom( n, 0 );

		rs = new indel(i, true, g1);

//		cout << *rs << endl;
		rs->apply( g1 );
		rs->deapply( g1 );
		if( g1 != g2 ){
			cerr << endl << "error: "<<g1<<" != "<<g2<<endl;
			exit(EXIT_FAILURE);
		}
		delete rs;
	}
	cerr << "success"<<endl;
	cerr << "test (apply/deapply): all inversions .. ";
	for( unsigned i=0; i<(unsigned)n; i++ ){
		for( unsigned j=i; j<(unsigned)n; j++ ){
			g1 = genom( n, 0 );
			rs = new rev(i, j, g1);
			rs->apply(g1);
			rs->deapply(g1);
			if( g1 != g2 ){
				cerr << endl << "error: "<<g1<<" != "<<g2<<endl;
				exit(EXIT_FAILURE);
			}

			delete rs;
		}
	}
	cerr << "success"<<endl;
	cerr << "test (apply/deapply): all transpositions .. ";
	for( unsigned i=0; i<(unsigned)n; i++ ){
		for( unsigned j=i+1; j<(unsigned)n; j++ ){
			for( unsigned k=j+1; k<=(unsigned)n; k++ ){

				g1 = genom( n, 0 );
				rs = new transp(i,j,k, g1);
//				cout << i<< " "<<j<<" " <<k<<endl;
//				cout << *rs<<endl;
				rs->apply(g1);
				rs->deapply(g1);
				if( g1 != g2 ){
					cerr << endl << "error: "<<g1<<" != "<<g2<<endl;
					exit(EXIT_FAILURE);
				}

			delete rs;
			}
		}
	}
	cerr << "success"<<endl;
	cerr << "test (apply/deapply): all inverse transpositions .. ";
	for( unsigned i=0; i<(unsigned)n; i++ ){
		for( unsigned j=i; j<(unsigned)n; j++ ){
			for( unsigned k=0; k<(unsigned)n; k++ ){
//				cout << i<< " "<<j<<" " <<k<<endl;
				g1 = genom( n, 0 );
				if( k<i ){
					rs = new revtransp(i,j, k, i-1, g1);
				}else if( k>j ){
					rs = new revtransp(i,j, j+1, k, g1);
				}else{
					continue;
				}

//				cout << *rs<<endl;
				rs->apply(g1);
				rs->deapply(g1);
				if( g1 != g2 ){
					cerr << endl << "error: "<<g1<<" != "<<g2<<endl;
					exit(EXIT_FAILURE);
				}
			delete rs;
			}
		}
	}
	cerr << "success"<<endl;

	cerr << "test (apply/deapply): all TDRLs .. ";
	counter cnt( n, 2, 0, false );
	vector<bool> bcnt(n);
	do{
		g1 = genom( n, 0 );
		for( unsigned i=0; i<(unsigned)n; i++ ){
			bcnt[i] = ( cnt[i]>0 ) ? true : false;
		}
//		copy( bcnt.begin(), bcnt.end(), ostream_iterator<bool>(cerr,"") ); cerr<<endl;
		rs = new tdrl(bcnt, g1);
//		cerr << *rs <<endl;
//		cerr << "apply" <<endl;
		rs->apply(g1);
//		if( g1 == g2 ){
//			delete rs;
//			cnt++;
//			continue;
//		}
//		cerr << "deapply" <<endl;
		rs->deapply(g1);
//		cerr << g1<<endl;
		if( g1 != g2 ){
			cerr << endl << "error: "<<g1<<" != "<<g2<<endl;
			exit(EXIT_FAILURE);
		}
		delete rs;
		cnt++;
	}while( cnt.isvalid() );
	cerr << "success"<<endl;

	cerr << "test (apply/deapply): random scenarios .. ";
	for( unsigned r = 0; r<NRTESTS; r++ ){
			n = ask_rng(10, 100);
			k = ask_rng(1, n/4);

			g1 = genom( n, 0 );
			g2 = genom( n, 0 );

			prob.assign( 4, 0 );
			for( unsigned j = 0; j<100; j++ ){
				prob[ ask_rng(0,3) ]+=1;
			}

			rs = new ordered(k, prob, g2, trace);
			rs->apply( g1 );
			if( g1 != g2 ){
				cerr << endl << "error: "<<g1<<" != "<<g2<<endl;
				exit(EXIT_FAILURE);
			}
			g2 = genom( n, 0 );
			rs->deapply( g1 );
			if( g1 != g2 ){
				cerr << endl << "error: "<<g1<<" != "<<g2<<endl;
				exit(EXIT_FAILURE);
			}
			delete rs; rs=NULL;
		}
	cerr << "success"<<endl;
	return EXIT_SUCCESS;
}
