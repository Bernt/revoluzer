/*
 * test-crex.cpp
 *
 * consistency checks for break point distance
 * - between X pairs of random permutations of length in 10:100
 * - between X pairs of random permutations of length in 10:100
 *   where random elements [1:n/4] are deleted from one of the permutations
 * - between X pairs of random permutations of length in 10:100
 *   where random elements [1:n/4] are deleted from both permutations
 *
 *  Created on: May 31, 2010
 *      Author: maze
 */

#include <algorithm>
#include <iostream>
#include <stdlib.h>

#include "dstnc_bp.hpp"
#include "rearrangements.hpp"
#include "rrrmtrand.hpp"

#define NRTESTS 10000

using namespace std;

int main( int argc, char *argv[] ){
//	int circular =0;
	int n,
		k,
		o;	// operation
	genom g1, g2;
	rrrmt *rs = NULL;
	vector<genom> trace;
	vector<float> prob(4,1.0);

	init_rng( 42 );

	// equal gene content
	for( unsigned r = 0; r<NRTESTS; r++ ){
		n = ask_rng(10, 100);
		k = ask_rng(1, n);

		dstnc_bp dst( n, true, false );

		g1 = genom( n, 0 );
		g2 = genom( n, 0 );
		rs = new rrrmt_rand(k, prob, g2, trace, true, std::numeric_limits< unsigned >::max());

		if( dst.calc(g1, g2) != dst.calc(g2, g1) ){
			cerr << "asymmetric breakpoint distance: "<<dst.calc(g1, g2)<<" != "<<dst.calc(g2, g1)<<endl;
			cerr <<">g1"<<endl<< g1 << endl;
			cerr <<">g2"<<endl<< g2 << endl;
			return EXIT_FAILURE;
		}
		delete rs;
	}

	// elements deleted from one of the permutations
	for( unsigned r = 0; r<NRTESTS; r++ ){
		n = ask_rng(10, 100);
		k = ask_rng(1, n);

		dstnc_bp dst( n, true, false );

		g1 = genom( n, 0 );
		g2 = genom( n, 0 );
		rs = new rrrmt_rand(k, prob, g2, trace, true, std::numeric_limits< unsigned >::max());

		o = ask_rng(1, n/4);
		for( unsigned i=0; i<(unsigned)o; i++){
			g1.chromosom.erase( g1.begin()+ask_rng(0, g1.size()-1) );
		}

		if( dst.calc(g1, g2) != dst.calc(g2, g1) ){
			cerr << "asymmetric breakpoint distance: "<<dst.calc(g1, g2)<<" != "<<dst.calc(g2, g1)<<endl;
			cerr <<">g1"<<endl<< g1 << endl;
			cerr <<">g2"<<endl<< g2 << endl;
			return EXIT_FAILURE;
		}
		delete rs;
	}

	// elements deleted from both permutations
	for( unsigned r = 0; r<NRTESTS; r++ ){
		n = ask_rng(10, 100);
		k = ask_rng(1, n);

		dstnc_bp dst( n, true, false );

		g1 = genom( n, 0 );
		g2 = genom( n, 0 );
		rs = new rrrmt_rand(k, prob, g2, trace, true, std::numeric_limits< unsigned >::max());

		o = ask_rng(1, n/4);
		for( unsigned i=0; i<(unsigned)o; i++){
			g1.chromosom.erase( g1.begin()+ask_rng(0, g1.size()-1) );
		}
		o = ask_rng(1, n/4);
		for( unsigned i=0; i<(unsigned)o; i++){
			g2.chromosom.erase( g2.begin()+ask_rng(0, g2.size()-1) );
		}

		if( dst.calc(g1, g2) != dst.calc(g2, g1) ){
			cerr << "asymmetric breakpoint distance: "<<dst.calc(g1, g2)<<" != "<<dst.calc(g2, g1)<<endl;
			cerr <<">g1"<<endl<< g1 << endl;
			cerr <<">g2"<<endl<< g2 << endl;
			return EXIT_FAILURE;
		}
		delete rs;
	}

	return EXIT_SUCCESS;
}
