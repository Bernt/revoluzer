/*
 * test-baderrt.cpp
 *
 * consistency checks for Bader's rearrangement reconstruction
 * 1 create singe rearrangements (reversal/transposition/reverse transposition)
 *   and reconstruct .. check for equality
 * 2 create random scenarios and check for the reconstruction of a scenario
 *
 *  Created on: May 31, 2010
 *      Author: maze
 */

#include <algorithm>
#include <getopt.h>
#include <iostream>
#include <stdlib.h>
#include <typeinfo>

#include "baderrt.hpp"
#include "costfoo.hpp"
#include "rrrmtrand.hpp"

#define NRTESTS 1000

using namespace std;

/**
 * get command line options
 * @param[out] crexone use crex1
 */
void getoptions( int argc, char *argv[], string &folder );

void usage();

int main( int argc, char *argv[] ){
	costfoo *cst;
//	int circular =0;
	int n,
		k,
		o;		// operation;
	genom g1, g2;
	string folder;
	rrrmt *rs = NULL, *cs = NULL, *s = NULL;
	vector<genom> trace;
	vector<float> prob(4,0);

	getoptions(argc, argv, folder);

	init_rng( 42 );


//	for( unsigned r = 0; r<NRTESTS; r++ ){
//		int n=10;
//		itnode *itroot;
//		interval_tree_random(&itroot, NULL, make_pair(0,n-1), 2, 4);
//		genom id(n,0), f;
////		interval_tree_print( itroot, id, cout ); cout << endl;
//		front(itroot, id, f);
//		cst = new costfoo_equi();
//
//
//		cs = new crex2(id,f, true, cst, true);
//		rs = new crex2(id,f, true, cst);
//		cerr << "  complete "; cs->output(cerr, 0, 1); cerr <<endl;
//		cerr << "incomplete "; rs->output(cerr, 0, 1); cerr <<endl;
//
//		s = (unordered*)cs->intersect( rs );
//		cerr << "           "; s->output(cerr, 0, 1); cerr <<endl;
//		if( rs->length( ALTMAX ) != s->length( ALTMAX ) || cs->length( ALTMAX ) != s->length( ALTMAX ) ){
//			cerr <<"crex2 implementations disagree"<<endl;
//			cerr <<"crex2 compl "<<endl<< *cs << endl;
//			cerr <<"crex2 limit "<<endl<< *rs << endl;
//			cerr <<"            "<<endl<< *s << endl;
//			cerr << rs->length( ALTMAX )<<" " <<s->length( ALTMAX )<<" "<< cs->length( ALTMAX )<<endl;
//			return EXIT_FAILURE;
//		}
//
//		cs->apply( id );
//		if (id != f){
//			cerr <<"crex2 application disagrees"<<endl;
//			return EXIT_FAILURE;
//		}
//
//		delete cst;
//		delete s;
//		delete cs;
//		delete rs;
//		interval_tree_free(itroot);
//		cout << r << f<<endl;
//	}


//
	cst = new costfoo_equi();
	for( unsigned r = 0; r<NRTESTS; r++ ){
		n = 10;
//		n= ask_rng(10, 100);
		k = 1;
		o = ask_rng(0, 2);

		g1 = genom( n, 0 );
		g2 = genom( n, 0 );

		prob.assign( 4, 0 );
		prob[o]=100;

		rs = new rrrmt_rand(k, prob, g2, trace, true, std::numeric_limits< unsigned >::max());
//		if( tmp != NULL ){
////			rs->insert( tmp );
//		}
//		cerr << g1<<endl;
//		cerr << g2<<endl;
		cs = new baderrt(g1, g2, cst, true, false);

		s = (unordered*)cs->intersect( rs );

		if( s->length( ALTMAX ) != 1 ){
			cerr << "intersection of size != 1 found"<<endl;
			cerr <<"random "<<endl<< *rs << endl;
			cerr <<"baderrt"<<endl<< *cs << endl;
			cerr <<"cut    "<<endl<< *s << endl;
			return EXIT_FAILURE;
		}

		delete s; s=NULL;
		delete cs; cs=NULL;
		delete rs; rs=NULL;
	}

	cerr << "random single rearrangement -> passed "<<endl;

	for( unsigned r = 0; r<NRTESTS; r++ ){
//		n = ask_rng(10, 100);
		n = 10;
		k = ask_rng(1, n/4);

		g1 = genom( n, 0 );
		g2 = genom( n, 0 );

		prob.assign( 4, 0 );
		for( unsigned j = 0; j<100; j++ ){
			prob[ ask_rng(0,2) ]+=1.0;
		}

		rs = new rrrmt_rand(k, prob, g2, trace, true, std::numeric_limits< unsigned >::max());
		cerr << *rs << endl;
		//		cerr << g1<<endl<<g2<<endl;
		cs = new baderrt(g1, g2, cst, true, false);

		cs->apply( g1 );
		if( g1 != g2 ){
			cerr << "reconstruction failed"<<endl;
			cerr <<"random "<<endl<< *rs << endl;
			cerr <<"baderrt"<<endl<< *cs << endl;
			cerr <<"cut    "<<endl<< *s << endl;
			return EXIT_FAILURE;
		}

		delete s; s=NULL;
		delete cs; cs=NULL;
		delete rs; rs=NULL;
//		cerr << "===="<<endl;
	}
	cerr << "random multiple rearrangement -> passed "<<endl;
	delete cst;
	return EXIT_SUCCESS;
}

void getoptions( int argc, char *argv[], string &folder ){
	int c;

	while (1) {
		static struct option long_options[] = {
			{"folder",  required_argument, 0, 'f'},
			{0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:", long_options, &option_index);
        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'f':
				cerr << optarg<<endl;
				folder = optarg;
				break;
			case 'h':
				usage();
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}
}

void usage(){
	cerr << "test crex / crex2"<<endl;
	cerr << "test-crex"<<endl;
	cerr << "--crex1: test crex1 (default crex2)"<<endl;
	cerr << "--folder: folder containing the test files"<<endl;
	exit(EXIT_FAILURE);
}
