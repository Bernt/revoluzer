/*
 * test-pimp.cpp
 *
 * compare the results of ecip and tcip on random instances
 *
 *  Created on: May 17, 2010
 *      Author: maze
 */

#define NRTESTS 10000

#include <algorithm>
#include <iostream>
#include <stdlib.h>

#include "caprara.hpp"
#include "helpers.hpp"
#include "rearrangements.hpp"
#include "rrrmtrand.hpp"

using namespace std;

int main( int argc, char *argv[] ){
//	int circular =0;
	int n,
		k;
	unordered *rs;
	int tcipmedcnt,
		tcipscore,
		ecipmedcnt,
		ecipscore;
	vector<genom> genomes(3),
		trace,
		tcipmedians,
		ecipmedians;
	vector<float> prob(4,0);
	prob[0] = 100;
	dstnc *pinv;

	init_rng( 42 );

	for( unsigned r = 0; r<NRTESTS; r++ ){
		n = ask_rng(10, 30);
		k = ask_rng(1, n/3);
		if( (r+1)%100 == 0 ){
			cout <<r+1<<"/"<< NRTESTS<<"\r";
			cout.flush();
		}

		pinv = new dstnc_pinv(n);
		for( unsigned i=0; i<3;i++ ){
			genomes[i] = genom( n, 0 );
			rs = new rrrmt_rand(k, prob, genomes[i], trace, true, std::numeric_limits< unsigned >::max());
			trace.clear();
			delete rs;
		}

		tcipmedcnt = 0;
		tcipscore = 0;
		tcipmedians.clear();
		tcip( genomes, 1, tcipmedcnt, tcipscore,  tcipmedians);

		int pair_pd = 0;

//		caprara(genomes, dynamic_restrictions, conserved, common, pair_pd,
//			sig, circular, lower_bound, upper_bound,
//			allmed, lazy,
//			&mediancnt, medians);
		caprara( genomes, 0, 0, 1, pair_pd,
				0, 0, NO_BOUND, NO_BOUND,
				1, 0, ecipmedcnt,
				ecipmedians);

		ecipscore = pinv->calcsum(ecipmedians[0], genomes, genomes);

		if( tcipmedcnt != ecipmedcnt || tcipscore != ecipscore || tcipmedians != ecipmedians){
			cout << ">1"<<endl<<genomes[0]<<endl<< ">2"<<endl<<genomes[1]<<endl<< ">3"<<endl<<genomes[2]<<endl;
			cout << "#tcip "<<tcipmedcnt<<" medians of score "<<tcipscore<<endl;
			cout << "#ecip "<<ecipmedcnt<<" medians of score "<<ecipscore<<endl;
			cout << "#tcip medians:"<<endl;
			cout << tcipmedians<<endl;
			cout << "#ecip medians:"<<endl;
			cout << ecipmedians<<endl;
			exit(EXIT_FAILURE);
		}
		delete pinv;

	}
	return EXIT_SUCCESS;
}
