/*
 * test-pqrtree.cpp
 *
 * insert x random substrings of the identity permutation of length n into the
 * a pqr tree and test for R nodes (which should not appear)
 * - n is chosen at random from [10:100]
 * - x is chosen at random from [1:2n]
 * - the substrings are determined by randomly choosing start and end point
 *
 *  Created on: Aug 11, 2010
 *      Author: maze
 */

#define NRTESTS 1000000

#include <iostream>
#include <stdlib.h>

#include "pqrtree.hpp"

using namespace std;

int main( int argc, char *argv[] ){

	pqrtree<int> *pqr = NULL;
	unsigned n, k, s, e;
	set<int> tmp;


	init_rng( 42 );
	// take x? random substrings of the identity of length random length
	// and insert them in a pqrtree. check if the insertion is successful
	for( unsigned r = 0; r<NRTESTS; r++ ){
		if( (r+1)%1000 == 0 ){
			cout <<r+1<<"/"<< NRTESTS<<"\r";
			cout.flush();
		}

		n = ask_rng(10, 100);
		k = ask_rng(1, 2*n);

		pqr = new pqrtree<int>( 1, n );

//		cerr << "N = "<< n<<endl;
		// CREATE empty pqtree for n elements

		for(unsigned j=0; j<k; j++){
			s = ask_rng(1, n);
			e = ask_rng(1, n);

			if (s < e){
				swap(s,e);
			}

//			cerr << "S = "<<s<<" E = "<<e<<endl;
			for( int l=s; l<=e; l++ ){
				tmp.insert(l);
			}

			// INSERT SET
			if( ! pqr->reduce( tmp.begin(), tmp.end() ) ){
				cerr << "could not insert ";
				copy( tmp.begin(), tmp.end(), ostream_iterator<int>( cerr, " " ) );cerr<< endl;
			}

			tmp.clear();
		}

		delete pqr;

	}
	cout <<endl;

	return EXIT_SUCCESS;
}
