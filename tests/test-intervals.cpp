/*
 * test-intervals.cpp
 *
 *  Created on: May 17, 2010
 *      Author: maze
 */

#include <algorithm>
#include <iostream>
#include <stdlib.h>

#include "common.hpp"
#include "common_old.hpp"
#include "helpers.hpp"
#include "rearrangements.hpp"
#include "rrrmtrand.hpp"



using namespace std;

int main( int argc, char *argv[] ){
	int circular =0;
	int n,
		k;
	genom g1, g2;
	unordered *rs;
	vector<genom> trace;
	vector<float> prob(4,0);
	prob[0] = 100;

	// common intervals
	vector<pair<int,int> > citmp;
	vector<pair<unsigned,unsigned> > cio, cin;

	// irreducible intervals
	vector<pair<int,int> > icitmp;
	vector<pair<unsigned,unsigned> > icio, icin;

	// strong intervals
	vector<pair<int,int> > sci;

	init_rng( );

	for( unsigned r = 0; r<100000; r++ ){
		n = ask_rng(10, 100);
		k = ask_rng(1, n);
//		cout <<"r "<< n<<" "<<k<<endl;

		g1 = genom( n, 0 );
		g2 = genom( n, 0 );

		rs = new rrrmt_rand(k, prob, g2, trace, true, std::numeric_limits< unsigned >::max());
		delete rs;

		// common intervals
		cio = find_common_intervals(g1, g2, 0, 0, circular, 0);
		common_intervals(g1, g2, circular, 1, 0, citmp);
		for( unsigned i=0; i<citmp.size(); i++ ){
			cin.push_back(make_pair( citmp[i].first, citmp[i].second ));
		}
		// insert intervals of size 1 in the set of intervals returned
		// from stoye's code (Stoye excluded those in the definition)
		for(unsigned i=0; i<(unsigned)n; i++)
			cio.push_back( make_pair(i,i) );

		sort( cio.begin(), cio.end() );
		sort( cin.begin(), cin.end() );

		if( cio != cin ){
			cerr << "common interval mismatch "<<endl;
			cerr << "> g1 "<<endl << g1<<endl;
			cerr << "> g2 "<<endl << g2<<endl;
			cerr << "--- old"<<endl;
			for( vector<pair<unsigned,unsigned> >::const_iterator it=cio.begin(); it!=cio.end(); it++ )
				cerr << it->first<<","<<it->second<<endl;
			cerr << "--- new"<<endl;
			for( vector<pair<unsigned,unsigned> >::const_iterator it=cin.begin(); it!=cin.end(); it++ )
				cerr << it->first<<","<<it->second<<endl;
			return EXIT_FAILURE;
		}

		// irreducible intervals
		icio = find_common_intervals(g1, g2, 0, 1, circular, 0);
		irreducible_intervals(g1, g2, n, icitmp);
		for( unsigned i=0; i<icitmp.size(); i++ ){
			icin.push_back(make_pair( icitmp[i].first, icitmp[i].second ));
		}
		sort( icio.begin(), icio.end() );
		sort( icin.begin(), icin.end() );

		if( icio != icin ){
			cerr << "irreducible common interval mismatch "<<endl;
			cerr << "> g1 "<<endl << g1<<endl;
			cerr << "> g2 "<<endl << g2<<endl;
			cout << "--- old"<<endl;
			for( vector<pair<unsigned,unsigned> >::const_iterator it=icio.begin(); it!=icio.end(); it++ )
				cerr << it->first<<","<<it->second<<endl;
			cerr << "--- new"<<endl;
			for( vector<pair<unsigned,unsigned> >::const_iterator it=icin.begin(); it!=icin.end(); it++ )
				cerr << it->first<<","<<it->second<<endl;
			return EXIT_FAILURE;
		}
		// strong intervals
		sci = vector<pair<int,int> >(2*n);
		strong_intervals(g1, g2, n, sci, true, true);

		for( unsigned i=0; i<sci.size(); i++ ){
			unsigned sf = sci[i].first,
					ss = sci[i].second;
			for( unsigned j=0; j<cin.size(); j++ ){
				unsigned cf = cin[j].first,
						cs = cin[j].second;
				if( !(ss < cf || sf > cf	|| // disjoint
						(sf<=cf && cs<=ss) ||
						(cf<=sf && ss<=cs) ) ){

					cerr << "strong interval failure"<<endl;
					cerr << sf<<","<<ss<<" - "<<cf<<","<<cs<<endl;
				}
			}
		}


//		for( vector<pair<int,int> >::const_iterator it=sci.begin(); it!=sci.end(); it++ )
//			cerr << it->first<<","<<it->second<<endl;




		icio.clear();
		icin.clear();
		icitmp.clear();

		cio.clear();
		cin.clear();
		citmp.clear();

		sci.clear();
	}
	return EXIT_SUCCESS;
}
