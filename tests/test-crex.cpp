/*
 * test-crex.cpp
 *
 * consistency checks for crex
 * 1 create singe rearrangements (reversal/transposition/reverse transposition)
 *   and reconstruct .. check for equality
 * 2 create a single tdrl .. and check for the reconstruction of a single
 *   rearrangement (because random tdrls may be transpositions)
 * 3 create random scenarios and check for the reconstruction of a scenario
 *
 *  Created on: May 31, 2010
 *      Author: maze
 */

#include <algorithm>
#include <getopt.h>
#include <iostream>
#include <stdlib.h>
#include <typeinfo>

#include "crex.hpp"
#include "costfoo.hpp"
#include "rrrmtrand.hpp"

#define NRTESTS 1000

using namespace std;

/**
 * get command line options
 * @param[out] crexone use crex1
 */
void getoptions( int argc, char *argv[], bool &crexone, string &folder );

void usage();

/**
 * test crex2 with the pattern (and cost function)
 * that should be best explained with a T + I
 * @return true iff successful
 */
bool test_crex2_T_I( );

/**
 * test crex2 with the pattern (and cost function)
 * that should be best explained with a T + iT
 * @return true iff successful
 */
bool test_crex2_T_iT( );

/**
 * test crex2 with the pattern (and cost function)
 * that should be best explained with 2iT
 * @return true iff successful
 */
bool test_crex2_2iT( );

/**
 * test crex2 with the pattern (and cost function)
 * that should be best explained with a T and 2iT
 * @return true iff successful
 */
bool test_crex2_T_2iT();

int main( int argc, char *argv[] ){
	bool crexone = false;
	costfoo *cst;
//	int circular =0;
	int n,
		k,
		o;		// operation;
	genom g1, g2;
	string folder;
	rrrmt *rs = NULL, *cs = NULL, *s = NULL;
	vector<genom> trace;
	vector<float> prob(4,0);

	getoptions(argc, argv, crexone, folder);

	init_rng( 42 );


//	if( not crexone ){
//		test_crex2_T_I();
//		test_crex2_T_iT();
//		test_crex2_2iT();
//		test_crex2_T_2iT();
//	}

	for( unsigned r = 0; r<NRTESTS; r++ ){
		int n=10;
		itnode *itroot;
		interval_tree_random(&itroot, NULL, make_pair(0,n-1), 2, 4);
		genom id(n,0), f;
//		interval_tree_print( itroot, id, cout ); cout << endl;
		front(itroot, id, f);
		cst = new costfoo_equi();


		cs = new crex2(id,f, true, cst, true);
		rs = new crex2(id,f, true, cst);
		cerr << "  complete "; cs->output(cerr, 0, 1); cerr <<endl;
		cerr << "incomplete "; rs->output(cerr, 0, 1); cerr <<endl;

		s = (unordered*)cs->intersect( rs );
		cerr << "           "; s->output(cerr, 0, 1); cerr <<endl;
		if( rs->length( ALTMAX ) != s->length( ALTMAX ) || cs->length( ALTMAX ) != s->length( ALTMAX ) ){
			cerr <<"crex2 implementations disagree"<<endl;
			cerr <<"crex2 compl "<<endl<< *cs << endl;
			cerr <<"crex2 limit "<<endl<< *rs << endl;
			cerr <<"            "<<endl<< *s << endl;
			cerr << rs->length( ALTMAX )<<" " <<s->length( ALTMAX )<<" "<< cs->length( ALTMAX )<<endl;
			return EXIT_FAILURE;
		}

		cs->apply( id );
		if (id != f){
			cerr <<"crex2 application disagrees"<<endl;
			return EXIT_FAILURE;
		}

		delete cst;
		delete s;
		delete cs;
		delete rs;
		interval_tree_free(itroot);
		cout << r << f<<endl;
	}

	exit(EXIT_FAILURE);

	cst = new costfoo_equi();
	for( unsigned r = 0; r<NRTESTS; r++ ){
//		rrrmt *tmp=NULL;

		n = 10;
//				ask_rng(10, 100);
		k = 1;
		o = ask_rng(0, 2);
		prob.assign( 4, 0 );
		prob[o]=100;

		g1 = genom( n, 0 );
		g2 = genom( n, 0 );

//		d = ask_rng(0, g1.size()+1);
//		if( d < g1.size() ){
////			g2.chromosom.erase( g2.chromosom.begin()+d );
////			tmp = new indel(d, true, g2);
//			g1.chromosom.erase( g1.chromosom.begin()+d );
//			tmp = new indel(d, true, g1);
//		}
		rs = new rrrmt_rand(k, prob, g2, trace, true, std::numeric_limits< unsigned >::max());
//		if( tmp != NULL ){
////			rs->insert( tmp );
//		}
		if(crexone){
			cs = new crex(g1, g2, false, 0, false);
		}else{
			cs = new crex2(g1,g2, true, cst);
		}

		s = (unordered*)cs->intersect( rs );

		if( s->length( ALTMAX ) != 1 ){
			cerr << "intersection of size != 1 found"<<endl;
			cerr <<"random"<<endl<< *rs << endl;
			cerr <<"crex  "<<endl<< *cs << endl;
			cerr <<"cut   "<<endl<< *s << endl;
			return EXIT_FAILURE;
		}

		delete s; s=NULL;
		delete cs; cs=NULL;
		delete rs; rs=NULL;
	}

	if( not crexone ){
		return EXIT_SUCCESS;
	}

	for( unsigned r = 0; r<NRTESTS; r++ ){
		n = ask_rng(10, 100);
		k = 1;
		o = 3;

		g1 = genom( n, 0 );
		g2 = genom( n, 0 );

		prob.assign( 4, 0 );
		prob[o]=100;

		rs = new rrrmt_rand(k, prob, g2, trace, true, std::numeric_limits< unsigned >::max());
		if(crexone){
			cs = new crex(g1, g2, false, 0, false);
		}else{
			cs = new crex2(g1, g2, true, cst);
		}

		if( rs->length( ALTMAX ) != cs->length( ALTMAX ) ){
			genom g = g1;
			rs->apply( g );
			if( g != g1 ){
				cerr <<"random tdrl not reconstructed as one operation"<<endl;
				cerr <<"random"<<endl<< *rs << endl;
				cerr <<"crex  "<<endl<< *cs << endl;
				return EXIT_FAILURE;
			}else{
				cerr << "warning: empty TDRL was generated"<<endl;
			}
		}

		delete cs; cs = NULL;
		if( rs!=NULL ){
			delete rs; rs = NULL;
		}
	}

	for( unsigned r = 0; r<NRTESTS; r++ ){
		n = ask_rng(10, 100);
		k = ask_rng(1, n/4);

		g1 = genom( n, 0 );
		g2 = genom( n, 0 );

		prob.assign( 4, 0 );
		for( unsigned j = 0; j<100; j++ ){
			prob[ ask_rng(0,3) ]+=1.0;
		}

		rs = new rrrmt_rand(k, prob, g2, trace, true, std::numeric_limits< unsigned >::max());
//		cerr << g1<<endl<<g2<<endl;

		if(crexone){
			cs = new crex(g1, g2, false, 2, false);
		}else{
			cs = new crex2(g1, g2, true, cst);
		}

		cs->apply( g1 );
		if( g1 != g2 ){
			cerr << "reconstruction failed"<<endl;
			cerr <<"random"<<endl<< *rs << endl;
			cerr <<"crex  "<<endl<< *cs << endl;
			cerr <<"cut   "<<endl<< *s << endl;
			return EXIT_FAILURE;
		}

		delete s; s=NULL;
		delete cs; cs=NULL;
		delete rs; rs=NULL;
//		cerr << "===="<<endl;
	}

	delete cst;
	return EXIT_SUCCESS;
}

void getoptions( int argc, char *argv[], bool &crexone, string &folder ){
	int c;

	while (1) {
		static struct option long_options[] = {
			{"crex1",   no_argument, 0, 'c'},
			{"folder",  required_argument, 0, 'f'},
			{0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "f:", long_options, &option_index);
        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'c':
				crexone = true;
				break;
			case 'f':
				cerr << optarg<<endl;
				folder = optarg;
				break;
			case 'h':
				usage();
				break;
			case '?':
				exit(EXIT_FAILURE);
				break; /* getopt_long already printed an error message. */
			default:
				usage();
        }
	}
}

void usage(){
	cerr << "test crex / crex2"<<endl;
	cerr << "test-crex"<<endl;
	cerr << "--crex1: test crex1 (default crex2)"<<endl;
	cerr << "--folder: folder containing the test files"<<endl;
	exit(EXIT_FAILURE);
}

bool test_crex2_T_I( ){
	costfoo_by_type *cstt;
	genom g1, g2;
	rrrmt *r, *rs;
	unordered *cs, *s;

	g1 = genom( 2, 0 );
	g2 = genom( 2, 0 );

	cs = new unordered();
	r = new rev(0, 1, g1);
	cs->insert( r );
	r = new transp(0, 1, 2, g1);
	cs->insert( r );
	cs->apply(g1);

	cstt = new costfoo_by_type();
	cstt->set(REVNM, 2); cstt->set(TRANM, 1); cstt->set(RTRANM, 3);

	rs = new crex2( g1, g2, true, cstt, false );

	s = (unordered*)cs->intersect( rs );
	if( s->length( ALTMAX ) != 2 ){
		cerr << "T I does not work"<<endl << *cs <<endl << *rs <<endl << *s <<endl;
		return false;
	}
	delete rs; delete cstt; delete cs; delete s; return true;

	return true;
}

bool test_crex2_T_iT( ){
	costfoo_by_type *cstt;
	genom g1, g2;
	rrrmt *r, *rs;
	unordered *cs, *s;

	g1 = genom( 2, 0 );
	g2 = genom( 2, 0 );

	cs = new unordered();
	r = new revtransp(0, 0, 1, 1, g1);
	cs->insert( r );
	r = new transp(0, 1, 2, g1);
	cs->insert( r );
	cs->apply(g1);

	cstt = new costfoo_by_type();
	cstt->set(REVNM, 3); cstt->set(TRANM, 1); cstt->set(RTRANM, 1);

	rs = new crex2( g1, g2, true, cstt, false );

	s = (unordered*)cs->intersect( rs );
	if( s->length( ALTMAX ) != 2 ){
		cerr << "T iT does not work"<<endl << *cs <<endl << *rs <<endl << *s <<endl;
		return false;
	}
	delete rs; delete cstt; delete cs; delete s; return true;

	return true;
}
bool test_crex2_2iT( ){
	costfoo_by_type *cstt;
	genom g1, g2;
	rrrmt *r, *rs;
	unordered *cs, *s;

	g1 = genom( 4, 0 );
	g2 = genom( 4, 0 );

	cs = new unordered();
	r = new revtransp(0, 2, 3, 3, g1);
	cs->insert( r );
	r = new revtransp(1, 3, 0, 0, g1);
	cs->insert( r );
	cs->apply(g1);

	cstt = new costfoo_by_type();
	cstt->set(REVNM, 3); cstt->set(TRANM, 1); cstt->set(RTRANM, 1);
	rs = new crex2( g1, g2, true, cstt, false );
	s = (unordered*)cs->intersect( rs );
	if( s->length( ALTMAX ) != 2 ){
		cerr << "2iT does not work"<<endl << *cs <<endl << *rs <<endl << *s <<endl;
		return false;
	}
	delete rs; delete cstt; delete cs; delete s; return true;
	return true;
}

bool test_crex2_T_2iT( ){
	costfoo_by_type *cstt;
	genom g1, g2;
	rrrmt *r, *rs;
	unordered *cs, *s;

	g1 = genom( 2, 0 );
	g2 = genom( 2, 0 );

	cs = new unordered();
	r = new transp(0, 1, 2, g1);
	cs->insert( r );
	r = new revtransp(0, 0, 1, 1, g1);
	cs->insert( r );
	r = new revtransp(1, 1, 0, 0, g1);
	cs->insert( r );
	cs->apply(g1);

	cstt = new costfoo_by_type();
	cstt->set(REVNM, 3); cstt->set(TRANM, 1); cstt->set(RTRANM, 1);
	rs = new crex2( g1, g2, true, cstt, false );
	s = (unordered*)cs->intersect( rs );
	if( s->length( ALTMAX ) != 3 ){
		cerr << "T 2iT does not work"<<endl << *cs <<endl << *rs <<endl << *s <<endl;
		return false;
	}
	delete rs; delete cstt; delete cs; delete s; return true;
	return true;
}
