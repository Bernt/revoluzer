#include <iostream>

#include <regex.h>
#include <cstdlib>

#include "helpers.hpp"
#include "node.hpp"
#include "bintree.hpp"

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void append(node *n, string newick){

	unsigned int bracketLevel=0;
	string newick_part(""),
		s;
	regex_t re, 
		re2; // without distance
	regmatch_t match[8]; 
	int status;
		// compile some regexes
	status = regcomp(&re,"^([\\._[:alnum:][:space:]-]+):[[:digit:]]+$",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "node::append: Could not compile regex pattern re."<<endl;
		exit(1);
	}
	status = regcomp(&re2,"^([\\._[:alnum:][:space:]-]+)$",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "node::append: Could not compile regex pattern re2."<<endl;
		exit(1);
	}
	
		// Abbruchbedingung f?r Rekursion
	if(regexec (&re, &newick[0], 2, &match[0], 0)==0 || regexec (&re2, &newick[0], 2, &match[0], 0)==0){
			// set the name of the geome at the leaf
		s.assign(newick.begin()+match[1].rm_so, newick.begin()+match[1].rm_eo);
		n->name = s;
		regfree(&re);
		regfree(&re2);
		return;
	}

	for(unsigned int i=0; i< newick.size(); i++){
		if (newick[i] == '('){
			bracketLevel++;
			if(bracketLevel!=1) 
				newick_part += newick[i];
		}
		else if (newick[i] == ')'){
			if(bracketLevel!=1) 
				newick_part += newick[i];
			bracketLevel--;
		}else if (newick[i]==',' && bracketLevel==1){
			if(n->lchild == NULL){
				n->lchild = init(n->tree, n);
				append(n->lchild, newick_part);
				newick_part = "";
			}else if(n->lchild != NULL && n->parent == NULL){
				n->parent = init(n->tree, n);
				append(n->parent, newick_part);
				newick_part = "";
			}
		}else if(bracketLevel>0){
			newick_part += newick[i];
		}
	}
	n->rchild = init(n->tree, n);
	append(n->rchild, newick_part);

	newick_part = "";
	regfree(&re);
	regfree(&re2);
	return ;
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void append(node *n, string newick, vector<genom> &genomes, vector<string> &names){

	unsigned int bracketLevel=0;
	string newick_part(""),s;
	regex_t re;
	regmatch_t match[8]; 
	int status;
		// compile some regexes
	status = regcomp(&re,"^([\\._[:alnum:][:space:]-]+):[[:digit:]]+$",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "node::append: Could not compile regex pattern re."<<endl;
		exit(-1);
	}

	if(!isroot(n)){
		n->g = genomes.back();
		genomes.pop_back();
		n->name = names.back();
		names.pop_back();
	}

		//~ // Abbruchbedingung f?r Rekursion
	if(regexec (&re, &newick[0], 2, &match[0], 0)==0){
			// set the name of the geome at the leaf
		s.assign(newick.begin()+match[1].rm_so, newick.begin()+match[1].rm_eo);
		n->name = s;
		return;
	}

	for(unsigned int i=0; i< newick.size(); i++){
		if (newick[i] == '('){
			bracketLevel++;
			if(bracketLevel!=1) 
				newick_part += newick[i];
		}
		else if (newick[i] == ')'){
			if(bracketLevel!=1) 
				newick_part += newick[i];
			bracketLevel--;
		}else if (newick[i]==',' && bracketLevel==1){
			n->lchild = init(n->tree, n);
			append(n->lchild, newick_part, genomes, names);
			newick_part = "";
		}else if(bracketLevel>0){
			newick_part += newick[i];
		}
	}
	n->rchild = init(n->tree, n);
	append(n->rchild, newick_part, genomes, names);

	newick_part = "";
	regfree(&re);
	return ;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void append(node *n, string newick, genom g, int r, int &applied, int &cnt){
	genom tmp;
	unsigned int bracketLevel=0;
	string newick_part(""),s;
	regex_t re;
	regmatch_t match[8]; 
	int status;
		// compile some regexes
	status = regcomp(&re,"^([\\._[:alnum:][:space:]-]+):[[:digit:]]+$",REG_ICASE|REG_EXTENDED);
	if( status != 0 ) {
		cout << "node::append: Could not compile regex pattern re."<<endl;
		exit(-1);
	}

	n->g = g;

		//~ // Abbruchbedingung f?r Rekursion
	if(regexec (&re, &newick[0], 2, &match[0], 0)==0){
			// set the name of the geome at the leaf
		s.assign(newick.begin()+match[1].rm_so, newick.begin()+match[1].rm_eo);
		n->name = s;
		return;
	}

	n->name = "A"+int2string(cnt);
	cnt++;
	for(unsigned int i=0; i< newick.size(); i++){
		if (newick[i] == '('){
			bracketLevel++;
			if(bracketLevel!=1) 
				newick_part += newick[i];
		}
		else if (newick[i] == ')'){
			if(bracketLevel!=1) 
				newick_part += newick[i];
			bracketLevel--;
		}else if (newick[i]==',' && bracketLevel==1){
			tmp = g;
			tmp.evolve(r, 0);
			n->lchild = init(n->tree, n);			
			applied+=r;
			append(n->lchild, newick_part, tmp, r, applied, cnt);
			newick_part = "";
		}else if(bracketLevel>0){
			newick_part += newick[i];
		}
	}
	n->rchild = init(n->tree, n);
	tmp = g;
	tmp.evolve(r, 0);
	applied+=r;
	append(n->rchild, newick_part, tmp, r, applied, cnt);

	newick_part = "";
	return ;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void free_node(node *n){
	
	if( n->lchild != NULL )
		free_node( n->lchild );
	if( n->rchild != NULL )
		free_node( n->rchild );
	delete( n );
	
	return;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

node * init(bintree * bt, node *p){
	node *n = new node;
	n->tree = bt;
	n->parent = p;
	n->lchild = NULL;
	n->rchild = NULL;
	return n;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void inner_genomes(node *n, vector<string> &names, vector<genom> &genomes){

	if(n->lchild != NULL && n->rchild != NULL){
		genomes.push_back(n->g);
		names.push_back(n->name);
		inner_genomes(n->lchild, names, genomes);
		inner_genomes(n->rchild, names, genomes);
	}
	
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void inner_nodes(node *n, vector<node *> &nodes){
	if(n->lchild != NULL && n->rchild != NULL){
		nodes.push_back(n);
		inner_nodes(n->lchild, nodes);
		inner_nodes(n->rchild, nodes);
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool isleaf(node *n) {
	if(n->rchild == NULL && n->lchild == NULL)
		return true;
	else
		return false;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool isroot(node * n) {

	if (n == n->tree->root){
		return true;
	}else{
		return false;
	}
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void leaf_genomes(node *n, vector<string> &names, vector<genom> &genomes){
	if(n->lchild == NULL && n->rchild == NULL){
		genomes.push_back(n->g);
		names.push_back(n->name);
	}else{
		leaf_genomes(n->lchild, names, genomes);
		leaf_genomes(n->rchild, names, genomes);
	}
	
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void leaf_names(node *n, vector<string> &names){
	if(n->lchild == NULL && n->rchild == NULL){
		names.push_back(n->name);
	}else{
		leaf_names(n->lchild, names);
		leaf_names(n->rchild, names);
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int lower_bound(node *n, map<string, int> &name_idx, vector<vector<unsigned> > &dist,
		vector<string> &l, vector<string> &r, vector<vector<unsigned> > &mv){
	unsigned max = 0;
	vector<string> ll,
		lr,
		rl,
		rr;
	vector<vector<unsigned> > ml,
		mr,
		delta;

	mv = vector<vector<unsigned> >(name_idx.size(), vector<unsigned>(name_idx.size(), 0) );
	delta = vector<vector<unsigned> >(name_idx.size(), vector<unsigned>(name_idx.size(), 0) );
	
	if(n->lchild != NULL){
		lower_bound(n->lchild, name_idx, dist, ll, lr, ml);
		if( isleaf(n->lchild) ){
			l.push_back(n->lchild->name);
			ll = lr = l;
		}else{
			l.insert( l.end(), ll.begin(), ll.end() );
			l.insert( l.end(), lr.begin(), lr.end() );
		}
	}
	if(n->rchild != NULL){
		lower_bound(n->rchild, name_idx, dist, rl, rr, mr);	
		if( isleaf(n->rchild) ){
			r.push_back(n->rchild->name);
			rl = rr = r;			
		}else{
			r.insert( r.end(), rl.begin(), rl.end() );
			r.insert( r.end(), rr.begin(), rr.end() );
		}
	}

//	cout << n->name << " -> ";
//	cout << "	( ";
//	for(unsigned i=0; i<ll.size(); i++){
//		cout << ll[i] << " ";
//	}
//	cout << " , ";
//	for(unsigned i=0; i<lr.size(); i++){
//		cout << lr[i] << " ";
//	}
//	cout << " , ";
//	for(unsigned i=0; i<rl.size(); i++){
//		cout << rl[i] << " ";
//	}
//	cout << " , ";
//	for(unsigned i=0; i<rr.size(); i++){
//		cout << rr[i] << " ";
//	}
//	cout <<" )"<< endl;
	
		// compute delta
	for(unsigned a=0; a<ll.size(); a++){
		for(unsigned c=0; c<rl.size(); c++){
			max = 0;
			for(unsigned b=0; b<lr.size(); b++){
				if( max < ml[ name_idx[ll[a]] ][ name_idx[lr[b]] ] + dist[ name_idx[lr[b]] ][ name_idx[rl[c]] ] ){
					max = ml[ name_idx[ll[a]] ][ name_idx[lr[b]] ] + dist[ name_idx[lr[b]] ][ name_idx[rl[c]] ];
				}
			}
			delta[ name_idx[ll[a]] ][ name_idx[rl[c]] ] = delta[ name_idx[rl[c]] ][ name_idx[ll[a]] ] = max;
		}
	}
	
	for(unsigned a=0; a<ll.size(); a++){
		for(unsigned d=0; d<rr.size(); d++){
			max = 0;
			
			for(unsigned c=0; c<rl.size(); c++){
				if(max < delta[ name_idx[ll[a]] ][ name_idx[rl[c]] ] + mr[ name_idx[rl[c]] ][ name_idx[rr[d]] ] ){
					max = delta[ name_idx[ll[a]] ][ name_idx[rl[c]] ] + mr[ name_idx[rl[c]] ][ name_idx[rr[d]] ];
				}
			}
			mv[ name_idx[ll[a]] ][ name_idx[rr[d]] ] = mv[ name_idx[rr[d]] ][ name_idx[ll[a]] ] = max;
		}
	}

//	cout << "Ml"<<endl;		
//	for(unsigned i=0; i<ml.size(); i++){
//		for(unsigned j=0; j<ml[i].size(); j++){
//			cout.width(2);
//			cout << ml[i][j] << " ";
//		}
//		cout << endl;
//	}
//	cout << "Mr"<<endl;		
//	for(unsigned i=0; i<mr.size(); i++){
//		for(unsigned j=0; j<mr[i].size(); j++){
//			cout.width(2);
//			cout << mr[i][j] << " ";
//		}
//		cout << endl;
//	}
//	cout << "delta "<<endl;
//	for(unsigned i=0; i<delta.size(); i++){
//		for(unsigned j=0; j<delta[i].size(); j++){
//			cout.width(2);
//			cout << delta[i][j] << " ";
//		}
//		cout << endl;
//	}
//	cout << "Mv"<<endl;
//	for(unsigned i=0; i<mv.size(); i++){
//		for(unsigned j=0; j<mv[i].size(); j++){
//			cout.width(2);
//			cout << mv[i][j] << " ";
//		}
//		cout << endl;
//	}

		// get the lower bound at the root
	if(isroot(n)){
		max = 0;
		for(unsigned a=0; a<ll.size(); a++){
			for(unsigned d=0; d<rr.size(); d++){
				if( max < mv[ name_idx[ll[a]] ][ name_idx[rr[d]] ]) 
					max = mv[ name_idx[ll[a]] ][ name_idx[rr[d]] ];
			}
		}
		return max/2 + max%2;
	}
	
	return 0;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
string newick(node *n, bool printdist, bool inner){
	string nwk;
	
	if(n->lchild != NULL && n->rchild != NULL){
		nwk.append("(");
		nwk.append(newick(n->lchild, printdist, inner));
		nwk.append(",");
		nwk.append(newick(n->rchild, printdist, inner));
		nwk.append(")");
		if(inner != 0)
			nwk.append(n->name);
	}else{
		nwk.append(n->name);
	}
	
	if(printdist){
		if( isroot(n) )
			nwk.append(":0");
		else{
			nwk.append(":");
			nwk.append(int2string( n->tree->_dst->calc( (n->g), n->parent->g) ) );
		}
	}
	return nwk ;
	
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void print_newick(node * n, bool printdist, bool inner){
	if (n == NULL )
		return;
	if( n->lchild != NULL && n->rchild != NULL){
		cout << "(";
		print_newick(n->lchild, printdist, inner);
		cout << ",";
		print_newick(n->rchild, printdist, inner);
		cout << ")";
		if(inner != 0)
			cout << n->name;
	}else{
		cout <<  n->name;
	}
	
	if(printdist){
		if( isroot(n) )
			cout << ":0";
		else{
			cout << ":"<<int2string( n->tree->_dst->calc( (n->g), n->parent->g ) );
		}
	}
	return ;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int score(node *n){
	if(n->lchild != NULL && n->rchild != NULL){
		return ( n->tree->_dst->calc(n->g, n->parent->g) +
				+ score(n->lchild) + score(n->rchild));
	}else{
		return (n->tree->_dst->calc(n->g, n->parent->g));
	}
	
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void sort(node *n, string &start){
	string lstart, 
		rstart;
	
	if(n->lchild != NULL && n->rchild != NULL){
		sort(n->lchild, lstart);
		sort(n->rchild, rstart);

		if(lstart > rstart){
			swap(n->lchild, n->rchild);
			start = rstart;
		}else{
			start = lstart;
		}
	}else{
		start = n->name;
	}
	return;
	
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void triples(node *n, 
	vector<vector<genom> > &trips, vector<vector<string> > &trips_names, 
	vector<genom> &meds, vector<string> &meds_names){

	if(n->lchild && n->rchild) {
		trips.push_back( vector<genom>() );  
		trips.back().push_back( n->rchild->g );
		trips.back().push_back( n->lchild->g );
		trips.back().push_back( n->parent->g );
		
		trips_names.push_back( vector<string>() );
		trips_names.back().push_back( n->rchild->name );
		trips_names.back().push_back( n->lchild->name );
		trips_names.back().push_back( n->parent->name );
		
		meds.push_back(n->g);
		meds_names.push_back(n->name);
	
		triples(n->lchild, trips, trips_names, meds, meds_names);
		triples(n->rchild, trips, trips_names, meds, meds_names);
	}
	
	
}

/*
genom node::getGenom(){
	return g;
}
*/

/*
node::node(bintree *bt, node *p){
	tree = bt;
	//~ if(tree->circular)
		//~ g.setCircular(true);
	parent = p;
	lChild = rChild = NULL;
}

///////////////////////////////////////////////////////////////////////////////

node::node(){
	tree = NULL;
	parent =	lChild = rChild = NULL;
}

///////////////////////////////////////////////////////////////////////////////

ostream & operator<<(ostream &out, node *n){
	
	if(n->lChild || n->rChild){
		out << "(";
		if(n->lChild)
			out << n->lChild;
		out << ",";
		if(n->isRoot() && n->parent){
			out << n->parent;
			out << ",";
		}	
		if(n->rChild)
			out << n->rChild;
		out << ")";
	}
	else{
		out << n->name;
	}
	
	//~ if (n->parent){
		//~ genom g = n->getGenom(),
			//~ h = n->parent->getGenom();
		//~ hdata hd = n->tree->get_hdata();
		
		//~ cout <<"g"<< g<< endl;
		//~ cout <<"h"<< h<< endl;
		//~ out << ":" << g.distance(h, hd);
	//~ }

	return out;
}

void node::output(void){
	cout << this->g << endl;
	if(isRoot() && parent)
		parent->output();
	if (lChild)
		lChild->output();
	if(rChild)
		rChild->output();
}*/

///////////////////////////////////////////////////////////////////////////////
/*
void node::append(vector<genom> &genomes){
	vector<genom> lGenomes,
		rGenomes;
		// exit of recursion
	if(genomes.size()==1){
		g = genomes[0];
		tree->leafs.push_back(this);
		return;
	}
		// split the genomes in 2 equaly sized parts
	for(unsigned i=0; i<genomes.size(); i++){
		if(i%2)
			lGenomes.push_back(genomes[i]);
		else
			rGenomes.push_back(genomes[i]);
	}
		// append the child nodes 	

	lChild = new node(this->tree, this);
	lChild->append(lGenomes);
	rChild = new node(this->tree, this);
	rChild->append(rGenomes);
	return;
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
int node::countDescendants(){
	int lCnt = 0,
		rCnt = 0;
	if (lChild) {
		lCnt = lChild->countDescendants();
	}
	if(rChild){
		rCnt = rChild->countDescendants();
	}
	return (1+lCnt+rCnt);
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
int node::countLeafs(){
	int lCnt = 0,
		rCnt = 0;
	if (lChild) {
		lCnt = lChild->countLeafs();
	}
	if(rChild){
		rCnt = rChild->countLeafs();
	}
	if(!lChild && !rChild)
		return 1;
	else 
		return (lCnt+rCnt);
	
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
int node::distance(genom target, hdata &hd){
	int dist = g.distance(target, hd);
	
	if(isRoot() && parent){
		if(parent->distance(target, hd) < dist)
			dist = parent->distance(target, hd);
	}
	if(rChild){
		if(rChild->distance(target, hd) < dist)
			dist = rChild->distance(target, hd);
	}
	if(lChild){
		if(lChild->distance(target, hd) < dist)
			dist = lChild->distance(target, hd);
	}
	
	return dist;
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
int node::distance(node *n, hdata &hd){
	return this->g.distance(n->g, hd);
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
int node::distanceSum(hdata &hd){
	int dist = 0;
	
	if(isRoot() && parent){
		dist += this->distance(this->parent, hd);
		dist += this->parent->distanceSum(hd);		
	}
	if(rChild){
		dist += this->distance(this->rChild, hd);
		dist += this->rChild->distanceSum(hd);
	}
	if(lChild){
		dist += this->distance(this->lChild, hd);
		dist += this->lChild->distanceSum(hd);
	}
	return dist;
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
int node::distanceSumWithoutRoot(hdata &hd){
	int dist = 0;
	if(lChild && rChild)
		dist += lChild->distance(this->rChild, hd);

	if(rChild){
		dist += this->rChild->distanceSum(hd);
	}
	if(lChild){
		dist += this->lChild->distanceSum(hd);
	}
	
	return dist;
}

///////////////////////////////////////////////////////////////////////////////

node *node::getDeepestUnresolvedChild(){
	node *lUc=NULL,	// left unresolved child
		*rUc=NULL,		// right unresolved child
		*pUc=NULL, 		// parent unresolved child
		*ret=NULL;
	
		// check if calling node is resolvable
	if(isResolvable())
		return this;
	
		// get the deepest unresolved childs of the childs
	if(!lChild->isResolved())
		lUc = lChild->getDeepestUnresolvedChild();
	if(!rChild->isResolved())
		rUc = rChild->getDeepestUnresolvedChild();
		// and if the node is the root-node of the parent
	if(isRoot() && !parent->isResolved())
		pUc = parent->getDeepestUnresolvedChild();
	
	if (lUc)
		ret = lUc;
	if (rUc && (!ret || rUc->getLevel() > ret->getLevel()))
		ret = rUc;
	if (pUc && (!ret || pUc->getLevel() > ret->getLevel()))
		ret = pUc;
		
	return ret;
}
*/
///////////////////////////////////////////////////////////////////////////////


/*
///////////////////////////////////////////////////////////////////////////////

vector<node *> node::getInnerNodes(){
	vector<node *> inn;
		// test if the node is a leaf 
	if (!lChild && !rChild){
		return inn;
	}
		// else search the childs
	else{
		inn.push_back(this);
		if(lChild){
			vector<node *> linn;
			linn = lChild->getInnerNodes();
			for (unsigned i=0; i< linn.size(); i++)
				inn.push_back(linn[i]);
		}
		if(rChild){
			vector<node *> rinn;
			rinn = rChild->getInnerNodes();
			for (unsigned i=0; i< rinn.size(); i++)
				inn.push_back(rinn[i]);
		}
	}
	return inn;	
	
}
*/
///////////////////////////////////////////////////////////////////////////////

//~ node *node::getLca(node *n){
	//~ node *n1 = this,
		//~ *n2 = n;
	//~ /*!todo too often call getLevel() */
	//~ while(n1 != n2){
		//~ if(n1->getLevel() != n2->getLevel()){
			//~ if (n1->getLevel() < n2->getLevel())
				//~ n1 = n1->parent;
			//~ if (n2->getLevel() < n1->getLevel())
				//~ n2 = n2->parent;
		//~ }
		//~ else{
			//~ n1 = n1->parent;
			//~ n2 = n2->parent;
		//~ }
	//~ }
	//~ return n1;
//~ }

///////////////////////////////////////////////////////////////////////////////
/*
int node::getLevel(){
	if(isRoot())
		return 0;
	else{
		return parent->getLevel()+1;
	}
}

///////////////////////////////////////////////////////////////////////////////

node *node::getNextResolvedChild(){
	node *l=NULL, 
		*r=NULL;
	if(isResolved())
		return this;
	else{
		if(lChild)
			l = lChild->getNextResolvedChild();
		if(rChild)
			r = rChild->getNextResolvedChild();
		
		if(l->getLevel() < r->getLevel())
			return l;
		else
			return r;
		
	}
	
}

///////////////////////////////////////////////////////////////////////////////


vector<node *> node::getResolvedNodes(){
	vector<node *> rn;
		// test if the node is a leaf or resolved
		// -> resolvedNodes = this leaf
	if ((!lChild && !rChild) || g.size()){
		rn.push_back(this);
	}
		// else search the childs
	else{
		if(lChild){
			vector<node *> lrn;
			lrn = lChild->getResolvedNodes();
			for (unsigned i=0; i< lrn.size(); i++)
				rn.push_back(lrn[i]);
		}
		if(rChild){
			vector<node *> rrn;
			rrn = rChild->getResolvedNodes();
			for (unsigned i=0; i< rrn.size(); i++)
				rn.push_back(rrn[i]);
		}
		if(isRoot() && parent){
			vector<node *> prn;
			prn = parent->getResolvedNodes();
			for (unsigned i=0; i< prn.size(); i++)
				rn.push_back(prn[i]);
		}
		
	}
	return rn;
}

///////////////////////////////////////////////////////////////////////////////

node *node::getSibling(){
		// the root has no sibling
	if(!parent)
		return NULL;
		// test on which side of the parent the node is and return the opposite
	if (parent->lChild == this)
		return parent->rChild;
	else if(parent->rChild == this)
		return parent->lChild;
	else
		return NULL;
	
}

///////////////////////////////////////////////////////////////////////////////

vector<node *> node::getUnresolvedNodes(){
	vector<node *> urn;

	if(this->isResolvable()){
		urn.push_back(this);
	}else{
		if(lChild && !lChild->isResolved()){
			vector<node *> lurn;
			lurn = lChild->getUnresolvedNodes();
			for (unsigned i=0; i< lurn.size(); i++)
				urn.push_back(lurn[i]);
		}
		if(rChild && !rChild->isResolved()){
			vector<node *> rurn;
			rurn = rChild->getUnresolvedNodes();
			for (unsigned i=0; i< rurn.size(); i++)
				urn.push_back(rurn[i]);
		}
		if(isRoot() && parent && !parent->isResolved()){
			vector<node *> purn;
			purn = rChild->getUnresolvedNodes();
			for (unsigned i=0; i< purn.size(); i++)
				urn.push_back(purn[i]);
		}
	}
	return urn;
}

///////////////////////////////////////////////////////////////////////////////

bool node::hasParent(){
	if(parent)
		return true;
	else
		return false;
}

///////////////////////////////////////////////////////////////////////////////

bool node::isDescendant(node *n){
	if(this == NULL)
		return false;
		// break the recursion when the node is found
	if(this == n){
		return true;
		// else try to check for the parent node
	}else{
		//cout << "ID"<<endl;
		
		if(!isRoot() && parent){
			//cout << "OK"<<endl;
			return parent->isDescendant(n);
		}else{
			//cout << "!OK"<<endl;
			return false;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

bool node::isResolvable(){
	bool ret = false;
		// test if the childs are resolved
	if(lChild && rChild)
		ret = lChild->isResolved() && rChild->isResolved();
		// if the node is the root -> test the parent too
	if (isRoot())
		ret &= parent->isResolved();
	
	return ret;
}

///////////////////////////////////////////////////////////////////////////////

bool node::isResolved(){
	return (g.size()>0);
}
*/
///////////////////////////////////////////////////////////////////////////////


/*
///////////////////////////////////////////////////////////////////////////////

node* node::insert(node *n1, genom g, int i){
	
	node *newnode = NULL;
	string name = "A"+int2string(i);


		// never use this function with two nodes each having a parent node
	if(n1->hasParent() && this->hasParent()){
		cout << "node::insert called with two nodes both having parents!"<<endl;
		exit(0);
	}

		// construct an new node with the given genome and name
	newnode = new node(n1->tree, NULL);
	newnode->g = g;
	//!@todo set the name anywhere else
	//~ newnode->g.setName (name);

		// both nodes have no parent the new node gets simply the 
		// parent of them
	if(!n1->hasParent() && !this->hasParent()){
			// set the nodes as children of the new node (sequence doesn't matter)
		newnode->lChild = n1;
		newnode->rChild = this;
			// set the new node as parent of both nodes
		n1->parent = newnode;
		this->parent = newnode;
			// set the newnode as root of the tree
		n1->tree->root = newnode;
		
	}
		// one of the 2 nodes (n1 / this) is loose (has no parent)
		// -> a new node should be inserted between the already integrated 
		// 	node and its parent; and the loose node gets connected to 
		// 	the new node
	else{
		node *i, *l;	// node pointers for the i(ntegrated) and l(oose) nodes

			// check which (this / n1) node is the integrated one and assing
			// this & n1 accordingly to i & l
		if(this->hasParent()){
			l = n1;
			i = this;
		}else{
			l = this;
			i = n1;
		}

			// connect the newnode (the order of the childs does not matter)
		newnode->parent = i->parent;
		newnode->lChild = i;
		newnode->rChild = l;		

			// reset the parent of the integrated node
		if(i->parent->lChild == i)
			i->parent->lChild = newnode;
		else
			i->parent->rChild = newnode;
		
			// connect i and l to its new parent
		i->parent = newnode;
		l->parent = newnode;
		
	}	
	return newnode;
}

///////////////////////////////////////////////////////////////////////////////

int node::maxLeafDistance() const{
	int ld=0, rd=0, pd=0,
		max = 0;

		// get the leaf distances for each child and for the parent 
		// if the node is the root and the tree is unrooted
	if (lChild) ld = lChild->maxLeafDistance()+1;
	if (rChild) rd = rChild->maxLeafDistance()+1;
	if(isRoot() && parent) pd = parent->maxLeafDistance()+1;

		// determine the maximum
	max = (ld > rd) ? ld : rd;
	max = (pd > max)? pd : max;

	return max;
	
}

///////////////////////////////////////////////////////////////////////////////

int node::minLeafDistance() const{
	int ld=0, rd=0, pd=0,
		max = 0;

		// get the leaf distances for each child and for the parent 
		// if the node is the root and the tree is unrooted
	if (lChild) ld = lChild->minLeafDistance()+1;
	if (rChild) rd = rChild->minLeafDistance()+1;
	if(isRoot() && parent) pd = parent->minLeafDistance()+1;

		// determine the maximum
	max = (ld < rd) ? ld : rd;
	max = (pd < max)? pd : max;

	return max;
	
}

*/
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////


/*
///////////////////////////////////////////////////////////////////////////////

string node::output_newick(){
	string ret ="";		

	if (lChild)				
		ret += "("+lChild->output_newick()+",";
	if(isRoot() && parent)
		ret += parent->output_newick()+",";	
	if(rChild)
		ret += rChild->output_newick()+")";
	
	//!@todo get the name anywhere else !!
	//~ ret += this->g.getName();
	//if(parent)
	//	ret += ":"+int2string(this->distance(parent));
	
	return ret;
}
*/
///////////////////////////////////////////////////////////////////////////////

//~ void node::output_tulip(string &nodes, int &node_num, string &edges, int &edge_num, string &outdegree, string &labels){
	//~ int nn=node_num;
	
	//~ nodes += " "+int2string(node_num);
	//~ if (lChild && rChild)
		//~ outdegree += "(node "+int2string(node_num)+" \"2\")\n";
	//~ else
		//~ outdegree += "(node "+int2string(node_num)+" \"0\")\n";

	//~ labels += "(node "+int2string(node_num)+" \"";
	//~ //!@todo get the name anywhere else !!
	//~ // labels += g.getName();
	//~ /* (code for output of chromosome ())
	//~ for(unsigned int i=0; i<g.chromosom.size(); i++){
		//~ labels += int2string(g.chromosom[i])+" ";
	//~ }
	//~ */
	//~ labels += "\")\n";
	
	//~ node_num++;
	//~ if (lChild) {
		//~ edges += "(edge "+int2string(edge_num)+" "+int2string(nn)+" "+int2string(node_num)+")\n";
		//~ edge_num++;
		//~ lChild->output_tulip(nodes, node_num, edges, edge_num, outdegree,labels);
	//~ }
	//~ if(rChild){
		//~ edges += "(edge "+int2string(edge_num)+" "+int2string(nn)+" "+int2string(node_num)+")\n";
		//~ edge_num++;		
		//~ rChild->output_tulip(nodes, node_num, edges, edge_num, outdegree, labels);
	//~ }
//~ }

///////////////////////////////////////////////////////////////////////////////
/*
void node::setGenom(genom &newGenom){
	 g = newGenom;
}

///////////////////////////////////////////////////////////////////////////////

void node::setParent(node *n){
	parent = n;
}

///////////////////////////////////////////////////////////////////////////////

int node::shake(hdata &hd){
	vector<genom> neighbours;
	int enhancement=0,
		local_enhancement=0;
	genom enhanced_genom;

	if(lChild)
		enhancement += lChild->shake(hd);
	if(rChild)
		enhancement += rChild->shake(hd);
	if(isRoot() && parent)
		enhancement += parent->shake(hd);
	
	//~ cout << "SHAKE "<< g.getName()<<endl;
	
	if(parent)
		neighbours.push_back(parent->g);
	if(lChild)
		neighbours.push_back(lChild->g);
	if(rChild)
		neighbours.push_back(rChild->g);

	if(neighbours.size()>1){	// don't shake the leafs
		vector< pair<int,int> > goodReversals;
		//goodReversals = this->g.getGoodReversals(neighbours);
		//~ goodReversals = this->g.getGoodReversalsWithFullOrientation(neighbours);
		goodReversals = this->g.getReversals();
		// cout <<"shake Reversals" << goodReversals.size() << endl;
		for (unsigned int i=0; i<goodReversals.size(); i++){
			int delta;
			genom temp = this->g;
			temp.reverse(goodReversals[i]);
			delta = this->g.distance(neighbours, hd) - temp.distance(neighbours, hd);
			
			if(delta > local_enhancement){
				local_enhancement = delta;
				enhanced_genom = temp;
			}
		}
	}
	if (local_enhancement){
		this->g = enhanced_genom;
		enhancement+=local_enhancement;
	}
	return enhancement;
}

///////////////////////////////////////////////////////////////////////////////

int node::treeDistance(node *n){
	int dist = 0;
		// goal reached
	if(this == n)
		return dist;
		// this is descendant of n -> move this nearer to n
	else if (this->isDescendant(n))
		return this->parent->treeDistance(n)+1;
		// n is descendant of this -> move n nearer to this
	else if (n->isDescendant(this))
		return n->parent->treeDistance(this)+1;
		// n and this are in different parts of the tree
	else
		return this->parent->treeDistance(n->parent)+2;
}

*/
