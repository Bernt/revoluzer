/*
 * dstnc_crex.cpp
 *
 *  Created on: Sep 25, 2010
 *      Author: maze
 */

#include "dstnc_crex.hpp"

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

dstnc_crex::dstnc_crex( unsigned n, bool bps, bool mkalt, unsigned maxszen ){
	_bps = bps;
	_mkalt = mkalt;
	_maxszen = maxszen;
	_srcinv = vector<int>( n+1, std::numeric_limits< int >::max() );
	_tgtinv = vector<int>( n+1, std::numeric_limits< int >::max() );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

dstnc_crex::~dstnc_crex(){}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void dstnc_crex::adapt(unsigned n){
	_srcinv = vector<int>( n+1, std::numeric_limits< int >::max() );
	_tgtinv = vector<int>( n+1, std::numeric_limits< int >::max() );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned dstnc_crex::calc( const genom &src, const genom &tgt ){
	unsigned d=0;

//	cerr << "dstnc_crex "<<endl;
//	cerr << src<<"("<<src.size()<<")"<<endl<<tgt<<"("<<tgt.size()<<")"<<endl;

//	fill( _srcinv.begin(), _srcinv.end(), std::numeric_limits< int >::max() );
//	for( unsigned i=0; i<src.size(); i++ ){
//		_srcinv[ abs(src[i]) ] = (src[i]>0) ? i : -i;
//	}
//	fill( _tgtinv.begin(), _tgtinv.end(), std::numeric_limits< int >::max() );
//	for( unsigned i=0; i<tgt.size(); i++ ){
//		_tgtinv[ abs(tgt[i]) ] = (tgt[i]>0) ? i : -i;
//	}
//
//	for( unsigned i=1; i<_srcinv.size(); i++ ){
//		if( _srcinv[i]==std::numeric_limits< int >::max() && _tgtinv[i]==std::numeric_limits< int >::max() ){	// element missing in both
//
//		}if( _srcinv[i]==std::numeric_limits< int >::max() || _tgtinv[i]!=std::numeric_limits< int >::max()  ){
//			d += 1;
//		}else{
//
//		}
//	}

	crex scenario(src, tgt, _bps, _maxszen, _mkalt);
	d+= scenario.length( ALTMIN );

	return d;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

dstnc* dstnc_crex::clone() const{
	return new dstnc_crex( _bps, _mkalt, _maxszen );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ostream & dstnc_crex::output(ostream &os) const{
	os << "crex distance";
	return os;
}
