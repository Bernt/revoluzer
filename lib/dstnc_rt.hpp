/*
 * transpdist.h
 *
 *  Created on: Jul 2, 2009
 *      Author: maze
 */

#ifndef TRANSPDIST_H_
#define TRANSPDIST_H_

#include "genom.hpp"
#include "dstnc.hpp"

#include "weightedbb.h"

class dstnc_rt : public dstnc{
private:
	weightedbb::WeightedBB *_weightedbb;
	bool _transp, _circular;
	unsigned _n, _wr, _wt;
	genom _gg,
		_hh;
public:
	/**
	 * constructor: initialize M. Bader's median solver
	 * @param[in] n length of the genomes
	 * @param[in] circular circularity
	 * @param[in] wr reversal weight (irrelevant iff transp)
	 * @param[in] wt transposition weight (irrelevant iff transp)
	 * @param[in] transp compute transposition distance iff true
	 * else: weighted reversal transposition distance
	 */
	dstnc_rt(  unsigned n, bool circular, unsigned wr, unsigned wt, bool transp );

	/**
	 * destructor
	 */
	virtual ~dstnc_rt();

	/**
	 * adapt the distance functor to a new genome length
	 * @param[in] n the new length
	 */
	void adapt( unsigned n );

	/**
	 * compute the transposition distance
	 * @param[in] g g
	 * @param[in] g h
	 * @return the distance
	 */
	unsigned calc( const genom &g, const genom &h);

	dstnc* clone() const;

	ostream & output(ostream &os) const;
};

#endif /* TRANSPDIST_H_ */
