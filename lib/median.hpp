#ifndef MEDIAN_HPP_
#define MEDIAN_HPP_

#include <vector>

#include "genom.hpp"
#include "dstnc.hpp"
//#include "dstnc_pinv.hpp"

#define NO_BOUND 0

class mediancache;

/**
 * abstract base class for median solvers
 * derived classes must:
 * 1. assign some distance function to _dst, delete is NOT called automatically by ~mediansolver
 * 2. implement solve()
 * 3. implement output()
 *
 */
class mediansolver{
protected:
	dstnc *_dst;	// the underlying distance funtion
public:

		/**
		 * destructor
		 */
		virtual ~mediansolver();

//		/**
//		 * a redirector to the corresponding distance measure
//		 * @param[in] g1 a genome
//		 * @param[in] g2 another genome
//		 * @param[in] genomes the genomes of a median problem, this is needed because
//		 * some of the distance measures depend on the context of the median problem
//		 * @return the distance
//		 */
//		virtual unsigned distance( const genom &g1, const genom &g2, const vector<genom> &genomes = vector<genom>() ) = 0;

		/**
		 * get the circumference
		 * only available for three input genomes
		 * @param[in] genomes the three input genomes
		 */
		virtual unsigned circumference( const vector<genom> &genomes ) const;

		/**
		 * get a pointer to the distance funtion
		 * @return the pointer
		 */
		dstnc *get_dstncfoo() const;

		/**
		 * print available commandline options
		 * @todo make pure virtual .. i.e. remove implementation and add = 0
		 */
		virtual void printopt() const ;

		/**
		 * get a lower bound for the score of a median problem,
		 * this is the sum of distances of the triangle / 2
		 * where the distance is computed by the distance function of
		 * the corresponding median solver
		 *
		 * @param[in] problem the median problem
		 * @return the lower bound
		 */
		virtual unsigned lower_bound(const vector<genom> &problem ) const;

		/**
		 * get the score of a median problem
		 * @param[in] problem the median problem
		 * @param[in] lower_bound
		 * @param[in] upper_bound
		 * @return the score of the median problem
		 */
		virtual unsigned score( const vector<genom> &problem );

		/**
		 * solve a median problem with the median solver
		 * @param[in] problem the median problem
		 * @param[in] allmed get all medians iff true; otherwise return only one
		 * @param[in] lower_bound lower bound of the median score, only solutions with a
		 * score >= lower_bound are returned, if the value is 0 or INT_MAX the best solutions are returned
		 * @param[in] upper_bound upper_bound of the median score, only solutions with a
		 * score <= upper_bound are returned, if the value is 0 or INT_MAX the best solutions are returned
		 * @param[out] mediancnt the number of medians
		 * @param[out] score the score of the solution or INT_MAX if none found (e.g. when bounds could not be fulfilled)
		 * @param[out] medians the medians
		 */
		virtual void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
				int &mediancnt, int &score, vector<genom> &medians) = 0;

		/**
		 * calculate the sum of pairwise difference value
		 * only available for three input genomes
		 * @param[in] problem the instance
		 * @return spd
		 */
		virtual unsigned spd( const vector<genom> &problem ) const;

		/**
		 * output operator, dispatches to the output function of derived classes
		 * @param[in] os the stream to write in
		 * @param[in] the median solver to output
		 */
		friend ostream &operator<<(ostream &os, const mediansolver &ms);

		/**
		 * the actual output function .. should be implemented in derived classes
		 * @param[in] os the stream to write into
		 * @return the stream
		 */
		virtual ostream & output(ostream &os) const = 0;

		/**
		 * get an upper bound for the score of a median problem
		 * this is the minimum of the sum of the distances of two sides
		 *
		 * @param[in] problem the median problem
		 * @return the upper bound
		 */
		virtual unsigned upper_bound(const vector<genom> &problem ) const;
};


/**
 * class implementing a cached median solver
 */
class mediancache : public mediansolver{
private:
	mediansolver *_solver;
	map<vector<genom>, int > _cache;	// mapping from median problems to an index in the following vectors

	vector<int> _score;							// scores of the cached medians, or upper bound if no median found so far
	vector<vector<genom> > _medians;			// cached medians
	vector<bool> _allmeds;						// are all medians already computed
public:
	mediancache(mediansolver *solver);

	/**
	 * destructor, free the cached memory and delete the median solver
	 */
	~mediancache();

	/**
	 * the output operation
	 * @see mediansolver::output
	 */
	ostream &output( ostream &os ) const;

	/**
	 * solve the median problem with the trex method
	 * @see mediansolver::solve
	 */
	void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
			int &mediancnt, int &score, vector<genom> &medians);
};


/**
 * a generic brute force median solver
 * given a distance function it finds minimal solutions
 * by enumerating all permutations
 */
class median_hcenum : public mediansolver{
private:
	vector<genom> _cand; /* all possible permutations of length n */
public:
	/**
	 * default constructor does nothing
	 */
	median_hcenum(){};

	/**
	 * initialise the median solver to be suitable for genomes of length n
	 * which may be linear or circular
	 *
	 * @param[in] dst the distance function that should be used for minimising
	 * @param[in] n the desired length
	 * @param[in] circular clear, isn't it?
	 */
	median_hcenum( dstnc *dst, unsigned n, bool circular );

	/**
	 * destructor
	 */
	virtual ~median_hcenum();

	/**
	 * the output operation
	 * @see mediansolver::output
	 */
	ostream &output( ostream &os ) const;

	/**
	 * solve median problem be enumeration
	 * @see mediansolver::solve
	 */
	void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
			int &mediancnt, int &score, vector<genom> &medians);
};



/**
 * median solver based on the absence / presence of common intervals
 * IMPORTANT: feed it with unsigned genomes (of length 2n+2)
 */

//class cisolve : public mediansolver{
//private:
//public:
//	/**
//	 *
//	 */
//	cisolve( ){};
//
//	/**
//	 * return the number "incommon" intervals, i.e. the number of intervals of the two
//	 * permutations - 2*the number of common intervals
//	 * @see mediansolver::distance
//	 */
//	unsigned distance( const genom &g1, const genom &g2, const vector<genom> &genomes = vector<genom>() );
//
//	/**
//	 * the output operation
//	 * @see mediansolver::output
//	 */
//	virtual ostream & output( ostream &os ) const;
//
//	vector<genom> pack(const vector<genom> &genomes);
//
//	/**
//	 *
//	 */
//	void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
//			int &mediancnt, int &score, vector<genom> &medians);
//
//	vector<genom> unpack(const vector<genom> &genomes);
//};
//
//
//class trexsolve : public mediansolver{
//private:
//	bool _bps, 			/*! construct alternative break point scenario */
//		_alt;			/*! generate alternative scenarios */
//	unsigned _maxcons,	/*! maximum allowed consistency */
//		_maxszen; 		/*! maximal alternatives in prime nodes */
//public:
//	/**
//	 * initialise a trex median solver
//	 * @param[in] maxcons maximal allowed consistency
//	 * @param[in] bps  construct alternative break point scenario
//	 * @param[in] maxszen maximal alternatives in prime nodes
//	 */
//	trexsolve( unsigned maxcons, bool bps, unsigned maxszen, bool alt ) : _bps(bps), _alt(alt), _maxcons(maxcons), _maxszen(maxszen) {};
//
//	/**
//	 * return crex distance, i.e. the minimum of the distance
//	 * of g1 towards g2 and g2 towards g1
//	 * you dont have to pass genomes this is not needed here
//	 * @see mediansolver::distance
//	 */
//	unsigned distance( const genom &g1, const genom &g2, const vector<genom> &genomes = vector<genom>() );
//
//	/**
//	 * the output operation
//	 * @see mediansolver::output
//	 */
//	virtual ostream & output( ostream &os ) const;
//
//	/**
//	 * solve the median problem with the trex method
//	 * @see mediansolver::solve
//	 */
//	void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
//			int &mediancnt, int &score, vector<genom> &medians);
//};


/**
 * class implementing compare operator for median problems
 * comparison is done by comparing the lowerbounds
 */
class LowerBoundLess : public less<vector<genom> > {
private:
	mediansolver *_ms;	/* the median solver .. the lower bound computation depends on the problem */
	vector<genom> *_genomes,
		_s,_t;
public:
	/**
	 * constructor
	 * @param[in] nms a median solver, this will be used for lower bound computation
	 * @param[in]
	 */
	LowerBoundLess( mediansolver *ms, vector<genom> *genomes ) : _ms(ms), _genomes(genomes){
		_s = vector<genom>(3);
		_t = vector<genom>(3);
	}

	/**
	 * the comparison operator
	 * @param[in] x a median problem
	 * @param[in] y another median problem
	 * @return true iff lb(x) < lb(y), false else
	 */
	bool operator()(vector<unsigned> x, vector<unsigned> y) {
		for(unsigned i=0; i<3; i++){
			_s[i]=(*_genomes)[x[i]];
			_t[i]=(*_genomes)[y[i]];
		}
		return _ms->lower_bound(_s) > _ms->lower_bound(_t);
	}
};

///**
// * compute the circumference of a median problem
// * \f$ c = d_{0,1}+d_{1,2}+d_{2,0}\f$
// * @param[in] genomes the median problem (must be 3 genomes)
// * @param[in] pair_pd use the common intervals of genome pairs for perfect reversal distance computation, else use common intervals of all genomes
// * @param[in] common use perfect reversal distance
// * @param[in] hd helping memory
// */
//int circumference(const vector<genom> &genomes, int common, int pair_pd, hdata &hd);

///**
// * compute the lower bound of a median problem
// * \f$ lb = \lceil 1/2 (d_{0,1}+d_{1,2}+d_{2,0}) \rceil \f$
// * @param[in] genomes the median problem (must be 3 genomes)
// * @param[in] common use perfect reversal distance
// * @param[in] pair_pd use the common intervals of genome pairs for perfect reversal distance computation, else use common intervals of all genomes
// * @param[in] hd helping memory
// * @return the lower bound = ceil(circumference / 2)
// */
//int lower_bound(const vector<genom> &genomes, int common, int pair_pd, hdata &hd);

///**
// * compute the sum of pairwise differences for a median problem
// * \f$ spd = |d_{0,1}-d_{0,2}| + |d_{1,0}-d_{1,2}| + |d_{2,0}-d_{2,1}|\f$
// *
// * @param[in] genomes the median problem
// * @param[in] common use perfect reversal distance
// * @param[in] hd helping memory
// * @return the spd
// */
//
//int sum_of_pairwise_differences(const vector<genom> &genomes, int common, hdata &hd);

///**
// * compute the lower bound for a median problem
// * \f$ ub = min\{d_{0,1}+d_{0,2}, d_{1,0}+d_{1,2}, d_{2,0}+d_{2,1}\} \f$
// * @param[in] genomes the median problem
// * @param[in] common use perfect reversal distance
// * @param[in] pair_pd use the common intervals of genome pairs for perfect reversal distance computation, else use common intervals of all genomes
// * @param[in] hd helping memory
// * @return the upper bound
// */
//int upper_bound(const vector<genom> &genomes, int common, int pair_pd);

#endif /*MEDIAN_HPP_*/
