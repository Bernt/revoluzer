/*
 * dstnc_crex.hpp
 *
 *  Created on: Sep 25, 2010
 *      Author: maze
 */

#ifndef DSTNC_CREX_HPP_
#define DSTNC_CREX_HPP_

#include "crex.hpp"
#include "dstnc.hpp"

/**
 * rearrangement distance as number of events in a crex scenario
 */
class dstnc_crex : public dstnc {
private:
	bool _bps, 			/* add breakpoint scenario */
		_mkalt;			/* make alternative scenarios */
	unsigned _maxszen;	/* maximal number of alternative scenrios for prime nodes */
	vector<int> _srcinv, _tgtinv;
public:

	/**
	 * constructor
	 *
	 * @param[in] n maximum element to appear in the gene orders
	 * @param[in] bps add breakpoint scenarios
	 * @param[in] mkalt make alternative scenarios for transp. and reverse transpositions
	 * @param[in] maxszen maximum number of alternative scenarios for prime nodes
	 */
	dstnc_crex( unsigned n, bool bps, bool mkalt, unsigned maxszen = 2 );

	/**
	 * destructor
	 */
	virtual ~dstnc_crex();

	/**
	 * adapt the distance calculator to a new genome length
	 * @param[in] n the new length
	 */
	void adapt(unsigned n);

	/**
	 * get the crex distance between two gene orders
	 * @param[in] src a genome
	 * @param[in] tgt another genome
	 * @return the reversal distance
	 */
	unsigned calc( const genom &src, const genom &tgt );

	/**
	 * return a pointer to a copy of a dstnc
	 * this is a virtual copy constructor
	 * @return the pointer
	 */
	dstnc* clone() const;

	/**
	 * output function
	 * @param[in] stream to write into
	 * @return the stream
	 */
	ostream & output(ostream &os) const;
};

#endif /* DSTNC_CREX_HPP_ */
