/*
 * character.hpp
 *
 * defines an abstract base class for a character
 * basically it has no meaning and functionality
 * but it serves to treat different character types
 * in a unified framework
 *
 * the only functionality that has to be implemented
 * in derived classes are clone, create, and
 * output functions
 *
 * @author: maze
 */

#ifndef CHARACTER_HPP_
#define CHARACTER_HPP_

#include <iostream>

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

class character;

/**
 * abstract base class for functors computing the weight of a character
 */
class character_weight{
private:
public:
	character_weight(){};

	/**
	 * virtual destructor stub
	 */
	virtual ~character_weight(){};

	/**
	 * function object computing the weight
	 * @param[in] c a character
	 * @return weight of c
	 */
	virtual double operator()(const character *c ) const = 0;
};


/**
 * computing the weight of a character as 1
 */
class character_weight_unit : public character_weight{
private:
public:
	/**
	 * constructor
	 */
	character_weight_unit(){};

	/**
	 * destructor
	 */
	~character_weight_unit(){};

	/**
	 * just return 1.0 as weight of any given character
	 * @param c a character
	 * @return weight = 1.0
	 */
	double operator()(const character *c ) const { return 1.0; };
};

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/**
 * abstract base class for a phylogenetic character
 */
class character{

public:
	/**
	 * virtual destructor stub
	 */
	virtual ~character(){};

	/**
	 * copy constructor
	 * @return a copy of the character
	 */
	virtual character* clone() const = 0;

	/**
	 * constructor
	 * @return an empty character
	 */
	virtual character* create() const = 0;

	/**
	 * output a textual representation of the character
	 * @param[in,out] out the stream to write into
	 */
	virtual void output( std::ostream &out ) const = 0;

};



#endif /* CHARACTER_HPP_ */

