/*
 * chains.hpp
 *
 *  Created on: Jun 2, 2009
 *      Author: maze
 */

#ifndef CHAINS_HPP_
#define CHAINS_HPP_

#include <vector>
#include "genom.hpp"

using namespace std;

/**
 * chains of two permutations
 */
class chains {
private:
	/**
	 * the number of elements in the two permutations for which the chains are
	 * computed
	 */
	int _n;

	/**
	 * the number of chains, or an upper bound if not all elements (pairs) are
	 * added
	 */
	int _cnt;

	/**
	 * there are n-1 pairs of elements which are possibly connected (the
	 * consecutive pairs in the target permutation) for each pair it is stored
	 * if they are connected in the same chain (true), or not (false)
	 */
	vector<bool> _connect;

	vector<int> *_srcinv,	/** pointer to the inverse permutation of the source permutation */
		*_tgtinv;			/** pointer to the inverse permutation of the target permutation */
	const genom *_tgt;		/** pointer to the target permutation */
public:
	/**
	 * initialize chains, suitable for a permutation on length n,
	 * all possible connected pairs are set to true, i.e. there is 1 chain
	 */
	chains( int n, vector<int> *srcinv, vector<int> *tgtinv, const genom *tgt);

	/**
	 * get the number of chains; note that if not all orientations of successive
	 * pairs are known then only a lower bound is returned. This is because
	 * if its unknown if two elements are connected, then they are regarded
	 * as connected
	 *
	 * @param[in] tpe number of unprocessed elements
	 * @return the number of chains
	 */
	unsigned cnt( ) const;

	/**
	 * output the chains
	 * @param[in,out] out the stream to write to
	 * @param[in] c the chains
	 */
	friend ostream &operator<<(ostream &os, const chains &c);

	/**
	 * when building the source permutation successively by adding an element e
	 * this function updates the chains accordingly
	 * - lookup the right r and left neighbour (if existent) in the target
	 *   permutation and add the pair le and er to the chains
	 */
	void update( int e );

	/**
	 * update chains given the information that d is left of e in the target
	 * permutations if d and e are in the same orientation in the source
	 * permutation:
	 * - decrease the number of chains by 1 (if not already connected)
	 * - mark as connected
	 * @param[in] d d
	 * @param[in] e e
	 */
	void update( int d, int e );

};

#endif /* CHAINS_HPP_ */
