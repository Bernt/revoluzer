/*
 * dstnc_pinv.cpp
 *
 *  Created on: May 24, 2010
 *      Author: maze
 */

#include "dstnc_pinv.hpp"

#include <limits>


dstnc_pinv::dstnc_pinv( unsigned n ) {
	adapt(n);
}

dstnc_pinv::~dstnc_pinv() {
	delete _rd;
	_strong_int.clear();
	_r.clear();
	_l.clear();
	_strong_int.clear();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void dstnc_pinv::adapt( unsigned n ){
	_n = n;
	_strong_int = vector<pair<int,int> >(2*n, pair<int,int>());
	_r = vector<int>( n+1, 0 );
	_l = vector<int>( n+1, 0 );
	_rd = new dstnc_inv(n, false);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned dstnc_pinv::calc( const genom &src, const genom &tgt ){
	genom g;				// the identified genome
	unsigned n=src.size();	// length of the genomes

	// get the strong intervals of the genomes
	strong_intervals(src, tgt, n, _r, _l, _strong_int);

	// now identify in the correct order ( make the g1 the target and g2 the source
	// -- because the strong intervals .. and therefore the interval tree is computed
	// relative to g2 ) to get the interval tree
	g = tgt.identify_g(src);
	return _perfect_distance( g );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// compute the perfect distance between g1 and g2 where the common intervals
// of genomes have to be preserved
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned dstnc_pinv::calc( const genom &src, const genom &tgt, const vector<genom> &genomes ){
	genom g;				// the 'identified' genomes (g->id is the same as g1->g2)
	unsigned n = src.size();		// genome length
	vector<genom> gi;

		// construct the genome vector consisting of g1, g2, genome_1...genome_m
		// in order to compute the intervals relative to g1
	gi.push_back(src);
	gi.push_back(tgt);
	gi.insert(gi.end(), genomes.begin(), genomes.end());

		// get the strong intervals relative to gi[0], i.e. g1
	strong_intervals(gi, n, _r, _l, _strong_int );

			// now identify in the correct order ( make the g1 the target and g2 the source
			// -- because the strong intervals .. and therefore the interval tree is computed
			// relative to g2 ) to get the interval tree
	g = gi[1].identify_g(gi[0]);
	return _perfect_distance( g );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

dstnc* dstnc_pinv::clone() const{
	return (new dstnc_pinv( _n ) );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void dstnc_pinv::_interval_tree_properties(itnode *p, const genom &g,
		unsigned n, unsigned &dist) {

	for( unsigned i=0; i<p->children.size(); i++ ){
		_interval_tree_properties( p->children[i], g, n, dist);
	}

		// prime node with linear parent (i.e. known sign)
		// compute the distance depending on the sign of the parent
	if(p->type == PRI){
		dist += _prime_distance(p, n, g);
	}
		// linear node
		// count the number of child nodes with different sign
		// (so far the orientation of the root does not matter, i.e. linear undirected genome model)
	else {
		if( p->parent == NULL ){
				// currently i want the linear directed genome model
				// so check if the (linear) root is decreasing, if so add a reversal
				// (comment out to get linear undirected)
			if ( p->sign == DEC ){
#ifdef DEBUG_PERFECTD
				cout << "sign diff root"<<endl;
#endif//DEBUG_PERFECTD
				dist ++;
			}
		}else{
			if( p->parent->type != PRI && ( p->sign != p->parent->sign ) ){
#ifdef DEBUG_PERFECTD
				cout << "sign diff "<<"["<<p->i.first <<","<<p->i.second<<"] - ["<<p->parent->i.first<<","<<p->parent->i.second<<"]"<<endl;
#endif//DEBUG_PERFECTD
				dist++;
			}
		}
	}
//	cout << "["<<p->i.first <<","<<p->i.second<<"] :"<<dist<<endl;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned dstnc_pinv::_perfect_distance( const genom g ) {
	unsigned n = g.size(),
		dist = 0;		// the computed distance
	itnode* tree_root;	// the tree (accesible by its root)

	interval_tree( _strong_int, n, g, &tree_root );
	interval_tree_type( tree_root, _r, _l, g );
	interval_tree_sign( tree_root, g );
#ifdef DEBUG_PERFECTD
	cout << "g "<<g<<endl;
	cout << "strong intervals"<<endl;
	for(unsigned i=0; i<_strong_int.size(); i++)
		cout << "["<< _strong_int[i].first <<","<<_strong_int[i].second<<"] ";
	cout << endl;
	cout << "interval tree"<<endl;
	interval_tree_print(tree_root, g, cout, "");
#endif
	_interval_tree_properties(tree_root, g, n, dist);
		// free memory
	interval_tree_free(tree_root);
#ifdef DEBUG_PERFECTD
	cout << "distance "<<dist<<endl;
#endif//DEBUG_PERFECTD
	return dist;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned dstnc_pinv::_prime_distance( itnode *p, int n, const genom &g ){
	genom id,
		nid;
	int pdist = std::numeric_limits< int >::max(),		// best distance to the positive identity
		ndist = std::numeric_limits< int >::max(),		// best distance to the negative identity
		d,
		options;
	static vector<int> ambiguous_signs,	// current sign assignment to children with unknown sign
		ambiguous_pos;					// indices of children with unknown sign
	int ambiguous_cnt = 0;				// number of childnodes with unknown sign

	if((int) ambiguous_signs.size() != n){
		ambiguous_signs = vector<int>(n, INC);
		ambiguous_pos = vector<int>(n, 0);
	}

#ifdef DEBUG_PRIMED
	cout << "prime distance for ["<<p->i.first<<","<<p->i.second<<"] typ "<< p->type<<" sig "<< p->sign <<endl;
#endif

		// get quotient permutation
		// - take an arbitrary element of each children (e.g. the first)
		// - compute the quotient permutation
		// - assign the signs of the children to the corresponding elements of the
		// 	quotient permutation (if unknown add the to the ambiguos positions)
	genom quot;
	quotient_permutation(p, g, quot, 0, 0);

	for(unsigned i=0; i<p->children.size(); i++){
		if(p->children[i]->sign == DEC)
			quot[i] *= -1;
		else if( p->children[i]->sign == UNK ){
			ambiguous_signs[ambiguous_cnt] = INC;
			ambiguous_pos[ambiguous_cnt] = i;
			ambiguous_cnt++;
		}
	}
//	cout << "qout "<<quot<<endl;
	id = genom(quot.size(), 0);
	nid = genom_nid(quot.size());

	options = ppow( ambiguous_cnt );
#ifdef DEBUG_PRIMED
	cout << ambiguous_cnt<<" ambig. positions -> #options = "<<options <<endl;
	cout << "ambiguous positions: ";
	for(int j=0; j<ambiguous_cnt; j++){
		cout << ambiguous_pos[j] <<" ";
	}cout << endl;
#endif

	for(int i=0; i<options; i++){
			// assign the current sign assignment to the ambiguous positions
		for(int j=0; j<ambiguous_cnt; j++){
			if(quot[ ambiguous_pos[j] ] > 0 && ambiguous_signs[j] == DEC)
				quot[ ambiguous_pos[j] ] *= -1;
			if(quot[ ambiguous_pos[j] ] < 0 && ambiguous_signs[j] == INC)
				quot[ ambiguous_pos[j] ] *= -1;
		}

		if(p->sign == INC || p->sign == UNK){
			d = _rd->calc( quot, id);
			if(d<pdist){
				pdist = d;
			}
		}
			// if negative -> sort to nedative identity d( q, -id ) = d( -q, id )
		if(p->sign == DEC || p->sign == UNK){
			d = _rd->calc(quot, nid);
			if(d<ndist){
				ndist = d;
			}
		}

			// compute the next possible sign assignment
		ambiguous_signs[0]++;
		for(int j=0; j<ambiguous_cnt; j++){
			if(ambiguous_signs[j] == DEC || ambiguous_signs[j] == INC){
				break;
			}else{
				ambiguous_signs[j] = INC;
				if(j+1<ambiguous_cnt)
					ambiguous_signs[j+1]++;
			}
		}
	}
#ifdef DEBUG_PRIMED
	cout << "pdist  = "<<pdist<<" ndist = "<<ndist <<endl;
#endif
		// if the sign of the node is known (only possible if it is the child of a linear node)
		// than take the minimal distance to the positive/negative identity, depending on the
		// sign of the parent node
		// if the sign is unknown take the minimal distance and set the sign of the
		// node if the distances are unequal, else the sign stays unknown
	if(p->sign == INC){
		d = pdist;
	}else if(p->sign == DEC){
		d = ndist;
	}else{
		if(pdist < ndist){
			p->sign = INC;
			d = pdist;
		}else if(ndist < pdist){
			p->sign = DEC;
			d = ndist;
		}else{
			d = pdist;
		}
	}
#ifdef DEBUG_PRIMED
	cout << "prime distance = "<<d <<endl;
#endif
	return d;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ostream & dstnc_pinv::output(ostream &os) const{
	os << "preserving inversion distance for n="<<_n;
	return os;
}




// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// perfect distance computation for c programs
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int perfect_distance_c(int *g1_p, int *g2_p, int n ){

	genom g1(g1_p, n, 0),
		g2(g2_p, n, 0);
	int dist = 0;
	dstnc_pinv *pinv = new dstnc_pinv(n);
	dist = pinv->calc( g1, g2 );
	delete pinv;
	return dist;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int perfect_distance_glob_c( int *g1_p, int *g2_p, int **genomes_p, int n,
		int m ){

	genom g1(g1_p, n, 0),
		g2(g2_p, n, 0);
	int dist = 0;
	vector<genom> genomes;
	dstnc_pinv *pinv = new dstnc_pinv(n);

	for( int i=1; i<=m; i++ ){
		genomes.push_back( genom(genomes_p[i], n, 0) );
	}

	dist = pinv->calc( g1, g2, genomes );
	delete pinv;
	return dist;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int perfect_distance_glob_sum_c( int *g1_p, int *genomes_p1, int *genomes_p2,
		int *genomes_p3, int n){

	genom g1(g1_p, n, 0);
	int dist = 0;
	vector<genom> genomes;
	dstnc_pinv *pinv = new dstnc_pinv(n);

	genomes.push_back( genom(genomes_p1, n, 0) );
	genomes.push_back( genom(genomes_p2, n, 0) );
	genomes.push_back( genom(genomes_p3, n, 0) );

	dist = pinv->calcsum(g1, genomes, genomes);
	delete pinv;
	return dist;
}
