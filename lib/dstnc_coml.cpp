/*
 * dstnc_coml.cpp
 *
 *  Created on: Mar 31, 2011
 *      Author: maze
 */

#include "dstnc_coml.hpp"
#include "common.hpp"

dstnc_coml::dstnc_coml(  unsigned n, bool circular  ) {
	_circular = circular;
	adapt( n );
}

dstnc_coml::~dstnc_coml() {
}

void dstnc_coml::adapt( unsigned n ){
	_n = n;

	// max inclusive trivial
	if( ! _circular ){
		// sum_{i=1}^n i*(n-i+1) = 1/6 n (n+1) (n+2)
		_max = (n*(n+1)*(n+2))/6  ;
	}else{
		// sum_{j=1}^{n-1} j*n = 1/2 (n-1) n^2
		// j*n: of each size j there are n possible intervals
		// + 1 of size n
		_max = ((n-1)*n*n)/2 + n;
	}
//	cout <<"max"<< _max<<endl;
};

unsigned dstnc_coml::calc( const genom &src, const genom &tgt ){
	unsigned s=0;
	vector<pair<int, int> > ci;

	// Test for valid chromosom
	if ( _n != src.size() || _n != tgt.size() ) {
		cerr << "error: difference in number of genes: "<<_n<<" "<<src.size()<<" "<<tgt.size()<<endl;
		cout << src << endl<<tgt<<endl;
		exit( EXIT_FAILURE );
	}

//	cout <<"cid src "<< src<<endl;
//	cout <<"cid tgt "<< tgt<<endl;
//                                       triv sign
	common_intervals(src, tgt, _circular,   1 ,   0, ci);
//	cout << ci.size()<<endl;;
//	cout << "----------"<<endl;
	s = 0;
	for(unsigned i=0; i<ci.size(); i++){
//		cout << ci[i].first<< ":"<<ci[i].second<<" ";
		if( ci[i].first > ci[i].second ){
//			cout << (_n-ci[i].first + ci[i].second+1)<<endl;
			s+= (_n-ci[i].first + ci[i].second+1);
		}else{
//			cout << (ci[i].second-ci[i].first+1)<<endl;
			s+= (ci[i].second-ci[i].first+1);
		}
	}
//	cout << "----------"<<s<<endl;
	return _max - s;

}

dstnc* dstnc_coml::clone() const{
	return new dstnc_coml( _n, _circular );
}

//dstnc* dstnc_com::create(){
//	return new dstnc_com();
//}

ostream & dstnc_coml::output(ostream &os) const{
	os << "length weighted common interval distance for n="<<_n;
	return os;
}
