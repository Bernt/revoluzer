#ifndef _CAPRARA_HPP_
#define _CAPRARA_HPP_

	// caprara, cip, ecip defines

	// bounds
#define UB 1
#define LB -1

#define DYN_CONSTRAINTS 1
#define STA_CONSTRAINTS 0

#define ONE_MEDIAN 0
#define ALL_MEDIAN 1

#define LAZY 1
#define NOTLAZY 0

// rmp methods
#define RMPCAPRARA 1

// preserving median solver methods
#define CIP 1
#define ECIP 2
#define TCIP 3

	// TCIP defines
//#define M 3

// if ITERCOMB is not defined
// - mpqnode
//   * define a vector min storing the minimal scores for each target orientation
// - in tcip_pnode_medians():
//   * store the 'sum' from the leaves of the P-component in the scores array
//   * compute the min vector the minimal sums over all ambigous sign assignments for the target orientations
//   * at the end set all values in the scores multidim matrix which are greater
//     than the corresponding target orientation to INT_MAX
// - in tcip_construct_medians:
//   * do not iterate over all possible sign assignments

//#define ITERCOMB

#include <vector>
#include "common.hpp"
#include "dstnc_pinv.hpp"
#include "genom.hpp"
#include "median.hpp"

using namespace std;

class rmp : public mediansolver{
private:
public:

	/**
	 * constructor
	 * @param[in] n length of the gene orders to handle
	 * @param[in] circularity of the gene orders to handle
	 */
	rmp( unsigned n, bool circular );

	/**
	 * destructor
	 */
	~rmp();

	/**
	 * the output operation
	 * @see mediansolver::output
	 */
	virtual ostream & output( ostream &os ) const;

	/**
	 * solve a reversal median problem
	 * @see mediansolver::solve
	 */
	void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
			int &mediancnt, int &score, vector<genom> &medians);
};


class rmp_common : public mediansolver{
private:
	int _method,
		_pair_pd;
public:
	/**
	 * construct a common interval preserving median solver
	 * @param[in] method CIP, ECIP, TCIP(default)
	 */
	rmp_common( unsigned n, bool circular, int method, int pair_pd);

	/**
	 * the output operation
	 * @see mediansolver::output
	 */
	ostream &output( ostream &os ) const;

	/**
	 * solve a reversal median problem
	 * @see mediansolver::solve
	 */
	void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
			int &mediancnt, int &score, vector<genom> &medians);
};


	// a node of an interval tree
typedef struct mpqnode_struct{
	mpqnode_struct* parent;		// the parent node
	vector<mpqnode_struct* > children;	// the child nodes

	pair<int,int> i;	// the interval
	int type; 			// P, Q
	int signs;			// INC / DEC (for each of the 3 genomes); (+++: 0, ++-:4, +-+:2, +--:6 )

	vector<vector<genom> > medians; // local medians of the node (for each combination of target orientations and ambigous signs)
	vector<int> scores;				// scores of the local medians ((4^p)-dimensional for P-nodes)
									// if not ITERCOMB -> smallest sum from the leaves
#ifndef ITERCOMB
	vector<int> min;				// minimal sum of of the target orientation
									// (minimal over all possibilities of amb. sign assignments)
#endif
} mpqnode;

/**
 * prints usage information for the program
 */
void usage();

/**
 * wrapper for capraras median solver in grappa
 * @param[in] genomes the genomes
 * @param[in] dynamic_restrictions this toggles the TCIP behaviour if common / conserved intervals
 * are preserved
 * @param[in] conserved use conserved interval restriction
 * @param[in] common use common interval restriction
 * @param[in] pair_pd use pairwise perfect distance / global perfect distance:
 * 	a) pairwise: for each pair of median,permutation the common intervals are computed and
 * 		the perfect distance is computed with respect to these intervals
 *  b) global: the common intervals for the permutations are computed and the perfect distance
 * 		for each pair of permutation,median is computed with respect to these common intervals
 * @param[in] sig use signed common intervals
 * @param[in] circular treat the genomes as circular onescircular
 * @param[in] lower_bound lower bound of the median score,
 *	only solutions with a score >= lower_bound are returned,
 *	if the value is 0 or INT_MAX the best solutions are returned
 * @param[in] upper_bound upper_bound of the median score,
 *	only solutions with a score <= upper_bound are returned,
 *	if the value is 0 or INT_MAX the best solutions are returned
 * @param[in] allmed get all medians
 * @param[in] lazy if in the one-median-version the trivial median score is equal to the lower bound
 *	simply take this one (this is the standard behaviour implemented in GRAPPA .. uhh ohh)
 * @param[out] mediancnt number of medians
 * @param[out] medians the medians
 */

void caprara(const vector<genom> &genomes,
	int dynamic_restrictions, int conserved, int common, int pair_pd,
	int sig, int circular, int lower_bound, int upper_bound,
	int allmed, int lazy,
	int &mediancnt, vector<genom> &medians);

/**
 * initialise a pqnode
 *
 * - sets the given parent
 * - adds the new node to the list of children of the parent
 * - sets the given type and interval
 * - sign is initialised to 0
 *
 * @param[in] parent the parent node
 * @param[out] n the node which should be initialised
 * @param[in] type the type of the node P/Q
 * @param[in] interval the corresponding interval
 */
void init_mpqnode(mpqnode* parent, mpqnode** n, int type, pair<int,int> interval);

/**
 * get the p-th sign of a node n
 * @param[in] n the node
 * @param[in] p the position 0..(m-1)
 * @param[in] m the number of genomes which are represented by the mpq-tree
 * @return the sign INC / DEC
 */
int mpqnode_getsign( mpqnode *n, int p, unsigned m);

/**
 * set the p-th sign of a node n
 * @param[in] n the node
 * @param[in] pos the position 0..(m-1)
 * @param[in] m the number of genomes which are represented by the mpq-tree
 * @param[in] sign the sign to set
 */
void mpqnode_setsign( mpqnode *n, int pos, int sign, unsigned m );

/**
 * build the pqtree for the given (identified) genomes
 * @param[in] genomes the (identified) genomes
 * @param[in] n the length of the genomes
 * @param[out] mpqroot the root of the constructed mpqtree
 */
void mpqtree(const vector<genom> &genomes, int n, mpqnode **mpqroot);

/**
 * build a mpqtree for a set of genomes
 * @param[in] genomes the genomes
 * @param[in] n length of the genomes
 * @return pointer to the root node of the constructed tree
 */
mpqnode * mpqtree_construct(const vector<genom> &genomes, int n);

/**
 * print a mpq tree
 * @param[in] p the node to start the printing
 * @param[in] m the number of genomes which are represented by the mpq-tree
 */
void mpqtree_print(mpqnode *p, unsigned m);

/**
 * print a mpq tree as dot string
 * @param[in] p the node to start the printing
 * @param[in] g the reference genome used for building the tree
 * @param[in] m number of genomes which the mpw-tree represents
 * @param[in,out] o the stream to write into
 * @param[in] horizontal print the signs horizontal on the to o each node, otherwise vertical on the right
 * @param[in] nmap namemap if given the names are printed, else the elements
 */
void mpqtree_print_dot(mpqnode *p, const genom &g, unsigned m, ostream &o, bool horizontal);

/**
 * get the primenodes of an mpqtree
 * @param[in] p the root of the tree
 * @param[out] pnode the prime nodes
 */
void mpqtree_primenodes(mpqnode *p, vector<mpqnode *> &pnode);

/**
 * get the quotient permutations of a node
 * @param[in] node a node of a mpqtree
 * @param[in] genomes the original genomes
 * @param[in] n length of the genomes
 * @param[in] inv_pis inverse permutations of genomes
 * @param[in] sign get signed quotient permutations iff true
 * @param[out] qperm the quotient permutations
 */
void mpqtree_quotientpermutations( mpqnode *node, const vector<genom> &genomes, int n,
	const vector<vector<int> > &inv_pis, bool sign, vector<genom> &qperm );
/**
 * determine the signs of the Q nodes
 * @param[in] n the start node .. the function will determine the signs of nodes in the subtree recursively
 * @param[in] genomes the genomes for which the tree is build
 * @param[in] inv_pis the inverse permutations of the genomes
 */
void mpqtree_signs(mpqnode *n, const vector<genom> &genomes, const vector<vector<int> > &inv_pis);

/**
 * remove a pqtree from the memory
 * @param[in] p the root of the pqtree
 */
void mpqtree_free(mpqnode *p);

/**
 * get the front of the mpq-sub-tree starting at the given node
 * @param[in] n the start node
 * @param[in] g the genome (usually the first in the dataset
 * @param[out] the front
 */
void mpqtree_front( mpqnode *n, const genom &g,  genom &f );

/**
 * check if the pqtree has a pnode
 * @param p node of the pq tree where the check should start
 * @return true if it contains a pnode, else false
 */
bool mpqtree_haspnode(mpqnode *p);

/**
 * @todo implement lower bound and upper bound
 * @param[in] genomes the median problem
 * @param[in] allmed compute all medians if equal ALL_MEDIAN, compute only one median if equal ONE_MEDIAN
 * @param[out] mediancnt the number of found distinct medians
 * @param[out] score the score of the found medians
 * @param[out] medians the actual medians
 */
void tcip(const vector<genom> &genomes,
		int allmed, int &mediancnt,
		int &score, vector<genom> &medians);

/**
 * wrapper to use tcip from C
 * @param[in] gen the input genomes
 * @param[in] ngenes number of genes per genome
 * @param[in] circular handle the genomes as circular if != 0
 * @param[out] median pointer to the median
 * @return the score
 */
extern "C" int tcip_c( struct genome_struct **gen, int ngenes, int circular, int *median );

/**
 * apply local medians of node n to the global medians
 * @param[in] n the node of the pq tree
 * @param[in] lmed the local medians
 * @param[in,out] medians the global medians
 */
void tcip_apply_lmedians( const mpqnode *n, const vector<genom> &lmed, vector<genom> &medians );

/**
 * construct the medians from the medians of the nodes of the given pqtree
 * @param[in] n the starting node of the pqtree (root)
 * @param[in] medians the medians (init with pi_0)
 * @param[out] score the score of the medians
 * @param[in] allmed if 0 get only 1 median, else get all medians
 */
void tcip_construct_medians( mpqnode *n, vector<genom> &medians, int &score, int allmed);

/**
 * solve the median problems for all nodes
 * @param[in] node the node to start with (usually the root)
 * @param[in] genomes the original median problem
 * @param[in] n the length of the genomes of the original problem
 * @param[in] inv_pis the inverse permutations of the original median problem
 * @param[in] allmed compute all medians, no changes here but calling tcip_pnode_medians whith the desired value
 * @param[in] rd reversal distance functor
 */
void tcip_node_medians( mpqnode *node, const vector<genom> &genomes, int n,
		const vector<vector<int> > &inv_pis, int allmed, dstnc_inv *rd);

/**
 * solve the median problem(s) of a P node
 * @param[in] node the node to start with (usually the root)
 * @param[in] genomes the original median problem
 * @param[in] n the length of the genomes of the original problem
 * @param[in] inv_pis the inverse permutations of the original median problem
 * @param[in] allmed compute all medians, toggles all median computation for the oRMPs
 * @param[in] rd reversal distance functor
 */
void tcip_pnode_medians(mpqnode *node, const vector<genom> &genomes, int n,
		const vector<vector<int> > &inv_pis, int allmed, dstnc_inv *rd);

/**
 * get the best combinations target orientations of the components by
 * - extensive enumeration i.e. all possible "target sign" assignments (if ITERCOMB)
 * - combination of the best target orientation (if not ITERCOMB)
 *
 * @param[in] component the nodes of the component
 * @param[out] solution the best target orientation combinations
 * @param[out] ambpos the ambigous positions of the nodes in the component
 * @param[out] marrpos a vector (for each node of the component) for accession the m-dim arrays of the nodes
 * @param[out] marrlen a vector specifying the lengths of the dimensions of the m-dim arrays of the nodes
 * @param[in] allmed get all medians if != 0
 * @return the score
 */
int tcip_solve_component( vector<mpqnode *> &component, vector<vector<int> > &solution, vector<vector<int> > &ambpos,
	vector<vector<int> > &marrpos, vector<vector<int> > &marrlen, int allmed  );

#endif
