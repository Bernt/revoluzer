/** @file distmat.cpp
 * output the distance matrix of a given dataset
 */

#include <iostream>
#include <vector>

#include "distmat.hpp"


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

vector<vector<unsigned> > distance_matrix(const vector<genom> &genomes,
		dstnc *dist){

	vector<vector<unsigned> > dist_mat =
			vector<vector<unsigned> >( genomes.size(),
					vector<unsigned>(genomes.size(), 0) );

	distance_matrix( genomes, dist, dist_mat );

	return dist_mat;

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void distance_matrix(const vector<genom> &genomes,
	dstnc *dist, vector<vector<unsigned> > &dist_mat){

	for(unsigned i=0; i<genomes.size(); i++){
		for(unsigned j=0; j<genomes.size(); j++){
			if( i == j )
				continue;
			dist_mat[i][j] = dist->calc( genomes[i], genomes[j] );
		}
	}

	return;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void reset_distance_matrix(unsigned pos,
		vector<vector<unsigned> > &dist_mat){

	for(unsigned i=0; i<dist_mat.size(); i++)
		dist_mat[i][pos] = dist_mat[pos][i] = 0;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void update_distance_matrix(vector<genom> &genomes, unsigned pos,
	dstnc *dist, vector<vector<unsigned> > &dist_mat){

	for(unsigned i=0; i<genomes.size(); i++){
		dist_mat[i][pos] = dist_mat[pos][i] = dist->calc(genomes[i], genomes[pos]);
	}
}


//void distance_matrix(const vector<genom> &genomes,
//	int conserved, int common, int pair_pd, int sign,
//	int interval, int bp, int circular, hdata &hd, vector<vector<int> > &dist_mat){
//
//	int max;
//	vector<pair<int,int> > ci; 	// common intervals
//
//	max = ( genomes[0].size() * (genomes[0].size()+1) ) /2;
//	if(circular)
//		max *= 2;
//
//	dstnc *dst = NULL;
//	if( interval ){
//		if( common ){dst = new dstnc_com( genomes[0].size(), circular);}
//		else if(conserved){}
//	}else{
//		if(common){}
//		else if(conserved){}
//		else if (bp){dst = new dstnc_bp( genomes[0].size());}
//		else{dst = new dstnc_inv( genomes[0].size(), circular );}
//	}
//
//	for(unsigned i=0; i<genomes.size(); i++){
//		for(unsigned j=i+1; j<genomes.size(); j++){
//			if(interval){	// nr of intervals
//				if(common){
//					common_intervals(genomes[i], genomes[j], circular, 1, sign, ci);
//					dist_mat[i][j] = dist_mat[j][i] = max - ci.size();
//						// find_common_intervals(genomes[i], genomes[j], sign, 0, circular, 1, hd).size();
//				}else if(conserved){
//					dist_mat[i][j] = dist_mat[j][i] = conserved_intervals(genomes[i], genomes[j], circular, hd).size();
//				}
//			}else{			// nr of reversals
//				if(common){
//					if(pair_pd == 1){
//						dist_mat[i][j] = dist_mat[j][i] =
//							perfect_distance(genomes[i], genomes[j], hd);
//					}else{
//						dist_mat[i][j] = dist_mat[j][i] =
//							perfect_distance_glob(genomes[i], genomes[j], genomes, genomes[i].size(), hd);
//					}
//				}else if(conserved){
//					dist_mat[i][j] = dist_mat[j][i] =
//						preserving_reversal_distance(genomes[i], genomes[j], circular, hd);
//				}else if (bp){
//					dist_mat[i][j] = dist_mat[j][i] =
//						dst->calc(genomes[i], genomes[j]);
//				}else {
//					dist_mat[i][j] = dist_mat[j][i] =
//						dst->calc(genomes[i], genomes[j]);
//				}
//			}
//		}
//	}
//
//	return;
//}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//void update_distance_matrix(vector<genom> &genomes, unsigned pos,
//	int conserved, int common, int sign,
//	int interval, int bp, int circular, hdata &hd, vector<vector<int> > &dist_mat){
//
//	vector<pair<int, int> > comint;
//
//	// @todo if all distance funtions are implemented as distance objects
//	// then remove this funtion, the includes (all but dstnc)
//	dstnc *dst = NULL;
//	if( interval ){
//		if( common ){}
//		else if(conserved){}
//	}else{
//		if(common){}
//		else if(conserved){}
//		else if (bp){
//			dst = new dstnc_bp( genomes[0].size());
//		}else{
//			dst = new dstnc_inv( genomes[0].size(), circular );
//		}
//	}
//
//	for(unsigned i=0; i<genomes.size(); i++){
//		if(interval){	// nr of intervals
//			if(common){
//				common_intervals(genomes[i], genomes[pos], circular, WITHOUTTRIV, sign, comint);
//				dist_mat[i][pos] = dist_mat[pos][i] = comint.size();
//			}else if(conserved){
//				dist_mat[i][pos] = dist_mat[pos][i] = conserved_intervals(genomes[i], genomes[pos], circular, hd).size();
//			}
//		}else{			// nr of reversals
//			if(common){
//				dist_mat[i][pos] = dist_mat[pos][i] =
//					perfect_distance(genomes[i], genomes[pos], hd);
//			}else if(conserved){
//				dist_mat[i][pos] = dist_mat[pos][i] =
//					preserving_reversal_distance(genomes[i], genomes[pos], circular, hd);
//			}else if (bp){
//				dist_mat[i][pos] = dist_mat[pos][i] =
//						dst->calc(genomes[i], genomes[pos]);
//			}else{
//				dist_mat[i][pos] = dist_mat[pos][i] =
//						dst->calc(genomes[i], genomes[pos]);
//			}
//		}
//	}
//}
