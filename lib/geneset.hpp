/*
 * geneset.hpp
 *
 *  Created on: Aug 21, 2010
 *      Author: maze
 */

#ifndef GENESET_HPP_
#define GENESET_HPP_

#include <iterator>
#include <set>
#include <string>
#include <vector>

#include "genom.hpp"
#include "helpers.hpp"
#include "character.hpp"

using namespace std;

class geneset : public character{
private:
	/**
	 * the include exclude map. for elements [1:n]
	 * true: element is included, false: element not included
	 *
	 * for directed 0 and n+1 should be included
	 */
	vector<bool> _ie;

	/**
	 * the set of all elements
	 * necessary for complementing
	 */
	vector<bool> _base;

	/**
	 * the names of the elements in the set
	 **/
	vector<string> *_nmap;
public:

	/**
	 * construct an empty set with NULL name map
	 */
	geneset();

	/**
	 * construct an empty set with a certain name map
	 * the set can store elements {1,...,n}
	 *
	 * complement will give {1,...,n}\setminus current
	 *
	 * @param[in] nmap a pointer to a name map
	 * @param[in] n number of elements (in [1..n]) to be stored in the set
	 */
	geneset( vector<string> *nmap, unsigned n );

	/**
	 * construct an empty set with a certain name map
	 * the set can store elements {1,...,n} \cap base
	 *
	 * complement will give base\setminus current
	 * @param[in] nmap a pointer to a name map
	 * @param[in] n biggest element to be stored in the set
	 * @param[in] base the base set
	 */
	geneset( vector<string> *nmap, unsigned n, const vector<int> &base  );

	/**
	 *
	 */
	vector<bool>::const_iterator begin( ) const;

	/**
	 * remove all elements from the set
	 */
	void clear();

	/**
	 * copy constructur
	 */
	character *clone() const;

	/**
	 * constructur
	 */
	character *create() const;

	/**
	 *
	 */
	void complement();

	/**
	 *
	 */
	vector<bool>::const_iterator end( ) const;

	/**
	 *
	 */
	vector<bool>::const_iterator find( unsigned e ) const;

	/**
	 * get a set representation of the gene set
	 * @return a set of integers
	 */
	set<int> get_set() const;

	/**
	 * insert an element
	 * @param[in] e the element
	 */
	void insert( int e );

	/**
	 * insert the elements in the range a..b in the gene set
	 * the range should contain unsigned integers
	 *
	 * @param[in] __first the begin of the range
	 * @param[in] __last the end of the range
	 */
	template<typename _InputIterator>
	void insert( _InputIterator __first, _InputIterator __last );

	/**
	 * comparison operator
	 * @param[in] g1 a geneset
	 * @param[in] g2 another gene set
	 * @return the result of < for the include exclude maps defining the gene sets
	 */
	friend bool operator< (const geneset &g1, const geneset &g2);

	/***
	 * output operator
	 * @param[in,out] out the stream to write into
	 * @param[in] g the geneset to write
	 * @return the stream
	 */
	friend ostream & operator<<(ostream &out, const geneset &g);

	/**
	 * write a textual representation of the character in the stream
	 * @param[in,out] out the stream
	 */
	virtual void output( ostream &out ) const;

	/**
	 * compare two gene sets
	 * @param[in] b the other gene set
	 * @return true iff equal
	 */
	bool operator ==(const geneset& b) const;

	/**
	 * number of elements in the geneset
	 * @return the number
	 */
	unsigned size() const;
};


class geneset_weight : public character_weight{
private:
	bool _circular;	/** circularity of the genomes */
	bool _directed; /** genome orientation */
	unsigned _n; 	/** length of the genomes */
	SgnHandType _sh;	/** sign handling mode */
	vector<double> _pr;	/** the n-th row of pascals triangle */

public:
	/**
	 * constructor
	 * @param[in] n genome length
	 * @param[in] circular circularity of the genome
	 * @param[in] sh the sign handling mode of the underlying genomes
	 * @param[in] directed directed or undirected genomes
	 */
	geneset_weight( unsigned n, bool circular, SgnHandType sh, bool directed  );

	/**
	 * destructor
	 */
	~geneset_weight();

//	/**
//	 * weight calculating functor
//	 * @param[in] c the character
//	 * @return weight
//	 */
//	double operator()(const character *c ) const;

	/**
	 * weight calculating functor
	 * @param[in] c the character
	 * @return weight
	 */
	double operator()(const character *c ) const;
};


//class geneset : public set<int> {
//private:
//	/**
//	 * the names of the elements in the set
//	 **/
//	vector<string> *_nmap;
//public:
//
//	/**
//	 * construct an empty set with NULL name map
//	 */
//	geneset();
//
//	/**
//	 * construct an empty set with a certain name map
//	 * @param[in] nmap a pointer to a name map
//	 */
//	geneset( vector<string> *nmap );
//
//	/***
//	 * output operator
//	 * @param[in,out] out the stream to write into
//	 * @param[in] g the geneset to write
//	 * @return the stream
//	 */
//	friend ostream & operator<<(ostream &out, const geneset &g);
//};



/**
 * get the consecutive gene sets of one (circular/linear) gene order
 * @param[in] g the gene order
 * @param[in] n set the set of considered genes to [1:n]
 * @param[in] circular the circularity of the gene order
 * @param[in] complement if true: return both of the complementary intervals
 * else: only one is returned (the one which does not contain the first element)
 * @param[in] sgnhnd signhandling mode
 * 	if DOUBLE then ensure that from from the flanking genes only 2i or 2i-1 is included
 * @param[in] maxsize the maximum size of the genesets.
 *  note: for circular genomes the actual size of the returned genesets
 *  might be bigger, but it holds that the smaller of the complementary
 *  pair is <= maxsize
 * @param[in] trivial include sets of size 1 and >= size-1
 * @param[out] __result an iterator to a place to store the binary characters
 */
template<typename _OutputIterator>
void getgenesets( const genom &g, unsigned n, bool circular, bool complement,
		SgnHandType sghnd,
		unsigned maxsize, bool trivial, _OutputIterator __result){

//	cerr << "getgenesets( n "<<n<<", circular: "<<circular<<", complement: "<<complement<<", maxsize: "<<maxsize<<", trivial: "<<trivial<<")"<<endl;
//	cerr << g<<endl;
//	copy( g.chromosom.begin(), g.chromosom.end(), ostream_iterator<int>(cerr, " ") );cerr<<endl;

	geneset tmpset(g.get_nmap(), n, g.chromosom);	// set of genes (for construction)
//	geneset tmpset(NULL, n, g.chromosom);	// set of genes (for construction)
//			all(g.get_nmap(), g.size());	// set of all genes
	bool cmpl = false;

	// if the complementary intervals are to be computed the set of all
	// genes needs to be known
//	if( circular && !complement ){
//		all.insert( g.chromosom.begin(), g.chromosom.end() );
//	}


	for(unsigned j=0; j<g.size(); j++){
		tmpset.clear();
		unsigned k=j;
		while( true ){
//			cout<< j<<" "<<k<<endl;
			if( k >= j+maxsize ){
				break;
			}

			// linear  : k is iterated to the end of the genome
			// circular: k is iterated  until it reaches j again in a
			// 		circular fashion
			if( ( !circular && k>=g.size()) ||
				(  circular && k>=g.size() && j<=k%g.size()) ){
				break;
			}

			// append the next element
//			cout << "insert"<< g[k%g.size()]<<endl;
			tmpset.insert(g[k%g.size()]);
//			cout<< j<<" "<<k<<" " << tmpset<<endl;

			// for the case of circular genomes when only one of the
			// complementary gene sets is to be enumerated we need to replace
			// intervals containing a specific element (the first) by
			// the complement
			if( circular && !complement &&
					tmpset.find(g[0]) != tmpset.end() ){

//				set_difference(all.begin(), all.end(), tmpset.begin(), tmpset.end(),
//						insert_iterator<geneset>(cmpset, cmpset.begin()));
//				cout << "complement "<<tmpset;
//				swap( tmpset, cmpset );
				tmpset.complement();
				cmpl = true;
//				cout << " -> "<<tmpset<<endl;
			}

//			// if signs are handled by doubling the genome, then make sure that only
//			// 2i or 2i-1 are included for the flanking elements
//			if( sghnd == DOUBLE &&
//					( tmpset.find( unsign_other( g[ k%g.size() ] ) ) != tmpset.end() ||
//					  tmpset.find( unsign_other( g[ j%g.size() ] ) ) != tmpset.end() ) ){
//
//			}
//			else


			// sets containing only the elements 2i and 2i-1 are to be excluded
			if( sghnd == DOUBLE && tmpset.size() == 2 &&
					 unsign_other( g[ k%g.size() ] ) == g[ j%g.size()]){

			}
			// sets of size 1 and sets with more than n-2 elements are excluded
			else if( !trivial && ( tmpset.size() <= 1 || tmpset.size() >= g.size()-1 ) ){

			}else{
//				cerr << tmpset<<endl;
			    *__result = tmpset;
			    ++__result;
			}

//			if( trivial || (tmpset.size() > 1 && tmpset.size() < g.size()-1) ){
//
//			}
//			else{
//
//			}

			// re-replacement when we used the complementary interval
			if( circular && !complement && cmpl ){
//				swap( tmpset, cmpset );
//				cmpset.clear();
				tmpset.complement();
				cmpl = false;

			}

			k++;
		}
	}
//	cout << "final"<< endl;
//	for( set<geneset>::const_iterator it = gs.begin(); it!=gs.end(); it++ ){
//		cout << *it << endl;
//	}
//	return gs;
}
#endif /* GENESET_HPP_ */
