#include <iostream>
#include <vector>

#include "common.hpp"
#include "conserved.hpp"
#include "dstnc.hpp"
#include "io.hpp"
#include "genom.hpp"
#include "helpers.hpp"

/**
 * get the distance matrix for some given genomes
 * @param[in] genomes the genomes for whicht the distance matrix is sought
 * @param[in] dst the distance functor
 * @return the distance matrix
 */
vector<vector<unsigned> > distance_matrix(const vector<genom> &genomes,
	dstnc *dst );

/**
 * get the distance matrix for some given genomes, in contrast to the above
 * function memory initialisation has to be done externally
 * @param[in] genomes the genomes for whicht the distance matrix is sought
 * @param[in] dst the distance functor
 * @param[in, out] the distance matrix (has to be initialised externally)
 */
void distance_matrix(const vector<genom> &genomes,
	dstnc *dst, vector<vector<unsigned> > &dist_mat);


/**
 * reset one row & column of a distance matrix to 0
 * @param[in] pos the row/column index
 * @param[in,out] dist_mat the distance matrix
 */
void reset_distance_matrix(unsigned pos, vector<vector<unsigned> > &dist_mat);

/**
 * update one row (and the corresponding row) of a distance matrix
 * @param[in] genomes the genomes for whicht the distance matrix is sought
 * @param[in] pos position to update
 * @param[in] dst the distance functor
 * @param[in,out] the distance matrix
 */
void update_distance_matrix(vector<genom> &genomes, unsigned pos,
	dstnc *dst, vector<vector<unsigned> > &dist_mat);

//void update_distance_matrix(vector<genom> &genomes, unsigned pos,
//	int conserved, int common, int sign,
//	int interval, int bp, int circular, hdata &hd, vector<vector<int> > &dist_mat);
//void distance_matrix(const vector<genom> &genomes,
//	int conserved, int common, int pair_pd, int sign,
//	int interval, int bp, int circular, hdata &hd, vector<vector<int> > &dist_mat);

