/*
 * dstnc_tdl.hpp
 *
 *  Created on: Sep 24, 2010
 *      Author: maze
 */

#ifndef DSTNC_TDL_HPP_
#define DSTNC_TDL_HPP_

#include "dstnc.hpp"
#include "genom.hpp"

class dstnc_tdl : public dstnc {
private:
public:
	/**
	 * constructor: does nothing
	 */
	dstnc_tdl();

	/**
	 * destructor: does nothing
	 */
	virtual ~dstnc_tdl();

	/**
	 * adapt the distance calculator to a new genome length
	 * @param[in] n the new length
	 */
	virtual void adapt( unsigned n );
	/**
	 * get the distance from src to tgt
	 * @param[in] src the source genome
	 * @param[in] tgt the target genome
	 * return the distance
	 */
	virtual unsigned calc( const genom &src, const genom &tgt );
	/**
	 * return a pointer to a copy of a dstnc
	 * this is a virtual copy constructor
	 * @return the pointer
	 */
	virtual dstnc* clone() const;

	/**
	 * the output function
	 * @param[in] os the stream to write into
	 * @return the stream
	 */
	virtual ostream & output(ostream &os) const;
};


#endif /* DSTNC_TDL_HPP_ */
