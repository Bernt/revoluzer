/*
 * bpdist.hpp
 * @author: M. Bernt
 */

#ifndef BPDIST_HPP_
#define BPDIST_HPP_

#include <iostream>

#include "dstnc.hpp"


/**
 * class for computing the breakpoint distance
 * allows for deletions
 */
class dstnc_bp : public dstnc {
private:
	unsigned _n;	/* number of elements in complete genomes */
	bool _circular,	/* circularity of the genomes */
		_directed;	/* linear directed? */
	vector<int> _ssuc, /*	the successor of each gene in src */
		_tsuc;		/* the successor of each gene in  tgt */
public:

	/**
	 * constructor
	 * @param[in] n number of elements in complete genomes
	 * @param[in] circular circularity of the genomes
	 * @param[in] directed directed linear genome?
	 */
	dstnc_bp( unsigned n, bool circular, bool directed );

	/**
	 * destructor
	 */
	virtual ~dstnc_bp();

	/**
	 * adapt the breakpoint distance calculator to a new genome length
	 * @param[in] n the new length
	 */
	void adapt(unsigned n);

	/**
	 * get the breakpoint distance between two gene orders
	 * @param[in] src a genome
	 * @param[in] tgt another genome
	 * @param[in] context @see distance::calc
	 * @return the reversal distance
	 */
	unsigned calc( const genom &src, const genom &tgt );

	/**
	 * return a pointer to a copy of a dstnc
	 * this is a virtual copy constructor
	 * @return the pointer
	 */
	dstnc* clone() const;

//	/**
//	 * virtual constructor
//	 * @return an empty instance of an object
//	 */
//	dstnc* create() const;

	/**
	 * output function
	 * @param[in] stream to write into
	 * @return the stream
	 */
	ostream & output(ostream &os) const;
};

#endif /* BPDIST_HPP_ */
