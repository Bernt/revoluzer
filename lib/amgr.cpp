#include <algorithm>
#include <limits>
#include <map>

#include "amgr.hpp"
#include "bintree.hpp"
#include "caprara.hpp"
#include "distmat.hpp"

//#define DEBUG_AMGR

	// use the median problem cache
#define CACHED

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void amgr(vector<genom> &genomes, int m, vector<string> &names,
		mediansolver *solver,
		int all_triples, int all_medians, int all_medians_deepest,
		vector<int> pick, vector<int> pick_level, int combine,
		int random, int greedy, double greedyness,
		vector<vector<unsigned> > &dist_mat, vector<pair<unsigned, unsigned> > &edges,
		vector<bool> &included_genomes,
		int &tree_score, int &best_tree_score, int lb,
		int bounding, vector<int> &partial_bound,
		int depth, vector<pair<unsigned, unsigned> > &progress,
		vector<unsigned> &choices,
		map<vector<pair<unsigned, unsigned> >, vector< vector<genom> >  > &solutions,
		int &solution_cnt, int verbose){

	//~ bintree *bt;					// a binary tree for the solutions
	int best_triple_score = std::numeric_limits< int >::max(),	// score of the best triple found in the candidates
		triple_score = 0,			// the resulting score of a run of capraras
		triple_lbound = NO_BOUND,	// lower and upper bound for capraras ms
		triple_ubound = NO_BOUND,
		mediancnt = 0;				// number of found solutions
	map<vector<genom>, int >::iterator cache_it;
	pair<unsigned, unsigned> split_edge;
	vector<pair<int, vector<int> > > triple_candidate, 	// the candidates for triples to check
		triple;			// the triples to check
	vector<genom> t(3),	// a triple of genomes for input to capraras
		medians;		// output of capraras: the medians
	vector<vector<genom> > triple_medians;	// the medians of the triples which have to be checked

		// if the best known tree is better than a given lower bound -> abort here
		// (only usefull for testing)
	if(best_tree_score <= lb){
		return;
	}

		// update the level bounds
	if(bounding == 2){
		if(partial_bound[depth] > tree_score)
			partial_bound[depth] = tree_score;
	}

//	cout <<  tree_score<< " - "<<lower_bound(genomes, m, dist_mat, included_genomes, tree_score)<<" - "<<best_tree_score<<endl;
//	if( lower_bound(genomes, m, dist_mat, included_genomes, tree_score) > best_tree_score ){
//		cout << "lb "<< depth <<endl;
//		return;
//	}

	//~ if(solution_cnt > 100000)
		//~ return;

	if(verbose){
		print_progress(progress, tree_score, best_tree_score, solution_cnt);
	}
		// get the triple candidates, i.e.:
		// - on the first level: all possible triples
		// - on all other levels: all triples consisting of 2 connected (which have to be incident
		//		to the same edge) and 1 unconnected node such that the difference of half
		// 		circumfence minus the length of the split edge is minimal
		// 		they are returned together with their lower bound
	triple_candidate = triples(genomes, m, dist_mat, edges, included_genomes);

		// if no triple is found, then there is  nothing more to add, i.e. completed a tree
	if(triple_candidate.size() == 0){
		//~ cout <<tree_score <<" "<<endl;

		if (tree_score <= best_tree_score){
			vector<pair<unsigned, unsigned> > sol_edges = edges;
			sort(sol_edges.begin(), sol_edges.end());

			//~ bt = init(sol_edges, genomes, names);
			//~ cout << tree_score<<" "<<endl;
			//~ for(unsigned i=0; i<choices.size(); i++){
				//~ cout << choices[i]<<" ";
			//~ }
			//~ cout << newick(bt, 0, 0, 1) << endl;
			//~ free_tree(bt);

			if(tree_score < best_tree_score){
				best_tree_score = tree_score;
				solutions.clear();
				solution_cnt = 0;
				if(verbose)
					cerr << endl;
			}

			if(solutions.find(sol_edges) == solutions.end()){
				solutions[sol_edges].push_back(genomes);
				solution_cnt ++;
				//~ cout <<"# " <<tree_score <<" "<<endl;
//~ #ifdef DEBUG_AMGR
				//~ for(unsigned i=0; i<sol_edges.size(); i++)
					//~ cout << sol_edges[i].first <<"," <<sol_edges[i].second<<" ";
				//~ cout <<endl;
//~ #endif//DEBUG_AMGR
				//~ cout << "# ";
				//~ bt = init(sol_edges, genomes, names);
				//~ print_newick(bt, 1, 0); cout <<endl;
				//~ free_tree(bt);
			}
		}
	}
		// there is something to add, so just do it
	else{
#ifdef DEBUG_AMGR
		for(unsigned i=0; i<genomes.size(); i++){
			cout << included_genomes[i];
		}
		cout << endl;
		cout << "triple candidates "<<endl;
		for(unsigned i=0; i<triple_candidate.size(); i++){
			cout << "lb "<< triple_candidate[i].first <<" ("<< triple_candidate[i].second[0]<<","<<triple_candidate[i].second[1]<<","<<triple_candidate[i].second[2]<<") e("<< triple_candidate[i].second[3]<<")"<<endl;
		}
		cout << endl;
#endif//DEBG_AMGR

			// get the best of the candidate triples. therefore their real score has to be computed
			// i.e. the median problem has to be solved.
			// - the candidates are sorted by their lower  bound, so we can abort the loop if
			// 	the best known score for a triple is better than the lowerbound of the current
			// 	triple (not in the ungreedy mode)
			// - only one median is computed even if all medians should be used, all medians
			// 	are computed later, when the best triples are known (until now there are
			// 	just lower bounds)
			// - to speed this up the best known score is given to the median solver as upper
			// 	bound;
			// - for more speed up the median problems are cached - so if the current triple
			// 	is in the "cache" take the precomputed value, else compute the median
			// 	and add it to the cache
		for(unsigned i=0; i<triple_candidate.size(); i++){
				// some resets
			triple_score = 0;
			medians.clear();
			mediancnt = 0;

				// if greedy: if the lowerbound for the current triple is bigger than the
				// best known solution -> abort
			if( greedy != 0 && best_triple_score != std::numeric_limits< int >::max() && (int)round(best_triple_score * greedyness) < triple_candidate[i].first) {
				break;
			}

				// if the treescore plus the lowerbound is greater than the tree score bound
				// then this path needn't be followed. because triples after the current are
				// greater these have not to be considered -> abort
			if(bounding > 0){
				if(bounding == 2 && ( (tree_score + triple_candidate[i].first) > partial_bound[depth+1] )){
					break;
				}else if(bounding == 1 && ( (tree_score + triple_candidate[i].first) > best_tree_score)){
					break;
				}
			}

				// init the input triple
			t[0] = genomes[ triple_candidate[i].second[0] ];
			t[1] = genomes[ triple_candidate[i].second[1] ];
			t[2] = genomes[ triple_candidate[i].second[2] ];

				// compute the median problem consisting of triple t0,t1,t2
				// if not greedy (exhaustive) -> no bounding, i.e. leave it NO_BOUND
				// 	because we want all solutions for all the triples
				// else
				// 	the upper bound for capraras is the best known solution (because the
				// 	best score is minus the corresponding edge, the edge has to be added here),
				// 	and if no solution is known -> no bound
			if(greedy != 0 && best_triple_score > 0 && best_triple_score < std::numeric_limits< int >::max()){
				triple_ubound = (int)round(best_triple_score * greedyness);
				if(triple_candidate[i].second[3] != -1)
					triple_ubound += dist_mat[triple_candidate[i].second[1]][triple_candidate[i].second[2]];
			}

				// get median and the scores (dont get medians worser than the best known
				// solution -> upper bound, only get one median while getting the best triples)
			solver->solve(t, false, mediancnt, NO_BOUND, triple_ubound, triple_score, medians);
//			if (use_tcip != 0){
//				tcip(t, ONE_MEDIAN, mediancnt, triple_score, medians, NO_BOUND, 0, 0);
//			}else{
//				caprara(t, DYN_CONSTRAINTS,
//					conserved, common, pair_pd, sign, circular, NO_BOUND, triple_ubound,
//					ONE_MEDIAN, NOTLAZY, 0, mediancnt, triple_score, medians);
//			}

				// remove the split edge from the score
			if(triple_candidate[i].second[3] != -1)
				triple_score -= dist_mat[triple_candidate[i].second[1]][triple_candidate[i].second[2]];

				// bound against the now known score
			if(bounding > 0){
				if(bounding == 2 && ( (tree_score + triple_score) > partial_bound[depth+1] )){
					continue;
				}
				else if(bounding == 1 && ((tree_score + triple_score) > best_tree_score))
					continue;
			}

			triple_candidate[i].first = triple_score;
				// check if the medians are better or as good as the medians for the other triples
			if( greedy == 0 || best_triple_score == std::numeric_limits< int >::max() || triple_score <= (int)round(best_triple_score * greedyness)){

				if( greedy != 0 && triple_score < best_triple_score){
					best_triple_score = triple_score;
					if(greedy != 0){
						vector<pair<int, vector<int> > > new_triple;
						vector<vector<genom> > new_triple_medians;
						for(unsigned k=0; k<triple.size(); k++){
							if ( triple_candidate[i].first <= (int)round(best_triple_score * greedyness) ){
								new_triple.push_back( triple[k] );
								new_triple_medians.push_back( triple_medians[k] );
							}
						}
						triple.clear();
						triple_medians.clear();
						triple = new_triple;
						triple_medians = new_triple_medians;
					}else{
						triple.clear();
						triple_medians.clear();
					}
				}

				triple.push_back( triple_candidate[i] );
				triple_medians.push_back( medians );
			}
		}

			// if all medians are wanted -> compute them now, and provide the known
			// score as bound (upper as well as lower)
		if(all_medians != 0 && ( (depth < m-3) || all_medians_deepest == 1) ){
			for(unsigned i=0; (all_triples == 0 && i<1) || ((greedy == 0 || all_triples == 1) && i<triple.size()); i++){
				t[0] = genomes[ triple[i].second[0] ];
				t[1] = genomes[ triple[i].second[1] ];
				t[2] = genomes[ triple[i].second[2] ];

				triple_medians[i].clear();

				triple_lbound = triple_ubound = triple[i].first;
				if(triple[i].second[3] != -1){
					triple_ubound += dist_mat[triple[i].second[1]][triple[i].second[2]];
					triple_lbound = triple_ubound;
				}

					// compute all medians
				solver->solve( t, all_medians, triple_lbound, triple_ubound, mediancnt, triple_score, medians );
//				if(tcip != 0){
//					tcip(t, all_medians, mediancnt, triple_score, medians, NO_BOUND, 0, 0);
//				}else{
//					caprara(t, DYN_CONSTRAINTS,
//						conserved, common, pair_pd, sign, circular, triple_lbound,
//						triple_ubound, all_medians, NOTLAZY, 0,
//						mediancnt, triple_score, medians);
//				}
				//~ cout << "AM "<<depth<<" ";

				triple_medians[i] = medians;
			}
		}
			// if the working order should be random
			// -> randomize the order of the triples together with their medians
			//    and randomise the medians of each triple
		if(random != 0){
			vector<unsigned> shuffle;
			vector<vector<genom> > tmp_triple_medians;
			vector<pair<int, vector<int> > > tmp_triple;

			shuffle = vector<unsigned>(triple.size());
			tmp_triple = vector<pair<int, vector<int> > >(triple.size());
			tmp_triple_medians = vector<vector<genom> >(triple.size());

			for(unsigned i=0; i<shuffle.size(); i++){
				shuffle[i] = i;
			}
			random_shuffle(shuffle.begin(), shuffle.end());
			for(unsigned i=0; i<shuffle.size(); i++){
				tmp_triple[i] = triple[ shuffle[i] ];
				tmp_triple_medians[i] = triple_medians[ shuffle[i] ];
			}
			triple = tmp_triple;
			triple_medians = tmp_triple_medians;
			tmp_triple.clear();
			tmp_triple_medians.clear();

			for(unsigned i=0; i<triple_medians.size(); i++)
				random_shuffle(triple_medians[i].begin(), triple_medians[i].end());
		}

			// apply the filters if there are some specified
		if(pick.size() > 0){
			filter_medians(triple, triple_medians, genomes, included_genomes,
				m, pick, pick_level, combine,
				solver->get_dstncfoo(), dist_mat, edges);
		}

#ifdef DEBUG_AMGR
		cout << "triple "<<triple.size()<<" "<<triple_medians.size()<<endl;
		for(unsigned i=0; (all_triples == 0 && i<1) || ((greedy==0 || all_triples == 1) && i<triple.size()); i++){
			cout <<"score "<< triple[i].first<< " "<<triple_medians[i].size() <<"x (";
			copy(triple[i].second.begin(), triple[i].second.end(), ostream_iterator<int>(cout, " ") );
			cout << endl;
		}
		cout << endl;
#endif//DEBUG_AMGR

			// update progress information
		progress[depth].first = 0;
		for(unsigned i=0; i<triple.size(); i++){
			progress[depth].first += triple_medians[i].size();
		}
		progress[depth].second = progress[depth].first;

			// now: branch over the remaining triples
		for(unsigned i=0; i<triple.size(); i++){
				// the split edge has to be removed and 3 new edges have to be added;
				// to save some costs the split edge is replaced by on of the three (only
				// if this is not the initialisation step, because there is no split edge)
			if(triple[i].second[3] != -1){
				split_edge = edges[ triple[i].second[3] ];	// save the split edge
				edges[ triple[i].second[3] ].first = triple[i].second[0]; // replace it
				edges[ triple[i].second[3] ].second = genomes.size();
			}else{
				edges.push_back( pair<unsigned, unsigned>(triple[i].second[0], genomes.size()) );
			}

			edges.push_back( pair<unsigned, unsigned>(triple[i].second[1], genomes.size()) );
			edges.push_back( pair<unsigned, unsigned>(triple[i].second[2], genomes.size()) );

				// mark the added genomes (the new one and the median)
				// if there is no edge so far the other two genomes have to be marked also
			included_genomes[triple[i].second[0]] = true;

			if(triple[i].second[3] == -1){
				included_genomes[triple[i].second[1]] = true;
				included_genomes[triple[i].second[2]] = true;
			}
			included_genomes[genomes.size()] = true;

			tree_score += triple[i].first;
			choices[depth] = triple[i].first;
			for(unsigned j=0; j<triple_medians[i].size(); j++){
				genomes.push_back( triple_medians[i][j] );

				update_distance_matrix(genomes, genomes.size()-1, solver->get_dstncfoo(), dist_mat);
				amgr(genomes, m, names, solver,
//					conserved, common, pair_pd, sign,
					all_triples, all_medians, all_medians_deepest,
					pick, pick_level, combine, random, greedy, greedyness,
					dist_mat, edges, included_genomes,
//					median_cache, median_cache_score, median_cache_medians, median_cache_allmeds,
					tree_score, best_tree_score, lb,
					bounding, partial_bound, depth+1, progress,
					choices, solutions, solution_cnt, verbose);

				progress[depth].first--;
				genomes.pop_back();

			}

			reset_distance_matrix(genomes.size(), dist_mat);
			tree_score -= triple[i].first;
				// unmark the added genome
			included_genomes[ triple[i].second[0] ] = false;
			if(triple[i].second[3] == -1){
				included_genomes[triple[i].second[1]] = false;
				included_genomes[triple[i].second[2]] = false;
			}
			included_genomes[genomes.size()] = false;
				// restore the edges
			if(triple[i].second[3] != -1){
				edges[ triple[i].second[3] ] = split_edge;
			}else{
				edges.pop_back();
			}
			edges.pop_back();
			edges.pop_back();

			if((greedy && all_triples == 0)){
				break;
			}
		}
	}

	return;

}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void filter_medians(const vector<pair<int, vector<int> > > &triple,
		vector<vector<genom> > &medians, vector<genom> &genomes,
		vector<bool> &included_genomes, int m, vector<int> mode,
		vector<int> p,
		int combine,
		dstnc *dst,
		vector<vector<unsigned> > &dist_mat, vector<pair<unsigned, unsigned> > &edges){

	int minmax = 0;
	vector<vector<genom> > filtered(medians.size());
	vector<vector<int> > criterium(medians.size());
	vector<pair<int, vector<int> > > triple_candidate;
	vector< vector<vector<genom> > > pre_filtered(mode.size(), vector<vector<genom> >(medians.size()));

		// select p random medians
	if(mode.size() == 1 && mode[0] == SELECT_RANDOM){
		vector<pair<unsigned, unsigned> > allmeds;

		for(unsigned i=0; i<medians.size(); i++){
			for(unsigned j=0; j<medians[i].size(); j++){
				allmeds.push_back(pair<unsigned, unsigned>(i,j));
			}
		}
		random_shuffle(allmeds.begin(), allmeds.end());

		for(unsigned i=0; i< (unsigned)p[0] && i<allmeds.size(); i++){
			pre_filtered[0][ allmeds[i].first].push_back( medians[allmeds[i].first][allmeds[i].second] );
		}
	}else if(mode.size() == 1 && mode[0] == SELECT_RANDOM_PC){
		for(unsigned i=0; i<medians.size(); i++){

			vector<int> component_idx(medians[i].size(), -1);
			vector<vector<genom> >component;
			int next_comp = 0,
				b, s;

			if(medians[i].size() < 2){
				pre_filtered[0][i] = medians[i];
				continue;
			}
				// get the components
			for(unsigned j=0; j<medians[i].size(); j++){
				for(unsigned k=j+1; k<medians[i].size(); k++){
					if( dst->calc(medians[i][j], medians[i][k]) == 1){
						if( component_idx[j] == -1 && component_idx[k] == -1 ){
							component_idx[j] = component_idx[k] = next_comp;
							next_comp++;
						}
						else if( component_idx[j] > -1 && component_idx[k] == -1){
							component_idx[k] = component_idx[j];
						}else if( component_idx[k] > -1 && component_idx[j] == -1){
							component_idx[j] = component_idx[k];
						}else if( component_idx[j] > -1 && component_idx[k] > -1 && component_idx[j] != component_idx[k]){
							if(component_idx[j] > component_idx[k]){
								b = component_idx[j];
								s = component_idx[k];
							}else{
								b = component_idx[k];
								s = component_idx[j];
							}
							for(unsigned l=0; l<component_idx.size(); l++){
								if(component_idx[l] == b)
									component_idx[l] = s;
								if(component_idx[l] > b)
									component_idx[l]--;
							}
							next_comp--;
						}
					}
				}
			}

			component = vector<vector<genom> >(next_comp);
			for(unsigned j=0; j<component_idx.size(); j++){
				if(component_idx[j] > -1){
					component[ component_idx[j] ].push_back(medians[i][j]);
				}else{
					component.push_back( vector<genom>() );
					component.back().push_back(medians[i][j]);
				}
			}

			for(unsigned j=0; j<component.size(); j++){
				random_shuffle(component[j].begin(), component[j].end());

				for(unsigned k=0; k< (unsigned)p[0] && k<component[j].size(); k++){
					pre_filtered[0][ i ].push_back( component[j][k] );
				}
			}
		}
	}
	else{
		for(unsigned h=0; h<mode.size(); h++){
			if( abs(mode[h])==abs(SELECT_CENTRAL) || abs(mode[h])==abs(SELECT_NEAREST)
				|| abs(mode[h])==abs(SELECT_MIN_SKEW) || abs(mode[h])==abs(SELECT_MAX_LONGESTEDGE)
				|| abs(mode[h])==abs(SELECT_MAX_SHORTESTEDGE) ){

				if(mode[h] == SELECT_NEAREST || mode[h] == SELECT_MIN_SKEW
					|| mode[h] == SELECT_CENTRAL || mode[h] == SELECT_MIN_LONGESTEDGE
					|| mode[h] == SELECT_MIN_SHORTESTEDGE)
					minmax = std::numeric_limits< int >::max();
				else
					minmax = 0;

				for(unsigned i=0; i<medians.size(); i++){
					criterium[i] = vector<int>(medians[i].size(), 0);

					if(mode[h] == SELECT_CENTRAL){
						minmax = std::numeric_limits< int >::max();
					}else if(mode[h] == SELECT_DECENTRAL){
						minmax = 0;
					}

					for(unsigned j=0; j<medians[i].size(); j++){
						if(mode[h] == SELECT_NEAREST || mode[h] == SELECT_FAREST){
							for(unsigned k=0; k<genomes.size() && k<(unsigned)m; k++){
								if(!included_genomes[k])
									criterium[i][j] += dst->calc(medians[i][j], genomes[k]);
							}
						}else if(mode[h] == SELECT_CENTRAL || mode[h] == SELECT_DECENTRAL){
							for(unsigned k=0; k<medians[i].size(); k++){
								if(k != j)
									criterium[i][j] += dst->calc( medians[i][j], medians[i][k] );
							}
						}else if(mode[h] == SELECT_MIN_SKEW || mode[h] == SELECT_MAX_SKEW){
							int d, min = std::numeric_limits< int >::max(), max = 0;
							for(unsigned k=0; k<triple[i].second.size()-1; k++){
								d = dst->calc( medians[i][j], genomes[ triple[i].second[k] ]);
								if(min > d)
									min = d;
								if(max < d)
									max = d;
							}
							criterium[i][j] = max - min;
						}
						else if(mode[h] == SELECT_MAX_LONGESTEDGE || SELECT_MIN_LONGESTEDGE ||
							mode[h] == SELECT_MAX_SHORTESTEDGE || SELECT_MIN_SHORTESTEDGE){
							int d = 0, mM=0;
							if(mode[h] == SELECT_MAX_LONGESTEDGE || mode[h] == SELECT_MIN_LONGESTEDGE)
								mM = 0;
							else
								mM = std::numeric_limits< int >::max();
							for(unsigned k=0; k<triple[i].second.size()-1; k++){
								d = dst->calc( medians[i][j], genomes[ triple[i].second[k] ]);
								if(d > mM && (mode[h] == SELECT_MAX_LONGESTEDGE || mode[h] == SELECT_MIN_LONGESTEDGE))
									mM = d;
								if(d < mM && (mode[h] == SELECT_MAX_SHORTESTEDGE || mode[h] == SELECT_MIN_SHORTESTEDGE))
									mM = d;
							}

							criterium[i][j] = mM;

						}


						if(mode[h] == SELECT_NEAREST || mode[h] == SELECT_MIN_SKEW || mode[h] == SELECT_CENTRAL ||
							mode[h] == SELECT_MIN_LONGESTEDGE || mode[h] == SELECT_MIN_SHORTESTEDGE){

							if(criterium[i][j] < minmax)
								minmax = criterium[i][j];
						}else{
							if(criterium[i][j] > minmax)
								minmax = criterium[i][j];
						}
					}

					if(mode[h] == SELECT_CENTRAL || mode[h] == SELECT_DECENTRAL){
						if(mode[h] == SELECT_CENTRAL)
							minmax += (int) round( minmax*p[h]/100.0 );
						else
							minmax -= (int) round( minmax*p[h]/100.0 );

						for(unsigned j=0; j<medians[i].size(); j++){
							if((mode[h] == SELECT_CENTRAL && criterium[i][j] <= minmax) ||
								(mode[h] == SELECT_DECENTRAL && criterium[i][j] >= minmax)){
								pre_filtered[h][i].push_back(medians[i][j]);
							}
						}
					}
				}
			}else if(mode[h] == SELECT_LOOKFORWARD){
				minmax = std::numeric_limits< int >::max();
				for(unsigned i=0; i<triple.size(); i++){
					criterium[i] = vector<int>(medians[i].size(), std::numeric_limits< int >::max());

					included_genomes[triple[i].second[0]] = true;
					if(triple[i].second[3] == -1){
						included_genomes[triple[i].second[1]] = true;
						included_genomes[triple[i].second[2]] = true;
					}
					included_genomes[genomes.size()] = true;

					for(unsigned j=0; j<medians[i].size(); j++){
						genomes.push_back(medians[i][j]);

						update_distance_matrix(genomes, genomes.size()-1, dst, dist_mat);

						triple_candidate = triples(genomes, m, dist_mat, edges, included_genomes);
						if(triple_candidate.size()>0)
							criterium[i][j] = triple_candidate[0].first;
						if(criterium[i][j] < minmax)
							minmax = criterium[i][j];
						genomes.pop_back();
					}
					reset_distance_matrix(genomes.size(), dist_mat);
					included_genomes[ triple[i].second[0] ] = false;
					if(triple[i].second[3] == -1){
						included_genomes[triple[i].second[1]] = false;
						included_genomes[triple[i].second[2]] = false;
					}
					included_genomes[genomes.size()] = false;
				}
			}

			if(abs(mode[h]) == abs(SELECT_NEAREST) || abs(mode[h]) == abs(SELECT_MIN_SKEW)
				|| abs(mode[h])==abs(SELECT_MAX_LONGESTEDGE) || abs(mode[h])==abs(SELECT_MAX_SHORTESTEDGE) ||
				mode[h] == SELECT_LOOKFORWARD){

				if(mode[h] == SELECT_NEAREST || mode[h] == SELECT_MIN_SKEW || mode[h] == SELECT_MIN_LONGESTEDGE ||
					mode[h] == SELECT_MIN_SHORTESTEDGE||mode[h]==SELECT_LOOKFORWARD)
					minmax += (int) round( minmax*p[h]/100.0 );
				else
					minmax -= (int) round( minmax*p[h]/100.0 );

				for(unsigned i=0; i<medians.size(); i++){
					for(unsigned j=0; j<medians[i].size(); j++){

						if((criterium[i][j] <= minmax&&(mode[h] == SELECT_NEAREST|| mode[h] == SELECT_MIN_SKEW||mode[h]==SELECT_MIN_LONGESTEDGE||mode[h]==SELECT_MIN_SHORTESTEDGE||mode[h]==SELECT_LOOKFORWARD) ) ||
							(criterium[i][j] >= minmax&&(mode[h] == SELECT_FAREST|| mode[h] == SELECT_MAX_SKEW||mode[h]==SELECT_MAX_LONGESTEDGE||mode[h]==SELECT_MAX_SHORTESTEDGE) )){

							pre_filtered[h][i].push_back(medians[i][j]);
						}
					}
				}
			}
		}
	}

		// combine the results of the single filters
	for(unsigned i=1; i<pre_filtered.size(); i++){
		for(unsigned j=0; j<pre_filtered[i].size(); j++){
			vector<genom> tmp;
			sort(pre_filtered[i][j].begin(), pre_filtered[i][j].end());
			sort(pre_filtered[0][j].begin(), pre_filtered[0][j].end());
			if( combine == COMBINE_UNION ){
				set_union(pre_filtered[i][j].begin(), pre_filtered[i][j].end(),
					pre_filtered[0][j].begin(), pre_filtered[0][j].end(),
					back_inserter(tmp));
			}else if( combine == COMBINE_INTERSECTION ){
				set_intersection(pre_filtered[i][j].begin(), pre_filtered[i][j].end(),
					pre_filtered[0][j].begin(), pre_filtered[0][j].end(),
					back_inserter(tmp));
			}else{
				cout << "unknown combination mode"<<endl;
				exit(1);
			}
			pre_filtered[0][j] = tmp;
			tmp.clear();
		}
	}
	filtered = pre_filtered[0];
	medians = filtered;
	filtered.clear();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int lower_bound(vector<genom> &genomes, unsigned m,
	vector<vector<int> > &dist_mat, vector<bool> &included_genomes,
	int &tree_score){

	int k=0,
		lb;
	static vector<pair<int, pair<int, int> > > d;	// mapping distances of a pair of genomes to the pair
	static vector<bool> included;
	static bool initialised = false;

	if(initialised == false){
		included = vector<bool>(2 * m, false);
		d = vector<pair<int, pair<int, int> > >((2*m)*((2*m)-1)/2);
		initialised = true;
	}

	for(unsigned i=0; i<genomes.size(); i++){
			included[i] = included_genomes[i];
	}

	cout << "included "<<endl;
	for(unsigned i=0; i<genomes.size(); i++){
		cout << (int)included[i]<<" " ;
	}cout <<endl;

	for(unsigned i=0; i<genomes.size(); i++){
		for(unsigned j=i+1; j<genomes.size(); j++){
			d[k].first = dist_mat[i][j];
			d[k].second.first = i;
			d[k].second.second = j;
			k++;
		}
	}

	sort(d.begin(), d.end());

	lb = tree_score;
	for(unsigned i=0; i<d.size(); i++){
//		cout << d[i].first <<" ("<<d[i].second.first<<","<<d[i].second.second <<")";
		if(included[d[i].second.first] == false || included[d[i].second.second] == false){
			included[d[i].second.first] = true;
			included[d[i].second.second] = true;
			lb += d[i].first;
//			cout << " y"<<endl;
		}
//		else
//			cout << " n"<<endl;
	}

	return lb;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

vector<pair<int, vector<int> > > triples(
	vector<genom> &genomes, unsigned m, const vector<vector<unsigned> > &dist_mat,
	vector<pair<unsigned, unsigned> > &edges, vector<bool> &included_genomes){

	int bound;
	pair<int, vector<int> > t(0, vector<int>(4,-1) );
	vector<pair<int, vector<int> > > triple;

	if(edges.size() == 0){
		for(unsigned i=0; i<genomes.size() && i<m; i++){
			for(unsigned j=i+1; j<genomes.size(); j++){
				for(unsigned k=j+1; k<genomes.size(); k++){

					bound = (int) ceil( ( dist_mat[ i ][ j ]
						+ dist_mat[ j ][ k ]
						+ dist_mat[ k ][ i ] ) / 2.0 );


					t.first = bound;
					t.second[0] = i;
					t.second[1] = j;
					t.second[2] = k;
					// t.second[3] = -1; -> already by initialisation
					triple.push_back( t );
				}
			}
		}
	}else{
			// compute for each triple (one unconnected genome and an edge from the partial tree)
			// the lowerbound of the score = d(triple) - d(edge)
		for(unsigned i=0; i<genomes.size() && i<m; i++){
			if(included_genomes[i])
				continue;

			for(unsigned j=0; j<edges.size(); j++){

				bound = ( (int) ceil( ( dist_mat[ i ][ edges[j].first ]
					+ dist_mat[ edges[j].first ][ edges[j].second ]
					+ dist_mat[ edges[j].second ][ i ] ) / 2.0 ) )
					- dist_mat[ edges[j].first ][ edges[j].second ];

				t.first = bound;
				t.second[0] = i;
				t.second[1] = edges[j].first;
				t.second[2] = edges[j].second;
				t.second[3] = j;

				triple.push_back(t);
			}
		}
	}
		// sort the triples by the bound
	sort(triple.begin(), triple.end());

	return triple;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void print_progress(vector<pair<unsigned, unsigned> > &progress, int tree_score,
	int best_tree_score, int solution_cnt){

	static unsigned cnt = 0;
	static unsigned every = progress.size()/30;

	cnt++;
	if(every<1)
		every = 1;
		// if(cnt % 100 == 1 || cnt < progress.size()){
			cerr << "\r";
			for(unsigned i=0; i<progress.size(); i++){
//				if(i%every == 0 || i==progress.size()-1)
					cerr << progress[i].first<<"/"<<progress[i].second<<" ";
			}if(best_tree_score != std::numeric_limits< int >::max())
				cerr <<"-> "<< solution_cnt<<" x "<< best_tree_score<<"      ";
			else
				cerr << tree_score;

	//~ }
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//~ vector<pair<int, pair<unsigned, unsigned> > > nearest_nodes(
		//~ vector<genom> &genomes,
		//~ unsigned m, const vector<vector<int> > &dist_mat, int mode,
		//~ vector<pair<unsigned, unsigned> > &edges,
		//~ vector<bool> &included_genomes){

	//~ int bound;
		//~ // the nearest nodes .. a bound for each pair of genome and edge
	//~ vector<pair<int, pair<unsigned, unsigned> > > nn;

		//~ // compute for each triple (one unconnected genome and an edge from the partial tree)
		//~ // the lowerbound of the score = d(triple) - d(edge)
	//~ for(unsigned i=0; i<genomes.size() && i<m; i++){
		//~ if(included_genomes[i])
			//~ continue;

		//~ for(unsigned j=0; j<edges.size(); j++){
			//~ if(mode == SELECT_SMALLEST){
				//~ bound = ( (int) ceil( ( dist_mat[ i ][ edges[j].first ]
					//~ + dist_mat[ edges[j].first ][ edges[j].second ]
					//~ + dist_mat[ edges[j].second ][ i ] ) / 2.0 ) )
					//~ - dist_mat[ edges[j].first ][ edges[j].second ];
			//~ }else if(mode == SELECT_BIGGEST){
				//~ bound = min(dist_mat[ i ][ edges[j].first ],  dist_mat[ edges[j].first ][ edges[j].second ]);
				//~ bound = min(bound, dist_mat[ edges[j].second ][ i ]);
				//~ bound -= - dist_mat[ edges[j].first ][ edges[j].second ];
			//~ }
			//~ nn.push_back( pair<int, pair<unsigned, unsigned> >(bound, pair<unsigned, unsigned>(i,j) ) );
		//~ }
	//~ }

	//~ sort(nn.begin(), nn.end());

	//~ if(mode == SELECT_BIGGEST)
		//~ reverse(nn.begin(), nn.end());

	//~ return nn;
//~ }

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//~ vector<pair<unsigned, unsigned> > smallest_pairs(const vector<vector<int> > &dist_mat,
	//~ int mode){

	//~ int m = std::numeric_limits< int >::max();						// min/max value (>0) in the distance matrix
	//~ vector<pair<unsigned, unsigned> > sps;	// smallest pairs

	//~ if(mode == SELECT_SMALLEST)
		//~ m = std::numeric_limits< int >::max();
	//~ else if(mode == SELECT_BIGGEST)
		//~ m = 0;

	//~ for(unsigned i=0; i<dist_mat.size(); i++){
		//~ for(unsigned j=i+1; j<dist_mat.size(); j++){
			//~ if(dist_mat[i][j] > 0 &&
				//~ ( (mode == SELECT_SMALLEST && dist_mat[i][j] <= m ) ||
				//~ ( mode == SELECT_BIGGEST && dist_mat[i][j] >= m ) ) ){

				//~ if(( mode == SELECT_SMALLEST && dist_mat[i][j] < m ) ||
					//~ (mode == SELECT_BIGGEST && dist_mat[i][j] > m) ){
					//~ sps.clear();
					//~ m = dist_mat[i][j];
				//~ }
				//~ sps.push_back( pair<unsigned, unsigned>(i,j) );
			//~ }
			//if(dist_mat[i][j] > 0)
			//	sps.push_back( pair<unsigned, unsigned>(i,j) );
		//~ }
	//~ }

	//~ return sps;
//~ }
