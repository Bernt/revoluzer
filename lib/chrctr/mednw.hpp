/*
 * mednw.hpp
 *
 *  Created on: Sep 27, 2010
 *      Author: maze
 */

#ifndef MEDNW_HPP_
#define MEDNW_HPP_

#include <list>

// forward decalration of mednw
template<class T> class mednwe;

/**
 * class for storing the data of one node of a median network
 */
template<class T>
class mednwnde {
private:
	vector<bool> _states;	/* the states of the characters */
	ccc<T> *_cc;			/* the consistency checker of the node */
public:
	/**
	 * default constructor
	 */
	mednwnde(  ): _cc (NULL) {};

	/**
	 * copy constructor
	 * @param[in] other the original
	 */
	mednwnde( const mednwnde<T> &other );

	/**
	 * construct a node
	 * @param[in] states the states to be set initially
	 * @param[in] cc the initial consistency checker
	 */
	mednwnde( const vector<bool> &states, ccc<T> *cc );

	/**
	 * destructor
	 */
	virtual ~mednwnde();

	/**
	 * getter for the states vector
	 * @return a copy of the states vector
	 */
	vector<bool> getstates() const;

	/**
	 * getter for the consistency checker
	 * @return a pointer to a copy of the consistency checker
	 * (or NULL if there is no consitency checker)
	 */
	ccc<T> *getccc() const;

	/**
	 * assignment opertator
	 * @param[in] other the rhs of the equation
	 * @return lhs
	 */
	mednwnde<T> & operator = (const mednwnde<T> & other);

	friend class mednwe<T>;
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
mednwnde<T>::mednwnde( const mednwnde<T> &other ){
	_states = other._states;
	if( other._cc != NULL )
		_cc = other._cc->clone();
	else
		_cc = NULL;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
mednwnde<T>::mednwnde( const vector<bool> &states, ccc<T> *cc ){
	_states = states;
//	copy(states.begin(), states.end(), ostream_iterator<bool>(cout, ""));
//	cout <<endl;
	_cc = cc;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
mednwnde<T>::~mednwnde(){
	_states.clear();
	if(_cc!=NULL)
		delete _cc;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
ccc<T> *mednwnde<T>::getccc() const{
	if(_cc==NULL){
		return NULL;
	}
	return _cc->clone();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
vector<bool> mednwnde<T>::getstates() const{
	return _states;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
mednwnde<T> & mednwnde<T>::operator = (const mednwnde<T> & other){
    if (this != &other){ // protect against invalid self-assignment
        // deallocate old memory
    	_states.clear();
    	if(_cc!=NULL)
    		delete _cc;
    	// assign the new memory to the object
    	_states = other._states;

    	if( other._cc != NULL )
    		_cc = other._cc->clone();
    	else
			_cc = NULL;
    }
    return *this;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
class mednwe{
private:
//	list<ccc<T> *> _ndcc;		/* the consistency checker of the nodes */
//	list<vector<bool> > _nodes;	/* the nodes of the median network */
	list<mednwnde<T> > _nodes;
	vector<unsigned> _toadd;	/* number of observation to be added in later
									steps of the algorithm */
	vector<chrctr<T> > _data; 	/* the data characters that created the NW */
public:

	/**
	 * default constructor
	 */
	mednwe();

	/**
	 * construct a median network from binary character data
	 * @param[in,out] chars a list of binary characters, note the order of the
	 * characters may be modified
	 * @param[in] cc a consistency checker for the underlying data type
	 * @param[in] online apply the consistency checker in an online fashion iff
	 * true
	 */
	mednwe( vector<chrctr<T> > &chars, const ccc<T> *cc, bool online );

	/**
	 * destructor
	 */
	virtual ~mednwe( ){};

	/**
	 * apply an consistency checker on the currently stored nodes
	 * delete if inconsistent
	 * @param[in] cc consistency checker
	 */
	void apply( const ccc<T> *cc );

	/**
	 * get the iterator pointing to the first node of the median network
	 * @return the iterator
	 */
	typename list<mednwnde<T> >::const_iterator begin() const;

	/**
	 * get the iterator pointing to the end of the list of nodes of
	 * the median network
	 * @return the iterator
	 */
	typename list<mednwnde<T> >::const_iterator end() const;
};


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
mednwe<T>::mednwe( vector<chrctr<T> > &chars, const ccc<T> *cc, bool online ){
	ccc<T> *tmpcc = NULL;	// for copying the consistency checker
	unsigned k=0;			// iteration counter of the algorithm
	typename list<mednwnde<T> >::iterator nn;	// iterator pointing to an
												// inserted node
	// for a given split A|B the sets A_i and B_i give the set of characters
	// (0/1) found in the data matrix at column i in rows corresponding to rows
	// in A (resp. B). the sets A_i and B_i are represented here as bool vector
	// of length 2: the first entry of A_i is true iff 0\in A_i and the
	// second if 1\in A_i (same for B_i)
	vector<vector<bool > > A, B;

	// sort the data in order to speed up computation
	sort( chars.begin(), chars.end(), cmpsplitsize<T> );
	_data = chars;

	// calculate the number of observations that will be added at most in
	// iterations [k..#chars]
	_toadd = vector<unsigned>( _data.size(), 0 );
	_toadd[ _toadd.size()-1 ] = 0;
	for( unsigned i=1; i<_data.size(); i++ ){
		_toadd[ _toadd.size()-i-1 ] = _toadd[ _toadd.size()-i ] +
				max( _data[ _data.size()-i ].getobscnt( true ),
						_data[ _data.size()-i ].getobscnt( false ));
	}

//	cerr << "data"<<endl;
//	for( unsigned i=0; i<_data.size(); i++ ){
//		cerr << _toadd[i]<<" " << _data[i] << endl;
//	}

	// init the median network to one node
	// and init the consistency checker if online
	if( online && cc != NULL ){
		_nodes.push_back( mednwnde<T>( vector<bool>(chars.size(), false), cc->clone()) );
	}else{
		_nodes.push_back( mednwnde<T>( vector<bool>(chars.size(), false), NULL ) );
	}


//	_nodes.insert(_nodes.begin(), vector<bool>(chars.size()));
//	if( online ){ _ndcc.insert(_ndcc.begin(), cc->clone());}

	// check first column for constant. if so then set the first
	// column in the node of the median network
	bool constant = _data[0].isconstant();
#ifdef DEBUG_MEDNWCD
	if( constant ){
		cerr << "constant 1st row: init graph with one node labeled by "<<_data[0][0]<<endl;
	}else{
		cerr << "init graph as usual with one node with an empty label"<<endl;
	}
#endif//DEBUG_MEDNWCD

	if( constant ){
//		copy( _nodes.begin()->_states.begin(), _nodes.begin()->_states.end(), ostream_iterator<bool>(cout, "") ); cout <<endl;
		_nodes.begin()->_states[0] = _data[0][0];
		if( online && _nodes.begin()->_cc != NULL ){ _nodes.begin()->_cc->add( _data[0], _data[0][0], _toadd[0] );}
		k = 1;
	}else{
		k = 0;
	}

	// init A and B
	A = vector<vector<bool> >( _data.size(), vector<bool>(2, false) );
	B = vector<vector<bool> >( _data.size(), vector<bool>(2, false) );

	// iterate
	for( ; k<_data.size(); k++ ){
#ifdef DEBUG_MEDNWCD
		cerr << "### k = "<<k<< " ### ";
		cerr <<_data[k]<<endl;
#endif//DEBUG_MEDNWCD

		// compute A_i and B_i for i\in [0:k-1]
		for( unsigned i=0; i<k; i++ ){
			// reset first
			A[i][0] = false; A[i][1] = false;
			B[i][0] = false; B[i][1] = false;
			// compute
			for( unsigned j=0; j<_data[k].size(); j++ ){
				if( _data[k][j] ){
					B[i][ _data[i][j] ] = true;
				}else{
					A[i][ _data[i][j] ] = true;
				}
			}
		}

#ifdef DEBUG_MEDNWCD
		cerr << "A";
		for( unsigned i=0; i<k; i++ ){
			if( A[i][0] && A[i][0] ){cerr << "*";}
			else if( A[i][0] && !A[i][0]  ){cerr << "0";}
			else if( !A[i][0] && A[i][0] ){cerr << "1";}
			else{cout << "?";}
		}
		cerr << endl;
		cerr << "B";
		for( unsigned i=0; i<k; i++ ){
			if( B[i][0] && B[i][0]  ){cerr  << "*";}
			else if( B[i][0] && !B[i][0]  ){cerr  << "0";}
			else if( !B[i][0] && B[i][0]  ){cerr  << "1";}
			else{cout << "?";}
		}
		cerr << endl;
#endif//DEBUG_MEDNWCD

//		for( unsigned i=0; i<_nodes.size(); i++){

		// iterate over all nodes and
		// A) decide if the node is in [A], [B], or both
		//    (indicated by the flags a and b)
		// B) set the k-th character of the nodes and duplicate
		//    nodes appropriately

//		typename list<ccc<T> *>::iterator jt = _ndcc.begin();
//		list<vector<bool> >::iterator it = _nodes.begin();
		for( typename list<mednwnde<T> >::iterator it=_nodes.begin(); it!=_nodes.end(); ){

			// A)
			bool a = true,
				b = true;

			for( unsigned j=0; j<k; j++ ){
				if( ! A[j][ it->_states[j] ] ){
					a = false;
					break;
				}
			}

			for( unsigned j=0; j<k; j++ ){
				if( ! B[j][ it->_states[j] ] ){
					b = false;
					break;
				}
			}

#ifdef DEBUG_MEDNWCD
			cerr << "node ";
			for( unsigned j=0; j<k; j++){cout << it->_states[j];}
			if( a && b ){cerr << " ab"<<endl;}
			else if( a && !b ){cerr << " a"<<endl;}
			else if( !a && b ){cerr << " b"<<endl;}
			else{cerr<< "??"<<endl;}
#endif//DEBUG_MEDNWCD

			// B)
			// in online mode check if appending 0 or 1 is valid
			// and modify a and b accordingly

			if( online && it->_cc != NULL ){
				bool aval = true, bval = true;
				if( a && b ){
					tmpcc = it->_cc->clone();
					if( ! (it->_cc)->add( _data[k], false, _toadd[k] ) ){
						a = aval = false;
					}
					if( ! tmpcc->add( _data[k], true, _toadd[k] ) ){
						b = bval = false;
						delete tmpcc;
					}
					if( a && b ){
//						_ndcc.insert( jt, tmpcc );
					}else if( !a && b ){
						delete it->_cc;
						it->_cc = tmpcc;
					}
				}else if( a && !b ){
					if( ! (it->_cc)->add( _data[k], false, _toadd[k] ) )
						a = aval = false;
				}else if ( !a && b ){
					if( ! (it->_cc)->add( _data[k], true, _toadd[k] ) )
						b = bval = false;
				}

				if( !aval || !bval ){
					cout << "# invalid ";
					copy(it->_states.begin(), it->_states.begin()+(k-1), ostream_iterator<bool>(cout,""));
				}
				if( !aval && !bval ) cout<<"**"<<endl;
				if( !aval && bval ) cout<<"0*"<<endl;
				if( aval && !bval ) cout<<"1*"<<endl;

#ifdef DEBUG_MEDNWCD
				cerr << "online mode modification";
				if( a && b ){cerr << " ab"<<endl;}
				else if( a && !b ){cerr << " a"<<endl;}
				else if( !a && b ){cerr << " b"<<endl;}
				else{cerr<< "??"<<endl;}
#endif//DEBUG_MEDNWCD
			}

			if( a && b ){
				// construct a new node with true appended
				nn = _nodes.insert( it, mednwnde<T>( it->_states, tmpcc) );
				nn->_states[k] = true;
				// ... and a new node with false appended
				it->_states[k] = false;
			}else if( a && !b ){
				it->_states[k] = false;
			}else if( !a && b ){
				it->_states[k] = true;
			}

			// in the offline mode each node in at least one of [A] or [B]
			if( !online && !( a || b ) ){
				cerr<< "mednw: neither a nor b is true "<<endl;
				exit(EXIT_FAILURE);
			}

			if( online && !( a || b ) ){
				it = _nodes.erase( it );
			}else{
				it++; // jt++;
			}
		}

//		cout <<data[k].splitsize()<<" " << newnodes.size()<<" " <<(float)newnodes.size()/_nodes.size()<<endl;

//#ifdef DEBUG_MEDNWCD
		cerr <<"k = "<<k+1<<"/"<< _data.size()<<" size "<< _nodes.size()<<" nodes"<<"                       \r";

//#endif//DEBUG_MEDNWCD
	}
	cerr<< endl;

	// online mode: output the final number of nodes
	// offline mode: apply the constraints
	if( online || cc == NULL  ){
		cerr <<"# " << _nodes.size() <<" nodes"<< endl;
	}else{
		apply( cc );
	}

//	cerr << "final "<<_nodes.size()<<" nodes"<<endl;
//	for( unsigned i=0; i<_nodes.size(); i++ ){
//		copy( _nodes[i].begin(), _nodes[i].end(), ostream_iterator<bool>(cerr,"") );cerr << endl;
//	}

//	for( unsigned i=0; i<_nodes.size(); i++ ){
//		unsigned cnt = 0;
//		for( unsigned j=0; j<_nodes[i].size(); j++ ){
//			if( _nodes[i][j] == true )
//				cnt++;
//		}
//		cout <<cnt<< endl;
//	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
void mednwe<T>::apply( const ccc<T> *cc ){

	unsigned i=0, ondcnt = _nodes.size();	// number of nodes before deleting invalid
	bool valid = true;
 	for( typename list<mednwnde<T> >::iterator it=_nodes.begin();
 			it!=_nodes.end(); ){

		it->_cc = cc->clone();

		if( (i+1) % 100 == 0 ){
			cerr << "checking node "<<i+1<<" / "<<ondcnt<<"\r";//<<endl;
			cerr.flush();
		}

		valid = true;
		for( unsigned j=0; j<it->_states.size(); j++ ){
			if( ! it->_cc->add( _data[j], it->_states[j], _toadd[j] ) ){
				valid=false;
				break;
			}
		}

		if( valid ){
//			cout << " valid" <<endl;
			it++;
		}else{
//			cout << "# invalid ";
//			copy(it->_states.begin(), it->_states.end(), ostream_iterator<bool>(cout,""));
//			cout << endl;
			it = _nodes.erase( it );
		}
		i++;
	}
 	cerr << endl;
	cout << endl<< "# "<< _nodes.size() <<" / "<< ondcnt << " valid"<< endl;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
typename list<mednwnde<T> >::const_iterator mednwe<T>::begin() const{
	return _nodes.begin();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
typename list<mednwnde<T> >::const_iterator mednwe<T>::end() const{
	return _nodes.end();
}

class majmedian{
private:

/**
 * a class implementing a median solver for three binary sequences.
 * the median is given by the majority per position.
 */
public:
	/**
	 * constructor.
	 */
	majmedian();

	/**
	 * apply the median solver to one problem
	 * @param[in] matrix one median problem
	 */
	vector<bool> solve( const vector<vector<bool> > &matrix ) const;

	/**
	 * solve one median problem given within a larger data set
	 * @param[in] matrix a data set
	 * @param[in] i the index of the 1st sequence of the median problem
	 * @param[in] j the index of the 2nd sequence of the median problem
	 * @param[in] j the index of the 3rd sequence of the median problem
	 */
	vector<bool> solve( const vector<vector<bool> > &matrix,
			unsigned i, unsigned j, unsigned k) const;
};

majmedian::majmedian(){

}



vector<bool> majmedian::solve( const vector<vector<bool> > &matrix ) const{
	if( matrix.size() != 3 ){
		cerr << "majmedian::solve is only implemented for 3 sequences"<<endl;
		exit(EXIT_FAILURE);
	}
	return solve( matrix, 0, 1, 2 );
}

vector<bool> majmedian::solve( const vector<vector<bool> > &matrix,
			unsigned i, unsigned j, unsigned k) const{

	vector<bool> median( matrix[0].size() );
	vector<unsigned> cnt(2,0);
//	cout << "m "; copy( matrix[i].begin(), matrix[i].end(), ostream_iterator<bool>(cout, "") ); cout << endl;
//	cout << "m "; copy( matrix[j].begin(), matrix[j].end(), ostream_iterator<bool>(cout, "") ); cout << endl;
//	cout << "m "; copy( matrix[k].begin(), matrix[k].end(), ostream_iterator<bool>(cout, "") ); cout << endl;

	for(unsigned l=0; l<median.size(); l++){
		cnt.assign(2, 0);
		cnt[ matrix[i][l] ]++;
		cnt[ matrix[j][l] ]++;
		cnt[ matrix[k][l] ]++;
//		cout<< cnt[0]<<cnt[1]<<" ";
		if( cnt[0] > cnt[1] ){
			median[l] = false;
		}else{
			median[l] = true;
		}
	}
//	cout << endl;
//	cout << "M ";
//	copy( median.begin(), median.end(),
//					ostream_iterator<bool>(cout, "") ); cout << endl;
	return median;
}



//class mednw{
//private:
//	vector<vector<bool> > _nodes;	/* the nodes of the median network */
//public:
//	mednw( const vector<chrctr> &data, majmedian *msolver );
//};

//mednw::mednw( const vector<chrctr> &data, majmedian *msolver ){
//	bool exp = false;	// true if a new sequence was added
//	counter *cnt;	//
//	_nodes;
//	unsigned i,j,k,				// iteration variables
//		ls,cs, neg=0; 			// last and current number of nodes
//	vector<bool> median;
//
//	claw::trie<bool> nodetest;
//
//	for( unsigned i=0; i<data.size(); i++ ){
//		if( nodetest.count( data[i].begin(), data[i].end() ) == 0 ){
//			cout << "\r"<<_nodes.size()<<" "<<neg; cout.flush();
//			_nodes.push_back( data[i] );
//			nodetest.insert( data[i].begin(), data[i].end() );
//		}else{
//			neg++;
//		}
//	}
//
//	ls = 0;
//	do{
//		exp = false;
//		cs = _nodes.size();
//
////		cout << "last "<<ls<<" current "<<cs<< "nodes "<<_nodes.size()<<endl;
//
//		// all combinations of three new added nodes
//		cnt = new counter( 3, cs, ls, true );
//		while( cnt->isvalid() ){
////			cout << "A"<<(*cnt)[0]<<" "<< (*cnt)[1]<<" "<< (*cnt)[2]<<endl;
//			median = msolver->solve( _nodes, (*cnt)[0], (*cnt)[1], (*cnt)[2]);
//			if( nodetest.count( median.begin(), median.end() ) == 0 ){
////			if( find( _nodes.begin(), _nodes.end(), median ) == _nodes.end() ){
//				cout << "\r"<<_nodes.size()<<" "<<neg; cout.flush();
//				exp = true;
//				_nodes.push_back( median );
//				nodetest.insert( median.begin(), median.end() );
//			}else{
//				neg++;
//			}
//			(*cnt)++;
//		};
//		delete cnt;
//
//		// all combinations of two new added nodes and one old
//		cnt = new counter( 2, cs, ls, true );
//		while( cnt->isvalid() ){
//			for( unsigned i=0; i<ls; i++ ){
////				cerr <<"B"<<(*cnt)[0]<<" "<< (*cnt)[1]<<" "<< i<<endl;
//				median = msolver->solve( _nodes, (*cnt)[0], (*cnt)[1], i);
//				if( nodetest.count( median.begin(), median.end() ) == 0 ){
////				if( find( _nodes.begin(), _nodes.end(), median ) == _nodes.end() ){
//					cout << "\r"<<_nodes.size()<<" "<<neg; cout.flush();
//					exp = true;
//					_nodes.push_back( median );
//					nodetest.insert( median.begin(), median.end() );
//				}else{
//					neg++;
//				}
//			}
//			(*cnt)++;
//		}
//		delete cnt;
//
//		// all combinations of two old nodes and one added
//		cnt = new counter( 2, ls, 0, true );
//		while( cnt->isvalid() ){
//			for( unsigned i=ls; i<cs; i++ ){
////				cerr << "C"<<(*cnt)[0]<<" "<< (*cnt)[1]<<" "<< i<<endl;
//				median = msolver->solve( _nodes, (*cnt)[0], (*cnt)[1], i);
////				if( find( _nodes.begin(), _nodes.end(), median ) == _nodes.end() ){
//				if( nodetest.count( median.begin(), median.end() ) == 0 ){
//					cout << "\r"<<_nodes.size()<<" "<<neg; cout.flush();
//					exp = true;
//					_nodes.push_back( median );
//					nodetest.insert( median.begin(), median.end() );
//				}else{
//					neg++;
//				}
//			}
//			(*cnt)++;
//		}
//		delete cnt;
//
//
//		cout <<endl;
//		ls = cs;
//	}while( exp );
//
//	for( unsigned i=0; i<_nodes.size(); i++ ){
//		copy( _nodes[i].begin(), _nodes[i].end(), ostream_iterator<bool>(cout,"") );cout << endl;
//	}
//
//}

#endif /* MEDNW_HPP_ */
