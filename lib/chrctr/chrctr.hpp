/*
 * chrctr.h
 *
 *  Created on: May 22, 2010
 *      Author: M. Bernt
 */

#ifndef CHARACTER_H_
#define CHARACTER_H_

#include <algorithm>
#include <ext/functional>	// for compose
#include <iterator>
#include <iostream>
#include <set>
#include <vector>

using namespace std;

/**
 * a class for storing a binary character
 */
template< class T >
class chrctr{
protected:
	vector<bool> _chr;  /* the (normalised) character data, i.e. _data[0]=false */
	/**
	 * the data supporting th	e split given by the binary data.
	 * implemented as vector of length 2.
	 * the first component gives the data present at the 0-side,
	 * and the 2nd component the data present at the 1 side */
	vector<vector<T> > _data;
public:
//	/**
//	 * default constructor makes nothing
//	 */
//	chrctr(){};
//
//	/**
//	 * construct a character of a given size
//	 * the data is initialised to have length s and the states are set to false
//	 * weight is initialised to 0
//	 *
//	 * @param size the desired size
//	 */
//	chrctr( unsigned s );
//
//	/**
//	 * construct a character given its binary states, note internally a
//	 * normalised variant is stored where the first state is 0
//	 * weight is initialised to 1
//	 *
//	 * @param data the binary data
//	 */
//	chrctr( const vector<bool> &data );
//
//	/**
//	 * construct a character from the column of a data matrix
//	 * @param[in] matrix the data matrix data[i][j] gives the j-th character
//	 * of the i-th species
//	 * the weight is initialised to 1
//	 * @param[in] col a column in the data matrix
//	 */
//	chrctr(const vector<vector<bool> > &matrix, unsigned col);
//
//	/**
//	 * construct a character from a bipartition a set containing two sets of integers
//	 * @param[in] split
//	 */
//	chrctr( const set<set<unsigned> > &split );
//
//	/**
//	 * destructor
//	 */
//	virtual ~chrctr();
//
//	/**
//	 * add one observation to the character (observation: a evidience for the split)
//	 * i.e. increase the weight by one
//	 *
//	 * @param[in] o the observation
//	 * @param[in] zo state of species 0 (true: the observation is made for species 0, false: not)
//	 */
//	virtual void addobservation( const T &o, bool zo );

//	/**
//	 * check if two characters are compatible
//	 * @param[in] c another character
//	 * @return true iff compatible
//	 */
//	bool compatible( const chrctr<T> &c ) const;
//
//	/**
//	 * determine the count of zeroes or ones
//	 * @return the desired number
//	 */
//	unsigned count( bool state ) const;

//	/**
//	 * get the vector of observed data for one of the two sides
//	 * of the split
//	 * @param[in] side the side of the split
//	 */
//	vector<T> getobs( bool side ) const;

//	/**
//	 * get the number of observations coded in one side of the character
//	 * @param[in] side the side
//	 * @return the number of observations
//	 */
//	unsigned getobscnt( bool side ) const;
//
//	/**
//	 * get the number of observations supporting the character
//	 * @return the number
//	 */
//	unsigned getobscnt( ) const;

//	/**
//	 * check if all values are 0 (or all are)
//	 */
//	bool isconstant() const;
//
//	/**
//	 * check if the character is informative
//	 * @return true iff the character is informative
//	 */
//	bool isinformative() const;

//	/**
//	 * return the character state for species i
//	 *@param[in] i the index
//	 *@return the state at index i
//	 */
//	virtual bool operator[](unsigned i) const;

//	/**
//	 * less than comparison operator. it just compares the binary data vectors.
//	 * this operator is necessary for storing characters in sets and
//	 * maps
//	 * @param[in] c another character
//	 * @return the result of the comparison of the normalised data vectors
//	 * 	with '<'
//	 */
//	bool operator<( const chrctr &c) const;
//
//	/**
//	 * equality comparison operator. it just compares the binary data vectors.
//	 * this operator is necessary for finding characters
//	 * @param[in] c another character
//	 * @return the result of the comparison of the normalised data vectors
//	 * 	with '<'
//	 */
//	bool operator==( const chrctr &c) const;

//	/**
//	 * output operator
//	 * @param[out] the stream to write into
//	 * @param[out] c the character
//	 * @return stream
//	 */
//	template<class S> friend ostream & operator<<(ostream &out, const chrctr<S> &c);

//	/**
//	 * write the split in nexus format, i.e. a space separated list of indices
//	 * where the state is equal to the state of a reference species
//	 * @param[in,out] out the stream to write to
//	 * @param[in] ref the index of the reference species
//	 */
//	void output_nexsplit( ostream &out, unsigned ref) const;

//	/**
//	 * get the size, i.e. the number of species that are described
//	 * by the character
//	 */
//	unsigned size() const;
//
//	/**
//	 * compute the size of the split defined by the character, i.e.
//	 * the smaller of the number of 0s and the number of ones
//	 * @return the split size
//	 */
//	unsigned splitsize() const;
};

/**
 * comparison function. compares the split sizes of two characters
 * @param[in] a a character
 * @param[in] b a character
 * @return the result of the comparison (<) of the split sizes
 */
template<class T>
bool cmpsplitsize(const chrctr<T> &a, const chrctr<T> &b);

/**
 * comparison function. compares the number of observations
 * @param[in] a a character
 * @param[in] b a character
 * @return the result of the comparison (<) of the split sizes
 */
template<class T>
bool cmpobscnt(const chrctr<T> &a, const chrctr<T> &b);

///**
// * get a compatibe subset (generated greedily by number of observations)
// * @param[in] c a list of characters
// * @return a list of compatible splits
// */
//template<class T>
//void remove_incompatible_greedy( vector<chrctr<T> > &c){
//	bool comp;
//
//	// sort by weight
//	sort(c.begin(), c.end(), cmpobscnt<T>);
//	reverse( c.begin(), c.end() );
//	for ( typename vector<chrctr<T> >::iterator it=c.begin(); it!=c.end(); ){
//
//		// check for compatibility with previous
//		comp = true;
//		for ( typename vector<chrctr<T> >::iterator jt=c.begin(); jt!=it; jt++){
//			if( ! it->compatible( *jt ) ){
//				comp = false;
//				break;
//			}
//		}
//
//		if(comp){
//			it++;
//		}else{
//			it = c.erase( it );
//		}
//	}
//}
//
///**
// * get a compatibe subset (generated greedily by number of observations)
// * @param[in] c a list of characters
// * @return a list of compatible splits
// */
//template<class T>
//vector<chrctr<T> > remove_incompatible_greedy( const vector<chrctr<T> > &c){
//	vector<chrctr<T> > gc = c;
//	remove_incompatible_greedy(gc);
//	return gc;
//}

///**
// * get the subset of non constant characters
// * @param[in] c a list of characters
// * @return a list of non-constant splits
// */
//template<class T>
//void remove_constant( vector<chrctr<T> > &c){
//	c.erase(
//		remove_if(c.begin(), c.end(), mem_fun_ref(&chrctr<T>::isconstant) ),
//		c.end());
//}

///**
// * get the subset of non constant characters
// * @param[in] c a list of characters
// * @return a list of non-constant splits
// */
//template<class T>
//vector<chrctr<T> > remove_constant( const vector<chrctr<T> > &c){
//	vector<chrctr<T> > gc = c;
//	remove_constant(gc);
//	return gc;
//}

///**
// * get a compatibe subset (generated greedily by number of observations)
// * @param[in,out] c a list of characters
// */
//template<class T>
//void remove_uninf( vector<chrctr<T> > &c){
//	c.erase(
//		remove_if(c.begin(), c.end(), __gnu_cxx::compose1(logical_not<bool>(), mem_fun_ref(&chrctr<T>::isinformative)) ),
//		c.end());
//}
//
///**
// * get a compatibe subset (generated greedily by number of observations)
// * @param[in] c a list of characters
// * @return a list of compatible splits
// */
//template<class T>
//vector<chrctr<T> > remove_uninf( const vector<chrctr<T> > &c){
//	vector<chrctr<T> > gc = c;
//	remove_uninf(gc);
//	return gc;
//}


//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//chrctr<T>::chrctr(unsigned size){
//	_chr = vector<bool>( size, false );
//	_data = vector<vector<T> >(2);
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//chrctr<T>::chrctr( const vector<bool> &data ){
//	_chr = data;
//	_data = vector<vector<T> >(2);
//
//	// normalise data vector if necessary
//	if( _chr[0] ){
//		for( unsigned i=0; i<_chr.size();i++ ){
//			_chr[i] = !_chr[i];
//		}
//	}
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//chrctr<T>::chrctr( const vector<vector<bool> > &matrix, unsigned col ){
//	_chr = vector<bool>( matrix.size(), false );
//	_data = vector<vector<T> >(2);
//
//	for( unsigned i=0; i<_chr.size(); i++ ){
//		_chr[i] = matrix[i][col];
//	}
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//chrctr<T>::chrctr( const set<set<unsigned> > &split ){
//	if( split.size() != 2 ){
//		cerr << "error: chrctr<T>::chrctr received non split "<<endl;
//		exit(EXIT_FAILURE);
//	}
//
//	bool st = true;
//
//	for( set<set<unsigned> >::const_iterator jt = split.begin(); jt!=split.end(); jt++){
////		cerr << "X"<<endl;
//		for( set<unsigned>::iterator it=jt->begin(); it!=jt->end(); it++ ){
//			if( _chr.size() < ((*it) + 1) ){
////				cerr << "resize"<<endl;
//				_chr.resize( ((*it)+1), false );
//			}
////			copy( _chr.begin(), _chr.end(), ostream_iterator<bool>(cerr, ""));cerr<<" "<<*it <<endl;
//			_chr[ *it ] = st;
//		}
//		st = !st;
//	}
////	cerr << "====================="<<endl;
//	_data = vector<vector<T> >(2);
//
//	// normalise data vector if necessary
//	if( _chr[0] ){
//		for( unsigned i=0; i<_chr.size();i++ ){
//			_chr[i] = !_chr[i];
//		}
//	}
//}
//
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//chrctr<T>::~chrctr() {
//	_chr.clear();
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//void chrctr<T>::addobservation( const T &o, bool zo ){
//	// if o was observed for species 0 then this means
//	// that o is for character state 0 in the normalised character
//	// and the other way round
//	_data[ ((zo)?0:1) ].push_back(o);
//}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
bool cmpsplitsize(const chrctr<T> &a, const chrctr<T> &b){
	return a.splitsize() < b.splitsize();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
bool cmpobscnt(const chrctr<T> &a, const chrctr<T> &b){
	return a.getobscnt() < b.getobscnt();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//template<class T>
//bool chrctr<T>::compatible( const chrctr<T> &c ) const{
//	int states = 0;
//	for( unsigned i=0; i<size(); i++ ){
//		if( !_chr[i] && !c[i] )
//			states |= 1;
//		if( _chr[i] && !c[i] )
//			states |= 2;
//		if( !_chr[i] && c[i] )
//			states |= 4;
//		if( _chr[i] && c[i] )
//			states |= 8;
//
//		if(states == 15){
//			return false;
//		}
//	}
//	return true;
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//unsigned chrctr<T>::count(bool state) const{
//	unsigned cnt = 0;
//	for( unsigned i=0; i<_chr.size(); i++ ){
//		if( _chr[i] == state ){
//			cnt++;
//		}
//	}
////	cerr << "state "<<state<<" cnt "<<cnt<<endl;
//	return cnt;
//}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//vector<T> chrctr<T>::getobs( bool side ) const{
//	if( side ){
//		return _data[1];
//	}else{
//		return _data[0];
//	}
//}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//unsigned chrctr<T>::getobscnt( bool side ) const{
//	if( side ){
//		return _data[1].size();
//	}else{
//		return _data[0].size();
//	}
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//template<class T>
//unsigned chrctr<T>::getobscnt( ) const{
//	return _data[0].size()+_data[1].size();
//}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//bool chrctr<T>::isconstant() const{
//	unsigned o = count(true);
////	copy( _data.begin(), _data.end(), ostream_iterator<bool>(cerr, "") );cerr<<endl;
////	cerr << o<<" ones"<<endl;
//	if( o == 0 || o == _chr.size() ){
////		cerr << "constant"<<endl;
//		return true;
//	}else{
////		cerr << "NOT constant"<<endl;
//		return false;
//	}
//};

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//bool chrctr<T>::isinformative() const{
//	unsigned o = count(true);
////	copy( _data.begin(), _data.end(), ostream_iterator<bool>(cerr, "") );cerr<<endl;
//	if( o < 2 || o > _chr.size()-2 ){
////		cerr << "Uninformative"<<endl;
//		return false;
//	}else{
////		cerr << "informative"<<endl;
//		return true;
//	}
//}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//bool chrctr<T>::operator[](unsigned i) const{
//	return _chr[i];
//}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//bool chrctr<T>::operator<( const chrctr &c) const{
//	return ( _chr < c._chr );
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//bool chrctr<T>::operator==( const chrctr &c) const{
//	return ( _chr == c._chr );
//}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//ostream & operator<<(ostream &out, const chrctr<T> &c){
//	copy( c._chr.begin(), c._chr.end(), ostream_iterator<bool>(out, "") );
////	out <<" "<< c._data[0].size()<< " "<< c._data[1].size();
////	out <<" "<< c.splitsize()<< " "<< c.splitsize();
////	out << " (";
////	for(unsigned i=0;i<c._data[0].size(); i++){
////		out <<"[" << c._data[0][i]<< "]"<<c._data[0][i].size()<<endl;
////	}
////	out << ")";
//
////	out << "(";
////	for(unsigned i=0;i<c._data[1].size(); i++){
////		out<< "[" << c._data[1][i] << "]"<<c._data[1][i].size()<<endl;
////	}
////	out << ")";
//	return out;
//}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//template<class T>
//void chrctr<T>::output_nexsplit( ostream &out, unsigned ref ) const{
//
//	for( unsigned i=0; i<_chr.size(); i++ ){
//		if( _chr[i] == _chr[ref] ){
//			out << i+1 <<" ";
//		}
//	}
//}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//
//template<class T>
//unsigned chrctr<T>::size() const{
//	return _chr.size();
//}
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<class T>
//unsigned chrctr<T>::splitsize() const{
//	unsigned o = count( true ),
//		z;
//	z = _chr.size()-o;
//	return ( (o<z)?o:z );
//}

#endif /* CHARACTER_H_ */
