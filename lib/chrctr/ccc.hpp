/*
 * ccc.hpp
 *
 * consistency checkers
 * - abstract base class ccc
 * - null class
 *.
 *  Created on: Sep 27, 2010
 *      Author: maze
 */

#ifndef CCC_HPP_
#define CCC_HPP_

#include "chrctr.hpp"
#include "genom.hpp"

/**
 * abstract base class for a consistency checker
 * derived classes must implement the add function
 */
template<class T>
class ccc{
private:
	unsigned _ocnt;		/* count the number of added observations */
	unsigned _minobs;	/* minimum number of observation to be achieved */
public:
	ccc( unsigned minobs ) : _ocnt (0), _minobs (minobs){ };
	virtual ~ccc(){ };

private:
	/**
	 * add a new constraint
	 * @param[in] con a constraint
	 * @return true if the addition of the constraint is allowed
	 */
	virtual bool add( const T &con ) = 0;
public:

	/**
	 * add a new character in a given state to the data
	 * @param c the character
	 * @param state the state of the character
	 * @param[in] toadd the number of observations that will be added to the
	 * character at most after adding c
	 * @return true if the addition is allowed
	 */
	virtual bool add( chrctr<T> &c, bool state, unsigned toadd );

	virtual ccc<T>* clone()const = 0;

	/**
	 * determine the number of permutations consistent with the
	 * current constraints
	 * @return the number of consistent permutations
	 */
	virtual unsigned cntconsistent() const = 0;

	/**
	 * get the number of observations added to the consistency checker
	 * @return the number of observations
	 */
	virtual unsigned getobs() const;

//	/**
//	 * get one consistent permutation
//	 * @return the a permutation
//	 */
//	virtual genom getpermutation() const = 0;
//
//	/**
//	 * get all consistent permutation
//	 * @return the a permutation
//	 */
//	virtual vector<genom> getpermutations() const = 0;

	/**
	 * output operator. will wall the output function of the derived classes
	 * @param[in,out] os the stream to write into
	 * @param[in] the consistency checker to write
	 * @return the stream
	 */
	template<class U>
	friend ostream &operator<<(ostream &os, const ccc<U> &c);

	virtual ostream &output( ostream &os ) const = 0;
};
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
class ccc_null : public ccc<T>{
public:
	ccc_null() : ccc<T>(0) {}
    ~ccc_null(){}

private:
	/**
	 * add a new adjacency
	 * @param[in] pair an adjacency
	 * @return true if the addition of the pair allows
	 * for a linear/circular ordering
	 */
	bool add( const T &con );
public:

	ccc<T>* clone()const;

	unsigned cntconsistent() const;

	/**
	 * get one consistent permutation
	 * @return the a permutation
	 */
	genom getpermutation() const;

	/**
	 * get all consistent permutation
	 * @return the a permutation
	 */
	vector<genom> getpermutations() const;

	/**
	 * get the number of connected components
	 * @return number of connected component
	 */
	unsigned nrcmp( ) const;

	/**
	 * the output function of the adjacency consistency checker
	 * @param[in,out] the stream to write into
	 * @return the written stream
	 */
	ostream &output( ostream &os )const;
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class T >
bool ccc<T>::add( chrctr<T> &c, bool state, unsigned toadd ){
	vector<T> cst = c.getobs( state );

	// if the number of already added observations + the number of
	// observations added by this function + the number of observations that
	// may be added later is smaller than the smallest acceptable number
	// of observations -> no need to continue
	if( (_ocnt + cst.size() + toadd) < _minobs ){
		return false;
	}

	// add the observations
	for(unsigned k=0; k<cst.size(); k++){
		if( ! add( cst[k] )){
			return false;
		}
		_ocnt++;
	}

//	cerr << " +> 1"<<endl;
	return true;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<class T>
unsigned ccc<T>::getobs() const{
	return _ocnt;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template< class U >
ostream &operator<<(ostream &os, const ccc<U> &c){
    return c.output(os);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
bool ccc_null<T>::add( const T &con ){
	return true;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
ccc<T>* ccc_null<T>::clone()const{
	return new ccc_null<T>();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
unsigned ccc_null<T>::cntconsistent() const{
	return 1;
}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<typename T>
//genom ccc_null<T>::getpermutation() const{
//	return genom();
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//template<typename T>
//vector<genom> ccc_null<T>::getpermutations() const {
//	return vector<genom>();
//}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename T>
ostream &ccc_null<T>::output( ostream &os ) const {
	os << "ccc_null";
	return os;
}

#endif /* CCC_HPP_ */
