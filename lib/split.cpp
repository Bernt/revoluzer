/*
 * splitsys.cpp
 *
 *  Created on: Jun 10, 2011
 *      Author: maze
 */

#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <ext/functional>	// for compose
#include <functional>
#include <iterator>
#include <iostream>

#include <glpk.h>

#include "split.hpp"

using namespace std;


template <typename S, typename T>
class sort_helper: binary_function<T,T,bool>{
	S *p;

public:
	sort_helper( S *p0): p( p0 ) {}

	bool operator()( const T &a, const T &b ) {
		return p->operator()(a, b);
	}
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// split functions
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

split::split(){};

split::split( const split &s ){
	_bp = s._bp;
	for( unsigned i=0; i<s._data.size(); i++ ){
		_data.push_back( vector<character*>(s._data[i].size(), NULL ) );
		for( unsigned j=0; j<s._data[i].size(); j++ ){
			_data[i][j] = s._data[i][j]->clone();
		}
	}
}

// ----------------------------------------------------------------------------

split::split(unsigned size){
	_bp = vector<bool>( size, false );
	_data = vector<vector< character*> >(2, vector< character*>());
}

// ----------------------------------------------------------------------------

split::split( const vector<bool> &data ){
	_bp = data;
	_data = vector<vector< character*> >(2, vector< character*>() );

	normalize();
}

// ----------------------------------------------------------------------------

split::split( const vector<vector<bool> > &matrix, unsigned col ){
	_bp = vector<bool>( matrix.size(), false );
	_data = vector<vector< character*> >(2, vector< character*>());

	for( unsigned i=0; i<_bp.size(); i++ ){
		_bp[i] = matrix[i][col];
	}

	normalize();
}

// ----------------------------------------------------------------------------

split::split( const set<set<unsigned> > &split ){
	if( split.size() != 2 ){
		cerr << "error: split::split received non split "<<endl;
		exit(EXIT_FAILURE);
	}

	bool st = true;

	for( set<set<unsigned> >::const_iterator jt = split.begin(); jt!=split.end(); jt++){
//		cerr << "X"<<endl;
		for( set<unsigned>::iterator it=jt->begin(); it!=jt->end(); it++ ){
			if( _bp.size() < ((*it) + 1) ){
//				cerr << "resize"<<endl;
				_bp.resize( ((*it)+1), false );
			}
//			copy( _bp.begin(), _bp.end(), ostream_iterator<bool>(cerr, ""));cerr<<" "<<*it <<endl;
			_bp[ *it ] = st;
		}
		st = !st;
	}
//	cerr << "====================="<<endl;
	_data = vector<vector< character*> >(2);

	normalize();
}

// ----------------------------------------------------------------------------

split::~split() {
//	cerr << "delete split "<<*this<<endl;
	_bp.clear();
	for( unsigned i=0; i<_data.size(); i++ ){
//		cerr <<i<<"  "<<_data[i].size()<<endl;
		for( unsigned j=0; j<_data[i].size(); j++ ){
			delete _data[i][j];
		}
	}
//	cerr << "~~~~"<<endl;
}

// ----------------------------------------------------------------------------

void split::addsupport( const character *o, bool zo ){
	// if o was observed for species 0 then this means
	// that o is for character state 0 in the normalised character
	// and the other way round
	_data[ ((zo)?0:1) ].push_back( o->clone() );
}

// ----------------------------------------------------------------------------

bool split::compatible( const split &c ) const{;
	int states = 0;
	for( unsigned i=0; i<size(); i++ ){
		if( !_bp[i] && !c[i] )
			states |= 1;
		if( _bp[i] && !c[i] )
			states |= 2;
		if( !_bp[i] && c[i] )
			states |= 4;
		if( _bp[i] && c[i] )
			states |= 8;

		if(states == 15){
			return false;
		}
	}
	return true;
}

// ----------------------------------------------------------------------------

bool split::constant() const{
	if( splitsize() == 0 ){
		return true;
	}else{
		return false;
	}
};


// ----------------------------------------------------------------------------

bool split::informative() const{
	if( splitsize() < 2 ){
		return false;
	}else{
		return true;
	}
}

// ----------------------------------------------------------------------------

void split::normalize(){
	if( _bp[0] ){
		for( unsigned i=0; i<_bp.size();i++ ){
			_bp[i] = !_bp[i];
		}
	}
}

// ----------------------------------------------------------------------------

bool split::operator[](unsigned i) const{
	return _bp[i];
}

// ----------------------------------------------------------------------------

bool split::operator<( const split &c) const{
	return ( _bp < c._bp );
}

// ----------------------------------------------------------------------------

bool split::operator==( const split &c) const{
	return ( _bp == c._bp );
}

// ----------------------------------------------------------------------------
split& split::operator=(const split& p) {
    if (this != &p) {  // make sure not same object
    	_bp = p._bp;
    	// Delete old memory.
    	for( unsigned i=0; i<_data.size(); i++ ){
    		for( unsigned j=0; j<_data[i].size(); j++ ){
    			delete _data[i][j];
    		}
    	}
    	_data = vector<vector<character*> >(2, vector<character*>());
    	// fill new data
    	for( unsigned i=0; i<p._data.size(); i++ ){
    		_data[i] = vector<character*>( p._data[i].size(), NULL );
    		for( unsigned j=0; j<p._data[i].size(); j++ ){
    			_data[i][j] = p._data[i][j]->clone();
    		}
    	}
    }
    return *this;    // Return ref for multiple assignment
}

// ----------------------------------------------------------------------------

ostream & operator<<(ostream &out, const split &c){
	c.output(out, true, false);
	return out;
}

// ----------------------------------------------------------------------------

void split::output( ostream &out, bool prnsplit, bool prnsupport ) const{

	if( prnsplit ){
		copy( _bp.begin(), _bp.end(), ostream_iterator<bool>(out, "") );
		out <<" "<< splitsize(false)<< " "<< splitsize(true);
	}
	if( prnsupport ){
		out <<" "<< _data[0].size()<< " "<< _data[1].size();
		out << " (";
		for(unsigned i=0;i<_data[0].size(); i++){
			_data[0][i]->output( out );
			out<< endl;
		}
		out << ")";
		out << "(";
		for(unsigned i=0;i<_data[1].size(); i++){
			_data[1][i]->output( out );
			out <<endl;
		}
		out << ")";
	}
}

// ----------------------------------------------------------------------------

void split::output_nexsplit( ostream &out, unsigned ref ) const{
	for( unsigned i=0; i<_bp.size(); i++ ){
		if( _bp[i] == _bp[ref] ){
			out << i+1 <<" ";
		}
	}
}

// ----------------------------------------------------------------------------

unsigned split::size() const{
	return _bp.size();
}

// ----------------------------------------------------------------------------

unsigned split::splitsize() const{
	unsigned o = splitsize( true ),
		z;
	z = size()-o;
	return ( (o<z)?o:z );
}

// ----------------------------------------------------------------------------

unsigned split::splitsize(bool state) const{
	return count(_bp.begin(), _bp.end(), state);
}

// ----------------------------------------------------------------------------

//vector<character*> split::support( bool side ) const{
//	if( side ){
//		return _data[1];
//	}else{
//		return _data[0];
//	}
//}

// ----------------------------------------------------------------------------

unsigned split::supportcnt( bool side ) const{
	return _data[side ? 1 : 0].size();
}

// ----------------------------------------------------------------------------

unsigned split::supportcnt( ) const{
	return _data[0].size()+_data[1].size();
}

// ----------------------------------------------------------------------------
vector<character*>::const_iterator split::support_begin( bool side ) const{
	return _data[ side ? 1 : 0 ].begin();
}

// ----------------------------------------------------------------------------

vector<character*>::const_iterator split::support_end( bool side ) const{
	return _data[ side ? 1 : 0 ].end();
}


// ----------------------------------------------------------------------------

double split::weight( const split_weight *foo ) const{
	return foo->calc( *this );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// split comparison functions
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool split_cmpsplitsize(const split &a, const split &b){
	return a.splitsize() < b.splitsize();
}

// ----------------------------------------------------------------------------

bool split_cmpsupport(const split &a, const split &b){
	return a.supportcnt() < b.supportcnt();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// split weight functions
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


split_weight::split_weight( character_weight *c, unsigned norm ) : _chrweightfoo( c ), _norm(norm){
}

split_weight::~split_weight(){
}


bool split_weight::operator()(const split &a, const split &b) const {
	return calc( a ) < calc(b);
}

// ----------------------------------------------------------------------------
void split_weight::set_norm( unsigned n ){
	if( _norm != 0 ){
		_norm = n;
	}
}

// ----------------------------------------------------------------------------

split_weight_unit::split_weight_unit( character_weight* cw,
		unsigned normalize ) : split_weight( cw, normalize ) {
}

// ----------------------------------------------------------------------------

double split_weight_unit::calc( const split &c ) const{
	if( _norm == 0 ){
		return 1.0;
	}else{
		cerr << "normalize"<<endl;
		return 1.0/_norm;
	}
}

// ----------------------------------------------------------------------------

split_weight_support::split_weight_support( character_weight* cw,
		unsigned normalize ) : split_weight( cw, normalize ) {
}

// ----------------------------------------------------------------------------

double split_weight_support::calc( const split &c ) const{
	double w = 0;

	for( unsigned i=0; i<2; i++ ){
		for( vector<character*>::const_iterator it=c.support_begin(i);
				it!=c.support_end(i); it++){

//			w += (*it)->weight( _chrweightfoo );
			w += (*_chrweightfoo)( (*it) );
		}
	}


	if( _norm > 0 ){
		cerr << "normalize"<<endl;
		w /=_norm;
	}

	return w;
}

// ----------------------------------------------------------------------------

split_weight_binomial::split_weight_binomial( character_weight* cw, unsigned m ,
		unsigned normalize) : split_weight( cw, normalize ) {

	_m = m;
	_pr = pascal_row( _m );
}

// ----------------------------------------------------------------------------

double split_weight_binomial::calc( const split &c ) const{
	double w = 0,
			pc = 0,
			p = 0;
	unsigned ss = 0,	// splitsize
		sss = 0;		// splitsize sum

//	cout << "SPLIT "<<c<<endl;

	for( unsigned i=0; i<2; i++ ){
		// determine split size (i.e. how many species are on side i)
		ss = c.splitsize( i );
		sss += ss;

		// get the probability (pc) for each character supporting the split
		// [since the weight funtion returns a weight (1-prob), compute 1-w]
		// compute the probability for the character occurring in the specific
		// configuration (i.e. the 1/0 pattern)
		for( vector<character*>::const_iterator it=c.support_begin(i);
				it!=c.support_end(i); it++){

			pc = 1-(*_chrweightfoo)( (*it) );
			p = _pr[ ss ] * std::pow(pc, ss ) * std::pow(1-pc, _m-ss );

			if( p==0 ){
				p = -1* numeric_limits<double>::min_exponent10;
			}else{
				p = -1 * log10( p );
			}

//			cerr <<ss<<" # "<<pc<< " -> "<< p<<endl;
			w += p;
//			cerr << w<<endl;
		}
	}

	if( sss != _m ){
		cerr << "split_weight_binomial: split sizes do not sum up"<<endl;
		exit(EXIT_FAILURE);
	}

//	cerr << w<<endl;

	if( _norm > 0 ){
		cerr << "normalize"<<endl;
		w /=_norm;
	}

	return w;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// split system functions functions
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//splitsystem::splitsystem( const vector<split> &splits, unsigned size, split_weight *wfoo ){
//	_size = size;
//	_weightfoo = wfoo;
//	_splits = splits;
//}

void splitsystem::add_uninformative( ){
	vector<bool> ts( _size, false );
//	split tc;
	for( unsigned i=0; i<_size; i++ ){
		ts[i] = true;
		split tc = split( ts );
		if( find( _splits.begin(), _splits.end(), tc ) == _splits.end() ){
			_splits.push_back( tc );
		}
		ts[i] = false;
	}
}
// ----------------------------------------------------------------------------

vector<split> splitsystem::get_splits() const{
	return _splits;
}

// ----------------------------------------------------------------------------

bool splitsystem::inlcudes( const split &s ){
	return ( find( _splits.begin(), _splits.end(), s ) != _splits.end() );
}

// ----------------------------------------------------------------------------

void splitsystem::output_nexus( ostream &out ) const{

	out << "BEGIN Splits;"<<endl;
	out << "DIMENSIONS ntax="<<_size<<" nsplits="<< _splits.size() <<";"<<endl;
	out << "FORMAT labels=no weights=yes confidences=no intervals=no;"<<endl;
	//	PROPERTIES fit=-1.0 cyclic;
	//	CYCLE 1 3 4 5 6 2;
	out << "MATRIX"<<endl;

	for( unsigned i=0; i<_splits.size(); i++ ){
		out << "["<<i+1<<", size="<< _splits[i].splitsize() <<"]\t"
				<< _splweightfoo->calc( _splits[i] ) << "\t";
		_splits[i].output_nexsplit( out, 0  );
		out << "," <<endl;
	}
	out << ";"<<endl;
	out << "END; [Splits]"<<endl;

}

// ----------------------------------------------------------------------------
void splitsystem::output_nexus_chars( ostream &out ) const{

	unsigned nchar = 0;
	for( unsigned j=0; j<_splits.size(); j++ ){
		nchar += _splits[j].supportcnt();
	}


	out << "BEGIN CHARACTERS;"<<endl;
	out << "DIMENSIONS NCHAR="<< nchar <<";"<<endl;;
	out << "format datatype=standard labels=no;"<<endl;
	out << "matrix"<<endl;
	for( unsigned i=0; i<_size; i++ ){
		for( unsigned j=0; j<_splits.size(); j++ ){
			for(unsigned x=0; x<_splits[j].supportcnt(); x++){
				out << _splits[j][i];
			}
		}
		out << endl;
	}

//	[FORMAT
//	[DATATYPE={STANDARD|DNA|RNA|PROTEIN}]
//	[RESPECTCASE]
//	[MISSING=symbol]
//	[GAP=symbol]
//	[SYMBOLS="symbol symbol ..."]
//	[LABELS={NO|LEFT}]
//	[TRANSPOSE={NO|YES}]
//	[INTERLEAVE={NO|YES}]
//	[TOKENS=NO]
//	;]
//	[CHARWEIGHTS wgt_1 wgt_2 ... wgt_nchar;]
//	[CHARSTATELABELS character-number [character-name]
//	[ /state-name [ state-name... ] ], ...;]
//	MATRIX
//	sequence data in specified format
	out << ";"<<endl;
	out << "END;"<<endl;

}

// ----------------------------------------------------------------------------
bool splitsystem::cmp_splitweight( const split &a, const split &b ) const{
	return _splweightfoo->calc( a ) < _splweightfoo->calc( b );
}

// ----------------------------------------------------------------------------
bool splitsystem::operator()( const split &a, const split &b ) const{
	return _splweightfoo->calc( a ) < _splweightfoo->calc( b );
}

// ----------------------------------------------------------------------------
unsigned splitsystem::supportcnt( ) const{
	unsigned s=0;
	for(unsigned i=0; i<_splits.size(); i++){
		s += _splits[i].supportcnt();
	}
	return s;
}

// ----------------------------------------------------------------------------

void splitsystem::remove_constant( ){
	for( vector<split>::iterator it=_splits.begin(); it!=_splits.end(); ){
		if( it->constant() ){
			it = _splits.erase( it );
		}else{
			++it;
		}
	}
	// reset support count
	_splweightfoo->set_norm( supportcnt() );
}

// ----------------------------------------------------------------------------

void splitsystem::remove_incompatible_greedy(){
	bool comp;

//	cerr << "START GREEDY"<<endl;
	// sort by weight (*this) makes a comparison
	// based on the split weight

	sort_helper<splitsystem,split> sh( this );
	sort( _splits.begin(), _splits.end(),  sh);

	// THIS CONSUMES ENORMOUS AMOUNTS OF MEM
//	sort( _splits.begin(), _splits.end(),  *this);

	reverse( _splits.begin(), _splits.end() );

	for ( vector<split>::iterator it=_splits.begin(); it!=_splits.end(); ){

		// check for compatibility with previous
		comp = true;
		for ( vector<split>::iterator jt=_splits.begin(); jt!=it; jt++){
			if( ! it->compatible( *jt ) ){
				comp = false;
				break;
			}
		}

		if(comp){
			it++;
		}else{
			it = _splits.erase( it );
		}
	}
//	cerr << "START GREEDY"<<endl;
	// reset support count
	_splweightfoo->set_norm( supportcnt() );
}

// ----------------------------------------------------------------------------


void splitsystem::remove_incompatible_lp( int tm_lim ){
	glp_prob *lp;
	unsigned //wsum, 		// sum of observations
		cidx;			// constraint counter
	vector<int> ia, ja;
	vector<double> ar;

//	cout <<c.size()<< "splits "<<endl;

	ia = vector<int>(1, 0);
	ja = vector<int>(1, 0);
	ar = vector<double>(1, 0.0);

	lp = glp_create_prob();
	glp_set_prob_name(lp, "max-weight-compatible-splits");
	glp_set_obj_dir( lp, GLP_MAX );

//	// determine weight sum
//	wsum=0;
//	for( unsigned i=0; i<c.size(); i++ ){
//		wsum += c[i].getobscnt();
//	}
	cidx = 0;

	// add columns (1 per split)
	glp_add_cols( lp, _splits.size() );
	for ( unsigned i=0; i<_splits.size(); i++ ){

		// set up variable x_i \in {0,1} with coefficient w_i
		glp_set_col_name( lp, i+1, (string("x")+int2string(i+1)).c_str());
		glp_set_col_bnds( lp, i+1, GLP_DB, 0.0, 1.0);
		glp_set_col_kind( lp, i+1, GLP_BV);
		glp_set_obj_coef( lp, i+1, _splweightfoo->calc( _splits[i] ) );

		for ( unsigned j=i+1; j<_splits.size(); j++){
			if( ! _splits[i].compatible( _splits[j] ) ){
//				cout << "incompatible "<<i+1<<" " <<j+1<<endl;

				cidx++;
				ia.push_back(cidx);
				ja.push_back(i+1);
				ar.push_back(1.0);

				ia.push_back(cidx);
				ja.push_back(j+1);
				ar.push_back(1.0);
			}
		}
	}

//	cout << cidx<<" constraints"<<endl;
	glp_add_rows( lp,  cidx);
	for( unsigned i=1; i<=cidx; i++ ){
		glp_set_row_name( lp, i, (string("p")+int2string(i)).c_str() );
		glp_set_row_bnds( lp, i, GLP_UP, 0, 1.0);
	}

//cout << ia.size()-1<<endl;
	glp_load_matrix( lp, ia.size()-1, (int*) &(ia[0]), (int*) &(ja[0]), (double *) &(ar[0]) );
//	glp_write_lp(lp, NULL, "test.cplex");

//	glp_simplex(lp, NULL);
//	cout << "objective value "<<glp_get_obj_val(lp) <<endl; ;
//	for( unsigned i=0; i<c.size(); i++ ){
//		cout << glp_get_col_prim(lp, i+1)<<" ";
//	}
//	cout << endl;


	glp_iocp parm;
	glp_init_iocp(&parm);

//	parm.msg_lev = GLP_MSG_OFF; // no output;
	parm.msg_lev = GLP_MSG_ERR; // error and warning messages only;
//	parm.msg_lev = GLP_MSG_ON; // normal output;
//	parm.msg_lev = GLP_MSG_ALL; // full output (including informational messages).

	parm.presolve = GLP_ON;
	parm.binarize = GLP_ON;

	// backtracking technique
//	parm.bt_tech = GLP_BT_DFS;	// depth first (less memory)
//	parm.bt_tech = GLP_BT_BFS;	// breadth first
	parm.bt_tech = GLP_BT_BLB;	// best local bound (default)

	// branching technique
//	parm.br_tech = GLP_BR_FFV;	// first fractional
	parm.br_tech = GLP_BR_MFV;	// most fractional
//	parm.br_tech = GLP_BR_DTH;  // heuristic by Driebeck and Tomlin (default)


//	parm.pp_tech = GLP_PP_NONE;  	// disable preprocessing;
//	parm.pp_tech = GLP_PP_ROOT; 	// preprocessing only on the root level;
	parm.pp_tech = GLP_PP_ALL;  	// preprocessing on all levels (default)

	parm.tm_lim = tm_lim; 			// time limit (millisec)

	glp_intopt(lp, &parm);

	switch( glp_mip_status(lp) ){
	case GLP_OPT:
		cerr << "# max weight "<<glp_mip_obj_val(lp)<< " (optimal)" <<endl;
		break;
	case GLP_FEAS:
		cerr << "# weight "<<glp_mip_obj_val(lp)<< " " <<endl;
		break;
	default:
		cerr << "ILP found no solution -> aborting" <<endl;
		exit(EXIT_FAILURE);
		break;
	}

//	for( unsigned i=0; i<c.size(); i++ ){
//		cout << glp_mip_col_val(lp, i+1)<<" ";
//	}
//	cout << endl;

	// get the max compatible subset
	for( int i=_splits.size()-1; i>=0; i-- ){
		if( glp_mip_col_val(lp, i+1) != 1 ){
			_splits.erase( _splits.begin()+i );
		}
	}
	glp_delete_prob(lp);

	// reset support count
	_splweightfoo->set_norm( supportcnt() );
}

// ----------------------------------------------------------------------------

void splitsystem::remove_uninformative( ){
	for( vector<split>::iterator it=_splits.begin(); it!=_splits.end(); ){
		if( it->informative() ){
			++it;
		}else{
			it = _splits.erase( it );
		}
	}

	// reset support count
	_splweightfoo->set_norm( supportcnt() );
}
