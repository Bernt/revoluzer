/*
 * dstnc_pinv.h
 *
 *  Created on: May 24, 2010
 *      Author: maze
 */

#ifndef DSTNC_PINV_HPP_
#define DSTNC_PINV_HPP_

#include "common.hpp"
#include "dstnc.hpp"
#include "helpers.hpp"
#include "dstnc_inv.hpp"

/**
 * distance for calculating the preserving inversion distance
 * using signed strong interval trees
 */
class dstnc_pinv: public dstnc {
private:
	unsigned _n;
	vector<int> _r,						// the generator
		_l;
	vector<pair<int,int> > _strong_int;	// strong intervals
	dstnc_inv *_rd;

	/**
	 * recursively compute the costs for linear and prime nodes in the subtree of p
	 * @param[in] p a node of a signed strong interval tree
	 * @param[in] g the genome underlying the signed strong interval tree
	 * @param[in] n the length of the genome
	 * @param[out] dist the accumated cost (should be initialized with 0)
	 */
	void _interval_tree_properties(itnode *p, const genom &g, unsigned n, unsigned &dist);

	/**
	 * compute the perfect distance between g and id,
	 *@param[in] g a genome
	 *@return the perfect distance
	 */
	unsigned _perfect_distance(const genom g);

	/**
	 * compute the distance of a prime node with linear parent
	 *@param[in] p the prime node
	 *@param[in] n the length of the underlying genomes
	 *@param[in] g the underlying genome
	 *@return the distance of the prime node
	 */
	unsigned _prime_distance(itnode *p, int n, const genom &g);
public:
	/**
	 * default constructor
	 */
	dstnc_pinv();

	/**
	 * constructor
	 */
	dstnc_pinv( unsigned n );

	/**
	 * destructor
	 */
	virtual ~dstnc_pinv();

	/**
	 * adapt the distance calculator to a new genome length
	 * @see dstnc::adapt
	 */
	void adapt( unsigned n );

	/**
	 * calulate the distance preserving the common intervals of g1 and g2
	 * @see dstnc::calc
	 */
	unsigned calc( const genom &src, const genom &tgt );

	/**
	 * calculate the distance from g1 to g2 preserving the common intervals of
	 * a set of genomes
	 * @see dstnc::calc
	 */
	unsigned calc( const genom &src, const genom &tgt, const vector<genom> &genomes );

	/**
	 * copy constructor
	 * @see dstnc::clone
	 */
	dstnc* clone() const;

	/**
	 * print information
	 * @see dstnc::calc
	 */
	ostream & output(ostream &os) const;
};

/**
 * a c wrapper .. just to make it callable from c
 *@param[in] g1_p a genome
 *@param[in] g2_p anoter genome
 *@param[in] n the length of the genomes
 *@return the perfect distance
 */
int perfect_distance_c(int *g1_p, int *g2_p, int n);

/**
 * a c wrapper for computing the distance from g1_p to g2_p while
 * preserving the comon intervals of genomes_p
 * @param[in] g1_p a genome
 * @param[in] g2_p another genome
 * @param[in] genomes_p a set of genomes
 * @param[in] n the length of the genomes g1_p, g2_p, and all genomes in genomes_p
 * @param[in] m the number of genomes in genomes_p
 */
int perfect_distance_glob_c( int *g1_p, int *g2_p, int **genomes_p, int n, int m );

/**
 * a c wrapper for computing the sum of preserving distance
 * from g1_p to the genomes
 * @param[in] g1_p a genome
 * @param[in] genomes_p1 the first genome in a set of genomes
 * @param[in] genomes_p2 the second genome in a set of genomes
 * @param[in] genomes_p3 the third genome in a set of genomes
 * @param[in] n the length of the genomes
 * @return the perfect distance
 */
int perfect_distance_glob_sum_c( int *g1_p, int *genomes_p1, int *genomes_p2, int *genomes_p3, int n);

#endif /* DSTNC_PINV_HPP_ */
