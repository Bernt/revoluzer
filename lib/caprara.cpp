#include <algorithm>
#include <iostream>
#include <iterator>
#include <limits>

#include "caprara.hpp"
#include "conserved.hpp"
#include "dstnc_inv.hpp"
#include "io.hpp"
#include "helpers.hpp"
#include "median.hpp"

//#include "condense.h"
//#include "condense3.h"
#include "inversion_median_alberto.h"

// enable old code: i.e. iterate over all combinations of k-signs
//#define ITERCOMB

//#define DEBUG_TCIP
//#define DEBUG_PRIMESCORE
//#define DEBUG_SOLVE_COMP
//#define DEBUG_CONSTRMED

//#define ADDTIME
#ifdef ADDTIME
	unsigned long skip,
		compute;
#endif//ADDTIME

using namespace std;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

rmp::rmp( unsigned n, bool circular ){
	_dst = new dstnc_inv( n, circular );
}

rmp::~rmp(){
	delete _dst;
}

void rmp::solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
		int &mediancnt, int &score,	vector<genom> &medians) {

	caprara(problem, 0, 0, 0, 0, 0, 0, lower_bound, upper_bound, allmed, NOTLAZY, mediancnt, medians);
	if( mediancnt > 0 ){
		score = _dst->calcsum( medians[0], problem );
	}else{
		score = std::numeric_limits< int >::max();
	}
}

ostream &rmp::output( ostream &os ) const {
	os << "RMP(Caprara)";
	return os;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

rmp_common::rmp_common( unsigned n, bool circular, int method, int pair_pd) {
	switch( method ){
		case TCIP:
			if( pair_pd != 0 ){
				cerr << "rmp_common: TCIP with 'pairwise perfect distance' is not available"<<endl;
				exit(0);
			}
		case ECIP:
		case CIP:
			_method = method;
			break;
		default:
			cerr << "invalid rmp solver:"<<method << endl;
			exit(0);
	}
	if( pair_pd ){
		cerr << "rmp_common: pairwise common interval preserving distance is not implemented"<<endl;
		exit(EXIT_FAILURE);
		//		_dst =
	}else{
		_dst = new dstnc_pinv(n);
	}
}

//unsigned rmp_common::distance( const genom &g1, const genom &g2, const vector<genom> &gemomes ){
//	return g1.distance(g2, *_hd);
//}

void rmp_common::solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
		int &mediancnt, int &score, vector<genom> &medians) {

	switch( _method ){
		case CIP:
			caprara(problem, 0, 0, 1, _pair_pd, 0, 0, lower_bound, upper_bound, allmed, NOTLAZY, mediancnt, medians);
			break;
		case ECIP:
			caprara(problem, 1, 0, 1, _pair_pd, 0, 0, lower_bound, upper_bound, allmed, NOTLAZY, mediancnt, medians);
			break;
		case TCIP:
			tcip(problem, allmed, mediancnt, score, medians);
			break;
	}
	if( mediancnt > 0 ){
		score = _dst->calcsum( medians[0], problem );
		cout << "score "<<score<<endl;
	}else{
		score = std::numeric_limits< int >::max();
	}
}

ostream &rmp_common::output( ostream &os ) const {
	os << "pIMP(";
	switch( _method ){
		case CIP:
			os << "CIP";
			break;
		case ECIP:
			os << "ECIP";
			break;
		case TCIP:
			os << "TCIP";
			break;
	}
	os << ")";
	return os;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void caprara(const vector<genom> &genomes,
		int dynamic_restrictions, int conserved, int common, int pair_pd,
		int sig, int circular, int lower_bound, int upper_bound,
		int allmed, int lazy, int &mediancnt,
		vector<genom> &medians){

	int **medians_ptr=NULL,	// medians returned by caprara
		n = 0;				// number of genes
	struct genome_struct *gs[3];
	int *genes;

	if(genomes.size() != 3){
		cout << "caprara: need exactly 3 genomes!"<<endl;
		exit(1);
	}

	n = genomes[0].size();

	mediancnt = 0;
	medians.clear();

	genes = (int *) malloc(n * sizeof(int));
	for(unsigned i=0; i<3; i++){
		gs[i] = (genome_struct *) malloc(sizeof(genome_struct));
	}

		// fill with data
	for(unsigned i=0; i<genomes.size(); i++){
		gs[i]->genes = genomes[i].get_pointer();
	}

	if(upper_bound == std::numeric_limits< int >::max())
		upper_bound = 0;
	if(lower_bound == std::numeric_limits< int >::max())
		lower_bound = 0;

		// call the median solver
	albert_inversion_median_noncircular_mb ( gs, n, genes, &mediancnt,
		&medians_ptr, lower_bound, upper_bound, dynamic_restrictions,
		conserved, common, pair_pd, sig, circular, allmed, lazy);

	for(unsigned i = 0 ; i < (unsigned)mediancnt ; i++){
		genom temp;
		temp = genom(medians_ptr[i], n, circular, genomes[0].get_nmap());
//		if( temp == genomes[0] || temp == genomes[1] && temp == genomes[2] ){
//			medians.clear();
//			break;
//		}
		medians.push_back(temp);
		free(medians_ptr[i]);
	}

//	cerr << "todo: move distance reallocation "<<endl;
//	dstnc_inv *rd;
//	rd = new dstnc_inv( genomes[0].size(), circular );
//
//		// compute the score  if there is a median, else set it to std::numeric_limits< int >::max()
//	if (mediancnt > 0){
//		for(unsigned j=0; j<medians.size(); j++){
//			cout << "j "<<j<<endl;
//			cout << medians[j]<<endl;
//			score = 0;
//			if(common != 0 && pair_pd == 0){
//				score = perfect_distance_glob_sum(medians[j], genomes, medians[j].size());
//			}
//			else{
//				for(unsigned i=0; i<3; i++){
//					if (common)
//						score += perfect_distance(medians[j], genomes[i]);
//					else if (conserved)
//						score += preserving_reversal_distance(medians[j], genomes[i], circular);
//					else{
//						score += rd->calc( medians[j], genomes[i]);
//						cout << rd->calc( medians[j], genomes[i])<<" ";
//					}
//				}
//				cout <<" => " << score<<endl;
//			}
//
//			if ( j>0 ){
//				if (score != sc){
//					cerr << "different median score"<<endl;
//					cerr << medians.size()<<" medians"<<endl;
//					cout << score << " != "<<rd->calcsum(medians[j], genomes)<<endl;
//					exit(1);
//				}
//			}
//			sc = score;
//		}
//	}else{
//		score = std::numeric_limits< int >::max();
//	}

//	cout << "returning "<< mediancnt << " medians of score "<<score << endl;
//	for(unsigned i=0; i<medians.size(); i++)
//		cout << medians[i] << endl;

	sort(medians.begin(), medians.end());
	unique(medians);
	mediancnt = medians.size();

	for(unsigned i=0; i<3; i++){
		free( gs[i] );
	}
	free(genes);
	free(medians_ptr);
	return;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void init_mpqnode(mpqnode* parent, mpqnode** n, int type, pair<int,int> interval){

	*n = new(mpqnode);
	(*n)->parent = parent;

	if(parent != NULL){
		parent->children.push_back(*n);
	}

	(*n)->i = interval;
	(*n)->type = type;
	(*n)->signs = 0;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int mpqnode_getsign( mpqnode *n, int p, unsigned m){
	if( (n->signs & ppow(m-1-p)) > 0 ){
		return DEC;
	}else{
		return INC;
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void mpqnode_setsign( mpqnode *n, int p, int sign, unsigned m){
	if( sign == DEC ){
		n->signs |=  ppow(m-1-p);
	}else{
		n->signs &=  (n->signs - ppow(m-1-p));
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void mpqtree( const itnode *itnode, const vector<int> &r, const vector<int> &l,
		mpqnode *parent, mpqnode **mpqroot ){

	int type = PRI;
	mpqnode *t;

		// determine the type of nodes with more than 2 children (P/Q)
	if( itnode->children.size() > 2){
		int i = itnode->children[0]->i.first  +1,
			j = itnode->children[1]->i.second +1;

			// if (i..j) is an interval -> it is an Q node
		if(l[j] <= i && i <= j && j <= r[i]){
			type = LIN;
		}
	}
		// nodes with 2 or 0 childs are Q nodes (1 is impossible)
	else if ( itnode->children.size() <= 2 ){
		type = LIN;
	}

		// init the node
	init_mpqnode( parent, mpqroot, type, itnode->i);

		// recursion
	for(unsigned i = 0; i< itnode->children.size(); i++){
		mpqtree( itnode->children[i], r, l, *mpqroot , &t);
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// build a tree from a set of identified genomes
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void mpqtree(const vector<genom> &genomes, int n, mpqnode **mpqroot){
	itnode *itroot = NULL;

	static vector<int> r,		// the generator
		l;
	vector<genom> gi = genomes;
	static vector<pair<int,int> > strong_int;

	if((int)r.size() != n+1){
		r = vector<int>( n+1, 0 );
		l = vector<int>( n+1, 0 );
		strong_int = vector<pair<int,int> > (2*n);
	}

		// compute the generator and the strong intervals
	identify(gi);
	combined_generator(gi, n, r, l);
	canonical_generator(r, l);
	strong_intervals(r, l, strong_int);

		// get the interval tree from the strong intervals
	interval_tree(strong_int, n, gi[0], &itroot);

		// convert the interval tree into an mpq tree
	mpqtree( itroot, r, l,  NULL, mpqroot );
		// free the memory of the interval tree
	interval_tree_free(itroot);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

mpqnode * mpqtree_construct(const vector<genom> &genomes, int n){
	mpqnode *mpqroot;				// the root of the pqtree of the input genomes
	vector<vector<int> > inv_pis;	// the positions of the elements in the genomes
	vector<genom> idgenomes = genomes; 	// identified genomes

		// identify the genomes, i.e. rename the elements such that
		// the first genome becomes the identity
	identify(idgenomes);
		// allocate memory for the inverse permutations and compute them
	inv_pis = vector<vector<int> >(idgenomes.size(), vector<int>(n+1,0));
	for(unsigned i=0; i<idgenomes.size(); i++){
		for(unsigned j=0; j<idgenomes[i].size(); j++){
			inv_pis[i][ abs(idgenomes[i][j]) ] = j;
		}
	}

		// compute the mpqtree and determine the node types
	mpqtree( idgenomes, n, &mpqroot);
	mpqtree_signs(mpqroot, idgenomes, inv_pis);

	return mpqroot;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// free a node of a mpqtree and all child nodes
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void mpqtree_free(mpqnode *p){
	for( unsigned i=0; i<p->children.size(); i++ ){
		mpqtree_free( p->children[i] );
	}
	p->medians.clear();
	p->scores.clear();
#ifndef ITERCOMB
	p->min.clear();
#endif//ITERCOMB
	p->children.clear();
	delete( p );

}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void mpqtree_front( mpqnode *n, const genom &g,  genom &f ){
	for(unsigned i=0; i<n->children.size(); i++){
		mpqtree_front( n->children[i], g, f );
	}
	if( n->children.size() == 0 ){
		if( n->i.first < 0 ){
			f.push_back( -1*g[ abs(n->i.first) ] );
		}else{
			f.push_back( g[ abs(n->i.first) ] );
		}
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool mpqtree_haspnode(mpqnode *p){
	if ( p->type == PRI ){
		return true;
	}
	for(unsigned i=0; i<p->children.size(); i++){
		if ( mpqtree_haspnode(p->children[i]) ){
			return true;
		}
	}
	return false;

}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void mpqtree_primenodes(mpqnode *p, vector<mpqnode *> &pnode){
	for( unsigned i=0; i<p->children.size(); i++ ){
		mpqtree_primenodes( p->children[i], pnode);
	}
	if( p->type == PRI ){
		pnode.push_back( p );
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// print the tree
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void mpqtree_print(mpqnode *p, unsigned m){

	cout << "PQ-tree: "<<endl;
	if(p == NULL){
		cout << " is NULL"<<endl;
		return;
	}

	cout << "("<< p->i.first<<","<<p->i.second<<") ";
	switch (p->type){
		case LIN: {cout << "lin "; break;}
		case PRI: {cout << "pri "; break;}
	}

	for(unsigned h=0; h < m; h++){
		if( mpqnode_getsign( p, h, m) == INC){
			cout << "inc ";
		}else if( mpqnode_getsign( p, h, m) == DEC ){
			cout << "dec ";
		}else if( mpqnode_getsign( p, h, m) == UNK ){
			cout << "unk ";
		}else{
			cout << "INVALID ";
		}
	}

	cout << "-> ";
	for( unsigned i=0; i<p->children.size(); i++ ){
		cout << "("<<p->children[i]->i.first<<","<<p->children[i]->i.second<<") ";
	}
	cout << endl;
	for( unsigned i=0; i<p->children.size(); i++ ){
		mpqtree_print( p->children[i], m );
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void mpqtree_print_dot(mpqnode *p, const genom &g, unsigned m, ostream &o, bool horizontal){

	string shape = "",
		s0, s1, s2;
	int border = 1;
	mpqnode *tmp;

		// print header once (before starting the root node)
		// here all graph properties are set and the leaves
		// are set to the same rank
	if( p->parent == NULL ){
		o << "digraph g{"<<endl;
		o << "ordering=out;"<<endl;
//		o << "fontname=Helvetica;"<<endl;
		o << "node[shape=plaintext, fontsize=100];"<<endl;
		o << "nodesep=0.05;"<<endl;
		o << "ranksep=5;"<<endl;
		o << "{ rank = same; ";
		for( unsigned i=0; i<g.size(); i++){
			 o << "\"f"<< i <<"t"<<i<<"\"; ";
		}
		o << "}"<<endl;
	}

	if (p->type == PRI){
		shape = "shape=ellipse,";
		border = 0;
	}

	// define the node (the name is fSTARTtEND where start and end are the boundaries of the interval)
	o << "f"<< p->i.first<<"t"<<p->i.second<<" ["<<shape<<"label=";
	o << "<<TABLE CELLSPACING=\"0\" cellpadding=\"0\" BORDER=\"0\" CELLBORDER=\""<<border<<"\">"<<endl;
// o << "<TD PORT=\"p\">";
	o<< "<TR>";
	if(!horizontal){
		o << "<TD>";
		for(int i=p->i.first  ; i<p->i.second; i++){
			print_element(g[i], o, 1, "", g.get_nmap());
			o << " ";
		}
		print_element(g[p->i.second], o, 1, "", g.get_nmap());
		o<<"</TD>";
	}

	if( p->type == LIN || ( p->type == PRI && (p->parent != NULL && p->parent->type == LIN) )){
		if(p->type == LIN){	tmp = p; }
		else{				tmp = p->parent;}

		o << "<TD>";
		o << "<FONT POINT-SIZE=\"70.0\">";
		for(unsigned i=0; i<m-1; i++){
			if( mpqnode_getsign( tmp, i, m) == INC ){ o << "+"; if(!horizontal){o<<"<BR/>";}}
			else{o << "-"; if(!horizontal){o<<"<BR/>";}}
		}
		if( mpqnode_getsign( tmp, m-1, m) == INC ){o << "+";}
		else{o << "-";}
		o << "</FONT></TD>";
	}

	if(horizontal){
		o<<"</TR><TR><TD>";
		for(int i=p->i.first ; i<p->i.second; i++){
			print_element(g[i], o, 1, "", g.get_nmap());
			o << " ";
		}
		print_element(g[p->i.second] ,o, 1, "", g.get_nmap() );
		o<<"</TD></TR>";
	}else{
		o<<"</TR>";
	}

	o << "</TABLE>>]"<< endl;

		// define the edge to the parent node
	if( p->parent != NULL ){
		o << "f"<<p->parent->i.first<<"t"<<p->parent->i.second ;
//		if (p->parent->type == LIN)
//			o << ":p";
		o << " -> "<< "f"<< p->i.first<<"t"<<p->i.second;
//		if(p->type == LIN)
//			o <<":p";
		o << "[dir=none]"<<endl;
	}

		// recursion
	for(unsigned i=0; i<p->children.size(); i++){
		mpqtree_print_dot( p->children[i], g, m, o, horizontal );
	}

		// end the grpah
	if( p->parent == NULL ){
		o << "}"<<endl;
	}
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// fill the connected components of p nodes (connected by p-p edges), given
// the topest node of a component; this node is the first in the component
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void mpqtree_pnode_component( mpqnode *n, vector<mpqnode *> &component ){
	component.push_back( n );
	for(unsigned i=0; i<n->children.size(); i++ ){
		if( n->children[i]->type == PRI ){
			mpqtree_pnode_component( n->children[i], component );
		}
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// determine quotient permutations of a node
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void mpqtree_quotientpermutations( mpqnode *node, const vector<genom> &genomes, int n,
	const vector<vector<int> > &inv_pis, bool sign, vector<genom> &qperm ){

	vector<int> start_element; 			// the start elements of the child intervals in genomes[0]
	qperm = vector<genom>(genomes.size(), genom(node->children.size(), 0));
	start_element = vector<int>(node->children.size(), 0);					// I)

	for(unsigned j=0; j<node->children.size(); j++){
		start_element[j] = genomes[0][ node->children[j]->i.first ];
	}
	for(unsigned i=0; i<genomes.size(); i++){
		for(unsigned j=0; j<node->children.size(); j++){					// II)
			qperm[i][j] = inv_pis[i][ abs( start_element[j] ) ];
		}
		sort(qperm[i].begin(), qperm[i].end());									// III)
		for(unsigned j=0; j<node->children.size(); j++){					// IV
			qperm[i][j] = genomes[i][ qperm[i][j] ];
		}
		quotient_permutation(qperm[i], n);									// V)
		for(unsigned j=0; j<qperm[i].size(); j++){							// VI)
			if( node->children[ qperm[i][j]-1 ]->type != PRI && mpqnode_getsign(node->children[ qperm[i][j]-1 ], i, genomes.size()) == DEC )
				qperm[i][j] *= -1;
		}
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// assign the signs of the nodes
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void mpqtree_signs(mpqnode *n, const vector<genom> &genomes, const vector<vector<int> > &inv_pis){
	int start_elm,
		end_elm;

		// recursion
	for(unsigned i=0; i<n->children.size(); i++){
		mpqtree_signs( n->children[i], genomes, inv_pis);
	}

//	cerr << "mpqtree_signs"<<endl;
//	cerr << genomes<<endl;

		// only Q nodes are considered
	if( n->type == LIN ){

//		cout << "Q node ["<<n->i.first<<","<<n->i.second<<"]"<<endl;

		n->signs = 0;
			// leaf: take the signs of the elements;
			// negative -> decreasing and positive -> increasing
		if( n->children.size() == 0 ){
			start_elm = genomes[0][ n->i.first ];
			for( unsigned i=1; i<genomes.size(); i++ ){
				if( genomes[i][ inv_pis[i][ abs( start_elm ) ] ] < 0 ){
					mpqnode_setsign(n, i, DEC, genomes.size());
				}
			}
		}else{
				// inner node:
				// determine if the node is increasing / decreasing (for each genome)
				// by comparing the positions of the smallest and the biggest elements
				// i.e. the first and the last in genomes[0]
			start_elm = genomes[0][ n->children[0]->i.first ];
			end_elm   = genomes[0][ n->children.back()->i.first ];
//			cout << start_elm << " " << end_elm<<endl;
			for( unsigned i=1; i<genomes.size(); i++ ){
				if( inv_pis[i][ abs( start_elm ) ] > inv_pis[i][ abs( end_elm ) ] ){
					mpqnode_setsign(n, i, DEC, genomes.size());
				}
			}
		}
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void tcip(const vector<genom> &genomes,
		int allmed, int &mediancnt,
		int &score, vector<genom> &medians){

//	if(genomes.size() != 3){
//		cerr << "tcip: need exacly 3 genomes"<<endl;
//		exit(1);
//	}

	mpqnode *mpqroot;				// the root of the pqtree of the input genomes
	int n = genomes[0].size();		// the length of the genomes
	vector<vector<int> > inv_pis;	// the positions of the elements in the genomes
	vector<genom> idgenomes = genomes; 	// identified genomes
	dstnc_inv *rd  = NULL;

		// reset the medians and results
	score = 0;
	mediancnt = 0;
	medians.clear();

	rd = new dstnc_inv(n, false);

		// identify the genomes, i.e. rename the elements such that
		// the first genome becomes the identity
	identify(idgenomes);

		// allocate memory for the inverse permutations and compute them
	inv_pis = vector<vector<int> >(idgenomes.size(), vector<int>(n+1,0));
	for(unsigned i=0; i<idgenomes.size(); i++){
		for(unsigned j=0; j<idgenomes[i].size(); j++){
			inv_pis[i][ abs(idgenomes[i][j]) ] = j;
		}
	}

		// compute the mpqtree and determine the node types
	mpqtree( idgenomes, n, &mpqroot);
	mpqtree_signs(mpqroot, idgenomes, inv_pis);

#ifdef DEBUG_TCIP
	cout << "genomes "<<genomes;
	cout << "identified genomes" << idgenomes ;
	mpqtree_print(mpqroot, genomes.size());
#endif

#ifdef ADDTIME
	double time = get_time();
	skip = 0;
	compute = 0;
#endif//ADDTIME
		// start the recursion, i.e. count sign differences between linear nodes
		// and precompute all oRMPs to solve for pnodes
	tcip_node_medians(mpqroot, idgenomes, n, inv_pis, allmed, rd);

#ifdef ADDTIME
	time = get_time() - time;
	cout << "#oRMPtime "<<time<<endl;
#endif//ADDTIME

		// recontruct the medians, start with the identity permutation, and apply
		// reversals implied by the 1st component of the k-signs and replace
		// pnode quotient permutations by the local medians (the best combinations
		// of pnode medians are computed also here)
		// NOTE: we don't start at the identity because we want the medians
		// 'relative to' the original permutations - not of the identified ones.
		// so the we start at pi_1 (we make only reversals of intervals and reorder
		// children (intervals) of pnodes .. the intervals are relative
		// to the first permutation (the id) and correstpond directly to the
		// intervals in the identity)

		// init the medians with the id (i.e. the first genome)
	medians.push_back( genomes[0] );
#ifdef ADDTIME
	time = get_time();
#endif//ADDTIME
	tcip_construct_medians(mpqroot, medians, score, allmed);
		// make the medians unique .. remove duplicates
	sort(medians.begin(), medians.end());
	unique(medians);
	mediancnt = medians.size();

#ifdef ADDTIME
	time = get_time() - time;
	cout << "#mediantime "<<time<<endl;;
#endif//ADDTIME

#ifdef DEBUG_TCIP
	dstnc *pinv = new dstnc_pinv( n );
	cout << "DEidentified medians score should be "<<score<<endl;
	for(unsigned i=0; i<medians.size(); i++){
		cout << pinv->calcsum(medians[i], genomes, genomes) << " : "<<medians[i]<<endl;
		for(unsigned j=0; j<genomes.size(); j++)
			cout << pinv->calc(medians[i], genomes[j], genomes) << " ";
		cout << endl;
	}
	delete pinv;
#endif//DEBUG_TCIP


		// free memory
	inv_pis.clear();
	idgenomes.clear();
	mpqtree_free(mpqroot);
	delete rd;
#ifdef ADDTIME
	cout <<"#skip " << skip<<endl;;
	cout <<"#compute " << compute<<endl;
#endif//ADDTIME
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int tcip_c( struct genome_struct **gen, int ngenes, int circular, int *median ){
	int mediancnt = 0,
		score = 0;
	vector<genom> genomes,
		medians;

	for(int i=0; i<3; i++){
		genomes.push_back( genom(gen[i]->genes, ngenes, circular) );
	}

	tcip(genomes, ONE_MEDIAN, mediancnt, score, medians);

	median = medians[0].get_pointer();
	return score;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void tcip_apply_lmedians( const mpqnode *n, const vector<genom> &lmed, vector<genom> &medians ){

	int pos; 						// current replace position
	vector<genom> newmeds;			// the new medians
	vector< pair<int, int> > ip;	// the intervals of the child nodes (this is the identity)

		// get the intervals of the child nodes
	ip = vector<pair<int, int> >(n->children.size());
	for(unsigned i=0; i<n->children.size(); i++){
		ip[i] = n->children[i]->i ;
	}

	for (unsigned i=0; i<lmed.size(); i++){			// iterate over all local (node) medians ...

//		cout << "apply "<<lmed[i]<<endl;

		for(unsigned k=0; k<medians.size(); k++){	// modify each of the medians acording to the current prime median
													// i.e. apply it to all medians
			newmeds.push_back(medians[k]);

				// reorder the childrens
			pos = n->i.first;
			for(unsigned l=0; l<lmed[i].size(); l++){
				if(lmed[i][l] > 0){
					for(int m=ip[ lmed[i][l]-1 ].first; m<=ip[ lmed[i][l]-1 ].second; m++){
//							cout << "m["<<pos<<"] "<<endl;
						newmeds.back()[pos] = medians[k][m];
						pos++;
					}
				}else{
					for(int m=ip[ abs(lmed[i][l])-1 ].second; m>=ip[ abs(lmed[i][l])-1 ].first; m--){
//							cout << "m["<<pos<<"] "<<endl;
						newmeds.back()[pos] = -1 * medians[k][m];
						pos++;
					}
				}
			}
//			cout << newmeds<<endl;
//			cout << "-------------"<<endl;

		}

	}

	medians = newmeds;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void tcip_construct_medians( mpqnode *n, vector<genom> &medians, int &score, int allmed){

	vector<genom> lmed;				// a local median of a pnode
	vector<vector<genom> > smed; 	// medians of one solution
	vector<mpqnode *> component; 	// a component of p nodes
	vector<vector<int> > solution,	// the best combinations of pnode orientations
		ambpos,						// the ambigous positions of each child
		marrpos,					// access positions and lengths ..
		marrlen;					// .. of the multidimensional array (scores medians)

		// recursion (i.e. depth first)
	for(unsigned i=0; i<n->children.size(); i++){
		tcip_construct_medians(n->children[i], medians, score, allmed);
	}

		// linear nodes have to be considered if: a) they are the root or b) they have a linear parent.
		// if they have a prime parent the orientations in the medians follows from the orientation of
		// medians of the prime-RMP of the parent
	if( n->type == LIN && (n->parent == NULL || n->parent->type == LIN) ){
		if(n->scores.size() > 0)
			score += n->scores[0];
		if(n->medians.size() > 0 ){
			if (n->medians.size() == 2){
				lmed = medians;	// abuse the lmedian an temporary storage
			}
			for(unsigned i=0; i<medians.size(); i++){
				reverse(medians[i], n->i.first, n->i.second);
			}
			if (n->medians.size() == 2){
				medians.insert(medians.end(), lmed.begin(), lmed.end());
			}
#ifdef DEBUG_CONSTRMED
		cout << "new medians "<<medians<<endl;
#endif//DEBUG_CONSTRMED
		}
	}
		// each component of pnodes is considered seperately, and as whole,
		// so we start at the topmost  pnode of a component, i.e. the pnode with linear parent or root.
		//
		// it is known that all nodes below the current are allready processed
	else if( n->type == PRI && (n->parent == NULL || n->parent->type == LIN) ){
			// get the nodes in the component, and find the optimal sign assignment combinations of the pnodes

		mpqtree_pnode_component( n, component );
		score += tcip_solve_component( component, solution, ambpos, marrpos, marrlen, allmed );
		smed = vector<vector<genom> >(solution.size(), medians);

		for( unsigned i=0; i<solution.size(); i++ ){
				// set the signs in the components
			for(unsigned j=0; j<solution[i].size(); j++){
				component[j]->signs = solution[i][j];
			}

				// apply the median of the components (starting from the end, because there are the lower -smaller-  ones)
			for( int j=component.size()-1; j>=0; j-- ){
					// I) get the medians of the node
				marrpos[j][0] = solution[i][j];
				for(unsigned k=0; k<ambpos[j].size(); k++){
					marrpos[j][k+1] = component[j]->children[ ambpos[j][k] ]->signs;
				}
				lmed = marray_get( component[j]->medians, marrlen[j], marrpos[j] );
					// II) apply them
				tcip_apply_lmedians(component[j], lmed, smed[i]);
			}
		}

		medians.clear();
		for(unsigned i=0; i<smed.size(); i++){
			medians.insert(medians.end(), smed[i].begin(), smed[i].end());
		}
#ifdef DEBUG_CONSTRMED
		cout << "new medians "<<medians<<endl;
#endif//DEBUG_CONSTRMED
			// make space for the processing of the next component
		smed.clear();
		solution.clear();
		component.clear();
	}
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// n the current node (initially this should be the root of the tree)
// genomes the genomes underlying the pqtree
// inv_pis the inverse permutations of the genomes, i.e. the indices of the elements in the genomes
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void tcip_node_medians( mpqnode *node, const vector<genom> &genomes, int n,
		const vector<vector<int> > &inv_pis, int allmed, dstnc_inv *rd ){


//TODO -> MEDIANS FOR LIN NODES + ABORT/CONTINUE IF PRIME NODE

	int diff = 0,	// number of sign differences (for Q node with Q parent)
		equa = 0;
		// recursion (so it is depth first)
	for(unsigned i=0; i<node->children.size(); i++){
		tcip_node_medians( node->children[i], genomes, n, inv_pis, allmed, rd);
	}

#ifdef DEBUG_TCIP
		if( node->type == PRI ){cout << "P";}
		else{cout << "Q";}
		cout << " node ["<<node->i.first<<","<<node->i.second<<"] with "<< node->children.size() <<" childs"<< " parent is ";
		if( node->parent == NULL ){cout << "the root"<<endl;}
		else if (node->parent->type == PRI){cout << "P "<<endl;}
		else if (node->parent->type == LIN){cout << "Q "<<endl;}
#endif//DEBUG_TCIP

		// handle the following cases:
		// a) linear node which is the root
		// b) linear node with prime parent
		// c) linear node with linear parent
		// d) prime node (the two different cases prime / linear parent are distinguished in prime_distance())

		// a) linear node which is the root
		//    - count the number of signs which are increasing ( 0 < #inc <= 3, because the first is always increasing
		// 	    as pi_0 is the identity [in the case of 3 input genomes])
		//    - if there is one or two increasing nodes: one reversal (the minority)
		//    - one increasing: the 1st (=id) is the only inc = minority, therefore reverse the interval
		//    [linear directed: sort them all to inc]
		//    [linear undirected: finished (it's not interesting if the three genomes are sorted increasing / decreasing)]
	if( node->type == LIN && node->parent == NULL ){
		for(unsigned i=0; i < genomes.size(); i++){
			if( mpqnode_getsign(node, i, genomes.size()) == DEC ){
				diff++;
			}
		}
		equa = genomes.size() - diff;
#ifdef DEBUG_TCIP
		cout << diff <<" dec, "<< equa<<" inc at root"<<endl;
#endif//DEBUG_TCIP


		if (diff < equa && diff > 0){			// reverse the signs which are different
			node->scores = vector<int>(1, diff);	// so #diff reversals are needed
												// the first one is not amongst them .. because all sigs are + for the first one, i.e. equal
		}else if (diff == equa){				// there are two equally good possibilities (only possible for odd number of genomes)
			node->scores = vector<int>(1, diff);
			node->medians = vector<vector<genom> >(2);
		}else if (equa < diff){					// reverse the signs which are equal (no test for "&& equa > 0" because the first is id -> always no difference)
			node->scores = vector<int>(1, equa);	// so #equa reversals are needed
			node->medians = vector<vector<genom> >(1);	// the first one is amongst them.
		}


//		switch (diff){
////			case 0: {break;} 			// 0 inc -> noop (cannot happen because the first is id)
//			case 1: {node->scores = vector<int>(1); node->medians = vector<vector<genom> >(1);  break;}	// 1 inc & 2 dec -> reverse the inc
//			case 2: {node->scores = vector<int>(1); break;}	// 2 inc & 1 dec -> reverse the dec
//			case 3: {break;}			// 3 inc -> noop
//			default: {
//				cerr << "tcip: impossible number of sign differences "<<diff<<endl;
//				exit(1);
//				break;
//			}
//		}
	}

		// b) it has a prime parent -> there is also nothing to do, because the node itself is already sorted and
		//    while solving the median problem of the parent P node the node will be included in the proper way
	else if( node->type == LIN && node-> parent->type == PRI ){
#ifdef DEBUG_TCIP
		cout << "noop"<<endl;
#endif//DEBUG_TCIP
	}
		// c) the node is linear and has a linear parent
		//    - count the number of sign differences between the node and its parent (a node has a sign
		//      for each of the three permutations, where the sign of the first  permutation is alway
		//      POS as it is the identity). FOR 3 PERMUTATIONS: if this number is:
		//      3 -> cannot occur, because in the first permutation the nodes are always positive
		//      2 -> the interval in the permutation with no difference has to be reversed : +1 reversal (this has to be the first perm.)
		//      1 -> the interval in the permutation where the difference occured has to be reversed : +1 reversal
		//      0 -> nothing to do
	else if( node->type == LIN && (node->parent != NULL && node->parent->type == LIN) ){
			// count how often the sign of the node is different in the node and its parent
		for(unsigned i=0; i < genomes.size() ; i++){
			if( mpqnode_getsign(node, i, genomes.size()) != mpqnode_getsign(node->parent, i, genomes.size()) ){
				diff++;
			}
		}
		equa = genomes.size()-diff;
#ifdef DEBUG_TCIP
		cout << diff <<" differences"<<endl;
#endif//DEBUG_TCIP

		if (diff < equa && diff > 0){
			node->scores = vector<int>(1, diff);	// so #diff reversals are needed
												// the first one is not amongst them .. because all sigs are + for the first one, i.e. equal
		}else if( diff == equa ){
			node->scores = vector<int>(1, diff);
			node->medians = vector<vector<genom> >(2);
		}else if (equa < diff){
			node->scores = vector<int>(1, equa);	// so #equa reversals are needed
			node->medians = vector<vector<genom> >(1);	// the first one is amongst them.
		}

//		switch (diff){
//			case 0: { break;}
//			case 1: {node->scores = vector<int>(1); break;}
//			case 2: {node->scores = vector<int>(1); node->medians = vector<vector<genom> >(1); break;}
//			default: {
//				cerr << "tcip: impossible number of sign differences "<<diff<<endl;
//				exit(1);
//				break;
//			}
//		}
	}
		// d) the node is a prime. the subcases (root, prime parent, linear parent are handled
		//    in prime_score)
	else if( node->type == PRI){
		tcip_pnode_medians(node, genomes, n, inv_pis, allmed, rd);
	}
		// should be impossible; just in case of possible and probable bugs
	else{
		cerr << "tcip: impossible tree configuration"<<endl;
		exit(1);
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// compute the score of a prime node
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void tcip_pnode_medians(mpqnode *node, const vector<genom> &genomes, int n,
		const vector<vector<int> > &inv_pis, int allmed, dstnc_inv *rd ){

	bool skip_subopt; 					// needed for ONE_MEDIAN .. for deciding to skip / not
	int mediancnt=0,					// number of medians found ..
		score=0,						// .. and their score for the oRMPs
		tgtor = 0;						// current target orientation
	vector<genom> rmp,					// the rmp to solve
		invrmp,							// inverse of the rmp permutations
		urmp,							// rmp for solving unoriented
		lmedians;						// the medians of a oRMP
	vector<int>
		marray_lens,					// hyper cube accession arrays, storing the length of the dimensions
		marray_pos;						// ... and a current access position
	vector<vector<int> > ambpos;		// the positions in the rmp which are amconst vector<vector<int> > &inv_pis, bigous (for each ambigous position
										// one vector storing the psoitions of the element in the permutations)

		// find the (quotient) permutations of the children in the given permutations, i.e. get the rmp to solve
		//    I)  For all children: take the element in genomes[0] at the start index of a children
		//    II) with the help of the inverse permutations get the indices of these elements in the other permutations
		//    III)sort these indices
		//    IV) take the elements at the positions
		//    V)  get the quotient permutations (note that the quotient permutations are unsigned)
		//    VI) assign signs to the elements of the quotient permutation (if known);
		//        - if the child is a Q node set it to + if increasing and - if decreasing; so change is only needed for -
		//        - P children are handled seperately later; (next step)
		//        and store the inverse permutations of the rmp
	rmp = vector<genom>(genomes.size(), genom(node->children.size(), 0));
	invrmp = vector<genom>(genomes.size(), genom(node->children.size() + 1, 0));
	urmp = vector<genom>(genomes.size());

	mpqtree_quotientpermutations(node, genomes, n, inv_pis, true, rmp);
	for( unsigned i=0; i<rmp.size(); i++ ){
		for( unsigned j=0; j<rmp[i].size(); j++ ){
			invrmp[i][ abs(rmp[i][j]) ] = j;
		}
	}
//	start_element = vector<int>(node->children.size(), 0);					// I)
//	for(unsigned j=0; j<node->children.size(); j++){
//		start_element[j] = genomes[0][ node->children[j]->i.first ];
//	}
//	for(unsigned i=0; i<genomes.size(); i++){
//		for(unsigned j=0; j<node->children.size(); j++){					// II)
//			rmp[i][j] = inv_pis[i][ abs( start_element[j] ) ];
//		}
//		sort(rmp[i].begin(), rmp[i].end());									// III)
//		for(unsigned j=0; j<node->children.size(); j++){					// IV
//			rmp[i][j] = genomes[i][ rmp[i][j] ];
//		}
//		quotient_permutation(rmp[i], n);									// V)
//		for(unsigned j=0; j<rmp[i].size(); j++){							// VI)
//			invrmp[i][ rmp[i][j] ] = j;
//			if( node->children[ rmp[i][j]-1 ]->type != PRI && mpqnode_getsign(node->children[ rmp[i][j]-1 ], i, genomes.size()) == DEC )
//				rmp[i][j] *= -1;
//		}
//	}



		// find the ambigous positions: P children:
		// A) find the P childs and store the positions in the rmp genomes in the ambpos vector
		// B) do the initial sign assignment
	for(unsigned i=0; i<node->children.size(); i++){			// A)
		if( node->children[i]->type == PRI ){
			ambpos.push_back( vector<int>() );
			for(unsigned j=0; j<genomes.size(); j++){
				ambpos.back().push_back( invrmp[j][i+1] );
			}
		}
	}

		// initialise the multidimensional arrays (aka hypercubes)
		// (and the accession vectors)
		// storing the scores and corresponding medians of the oRMPs
		// with the different sign combinations
	marray_lens = vector<int>( ambpos.size()+1, 4 );
		// handling of the different parent possibilities. the problem is that it is not
		// enough to compute the standard median problem, but the orientations of the parent
		// node have to be regarded.
		// - P parent: test all 4 possible parent orientations (standard)
		// - Q parent: 1 possibilitiy (in the directions of the the parent)
		// - root: also 1 possibility (all increasing )
		// this information is stored in the marray lens and pos arrays

		// TODO (for linear undirected handling the root P node has to be solved in all 4 possibilities, too.)
		// the idea for P nodes with P parents is the following:
		// * the rmp will be solved in all 9 possible directions
		// * the best directions are stored in the signs array of the node
		// * these (a)signments will be considered when the parent is solved, i.e. not all possibilities
		//   of sign assignments are tested, but only the best from the child
	if(node->parent == NULL){
		marray_lens[0] = 1;
	}else if( node->parent->type == LIN ){
		marray_lens[0] = 1;
	}
	marray_pos = vector<int>( ambpos.size()+1, 0 );
	marray_init( node->medians, marray_lens );
	marray_init( node->scores, marray_lens );


#ifdef DEBUG_PRIMESCORE
	cout << "RMP "<<endl<<rmp;
	cout <<ambpos.size() << " ambigous position(s)"<<endl;
	for(unsigned i=0; i<ambpos.size(); i++){
		cout << "element "<< rmp[0][ ambpos[i][0] ]<<" at positions (";
		for (unsigned j = 0; j < ambpos[i].size(); j++) {
			cout << ambpos[i][j]<<" ";
		}
		cout << ") ";
	}
	if (ambpos.size() > 0)
		cout << endl;
	cout << "hyper cube dimensions: "; copy(marray_lens.begin(), marray_lens.end(), ostream_iterator<int>(cout, " ")); cout << endl;
#endif//DEBUG_PRIMESCORE

#ifndef ITERCOMB
		// initialise the minima array which stores the minima of the hyper planes
		// corresponding to the target orientations
	node->min = vector<int>(marray_lens[0], std::numeric_limits< int >::max());
	int	minhp = std::numeric_limits< int >::max(),				// the minimal value of all hyper planes (i.e. the hypercube)
		maxhp = 0;
#endif//ITERCOMB

		// iterate over all (a)ssignment possibilities of the child nodes
	do{
#ifdef DEBUG_PRIMESCORE
		cout << "solving pos: "; copy(marray_pos.begin(), marray_pos.end(), ostream_iterator<int>(cout, " ")); cout << endl;
#endif//DEBUG_PRIMESCORE

		skip_subopt = false;
			// assign the current sign assignment to the ambiguous positions
			// only switch if necessary
		for(unsigned i=0; i<ambpos.size(); i++){
#ifndef ITERCOMB
			if ( node->children[ ambpos[i][0] ]->min[ marray_pos[i+1] ] == std::numeric_limits< int >::max() ){
				skip_subopt = true;
				break;
			}
#endif//ITERCOMB
			for(unsigned j=0; j<ambpos[i].size(); j++){
				if( rmp[j][ ambpos[i][j] ] < 0  && (marray_pos[i+1] & ppow( genomes.size()-1-j)) == INC )
					rmp[j][ ambpos[i][j] ] *= -1;
				if( rmp[j][ ambpos[i][j] ] > 0  && (marray_pos[i+1] & ppow( genomes.size()-1-j)) >= DEC )
					rmp[j][ ambpos[i][j] ] *= -1;
			}
		}

		if( allmed == ALL_MEDIAN || !skip_subopt ){
				// construct the oRMP to solve by reversing the input permutations
				// the use of tgtor is because marray_pos[0] stores in the case of
				// lin parent / root not the sign to test, but the access position
				// to the hypercube
			if(node->parent == NULL){
				tgtor = 0;
			}else if( node->parent->type == LIN ){
				tgtor = node->parent->signs;
			}else{
				tgtor = marray_pos[0];
			}

			for(unsigned k=0; k<genomes.size(); k++){
				urmp[k] = rmp[k];
				if((tgtor & ppow(genomes.size()-1-k)) >= DEC){
					reverse(urmp[k], 0, urmp[k].size()-1);
				}
			}
#ifdef DEBUG_PRIMESCORE
			cout << "oRMP"<<endl;
			for (unsigned i=0; i<urmp.size(); i++){
				cout << urmp[i]<<endl;
			}
#endif//DEBUG_PRIMESCORE
				// solve the rmp with the current sign assignments
#ifdef ADDTIME
			compute++;
#endif//ADDTIME
//			caprara(genomes, dynamic        , conserved, common, pair_pd, sig, circular, LB      , UB      , allmed, lazy   , mediancnt, medians )
			caprara(urmp   , 0 , 0        , 0     , 0      , 0  , 0       , NO_BOUND, NO_BOUND, allmed, NOTLAZY, mediancnt, lmedians);
			score = rd->calcsum(lmedians[0],urmp);
#ifdef DEBUG_PRIMESCORE
			cout << " -> "<< mediancnt << " medians of score "<< score<<":"<<endl;
			for(unsigned i=0; i<lmedians.size(); i++)
				cout << "       "<<i+1<<": "<<lmedians[i]<<endl;
#endif//DEBUG_PRIMESCORE

#ifndef ITERCOMB
	//			 get the minimal score sum (local score + best score (with the corresponding target orientation))
			for( unsigned i=0; i<ambpos.size(); i++ ){
				if( node->children[ambpos[i][0]]->type == PRI ){
					score += node->children[ ambpos[i][0]]->min[ marray_pos[i+1] ];
				}
			}
#ifdef DEBUG_PRIMESCORE
			cout << "score sum "<<score << endl;
#endif//DEBUG_PRIMESCORE
				 // check if the score is minimal
			if ( score < node->min[ marray_pos[0] ] ){
				node->min[ marray_pos[0] ] = score;
			}

			if ( score < minhp ){
				minhp = score;
			}
			if ( score > maxhp ){
				maxhp = score;
			}
#endif//ITERCOMB
		}// end of if skip / not
		else{
#ifdef ADDTIME
			skip++;
#endif//ADDTIME
#ifdef DEBUG_PRIMESCORE
			cout << "# skip "<<endl;
#endif//DEBUG_PRIMESCORE
			score = std::numeric_limits< int >::max();
		}

			// set the madians (regardless of ITERCOMB)
		marray_set( node->medians, marray_lens, marray_pos, lmedians );
			// set the score:
			// if ITERCOMB: the local score for the current sing assignments
			// else: the sum from the component leaves (the minima for the target orientation
			//     corresponding to the ambigous sign from the Pnode children )
		marray_set( node->scores, marray_lens, marray_pos, score);

			// compute the next sign assignment
			// if the 1st dimension (target orientation) is of length 1
			// i.e. only one orientation to test (lin parent / root)
			// only count up the ambigous signs, else also count up
			// the target orientations
		if( marray_lens[0] == 1){
			countt( 1, marray_lens, marray_pos);
		}else{
			countt( 0, marray_lens, marray_pos);
		}

		score = std::numeric_limits< int >::max();
		lmedians.clear();

			// compute only one sign combination if the bound should only be approximated
	}while( marray_pos[0] != std::numeric_limits< int >::max() );

#ifndef ITERCOMB
	// for each target orientation we can invalidate all ambigous sign combinations
	// which lead to a score (in combination with the target orientation) bigger
	// than the minima of the target orientation stored in the min array

	// if only one median should be computed we can set also the min array
	// (and subsequently the corresponding complete hyper plane) to std::numeric_limits< int >::max()
	// i.e. invalidate it -> this is evaluated in pnodes above leading to
	// a reduced number of oRMPs to solve. ( all combinations which include
	// the corresponding ambigous sign )
#ifdef DEBUG_PRIMESCORE
	cout << "min   "; copy(node->min.begin(), node->min.end(), ostream_iterator<int>(cout, " "));cout << endl;
	cout << "minhp "<<minhp<<" maxhp "<<maxhp<<endl;
#endif//DEBUG_PRIMESCORE

	int diff = 0;
	for( unsigned i=0; i<node->min.size(); i++ ){
			// sanity check: it is impossible that the minima differ by more than one
			// (if the scores are the bounds this does not hold)
		if ( abs( node->min[i] - minhp ) > 1 ){
			cerr << "tcip_pnode_medians: found difference > 1 in the minima"<<endl;
			cerr << "minhp "<<minhp<<" min "; copy(node->min.begin(), node->min.end(), ostream_iterator<int>(cerr, " ")); cerr << endl;
			exit(1);
		}

		diff += abs( node->min[i] - minhp );
		if ( allmed == ONE_MEDIAN && node->min[i] > minhp ){
			node->min[i] = std::numeric_limits< int >::max();
		}
	}
#ifdef DEBUG_PRIMESCORE
	if( allmed == ONE_MEDIAN  )
		cout << "min' "; copy(node->min.begin(), node->min.end(), ostream_iterator<int>(cout, " ")); cout << endl;
#endif
#ifdef ADDTIME
	if (node->min.size() > 1)
		cout << "#xidiff "<<node->children.size()<<" " <<4-diff<<endl;
#endif//ADDTIME

//	cout << " reset scores "<<endl;
	marray_pos.assign( marray_pos.size(), 0 );
	do{
//		cout << "pos "; copy(marray_pos.begin(), marray_pos.end(), ostream_iterator<int>(cout, " ")); cout << " ";
		score = marray_get( node->scores, marray_lens, marray_pos );
//		cout << "score "<<score <<" -> ";
		if(score > node->min[ marray_pos[0] ]){
			marray_set( node->scores, marray_lens, marray_pos, std::numeric_limits< int >::max());
//			cout << " std::numeric_limits< int >::max()"<<endl;
		}
//		else{
//			cout << score <<endl;
//		}
		countt( 0, marray_lens, marray_pos);
	}while( marray_pos[0] != std::numeric_limits< int >::max() );
//	cout <<" -> grey scores "; copy(node->scores.begin(), node->scores.end(), ostream_iterator<int>(cout, " ")); cout<< endl;
//	cout << endl;

#endif//ITERCOMB
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// component a vector containing all the P-nodes of the component
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int tcip_solve_component( vector<mpqnode *> &component, vector<vector<int> > &solution, vector<vector<int> > &ambpos,
	vector<vector<int> > &marrpos, vector<vector<int> > &marrlen, int allmed ){

	int score, 				// the score of a combination
		minscore = std::numeric_limits< int >::max();	// the score of the best found combinations

#ifdef ITERCOMB
	int	exit = 0;			// loop exit flag
#endif//ITERCOMB
	vector<int> s; 			// one solution candidate

	ambpos.clear();
	marrlen.clear();
	marrpos.clear();

#ifdef DEBUG_SOLVE_COMP
	cout << "solve component: ";
	for (unsigned i=0; i<component.size(); i++){
		cout << "["<<component[i]->i.first<<","<<component[i]->i.second<<"] ";
	}
	cout << endl;
#endif//DEBUG_SOLVE_COMP

		// initialise the solution vector (note that the first position s[0],
		// does not mean in (all cases) +++, it is just the one and only possibility )
	s = vector<int>( component.size(), 0);
		// initialise the multidimensional array - access vectors
	marrpos = vector<vector<int> >(component.size());
	marrlen = vector<vector<int> >(component.size());

		// get the ambigous positions (i.e. P-children) of each pnode in the component
		// - the ambpos vector stores for each component the ELEMENTS which are ambigous
		//   for each node the scores are stored in the multidimensional array 'scores'
		// - the marrpos specifies for each node the current access indices for 'scores'
		// - the marrlen specifies for each node the length of each dimension of 'scores'
		//   (always 4, but for the topmost node it is only 1)
	ambpos = vector<vector<int> >(component.size(), vector<int>());
	for(unsigned i=0; i<component.size(); i++){
		for(unsigned j=0; j<component[i]->children.size(); j++){
			if( component[i]->children[j]->type == PRI ){
				ambpos[i].push_back( j );
			}
		}
		marrpos[i] = vector<int>(ambpos[i].size()+1, 0);
		marrlen[i] = vector<int>(ambpos[i].size()+1, 4);
		if(i==0){
			marrlen[i][0]=1;
		}
	}

#ifdef ITERCOMB
		// test all possible combinations (i.e. test all possible sign assignment
		// to the edges (= 'target orientations' of the pnodes))
		// - the vector s stores the current 'target sign' assignment of the pnodes
		//   this is assigned to the nodes
		// - the signs of the ambigous children of the pnodes are implied (by the
		//   for the moment fixed) 'target signs' of the corresponding P-childs
		// - for each node the current marrpos is computed ( 'target sign', amb. sign 1, ... amb. sign k)
		//   and the score is taken from the multidim. array 'scores'
		//   -> and summed over all nodes over the component

		// - if the score is smaller than the best known solution(s)
		//   -> delete the solutions vector and set the new best score
		// - if its equally good (includes the case above)
		//   appen the current sign assignment s to the solutions vector

		// - get the next possible sign assignment (works like counting)

	do{
#ifdef DEBUG_SOLVE_COMP
		cout << "cand sol ";
		for( unsigned i=0; i<component.size(); i++ ){
			cout << s[i]<<" ";
		}
		cout << endl;
#endif//DEBUG_SOLVE_COMP
			// set the target orientations in the pnodes
		for( unsigned i=0; i<component.size(); i++ ){
			component[i]->signs = s[i];
		}
		score = 0;
			// get the score of the current assignment
		for(unsigned i=0; i<component.size(); i++){
			marrpos[i][0] = s[i];
			for(unsigned j=0; j<ambpos[i].size(); j++){
				marrpos[i][j+1] = component[i]->children[ ambpos[i][j] ]->signs;
			}
			score += marray_get( component[i]->scores, marrlen[i], marrpos[i] );
		}

		if(score <= minscore){
			if(score < minscore){
				solution.clear();
				minscore = score;
			}
			solution.push_back(s);
		}

			// compute the next solution candidate
		if( component.size() > 1 )
			s[1]++;
		for(unsigned i=1; i<s.size(); i++){
			if(s[i] < 4){
				break;
			}else{
				s[i] = 0;
				if(i+1 < s.size())
					s[i+1]++;
				else
					exit = 1;

			}
		}
	}while( component.size() > 1 && exit == 0);

	if( allmed == ONE_MEDIAN && solution.size() > 1){
		solution.erase( solution.begin()+1, solution.end() );
	}

#else
//	cout << "min ";copy(component[0]->min.begin(), component[0]->min.end(), ostream_iterator<int>(cout, " "));cout << endl;
	int curpnode = 0;	// the current pnode in the component vector
						// we want to start at the topmost -> 0

		// get the minimal solution of the topmost node (i.e. the minimal sum)
		// and init its sign accordingly (the sign is set to the first minima)
		// so other minima have to be selected later; we can iterate over min
		// because in in tcip_pnode_medians for the topmost node the currect
		// number of elements is stored in min (equal to the number of k-signs
		// to test, 1 for lin directed / 4 else )
		// note that if there is only one possibility (one element in min) this
		// does not have to correspond to (+++), but the sign of the parent
	for(unsigned i=0; i < component[0]->min.size(); i++ ){
		if( component[0]->min[i] < minscore ){
			minscore = component[0]->min[0];
			component[0]->signs = i;
		}
	}

	do{

//		cout << "curpnode "<<curpnode<<" sign "<< component[curpnode]->signs <<endl;

			// set the hyper plane to check (its the sign)
			// this has to be done only if the counter did not exceeded
			// the limit meanwhile
		if(marrpos[curpnode][0] != std::numeric_limits< int >::max()){
			marrpos[curpnode][0] = component[curpnode]->signs;
				// update the solution for the current node
			s[curpnode] = component[curpnode]->signs;
		}
			// get the next non std::numeric_limits< int >::max() (non minimal) score for the current target
			// orientation for the current pnode in the component (i.e. iterate over
			// the hyper plane of the multidim. array where the first pos (the
			// target orientation) is constant)

			// the loop goes on until we find a valid element or leave the hyper plane
			// if we have left the plane ->  all elements in the current pos vector are std::numeric_limits< int >::max()
		score = std::numeric_limits< int >::max();
		while( score == std::numeric_limits< int >::max() && marrpos[curpnode][0] != std::numeric_limits< int >::max() ){
				// get the score at the current position
			score = marray_get( component[curpnode]->scores, marrlen[curpnode], marrpos[curpnode] );
//			cout << "score "<<score<<endl;
				// if the score is equal to std::numeric_limits< int >::max() (invalid) we have to go on -> +1
				// if the the score is not std::numeric_limits< int >::max() ( this is what we are looking for )
				//    we don't have to count on because we need the position later (the next if
				//    outside the loop )
			if ( score == std::numeric_limits< int >::max() ){
				countt( 1, marrlen[curpnode], marrpos[curpnode]);
//				cout << "marrpos "; copy(marrpos[curpnode].begin(), marrpos[curpnode].end(), ostream_iterator<int>(cout, " "));cout << endl;
			}
		}

//		cout << "next minscore for curpnode "<<curpnode<<" is "<<score<<endl;
//		cout << "selected from ";
//		copy( component[curpnode]->scores.begin(), component[curpnode]->scores.end(), ostream_iterator<int>(cout, " ") ); cout << endl;
			// if we have found a valid entry in the hyper plane of the multidimensional
			// score array at the current target orientation
		if( score != std::numeric_limits< int >::max() ){
				// set the signs of the pnode children according to the sign choice
				// of the ambigous (p) children - which is the position in the mult. dim. array
			for ( unsigned i=0; i<ambpos[ curpnode ].size(); i++ ){
					// take care of: ambpos is one shorter than marrpos
				component[ curpnode ]->children[ ambpos[curpnode ][i] ]->signs = marrpos[curpnode][i+1];
			}
				// in the case that we find a valid entry in the plane (during the above loop)
				// we dont have incremented the marrpos .. therefore we have to do it here
				// because we dont want to check the same element in the next iteration of this loop
			countt( 1, marrlen[curpnode], marrpos[curpnode]);
//			cout << "marrpos "; copy(marrpos[curpnode].begin(), marrpos[curpnode].end(), ostream_iterator<int>(cout, " "));cout << endl;
				// go to the next node of the component
			curpnode++;
				// if we were allready at the last node
				// * get the solution stored in the pnodes
				// * and go back to the last node of the component
			if ( curpnode == (int)component.size() ){
				solution.push_back(s);
				if( allmed == ONE_MEDIAN ){
					break;
				}

				curpnode--;
			}
		}
			// if the iteration over the hyper plane did not find any more valid scores
			// -> go to the previous node of the component
		else{
				// reset the position array to 0, because we want to iterate again over all possible
				// combinations when we get back to this node later (ofcourse), the first element (target orient
				// will be set at the beginning of the loop, so we don't have to do it here)
			marrpos[curpnode].assign(ambpos[0].size()+1, 0);

			// in the root node we have to go to the next hyper plane (target orientation)
			// which leads to a minimal score sum (in the linear undirected case there won't
			// be any more because we only have to check 1 sign)
			// (this must not be done in (none-pcomponent-root nodes) because their target
			// orientation is fix, i.e. implied by a ambigous sign of the parent)
			if( curpnode == 0 ){
				component[0]->signs++;
				while( component[0]->signs < (int) component[0]->min.size() ){
					if( component[0]->min[ component[0]->signs ] == minscore ){
						break;
					}
					component[0]->signs++;
				}

				// if we have checked all planes of the root node, decrease curpnode
				// -> this well terminate the loop
				if( component[0]->signs >= marrlen[0][0] ){
					curpnode--;
				}
			}else{
				curpnode--;
			}
		}
	}while( curpnode >= 0 );
#endif//ITERCOMB

	return minscore;
}
