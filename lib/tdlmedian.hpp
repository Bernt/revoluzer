/*
 * tdl_median.hpp
 *
 *  Created on: May 13, 2009
 *      Author: maze
 */

#ifndef TDL_MEDIAN_HPP_
#define TDL_MEDIAN_HPP_

#include "chains.hpp"
#include "median.hpp"
#include "tdl.hpp"

class tdl_bbmedian : public mediansolver{
private:

public:

	/**
	 * constructor
	 * @param[in] hd helping memory
	 */
	tdl_bbmedian( );

	/**
	 * return the tdl distance distance of g1 and g2,
	 * you dont have to pass genomes this is not needed here
	 * @see mediansolver::distance
	 */
	unsigned distance( const genom &g1, const genom &g2, const vector<genom> &genomes = vector<genom>() );

	/**
	 * the output operation
	 * @see mediansolver::output
	 */
	virtual ostream & output( ostream &os ) const;

	/**
	 * solve a reversal median problem
	 * @see mediansolver::solve
	 */
	void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
			int &mediancnt, int &score, vector<genom> &medians);
};

class tdl_bfmedian : public mediansolver{
private:
public:

	/**
	 * constructor
	 * @param[in] hd helping memory
	 */
	tdl_bfmedian( );

	/**
	 * return the tdl distance distance of g1 and g2,
	 * you dont have to pass genomes this is not needed here
	 * @see mediansolver::distance
	 */
	unsigned distance( const genom &g1, const genom &g2, const vector<genom> &genomes = vector<genom>() );

	/**
	 * the output operation
	 * @see mediansolver::output
	 */
	virtual ostream & output( ostream &os ) const;

	/**
	 * solve a reversal median problem
	 * @see mediansolver::solve
	 */
	void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
			int &mediancnt, int &score, vector<genom> &medians);
};





#endif /* TDL_MEDIAN_HPP_ */
