/*
 * baderrt.cpp
 *
 *  Created on: May 5, 2014
 *      Author: maze
 */

#include "baderrt.hpp"

#include <regex.h>
#include <sstream>
#include <stdlib.h>


namespace std {

baderrt::baderrt() {
	// TODO Auto-generated constructor stub

}

baderrt::baderrt( const genom &g1, const genom &g2, const costfoo *cst, bool ipt, bool circular ){

	int wr, wt; // weights passed to the functions of M. Bader
	rrrmt *inv, *tra, *itra;
	stringstream ss;
	string s, line;

	cerr <<"===="<<endl<< g1 << endl<< g2<<endl;

	// determine weights
	inv = new rev(0, 1, g1);
	tra = new transp(0,1,2, g1);
	itra = new revtransp(0,0,1, 1, g1);

	wr = (int)round((*cst)[ inv ]);
	wt = (int)round((*cst)[ tra ]);

	if( (*cst)[ inv ] - wr != 0.0 ){
		cerr << "warning: using rounded inversion weight for Bader's functions"<<endl;
	}

	if( (*cst)[ tra ] - wt != 0.0 ){
		cerr << "warning: using rounded transposition weight for Bader's functions"<<endl;
	}

	if( (*cst)[ tra ] != (*cst)[ itra ] ){
		cerr << "warning: got != weights for transp. and inverse transp. -> using transposition weight"<<endl;
	}

	delete inv;
	delete tra;
	delete itra;

	weightedbb::WeightedBB *_weightedbb = new weightedbb::WeightedBB();
	_weightedbb->setHeapLimit(250000000 / (g1.size() +1));
	_weightedbb->setWeights(wr, wt, (ipt)?1:0 );

	genom _gg,
		_hh;

	_gg = genom( g1.size(), 0 );
	_hh = g1.identify_g( g2 );
//	_gg = g1;
//	_hh = g2;

	if( ! circular ){
		_gg.push_back( _gg.size()+1 );
		_hh.push_back( _hh.size()+1 );
	}

	cerr << _gg << endl;
	cerr << _hh << endl;

	_weightedbb->setPermutations(_gg.size(), _gg.get_pointer(), _hh.get_pointer());
	_weightedbb->sort( circular );

	// get the string output of the function of M. Bader
	_weightedbb->printResult( 1, ss );
//	s = ss.str();
//	ss.str(s);
	int i,j,k;
	regex_t rerev, retra, reitra;
	regmatch_t matchptr[4];

	genom tmpg = _gg;
	rrrmt *tmpr = NULL;

    if( regcomp(&rerev, "rev(\\([0-9]\\+\\), \\([0-9]\\+\\))", 0) ){
    	cerr <<"Could not compile regex rerev"<<endl;
    	exit(EXIT_FAILURE);
    }

    if( regcomp(&retra, "t(\\([0-9]\\+\\), \\([0-9]\\+\\), \\([0-9]\\+\\))", 0) ){
    	cerr <<"Could not compile regex retra"<<endl;
    	exit(EXIT_FAILURE);
    }

    if( regcomp(&reitra, "inv t(\\([0-9]\\+\\), \\([0-9]\\+\\), \\([0-9]\\+\\))", 0) ){
    	cerr <<"Could not compile regex retra"<<endl;
    	exit(EXIT_FAILURE);
    }

//    cerr << "START" << " -> "<< tmpg<< endl;

	while (getline(ss, line)) {

		cout <<"line"<< line << endl;
		//	"     rev(A, B) "; // A B A -> A -B A
		if( !regexec(&rerev, line.c_str(), 3, matchptr, 0) ){
			i = atoi( line.substr(matchptr[1].rm_so, matchptr[1].rm_eo - matchptr[1].rm_so).c_str() );
			j = atoi( line.substr(matchptr[2].rm_so, matchptr[2].rm_eo - matchptr[2].rm_so).c_str()  );
			tmpr = new rev( i-1, j-2, tmpg );
        }
		//	"inv t(A, B, C) ";// general case of inverted transposition (use for human readable output only)
		else if( !regexec(&reitra, line.c_str(), 4, matchptr, 0) ){
			i = atoi( line.substr(matchptr[1].rm_so, matchptr[1].rm_eo - matchptr[1].rm_so).c_str() );
			j = atoi( line.substr(matchptr[2].rm_so, matchptr[2].rm_eo - matchptr[2].rm_so).c_str()  );
			k = atoi( line.substr(matchptr[3].rm_so, matchptr[2].rm_eo - matchptr[3].rm_so).c_str()  );

//			tmpr = new revtransp(i-1, j-2, k-1, tmpg);
			tmpr = new revtransp(j-1, k-2, i-1, tmpg);
		}
		//	"    t(A, B, C) ";// A B C A -> A C B A
		else if( !regexec(&retra, line.c_str(), 4, matchptr, 0) ){
			i = atoi( line.substr(matchptr[1].rm_so, matchptr[1].rm_eo - matchptr[1].rm_so).c_str() );
			j = atoi( line.substr(matchptr[2].rm_so, matchptr[2].rm_eo - matchptr[2].rm_so).c_str()  );
			k = atoi( line.substr(matchptr[3].rm_so, matchptr[2].rm_eo - matchptr[3].rm_so).c_str()  );
			tmpr = new transp(i-1, j-1, k-2, tmpg);
        }

		if( tmpr != NULL ){
			tmpr->apply( tmpg );
			cerr << *tmpr << " -> "<< tmpg<< endl;
			sce.push_back( tmpr->clone() );
			delete tmpr;
			tmpr = NULL;
		}


		//	"tra(A, B, C) ";// A B C A -> A C -B A
		//	"trb(A, B, C) ";// A B C A -> A -C B A
		//	"rr(A, B, C) ";// A B C A -> A -B -C A
	}

    regfree(&rerev);
    regfree(&retra);
    regfree(&reitra);

	delete _weightedbb;
}

baderrt::~baderrt() {
	// TODO Auto-generated destructor stub
}

} /* namespace std */
