/*
 * transpdist.cpp
 *
 *  Created on: Jul 2, 2009
 *      Author: maze
 */

#include <iterator>

#include "dstnc_rt.hpp"

using namespace std;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

dstnc_rt::dstnc_rt( unsigned n, bool circular, unsigned wr, unsigned wt,
		bool transp ) {

	if( transp ){
		_wr = _wt = 1;
	}else{
		_wr = wr;
		_wt = wt;
	}
	_n = n;
	_circular = circular;
	_weightedbb = new weightedbb::WeightedBB();
	_weightedbb->setHeapLimit(250000000 / n );
	_weightedbb->setWeights(_wr, _wt, (transp)?0:1 );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

dstnc_rt::~dstnc_rt() {
	delete _weightedbb;
	_gg.clear();
	_hh.clear();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void dstnc_rt::adapt( unsigned n ){
	_n = n;
	_weightedbb->setHeapLimit(250000000 / n );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned dstnc_rt::calc( const genom &g, const genom &h ){
	_gg = g;
	_hh = h;

//	copy(_gg.chromosom.begin(), _gg.chromosom.end(), ostream_iterator<int>(cout," ")); cout << endl;
//	copy(_hh.chromosom.begin(), _hh.chromosom.end(), ostream_iterator<int>(cout," ")); cout << endl;

	if( ! _circular ){
		_gg.push_back( _gg.size()+1 );
		_hh.push_back( _hh.size()+1 );
	}

//	copy(_gg.begin(), _gg.end(), ostream_iterator<int>(cout," ")); cout << endl;
//	copy(_hh.begin(), _hh.end(), ostream_iterator<int>(cout," ")); cout << endl;
	_weightedbb->setPermutations(_gg.size(), _gg.get_pointer(), _hh.get_pointer());
	_weightedbb->sort( this->_circular );

	return _weightedbb->getDistance();

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

dstnc* dstnc_rt::clone() const{
	return new dstnc_rt( _n, _circular, _wr, _wt, _transp );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ostream & dstnc_rt::output(ostream &os) const{
	os << "M. Bader's reverse transposition distance(wr: "<<_wr<<", wt: "<<_wt<<", t:"<<_transp;
	return os;
}


