/**
 * chains.cpp
 *
 * @author: Matthias Bernt
 */

#include <cstdlib>
#include <iostream>

#include "chains.hpp"

using namespace std;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

chains::chains(int n, vector<int> *srcinv, vector<int> *tgtinv,
		const genom *tgt){

	_n = n;

	_cnt = 1;
	_connect = vector<bool>(n-1, true);

	_srcinv = srcinv;
	_tgtinv = tgtinv;
	_tgt = tgt;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned chains::cnt( ) const{
	return _cnt;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ostream& operator<<(ostream& out, const chains &c){

	for( unsigned i=0; i<c._tgt->size(); i++ ){
		out << (*c._tgt)[i];
		if( i<(c._tgt->size()-1) ){
			if( c._connect[i] == true )
				out << "=";
			else
				out << " ";
		}
	}

	return out;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void chains::update(int e){

	int df;
	e = abs(e);
	// let d and f be the the neighbouring elements to the left and right of e
	// in the target permutation, i.e. ... d e f ...

	// handle d e
	if( (*_tgtinv)[e] > 0){
		df = abs((*_tgt)[ (*_tgtinv)[e]-1 ]);
		update( df, e);
	}

	// handle e f
	if( (*_tgtinv)[e] < _n-1){
		df = abs((*_tgt)[ (*_tgtinv)[e]+1 ]);
		update( e, df);
	}
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void chains::update( int d, int e){

	// d left of e in the source permutation
	// -> make connection, decrease chain count
	if( (*_srcinv)[ d ] <= (*_srcinv)[e] ){
		if( !_connect[ (*_tgtinv)[d] ] )
			_cnt--;

		_connect[ (*_tgtinv)[d] ] = true;
	}
	// a) d right of e ((*_srcinv)[ d ] > (*_srcinv)[ e ])
	// b) d and e not placed yet (*_srcinv)[ d ] >= (*_srcinv)[ e ]
	//    (both should be set to INT_MAX)
	// -> remove connection, increase chain count
	else {
		if( _connect[ (*_tgtinv)[d] ] )
			_cnt++;

		_connect[ (*_tgtinv)[d] ] = false;
	}
}
