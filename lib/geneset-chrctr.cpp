#include <algorithm>
#include <cstdlib>
#include <limits>

#include "geneset-chrctr.hpp"

interval_splitsystem::interval_splitsystem( const vector<genom> &genomes,
		SgnHandType sgnhnd, bool circular, bool complement, unsigned &maxsize, unsigned &minobs,
		bool quiet,	split_weight *wfoo){

	map<geneset, unsigned > cmx;	// index of the observed gene sets
									// (maps to the row index in cm)
	map<split, unsigned > sidx;	// index of the constructed splits
									// (maps to index in _splits)
	map<split, unsigned >::iterator cit;

	unsigned cnt=0,	// number of processed gene sets (sum over all genomes)
		cnstcnt=0,	// number of charactrers
		infcnt=0,	// .. infomative characters
		uinfcnt=0;  // .. uninformative characters
	set<geneset> gs;	// the gene sets of a genomecircular
//	split tmp;
	vector<vector<bool> > cm;	// the character matrix; columns correspond to
								// species; each line represents one character

	// set split weight function
	if( wfoo != NULL ){
		_splweightfoo = wfoo;
	}else{
		cerr << "error in interval_splitsystem(): NULL weight function"<<endl;
		exit(EXIT_FAILURE);
	}

	if( maxsize == std::numeric_limits< unsigned >::max() ){
		maxsize = genomes[0].size() - 1 ;
		cerr << "# automatically calculated maxsize: "<<maxsize<<endl;
	}

//	// if the complementary intervals are to be computed the set of all
//	// genes needs to be known
//	if( circular && !complement ){
//		gs.insert( genomes[0].chromosom.begin(), genomes[0].chromosom.end() );
//	}

	// init the original characters, i.e. one character per observed gene set
	for(unsigned i=0; i<genomes.size(); i++){
		// get the gene sets defined by the i-th genome
//		cerr << "G "<<i<<endl;
		gs.clear();
		getgenesets(genomes[i], genomes[i].size(), circular, complement, sgnhnd, maxsize, false, inserter(gs, gs.begin()));


		// each genome should define the same number of gene sets
		if( i*gs.size() != cnt ){
			cerr << "error: differing gene set sizes"<<endl;
			exit(EXIT_FAILURE);
		}
		cnt += gs.size();

		// insert the gene sets in the map cmx (mapping gene sets to binary
		// representation of splits). since cmx and and gene set are sorted in
		// the same way (i.e. geneset::operator<) we can start the search
		// for the next gene set at the position in cmx where the current
		// gene set was found. should be more efficient than starting at
		// cmx.begin() each time.
		map<geneset, unsigned>::iterator mi=cmx.begin(),
				lmi=cmx.begin();
		for( set<geneset>::const_iterator it=gs.begin(); it!=gs.end(); it++ ){
			while( mi != cmx.end() && ((mi->first) < (*it)) ){
				lmi = mi;
				mi++;
			}

			// not found -> insert an entry in cmx and an empty binary
			// representation in cm
			if( mi == cmx.end() || *it < mi->first ){
				mi = cmx.insert(lmi, make_pair(*it, cm.size()) );
				cm.push_back( vector<bool>( genomes.size(), false ) );
			}
			// add the species to the split defined by the gene set
			cm[mi->second][ i ] = true;
		}
//		cerr << cmx.size()<<endl;
	}

	if( !quiet ){
		cerr << "# "<<cnt << " gene sets processed, i.e. "<< gs.size() <<" per genome" <<endl;
		cerr << "# "<<cm.size()  << " unique gene sets "<<endl;
	}
	minobs = gs.size();

	// add the (unique) splits with their corresponding gene sets
	for( map<geneset, unsigned>::const_iterator it=cmx.begin();
			it != cmx.end(); it++){

		// it->first = set
		// it->second= index of the character in cm

		// construct the split
		split tmp = split( cm[it->second] );

		// check if the split is already in the set of splits
		// add the gene set to the the split if not
		cit = sidx.find( tmp );
		if( cit == sidx.end() ){
			cit = sidx.insert( sidx.begin(), make_pair( tmp, _splits.size() ) );
			_splits.push_back( tmp );
		}
		_splits[ cit->second ].addsupport( &(it->first), cm[it->second][0] );
	}

	_size = genomes.size();

	for( unsigned i=0; i<_splits.size(); i++ ){
//		cerr << character[i] << " : ";
//		vector< vector<unsigned> > o = character[i].getobservation( false );
//		cerr << "("; for( unsigned j=0; j<o.size(); j++ ){copy(o[j].begin(), o[j].end(), ostream_iterator<unsigned>(cerr," ")); cerr<<",";} cerr<<")";
//		o = character[i].getobservation( true );
//		cerr << "("; for( unsigned j=0; j<o.size(); j++ ){copy(o[j].begin(), o[j].end(), ostream_iterator<unsigned>(cerr," ")); cerr<<",";} cerr<<")"<<endl;

		if( _splits[i].constant() ){
			cnstcnt++;
		}else if( _splits[i].informative() ){
			infcnt++;
		}else{
			uinfcnt++;
		}
	}

	if( !quiet ){
		cerr << "# "<<_splits.size() << " "<< infcnt<<" " <<cnstcnt << " " << uinfcnt << " characters (sum,informative,constant,uninformative) "<<endl;
	}

	random_shuffle(_splits.begin(), _splits.end());

	// set the normalisation constant of the splits to the sum of the support for the splits
	_splweightfoo->set_norm( supportcnt() );
}

//vector<chrctr<geneset> > init_characters( const vector<genom > &genomes,
//		bool circular, bool complement, unsigned &maxsize, unsigned &minobs, bool quiet ){
//
//
//}
