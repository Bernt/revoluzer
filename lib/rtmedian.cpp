/*
 * rtmedian.cpp
 *
 *  Created on: Dec 18, 2008
 *      Author: maze
 */

#include <cstdlib>
#include <getopt.h>

#include "rtmedian.hpp"

// FMODE 0: reversal 1: transposition 2: reversal+transposition
#define FMODE 2
// EXACT
#define EXACT false

#define WRDEFAULT 1
#define WTDEFAULT 2

using namespace std;
using namespace median;

rtmedian::rtmedian( int n, int argc, char *argv[] ) {
	int wr = WRDEFAULT,
		wt = WTDEFAULT,
		c = 0;

	while (1) {
		static struct option long_options[] = {
            {"rtmedwr",    required_argument, 0, 'R'},
            {"rtmedwt",    required_argument, 0, 'T'},
            {0, 0, 0, 0}
		};
        int option_index = 0;			// getopt_long stores the option index here.
        c = getopt_long (argc, argv, "R:T:",long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
        	break;

        switch (c){
			case 0: // If this option set a flag, do nothing else now.
				if (long_options[option_index].flag != 0)
					break;
				cout << "option "<< long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'R':
				wr += atoi(optarg);
				break;
			case 'T':
				wt += atoi(optarg);
				break;
			case '?':
				cout << "?"<<endl;
				break; /* getopt_long already printed an error message. */
        }
	}

	_med.init( n, wr, wt, FMODE, EXACT );
	_n = n;
}

unsigned rtmedian::distance( const genom &g1, const genom &g2, const vector<genom> &gemomes ){
	return _med.pairDist( g1.get_pointer(), g2.get_pointer() );
}

ostream &rtmedian::output( ostream &os ) const {
	os << "Reversal+Transposition Solver (M. Bader)";
	return os;
}

void rtmedian::printopt() const{
	cout << "rtmedian options:"<<endl;
	cout << "   --rtmedwr WEIGTH: reversal weight (default: "<< WRDEFAULT <<")"<<endl;
	cout << "   --rtmedwt WEIGTH: reversal weight (default: "<< WTDEFAULT <<")"<<endl;
}

void rtmedian::solve(const vector<genom> &problem, bool allmed,
		int lower_bound, int upper_bound, int &mediancnt,
		int &score, vector<genom> &medians ){

	int *tmedian;
	int status;

	tmedian = new int[ _n ];
	mediancnt = 0;
	cout << "=== rtmedian::solve ==="<<endl;
	cout << problem << endl;

	if( allmed ){
		/* @todo assumtion median is found .. maybe not true wehen upperbound is set in the future */
		score = status = _med.getFirstSolution(  problem[0].get_pointer(), problem[1].get_pointer(), problem[2].get_pointer(), tmedian );
		while( status != -1 ){
			medians.push_back( genom(tmedian, _n, 0, problem[0].get_nmap() ) );
			mediancnt++;
			cout << mediancnt << " : "<< medians.back() <<endl;
			cout << "get next "<<endl;
			status = _med.getNextSolution( tmedian );
			cout << "end get next "<<endl;
		}
		_med.cleanHeap();
	}else{
		/* @todo set upperbound parameter;
		 * assumtion median is found .. maybe not true wehen upperbound is set in the future */
		score = _med.solve( problem[0].get_pointer(), problem[1].get_pointer(), problem[2].get_pointer(), tmedian );
		medians.push_back( genom(tmedian, _n, 0, problem[0].get_nmap() ) );
		mediancnt++;
	}
	delete[] tmedian;
}

