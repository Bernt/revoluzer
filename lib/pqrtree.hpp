/**
 * PQR tree (and node) class
 * author: Matthias Bernt
 */


#ifndef PQRTREE_H
#define PQRTREE_H

#include <iostream>
#include <iterator>
#include <algorithm>
#include <limits>
#include <list>
#include <set>
#include <vector>
#include <queue>
#include <map>

//#include "genom.hpp"
#include "helpers.hpp"

using namespace std;

//#define DEBUGPQRPQR
//#define DEBUGPQRPQR_LABEL
//#define DEBUGPQRPQR_EXPAND

enum NodeType { LEAF = 1, PNODE = 2, QNODE = 3, RNODE = 4 };

enum LevelType { EMPTY = 1, PARTIAL = 2, FULL = 3 };

template<typename T>
class pqrnode
{
	private:
			// parent node
		pqrnode *parent;
			// the child nodes
		vector< pqrnode* > childs;

			// node type: P,Q, or R node
		NodeType type;
			// node mark: full, empty, or partial
		LevelType label;
			// the value of the (leaf) node
		T value;

	public:

	/**
	 * constructor
	 * @param[in] v the value
	 * @param[in] t the type
	 * @param[in] p the parent (default NULL)
	 * @param[in] l the label (default EMPTY)
	 */
	pqrnode(const NodeType &t, const T &v, pqrnode *p = NULL,
			const LevelType &l = EMPTY);

	/**
	 * copy constructor for a node
	 * @param[in] org the node to copy
	 */
	pqrnode( const pqrnode &org );

	/**
	 * destructor
	 */
	~pqrnode();

	/**
	 * append a new pointer to the child list
	 * @param[in] c the new child
	 */
	void append_child( pqrnode *c );

	/**
	 * append the constraints implied by p/q nodes in the subtree to the set
	 * @param[out] constr the constraints
	 * @return the elements in the leaves of the subtree
	 */
	set<T> append_constraints(vector<set<T> > &constr);

	/**
	 * check if the parent pointers are correct
	 */
	pqrnode *check();

	/**
	 * clear the child vector (but dont delete the pointers)
	 */
	void clear_childs();

	/**
	 * collapse the leaf children with values x1 and x2. the node has to be a QNODE.
	 * x1 = 2x-1 and x2 = 2x;
	 * @param[in] x1 2x-1
	 * @param[in] x2 2x
	 * @return the value of collapsed node
	 */
	int collapse( const T &x1, const T &x2 );

	/**
	 * remove the 0 and 2n+1 from the tree
	 * @param[in] tnp1 2n+1
	 */
	void collapse_root(int tnp1);

	/**
	 * count the number of nodes of a given type in the subtree
	 * @param[in] n the node type
	 * @return the number
	 */
	unsigned cnt_ndtype( NodeType n );

	/**
	 * expand the leaves in the subtree ..
	 */
	void expand();

	/**
	 * get the front of the subtree
	 * @param[out] p the front
	 */
	void front( vector<T> &p ) const;

	/**
	 * get the full, partial and empty childs of a node
	 * @param[out] f the full childs
	 * @param[out] p the partial childs
	 * @param[out] e the empty childs
	 */
	void get_fpe( vector<pqrnode* > &f, vector<pqrnode* > &p, vector<pqrnode* > &e );

	/**
	 * @return the parent
	 */
	char get_label();

	/**
	 * return the pointers to the leafs
	 * @param[out] leafs the leafs
	 */
	void get_leafs( map<T, pqrnode *> &leafs );

	/**
	 * @return the parent
	 */
	pqrnode* get_parent();

	/**
	 * @return the type
	 */
	int get_type() const;

	/**
	 * @return the value
	 */
	int get_value();

	/**
	 * check if there is a node of a given type in the subtree
	 * @return true if there is such a node, else false
	 */
	bool has_ndtype( NodeType n );

	/**
	 * check if a node is a leaf node
	 * @return true if is a leaf, else false
	 */
	bool is_leaf() const;

	/**
	 * determine the label empty, partial, full depending on its childs
	 */
	pqrnode* label_and_getlca();

	/**
	 * node c has to be a child of the called node. then the children of c
	 * are moved to be children of the called node.
	 *
	 * note this invalidated f,p,e of the calling node
	 * @param[in] c the child of the current node to be merged
	 */
	void merge_into_lca( const pqrnode *c );

	/**
	 * move the full and partial childs to the begining of calling (partial) child v.
	 * the new root is the last in the partial child list
	 *
	 * @param[in] the current considered partial child of the lca
	 * @param[in] f the full childs
	 * @param[in] p the partial childs (not containing v)
	 * @param[in] e the empty nodes
	 * @return a pointer to the new lca
	 */
	pqrnode* move_children_away( pqrnode *v, vector<pqrnode*> &f, vector<pqrnode*> &p, vector<pqrnode*> &e );

	/**
	 * output fuction, the names of the genes are taken from nmap
	 * @param nmap integer to gene name map
	 * @param os the stream to write into
	 */
	ostream & named_output(vector<string> *nmap, ostream &os) const;

	/**
	 * modifies the pqrtree to a normalised representation
	 * - Q nodes are reversed if the smallest leaf below the first childs
	 *   is larger than the smallest leaf below the last child
	 * - P & R nodes: the child order is transformed such that the child
	 *   including the smallest leaf is the first child
	 * @return the value of the smallest leaf in the subtree (used for
	 * recursive calls
	 */
	T normalise();

	/**
	 * recursively get the permutations which are consistent with the pqrtree
	 * @param[in] root the root node of the subtree
	 * @param[in] directed for the root of the tree: position of first child is constant
	 * @return the set of allowed permutations of the leaves below root
	 */
//	void permutations( vector<vector<T> > &p, pqrnode* root );
	vector<vector<T> > permutations( bool directed ) const;
//	void permutations( vector<genom> &p, pqrnode* root );

	/**
	 * get the number of permutations consistent with the pqrtree
	 * @param[in] directed first element fix
	 * @param[out] count the number of permutations
	 */
	void permutations_count(unsigned &count, bool directed ) const;

	/**
	 * template: prepare lca
	 * - take the black child nodes and group them in a new node (if only one black node -> noop)
	 * - f only contains the new node afterwards
	 *
	 * @param[in,out] f the full childs of the node -> empty afterwards
	 * @param[in] p the partial children of the node
	 * @param[in] e the empty children of the node
	 */
	void prepare_lca( vector<pqrnode* > &f, const vector<pqrnode* > &p, const vector<pqrnode* > &e );

	/**
	 * apply the templates
	 * @param[out] newlca the new lca
	 * @return true if the tree can be reduced to a pqtree without Rnodes, else false
	 */
	bool reduce(pqrnode** newlca);

	/**
	 * recursively reverse the order of the childrens
	 */
	void reverse();

	/**
	 * reverse the lca
	 * @param[in] cc the currently processed child node
	 */
	 void reverse_lca(pqrnode *cc);

	/**
	 * reverse a q node if its first child is 'emptier' than it's last child
	 */
	void reverse_qnode();

	/**
	 * reset the state of the node and recurse in the children
	 */
	void reset();

	pair<T,T> todot(ostream &os, vector<string> *nmap = NULL);

	/**
	 * transform a P node into a Q node
	 */
	void transform_pq();

	/**
	 * transform all prime nodes with 2 children into q nodes
	 */
	void transform_2childpq();

	/**
	 * set the label of a node
	 * @param[in] l the new label
	 */
	void set_label( LevelType l );

	/**
	 * set the parent
	 * @param[in] p the parent
	 */
	void set_parent( pqrnode *p  );

	/**
	 * set the type
	 * @param[in] t the new type
	 */
	void set_type( const NodeType &t );

	/**
	 * set the value
	 * @param[in] v the new value
	 */
	void set_value( const T &v );

	/**
	 * get the number of nodes in the subtree
	 * @param[in] leaf count leafs?
	 * @return number of nodes in subtree
	 */
	unsigned size( bool leaf ) const;

	/**
	 * recursively output a node and its childs
	 */
	template<class U>
	friend ostream & operator<<(ostream &os, const pqrnode<U> &g);

	pqrnode& operator= (const pqrnode& in);
};

template<typename T>
class pqrtree
{
	private:
		pqrnode<T> *root;	// the root of the tree
		map<T,pqrnode<T>* > leafs;	// the leaves of the tree

	public:

	/**
	 * empty constructor
	 */
	pqrtree();

	/**
	 * constructor - constructs a universal pqtree
	 * @param[in] S the set
	 * @return the pqr tree
	 */
	pqrtree(const set<T> &S);

	/**
	 * constructor - constructs a universal pqtree with leaves s to e
	 * @param[in] s
	 * @param[in] e
	 * @return the pqr tree
	 */
	pqrtree(T s, T e);

	/**
	 * constructor - constructs a tree with a linear root with the leafs as childs
	 * @param[in] S the set
	 * @return the pqr tree
	 */
	pqrtree(const vector<T> &S);


	/**
	 * copy constructor
	 * @param[in] orig the tree to copy
	 */
	pqrtree( const pqrtree<T> &orig );

	/**
	 * default destructor
	 */
	~pqrtree();

	/**
	 * check the parent pointers of the nodes in the tree
	 */
	void check();

	/**
	 * delete everything from the pqrtree
	 */
	void clear();

	/**
	 * collapse a tree, i.e. make it signed. this is the counterpart to expand
	 * @param[in] tnp1 2n+1
	 */
	void collapse(int tnp1);

	/**
	 * count the number of nodes of a given type in the tree
	 * @param[in] n the node type
	 * @return the number
	 */
	unsigned cnt_ndtype( NodeType n );

	/**
	 * get all subsets of i which are blocking with respect to the given tree
	 * @param[in] i the intervals
	 * @return the blocking subsets
	 */
	vector<set<T> > get_blocking_subsets(const vector<set<T> > &i );


	/**
	 * append the contrainst implied by p/q nodes to the vector
	 */
	void append_constraints( vector<set<T> > &constraints);

	/**
	 * check if there is a node of a given type in the subtree
	 * @return true if there is such a node, else false
	 */
	bool has_ndtype( NodeType n );

	/**
	 * expand a tree, i.e. make it unsigned
	 */
	void expand();

	/**
	 * label the nodes as empty, partial, or full,
	 * and return the lca of the full leaves
	 * @param[in] s the set
	 * @return pointer to the lca of the full leaves
	 */
	template<class _InputIterator>
	pqrnode<T> *label(const _InputIterator &s, const _InputIterator &e);

	/**
	 * output fuction, the names of the genes are taken from nmap
	 * @param nmap integer to gene name map
	 * @param os the stream to write into
	 */
	ostream & named_output( vector<string> *nmap, ostream &os) const;

	/**
	 * create a normalised pqrtree, s.t. the smallest leaf in the subtree
	 * of the first child of a node is smaller than:
	 * - the smallest leaf below the last child (Qnode)
	 * - the smallest leaf below any other child (P/Rnode)
	 */
	void normalise(  );

	/**
	 * get one permutation consistent with the pq tree
	 * @return a permutation
	 */
	vector<T> permutation() const;

	/**
	 * insert the permutations consistent with the pqrtree into the set
	 * @param[out] p the set where the permutations should be appended
	 * @param[in] directed the first leaf is the first element of all permutations
	 */
	void permutations( vector<vector<T> > &p, bool directed ) const;

//	void permutations( vector<genom> &p );
	/**
	 * get the number of permutations consistent with the pqrtree
	 * UINT_MAX may be returned if it is more then unsigned can represent
	 * @param[in] directed directed permutation, i.e. first elelemt fix?
	 * @return the number
	 */
	unsigned permutations_count( bool directed) const;

	void print_leaves();

	/**
	 * reduce the pqrtree with the set \f$ x,y \f$
	 * @param[in] x x
	 * @param[in] y y
	 * @return true iff the tree can be reduced to a PQ tree without R nodes
	 */
	bool reduce( T x, T y );

	/**
	 * apply a new set of restrictions, the set with in the range [b:e] will
	 * be consecutive afterwards
	 * @param[in] b the begin of the range
	 * @param[in] e the end of the range
	 * @return true if the tree can be reduced to a PQ tree without R nodes, else false
	 */
	template<class _InputIterator>
	bool reduce(const _InputIterator &b, const _InputIterator &e);

	/**
	 * apply sets of restrictions
	 * @param[in] sets the restrictions
	 * @return true if the tree can be reduced to a PQ tree without R nodes, else false
	 */
	bool reduce(const vector<set<T> > &sets);

	/**
	 * reduce the tree with all 'neutral' sets
	 * @param[in] sets the initial sets
	 */
	void reduce_with_neutral(const vector<set<T> > &sets);

	/**
	 * print the tree
	 */
	template<class U>
	friend ostream & operator<<(ostream &os, const pqrtree<U> &t);

	pqrtree& operator= (const pqrtree<T>& in);

	/**
	 * reset the state of the nodes
	 */
	void reset();

	/**
	 * get the number of nodes in the subtree
	 * @param[in] leaf count leafs?
	 * @return number of nodes in the subtree
	 */
	unsigned size( bool leaf ) const;

	void todot(ostream &os, vector<string> *nmap=NULL);
};

// *****************************************************************************
// constructor
// *****************************************************************************
template<typename T>
pqrnode<T>::pqrnode(const NodeType &t, const T &v, pqrnode *p, const LevelType &l ) : parent (p), type (t), label (l), value (v) {}

// *****************************************************************************
// copy constructor for a pqrnode
// *****************************************************************************
template<typename T>
pqrnode<T>::pqrnode( const pqrnode &org ){

	parent = NULL;
	type = org.type;
	label = org.label;
	value = org.value;

	for(unsigned i=0; i<org.childs.size(); i++){
		pqrnode *c = new pqrnode( *(org.childs[i]) );
		childs.push_back( c );
		c->set_parent( this );
	}
}

// *****************************************************************************
// destructor
// *****************************************************************************
template<typename T>
pqrnode<T>::~pqrnode(){
	for (unsigned i=0; i<childs.size(); i++){
		delete childs[i];
	}
	childs.clear();
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::append_child( pqrnode *c ){

	if(c == NULL){
		cerr << "append_child: NULL can not be appended"<<endl;
		exit( EXIT_FAILURE );
	}
	childs.push_back( c );
}


// *****************************************************************************
//
// *****************************************************************************
template<typename T>
set<T> pqrnode<T>::append_constraints(vector<set<T> > &constr){
	set<int> ret;
	vector<set<int> > chld_elems;

	if( type == LEAF ){
		ret.insert( value );
		return ret;
	}

		// recurse in the childs
	for(unsigned i=0; i<childs.size(); i++){
		chld_elems.push_back( childs[i]->append_constraints(constr) );
	}

		// get the elements in the leaves, if the node is a leaf insert its value
	for(unsigned i=0; i<chld_elems.size(); i++){
		ret.insert( chld_elems[i].begin(), chld_elems[i].end() );
	}

	if(type == QNODE){
		for(unsigned i=0; i<chld_elems.size()-1; i++){
			chld_elems[i].insert( chld_elems[i+1].begin(), chld_elems[i+1].end() );
//			if(chld_elems[i].size() > minsize){
				constr.push_back( chld_elems[i] );
//			}
		}
	}else if(type == PNODE){
		constr.push_back( ret );
	}else if(type == RNODE){
		cerr << "get_constraints: rnodes are not implemented!"<<endl;
		exit( EXIT_FAILURE );
	}

	return ret;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
pqrnode<T> *pqrnode<T>::check(){
	pqrnode<T> *prnt;
	for(unsigned i=0; i<childs.size(); i++){
		prnt = childs[i]->check();
		if( prnt != this ){
			cout << "wrong parent "<<*(childs[i])<<" : "<<*prnt <<endl;
			exit( EXIT_FAILURE );
		}
	}
	return parent;
}

// *****************************************************************************
// transform all prime nodes with 2 children into linear nodes
// *****************************************************************************

template<typename T>
void pqrnode<T>::transform_2childpq(){
	if( type == PNODE && childs.size() == 2 ){
		type = QNODE;
	}

	for(unsigned i=0; i<childs.size(); i++){
		childs[i]->transform_2childpq( );
	}
}


// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::clear_childs(){
	childs.clear();
}

// *****************************************************************************
// collapse the (leaf) children of the node with values x1=2*i-1, x2=2*i
//
// *****************************************************************************
template<typename T>
int pqrnode<T>::collapse( const T &x1, const T &x2 ){
	int ic1 = std::numeric_limits< int >::max(), 		// positions of the elements to collapse
		ic2 = std::numeric_limits< int >::max(),		// ... in the childs vector
		nval = 0;			// value of the node which is the result of collapsing x1,x2

	for(unsigned i=0; i<childs.size() && (ic1==std::numeric_limits< int >::max() || ic2 == std::numeric_limits< int >::max()) ; i++){
		if( childs[i]->type != LEAF )
			continue;
		if( childs[i]->value == x1){
			ic1 = i;
		}else if (childs[i]->value == x2 ){
			ic2 = i;
		}
	}

	if( ic1==std::numeric_limits< int >::max() || ic2 == std::numeric_limits< int >::max() ){
		cerr << "collapse: could not find one of "<< x1<<"/"<<x2 <<" in childs"<<endl;
		cerr << "   "<<*this<<endl;
		exit( EXIT_FAILURE );
	}

	if( type == QNODE ){
		if( abs( (int) (ic1 - ic2)) != 1 ){
			cerr << "collapse: "<< x1<<" "<<x2 << "are not neighboured in: "<<endl;
			cerr << "   "<<*this<<endl;
			exit( EXIT_FAILURE );
		}
	}
//	cout << "x  "<<x1<<" "<<x2<<endl;
//	cout << "ic "<<ic1<<" " <<ic2<<endl;
	if( ic1 < ic2 ){
		nval = (x1+1)/2;
//		cout << "nval = ("<<x1<<"+1)/2 = "<<nval<<endl;
	}else{
		nval = -1*(x1+1)/2;
//		cout << "nval = -1*("<<x1<<"+1)/2 = "<<nval<<endl;
	}


	childs[ic1]->value = nval;

	delete *(childs.begin()+ic2);
	childs.erase( childs.begin() + ic2 );
//	cout << "return "<<childs[ic1]->value<<endl;
	return nval;


//	if( p->get_type() == PNODE ){
//		p->set_type( LEAF );
//		p->set_value( i );
//		p->clear_childs();
//		delete nx1;
//		delete nx2;
//	}else if( p->get_type() == QNODE ){
//		if (p->collapse( 2*i-1, 2*i )){
//			cerr << "collapse: could not collapse "<<2*i-1<<" and "<<2*i<<endl;
//		}
//	}

}

// *****************************************************************************
// remove 0 and 2n+1 from the tree (root), and reverse the whole tree if the first child is
// 2n+1 and the last is 0
// *****************************************************************************
template<typename T>
void pqrnode<T>::collapse_root(int tnp1){
	if(parent != NULL){
		cerr << "collapse_root(): called for non-root node"<<endl;
		exit( EXIT_FAILURE );
	}
	if( childs.size() < 2 ){
		cerr << "collapse_root(): number of children is "<<childs.size()<<" < 2"<<endl;
		exit( EXIT_FAILURE );
	}
//	if( !childs[0]->is_leaf() ){
//		cerr << "collapse_root(): first child of the root is no leaf"<<endl;
//		exit( EXIT_FAILURE );
//	}
//	if( !childs.back()->is_leaf() ){
//		cerr << "collapse_root(): last child of the root is no leaf"<<endl;
//		exit( EXIT_FAILURE );
//	}

	if ( !(childs[0]->value == 0 && childs.back()->value == tnp1) and !(childs[0]->value == tnp1 && childs.back()->value == 0)){
//		cerr << "collapse_root(): first and last child must be 0 and 2n+1"<<endl;
//		cerr << *this << endl;
//		exit( EXIT_FAILURE );
		return;
	}

//	if(childs[0]->value == tnp1 &&  childs.back()->value== 0){
//		this->reverse( );
//	}

	childs.erase(childs.begin());
	childs.erase(childs.end()-1);
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
unsigned pqrnode<T>::cnt_ndtype( NodeType n ){
	unsigned cnt = 0;
	for( unsigned i=0; i<childs.size(); i++ ){
		cnt += childs[i]->cnt_ndtype(n);
	}
	if(type == n){
		cnt++;
	}
	return cnt;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::expand(){
	int x1, x2;
	pqrnode *c;

	for(unsigned i=0; i<childs.size(); i++){
		if( childs[i]->type == LEAF ){
			x1 = childs[i]->value;
			if(x1>0){
				x2 = 2*x1;
				x1 = 2*x1-1;
			}else{
				x2 = -2*x1-1;
				x1 = -2*x1;
			}

			if(type == QNODE){
				childs[i]->value = x1;
				childs[i]->type = LEAF;
				c = new pqrnode( LEAF, x2, this );

				childs.insert( childs.begin() + i+1, c );
				i++;
			}else if(type == PNODE && childs.size() > 1){
					// transform the node into a PNODE
				childs[i]->set_type(PNODE);
				childs[i]->set_value(0);

					// appent the new leafs x1, x2
				c = new pqrnode( LEAF, x1, childs[i]);
				childs[i]->childs.push_back(c);
				c = new pqrnode( LEAF, x2, childs[i]);
				childs[i]->childs.push_back(c);
			}else if(type == PNODE && childs.size() == 1){
				childs[i]->value = x1;
				c = new pqrnode( LEAF, x2, childs[i]);
				childs[i]->childs.push_back(c);
			}else{
				cerr <<"expand: node of unknown type "<<endl;
				exit( EXIT_FAILURE );
			}
		}else{
			childs[i]->expand();
		}
	}
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::front( vector<T> &p ) const {
	if( is_leaf() ){
		p.push_back(value);
	}else{
		for(unsigned i=0; i<childs.size(); i++){
			childs[i]->front(p);
		}
	}
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::get_fpe( vector<pqrnode<T> *> &f, vector<pqrnode<T> *> &p, vector<pqrnode<T> *> &e ){

	f.clear();
	p.clear();
	e.clear();

	for(unsigned i=0; i<childs.size(); i++){
		switch (childs[i]->label){
			case EMPTY: {e.push_back( childs[i] ); break; }
			case PARTIAL: {p.push_back( childs[i] ); break; }
			case FULL: {f.push_back( childs[i] ); break; }
			default: {
				cerr<< "get_fpe(): non PQR node "<<endl;
				exit( EXIT_FAILURE );
			}
		}
	}
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
char pqrnode<T>::get_label(){
	return label;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::get_leafs( map<T, pqrnode<T> *> &leafs ){

	for( unsigned i=0; i<childs.size(); i++ ){
		childs[i]->get_leafs( leafs );
	}

	if( type == LEAF ){
		leafs[value] = this;
	}
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
pqrnode<T>* pqrnode<T>::get_parent(){
	return parent;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
int pqrnode<T>::get_type() const{
	return type;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
int pqrnode<T>::get_value(){
	return value;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
bool pqrnode<T>::has_ndtype( NodeType n ){
	if(type == n){
		return true;
	}
	for( unsigned i=0; i<childs.size(); i++ ){
		if ( childs[i]->has_ndtype(n) ){
			return true;
		}
	}
	return false;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
bool pqrnode<T>::is_leaf() const{
	if (childs.size() == 0)
		return true;
	else
		return false;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
pqrnode<T>* pqrnode<T>::label_and_getlca(){
	int cnt = 0;			// number of full/partial children
	pqrnode* lca = NULL; 	// the lca

	label = FULL;
	for(unsigned i=0; i<childs.size(); i++){
		if( childs[i]->label == FULL ){
			cnt++;
			lca = childs[i]->label_and_getlca();
			if( childs[i]->label == PARTIAL ){
				label = PARTIAL;
			}
		}else{
			label = PARTIAL;
		}
	}

#ifdef DEBUGPQR_LABEL
	cout << "label&getlca cnt "<<cnt<<" "<<*this << endl;
#endif//DEBUGPQR_LABEL

	if( cnt == 1 ){
		return lca;
	}else{
		return this;
	}
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::merge_into_lca( const pqrnode<T> *c ){
#ifdef DEBUGPQR
cout << "merge into lca     ";
#endif//DEBUGPQR
	vector<pqrnode<T> *> newchilds;
	unsigned cnt = 0;

		// find the node that is to be merged
	for( unsigned i=0; i<childs.size(); i++ ){
		if( c == childs[i] ){
				// move all childs of the current child
				// and delete the current child
			for( unsigned j=0; j<childs[i]->childs.size(); j++ ){
				childs[i]->childs[j]->parent = this;
				newchilds.push_back( childs[i]->childs[j] );
			}
			childs[i]->childs.clear();
			delete childs[i];
			cnt++;
		}else{
			newchilds.push_back( childs[i] );
		}
	}
	childs.clear();
	childs = newchilds;

	if( cnt != 1 ){
		cerr << "merge_into_lca did not merge exactly one child into the lca but "<< cnt<< endl;
		exit( EXIT_FAILURE );
	}

#ifdef DEBUGPQR
cout << *this<<endl;
#endif//DEBUGPQR
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
pqrnode<T>* pqrnode<T>::move_children_away( pqrnode<T> *v,
		vector<pqrnode<T>*> &f, vector<pqrnode<T>*> &p,
		vector<pqrnode<T>*> &e ){

#ifdef DEBUGPQR
	cout << "move children away ";
#endif//DEBUGPQR

	pqrnode<T> *newlca;

		// the new lca is the last of the partial nodes
	newlca = p.back();
	p.pop_back();

		// delete all children of the lca (empty nodes and the new lca are
		// inserted back later)
	childs.clear();
#ifdef DEBUGPQR
	cout << "into "<<*newlca<<" ";
#endif//DEBUGPQR
		// if there are empty nodes -> (re)append them and the node itself
		// else the lca has to be deleted and the node v has to become
		// the child of the parent of the lca
	if( e.size() > 0 ){
		childs.insert( childs.end(), e.begin(), e.end() );
		childs.insert( childs.end(), newlca );
	}else{
			// move the properties of the newlca to the current node
		childs = newlca->childs;
		type = newlca->type;
		label = newlca->label;
		value = newlca->value;

		for(unsigned i=0; i<newlca->childs.size(); i++){
			newlca->childs[i]->parent = this;
		}

		newlca->childs.clear();
		delete newlca;

		newlca = this;
	}

		// insert the full and the partial nodes as childs of the new lca
		// (ofcourse the parent pointers have to be adjusted)
	newlca->childs.insert( newlca->childs.begin(), f.begin(), f.end() );
	newlca->childs.insert( newlca->childs.begin(), p.begin(), p.end() );
	for(unsigned i=0; i<f.size(); i++){
		f[i]->parent = newlca;
	}
	for(unsigned i=0; i<p.size(); i++){
		p[i]->parent = newlca;
	}

	f.clear();
	p.clear();
#ifdef DEBUGPQR
	cout << " -> "<<*this <<endl;
#endif//DEBUGPQR

	return newlca;
}

// *****************************************************************************
// output fuction, the names of the genes are taken from nmap
// *****************************************************************************
template<typename T>
ostream & pqrnode<T>::named_output( vector<string> *nmap, ostream &os ) const{
	switch (type){
		case QNODE:{os << "["; break;}
		case PNODE:{os << "("; break;}
		case RNODE:{os << "{"; break;}
		default: {break;}
	}

	for(unsigned i=0; i<childs.size(); i++){
		childs[i]->named_output( nmap, os );
		if(i < childs.size()-1)
			os << ",";
	}

	if(type == LEAF){
		if(value < 0)
			os << "-";
//		os << nmap[ abs(value) ];
		print_element(value, os, 1, "", nmap);
	}
	switch (type){
		case QNODE:{os << "]"; break;}
		case PNODE:{os << ")"; break;}
		case RNODE:{os << "}"; break;}
		default: {break;}
	}

	switch(label){
		case PARTIAL: {os << "+"; break;}
		case FULL: {os << "*"; break;}
		default: {break;}
	}
	return os;
}

// *****************************************************************************
//
// *****************************************************************************

template<typename T>
T pqrnode<T>::normalise(  ){
	if(childs.size()==0){
		return value;
	}else if( type == QNODE ){
		T f, b;
		f = childs[0]->normalise();
		b = (childs.back())->normalise();
		if( b < f ){
			reverse();
			return b;
		}else{
			return f;
		}
	}else{
		T tmp,
			min = childs[0]->normalise();
		for( unsigned i=1; i<childs.size(); i++ ){
			tmp = childs[i]->normalise();
			if( tmp < min ){
				min = tmp;
				swap( childs[0], childs[i] );
			}
		}
		return tmp;
	}

}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
vector<vector<T> > pqrnode<T>::permutations( bool directed ) const{

	vector<vector<T> > p,	// temporary memory for constructing the permutations
	 	 pp;				// the construted permutations
	vector<vector<vector<T> > > cp;	// child permutations, i.e. for each child
									// the permutations of the corr. subtree
	typename vector<vector<vector<T> > >::iterator it;

	// leaf: only one permutation: just the element
	if(childs.size()==0){
		return vector<vector<T> >(1, vector<T>(1, value) );
	}

	// get the permutations of the childs
	for( unsigned i=0; i<childs.size(); i++ ){
		cp.push_back( childs[i]->permutations( (i==0)?directed:false ) );
	}

//	cout << *this<< endl;
//	cout << "child permutations"<< endl;
//	for(unsigned i = 0; i< cp.size(); i++){
//		cout << "---"<< i <<endl;
//		for(unsigned j=0; j<cp[i].size(); j++){
//			copy( cp[i][j].begin(), cp[i][j].end(), ostream_iterator<T>( cout, " " ) );
//			cout << endl;
//		}
//	}
//	cout << "END child permuttaions"<<endl;



	if( type == QNODE ){
//		cout << "QNODE "<<endl;

		// loop for the two possible orientations
		// (after the first iteration the order of the children is reversed)
		for( unsigned o=0; o<2; o++ ){
			// the permutatiions are initialised as the permutations
			// of the first child. then the permutations of the following
			// childs are appended in all possible combinations
			p = cp[ 0 ];
			for( unsigned i=1; i<cp.size(); i++ ){
				// transform p such that it contains x copies of the current
				// content, with x = number of permutations of the i-th child
				// one copy is already there -> so only x-1 need to be appended
				unsigned s = p.size();
				for( unsigned j=0; j<cp[i].size()-1; j++ ){
					p.insert( p.end(), p.begin(), p.begin()+s );
				}

				// now append the the permutations of child i. the j-th permutation
				// of child i is appended to the jth copy of the permutations
				// of childs 0..i-1
				for( unsigned j=0; j<cp[i].size(); j++ ){
					for( unsigned k=0; k<s; k++ ){
						p[ j*s+k ].insert(p[ j*s+k ].end(), cp[i][j].begin(), cp[i][j].end() );
					}
				}
			}

			pp.insert( pp.end(), p.begin(), p.end() );

			// in the directed case the root has only one orientation
			if( !directed && parent == NULL ){
				break;
			}
			std::reverse( cp.begin(), cp.end() );
		}
	}else{
//		cout << "PNODE "<<endl;

		// iterate each permutation of the permutations of the children
		// and compose all combinations in the same way as above

		// in the directed case, the first child of the root is fixed
		if( directed && parent == NULL ){
			it = cp.begin()+1;
		}else{	// other wise also the first child may move
			it = cp.begin();
		}

		// for iterating all permutations with next_permutation from STL
		// we need to start with a sorted range
		sort( it, cp.end() );

		// iterate all permutations .. the internals are the same as
		// above in the qnode case
		do{
			p = cp[0];
			for( unsigned i=1; i<cp.size(); i++ ){
				unsigned s = p.size();
				for( unsigned j=0; j<cp[i].size()-1; j++ ){
					p.insert( p.end(), p.begin(), p.begin()+s );
				}
				for( unsigned j=0; j<cp[i].size(); j++ ){
					for( unsigned k=0; k<s; k++ ){
						p[ j*s+k ].insert(p[ j*s+k ].end(), cp[i][j].begin(), cp[i][j].end() );
					}
				}
			}
//			cout << "add "<<endl;
//			for( unsigned h=0; h<p.size(); h++ ){
//				copy( p[h].begin(), p[h].end(), ostream_iterator<int>(cout, " ") ); cout << endl;
//			}
//			cout<< "END FINAL P "<<endl;
			pp.insert( pp.end(), p.begin(), p.end() );
		}while( next_permutation(it, cp.end() ) );
	}
	return pp;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::permutations_count( unsigned &count, bool directed ) const {
	unsigned div=1;

	for(unsigned i=0; i<childs.size(); i++){
		childs[i]->permutations_count( count, directed );
//		cout << "childs["<<i<<"] = "<< childs[i]->get_value()<< "  count: "<<count<<endl;
	}
	if(childs.size()>0){
		div = (parent == NULL && !directed) ? 2 : 1;
		if(get_type() == QNODE){
			count *= (2/div);
		}
		else{
			count *= (fact(childs.size())/div);
		}
//		cout << "childssize" << childs.size()<< " count:"<<count<<endl;
	}
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::prepare_lca( vector<pqrnode<T> *> &f,
		const vector<pqrnode<T> *> &p, const vector<pqrnode<T> *> &e ){
#ifdef DEBUGPQR
cout << "prepare lca        ";
#endif//DEBUGPQR
	pqrnode<T> *newnode;		// the new node (if only one black : the lca)

	// if there is only one child nothing is to be done
	if(f.size() <= 1){
#ifdef DEBUGPQR
		cout << " -> <= 1 full node -> noop "<<endl;
#endif//DEBUGPQR
		return;
	}

	// remove the old childs (still available in f,p,e)
	childs.clear();

	// add a new full prime node as first child and make all full
	// nodes that have been children of the lca to children of
	// the new node (and set the parent of those nodes to the new node)
	newnode = new pqrnode(PNODE, 0, this, FULL );
	childs.push_back(newnode);
	newnode->childs.insert( newnode->childs.end(), f.begin(), f.end() );
	for( unsigned i=0; i<f.size(); i++ ){
		f[i]->parent = newnode;
	}

	// then re-add the partial and empty children to the lca
	this->childs.insert( this->childs.end(), p.begin(), p.end() );
	this->childs.insert( this->childs.end(), e.begin(), e.end() );

	// update the list of full children of the lca
	f.clear();
	f.push_back(newnode);

#ifdef DEBUGPQR
cout << " -> " << *this<<endl;
#endif//DEBUGPQR
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
bool pqrnode<T>::reduce(pqrnode<T>** newlca){
	bool reducable = true;
	vector< pqrnode *> f, p, e; // full, partial, and empty childs of the lca

	*newlca = NULL;

		// get the full, partial, and empty children of the lca
	get_fpe( f, p, e );

#ifdef DEBUGPQR
cout << "reduce node: "<<*this<< " f: "<<f.size()<<" p "<<p.size()<<" e "<<e.size()<<endl;
#endif//DEBUGPQR

	if( p.size() > 0  ){
		// all patterns *P transform the prime node into a linear node first
		// after this the the corresponding pattern *Q pattern is applied
		// i.e. after this point only patterns PQ, PR, QQ, QR, RQ, RR are
		// considered
		if( p.back()->type == PNODE ){
			p.back()->transform_pq( );
		}

		// as noted in the paper R nodes can be treated as Q nodes (to the cost of some
		// additional node rotations), therefore only the two cases
		// a) PQ, PR	(-> PQ template)
		// b) QQ, QR, RQ, RR	(-> QQ template)
		// need to be considered
		if( type == PNODE){
			prepare_lca( f, p, e );
			p.back()->reverse_qnode( );
			*newlca = move_children_away(p.back(), f, p, e);
		}else if( type == QNODE || type == RNODE ){
			reverse_lca( p.back() );
			p.back()->reverse_qnode( );
			merge_into_lca( p.back() );
			*newlca = this;
		}else{
			cerr << "pqrnode<T>::reduce: wrong node type"<<endl;
			exit( EXIT_FAILURE );
		}
	}else{
#ifdef DEBUGPQR
		cout << "no further partial nodes, step 4 "<<endl;
#endif//DEBUGPQR

		if(type == PNODE){
#ifdef DEBUGPQR
			cout << "PNODE "<< endl;
#endif//DEBUGPQR
			if( f.size() > 1 && e.size() > 0 ){
				// can reuse the function prepare_lca here because partial is empty
				prepare_lca( f, p, e );
			}
		}else if(type == QNODE){
			vector<int> blocks;
			blocks.push_back( childs[0]->label );
				// check if the full nodes are consecutive
//			cout << "labels ";
			for(unsigned i=1; i<childs.size(); i++){
//				cout << childs[i]->label<<" ";
				if(childs[i]->label != blocks.back() ){
					blocks.push_back( childs[i]->label );
				}
			}
//			cout << endl;

#ifdef DEBUGPQR
			cout << "QNODE"<< endl;
			cout << "blocks ";copy(blocks.begin(), blocks.end(), ostream_iterator<int>(cout, " ") );cout << endl;
#endif//DEBUGPQR
				// consecutive: F, E, FE, EF, EFE
			if( blocks.size() > 3 || (blocks.size() == 3 && blocks[0] != EMPTY ) ){
#ifdef DEBUGPQR
				cout << "not consecutive -> RNODE"<<endl;
#endif//DEBUGPQR
				type = RNODE;
				reducable = false;
			}else{
#ifdef DEBUGPQR
				cout << "consecutive -> noop"<<endl;
#endif//DEBUGPQR
			}
		}else if(type == RNODE){
#ifdef DEBUGPQR
			cout << "step 4 RNODE : noop"<<endl;
#endif//DEBUGPQR
		}
		*newlca = NULL;
	}

	return reducable;
/*
		// step 3:

		// transform partial child nodes p:
		// - if it is a PNODE :
		//   make it a QNODE and group the full, and empty childs of p into new pnodes,
		//   in the result the order of the childs is: full, partial, empty
		// - if it is a QNODE :
		//   reverse it such that 'fuller' childs come first
	for(unsigned i=0; i<p.size(); i++){
		if(p[i]->type == PNODE)
			p[i]->transform_pq( );
		else if( p[i]->type == QNODE ){
			p[i]->reverse_qnode( );
		}
	}

		// if the lca is a pnode then
		// * prepare the lca: group all childs nodes into a new pnode
		// * move the partial childs away
		// continue at the node where now all partial nodes are
	if( p.size() > 0 ){
		if( type == PNODE ){
			prepare_lca( f, p, e );
			*newlca = move_children_away(f, p, e);
		}
		else if( type == QNODE || type == RNODE ){
			if( type == QNODE ){
				reverse_lca( p[0] );
			}
			merge_into_lca();
			*newlca = this;
		}
	}else{
#ifdef DEBUGPQR
		cout << "no further partial nodes"<<endl;
#endif//DEBUGPQR

		if(type == PNODE){
			if( f.size() > 1 && e.size() > 0 ){
				prepare_lca( f, p, e );
			}
		}else if(type == QNODE){
			vector<int> blocks;
			blocks.push_back( childs[0]->label );
				// check if the full nodes are consecutive
			for(unsigned i=1; i<childs.size(); i++){
				if(childs[i]->label != blocks.back() ){
					blocks.push_back( childs[i]->label );
				}
			}

#ifdef DEBUGPQR
			cout << "step 4 QNODE: ";
			cout << "blocks ";copy(blocks.begin(), blocks.end(), ostream_iterator<int>(cout, " ") );cout << endl;
#endif//DEBUGPQR
				// consecutive: F, E, FE, EF, EFE
			if( blocks.size() > 3 || (blocks.size() == 3 && blocks[0] != EMPTY ) ){
#ifdef DEBUGPQR
				cout << "not consecutive -> RNODE"<<endl;
#endif//DEBUGPQR
				type = RNODE;
				reducable = false;
			}else{
#ifdef DEBUGPQR
				cout << "consecutive -> noop"<<endl;
#endif//DEBUGPQR
			}
		}else if(type == RNODE){
#ifdef DEBUGPQR
			cout << "step 4 RNODE : noop"<<endl;
#endif//DEBUGPQR
		}


		*newlca = NULL;
	}
	// step 4:


	return reducable;
*/
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::reset(){
	label = EMPTY;
	for(unsigned i=0; i<childs.size(); i++)
		childs[i]->reset();
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::reverse(){
	std::reverse(childs.begin(), childs.end());
	for(unsigned i=0; i<childs.size(); i++)
		childs[i]->reverse();

}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::reverse_qnode(){
#ifdef DEBUGPQR
	cout << "reverse q node     ";
#endif//DEBUGPQR
	if(childs.size() < 2){
		cerr << "reverse_qnode(): less than 2 childs"<<endl;
		exit( EXIT_FAILURE );
	}

		// make the left at least as full as the left end
	if( childs[0]->label < childs.back()->label ){
#ifdef DEBUGPQR
		cout << "L<(dark)R -> reverse : ";
#endif//DEBUGPQR
		std::reverse( childs.begin(), childs.end() );
	}
#ifdef DEBUGPQR
	else{cout << "L>=(dark)R -> noop : ";}
	cout <<*this<<endl;
#endif//DEBUGPQR

}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::reverse_lca(pqrnode<T> *cc){
#ifdef DEBUGPQR
	cout << "reverse lca         wrt "<<*cc<<" ";
#endif//DEBUGPQR
	int l = std::numeric_limits< int >::max(),
		r = std::numeric_limits< int >::max();

		// determine the left an right neighbor
	for(unsigned i=0; i<childs.size(); i++){
		if( cc == childs[i] ){
//			cout << "found at "<<i<< " ";
			if (i > 0){
				l = i - 1;
			}
			if ( i < childs.size()-1){
				r = i + 1;
			}
		}
	}
		// two neighbors
	if( l != std::numeric_limits< int >::max() && r != std::numeric_limits< int >::max() ){
//		cout << "2 nb. ";
		if ( childs[l]->label < childs[r]->label )
			std::reverse(childs.begin(), childs.end());
	}
	else if (r != std::numeric_limits< int >::max()){
//		cout << "r nb. "<<childs[r]->label<<" ";
		if ( childs[r]->label > EMPTY ){
			std::reverse(childs.begin(), childs.end());
		}
	}
	else if (l != std::numeric_limits< int >::max()){
//		cout << "l nb. ";
		if ( childs[l]->label == EMPTY ){
			std::reverse(childs.begin(), childs.end());
		}
	}

#ifdef DEBUGPQR
	cout << " -> "<<*this<<endl;
#endif//DEBUGPQR

}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::set_label( LevelType l ){
	label = l;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::set_parent( pqrnode *p  ){
	parent = p;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::set_type( const NodeType &t ){
	type = t;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::set_value( const T &v ){
	value = v;
}

// *****************************************************************************
//
// *****************************************************************************


template<typename T>
unsigned pqrnode<T>::size( bool leaf ) const{
	unsigned s = 1;
	if( !leaf && is_leaf() ){
		s = 0;
	}

	for( unsigned i=0; i<childs.size(); i++ ){
		s += childs[i]->size( leaf );
	}
	return s;
}
// *****************************************************************************
//
// *****************************************************************************
template<typename T>
pair<T,T> pqrnode<T>::todot(ostream &os, vector<string> *nmap){
	string shape;
	pair<T,T> p;
	vector<pair<T,T> > cp;

	cp = vector<pair<T,T> >(childs.size());

	if( is_leaf() ){
		p = make_pair( get_value(), get_value() );
	}
	for( unsigned i=0; i<childs.size(); i++ ){
		cp[i] = childs[i]->todot(os, nmap);
		if( i==0 )
			p = cp[i];
		else{
			p.first = min(p.first, cp[i].first);
			p.second = max(p.second, cp[i].second);
		}
	}
	if( p.first > p.second ){
		swap( p.first, p.second );
	}

	for( unsigned i=0; i<childs.size(); i++ ){
		os << "N"<<this <<" -- "<< "N"<<childs[i] <<";" <<endl;
//		os << cp[i].first<<"."<<cp[i].second <<" -- "<< p.first<<"."<<p.second<<";" <<endl;
	}

	if (type == RNODE){
		shape = "polygon, sides=5";
	}else if( type == PNODE ){
		shape = "ellipse";
	}else{
		shape = "polygon, sides=4";
	}

	os << "N"<<this << " [label=\"";
//	os << p.first<<"."<<p.second << " [label=\"";
	if( is_leaf( ) ){
		print_element(value, os, 1, "", nmap);
	}
	os << "\", shape="<<shape<<"];"<<endl;

	return p;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrnode<T>::transform_pq(){
	vector<pqrnode<T> *> f, p, e;	// full, partial, and empty childs
	pqrnode<T> *newnode;		// the new nodes

#ifdef DEBUGPQR
	cout << "transform p_q      ";
#endif//DEBUGPQR

	if(type != PNODE){
		cerr << "transform_unknownpq(): called for non P node"<<endl;
		exit( EXIT_FAILURE );
	}

		// get the full, partial, and empty childs (here for the prime child of the lca)
		// clear the child list
	get_fpe(f, p, e);
	childs.clear();

		// if there is more than 1 full node:
		//    - append a new P child and append all full nodes to the new node
		// else
		//    - just append it to the original node
		//      (if |f|==0 nothing happens)
	if( f.size() > 1 ){
		// new FULL prime node with value 0 (inner node) that has the current node as its parent
		newnode = new pqrnode(PNODE, 0, this, FULL );
		childs.push_back(newnode);
		newnode->childs.insert( newnode->childs.end(), f.begin(), f.end() );
		for( unsigned i=0; i<f.size(); i++ ){
			f[i]->parent = newnode;
		}
	}else{
		childs.insert( childs.end(), f.begin(), f.end() );
	}

		// put all partial nodes back to their place
	childs.insert( childs.end(), p.begin(), p.end() );

		// if there is more than 1 empty node:
		//    - append a new P child and append all empty nodes to the new node
		// else
		//    - just append it to the original node
		//      (if |e|==0 nothing happens)
	if(e.size() > 1){
		// new EMPTY prime node with value 0 (inner node) that has the current node as its parent
		newnode = new pqrnode(PNODE, 0, this, EMPTY );
		childs.push_back(newnode);
		newnode->childs.insert( newnode->childs.end(), e.begin(), e.end() );
		for( unsigned i=0; i<e.size(); i++ ){
			e[i]->parent = newnode;
		}
	}else{
		childs.insert( childs.end(), e.begin(), e.end() );
	}

		// adjust the node type
	type = QNODE;

#ifdef DEBUGPQR
	cout <<" -> " << *this << endl;
#endif//DEBUGPQR

}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
ostream &operator<<(ostream &os, const pqrnode<T> &n) {
	switch (n.type){
		case QNODE:{os << "["; break;}
		case PNODE:{os << "("; break;}
		case RNODE:{os << "{"; break;}
		default: {break;}
	}

	for(unsigned i=0; i<n.childs.size(); i++){
		os << *(n.childs[i]);
		if(i < n.childs.size()-1)
			os << ",";
	}

	if(n.type == LEAF)
		os << n.value;

	switch (n.type){
		case QNODE:{os << "]"; break;}
		case PNODE:{os << ")"; break;}
		case RNODE:{os << "}"; break;}
		default: {break;}
	}

	switch(n.label){
		case PARTIAL: {os << "+"; break;}
		case FULL: {os << "*"; break;}
		default: {break;}
	}

	return os;
}

// *****************************************************************************
// copy assignment
// *****************************************************************************
template<typename T>
pqrnode<T>& pqrnode<T>::operator= (const pqrnode<T>& in){

	parent = NULL;
	type = in.type;
	label = in.label;
	value = in.value;

	for(unsigned i=0; i<in.childs.size(); i++){
		pqrnode<T> *c = new pqrnode<T>( *(in.childs[i]) );
		childs.push_back( c );
		c->set_parent( this );
	}

	return *this;
}

// *****************************************************************************
// empty constructor
// *****************************************************************************
template<typename T>
pqrtree<T>::pqrtree(){
	root = NULL;
	leafs = map<int,pqrnode<T> *>();
}


// *****************************************************************************
// constructor P node and leafs from s to e
// *****************************************************************************
template<typename T>
pqrtree<T>::pqrtree(T s, T e){
		//set up the root node as a P-Node initially
	root = new pqrnode<T>(PNODE, 0);
	for(int i = s; i<=e; i++){
		pqrnode<T> *newnode;
		newnode = new pqrnode<T>(LEAF, i, root);
		root->append_child(newnode);
		leafs[i] = newnode;
	}
}

// *****************************************************************************
// constructor from an initial set -> P node and leafs
// *****************************************************************************
template<typename T>
pqrtree<T>::pqrtree(const set<T> &S){
		//set up the root node as a P-Node initially
	root = new pqrnode<T>(PNODE, 0);

	for(set<int>::iterator i=S.begin();i!=S.end();i++)
	{
		pqrnode<T> *newnode;
		newnode = new pqrnode<T>(LEAF, *i, root);
		root->append_child(newnode);
		leafs[*i] = newnode;
	}
}


// *****************************************************************************
//
// *****************************************************************************
template<typename T>
pqrtree<T>::pqrtree(const vector<T> &v){

		//set up the root node as a Q-Node initially
	root = new pqrnode<T>(QNODE, 0 );

	for( unsigned i=0; i<v.size(); i++){
		pqrnode<T> *newnode;
		newnode = new pqrnode<T>(LEAF, v[i], root );
		root->append_child(newnode);
		leafs[v[i]] = newnode;
	}
}

// *****************************************************************************
// copy constructor
// *****************************************************************************
template<typename T>
pqrtree<T>::pqrtree(const pqrtree &org) : root (NULL) {
		// copy the tree
	if( org.root != NULL ){

		root = new pqrnode<T>( *(org.root) );

			// assign the leafs
		root->get_leafs( leafs );
	}
}

// *****************************************************************************
// default destructor, just needs to delete the root
// *****************************************************************************
template<typename T>
pqrtree<T>::~pqrtree()
{
	if( root != NULL )
		delete root;
	leafs.clear();
}

// *****************************************************************************
// check the parent pointers
// *****************************************************************************
template<typename T>
void pqrtree<T>::check(){
	root->check();
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrtree<T>::clear(){
	if( root != NULL )
		delete root;
	leafs.clear();
}

// *****************************************************************************
// count nodes of a given type
// *****************************************************************************
template<typename T>
unsigned pqrtree<T>::cnt_ndtype( NodeType n ){
	if(root != NULL){
		return root->cnt_ndtype( n );
	}else{
		return 0;
	}
}

// *****************************************************************************
// collapse the tree .. make it signed .. the counterpart to expand
// *****************************************************************************
template<typename T>
void pqrtree<T>::collapse(int tnp1){
	int nleaf;
	T max;
	typename map<T, pqrnode<T> * >::iterator max_m;
	map<T, pqrnode<T> * > newleaves;
	pqrnode<T> *nx1,
		*nx2,
		*p;

	normalise();

	root->collapse_root(tnp1);
	leafs.erase( 0 );
	leafs.erase( tnp1 );
//	cout << *root << endl;

	max = 0;
	for( typename map<T, pqrnode<T>*>::const_iterator it=leafs.begin();
			it!=leafs.end(); it++ ){
		if( it->first > max ){
			max = it->first;
		}
	}
//	max = max_element( leafs.begin(), leafs.end() );
//	max = tnp1-1;

	if(max % 2 != 0){
		cerr << "collapse(): max "<<max<<endl;
		exit( EXIT_FAILURE );
	}

	max /= 2;
	for(int i=1; i<=max; i++){
//		cout << i<<" "<<2*i-1<<" " <<2*i<<endl;

		// if the there are deleted elements -> then the corresponding leaves are missing
		if( leafs.find( 2*i ) == leafs.end() ){
			continue;
		}

		nx1 = leafs[ 2*i - 1 ];
		nx2 = leafs[ 2*i ];

		p = nx1->get_parent();
		if(p != nx2->get_parent()){
			cerr << "collapse: "<<2*i-1<<" and "<<2*i<<" have different parents "<<endl;
			exit( EXIT_FAILURE );
		}

//		cout << "collapse "<<nx1->get_value() << "("<<2*i-1<<")" << " "<<nx2->get_value()<< "("<<2*i<<")"<<endl;
		nleaf = p->collapse( 2*i-1, 2*i );
//		cout << "nleaf "<<nleaf<<endl;
		newleaves[nleaf] = nx1;
		//		leafs.erase(2*i-1);
//		leafs.erase(2*i);
//		leafs[ nleaf ] = nx1;
//		print_leaves();
	}

	leafs.clear();
	leafs = newleaves;
}

// *****************************************************************************
// get all subsets of i which are blocking with respect to the given tree
// *****************************************************************************
template<typename T>
vector<set<T> > pqrtree<T>::get_blocking_subsets(const vector<set<T> > &sets ){

	pqrtree t( *this );	// copy of the original tree
	set<int> tmp;		// temporary variable
	vector<set<int> > m,
		b,
		newb,
		subsets;

	for(unsigned i=0; i<sets.size(); i++){
		cout << "\r reduce "<<t << " with ";
		copy(sets[i].begin(), sets[i].end(), ostream_iterator<int>(cout, " ") );
		cout <<" "<<i << "/"<<sets.size()<<" ";
		cout.flush();
		m.push_back(sets[i]);
		if (!t.reduce( sets[i].begin(), sets[i].end() )){
			cout <<"not reducable"<<endl;;
			break;
		}
		cout <<"fin"<<endl;
	}

	cout <<endl;

		// if the tree has no rnode, i.e. it can be reduced to a proper pqtree
		// then 'sets' is neutral (there is no conflict). so return the empty set
	if( !t.has_rnode() ){
		return b;
	}

	for( unsigned i=0; i<m.size(); i++ ){
		t = *this;		// copy the original tree
		tmp = m[i];
		m[i].clear();

		if( !t.reduce( m ) );
			m[i] = tmp;
	}

		// initialise the blocking set
	for( int i=m.size()-1; i>=0; i--){
		if( m[i].size() == 0 ){
			m.erase(m.begin()+i);
		}
	}
	b = m;

		// rekursion
	for( unsigned i=0; i<m.size(); i++ ){
		subsets = m;
		subsets.erase( subsets.begin( ) + i );

		newb = get_blocking_subsets( subsets );

		b.insert( b.end(), newb.begin(), newb.end() );
		sort(b.begin(), b.end());
		unique(b.begin(), b.end());

	}

	return b;
}

// *****************************************************************************
// get the constraints implied by p/q nodes
// *****************************************************************************
template<typename T>
void pqrtree<T>::append_constraints(vector<set<T> > &constraints){

	if(root != NULL)
		root->append_constraints(constraints);
}



// *****************************************************************************
// check if the tree has an rnode
// *****************************************************************************
template<typename T>
bool pqrtree<T>::has_ndtype( NodeType n ){
	if(root != NULL){
		return root->has_ndtype( n );
	}else{
		return false;
	}
}

// *****************************************************************************
// expand the tree .. make it unsigned
// *****************************************************************************
template<typename T>
void pqrtree<T>::expand(){
	root->expand();
	leafs.clear();
	root->get_leafs( leafs );
}

// *****************************************************************************
// label the nodes as empty, partial, or full
// *****************************************************************************
template<typename T>
template<class _InputIterator>
pqrnode<T> * pqrtree<T>::label(const _InputIterator &b, const _InputIterator &e ){

	queue<pqrnode<T>*> q;
	pqrnode<T> *frnt,			// the front of the queue
		*prnt,				// parent of the current node
		*lca = NULL; 		// the lca ..

		// put the leaves corresponding to the elements in s in the queue
		// and mark them as full
	for(_InputIterator it=b; it!=e; it++){
		pqrnode<T> *temp=leafs[*it];
		if(temp==NULL){		//make sure we have this in our leaves already
			cerr << "label(): unknown leaf label "<<*it<<endl;
			cerr << *this << endl;
			cerr << "leaves :"<<endl;

			for( typename map<T,pqrnode<T> * >::iterator jt = leafs.begin();
					jt!=leafs.end(); jt++){

				cout << jt->first<<" ";
			}
			cout <<endl;
			exit( EXIT_FAILURE );
		}
		temp->set_label( FULL );
		q.push(temp);
	}

	while( q.size() > 0 ){
			// remove X from the front of the queue
		frnt = q.front();
		q.pop();
		prnt = frnt->get_parent();
#ifdef DEBUGPQR_LABEL
		cout <<"  label: queue top = "<< *frnt ;
		if (prnt == NULL){
			cout << " is root"<<endl;
		}else if(prnt->get_label() == FULL) {
			cout << " prnt is known "<< *prnt<<endl;
		}else{
			cout << " proceed to prnt "<< *prnt <<endl;
		}
#endif//DEBUGPQR
		// no need to continue beyond the root (there is nothing)
		// if the parent was visited before (the FULL mark is misused
		// as an already-visited-flag)
		// otherwise add it to the queue for later processing
		if( prnt != NULL && prnt->get_label() != FULL ){
			prnt->set_label( FULL );
			q.push(prnt);
		}
	}

	// now all nodes on a path from the leafs in the set to the root
	// are (temporarily) marked as full. now determine the real labels
	// and the lca (the nodes on the path from the lca to the root
	// stay full.. but this is no problem)
	// now determine the real labels
	lca = root->label_and_getlca();

	if( lca == NULL ){
		cerr << "label: result is NULL node"<<endl;
		exit( EXIT_FAILURE );
	}

	return lca;
}

// *****************************************************************************
// output fuction, the names of the genes are taken from nmap
// *****************************************************************************
template<typename T>
ostream & pqrtree<T>::named_output( vector<string> *nmap, ostream &os ) const {
	if( root != NULL )
		root->named_output(nmap, os);
	return os;
}

// *****************************************************************************
//
// *****************************************************************************

template<typename T>
void pqrtree<T>::normalise(  ){
	if( root != NULL ){
		root->normalise();
	}
}

// *****************************************************************************
// get a consistent permutation
// *****************************************************************************
template<typename T>
vector<T> pqrtree<T>::permutation() const {
	vector<T> f;
	root->front( f );
	return f;
}


// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrtree<T>::permutations( vector< vector<T> > &p, bool directed ) const{
	p = root->permutations( directed );
}
//template<typename T>
//void pqrtree<T>::permutations( vector< genom > &p ){
//	root->permutations( p, root );
//}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
unsigned pqrtree<T>::permutations_count(bool directed) const {
	unsigned count = 1;

	if( root != NULL ){
		root->permutations_count( count, directed );
	}else{
		cerr << "pqrtree::permutations_count(): NULL root"<<endl;
		exit( EXIT_FAILURE );
	}

	if( count > 0 ){
		return count;
	}else{
		return std::numeric_limits< unsigned >::max();
	}
}

// *****************************************************************************
//
// *****************************************************************************

template<typename T>
void pqrtree<T>::print_leaves(){
	cout << "leafs ";
	for( typename map<int, pqrnode<T>*>::iterator it = leafs.begin();
			it!=leafs.end(); it++ ){
		cout << it->first<<"("<< it->second->get_value() <<") ";
	}
	cout << endl;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
bool pqrtree<T>::reduce(T x, T y){
	vector<T> tmp(2);
	tmp[0] = x;
	tmp[1] = y;
	return reduce(tmp.begin(), tmp.end());
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
template<class _InputIterator>
bool pqrtree<T>::reduce(const _InputIterator &b, const _InputIterator &e){
	bool reducable = true;

#ifdef DEBUGPQR
	cerr << "reduce tree "<< *root<<endl;
	cerr <<"   with ";	copy(b, e, ostream_iterator<int>(cout, " ") );
	cerr << endl;
#endif//DEBUGPQR
	pqrnode<T> *lca; // the lca of the full nodes

		// step 1 and 2 of the algorithm
		// 1: label the leaves corresponding to elements of s as full
		// 2: label inner nodes depending on the children up to the lca
		// and get the lca
	lca = label(b, e);

#ifdef DEBUGPQR
	cout << "   label -> lca: "<< *lca << endl;
#endif//DEBUGPQR

		// continue to reduce the current lca until no further reduction has to
		// be done. this case is indicated by the lca->reduce function by
		// setting the lca to NULL
	while( lca != NULL ){
		if( !lca->reduce( &lca ) ){
			reducable = false;
		}
	}

	// reset nodes to empty
	reset();
	root->check();

	// transform all prime nodes with 2 children into q nodes
	root->transform_2childpq();

	return reducable;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
bool pqrtree<T>::reduce(const vector<set<T> > &sets){
	bool reducable = true;

	for(unsigned i=0; i<sets.size(); i++){

		if( !reduce(sets[i].begin(), sets[i].end()) ){
			reducable = false;
		}
	}

	return reducable;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrtree<T>::reduce_with_neutral(const vector<set<T> > &sets){
	vector<set<int> > blocking, neutral;

	blocking = get_blocking_subsets( sets );

	set_difference( sets.begin(), sets.end(), blocking.begin(), blocking.end(), back_inserter(neutral) );

	reduce(neutral);

}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrtree<T>::reset(){
	root->reset();
}

// *****************************************************************************
// output fuction
// *****************************************************************************
template<typename T>
ostream &operator<<(ostream &os, const pqrtree<T> &t) {

	if( t.root != NULL )
		os << *(t.root) ;
	return os;
}

// *****************************************************************************
// assignment operator
// *****************************************************************************
template<typename T>
pqrtree<T>& pqrtree<T>::operator= (const pqrtree<T>& in){
	if( root != NULL ){
		delete root;
	}

	if(in.root != NULL){
			// copy the tree
		root = new pqrnode<T>( *(in.root) );
			// assign the leafs
		root->get_leafs( leafs );
	}else{
		root = NULL;
	}
	return *this;
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
unsigned pqrtree<T>::size( bool leaf ) const{
	if( root == NULL ){
		return 0;
	}else{
		return root->size(leaf);
	}
}

// *****************************************************************************
//
// *****************************************************************************
template<typename T>
void pqrtree<T>::todot( ostream &os, vector<string> *nmap){
	os << "graph pqrtree {"<<endl;
    os << "    graph [rankdir=TB,nodesep=0]"<<endl;
    os << "    node [shape=box]"<<endl;
    os << "    edge [fontsize=10]"<<endl;
	os << "    fontname=Helvetica;"<<endl;
	os << "    graph[fontsize=20];"<<endl;
	os << "    ordering=out;"<<endl;
	root->todot(os, nmap);
	os << "{ rank = same; ";
	for( typename map<T, pqrnode<T>*>::iterator it=leafs.begin();
			it!=leafs.end(); it++){
		 os << "N"<<it->second <<";"<<endl;
//		 os << (it->second)->get_value()<<"."<<(it->second)->get_value() <<";"<<endl;
	}
	os << "}"<<endl;
	os << "} "<<endl;
}

#endif//PQRTREE_HPP
