/*
 * tdl_median.cpp
 *
 *  Created on: May 13, 2009
 *      Author: maze
 */

#include <algorithm>
#include <assert.h>
#include <iostream>
#include <iterator>
#include <deque>
#include "tdlmedian.hpp"


using namespace std;


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

tdl_bbmedian::tdl_bbmedian( ){}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned tdl_bbmedian::distance( const genom &g1, const genom &g2,
		const vector<genom> &gemomes ){

	int dir;
	return min_tdrl_distance(g1, g2, dir);
}


/**
 * branch and bound TDRL median solver. recursively branch over all
 * permutations and bound with chains
 *
 * @param[in] problem the median problem, i.e. 2 or more unsigned permutations
 * @param[in,out] chns chains data structure for each input permutations
 * 		(initialize to empty chain)
 * @param[in,out] tpe "to be  placed elements" (initialize to all elements)
 * @param[in,out] p the recursively constructed genome (initialize to empty)
 * @param[in,out] pinv the inverse of the recursively constructed genome
 * 		(initialise to (n+1)*std::numeric_limits< int >::max())
 * @param[out] medians the found medians
 * @param[out] score the score of the medians (initialize to std::numeric_limits< int >::max())
 */
void _tdl_bbmedian( const vector<genom> &problem, vector<chains> &chns,
		deque<int> &tpe, genom &p, vector<int> &pinv, vector<genom>&medians,
		int &score, bool allmed){

	int e,
		se,
		d=0;

//	cout << "_tdl_bbmedian() "<<p<< endl;
//	for( unsigned i=0; i<chns.size(); i++ ){
//		cout << "    chains    "<<i<<" "<< chns[i]<<" cnt " <<chns[i].cnt( ) <<" "<<endl;
//	}
//	cout << endl;

	// get the score (if p is a complete permutations) or
	// a lower bound (if p is partial)
	for( unsigned i=0; i<chns.size(); i++ ){
		d += tdrl_distance( chns[i].cnt( )  );
	}
//	cout << "     score "<<d<<" best "<<score<<endl;

	if( d > score || (!allmed && d >= score) ){
		return;
	}

	// if no elements are to be placed (i.e. deque is empty and permutation full)
	// then the permutation can be added to the medians if the score requirements are met
	if( tpe.size()==0 ){
//		int c = 0;;
//		for( unsigned i=0; i<chns.size(); i++ ){
//			c += tdrl_distance( p, problem[i] );
//		}
//		if( d!=c ){
//			cerr << "_tdl_bbmedian d"<<d<<"!=c"<<c<<endl;
//			exit(EXIT_FAILURE);
//		}
//		cerr << p<<endl;
		if( d <= score ){
			if ( d < score ){
				score = d;
				medians.clear();
//				cerr << "best score "<<score<<endl;
			}
			if( allmed == true || medians.size() == 0 ){
				medians.push_back(p);
				medians.back().set_nmap( problem[0].get_nmap() );
			}
		}
//		else{
//		}

	}else{	// otherwise: add next element to be placed and do branch and bound
		se = tpe.front();
		do{
			e = tpe.front();			// get the next element to be placed
			tpe.pop_front();
			pinv[abs(e)] = p.size();	// update inverse
			p.chromosom.push_back(e);	// .. and permutation

//			cerr<< "perm "; copy( p.begin(), p.end(), ostream_iterator<int>(cerr, " ") ); cerr <<endl;
//			cerr<< "pinv "; copy( pinv.begin(), pinv.end(), ostream_iterator<int>(cerr, " ") ); cerr <<endl;

			for( unsigned i=0; i<chns.size(); i++ ){
				chns[i].update(e);		// add last element in p to the chains data structure
			}

			_tdl_bbmedian(problem, chns, tpe, p, pinv, medians, score, allmed);	// b-n-b

			p.chromosom.pop_back();		// remove the last element from the permutation
			pinv[abs(e)] = std::numeric_limits< int >::max();			// update inverse
			for( unsigned i=0; i<chns.size(); i++ ){
				chns[i].update(e);		// update chains
			}
			tpe.push_back(e);			// and add the element back to the "to be placed" data structure
		}while( tpe.front() != se );	// when the first element is again at the beginning we can stop
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void tdl_bbmedian::solve(const vector<genom> &problem, bool allmed,
		int lower_bound, int upper_bound, int &mediancnt, int &score,
		vector<genom> &medians) {

//	cerr <<"TDRL median "<<endl<< problem << endl;

	deque<int> tpe;	// elements which are to be placed
	genom p;			// the permutation that is recursively built
	unsigned n;
	vector<chains> chns;
	vector<int> pinv;	// .. and their inverse
	vector<vector<int> > probleminv;		// inverses of the given permutations

	if(problem.size() < 2){
		cerr << "error: tdl_bbmedian::solve() got less then two permutations"<<endl;
		exit(EXIT_FAILURE);
	}

	medians.clear();
	n = problem[0].size();
	pinv = vector<int>( n+1, std::numeric_limits< int >::max());

//	if( problem.size() == 2 ){
//		int dir;
//		score = min_tdrl_distance(problem[0], problem[1], dir);
//		if(dir <= 0){
//			medians.push_back(problem[0]);
//		}else{
//			medians.push_back(problem[1]);
//		}
//		for( unsigned i=0; i<n; i++ ){
//			tpe.push_back( (dir <= 0) ? problem[0][i] : problem[1][i] );
//		}
//	}else{
		score = std::numeric_limits< int >::max();
		for( unsigned i=0; i<n; i++ ){
			tpe.push_back(problem[0][i]);
		}
//	}

	probleminv = vector<vector<int> >( problem.size() );
	for( unsigned i=0; i<problem.size(); i++ ){
		probleminv[i] = problem[i].inverse(false);
		chns.push_back( chains(n, &pinv, &(probleminv[i]), &(problem[i])) );
	}

	_tdl_bbmedian(problem, chns, tpe, p, pinv, medians, score, allmed);

	sort(medians.begin(), medians.end());
	mediancnt = medians.size();

//	cerr << medians.size()<<" TDRL medians"<<endl;
//	cerr << medians<<endl;
//	cerr << "score "<<score<<endl;


}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ostream &tdl_bbmedian::output( ostream &os ) const {
	os << "TDRL median(branch and bound)";
	return os;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


tdl_bfmedian::tdl_bfmedian( ){}

unsigned tdl_bfmedian::distance( const genom &g1, const genom &g2,
		const vector<genom> &gemomes ){

	int dir;
	return min_tdrl_distance(g1, g2, dir);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


void tdl_bfmedian::solve( const vector<genom> &problem, bool allmed,
		int lower_bound, int upper_bound, int &mediancnt, int &score,
		vector<genom> &medians ){

	genom tmp = genom( problem[0].size(), 0);
	int d;
	medians.clear();
	score = std::numeric_limits< int >::max();

	do{
		d = 0;
		for( unsigned i=0; i<problem.size(); i++ ){
			d += tdrl_distance(tmp, problem[i]);
		}

		if( d <= score ){
			if( d < score ){
				score = d;
				medians.clear();
			}
			medians.push_back( tmp );
		}
	}while(next_permutation( tmp.chromosom.begin(), tmp.chromosom.end() ));
	mediancnt = medians.size();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


ostream &tdl_bfmedian::output( ostream &os ) const {
	os << "TDRL median(brute force)";
	return os;
}
