#ifndef ILP_RRRMT_HPP_
#define ILP_RRRMT_HPP_

#include <set>

#include "io.hpp"
#include "costfoo.hpp"
#include "common.hpp"
#include "helpers.hpp"
#include "rearrangements.hpp"
#include "tdl.hpp"

using namespace std;

class ilp_rrrmt : public unordered{
	private:

	public:
	/**
	 * construct an empty szenario
	 */
	ilp_rrrmt();
	/**
	 * constructor: get the scenario from g1 to g2
	 * @param[in] g1 a genome
	 * @param[in] g2 another genome
	 * @param[in] cst cost function
	 */
	ilp_rrrmt(const genom &g1, const genom &g2, const costfoo *cst);

	/**
	 * copy constructor
	 * @param[in] c the original
	 */
	ilp_rrrmt(const ilp_rrrmt &c );

	/**
	 * destructor
	 */
	~ilp_rrrmt();

	/**
	 * @return "ilp_rrrmt"
	 */
	string typestr() const;
};

#endif/*ILP_RRRMT_HPP_*/
