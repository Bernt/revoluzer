// version.h

#ifndef VERSION_H
#define VERSION_H

#define PROJECT_VERSION_MAJOR 0
#define PROJECT_VERSION_MINOR 1
#define PROJECT_VERSION_PATCH 6

#endif // VERSION_H

