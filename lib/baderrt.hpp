/*
 * baderrt.h
 *
 * wrapper to compute weighted inv + tra + itra scenario using the code of martin bader
 *
 *  Created on: May 5, 2014
 *      Author: maze
 */

#ifndef BADERRT_H_
#define BADERRT_H_

#include "costfoo.hpp"
#include "rearrangements.hpp"

#include "weightedbb.h"

namespace std {

class baderrt : public ordered {
public:
	baderrt();

	/**
	 * @param[in] ipt weighted inversion + transp distance iff true .. otherwise only transp
	 * @param[in] circular reat gene order as circular
	 */
	baderrt( const genom &g1, const genom &g2, const costfoo *cst, bool ipt, bool circular);

	virtual ~baderrt();
};

} /* namespace std */

#endif /* BADERRT_H_ */
