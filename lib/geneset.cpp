/*
 * geneset.cpp
 *
 *  Created on: Aug 23, 2010
 *      Author: maze
 */

#include <algorithm>
#include <iostream>
#include <iterator>

#include "geneset.hpp"

using namespace std;

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//geneset::geneset(){
//	_nmap = NULL;
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//geneset::geneset( vector<string> *nmap ){
//	_nmap = nmap;
//}
//
//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//ostream &operator<<(ostream &out, const geneset &g) {
//
//	for( set<int>::const_iterator it = g.begin(); it != g.end(); ){
//		print_element( *it, out, 1, "", g._nmap );
//		it++;
//		if( it != g.end() )
//			out << " ";
//	}
//	return out;
//}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

geneset::geneset(){
	_nmap = NULL;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

geneset::geneset( vector<string> *nmap, unsigned n ){
	_nmap = nmap;
	_ie = vector<bool>(n+1, false);
	_base = vector<bool>(n+1, false);
	for( unsigned i=1; i<=n; i++ )
		_base[i] = true;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

geneset::geneset( vector<string> *nmap, unsigned n, const vector<int> &base ){
	_nmap = nmap;
	_ie = vector<bool>(n+1, false);
	_base = vector<bool>(n+1, false);
	for( unsigned i=0; i<base.size(); i++ )
		_base[ base[i] ] = true;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

vector<bool>::const_iterator geneset::begin( ) const{
	vector<bool>::const_iterator it;
	for( it=_ie.begin(); it!=_ie.end(); it++ ){
		if( *it )
			break;
	}
	return it;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void geneset::clear(){
	_ie = vector<bool>( _ie.size(), false );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

character *geneset::clone() const{
	return new geneset(*this);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

character *geneset::create() const{
	return new geneset();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void geneset::complement(){
//	copy(_base.begin(), _base.end(), ostream_iterator<bool>(cout,"" ));cout << endl;
//	cout << "complement("<<*this<<") = ";
	for( unsigned i=1; i<_ie.size(); i++ ){
		if( _base[i] )
			_ie[i] = !_ie[i];
	}
//	cout << *this<<endl;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

vector<bool>::const_iterator geneset::end( ) const{
	return _ie.end();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

vector<bool>::const_iterator geneset::find( unsigned e ) const{

	if( e < _ie.size() && _ie[e] ){
		return _ie.begin()+e;
	}else{
		return _ie.end();
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

set<int> geneset::get_set() const{
	set<int> r;
	for( unsigned i=0; i<_ie.size(); i++ ){
		if( _ie[i] )
			r.insert( i );
	}
	return r;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void geneset::insert( int e ){
	if( !_base[abs(e)] ){
		cerr << "element "<<abs(e)<<" not allowed in gene set"<<endl;
		exit(EXIT_FAILURE);
	}

	_ie[ abs(e) ] = true;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

template<typename _InputIterator>
void geneset::insert( _InputIterator __first, _InputIterator __last ){
	for (; __first != __last; ++__first){
		insert( *__first );
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool operator< (const geneset &g1, const geneset &g2){
	return g1._ie < g2._ie;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ostream &operator<<(ostream &out, const geneset &g) {
	bool f = true;
	for( unsigned i=0; i<g._ie.size(); i++){
		if( g._ie[i] ){
			if( !f )
				out << " ";
			print_element( i, out, 1, "", g._nmap );
			f = false;
		}
	}
	return out;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void geneset::output(ostream &out) const {
	out << *this;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool geneset::operator ==(const geneset& b) const{
	return (_ie == b._ie);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

unsigned geneset::size() const{
	unsigned s = 0;
	for( unsigned i=0; i<_ie.size(); i++ )
		if( _ie[i] )
			s++;
	return s;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

geneset_weight::geneset_weight( unsigned n, bool circular, SgnHandType sh, bool directed ) : character_weight(){

	_circular = circular;
	_n = n;
	if( _circular ){
		_pr = pascal_row( _n - 1  );
	}else{
		_pr = pascal_row( _n );
	}
	_directed = directed;
	_pr = pascal_row( _n );
	_sh = sh;
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

geneset_weight::~geneset_weight(){
	_pr.clear();
}

//// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//double geneset_weight::operator()(const character *c ) const{
//	return (*this)( *c );
//}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

double geneset_weight::operator()(const character *cc ) const{
	double p = 0;	// the probability
	unsigned n = 0,// length of the genomes adapted for the formulas
		l = 0,		// length of the cluster
		fixed = 0;	// numer of fixed elements in the cluster

	geneset *c;
	c = (geneset *)cc;

	// determine fixed elements, i.e. elements in the given gene set
	// where the corresponding partner is not included. these elements
	// can only be the first or last elements in the cluster. this
	// is to be regarded in the computation.
	if( _sh == DOUBLE ){
		for ( unsigned i=1; i<=_n; i++){
			// if only one of 2i and 2i-1 is included -> fixed
			if( ( c->find( 2*i ) != c->end()) ^  (c->find( 2*i-1 ) != c->end() ) ){
				fixed++;
			}
		}
	}
	// for directed gene orders also 0 and _n+1 are fixed. so check if they
	// are in the cluster
	if( _directed ){
		if( c->find( 0 ) != c->end() ){
			fixed++;
		}
		if( c->find( (2 * _n)+1 ) != c->end() ){
			fixed++;
		}
	}
	// determine clustersize
	if( _sh == DOUBLE ){
		l = (c->size() - fixed)/2;
	}else if( _sh == UNSIGN ){
		l = c->size() - fixed;
	}else{
		cerr << "geneset_weight: unknown sign handling type"<<endl;
		exit(EXIT_FAILURE);
	}

	// determine n
	if( _circular ){
		n = _n-1;
	}else{
		n = _n;
	}

	// compute probability that the cluster appears by chance
	switch( fixed ){
	case 0:	// I
		p = (n-l+1)/_pr[l];
		break;
	case 1: // II
		p = 1/_pr[l];
		break;
	case 2: // III
		p = 1/((n-l) *_pr[l] * 2) ;
		break;
	default:
		cerr << "geneset_weight: more than 2 fixed elements"<<endl;
		exit(EXIT_FAILURE);
		break;
	}

//	cerr << "gsw "<<l<<" "<<fixed<<" " <<w<<" - "<<  *c<<endl;
	// return weight: 1-probability
	return 1-p;
}
