/** @file common_old.hpp
 * old common interval functions
 * - functions for calling stoyes code
 * - srac
 * - valid adjacencies for the modified version of capraras median solver
 */

#ifndef _COMMON_OLD_HPP_
#define _COMMON_OLD_HPP_

#include <vector>

#include "genomes.h"	// includes for common intervals
#include "irreducible.h"
#include "genom.hpp"
#include "common.hpp"

/**
 * determines how much common intervals of genomes are destroyed
 * by the genom g
 *@param[in] genomes the genomes
 *@param[in] g the added genom
 *@param[in,out] ci_pre the common intervals of genomes; if not provided they are computed inside the function
 *@param[out] ci_dif the difference common intervals 
 *@param[in] sig signed common intervals if 1
 *@param[in] circular treat genomes as circular genomes
 *@return the number of destroyed common intervals
 */
int changes_common_intervals(
	const vector<genom> &genomes, const genom &g, 
	vector<pair<unsigned, unsigned> > &ci_pre, 
	vector<pair<unsigned, unsigned> > &ci_dif, 
	int sig, int circular);

/**
 * C function determines how much common intervals of genomes are destroyed
 * by the genom g
 *@param[in] genomes_ptr the genomes
 *@param[in] m number of genomes in genomes_ptr
 *@param[in] g_ptr the added genom
 *@param[in] n length of the genomes
 *@param[in,out] ci_pre_ptr the common intervals of genomes; if not provided they are computed inside the function
 *@param[in,out] ci_pre_size number of the common intervals of genomes
 *@param[in,out] ci_dif_ptr the diference common intervals 
 *@param[in,out] ci_dif_size number of difference common intervals in ci_pre_ptr
 *@param[in] sig signed common intervals if 1
 *@param[in] circular treat genomes as circular genomes
 *@return  the number of destroyed common intervals
 */
extern "C" int changes_common_intervals(
	struct genome_struct **genomes_ptr, int m, 
	int *g_ptr, int n, 
	unsigned ***ci_pre_ptr, unsigned *ci_pre_size,
	unsigned ***ci_dif_ptr, unsigned *ci_dif_size, 
	int sig, int circular);

/**
 * compare two ranges. The last element of the ranges is the union of all ranges. if the size of the first ranges
 * is smaller than the second range return true, else false.
 *@param[in] r1 first range
 *@param[in] r2 second range
 *@return true if \f$ r1<r2 \f$, else false
 */
int compareRange(vector<pair<unsigned, unsigned> > r1, vector<pair<unsigned, unsigned> > r2);

/**
 * find the (irreducible) common intervals of the given genomes. 
 * the code from Jens Stoye is used. 
 *
 * note: intervals of size 1 are not included (as defined by Stoye)
 *
 *@param[in] g1 genome 1
 *@param[in] g2 genome 2
 *@param[in] sig if true use sign information for the common intervals
 *@param[in] irreducible return only the irreducible intervals if equal one
 *@param[in] circular treat genomes as circular genomes
 *@param[in] replace replace bordercrossing intervals (only for circular)
 *@return the (irreducible) common intervals
 */
vector<pair<unsigned, unsigned> > find_common_intervals(
		const genom &g1, const genom &g2, 
		int sig, int irreducible, int circular, int replace);

/**
 * find the common intervals of the given genomes. the code from Jens Stoye is used.
 * there are some anomalies for irreducible common intervals in circular genomes:
 * - intervals crossing the border (in one of the genomes) are replaced by their complement, 
 * 	if it is irreducible
 * - with signed irreducible common intervals the sign restriction does not hold for the 
 * 	complement (the order restrictions do still hold). therefor the unreplaced irreducible 
 *	common intervals have to be returned too.
 *
 * note: intervals of size 1 are not included (as defined by Stoye)
 *
 *@param[in, out] g the genomes
 *@param[in] sig if true use sign information for the common intervals
 *@param[in] irreducible return only the irreducible intervals if equal one
 *@param[in] circular treat genomes as circular genomes
 *@param[in] replace replace bordercrossing intervals (only for circular)
 *@return the (irreducible) common intervals
 */
vector<pair<unsigned, unsigned> >  find_common_intervals( vector<genom> g,
	int sig, int irreducible, int circular, 
	int replace);



/**
 * checks if a set of common intervals is reducible, the irreducible are added to the known
 * and reducible intervals are discarded
 *@param[in,out] common_intervals irreducible intervals known so far
 *@param[in] candidates intervals which are of unknown 'reducibility'
 *@param[in] n length of the genomes which are the basis of the given ci
 */
void remove_reducible(vector<pair<unsigned, unsigned> > &common_intervals, 
		const vector<pair<unsigned, unsigned> > &candidates, unsigned n);

/**
 * sorting by reversals with all common intervals. sorts p by sorting each range 
 * in ranges. 
 * - for each set of ranges \f$ c_1, \ldots ,c_m \f$ 
 * - sort \f$ c_i \f$ (for \f$ i=1,\ldots ,m \f$ ) c+ or c- and add the number of 
 * needed reversals to the distance
 * - if not decidable if c+ or c- should be taken .. try both (recursively)
 * - sort \f$ C=\bigcup c_i \f$ 
 *.
 *@param[in] p the permutation to sort
 *@param[in] ranges sort without cutting any range ind ranges
 *@param[in] cur_range_idx the current range set index
 *@param[in] cur_subrange_idx the current range index
 *@param[in] rd reversal distance
 *@return the smallest possible number of reversals needed to sort p without 
 * destroying the common intervals of p
 */
int srac(genom p, vector<vector<pair<unsigned, unsigned> > > ranges, 
	unsigned cur_range_idx, unsigned cur_subrange_idx, dstnc_inv *rd );

/**
 * sorting by reversals with all common intervals. The algorithm is from 
 * Figeac and Varre: 'Sorting By Reversals with Common Intervals' but slightly
 * modified.
 * - the funtion transforms the input permutations to \f$ \pi \f$ and \f$ \iota \f$
 * - computes the irreducible common intervals of \f$ \pi \f$ and determines 
 * the sets of overlapping intervals 
 * - for each overlap component the ranges wich can be sorted independently
 * are determined (the shortest subintervals between the two common interval borders)
 * - the distance is computed by srac(p, ranges, 0,0)
 *.
 *@param[in] g1 genom one
 *@param[in] g2 genom two
 *@param[in] sig if true use sign information for the common intervals
 *@param[in] circular treat genomes as circular genomes
 *@return the smallest possible number of reversals needed to sort g1 to g2 without 
 * destroying the common intervals of g1 and g2
 */
int srac(genom &g1, genom &g2, int sig, int circular );

/**
 * C function to get the reversaldistance preserving the common intervals
 *@param[in] src_p source genom (int array)
 *@param[in] tgt_p target genom (int array)
 *@param[in] sig if true use sign information for the common intervals
 *@param[in] circular treat genomes as circular genomes
 *@param[in] n length of the genomes
 *@return the preserving reversal distance
 */
extern "C" int srac_c(int *src_p, int *tgt_p, int sig, int circular,
	int n);


/**
 * compute for a subpermutation \f$ c=p([range]) \f$ (with the elements renamed,
 * such that \f$ c=\{1,..,len(range)\}\f$ ) the distance \f$ d(c,+c) \f$ and \f$ d(c,+c) \f$, 
 * where \f$ c+ \f$ is the positive identity permutation and \f$ c- \f$  is the 
 * negative identity permutation. 
 * - the smaller distance is returned
 * - next_perm is set to p where the range is sorted to the positive (negative) 
 * 	if \f$ d(c,+c) <= d(c,-c) \f$ (\f$ d(c,-c) < d(c,+c) \f$)
 * - if \f$ d(c,+c) = d(c,-c) \f$ then alter is set to p where the range is sorted to the negative 
 * - if the range is the whole permutation sorting is always done to the positive identity
 *.
 *@param[in] p the permutation 
 *@param[in] range the range to sort
 *@param[out] next_perm the permutation realisable with the smaller number of reversals
 *@param[out] alter the alternative if not decidable if to sort to the positive or negative
 *@param[in] rd reversal disatnce
 *@return the number of reversals needed
 */
int sub_genom_distance(genom p, pair<unsigned, unsigned> range, 
	genom &next_perm, genom &alter, dstnc_inv *rd);



#endif//_COMMON_OLD_HPP_
