#include <vector>
#include <map>

#include "dstnc.hpp"
#include "genom.hpp"

using namespace std;



struct bintree{
		/*! the root node of the tree*/
		struct node *root;
		/*! the vector of leafs of the tree */
		//~ vector< node * > leafs;
		/*! true if the genomes of the nodes of the tree are circular; else false  */
		bool circular;

		dstnc *_dst;
};

/**
 * free the memory of the tree structure and all the nodes of the tree
 *@param[in] bt the tree to free
 */
void free_tree(bintree *bt);


/**
 * init an empty bintree from a newick string
 *@param[in] newick the string 
 *@return the bintree
 */
bintree * init(string newick, dstnc *dst);

/**
 * function to initialise a bintree given in newick tree format, the genomes of the nodes 
 * are constructed by r random reversals from its parent nodes genome
 * 
 *@param[in] newick newick representation
 *@param[in] id the genome of the root node (the identity i would suggest)
 *@param[in] r number of reversals to apply per branch
 *@param[out] applied number of applied reversals 
 *@param[in] hd helping memory for genome functions
 *@return the binary tree
 */
bintree * init(string newick, genom id, int r, int &applied, dstnc *dst);

/**
 * function to initialise a bintree given the output from GRAPPA, ie. the newick 
 * representation and the genomes and names of the nodes in reverse preorder
 *@param[in] newick newick representation
 *@param[in] genomes the genomes
 *@param[in] names the names 
 *@param[in] hd helping memory for genome functions
 *@return the binary tree
 */
bintree * init(string newick, vector<genom> &genomes, vector<string> &names, dstnc *dst);

/**
 * function to initialise a bintree given the edge list 
 * the genomes 
 *@param[in] edges the edge list
 *@param[in] genomes the genomes
 *@param[in] hd helping memory
 *@return the binary tree
 */
bintree * init(vector<pair<unsigned, unsigned> > &edges, 
	vector<genom> &genomes, vector<string> &names, dstnc *dst);

/**
 * get the  genomes of the inner nodes of a bintree
 *@param[in] bt the bintree
 *@param[out] names the names of the genomes
 *@param[out] genomes the leaf genomes
 */
void inner_genomes(bintree *bt, vector<string> &names, vector<genom> &genomes);

/**
 * checks if the bintree is rooted
 *@param[in] bt the bintree
 *@return true if rooted, false else
 */
bool isrooted(bintree *bt);

/**
 * get the leaf genomes of a bintree
 *@param[in] bt the bintree
 *@param[out] names the names of the genomes
 *@param[out] genomes the leaf genomes
 */
void leaf_genomes(bintree *bt, vector<string> &names, vector<genom> &genomes);

/**
 * get the names of the leafs
 * @param[in] bt the binary tree
 * @param[out] the names of the leafs
 */
void leaf_names(bintree *bt, vector<string> &names);

/**
 * compute the lower bound of a given tree
 * @param[in] bt the binary tree
 * @param[in] dst a distance function
 * @return the lower bound
 */
int lower_bound(bintree *bt, dstnc *dst, hdata &hd);

/**
 * return the newick representation of the tree
 *@param[in] bt the tree
 *@param[in] distance print the distances (edge length)
 *@param[in] inner names of the inner nodes
 *@param[in] normalise sort the tree
 *@return the newick representation
 */
string newick(bintree *bt, bool distance, bool inner, bool normalise);

/**
 * return the newick representation of the tree
 *@param[in] bt the tree
 *@param[in] distance print the distances (edge length)
 *@param[in] inner names of the inner nodes
 */
void print_newick(bintree *bt, bool distance, bool inner);

/**
 * return the split- / robinson foulds distance between two trees
 *@param[in] bt1 trrrrreeeeee1 
 *@param[in] bt2 trrrrreeeeee2 
 *@return the distance
 */
int robinson_foulds(bintree *bt1, bintree *bt2);

/**
 * root a given (unrooted) tree 
 * @param[in,out] bt the binary tree
 */
void root(bintree *bt);

/**
 * the sum of the reversal distances of the edges
 *@param[in] bt the binary tree
 *@return the sum
 */
int score(bintree *bt);

/**
 * sort the tree (see node sort)
 *@param[in] bt the tree
 */
void sort(bintree *bt);

/**
 * get all triples of the tree, i.e. the three neighbours of each inner node
 *@param[in] bt the binary tree
 *@param[out] trips the triples
 *@param[out] trips_names the names of the genomes
 *@param[out] medians the inner nodes, i.e. the medians of the triples
 *@param[out] meds_names the names of the genomes
 */
void triples(bintree *bt, 
	vector<vector<genom> > &trips, vector<vector<string> > &trips_names, 
	vector<genom> &meds, vector<string> &meds_names);


/**
 * makes a rooted tree unrooted
 *@param[in] bt the tree to unroot
 */
void unroot(bintree *bt);

/**
 * class for a binary tree
 */
//~ class bintree{
	//~ friend class node;
	//~ private:
		//~ /*! the root node of the tree*/
		//~ node *root;
		//~ /*! the vector of leafs of the tree */
		//~ vector< node * > leafs;
		//~ /*! true if the genomes of the nodes of the tree are circular; else false  */
		//~ bool circular;
	
		//~ hdata hd;
	//~ public:
		/**
		 *constructor
		 */
		//~ bintree(string newick, vector<genom> &genomes, vector<string> &names,  hdata &hd);

		/**
		 * constructor: builds an empty bintree (only the root)
		 * and with the genomes as leafs (without connections between the nodes)
		 *@param genomes the genomes
		 *@param c circular genomes ?
		 */
		//~ bintree(vector<genom> genomes, bool c);

		/**
		 * function tries to set the root pointer to a neighbour node 
		 * wich is more central, if possible
		 *@param ...
		 *@return true if better node found; false if root is allready central
		 */
		//~ bool centralizeRoot();

		/**
		 * calculates the minimal distance to genome g
		 *@param g a genome
		 *@param[in] hd helping memory for genom functions
		 *@return the distance 
		 */
		//~ int distance(genom g, hdata &hd);

		/**
		 * calculates the sum of all edge-distances
		 *@return the distance sum
		 */
		//~ int distanceSum(hdata &hd);

		//~ hdata get_hdata();

		/**
		 * getter for the inner nodes of the tree
		 *@return the inner nodes
		 */
		//~ vector<node *> getInnerNodes();

		/**
		 * getter for the root node of the tree
		 *@return a pointer to the root node
		 */
		//~ node *getRoot();

		/**
		 * getter for the leaf nodes of the tree
		 *@return a vector of pointers to the leaf nodes
		 */
		//~ vector< node * > getLeafs();

		/**
		 * returns the maximum height of the tree
		 * @todo correct & more comments
		 *@return the maximum height
		 */
		//~ int maxLeafDistance();

		/**
		 * getter for the next unresolved node in the tree (=of the root node)
		 *@return pointer to the node
		 */
		/*node *getNextUnresolvedNode();*/

		/**
		 * sets the node r as the root
		 *@param r the new root
		 */		
		//~ void setRoot(node *r);
		
		/**
		 * shakes the tree (tries to move succesive all nodes by 1 (reversal
		 * distance) until no further improvement)
		 */
		//~ void shake(hdata &hd);

		/**
		 * output operator for the tree (outputs the tree in newick format)
		 *@param out the stream to output in
		 *@param t the tree to output
		 *@return output 
		 */
		//~ friend ostream & operator<<(ostream &out, bintree &t);

		/**
		 * prints the tree (all genomes of the tree)
		 */
		//~ void output(void);

		/**
		 * outputs the tree in tulip format
		 *@param filename name of the file to write in
		 */
		//~ void output_tulip(char *filename);
		
		/**
		 * if the tree is a rooted tree (root has no parent) the tree will be transformed in
		 * an unrooted tree by assinging the left and right childs of the root as parent of 
		 * each other. The new Node will get the node with the greater maximal distance to a leaf
		 */
		//~ void unRoot();
//~ };

/**
 * compares the level of 2 nodes (for sorting)
 */
//~ int compareLevel(node *, node *);
