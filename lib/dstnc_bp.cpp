/*
 * bpdist.cpp
 * calculate breakpoint distance
 * @uthor: M. Bernt
 */

#include "dstnc_bp.hpp"
#include "helpers.hpp"

#include <limits>

dstnc_bp::dstnc_bp( unsigned n, bool circular, bool directed ){
	_n = n;
	_circular = circular;
	_directed = directed;
	_ssuc = vector<int>(_n+1);
	_tsuc = vector<int>(_n+1);
//	cerr << "dstnc_bp::dstnc_bp("<<n<<")"<<endl;
}

dstnc_bp::~dstnc_bp(){
}

void dstnc_bp::adapt(unsigned n){
	_n = n;
	_ssuc.resize(_n+1);
	_tsuc.resize(_n+1);
//	cerr << "dstnc_bp::adapt("<<n<<")"<<endl;
}

unsigned dstnc_bp::calc( const genom &src, const genom &tgt ){
//	vector<int> _ssuc(_n+1),	// the successor of each gene in src
//		_tsuc(_n+1);			// and tgt
	unsigned bp = 0;

//	cerr <<"src "<< src<<endl;
//	cerr <<"tgt "<< tgt<<endl;

	// standard definition of a breakpoint for \pi, \sigma:
	// number of adjacencies in \pi which are not adjacencies in \sigma.
	// this works fine if gene content is equal (i.e. no deletions and duplications).
	//
	// the following implementation handles deletions correctly
	// for each of the elements [1:n] the successor in \pi and \sigma is determined
	// (where the successor of a positive element is the next and the previous
	// for a negative element);
	// the breakpoint distance is the number elements [1:n] which have a different
	// successor in \pi and \sigma
	// - if an element is deleted the successor is set to \infty;
	// - circular genomes are treated in a circular fashion;
	// - for directed genomes an auxilliary element 0 at position 0 is used.

	// reset successors
	_ssuc.assign( _n+1, std::numeric_limits< int >::max() );
	_tsuc.assign( _n+1, std::numeric_limits< int >::max() );

	// determine the successors of the genes in src
	for( unsigned i=0; i<src.size(); i++ ){
		int sgn = sign( src[i] );

		if( i+sgn < src.size() && i+sgn >= 0 ){
			_ssuc[ sgn*src[i] ] = sgn*src[i+sgn];
		}else{
			if( _circular ){
				if( i==0 ) _ssuc[ sgn*src[i] ] = sgn*src[src.size()-1];
				else _ssuc[ sgn*src[i] ] = sgn*src[0];
			}else{
				if( i==0 ) _ssuc[ sgn*src[i] ] = 0;//???
				else _ssuc[ sgn*src[i] ] = sgn*(_n+1);
			}
		}
	}
	if( _directed )
		_ssuc[0] = src[0];

//	cerr << "src suc ";
//	copy(_ssuc.begin(), _ssuc.end(), ostream_iterator<int>(cerr, " ")); cerr<<endl;

	// determine the successors of the genes in tgt
	for( unsigned i=0; i<tgt.size(); i++ ){
		int sgn = sign( tgt[i] );

		if( i+sgn < tgt.size() && i+sgn >= 0 ){
			_tsuc[ sgn*tgt[i] ] = sgn*tgt[i+sgn];
		}else{
			if( _circular ){
				if( i==0 ) _tsuc[ sgn*tgt[i] ] = sgn*tgt[tgt.size()-1];
				else _tsuc[ sgn*tgt[i] ] = sgn*tgt[0];
			}else{
				if( i==0 ) _tsuc[ sgn*tgt[i] ] = 0;//???
				else _tsuc[ sgn*tgt[i] ] = sgn*(_n+1);
			}
		}
	}
	if( _directed )
		_tsuc[0] = tgt[0];

//	cerr << "tgt suc ";
//	copy(_tsuc.begin(), _tsuc.end(), ostream_iterator<int>(cerr, " ")); cerr<<endl;

	// count break points
	for( unsigned i=0; i<_ssuc.size(); i++ ){
		if( _ssuc[i] != _tsuc[i] )
			bp++;
	}
	return bp;

//	// Test for valid chromosom
//	if ( _n != src.size() || _n != tgt.size() ) {
//		cerr << "error: dstnc_bp size mismatch"<<endl;
//		exit( EXIT_FAILURE );
//	}
//
//	unsigned bp = 0;
//	vector<int> g = src.identify(tgt);
//
//	for(unsigned j=0; j<g.size()-1; j++){
//		if( g[ j+1 ] - g[ j ] != 1 ){
//			bp++;
//		}
//	}
//	if( _directed && g[0]!=1 )
//		bp++;
//
//	if( (_directed || _circular) && g.back() != _n )
//		bp++;
//
//	return bp;
}

dstnc* dstnc_bp::clone() const{
	return new dstnc_bp( _n, _circular, _directed );
}


//dstnc* dstnc_bp::create() const{
//	return new dstnc_bp();
//}

ostream & dstnc_bp::output(ostream &os) const{
	os << "breakpoint distance for n="<<_n;
	return os;
}


///////////////////////////////////////////////////////////////////////////////

//int breakpoints(const vector<genom> &genomes, hdata &hd){
//
//	vector<bool> breakpoints(genomes[0].size()+1, 0);
//	vector<int> g;
//
//	for( unsigned i=1; i<genomes.size(); i++){
//		g = genomes[0].identify(genomes[i], hd);
//
//		for(unsigned j=0; j<g.size()-1; j++){
//			if( breakpoints[abs(g[j])] == 1)
//				continue;
//
//			if(g[j] >= 0){
//				if(g[j+1] - g[j] != 1){
//					breakpoints[ g[j] ] = 1;
//				}
//			}else if(g[j] < 0){
//				if( g[j] - g[j-1] != 1){
//					breakpoints[ -1*g[j] ] = 1;
//				}
//			}
//		}
//	}
//	//~ cout << "->"<<endl;
//	//~ for (unsigned i = 1; i < breakpoints.size(); i++) {
//		//~ cout.width(3);
//		//~ cout << i << " ";
//	//~ }
//	//~ cout << endl;
//
//	//~ for (unsigned i = 1; i < breakpoints.size(); i++) {
//		//~ cout.width(3);
//		//~ cout << breakpoints[i] << " ";
//	//~ }
//	//~ cout << endl;
//
//	return  sum(breakpoints);
//}
