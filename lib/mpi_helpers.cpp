#ifdef USEMPI

#include "mpi_helpers.hpp"

using namespace std;

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* GENOME VECTOR HANDLING FUNNCTIONS +++++++++++++++++++++++++++++++++++++++ */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void genomVector2intVector(const vector<genom> &gv, vector<int> &iv, const unsigned &n){
	unsigned offset = 0;	// the current position in the intvector
	vector<int> c;			// temporary chromosome
		// clear the int vector and resize it to its final size
	iv.clear();
	iv.resize(gv.size()*n);

		// copy the chromosomes from each genome into the intvector
	for(unsigned i=0; i<gv.size(); i++){
		c = gv[i].getChromosom();
		memcpy(&iv[offset], &c[0], n*sizeof(int));
		offset += n;
		c.clear();
	}
}

void intVector2genomVector(const vector<int> &iv, vector<genom> &gv, const unsigned &n, const char &circ){
	genom temp(n, circ);	// temporary genome with the wanted circularity
	vector<int> c(n);		// temporary chromosome
	int offset = 0;		// current offset in the intvector

		// clear the intvector and resize to its final size
	gv.clear();
	gv.reserve(iv.size()/n);

		// copy frames of length n into the temporary genome and push it on the genome vector
	for(unsigned i=0; i<iv.size()/n; i++){
		memcpy(&c[0], &iv[offset], n*sizeof(int));
		temp.setChromosom(c);
		gv.push_back(temp);
		offset += n;
	}
}

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* MPI HELPER & WRAPPER FUNNCTIONS +++++++++++++++++++++++++++++++++++++++++++*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void SendGenomeVector(const vector<genom> &g, unsigned from, unsigned to, 
				vector<int> &i, unsigned &uBuf, const unsigned &n, 
				int dest, MPI::Request &req){
	if(to > g.size())
		to = g.size();
	//~ cout << "from "<<from <<" to "<<to<<endl;
	vector<genom> part;
	part.insert(part.begin(), g.begin()+from, g.begin()+to);

		// construct an integer vector from the genom vector
	genomVector2intVector(part, i, n);
		// transfer the size to the dest
	uBuf = i.size();
	MPI::COMM_WORLD.Isend( &uBuf, 1, MPI::UNSIGNED, dest, 0);
	if(uBuf>0){
			// transfer the integer vector to the dest
		req = MPI::COMM_WORLD.Isend( &i[0], i.size(), MPI::INT, dest, 2);
	}
}

//~ void SendGenomeMap(const map<genom, vector<bool> > &gm, vector<genom> &gv, vector<int> &i, const unsigned &n, int dest, MPI::Request &req){
	//~ gv.clear();
	//~ gv.reserve(gm.size());
	
	//~ for(map<genom, vector<bool> >::const_iterator it=gm.begin(); it!=gm.end(); it++){
		//~ gv.push_back(it->first);
	//~ }
	//~ SendGenomeVector(gv, 0, gv.size(), i, n, dest, req);
//~ }

MPI::Request RecvGenomeVector(vector<int> &i, unsigned &size, 
		vector<genom> &g, char circ, const unsigned &n,
		int source, bool blocking, unsigned step){

	MPI::Request req = MPI::REQUEST_NULL;

	if(blocking){
		MPI::COMM_WORLD.Recv(&size, 1, MPI::UNSIGNED, source, 0);
		//~ cout << MPI::COMM_WORLD.Get_rank()<<" RECV "<< size<<endl;
		if(size>0){
			i.resize(size);
			MPI::COMM_WORLD.Recv( &i[0], size, MPI::INT, source, 2);
			intVector2genomVector(i, g, n, circ);
		}
	}else{
		switch (step){
			case 0: {
				req = MPI::COMM_WORLD.Irecv(&size, 1, MPI::UNSIGNED, source, 0);
				break;
			}
			case 1: {
				i.resize(size);
				if(size>0){
					req = MPI::COMM_WORLD.Irecv( &i[0], size, MPI::INT, source, 2);
				}
				break;
			}
			case 2: {
				intVector2genomVector(i, g, n, circ);
				break;
			}
			default:{
				cout << "RecvGenomeVector: unknown step"<<endl;
				break;
			}
		}
	}
	
	return req;
/*	
	//~ int size=0;
	//~ MPI::COMM_WORLD.Irecv( &size, 1, MPI::INT, source, 0);
	i.resize(size);
	if(size>0){
			// make the receive buffer large enough and receive the int vector
		req = MPI::COMM_WORLD.Irecv( &i[0], size, MPI::INT, source, 2);
			// transform the int vector to a genom vector
		//~ intVector2genomVector(i, g, n, circ);
		//~ cout << "received "<<g.size();
	}
*/
}

void BcastGenomeVector(vector<genom> &g, char circ, vector<int> &i, const unsigned &n){

		// the root node has to construct the integer vector 
	int size=0;

	if(MPI::COMM_WORLD.Get_rank() == 0){
		genomVector2intVector(g, i, n);
		size = i.size();
		//~ for(unsigned i=0; i< g.size(); i++)
			//~ cout << "bi"<< i<<" c "<<(int)g[i].getCircular()<<endl;
	}

		// broadcast the size of the following message
	//~ MPI::COMM_WORLD.Barrier();
	MPI::COMM_WORLD.Bcast( &size, 1, MPI::INT, 0);
		// the receiving nodes have to resize the receive buffer
	if(MPI::COMM_WORLD.Get_rank() != 0){
		i.resize(size);
	}
		// receive
	
	MPI::COMM_WORLD.Bcast( &i[0], i.size(), MPI_INT, 0);

	// get the genom vector
	if(MPI::COMM_WORLD.Get_rank() != 0){
		intVector2genomVector(i, g, n, circ);
		//~ for(unsigned i=0; i< g.size(); i++)
			//~ cout << "ai"<< i<<" c "<<(int)g[i].getCircular()<<endl;
	}
}

void GathervGenomeVector(vector<genom> &g, char circ, vector<int> &i, const unsigned &n){
	vector<int> sizes(MPI::COMM_WORLD.Get_size(),0);
	vector<int> displ(MPI::COMM_WORLD.Get_size(),0);
	int size=0;

	vector<int> ii;
	
		// construct the int-vector on every node and get its size
	genomVector2intVector(g, ii, n);
	size = ii.size();

		// gather the size of each intvector on node 0 in the sizes vector
	MPI::COMM_WORLD.Gather(&size, 1, MPI::INT, &sizes[0], 1, MPI_INT, 0);
		// let node 0 calculate the sum of the sizes, allocate enough memory 
		// and calculate the displacements
	if(MPI::COMM_WORLD.Get_rank() == 0){
		unsigned rsize=0;
		for(unsigned k=0; k<sizes.size(); k++){
			//~ cout << "k "<<k<<"  size "<<sizes[k]<<endl;
			rsize += sizes[k];
			if(k<sizes.size()-1){
				displ[k+1] = rsize;
			}
		}
		i.resize(rsize);
	}

		// gather the int vector on node 0
	MPI::COMM_WORLD.Gatherv(&ii[0], size, MPI_INT, &i[0], &sizes[0], &displ[0], MPI_INT, 0);

		// get the genome vectors
	if(MPI::COMM_WORLD.Get_rank() == 0){
		intVector2genomVector(i, g, n, circ);
	}
}

void GathervVector(vector<unsigned> &u, const int &proc_size, const int &proc_rank){
	vector<int> sizes(proc_size,0);
	vector<int> displ(proc_size,0);
	vector<unsigned> uu;
	int size = u.size();

	MPI::COMM_WORLD.Gather(&size, 1, MPI::INT, &sizes[0], 1, MPI_INT, 0);
	
	if(proc_rank == 0){
		unsigned rsize=0;
		for(unsigned k=0; k<sizes.size(); k++){
			rsize += sizes[k];
			if(k<sizes.size()-1){
				displ[k+1] = rsize;
			}
		}
		uu.resize(rsize);
	}
	MPI::COMM_WORLD.Gatherv(&u[0], size, MPI::UNSIGNED, &uu[0], &sizes[0], &displ[0], MPI::UNSIGNED, 0);
	
	if(proc_rank==0){
		u = uu;
	}
}

void mpiSpeedTest(int MPI_rank, int MPI_size, unsigned n, unsigned size){
	double start_time = 0, end_time = 0;
	vector<int> testI;
	vector<genom> testG, testG2;
	
	if(MPI_rank == 0){
		for (unsigned i=0; i< size; i++){
			genom tmpGenom = genom(n, 5, 0, 0, true, true, 0);
			testG.push_back(tmpGenom);
		}
	}
	
	if(MPI_rank == 0){
		cout << "broadcast test "<< testG.size()<<" genomes "<<endl;
	}
	MPI::COMM_WORLD.Barrier();

	start_time = MPI::Wtime();
	for(unsigned i=0; i< 1000; i++){
		BcastGenomeVector(testG, 1, testI, n);
	}
	end_time = MPI::Wtime(); 
	cout << MPI_rank<<" btime "<<end_time - start_time<<endl;

	
	//~ start_time = MPI::Wtime();
	//~ XMPI_Buoy("G");
	//~ GathervGenomeVector(temp, testI, n);
	//~ end_time = MPI::Wtime(); 
	//}
	//~ cout << MPI_rank<<" gtime "<<end_time - start_time<<endl;
	
	//~ cout <<MPI_rank<<" : " << testG.size()<<endl;
	
}
#endif
