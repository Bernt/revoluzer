/**
 * author: Matthias Bernt
 * coauthors: D.Merkle, M. Middendorf, K. Ramsch
 */
#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>
#include <sstream>

#include "ilp-rrrmt.hpp"

using namespace std;


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// construct an emptyscen scenario
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ilp_rrrmt::ilp_rrrmt() : unordered(){}

ilp_rrrmt::ilp_rrrmt( const genom &g1, const genom &g2, const costfoo *cst ){

	int n;	// length of the genomes
	vector<int> pi;
	genom gpi;
	rrrmt *r;


	n = g1.size();

	cout << g1 << endl;
	cout << g2 << endl;

	pi = g1.identify( g2 );

	copy( pi.begin(), pi.end(), ostream_iterator<int>(cout, " ") );
	cout << endl;


	gpi = g1.identify_g(g2);
	cout << gpi<<endl;


	r = new rev( 1, 5, g1 );
	// r->apply( g1 );
	this->insert( r );

}

ilp_rrrmt::ilp_rrrmt(const ilp_rrrmt &c ) : unordered(c){}

ilp_rrrmt::~ilp_rrrmt(){}

string ilp_rrrmt::typestr() const{
	return "ilp_rrrmt";
}

