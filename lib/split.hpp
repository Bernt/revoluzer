/*
 * splitsys.hpp
 *
 *  Created on: Jun 10, 2011
 *      Author: maze
 */

#ifndef SPLIT_HPP_
#define SPLIT_HPP_

#include <iostream>
#include <limits>
#include <set>
#include <vector>

#include "character.hpp"
#include "helpers.hpp"

using namespace std;

// forward declaration of split weight base class
class split_weight;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/**
 * class for a split
 * i.e. a bipartition of the species set and a supporting set of characters
 */
class split{
private:
	vector<bool> _bp;  /* the (normalised) bipartition data, i.e. _data[0]=false */
	/**
	 * the data supporting the split given by the binary data.
	 * implemented as vector of length 2.
	 * the first component gives the data present at the 0-side,
	 * and the 2nd component the data present at the 1 side */
	vector<vector<character*> > _data;

	/**
	 * normalize the internal binary representation such that the
	 * fist entry is 0
	 */
	void normalize();
public:
	/**
	 * default constructor does nothing
	 */
	split();

	/**
	 * copy constructor
	 * @param s another split
	 */
	split( const split &s );

	/**
	 * construct a character of a given size
	 * the data is initialised to have length s and the states are set to false
	 * weight is initialised to 0
	 *
	 * @param size the desired size
	 */
	split( unsigned s );

	/**
	 * construct a character given its binary states, note internally a
	 * normalised variant is stored where the first state is 0
	 * weight is initialised to 1
	 *
	 * @param data the binary data
	 */
	split( const vector<bool> &data );

	/**
	 * construct a character from the column of a data matrix
	 * @param[in] matrix the data matrix data[i][j] gives the j-th character
	 * of the i-th species
	 * the weight is initialised to 1
	 * @param[in] col a column in the data matrix
	 */
	split(const vector<vector<bool> > &matrix, unsigned col);

	/**
	 * construct a character from a bipartition a set containing two sets of integers
	 * @param[in] split
	 */
	split( const set<set<unsigned> > &split );

	/**
	 * destructor
	 */
	virtual ~split();

	/**
	 * add (a copy of) one supporting character to the split
	 *
	 * @param[in] o the observation
	 * @param[in] zo state of species 0 (true: the observation is made for species 0, false: not)
	 */
	void addsupport( const character *o, bool zo );

	/**
	 * check if two splits are compatible
	 * @param[in] c another split
	 * @return true iff compatible
	 */
	bool compatible( const split &c ) const;

	/**
	 * check if the split is constant (all values are 0)
	 * @retern true iff the split is constant
	 */
	bool constant() const;

	/**
	 * check if the character is informative
	 * @return true iff the character is informative
	 */
	bool informative() const;

	/**
	 * return the state for species i
	 *@param[in] i the index
	 *@return the state at index i
	 */
	virtual bool operator[](unsigned i) const;

	/**
	 * less than comparison operator. it just compares the binary data vectors.
	 * this operator is necessary for storing splits in sets and
	 * maps
	 * @param[in] c another split
	 * @return the result of the comparison of the normalised data vectors
	 * 	with '<'
	 */
	bool operator<( const split &c) const;

	/**
	 * equality comparison operator. it just compares the binary data vectors.
	 * this operator is necessary for finding splits
	 * @param[in] c another split
	 * @return the result of the comparison of the normalised data vectors
	 * 	with '<'
	 */
	bool operator==( const split &c) const;

	/**
	 * assignment operator
	 * @param[in] p another split
	 * @return copy
	 */
	split& operator=(const split& p);

	/**
	 * output operator
	 * @param[out] the stream to write into
	 * @param[out] c the character
	 * @return stream
	 */
	friend ostream & operator<<(ostream &out, const split &c);

	/**
	 * write a textual representation of the split to a stream
	 * @param out the stream
	 * @param prnsplit print info on the split
	 * @param prnsupport print info on the data supporting the split
	 */
	void output( ostream &out, bool prnsplit, bool prnsupport ) const;

	/**
	 * write the split in nexus format, i.e. a space separated list of indices
	 * where the state is equal to the state of a reference species
	 * @param[in,out] out the stream to write to
	 * @param[in] ref the index of the reference species
	 */
	void output_nexsplit( ostream &out, unsigned ref) const;



	/**
	 * get the size, i.e. the number of species that are described
	 * by the split
	 */
	unsigned size() const;

	/**
	 * compute the size of the split,
	 * i.e. the size of the smaller side
	 * @return the split size
	 */
	unsigned splitsize() const;

	/**
	 * determine the size of one side of the split
	 * @return the desired number
	 */
	unsigned splitsize( bool state ) const;

//	/**
//	 * get the vector of observed data for one of the two sides
//	 * of the split
//	 * @param[in] side the side of the split
//	 */
//	vector<character*> support( bool side ) const;

	/**
	 * get the number of observations supporting in one side of the split
	 * @param[in] side the side
	 * @return the number of supporting observations
	 */
	unsigned supportcnt( bool side ) const;

	/**
	 * get the number of observations supporting the character
	 * @return the number
	 */
	unsigned supportcnt( ) const;

	/**
	 * read only expose the supporting data
	 * @param side the side
	 * @return the begin iterator
	 */
	vector<character*>::const_iterator support_begin( bool side ) const;

	/**
	 * read only expose the supporting data
	 * @param side the side
	 * @return the end iterator
	 */
	vector<character*>::const_iterator support_end( bool side ) const;


	/**
	 * compute the weight of the split given the weight function
	 */
	double weight( const split_weight *foo ) const;
};

/**
 * compares two splits split by the splits sizes
 * @param[in] a a split
 * @param[in] b a split
 * @return the result of the comparison (<)
 */
bool split_cmpsplitsize(const split &a, const split &b);

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

class split_weight{
protected:
	/**
	 * character weighting function to be used
	 */
	character_weight* _chrweightfoo;	/** character weight function */
	/**
	 * normalisation constant
	 **/
	unsigned _norm;
public:
	/**
	 * constructor setting the character weight
	 * @param[in] c character weight foo
	 * @param[in] norm normalisation constant
	 */
	split_weight( character_weight *c, unsigned norm );

	/**
	 * destructor
	 */
	virtual ~split_weight();
	/**
	 * return the weight of a split
	 * @param[in] the split
	 * @return the weight
	 */
	virtual double calc( const split &c ) const = 0;

	/**
	 * make it usable a comparison function object
	 * @param[in] a a split
	 * @param[in] b a split
	 * @return true iff weight of a < weight of b
	 */
	virtual bool operator()(const split &a, const split &b) const;

	/**
	 * set a normalisation constant for the weighting function
	 * may be overwritten in derived classes
	 * @param[in] n normalisation constant
	 */
	virtual void set_norm( unsigned n );
};


/**
 * calculate the same split weight for each split
 * either 1 or normalised with some constant (e.g.
 * the number of splits)
 */
class split_weight_unit : public split_weight {
private:

public:
	/**
	 * constructor
	 * @param[in] normalize
	 * - if 0 then do not normalise
	 * - otherwise a normalisation constant
	 * (e.g. number of splits in the system)
	 * .
	 * @param[in] cw character weight function to be used
	 */
	split_weight_unit( character_weight* cw, unsigned normalize = 0  );

	/**
	 * @see split_weight::calc
	 * @param[in] the split
	 * @return the weight
	 */
	double calc( const split &c ) const;

};


/**
 * calculate the split weight as the sum of the weights of the supporting characters.
 * optionally normalised with some constant (e.g. overall weight sum of all characters)
 */
class split_weight_support : public split_weight {
private:
public:
	/**
	 * constructor
	 * @param[in] normalize
	 * - if 0 then do not normalise
	 * - otherwise a normalisation constant
	 * (reasonable would be to use the total number of
	 * support available for the split system)
	 * .
	 */
	split_weight_support( character_weight* cw, unsigned normalize = 0 );

	/**
	 * @see split_weight::calc
	 * @param[in] the split
	 * @return the weight
	 */
	double calc( const split &c ) const;

};

/**
 * calculate the split weight as the sum of the weights of the supporting characters.
 * optionally normalised with some constant (e.g. overall weight sum of all characters)
 */
class split_weight_binomial : public split_weight {
private:
	unsigned _m;	/** number of species */
	vector<double> _pr;	/** row of pascals triange */
public:
	/**
	 * constructor
	 * @param[in] cw character weight foo
	 * @param[in] normalize
	 * - if 0 then do not normalise
	 * - otherwise a normalisation constant
	 * (reasonable would be to use the total number of
	 * support available for the split system)
	 * .
	 * @param[in] m number of species
	 */
	split_weight_binomial( character_weight* cw, unsigned m , unsigned normalize = 0);

	/**
	 * @see split_weight::calc
	 * @param[in] the split
	 * @return the weight
	 */
	double calc( const split &c ) const;


};

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/**
 * define a split system, i.e. a set of splits and a weighting function for
 * splits and characters
 */
class splitsystem{
public:
	unsigned _size;
	/** set of splits */
	vector<split> _splits;
	/** weighting function for splits */
	split_weight *_splweightfoo;
public:
//	/**
//	 * construct a split system from given splits
//	 * @param[in] splits the splits
//	 * @param[in] size number of species managed in the system
//	 * @param[in] wfoo split weight function to use
//	 */
//	splitsystem( const vector<split> &splits, unsigned size, split_weight *wfoo );

	/**
	 * destructor
	 */
	virtual ~splitsystem(){}	;

	/**
	 * sometimes its favourable to have the uninformative splits
	 * in the system, so here is the function to add them
	 */
	virtual void add_uninformative();

	/**
	 * get a copy if the splits in the system
	 */
	vector<split> get_splits() const;

	/**
	 * check is a given split is in the splitsystem
	 */
	bool inlcudes( const split &s );

	/**
	 * comparison operator for two splits
	 * @param[in] a a split
	 * @param[in] b another split
	 */
	bool cmp_splitweight( const split &a, const split &b ) const;

	/**
	 * comparison operator for two splits
	 * @param[in] a a split
	 * @param[in] b another split
	 */
	bool operator()( const split &a, const split &b ) const;

	/**
	 * write the splits as nexus
	 * @param[in,out] out stream to write to
	 */
	void output_nexus( ostream &out ) const;


	/**
	 * write the characters of the splits as nexus
	 * @param[in,out] out stream to write to
	 */
	void output_nexus_chars( ostream &out ) const;

	/**
	 * remove constant splits
	 */
	virtual void remove_constant( );

	/**
	 * determine a maximum weight compatible subset of splits using ILP
	 * @param[in] tm_lim time limit
	 */
	void remove_incompatible_lp( int tm_lim = numeric_limits<int>::max() );

	/**
	 * get a compatibe subset of the splits (choose greedily by number of
	 * 		observations)
	 */
	virtual void remove_incompatible_greedy(  );

	/**
	 * remove uninformative splits
	 */
	virtual void remove_uninformative( );
	/**
	 * get the total number of observations supporting a split in the splitsystem
	 * @return the number
	 */
	unsigned supportcnt( ) const;
};

#endif /* SPLIT_HPP_ */
