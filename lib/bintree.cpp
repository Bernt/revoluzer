#include <fstream>
#include <iostream>
#include <sstream>
#include <set>
#include <string>
#include <vector>
#include <algorithm>

#include "helpers.hpp"
#include "bintree.hpp"
#include "node.hpp"

//~ #define DEBUG_INIT_FROM_EDGE

//~ #define DEBUG_RF

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void free_tree(bintree * bt){
	
	if(isrooted(bt) == false){
		free_node(bt->root->parent);
	}
	free_node(bt->root);
	
	delete( bt );
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bintree * init(string newick, dstnc *dst){

	bintree *bt = new bintree;

	bt->root = init( bt, NULL );
	
		// append the data give in newickformat to the root-node -> rooted bianry tree
	append(bt->root, newick);
	bt->_dst = dst;
		//~ // let the childs of the root forget the root
	if(isrooted(bt))
		unroot(bt);
	return bt;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bintree * init(string newick, vector<genom> &genomes, vector<string> &names,  
	dstnc *dst){

	bintree *bt = new bintree;

	bt->_dst = dst;
	bt->root = init( bt, NULL );
	
		// append the data give in newickformat to the root-node -> rooted bianry tree
	append(bt->root, newick, genomes, names);

		//~ // let the childs of the root forget the root
	bt->root->lchild->parent = bt->root->rchild;
	bt->root->rchild->parent = bt->root->lchild;
	bt->root = bt->root->rchild;
	return bt;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bintree * init(string newick, genom id, int r, int &applied, dstnc *dst){
	bintree *bt = new bintree;
	int cnt = 0;

	bt->_dst = dst;
	bt->root = init( bt, NULL );

		// append the data give in newickformat to the root-node -> rooted bianry tree
	append(bt->root, newick, id, r, applied, cnt);
	
	unroot(bt);
	
	return bt;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bintree * init(vector<pair<unsigned, unsigned> > &edges, 
	vector<genom> &genomes, vector<string> &names, dstnc *dst){

	bool combined = false;
	bintree *bt = new bintree;
	node **nodes;
	vector<bool> isleaf(genomes.size(), false);
	vector< vector<int> > target;
		
	target = vector<vector<int> >(genomes.size());

#ifdef DEBUG_INIT_FROM_EDGE
	for(unsigned i=0; i<edges.size(); i++)
		cout << "("<<edges[i].first<<","<<edges[i].second<<") ";
	cout << endl;
	cout <<genomes.size() <<" genomes"<<endl;
	for(unsigned i=0;  i< genomes.size(); i++)
		cout << genomes[i]<<endl;
	cout <<names.size()<<" names"<<endl; 
	for(unsigned i=0; i<names.size(); i++)
		cout << names[i]<<endl;
#endif//DEBUG_INIT_FROM_EDGE

		// init the nodes (memory, genome, name)
	nodes = (node **) malloc( sizeof(node*) * genomes.size());
	for(unsigned i=0; i<genomes.size(); i++){
		nodes[i] = init(bt, NULL);
		nodes[i]->g = genomes[i];
		
		if(i<names.size()){
			nodes[i]->name = names[i];
		}else{
			//~ nodes[i]->name = int2string(i);	
			nodes[i]->name = int2string(i);
		}
	}
	bt->_dst = dst;

	for(unsigned i=0; i<names.size(); i++)
		isleaf[i] = true;
	
	do{
		combined = false;
		for(unsigned i=0; i<edges.size(); i++){
			if(isleaf[ edges[i].first ]){
				target[edges[i].second].push_back( edges[i].first );
			}
			
			if(isleaf[ edges[i].second ]){
				target[edges[i].first].push_back( edges[i].second );
			}
		}
		for(unsigned i=0; i<target.size(); i++){
			if(target[ i ].size() == 2){
				combined = true;
#ifdef DEBUG_INIT_FROM_EDGE
				cout << "combine "<< i <<","<<target[ i ][0] << " & "
					<< i <<","<< target[ i ][1]<<endl;
#endif//DEBUG_INIT_FROM_EDGE
				nodes[ target[ i ][0] ]->parent = nodes[ i ];
				nodes[ target[ i ][1] ]->parent = nodes[ i ];
				
				nodes[ i ]->lchild = nodes[ target[ i ][0] ];
				nodes[ i ]->rchild = nodes[ target[ i ][1] ];
				
				isleaf[ i ] = (isleaf[ i ] == true)?false:true;
				isleaf[ target[ i ][0] ] = (isleaf[ target[ i ][0] ]==true)?false:true;
				isleaf[ target[ i ][1] ] = (isleaf[ target[ i ][1] ]==true)?false:true;
			}
			else if(target[i].size() == 3){
#ifdef DEBUG_INIT_FROM_EDGE
				cout << "combine "<< i <<","<<target[ i ][0] << " & "
					<< i <<","<< target[ i ][1]<<" & "
					<< i <<","<< target[ i ][2]<<endl;
#endif//DEBUG_INIT_FROM_EDGE
				nodes[ target[ i ][0] ]->parent = nodes[ i ];
				nodes[ target[ i ][1] ]->parent = nodes[ i ];
				nodes[ target[ i ][2] ]->parent = nodes[ i ];
				nodes[ i ]->lchild = nodes[ target[ i ][0] ];
				nodes[ i ]->rchild = nodes[ target[ i ][1] ];
				nodes[ i ]->parent = nodes[ target[ i ][2] ];
				bt->root = nodes[i];
				
				isleaf[ i ] = isleaf[ target[ i ][0] ] = 
					isleaf[ target[ i ][1] ] = isleaf[ target[ i ][2] ] = false;
				
			}
			target[i].clear();
		}
#ifdef DEBUG_INIT_FROM_EDGE
		cout << "..."<<endl;
#endif//DEBUG_INIT_FROM_EDGE
	}while(combined);
	
	if(sum(isleaf) > 0){
#ifdef DEBUG_INIT_FROM_EDGE
		cout << "more than 0 leafes left .. combining"<<endl;
#endif//DEBUG_INIT_FROM_EDGE
		for(unsigned i=0; i<edges.size(); i++){
			if(isleaf[ edges[i].first ] && isleaf[ edges[i].second ]) {
#ifdef DEBUG_INIT_FROM_EDGE
				cout << "edge ("<<edges[i].first<<","<<edges[i].second<<")"<<endl;
#endif//DEBUG_INIT_FROM_EDGE
				nodes[ edges[i].first ]->parent = nodes[ edges[i].second ];
				nodes[ edges[i].second ]->parent = nodes[ edges[i].first ];
				bt->root = nodes[ edges[i].first ];
				
				break;
			}
		}
	}
	
	for(unsigned i=0; i<target.size(); i++)
		target[i].clear();
	target.clear();
	free(nodes);
	
	return bt;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void inner_genomes(bintree *bt, vector<string> &names, vector<genom> &genomes){
	genomes.clear();
	names.clear();
	inner_genomes(bt->root->parent, names, genomes);
	inner_genomes(bt->root, names, genomes);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

bool isrooted(bintree *bt){
	if(bt->root->parent != NULL)
		return false;
	else
		return true;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void leaf_genomes(bintree *bt, vector<string> &names, vector<genom> &genomes){
	genomes.clear();
	names.clear();
	if(isrooted(bt) == false){
		leaf_genomes(bt->root->parent, names, genomes);
	}
	leaf_genomes(bt->root, names, genomes);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void leaf_names(bintree *bt, vector<string> &names){
	names.clear();
	if(isrooted(bt) == false){
		leaf_names(bt->root->parent, names);
	}
	leaf_names(bt->root, names);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int lower_bound(bintree *bt, dstnc *dst, hdata &hd){
	map<string, int> name_idx;
	vector<vector<unsigned> > mv,
		dist;					// distance matrix of the leafgenomes
	vector<string> l, r;
	vector<string> leafnames;
	vector<genom> leafgenomes;

	if(isrooted(bt) == false){
		root(bt);
	}

	leaf_genomes(bt, leafnames, leafgenomes);
	dist = vector<vector<unsigned> >(leafgenomes.size(), vector<unsigned>(leafgenomes.size(), 0));
	for(unsigned i=0; i<leafgenomes.size(); i++){
		name_idx[ leafnames[i] ] = i;
		for(unsigned j=i+1; j<leafgenomes.size(); j++){
			dist[i][j] = dist[j][i] = dst->calc( leafgenomes[i], leafgenomes[j] );
		}
	}
//	cout << "/ ";
//	for(unsigned i=0; i<dist.size(); i++){
//		cout.width(2);
//		cout << leafnames[i]<<" ";
//	}
//	cout << endl;	
//	for(unsigned i=0; i<dist.size(); i++){
//		cout << leafnames[i]<< " ";
//		for(unsigned j=0; j<dist[i].size(); j++){
//			cout.width(2);
//			cout << dist[i][j]<<" ";
//		}
//		cout << endl;
//	}
//	cout << newick(bt,0,1,0)<<endl;
	
	return lower_bound(bt->root, name_idx, dist, l, r, mv);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

string newick(bintree *bt, bool printdst, bool inner, bool normalise){
	string nwk;

	if(normalise){
		sort(bt);
	}

	nwk.append("(");
	if(isrooted(bt) == false){
		nwk.append( newick( bt->root->parent, printdst, inner) );
		nwk.append(",");
	}
	nwk.append(newick( bt->root->lchild, printdst, inner));
	nwk.append(",");
	nwk.append(newick( bt->root->rchild, printdst, inner));
	nwk.append(")");
	
	if(inner){
		nwk.append(bt->root->name);
	}
	nwk.append(";");

	return nwk;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void print_newick(bintree *bt, bool distance, bool inner){
	cout << "(";
	if(isrooted(bt) == false){
		print_newick( bt->root->parent, distance, inner);
		cout << ",";
	}
	print_newick( bt->root->lchild, distance, inner);
	cout << ",";
	print_newick( bt->root->rchild, distance, inner);
	cout << ")";

	if(inner){
		cout << bt->root->name;
	}
	cout << ";";

	return ;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int robinson_foulds(bintree *bt1, bintree *bt2){
	int rf = 0;
	vector<node *> inner_nodes1, 
		inner_nodes2;
	vector<string> leafs;
	vector<pair<vector<string>, vector<string> > > bipartitions1,
		bipartitions2, 
		identical_bipartitions;
	
	// RF / split distance is defined as d(t1, t2) = i(t1) + i(t2) - 2*v(t1,t2)
	// i(t): number of edges not incident to a leaf
	// v(t1,t2): number of identical splits
	
	leaf_names(bt1, leafs);
	sort(leafs.begin(), leafs.end());

#ifdef DEBUG_RF
		cout << "RF"<<endl;
	print_newick(bt1, 0, 0);cout<<endl;
	print_newick(bt2, 0, 0);cout<<endl;
	cout << "leafs"<<endl;
	for(unsigned i=0; i<leafs.size(); i++)
		cout << leafs[i]<<" ";
	cout << endl;
#endif
	
	inner_nodes(bt1->root, inner_nodes1);
	inner_nodes(bt1->root->parent, inner_nodes1);

	inner_nodes(bt2->root, inner_nodes2);
	inner_nodes(bt2->root->parent, inner_nodes2);

	bipartitions1 = vector<pair<vector<string>, vector<string> > >(inner_nodes1.size());
	bipartitions2 = vector<pair<vector<string>, vector<string> > >(inner_nodes2.size());
	
	for(unsigned i=0; i<inner_nodes1.size(); i++){
		leaf_names(inner_nodes1[i], bipartitions1[i].first);
		sort(bipartitions1[i].first.begin(), bipartitions1[i].first.end());
		
		set_difference(leafs.begin(), leafs.end(),
			bipartitions1[i].first.begin(), bipartitions1[i].first.end(), 
			back_inserter(bipartitions1[i].second)); 
		if(bipartitions1[i].second < bipartitions1[i].first)
			swap(bipartitions1[i].second, bipartitions1[i].first);
	}
	
	for(unsigned i=0; i<inner_nodes2.size(); i++){
		leaf_names(inner_nodes2[i], bipartitions2[i].first);
		sort(bipartitions2[i].first.begin(), bipartitions2[i].first.end());
		set_difference(leafs.begin(), leafs.end(),
			bipartitions2[i].first.begin(), bipartitions2[i].first.end(), 
			back_inserter(bipartitions2[i].second));
		if(bipartitions2[i].second < bipartitions2[i].first)
			swap(bipartitions2[i].second, bipartitions2[i].first);
	}
	
	sort(bipartitions1.begin(), bipartitions1.end());
	sort(bipartitions2.begin(), bipartitions2.end());
	
	for(unsigned i=0; i<bipartitions1.size(); i++){
		if(bipartitions1[i].first.size() <= 1 || bipartitions1[i].second.size() <= 1 ||
			( i>0 && bipartitions1[i-1] == bipartitions1[i] )){
			bipartitions1.erase(bipartitions1.begin()+i);
			i--;
		}
	}
	for(unsigned i=0; i<bipartitions2.size(); i++){
		if(bipartitions2[i].first.size() <= 1 || bipartitions2[i].second.size() <= 1 ||
			( i>0 && bipartitions2[i-1] == bipartitions2[i] )){
			bipartitions2.erase(bipartitions2.begin()+i);
			i--;
		}
	}
	
#ifdef DEBUG_RF	
	cout << "bipartitions1"<<endl;
	for(unsigned i=0; i<bipartitions1.size(); i++){
		cout << "{";
		for(unsigned j=0; j<bipartitions1[i].first.size(); j++)
			cout << bipartitions1[i].first[j]<<" ";
		cout << "} {";
		for(unsigned j=0; j<bipartitions1[i].second.size(); j++)
			cout << bipartitions1[i].second[j]<<" ";		
		cout << "}"<<endl;
	}
	cout << "bipartitions2"<<endl;
	for(unsigned i=0; i<bipartitions2.size(); i++){
		cout << "{";
		for(unsigned j=0; j<bipartitions2[i].first.size(); j++)
			cout << bipartitions2[i].first[j]<<" ";
		cout << "} {";
		for(unsigned j=0; j<bipartitions2[i].second.size(); j++)
			cout << bipartitions2[i].second[j]<<" ";		
		cout << "}"<<endl;
	}
#endif
	
	set_intersection(bipartitions1.begin(), bipartitions1.end(), 
		bipartitions2.begin(), bipartitions2.end(), 
		back_inserter( identical_bipartitions ));

	for(unsigned i=0; i<bipartitions1.size(); i++){
		if(bipartitions1[i].first.size()>1 && bipartitions1[i].second.size()>1)
			rf ++;
	}
	
	for(unsigned i=0; i<bipartitions2.size(); i++){
		if(bipartitions2[i].first.size()>1 && bipartitions2[i].second.size()>1)
			rf ++;
	}
	
	rf -= (2* identical_bipartitions.size());

	return rf;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void root(bintree *bt){
	
	node *newroot;
		// if the tree is already rooted -> there is nothing to do
	if(isrooted(bt) == true){
		return;
	}
		// init the new root node
	newroot = init( bt, NULL );
	newroot->name = "R";
	newroot->lchild = bt->root;
	newroot->rchild = bt->root->parent;
	
		// change the parents of the old root and its parent to the new root
	bt->root->parent->parent = newroot;
	bt->root->parent = newroot;
	
		// set the new root
	bt->root = newroot;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int score(bintree *bt){
	if(bt->root->parent == NULL){
		return score(bt->root->lchild)+score(bt->root->rchild);
	}else{
		return score(bt->root->parent) + score(bt->root->lchild)+score(bt->root->rchild);
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void sort(bintree *bt){
	node *newroot;
	string lstart, 
		rstart;

	sort(bt->root->parent, lstart);
	sort(bt->root, rstart);

	if(lstart > rstart){
		bt->root = bt->root->parent;
	}

	newroot = bt->root;
	while(newroot->parent != NULL && newroot->parent->lchild != NULL){
		newroot = newroot->parent;
		swap(newroot->parent, newroot->lchild);
	}
	bt->root = newroot;

	sort(bt->root->parent, lstart);
	sort(bt->root, rstart);

	if(lstart > rstart){
		bt->root = bt->root->parent;
	}

	
	return;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void unroot(bintree *bt){
	
	if(!isleaf ( bt->root->rchild )){
		bt->root->lchild->parent = bt->root->rchild;
		bt->root->rchild->parent = bt->root->lchild;
		bt->root = bt->root->rchild;
	}else if( isleaf ( bt->root->rchild ) ){
		bt->root->rchild->parent = bt->root->lchild;
		bt->root->lchild->parent = bt->root->rchild;
		bt->root = bt->root->lchild;
	}else{
		cout << "Error: tree could not transformed to unrooted"<<endl;
		exit(0);
	}

	
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void triples(bintree *bt, 
	vector<vector<genom> > &trips, vector<vector<string> > &trips_names, 
	vector<genom> &meds, vector<string> &meds_names){
	
	triples(bt->root, trips, trips_names, meds, meds_names);
	triples(bt->root->parent, trips, trips_names, meds, meds_names);

	return;
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
///////////////////////////////////////////////////////////////////////////////
bintree::bintree(vector<genom> genomes, bool circ){
	circular = circ;
		// make the root node
	// root = new node(this, NULL);
	root = NULL;
	for(unsigned i=0; i<genomes.size(); i++){
		node *t = new node(this, NULL);
		t->g = genomes[i];
		leafs.push_back(t);
	}
}

hdata bintree::get_hdata(){
	return hd;
}

ostream & operator<<(ostream &out, bintree &t){
	
	out << "("<<t.root->parent<<","<< t.root <<");";
	
	
	return out;
}

///////////////////////////////////////////////////////////////////////////////

void bintree::output(void){
	if(root)
		root->output();
	else
		cout << "CANT OUTPUT - root is NULL"<<endl;
}
*/






















///////////////////////////////////////////////////////////////////////////////
/*
bool bintree::centralizeRoot(){
	int lCnt = root->lChild->countLeafs(),										// count the leafs at the lChild, rChild & parent
		rCnt = root->rChild->countLeafs(),										// of the actual root of the tree
		pCnt = root->parent->countLeafs(),
		actDiff = abs(lCnt-rCnt) + abs(rCnt-pCnt) + abs(pCnt-lCnt);		// calc. the difference =? meassure for balance - lower diff -> more balanced

	if(abs(lCnt-rCnt)<2 &&  abs(pCnt-rCnt)<2 && abs(lCnt-pCnt)<2){		// test if balanced enough
		return 0;
	}else{
		node *possiblyNewRoot = NULL;												
		char direction = 0;															
		if(lCnt > rCnt && lCnt > pCnt){				// get the possiblyNewRoot and the direction of it (for later)
			possiblyNewRoot = root->lChild;			// take the one with the most descendant leaf 
			direction = 'l';
		}else if(rCnt > lCnt && rCnt > pCnt){
			possiblyNewRoot = root->rChild;
			direction = 'r';
		}else if(pCnt > lCnt && pCnt > rCnt){
			possiblyNewRoot = root->rChild;
			direction = 'p';
		}
		
		if(!possiblyNewRoot){	// test if there is a new root
			return 0;
		}
		
		int newLCnt = possiblyNewRoot->lChild->countLeafs(),		// calc. the leafs of the lChild, rCHild. parent of the possibly newRoot
			newRCnt = possiblyNewRoot->rChild->countLeafs(),
			newPCnt = (lCnt +rCnt +pCnt) - newLCnt - newRCnt, 		// !! tricky
			newDiff = abs(newLCnt-newRCnt) + abs(newRCnt-newPCnt) + abs(newPCnt-newLCnt);	// calc. the new difference
		
		if (newDiff >= actDiff)	// test if new root would be not better or worser 
			return 0;
		switch(direction){		// else use the stored direction
			case 'l':{				// and move the root to the other node 
				swap(root->lChild, root->parent);
				root = root->parent;
				break;
			}
			case 'r':{
				swap(root->rChild, root->parent);
				root = root->parent;
				break;
			}
			case 'p':{
				root = root->parent;
				break;
			}
		}
		return 1;
	}
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
int compareLevel(node *n1, node *n2){
	return (n1->getLevel() > n2->getLevel());
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
int bintree::distance(genom g, hdata &hd){
	return root->distance(g, hd);
}
*/
///////////////////////////////////////////////////////////////////////////////

/*
int bintree::distanceSum(hdata &hd){
	return root->distanceSum(hd);
}


vector< node * > bintree::getLeafs(){
	return leafs;
}
*/
///////////////////////////////////////////////////////////////////////////////

/*node *bintree::getNextUnresolvedNode(){
	return root->getNextUnresolvedChild();
}*/

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/*
vector<node *> bintree::getInnerNodes(){
	return root->getInnerNodes();
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
node *bintree::getRoot(){
	return  root;
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
int bintree::maxLeafDistance(){
	return root->maxLeafDistance();
}
*/
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/*
void bintree::output_tulip(char *filename){
	ofstream tulip(filename);
	string nodes = "(nodes",
		edges = "",
		outdegree_property="",
		labels="";
	int node_num = 0,
		edge_num = 0;

	if(!tulip.is_open()) error("Kann Outputdatei nicht schreiben : ",filename);

	root->output_tulip(nodes, node_num, edges, edge_num, outdegree_property, labels);
	nodes += ")";

	tulip << nodes << endl;
	tulip << edges << endl;
	
	labels = "(property  0 string \"viewLabel\"\n"+labels+")\n";
	tulip << labels << endl;
	
	outdegree_property = "(property  0 int \"outdegree\"\n"+outdegree_property+")";
	tulip << outdegree_property << endl;

	tulip << "(property  0 int \"viewShape\"" << endl;
	tulip << "(default \"1\" \"0\" )" << endl;
	tulip << ")" << endl;
	tulip << "(property  0 size \"viewSize\"" << endl;
	tulip << "(default \"(20.000000,20.000000,20.000000)\" \"(1.000000,1.000000,0.000000)\" )" << endl;
	tulip << ")" << endl;
	tulip << "(displaying " << endl;
	tulip << "(glyph 0 (plugin \"Cone\"))" << endl;
	tulip << "(glyph 1 (plugin \"Cube\"))" << endl;
	tulip << "(glyph 2 (plugin \"CubeOutLined\"))" << endl;
	tulip << "(glyph 3 (plugin \"Cylinder\"))" << endl;
	tulip << "(glyph 4 (plugin \"Sphere\"))" << endl;
	tulip << "(color \"backgroundColor\" \"(65,65,65,0)\")" << endl;
	tulip << "(bool \"_viewArrow\" false)" << endl;
	tulip << "(bool \"_viewLabel\" false)" << endl;
	tulip << "(bool \"_viewKey\" false)" << endl;
	tulip << "(bool \"_viewStrahler\" false)" << endl;
	tulip << "(bool \"_viewAutoScale\" true)" << endl;
	tulip << "(bool \"_incrementalRendering\" true)" << endl;
	tulip << "(bool \"_edgeColorInterpolate\" false)" << endl;
	tulip << "(bool \"_edge3D\" false)" << endl;
	tulip << "(uint \"_viewOrtho\" 1)" << endl;
	tulip << "(uint \"_FontsType\" 1)" << endl;
	tulip << "(int \"SupergraphId\" 9)" << endl;
	tulip << ")" << endl;	
}
*/
///////////////////////////////////////////////////////////////////////////////

/*
void bintree::setRoot(node *r){
	root = r;
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
void bintree::shake(hdata &hd){
	int enh=1;
	while(enh){
		enh = root->shake(hd);
	}
}
*/
///////////////////////////////////////////////////////////////////////////////
/*
void bintree::unRoot(){
	int lMld, rMld;

		// check if the tree is already unrooted, if so then do nothing
	if(root->parent)
		return;
	
		// get the maximum leaf distances of the child nodes
	lMld = root->lChild->maxLeafDistance();
	rMld = root->rChild->maxLeafDistance();
		// bypass the root node
	root->rChild->parent = root->lChild;
	root->lChild->parent = root->rChild;
		// set as the new root the child-node with greater maximumLeafDistance (if eq. -> doesn't matter)
	root = (lMld > rMld) ? root->lChild : root->rChild;
}
*/
///////////////////////////////////////////////////////////////////////////////
/* DEPRECATED : now extra funtion to get the newick-strings,
	the new contructor only gets one of these strings
bintree::bintree(vector<genom> genomes, char *filename, bool c){
	char *fname = new(char[strlen(filename)+7]);
	strcpy(fname, filename);
	strcat(fname, ".grappa");

	ifstream grappa_out(fname);
	string line;
	boost::regex re("^(.*);$");
	boost::smatch match;
	root = new node(this, NULL);
	
	if(!grappa_out) error("Kann Inputdatei nicht lesen : ",filename);
		
		// read the grappa output and search for the first output of a tree in the newick-tree
		// format and parse this (build up the binary tree)
	while(grappa_out){
		getline(grappa_out, line);
		if (boost::regex_match(line, match, re)){
			string s;
			s.assign(match[1].first, match[1].second);
			cout << s << endl;
   		root->append(s, genomes);
			break;
		}
	}
	
	for (unsigned int i=0; i<leafs.size(); i++){
		// used[leafs[i]->g] = 1;
	}
		
		// sortiere Bl?tter nach Level
	stable_sort(leafs.begin(), leafs.end(), compareLevel);
	circular = c;
	grappa_out.close();
}*/

