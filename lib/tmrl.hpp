#ifndef _TMRL_HPP_
#define _TMRL_HPP_

#include "common.hpp"
#include "genom.hpp"

typedef struct{
	int cpycnt,			// number of tandem copies needed for sorting
		pcpycnt,		// number of positive copies needed
		ncpycnt,		// number of negative copies needed
		mincpylen,		// minimal number of emlements remaining in one of the copies
		maxblkcnt;		// blkcnt: the elements remaining in one copy form a number of 
						// continuous blocks in the original permutation, this is their max number      
	vector<int> cnf;
} tmrl;

/**
 * locate tmrl events with the help of an interval tree
 * @param[in] ind the interval subtree to investigate
 * @param[in] src the genome to which the interval tree is relative to
 * @param[in] tgt the target permutation
 * @param[in] circular handle as circular, look also at the complementary common intervals
 */
void tmrloc_itree( itnode *ind, const genom &src, const genom &tgt, int circular, vector<pair<int,int> > &rtdrli, vector<tmrl> &rtdrls );

/**
 * locate tandem multiplication random loss events with the help 
 * of a interval tree:
 * preorder traverser the interval tree:
 * - for each node look for tmrls in the 'front permutation'
 * - if there is one report it (and don't traverse the children) otherwise continue with the children
 * @param[in] src the source genome
 * @param[in] tgt the target genome 
 * @param[in] the length of the genomes
 * @param[in] circular handle as circular, look also at the complementary common intervals
 */
void tmrloc_itree( const genom &src, const genom &org, int n, int circular, vector<pair<int,int> > &rtdrli, vector<tmrl> &rtdrls  );

#endif// _TMRL_HPP_
