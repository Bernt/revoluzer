#include <algorithm>
#include <iostream>
#include <iterator>
#include <limits>

#include "tmrl.hpp"

#define DEBUG_TMRLOC
#define DEBUG_TMRL

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// compute how to get from the id to g with one tandem multiplication
// random loss event.
// returns:
// - a vector of the length of g which contains for each element of g
//   the number of the copy from which the element originated, if the element
//   originated from a negative copy the number is negative.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void get_tmrl( const genom &g, tmrl &t ){
	int blk = 0,
		len = 0,	// length of the current copy (the remainders)
		dir = 0,	// current direction 0: no dir, 1: + right, -1, - left
		lpos = 0; 	// last position
	vector<int> cpycnf;	// directions of the tandem copys (+1 positive, -1 negative)
//		cnf(g.size(), std::numeric_limits< int >::max());	//

		// reinit the tmrl .. just to be sure
	t.cpycnt = t.pcpycnt = t.ncpycnt = t.maxblkcnt = 0;
	t.cnf.clear();
	t.cnf = vector<int>(g.size(), std::numeric_limits< int >::max());
	t.mincpylen = std::numeric_limits< int >::max();

#ifdef DEBUG_TMRL
	cout << "tmrl "<<g<<endl;
#endif//DEBUG_TMRL

	for(unsigned i=0; i<g.size(); i++){
		// cases where NO new copy is needed:
		// - direction in right (left) and the new element is right (left) of the last position, and it is positive (negative)
		// - the first element in the permutation is positive
		// so if these cases do not apply -> new copy
		if( !( (dir == 1 && abs(g[i])>lpos && g[i] > 0) ||
			(dir == -1 && abs(g[i])<lpos && g[i] < 0)) ){

			t.cpycnt++;
			if( g[i] > 0 ){
				cpycnf.push_back( 1 );
				t.pcpycnt++;
			}else{
				cpycnf.push_back( -1 );
				t.ncpycnt++;
			}

			if( len > 0 && len < t.mincpylen ){
				t.mincpylen = len;
			}
			if( blk > 0 && blk > t.maxblkcnt ){

				t.maxblkcnt = blk;
			}
			blk = 1;
			len = 0;
		}else{
			if( abs( lpos - abs(g[i]) ) > 1 ){
				blk++;
			}
		}


		len++;
		if( g[i] > 0 ){
			dir = 1;
		}else{
			dir = -1;
		}

		lpos = abs(g[i]);
		t.cnf[ i ] = t.cpycnt * dir;
	}

	if( len > 0 &&  len < t.mincpylen){
		t.mincpylen = len;
	}
	if( blk > 0 && blk > t.maxblkcnt ){
		t.maxblkcnt = blk;
	}


#ifdef DEBUG_TMRL
	cout << " -> cpycnt  "<<t.cpycnt<<endl;
	cout << "    pcpycnt "<<t.pcpycnt<<endl;
	cout << "    ncpycnt "<<t.ncpycnt<<endl;
	cout << "    cnf ";
	copy( t.cnf.begin(), t.cnf.end(), ostream_iterator<int>(cout, "") );
	cout << endl;
	cout << "    mincpylen " << t.mincpylen<<endl;
	cout << "    maxblkcnt " << t.maxblkcnt<<endl;
#endif//DEBUG_TMRL
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void tmrloc_itree( itnode *ind, const genom &g, int circular, vector<pair<int,int> > &rtdrli, vector<tmrl> &rtdrls ){
	int depth = 0,
		min = std::numeric_limits< int >::max(),
		minpos = 0;
	genom f;	// the front of the current node
	tmrl t;

	for( int i= ind->i.first-1;  i<ind->i.second; i++ ){
		f.push_back( g[i] );
	}


	depth = interval_tree_depth(ind);
	get_tmrl(f, t);

//	if( t.cpycnt == 2 && t.pcpycnt == 2 && t.ncpycnt == 0 ){
		rtdrli.push_back(ind->i);
		rtdrls.push_back( t );
//		return ;
//	}


	if(circular != 0 &&  ind->i.second < g.size() ){
		f.clear();
		for( int i= ind->i.second ; i< g.size(); i++ ){
			f.push_back( g[i] );
		}
		for( int i= 0 ; i< ind->i.first-1; i++ ){
			f.push_back( g[i] );
		}
				// search element to
		for(unsigned i=0; i<f.size(); i++){
			if(abs(f[i]) < min){
				min = abs(f[i]);
				minpos = i;
			}
		}
		rotate(f.begin(), f.begin()+minpos, f.end());
		cout << "complement "<<f<<endl;

		get_tmrl(f, t);
		rtdrli.push_back(make_pair(ind->i.second+1, ind->i.first-1) );
		rtdrls.push_back( t );

	}


		// traverse the children
	for(unsigned i=0; i<ind->children.size(); i++){
		tmrloc_itree( ind->children[i], g, circular, rtdrli, rtdrls );
	}
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void tmrloc_itree( const genom &src, const genom &tgt, int n, int circular, vector<pair<int,int> > &rtdrli, vector<tmrl> &rtdrls  ){
	genom g;
	itnode *iroot;

	rtdrls.clear();
	rtdrli.clear();

	g = src.identify_g(tgt);

#ifdef DEBUG_TMRLOC
	cout << "tmrloc itree"<<endl;
	cout << "src "<<src<<endl;
	cout << "tgt "<<tgt<<endl;
	cout << "g   "<<g<<endl;
#endif//DEBUG_TMRLOC

		// compute the interval tree relative to src
	interval_tree(tgt, src, n, &iroot);

	tmrloc_itree( iroot, g, circular, rtdrli, rtdrls );

		// free used memory
	interval_tree_free(iroot);
}



