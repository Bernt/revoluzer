#ifdef USEMPI

#ifndef _MPI_HELPERS_HPP_
#define _MPI_HELPERS_HPP_

#include <iostream> 
#include <map> 
#include <mpi.h>
#include <vector> 

#include "genom.hpp"

/**
 * copy a genom vector in a new intVector
 *@param[in] gv the source vector of genomes
 *@param[out] iv the target int vector
 *@param[in] n length of each genome
 */
void genomVector2intVector(const vector<genom> &gv, vector<int> &iv, const unsigned &n);

/**
 * copy a genom vector in a new intVector 
 *@param[in] iv the source int vector
 *@param[out] gv the target vector of genomes
 *@param[in] n length of each genome
 */
void intVector2genomVector(const vector<int> &iv, vector<genom> &gv, const unsigned &n, const char &circ);

/**
 * Send a genome vector (consisting of genomes of length n) to node dest. For this the integer vector 
 * is used as temporary storage.
 *@param[in] g the genome vector to send
 *@param[in,out] i the integer vector for temporary storage
 *@param[in,out] uBuf buffer space for one unsigned
 *@param[in] n the length of each genome
 *@param[in] dest the destination
 */
void SendGenomeVector(const vector<genom> &g, unsigned from, unsigned to, vector<int> &i, unsigned &uBuf, const unsigned &n, int dest, MPI::Request &req);

//~ void SendGenomeMap(const map<genom, vector<bool> > &gm, vector<genom> &gv, vector<int> &i, const unsigned &n, int dest);

/**
 * Receive a genome vector (consisting of genomes of length n) from node source. For this the integer vector 
 * is used as temporary storage. there are the possibilities of blocking and nonblocking recv. in the blocking mode 
 * the size and the intvector are received blocking and then the intvector is transformed into the genomevector.
 * in the nonblocking mode the three steps are seperated: 
 * - 0: init the receive of the size of the intvector (which can be received only if the size is known)
 * - 1: init the receive of the intvector only if size > 0
 * - 2 : transform the intvector into an genomvector
 * .
 * thus the function has to be called three times. 
 *@param[in,out] i the integer vector for temporary storage
 *@param[in,out] the size of i
 *@param[out] g the received genome vector
 *@param[in] circ the circularity of the genomes
 *@param[in] n the length of each genome
 *@param[in] source the source of the transmission
 *@param[in] blocking if true receive in blocking mode, else nonblocking
 *@param[in] step the step of receive operation
 *@return if nonblocking recv: the request of the nonblocking Irecv operations in step 0 and 1 else (blocking or step 2 of nonblocking) MPI::REQUEST_NULL
 */
MPI::Request RecvGenomeVector(vector<int> &i, unsigned &size, vector<genom> &g, char circ, const unsigned &n, int source, bool blocking, unsigned step=0);

/**
 * broadcast the genomevector g 
 *@param[in] g the genomes to broadcast
 *@param[in] i an integer vector as temporary storage
 *@param[in] n the length of each genome
 */
void BcastGenomeVector(vector<genom> &g, char circ, vector<int> &i, const unsigned &n);

/**
 * gather the genom vectors g at node 0
 *@param[in] g the genomes to gather
 *@param[in] i an integer vector as temporary storage
 *@param[in] n the length of each genome
 */
void GathervGenomeVector(vector<genom> &g, char circ, vector<int> &i, const unsigned &n);

/**
 * gather a vector of unsigned to node 0
 *@param[in,out] u the vector(s)
 *@param[in] proc_size size of the communicator
 *@param[in] proc_rank rank in the communicator
 */
void GathervVector(vector<unsigned> &u, const int &proc_size, const int &proc_rank);

/**
 * a little benchmark fuction
 *@param[in] MPI_rank the rank
 *@param[in] MPI_size the size
 *@param[in] n genome length
 *@param[in] size of the test
 */ 
void mpiSpeedTest(int MPI_rank, int MPI_size, unsigned n, unsigned size);

#endif

#endif//USEMPI
