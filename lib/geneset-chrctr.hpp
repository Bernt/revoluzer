#include "split.hpp"
#include "geneset.hpp"
#include "genom.hpp"

class interval_splitsystem : public splitsystem{
public:
	/**
	 * construct split system from the information of the absence and presence
	 * of intervals in a given set of genomes
	 * @param[in] genomes the gene orders
	 * @param[in] sgnhnd the unsigning hethod used
	 * @param[in] circular the circularity of the gene orders
	 * @param[in] complement applies to circular genomes only
	 *  if true: use both of the complementary intervals
	 * 	else: only one is used (the one which does not contain the first element
	 *  of the first genome)
	 * @param[in,out] maxsize the maximum size of the intervals. maxsize <= n must
	 *  hold. if UINT_MAX is given the maximum is set automatically to the max,
	 *  i.e. n-1;
	 *  note: for circular genomes the actual size of the returned intervals
	 *  might be bigger, but it holds that the smaller of the complementary
	 *  pair is <= maxsize
	 * @param[out] minobs the minimum number of observations (intervals) found for
	 *  one of the input genomes. for the current implementation the number of
	 *  intervals is the same for each genome:
	 *  - linear \f$ { n \choose 2}- {n-x+1 \choose 2} \f$,
	 *  - circular complement: \f$ \min( {n-1 \choose 2}-1, n(x-1) ) \f$,
	 *  - circular !complement: \f$ n*(x-1) \f$, where x is the maxsize
	 *.
	 * @param[in] quiet dont print status
	 * @param[in] wfoo split weight funtion to set
	 * @return a vector of binary characters
	 */
	interval_splitsystem(const vector<genom> &genomes,
			SgnHandType sgnhnd, bool circular, bool complement, unsigned &maxsize,
			unsigned &minobs, bool quiet, split_weight *wfoo);

	/**
	 * destructor
	 */
	virtual ~interval_splitsystem(){};
};



///**
// * init binary characters from the absence and presence if intervals
// * 	in given (circular/linear) gene orders
// */
//vector<chrctr<geneset> > init_characters( const vector<genom > &genomes,
//		bool circular, bool complement, unsigned &maxsize, unsigned &minobs, bool quiet = false );
