/*
 * comdist.cpp
 *
 * @author M. Bernt
 */

#include "dstnc_com.hpp"

#include "common.hpp"

void dstnc_com::adapt( unsigned n ){
	_n = n;
	if( ! _circular ){
		_max = ( _n * (_n-1)) + 1;
	}else{
		_max = (_n-1)*(_n) + 1;
	}
//	cout <<"max "<< _max<<" "<<n/2 <<endl;
};

dstnc_com::dstnc_com( unsigned n, bool circular ) {
	_circular = circular;
	adapt( n );
}

dstnc_com::~dstnc_com() {
}

unsigned dstnc_com::calc( const genom &src, const genom &tgt ){
	vector<pair<int, int> > ci;

	// Test for valid chromosom
	if ( _n != src.size() || _n != tgt.size() ) {
		cerr << "error: difference in number of genes: "<<_n<<" "<<src.size()<<" "<<tgt.size()<<endl;
		cout << src << endl<<tgt<<endl;
		exit( EXIT_FAILURE );
	}

//	cout <<"cid src "<< src<<endl;
//	cout <<"cid tgt "<< tgt<<endl;
//                                       triv sign
	common_intervals(src, tgt, _circular,   1 ,   0, ci);
//	cout << "----------"<<endl;
//	for(unsigned i=0; i<ci.size(); i++){
//		cout << ci[i].first<<":"<<ci[i].second<<endl;
//	}
//	cout << "-> "<<ci.size()<<" -> "<< _max - ci.size()<<endl;
//	cout << "----------" << ci.size()<<endl;
	return _max - ci.size();

}

dstnc* dstnc_com::clone() const{
	return new dstnc_com( _n, _circular );
}

//dstnc* dstnc_com::create(){
//	return new dstnc_com();
//}

ostream & dstnc_com::output(ostream &os) const{
	os << "common interval distance for n="<<_n;
	return os;
}
