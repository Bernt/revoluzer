#ifndef _AMGR_HPP_
#define _AMGR_HPP_

#include <vector>
#include "genom.hpp"
#include "median.hpp"

#define SELECT_NEAREST 1
#define SELECT_FAREST -1

#define SELECT_CENTRAL 2
#define SELECT_DECENTRAL -2

#define SELECT_MIN_SKEW 3
#define SELECT_MAX_SKEW -3

#define SELECT_MIN_LONGESTEDGE 4
#define SELECT_MAX_LONGESTEDGE -4

#define SELECT_MIN_SHORTESTEDGE 5
#define SELECT_MAX_SHORTESTEDGE -5

#define SELECT_RANDOM 9
#define SELECT_RANDOM_PC 19

#define SELECT_LOOKFORWARD 20

#define COMBINE_UNION 1
#define COMBINE_INTERSECTION 2


/**
 *@param[in, out] genomes the genomes
 *@param[in] m number of input genomes
 *@param[in] names names of the m input genomes
 *@param[in] solver median solver to use

// *@param[in] conserved number/preserving reversaldistance with respect to conserved intervals
// *@param[in] common number/prserving reversaldistance with respect to common intervals
// * @param[in] pair_pd use pairwise perfect distance / global perfect distance:
// * 	a) pairwise: for each pair of median,permutation the common intervals are computed and
// * 		the perfect distance is computed with respect to these intervals
// *  b) global: the common intervals for the permutations are computed and the perfect distance
// * 		for each pair of permutation,median is computed with respect to these common intervals
// *@param[in] sign use signed common intevals

 *@param[in] all_triples try all equally good triples and pairs
 *@param[in] all_medians use all medians returned from caprara
 *@param[in] all_medians_deepest skip all median computation on the last level
 *@param[in] pick filter modes
 *@param[in] pick_level variable for the filters
 *@param[in] combine how should the filter results be combined ? union / intersection
 *@param[in] random select random smallest pair / triple
 *@param[in] greedy take always only the best
 *@param[in] greedyness how much deviation of the greedy principle for the triple selection is allowed (e.g. 1.05 means that 5% deviation are allowed)
 *@param[in,out] dist_mat distance matrix of the genomes
 *@param[in,out] edges edges of the reconstructed tree
 *@param[in,out] included_genomes (given) genomes already inund = vector<int>(m, INT_MAX);cluded in the tree.
// *@param[in,out] median_cache the medians found on a branching level, i.e. a mapping from
// * triples (a vector of genomes) to an index in the score, medians and allmeds vector
 *@param[in,out] tree_score score of the partially reconstructed tree
 *@param[in,out] best_tree_score score of the best tree reconstructed so far
 *@param[in] lb stop if a tree with score <= lb is found (this is only useful
 * for testing .. the whole enumeration needs often lots of time -> so get the properties only until
 * a certain point (lb) of the bnb search)
 *@param[in] bounding
 * - 0: no bound
 * - 1: use the score of the best found tree as bound
 * - 2: use the score of the partial trees as bound per level
 *@param[in,out] partial_bound best score for each depth
 *@param[in] depth recursion depth (needed for progress info)
 *@param[in] progress progress of the algorithm
 *@param[out] choices triple sizes of the addings during the run of the algorithm
 *@param[out] solutions the solution trees : mapping from edge list to genome vectors (nodes)
 *@param[in] verbose print progress information
 */
void amgr(vector<genom> &genomes, int m, vector<string> &names,
		mediansolver *solver,
//		int conserved, int common, int pair_pd, int sign,
		int all_triples, int all_medians, int all_medians_deepest,
		vector<int> pick, vector<int> pick_level, int combine,
		int random, int greedy, double greedyness,
		vector<vector<unsigned> > &dist_mat, vector<pair<unsigned, unsigned> > &edges,
		vector<bool> &included_genomes,
		int &tree_score, int &best_tree_score, int lb,
		int bounding, vector<int> &partial_bound,
		int depth, vector<pair<unsigned, unsigned> > &progress,
		vector<unsigned> &choices,
		map<vector<pair<unsigned, unsigned> >, vector< vector<genom> >  > &solutions,
		int &solution_cnt, int verbose);

/**
 * remove unwanted medians
 *@param[in] triple the triples which produced the medians
 *@param[in,out] medians the sets of medians of the triples
 *@param[in] genomes input genomes
 *@param[in] included_genomes flags for connected / unconnected nodes
 *@param[in] m number of input genomes
 *@param[in] mode selection mode
 *@param[in] p a percentage
 *@param[in] combine how the results of the filters should be combined ? (union/intersection)
// *@param[in] conserved number/prserving reversaldistance with respect to conserved intervals
// *@param[in] common number/prserving reversaldistance with respect to common intervals
// *@param[in] sign use signed common intevals
 *@param[in,out] dist_mat distance matrix of the genomes
 *@param[in,out] edges edges of the reconstructed tree
 */
void filter_medians(const vector<pair<int, vector<int> > > &triple,
		vector<vector<genom> > &medians, vector<genom> &genomes,
		vector<bool> &included_genomes, int m, vector<int> mode,
		vector<int> p,
		int combine,
//		int conserved, int common, int sign,
		dstnc *dst,
		vector<vector<unsigned> > &dist_mat, vector<pair<unsigned, unsigned> > &edges);

/**
 * compute the lower bound for a partially reconstructed tree
 *@param[in] genomes the given genomes
 *@param[in] m number of genomes
 *@param[in] dist_mat the distances between the genomes
 *@param[in] included_genomes marked 1 of a genome is included
 *@param[in] tree_score score of the partially reconstructed tree
 *@return the lowerbound
 */
int lower_bound(vector<genom> &genomes, unsigned m,
	vector<vector<int> > &dist_mat, vector<bool> &included_genomes,
	int &tree_score);

/**
 *@param[in] progress progress information
 *@param[in] best_tree_score best score
 */
void print_progress(vector<pair<unsigned, unsigned> > &progress, int tree_score,
	int best_tree_score, int solution_cnt);

/**
 * get the next triples which (could) be tried; if there are no edges then all triples are returned,
 * else all pairs of edges and free nodes (i.e. triples) are returned
 *@param[in] genomes the genomes
 *@param[in] m number of input genomes
 *@param[in] dist_mat distance matrix of genomes
 *@param[in] edges edges of the reconstructed tree
 *@param[in] included_genomes genomes already included in the tree.
 *@return pair of the bound and the triple (indices of the genomes), and fourth entry which is the
 * index of the edge which belongs to the 2nd and 3rd genome of the triple
 */
vector<pair<int, vector<int> > > triples(
	vector<genom> &genomes, unsigned m, const vector<vector<unsigned> > &dist_mat,
	vector<pair<unsigned, unsigned> > &edges, vector<bool> &included_genomes);

#endif// _AMGR_HPP_


//~ /**
 //~ *@param[in] genomes the genomes
 //~ *@param[in] m number of input genomes
 //~ *@param[in] dist_mat distance matrix of genomes
 //~ *@param[in] mode selection modus (smallest / biggest / random)
 //~ *@param[in] edges edges of the reconstructed tree
 //~ *@param[in] included_genomes genomes already included in the tree.
 //~ *@return the pairs of unconnected genomes and edges of the partial tree with a lower
 //~ * bound for the connection
 //~ */
//~ vector<pair<int, pair<unsigned, unsigned> > > nearest_nodes(vector<genom> &genomes,
		//~ unsigned m, const vector<vector<int> > &dist_mat, int mode,
		//~ vector<pair<unsigned, unsigned> > &edges,
		//~ vector<bool> &included_genomes);

//~ /**
 //~ * get the pairs of indices with the smalles entries in the given distance matrix (not 0)
 //~ *@param[in] dist_mat the distance matrix
 //~ *@param[in] mode selection mode (smallest / biggest )
 //~ *@return pairs of indices (where the minima of the matrix are)
 //~ */
//~ vector<pair<unsigned, unsigned> > smallest_pairs(const vector<vector<int> > &dist_mat,
	//~ int mode);
