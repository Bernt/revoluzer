/*
 * rtmedian.hpp
 *
 *  Created on: Dec 18, 2008
 *      Author: maze
 */

#include "median.h"
#include "median.hpp"

#ifndef RTMEDIAN_HPP_
#define RTMEDIAN_HPP_

class rtmedian : public mediansolver{
private:
	int _n;
	median::Median _med;
public:

	/**
	 * constructor
	 * @param[in] n length of genomes
	 * @param[in] argc
	 * @param[in] argv allowed options --wt --wr
	 */
	rtmedian( int n, int argc, char *argv[] );

	/**
	 * return the reversal distance of g1 and g2,
	 * you dont have to pass genomes this is not needed here
	 * @see mediansolver::distance
	 */
	unsigned distance( const genom &g1, const genom &g2, const vector<genom> &genomes = vector<genom>() );

	/**
	 * the output operation
	 * @see mediansolver::output
	 */
	ostream &output( ostream &os ) const;

	/**
	 * print available commandline options
	 */
	void printopt() const;

	/**
	 * solve a reversal median problem
	 * @see mediansolver::solve
	 */
	void solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
			int &mediancnt, int &score, vector<genom> &medians);

};

#endif /* RTMEDIAN_HPP_ */
