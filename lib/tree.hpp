#ifndef TREE_HPP_
#define TREE_HPP_

#include "helpers.hpp"

#include <algorithm>
#include <iterator>
#include <limits>
#include <map>

template <class T> class phy;

template <class T> class node{
	template <class> friend class phy;
	private:
		vector<node *> chld;
		node *prnt;
		string name;
		T value;
		int dist;
	public:

	/**
	 * empty constructor
	 */
	node();

	/**
	 * copy constructor
	 */
	node(const node &in);

	/**
	 * construct a subtree given in newick format
	 * @param[in] nwk the newick formated tree
	 * @param[in] nombre node number
	 * @param[in] p the parent of the node (default NULL)
	 * @param[in] p the initial value of the nodes
	 */
	node( const string &nwk, int &nombre=0, node<T> *p=NULL, T initv = T() );

	/**
	 * destructor
	 */
	~node();

	/**
	 * get the bipartitions defined by the subtree
	 * @param[out] bp the bipartitions
	 * @param[in] mapping from leaf names to indices
	 * @return the leaves of the subtree
	 */
	set<unsigned> bipartitions( set< set<unsigned> > &bp, const map<string,unsigned> &name_idx ) const;

	/**
	 * get the depth of a node
	 * depth of a leaf is 0,
	 * depth of a node is max child depth +1
	 * @return depth
	 */
	unsigned depth();

	/**
	 * get all nodes in the subtree for each depth
	 * @param[out] po the nodes for each depth
	 * @param[in] leaves get the leaf nodes?
	 * @return depth of the node
	 */
	unsigned depthorder_nodes( vector<vector<node<T> *> > &po, bool leaves );

	/**
	 * get all nodes in the subtree ordered by depth
	 * @param[out] po the nodes in depth first order
	 * @param[in] leaves get the leaf nodes?
	 */
	void depthorder_nodes( vector<node<T> *> &po, bool leaves );

	/**
	 * dump the data of the nodes in the subtree into the stream
	 * @param[in,out] os the stream to write into
	 */
	void dump_data( ostream &os );

	/**
	 * get the childs of the node
	 * @return the childs
	 */
	vector<node<T> *> get_childs();

	/**
	 * get the iterator to the chld.begin()
	 * @return the iterator
	 */
	typename vector<node<T>*>::iterator get_child_begin();

	/**
	 * get the iterator to the chld.end()
	 * @return the iterator
	 */
	typename vector<node<T>*>::iterator get_child_end();

	/**
	 * @return the number of children
	 */
	unsigned get_child_size();

	/**
	 * get the values of the child nodes
	 * @return the values of the childs as vector
	 */
	vector<T> get_child_values();

	/**
	 * get the names of the child nodes
	 * @return the names of the childs as vector
	 */
	vector<string> get_child_names();

	/**
	 * get the pointers to the leafs of the subtree
	 * @param[out] leafs the pointers
	 */
	void get_leafs(map<string, node<T> *> &leafs);

	/**
	 * getter for the name of a node
	 * @return the name
	 */
	string get_name() const;

	/**
	 * get the pointer to the parent node
	 * @return the pointer to the parent
	 */
	node<T> * get_parent();

	/**
	 * get the parent value
	 * @return the parent value
	 */
	T get_parent_value();

	/**
	 * get the siblings of the current node, if any
	 * @param[out] sbl vector for appending the siblings
	 */
	void get_siblings(vector<node<T> *> &sbl);

	/**
	 * get the value of the node
	 * @return the value
	 */
	T get_value();

	/**
	 * get a pointer to the value of the node
	 * @return a pointer to the data
	 */
	T * get_value_ptr();

	/**
	 * check if the node is a leaf node
	 * @return true if it is a leaf, false else
	 */
	bool is_leaf() const;

	/**
	 * check if the node is the root node
	 * @return true if it is the root, false else
	 */
	bool is_root() const;

	/**
	 * get nodes in the subtree for which foo returns true,
	 * possibly pruning subtrees where false is returned
	 * @param[in] foo a function which takes the value of the node as parameter,
	 * 	nodes are added to nds if foo returns true or is NULL
	 * @param[in] subtree if true: include subtrees where foo returns false
	 */
	void nodes( vector<node<T> *> &nds, bool (*foo)(T&) = NULL, bool subtree = false);

	/**
	 * output operator for a node (and its subtree), prints newick format
	 * @param[in,out] os the stream to write into
	 * @param[in] r the node
	 */
	template<class S> friend ostream& operator<<(ostream& os, node<S>& r);

	/**
	 * get all nodes in the subtree in post order
	 * @param[out] po the nodes in postorder
	 * @param[in] leaves get the leaf nodes?
	 */
	void postorder_nodes( vector<node<T> *> &po, bool leaves );

	/**
	 * get all non leaf nodes in the subtree in pre order
	 * @param[out] po the nodes in preorder
	 * @param[in] leaves get the leaf nodes?
	 */
	void preorder_nodes( vector<node<T> *> &po, bool leaves );

	/**
	 * prune the subtree below the node
	 * in the resulting tree the node will be a leaf
	 */
	void prune( );

	/**
	 * prune a child from the tree
	 * @param ch the child to remove
	 * @return true iff successful
	 */
	bool remove_child( node<T> *ch );

	/**
	 * remove nodes from the tree that have only one child
	 */
	void remove_one_child_nodes();

	/**
	 * set the distance to the parent
	 * @param[in] d the distance
	 */
	void set_dist(int d);

	/**
	 * set node name
	 * @param[in] n the name
	 */
	void set_name( const string &n );

	/**
	 * set the parent pointer
	 * @param[in] p the new parent pointer
	 */
	void set_parent( node<T> *p);

	/**
	 * set the value of a node
	 * @param[in] v the new value
	 */
	void set_value( const T &v );

	void tonw_displaycss( ostream &os);

	void todot(ostream &os);
};

// *******************************************************************************************
template <class T> class phy{
	private:
		node<T> *root;
		map<string, node<T> *> leafs;
	public:

	/**
	 * empty constructor
	 */
	phy();

	/**
	 * copy constructor
	 * @param[in] in the tree to copy
	 */
	phy(const phy &in);

	/**
	 * construct a tree given in newick format
	 * @param[in] nwk the newick description
	 * @param[in] rt root the tree after reading
	 * @param[in] unrt unroot the tree after reading
	 * @param[in] initv the initial value to assign to all nodes
	 */
	phy(const string &nwk, bool rt=false, bool unrt=false, T initv = T());

	/**
	 * construct a tree given in newick format, and assign data to the leafs
	 * @param[in] nwk the newick description
	 * @param[in] names the names of the data, should correspond to the leafnames of the tree
	 * @param[in] data the data to assign to the leafs
	 * @param[in] rt root the tree after reading
	 * @param[in] unrt unroot the tree after reading
	 * @param[in] initv the initial value to assign to all non leaf nodes
	 */
	phy(const string &nwk, const vector<string> &names, const vector<T> &data, bool rt=false, bool unrt=false, T initv = T());

	/**
	 * destructor
	 */
	~phy();

	/**
	 * get the bipartitions defined by the tree
	 * @param[in] names vector of species names (must be equal to leaf names)
	 * @return the bipartitions (each bipartition is a two element set containing two sets)
	 */
	set<set<set<unsigned> > > bipartitions( const vector<string> &names ) const;

	/**
	 * get the depth of the tree, i.e. depth of the root
	 * @see node::depth
	 * @return depth
	 */
	unsigned depth();

	/**
	 * get all non-leaf nodes of the tree ordered by depth
	 * @param[out] po the nodes in depth order
	 * @param[in] leaves get the leaf nodes? default false
	 */
	void depthorder_nodes( vector<node<T> *> &nodes, bool leaves = false);

	/**
	 * get the list of names of the leaves
	 * @return the list
	 */
	vector<string> get_names(  ) const;
	/**
	 * get the root node
	 */
	node<T> * get_root();

	/**
	 * root an unrooted tree
	 */
	void mkroot( );

	/**
	 * unroot a rooted tree
	 */
	void mkunroot( );

	/**
	 * output operator for a tree, prints newick formated tree and the data at the nodes
	 * @param[in,out] os the stream to write into
	 * @param[in] r the node
	 */
	template<class S> friend ostream& operator<< (ostream &os, const phy<S>&r);

	/**
	 * copy assignment operator
	 * @param[in] in the element to copy
	 * @return the copy (surprise)
	 */
	phy& operator= (const phy& in);

	/**
	 * get all non-leaf nodes of the tree in post order
	 * @param[out] po the nodes in post order
	 * @param[in] leaves get the leaf nodes? default false
	 */
	void postorder_nodes( vector<node<T> *> &po, bool leaves = false);

	/**
	 * get all non-leaf nodes of the tree in pre order
	 * @param[out] po the nodes in post order
	 * @param[in] leaves get leaf nodes ? default false
	 */
	void preorder_nodes( vector<node<T> *> &po, bool leaves = false);

	/**
	 * remove all nodes from the tree that have only one child
	 */
	void remove_one_child_nodes();

	/**
	 * compute the robinson foulds distance
	 * @param[in] tree another tree
	 * @return the robinson foulds distance
	 */
	double robfould( const phy<T> &tree );

	/**
	 * compute the components of the robinson foulds distance
	 * @param[in] tree another tree
	 * @return a pair consisting of
	 * a) the number of splits in this-tree which are not in tree
	 * b) the number of splits in tree which are not in this-tree
	 */
	pair<unsigned,unsigned> splitdiff( const phy<T> &tree );


	/**
	 * write css description for use with nw_display
	 * @return[in,out] os the stream to write into
	 */
	void tonw_displaycss( ostream &os);

	/**
	 * write dot representation of the tree
	 * @param[in,out] the stream to write into
	 */
	void todot(ostream &os);
};

// *******************************************************************************************
// empty constructor
// *******************************************************************************************
template<class T> node<T>::node() {
	prnt = NULL;
	name = "";
	value = T();
	dist = std::numeric_limits< int >::max();
}

// *******************************************************************************************
// copy constructor
// *******************************************************************************************
template<class T> node<T>::node(const node &in){
	prnt = NULL;
	name = in.name;
	dist = in.dist;
	value = in.value;
	for(unsigned i=0; i<in.chld.size(); i++){
		node<T> *n = new node<T>( *(in.chld[i]) );
		n->set_parent( this );
		chld.push_back( n );
	}

}

// *******************************************************************************************
// construct a subtree given in newick format
// *******************************************************************************************
template<class T> node<T>::node(const string &nwk, int &nombre, node<T> *p, T initv){
	int pl=0;		// paranthesis level
	string part="";	// a part of the newick string
	unsigned i;

//	cout << nwk << endl;

	for(i=0; i<nwk.size(); i++){

		// increase parenthesis level
		if(nwk[i] == '('){
			pl++;
		}

//		cerr << nwk[i]<<"("<<pl<<")";

		// a subtree stops for nesting level 1 at ',' or ')'
		if(pl == 1 && (nwk[i]==',' || nwk[i] == ')') ){
			node<T> *nn = new node(part, nombre, this, initv);
			chld.push_back( nn );
			part = "";
		}
		// otherwis just record the content of the current subtree
		else if( !(pl==1 && nwk[i] == '(') ){
			part += nwk[i];
		}

		// decrease parenthesis level
		if(nwk[i] == ')'){
			pl--;
		}
//		if(pl < 1)
//			break;
	}

	// set the parent and the name
	prnt = p;
	value = initv;
	if( part != "" ){
		name = part;
	}else{
//		cout <<"i " << i << " len "<<nwk.size();
		if( nwk.begin()+i+1 == nwk.end()){
			name = string(nwk.begin()+i+1, nwk.end());
		}
	}

	// remove branch length if present
	string::size_type col = name.find( ":" );
	if( col != string::npos ) {
		name.erase(name.begin()+col, name.end());
	}

	col = name.find(";", 0);
	if( col != string::npos ) {
		name.erase(name.begin()+col, name.end());
	}
	dist = std::numeric_limits< int >::max();


		// set some "useful" name for nodes without a name
	if(name == "" || name ==";"){
		name = "A"+int2string(nombre);
		nombre++;
	}

//	cout << "name "<<name<<endl;
	return ;
}

// *******************************************************************************************
// destructor
// *******************************************************************************************
template<class T> node<T>::~node(){

	for(unsigned i=0; i<chld.size(); i++){
		delete chld[i];
	}
	chld.clear();
	name.clear();
}


// *******************************************************************************************
//
// *******************************************************************************************
template<class T>
set<unsigned> node<T>::bipartitions( set< set<unsigned> > &bp, const map<string, unsigned> &name_idx ) const{
	map<string,unsigned>::const_iterator it;
	set<unsigned> nn,	// union of the sets of the childs
		cnn;

//	if( is_root() ){
//		for (unsigned i=0; i<chld.size(); i++)
//			cout << chld[i]->get_name()<<endl;
//	}
//	cout <<"NODE "<< chld.size()<<" "<< get_name()<<endl;

	if( is_leaf() ){
		it = name_idx.find( name );
		if( it == name_idx.end() ){
			cerr << "node<T>::bipartitions: could not find index of "<<name<<endl;
			for(map<string, unsigned>::const_iterator it=name_idx.begin(); it!= name_idx.end(); it++  )
				cerr<< it->first <<endl;
			exit(EXIT_FAILURE);
		}
//		idx = name_idx[ name ];
		nn.insert( it->second );
	}else{
		for( unsigned i=0; i<chld.size(); i++ ){
			cnn = chld[i]->bipartitions( bp, name_idx );
			bp.insert( cnn );
			nn.insert( cnn.begin(), cnn.end() );
			cnn.clear();
		}
	}

	return nn;
}


// *******************************************************************************************
//
// *******************************************************************************************
template<class T>
unsigned node<T>::depth(){
	unsigned depth = 0;
	for( unsigned i=0; i<chld.size(); i++ ){
		depth = max( depth, chld[i]->depth()+1 );
	}
	return depth;
}
// *******************************************************************************************
//
// *******************************************************************************************
template<class T>
void node<T>::depthorder_nodes( vector<node<T> *> &nodes, bool leaves ){
	vector<vector<node<T> *> > nlevels;
	depthorder_nodes( nlevels, leaves );
	for( unsigned i=0; i<nlevels.size(); i++ ){
		for(unsigned j=0; j<nlevels[i].size(); j++){
			nodes.push_back( nlevels[i][j] );
		}
	}
}

// *******************************************************************************************
//
// *******************************************************************************************
template<class T>
unsigned node<T>::depthorder_nodes( vector<vector<node<T> *> > &nodes, bool leaves ){
	unsigned depth = 0;
	for( unsigned i=0; i<chld.size(); i++ ){
		depth = max(depth, chld[i]->depthorder_nodes(nodes, leaves)+1);
	}
	if( nodes.size() < depth+1 ){
		nodes.resize( depth+1 );
	}

	if( !is_leaf() || leaves  ){
		nodes[depth].push_back(this);
	}
	return depth;
}

// *******************************************************************************************
//
// *******************************************************************************************
template<class T>
void node<T>::dump_data( ostream &os ){
	os << ">"<<name<<endl<<value<<" -> (";
	for( unsigned i=0; i<chld.size(); i++){
		os << chld[i]->get_name()<<",";
	}cout <<")"<< endl;
	for( unsigned i=0; i<chld.size(); i++){
		chld[i]->dump_data(os);
	}
}

// *******************************************************************************************
// get chld.begin()
// *******************************************************************************************

template<class T>
typename vector<node<T> *>::iterator node<T>::get_child_begin(){
	return chld.begin();
}

// *******************************************************************************************
// get childs.end()
// *******************************************************************************************

template<class T>
typename vector<node<T> *>::iterator node<T>::get_child_end(){
	return chld.end();
}

// *******************************************************************************************
// get number of children
// *******************************************************************************************

template<class T>
unsigned node<T>::get_child_size(){
	return chld.size();
}

// *******************************************************************************************
// get the childs
// *******************************************************************************************
template<class T>
vector<node<T> *> node<T>::get_childs(){
	return chld;
}


// *******************************************************************************************
// get the values of the childs
// *******************************************************************************************
template<class T>
vector<T> node<T>::get_child_values(){
	vector<T> vals;

	for(unsigned i=0; i<chld.size(); i++){
		vals.push_back( chld[i]->value );
	}
	return vals;
}

// *******************************************************************************************
// get the names of the childs
// *******************************************************************************************
template<class T>
vector<string> node<T>::get_child_names(){
	vector<string> names;

	for(unsigned i=0; i<chld.size(); i++){
		names.push_back( chld[i]->name );
	}
	return names;
}


// *******************************************************************************************
// get the leafs .. mapping from the names to the pointers to the leafs
// *******************************************************************************************
template<class T>
void node<T>::get_leafs(map<string, node<T> *> &leafs){
	for(unsigned i=0; i<chld.size(); i++){
		chld[i]->get_leafs( leafs );
	}
	if(chld.size() == 0){
		leafs[name] = this;
	}
}

// *******************************************************************************************
// get the name
// *******************************************************************************************

template<class T>
string node<T>::get_name() const{
	return name;
}

// *******************************************************************************************
// get the pointer to the parent node
// *******************************************************************************************

template<class T>
node<T> * node<T>::get_parent(){
	return prnt;
}

// *******************************************************************************************
// get the parent value
// *******************************************************************************************

template<class T>
T node<T>::get_parent_value(){
	if(prnt != NULL)
		return prnt->value;
	else
		return T();
}

// *******************************************************************************************
// get the siblings of the current node
// *******************************************************************************************

template<class T>
void node<T>::get_siblings(vector<node<T> *> &sbl){
	node<T> *p = get_parent();

		// no parent -> no siblings
	if( p == NULL ){
		return;
	}
		// append all childs which are not equal to the node itself
	for( unsigned i=0; i<p->chld.size(); i++ ){
		if( chld[i] != this ){
			sbl.push_back(this);
		}
	}

}

// *******************************************************************************************
// get the value
// *******************************************************************************************

template<class T>
T node<T>::get_value(){
	return value;
}

// *******************************************************************************************
// get a pointer to the value
// *******************************************************************************************

template<class T>
T * node<T>::get_value_ptr(){
	return &value;
}

// *******************************************************************************************
// check if the node is a leaf
// *******************************************************************************************

template<class T>
bool node<T>::is_leaf() const{
	return ( chld.size() == 0 );
}

// *******************************************************************************************
// check if the node is the root
// *******************************************************************************************

template<class T>
bool node<T>::is_root() const{
	return (prnt==NULL);
}

// *******************************************************************************************
// get nodes in the subtree for which foo returns true, possibly pruning subtrees where false
// is returned (if subtree is true: include subtrees where foo returns false)

// *******************************************************************************************
template<class T>
void node<T>::nodes( vector<node<T> *> &nds, bool (*foo)(T&), bool subtree){
	bool decision;
	if (foo == NULL){
		decision = true;
	}else{
		decision = foo(value);
	}

	if( decision == false || subtree == true ){
		for( unsigned i=0; i<chld.size(); i++ ){
			chld[i]->nodes( nds, foo, subtree );
		}
	}
	if ( decision == true ){
		nds.push_back(this);
	}
}

// *******************************************************************************************
// output operator
// *******************************************************************************************
template<class S> ostream& operator<<(ostream& os, node<S>& r){
	for(unsigned i=0; i<r.chld.size(); i++){
		if(i==0)
			os << "(";
		os << *(r.chld[i]);
		if(i == r.chld.size()-1 )
			os << ")";
		else
			os << ",";
	}
	os <<r.name;
	if(r.dist != std::numeric_limits< int >::max())
		os << ":"<<r.dist;

	return os;
}

// *******************************************************************************************
// get all non leaf nodes in the subtree in postorder
// *******************************************************************************************
template<class T> void node<T>::postorder_nodes( vector<node<T> *> &po, bool leaves ){
	for(unsigned i=0; i<chld.size(); i++){
		chld[i]->postorder_nodes(po, leaves);
	}
	if( chld.size() > 0 || leaves == true ){
		po.push_back( this );
	}
}

// *******************************************************************************************
// get all non leaf nodes in the subtree in preorder
// *******************************************************************************************
template<class T> void node<T>::preorder_nodes( vector<node<T> *> &po, bool leaves ){
	if( chld.size() > 0 || leaves == true ){
		po.push_back( this );
	}
	for(unsigned i=0; i<chld.size(); i++){
		chld[i]->preorder_nodes(po, leaves);
	}
}

// *******************************************************************************************
//
// *******************************************************************************************
template<class T> void node<T>::prune(){
	for( unsigned i=0; i<chld.size(); i++ ){
		delete chld[i];
	}
	chld.clear();
}

// *******************************************************************************************
//
// *******************************************************************************************

template<class T>
bool node<T>::remove_child( node<T> *ch ){
	for( unsigned i=0; i<chld.size(); i++){
		if( chld[i] == ch ){
			chld[i]->prune();
			delete chld[i];
			chld.erase( chld.begin() + i);
			return true;
		}
	}
	return false;
}


// *******************************************************************************************
//
// *******************************************************************************************

template<class T>
void node<T>::remove_one_child_nodes(){
	node<T> *tmp;

	for( unsigned i=0; i<chld.size(); ){
		if( chld[i]->get_child_size() == 1 ){
			chld[i]->chld[0]->prnt = this;
			tmp = chld[i]->chld[0];
			chld[i]->chld.clear();
			delete chld[i];
			chld[i] = tmp;
		}else{
			chld[i]->remove_one_child_nodes();
			i++;
		}
	}
}

// *******************************************************************************************
// distance setter
// *******************************************************************************************
template<class T> void node<T>::set_dist(int d){
	dist = d;
}

// *******************************************************************************************
// name setter
// *******************************************************************************************
template<class T> void node<T>::set_name( const string &n ){
	name = n;
}

// *******************************************************************************************
// parent setter
// *******************************************************************************************
template<class T> void node<T>::set_parent( node<T> *p){
	prnt = p;
}

// *******************************************************************************************
// value setter
// *******************************************************************************************
template<class T> void node<T>::set_value( const T &v ){
	value = v;
}

// *******************************************************************************************
//
// *******************************************************************************************
template<class T> void node<T>::tonw_displaycss(ostream &os){
	string fillcolor,
		elabel,
		nlabel;

	nlabel = name;
	value.dotnodeprop(fillcolor, nlabel);

	elabel="";
	if( prnt != NULL){
		(value).dotedgeprop(prnt->value, elabel);
	}

	os <<"\"";
	if( elabel != "" ){
		os << "<text x='-5' y='-3' style='font-size:8'>"<<elabel<<"</text>";
	}
	os << "<circle fill='"<<fillcolor<<"' r='5'/>\" I " << name<<endl;

	for( unsigned i=0; i<chld.size(); i++ ){
		chld[i]->tonw_displaycss(os);
	}
}

// *******************************************************************************************
//
// *******************************************************************************************
template<class T> void node<T>::todot(ostream &os){
	string fillcolor,
		elabel,
		nlabel;

	for( unsigned i=0; i<chld.size(); i++ ){
		nlabel = name;
		value.dotnodeprop(fillcolor, nlabel);
		os <<"\""<< name<< "\" [";
		if(fillcolor != ""){
			os << "fillcolor="<<fillcolor<<", style=filled, ";
		}
		os << "label=\""<<nlabel<<"\"];"<<endl;

		chld[i]->todot(os);
		elabel="";
		(chld[i]->value).dotedgeprop(elabel);
		os <<"\""<< chld[i]->name <<"\" -- \""<< name<<"\" [label=\""<<elabel<<"\"];"<<endl;
	}
}

// *******************************************************************************************
// empty constructor
// *******************************************************************************************
template<class T> phy<T>::phy(){
	root = NULL;
}

// *******************************************************************************************
// copy constructor
// *******************************************************************************************
template<class T> phy<T>::phy(const phy &in){
		// copy the tree
	root = new node<T>( *(in.root) );

		// assign the leafs
	root->get_leafs( leafs );
}


// *******************************************************************************************
// construct a tree given in newick format
// *******************************************************************************************
template<class T> phy<T>::phy(const string &nwk, const vector<string> &names, const vector<T> &data, bool rt, bool unrt, T initv){

	typename map<string,node<T> >::iterator it;
	int nombre=0;

		// init the tree
	root = new node<T>(nwk, nombre, NULL, initv);

		// init the leaves' data
	root->get_leafs( leafs );
	vector<string> missing_leaf, missing_data;
	for( typename map<string, node<T> *>::iterator it = leafs.begin(); it != leafs.end(); it++){
		if( find(names.begin(), names.end(), it->first ) == names.end() ){
			missing_data.push_back( it->first );
		}
	}
	for( vector<string>::const_iterator it=names.begin(); it!=names.end(); it++ ){
		if( leafs.find( *it ) == leafs.end() ){
			missing_leaf.push_back( *it );
		}
	}
	if( missing_data.size() > 0 || missing_leaf.size() > 0 ){
		cerr << "error: could not construct tree: leafset != nameset "<<endl;
		cerr << "missing leafs:"; copy(missing_leaf.begin(), missing_leaf.end(), ostream_iterator<string>(cerr," "));cerr<<endl;
		cerr << "missing data :"; copy(missing_data.begin(), missing_data.end(), ostream_iterator<string>(cerr," "));cerr<<endl;
		exit(EXIT_FAILURE);
	}



//	if( leafs.size() != names.size() ){
//		cerr << "error: could not construct tree: leafset != nameset "<<endl;
//		cout << "leafs ";
//		for( typename map<string, node<T> *>::iterator it = leafs.begin(); it != leafs.end(); it++){
//			cout << it->first <<" ";
//		}
//		cout << endl;
//		cout << "names "; copy(names.begin(), names.end(), ostream_iterator<string>(cout," ")); cout << endl;
//		exit(1);
//	}

	for( unsigned i=0; i<names.size(); i++){
		if (leafs.find( names[i] ) == leafs.end() ){
			cerr << "phy: there is no leaf with the name "<<names[i]<<endl;
			cerr << "     the names of the leafs and the genomes have to consistent"<<endl;
			cerr << "    leafs  :"; for(typename map<string, node<T> *>::iterator it=leafs.begin(); it!=leafs.end(); it++) cerr << it->first<<" "; cerr << endl;
			cerr << "    genomes:"; copy(names.begin(), names.end(), ostream_iterator<string>(cerr, " ")); cerr << endl;
			exit(0);
		}
		leafs[ names[i] ]->set_value( data[i] );
	}
	if(rt && !unrt){
		mkroot();
	}else if(!rt && unrt){
		mkunroot();
	}else if(rt && unrt){
		cerr << "error: phy::phy(): root and unroot is true"<<endl;
		exit(0);
	}

	remove_one_child_nodes();
}

// *******************************************************************************************
// construct a tree given in newick format
// *******************************************************************************************
template<class T> phy<T>::phy(const string &nwk, bool rt, bool unrt, T initv){

	typename map<string,node<T> >::iterator it;
	int nombre=0;

		// init the tree
	root = new node<T>(nwk, nombre, NULL, initv);

		// init the leaves' data
	root->get_leafs( leafs );
	if(rt && !unrt){
		mkroot();
	}else if(!rt && unrt){
		mkunroot();
	}else if(rt && unrt){
		cerr << "error: phy::phy(): root and unroot is true"<<endl;
		exit(0);
	}
}


// *******************************************************************************************
// destructor
// *******************************************************************************************
template <class T>phy<T>::~phy(){
	if( root != NULL )
		delete root;
	leafs.clear();
}


// *******************************************************************************************
// get the bipartitions defined by the tree
// *******************************************************************************************

template <class T>
set<set<set<unsigned> > > phy<T>::bipartitions( const vector<string> &names ) const{
	map<string, unsigned> name_idx; // mapping of the names to the index in the names vector

	set<set<set<unsigned> > > bps;	// all bipartitions (always the part containing the 1)
	set<set<unsigned> > bp,		// one bipartition
		parts;	//
	set<unsigned> lfs,	// the leaf names
		complement;

	for( unsigned i=0; i<names.size(); i++ ){
		name_idx[names[i]] = i;
//		cout << "names "<<names[i]<<endl;
	}

	lfs = root->bipartitions( parts, name_idx );
//	cout << "initui "<<parts.size()<<endl;
	if( lfs.size() != leafs.size() ){
		cerr << "phy<T>::bipartitions(): wrong leaf count: "<<lfs.size()<<" "<<leafs.size()<<endl;
		cerr << *this << endl;
		exit(EXIT_FAILURE);
	}

	for( set<set<unsigned> >::iterator it = parts.begin(); it != parts.end(); it++ ){
		// get the complement of the part
		insert_iterator<set<unsigned> > ii(complement, complement.begin());
		set_difference(lfs.begin(), lfs.end(), it->begin(), it->end(), ii);
		// construct the bipartition
		bp.insert( *it );
		bp.insert( complement );
		// add the bipartition
		bps.insert(bp);
		// clean
		complement.clear();
		bp.clear();
	}
	return bps;
}

// *******************************************************************************************
//
// *******************************************************************************************
template<class T>
unsigned phy<T>::depth(){
	return root->depth();
}

// *******************************************************************************************
//
// *******************************************************************************************
template<class T>
void phy<T>::depthorder_nodes( vector<node<T> *> &nodes, bool leaves ){
	root->depthorder_nodes( nodes, leaves );
}

// *******************************************************************************************
// get name list
// *******************************************************************************************
template <class T> vector<string> phy<T>::get_names() const{
	vector<string> nm;
	for(typename map<string,node<T>* >::const_iterator it = leafs.begin(); it != leafs.end(); ++it){
		nm.push_back(it->first);
	}
	return nm;
}



// *******************************************************************************************
// get the root pointer
// *******************************************************************************************
template <class T> node<T> * phy<T>::get_root(){
	return root;
}

// *******************************************************************************************
//
// *******************************************************************************************

template <class T> void phy<T>::mkroot( ){
//	cerr << "mkroot"<<endl;
	if( root->chld.size() < 3 ){
		return;
	}

	node<T> *nroot = NULL;

	nroot = new node<T>();
	nroot->name = "root";

	nroot->chld.push_back( root->chld.back() );
	root->chld.pop_back();

	nroot->chld.push_back( root );
	root = nroot;
}

// *******************************************************************************************
//
// *******************************************************************************************

template <class T> void phy<T>::mkunroot( ){
	unsigned i=0;
//	cerr << "mkunroot"<< *root << endl;
	if( root->chld.size() < 2 ){
		return;
	}

	// if the first child of the root is a leaf then this leaf would
	// become the new root with only one child, i.e. the leaf set is
	// altered. this wont be good. therefore swap the first child
	// with a non leaf child
	for( i=1; i<root->chld.size(); i++ ){
		if(root->chld[0]->is_leaf())
			swap(root->chld[0], root->chld[i]);
		else
			break;
	}
	if( i==root->chld.size() && root->chld[0]->is_leaf() ){
		cerr<<"mkunroot( ): all children of the root are leaves"<<endl;
		exit(1);
	}

	// now do the unrooting
	for( unsigned i=1; i<root->chld.size(); i++ ){
		root->chld[i]->prnt = root->chld[0];
		root->chld[0]->chld.push_back( root->chld[i] );
	}

	node<T> *nroot = root->chld[0];
	nroot->set_parent( NULL );

	root->chld.clear();
	delete root;

	root = nroot;
}




// *******************************************************************************************
// output operator
// *******************************************************************************************
template <class S> ostream& operator<< (ostream& os, const phy<S>& p){
	os << *(p.root)<<";";
//	p.root->dump_data(os);
	return os;
}

// *******************************************************************************************
// copy assignment operator
// *******************************************************************************************
template <class T> phy<T>& phy<T>::operator= (const phy<T>& in){
		// copy the tree
	root = new node<T>( *(in.root) );
		// assign the leafs
	root->get_leafs( leafs );
	return *this;
}

// *******************************************************************************************
// get the nodes in post order
// *******************************************************************************************
template <class T> void phy<T>::postorder_nodes( vector<node<T> *> &po, bool leaves){
	po.clear();
	root->postorder_nodes( po, leaves );
}

// *******************************************************************************************
// get the nodes in post order
// *******************************************************************************************
template <class T> void phy<T>::preorder_nodes( vector<node<T> *> &po, bool leaves){
	po.clear();
	root->preorder_nodes( po, leaves );
}

// ****************************************************************************

template <class T> void phy<T>::remove_one_child_nodes(){


	node<T> *oroot=NULL;
	while(root->chld.size() == 1){
		oroot = root;
		root = root->chld[0];
		root->prnt = NULL;

		oroot->chld.clear();
		delete oroot;
	}

	root->remove_one_child_nodes();
}

template <class T>
double phy<T>::robfould( const phy<T> &tree ){
	set<set<set<unsigned> > > bp1, bp2, sd;
	vector<string> n1,n2;

	n1 = get_names();
	n2 = tree.get_names();

	sort(n1.begin(), n1.end());
	sort(n2.begin(), n2.end());
	if( n1 != n2 ){
		cerr <<"error: robinson foulds got trees with unequal leaf sets"<<endl;
		cerr << *this << endl << tree<<endl;
		exit(EXIT_FAILURE);
	}

	bp1 = bipartitions(n1);
	bp2 = tree.bipartitions(n1);

	set_symmetric_difference(bp1.begin(), bp1.end(),
			bp2.begin(), bp2.end(), inserter( sd, sd.begin() ));

//	if( (sd.size() % 2) != 0 ){
//		cerr << "error: robinson foulds distance non integer "<<endl;
//		cerr << *this << endl << tree<<endl;
//		exit(EXIT_FAILURE);
//	}
	return ((double)sd.size())/2.0;
}

// ****************************************************************************

template <class T>
pair<unsigned,unsigned> phy<T>::splitdiff( const phy<T> &tree ){
	set<set<set<unsigned> > > bp1, bp2, d1, d2;
	vector<string> n1,n2;

	n1 = get_names();
	n2 = tree.get_names();

	sort(n1.begin(), n1.end());
	sort(n2.begin(), n2.end());
	if( n1 != n2 ){
		cerr <<"error: robinson foulds got trees with unequal leaf sets"<<endl;
		cerr << *this << endl << tree<<endl;
		exit(EXIT_FAILURE);
	}

	bp1 = bipartitions(n1);
//	for(set<set<set<unsigned> > >::iterator it=bp1.begin(); it!=bp1.end(); it++){
//		for(set<set<unsigned> >::iterator jt=it->begin(); jt!=it->end(); jt++){
//			copy(jt->begin(), jt->end(), ostream_iterator<unsigned>(cout,",")); cout << " | ";
//		}
//		cout << endl;
//	}


	bp2 = tree.bipartitions(n1);

	set_difference( bp1.begin(), bp1.end(),
			bp2.begin(), bp2.end(), inserter( d1, d1.begin() ) );
	set_difference( bp2.begin(), bp2.end(),
			bp1.begin(), bp1.end(), inserter( d2, d2.begin() ) );

//	if( (sd.size() % 2) != 0 ){
//		cerr << "error: robinson foulds distance non integer "<<endl;
//		cerr << *this << endl << tree<<endl;
//		exit(EXIT_FAILURE);
//	}
	return make_pair( d1.size(), d2.size() );
}



// ****************************************************************************
//
// *******************************************************************************************
template <class T> void phy<T>::tonw_displaycss( ostream &os){
	root->tonw_displaycss(os);
}

// ****************************************************************************
//
// *******************************************************************************************
template <class T> void phy<T>::todot( ostream &os){
	os << "graph phy {"<<endl;
    os << "    graph [rankdir=LR nodesep=0]"<<endl;
    os << "    node [shape=box]"<<endl;
    os << "    edge [fontsize=10]"<<endl;
	os << "    fontname=Helvetica;"<<endl;
	os << "    graph[fontsize=20];"<<endl;

	os << "{ rank = same; ";

	for(typename map<string, node<T> *>::iterator it=leafs.begin(); it!=leafs.end(); it++){
		 os << "\""<<(it->second)->get_name()<<"\";"<<endl;
	}
	os << "}"<<endl;

	root->todot(os);
	os << "} "<<endl;
}

#endif /*TREE_HPP_*/
