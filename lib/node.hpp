#include "genom.hpp"
#include "dstnc.hpp"

struct node{
	/*! the tree the node belongs to */
	struct bintree *tree;
	/*! the parent node of the node */
	node *parent;
	/*! the left child node of the node */
	node *lchild;
	/*! the left child node of the node */
	node *rchild;
	/*! the name of the genome at the node*/
	string name;
	/*! length of the edge to the parent */
	int length;
	/*! the genom of the node */
	genom g;
};

/**
 * append an (empty) tree given in newick format to the node
 *@param[in] n the node
 *@param[in] newick the string representation of the tree
 */
void append(node *n, string newick);

/**
 * append a tree given in newick format to the node and assign the given names and 
 * genomes to the leafs
 *@param[in,out] n the node 
 *@param[in] newick the newick representation of the tree to append
 *@param[in] genomes the genomes for the leaf nodes
 *@param[in] names the names of the leaf nodes
 */
void append(node *n, string newick, vector<genom> &genomes, vector<string> &names);

/**
 * append a tree given in newick format to the node, and set a given genome to the node, 
 * the genomes of the children are constructed by appying r random reversals
 *@param[in,out] n the node 
 *@param[in] newick the newick representation of the tree to append
 *@param[in] g the genome for the node
 *@param[in] r number of reversals to appy on g to construct the childrens
 *@param[out] applied number of applied reversals
 *@param[in] cnt counts the internal nodes, for naming
 */
void append(node *n, string newick, genom g, int r, int &applied, int &cnt);

/**
 * free the nodes below n and the node itself
 *@param[in] n the node to free
 */
void free_node(node *n);

/**
 * return a new node, where all childs are NULL and the tree is set to bt
 *@param[in] bt the tree
 *@param[in] p the parent
 *@return a new node
 */
node * init(bintree * bt, node *p);

/**
 * get the genomes of innernodes of the subtree starting at node n
 *@param[in] n the node
 *@param[out] names the names of the genomes
 *@param[out] genomes the inner node's genomes
 */
void inner_genomes(node *n, vector<string> &names, vector<genom> &genomes);

/**
 * append the inner nodes which are in the subtree starting at n to the vector
 *@param[in] n the starting node
 *@param[out] nodes the nodes in the subtree
 */
void inner_nodes(node *n, vector<node *> &nodes);

/**
 * checks if a node is a leaf node
 *@param[in] n the node
 *@return true if 
 */
bool isleaf(node *n);

/**
 * check if a given node is the root node
 *@param[in] n the node to check
 *@return true if it is the root, else false
 */
bool isroot(node * n);

/**
 * get the leaf genomes of subtree starting at node n
 *@param[in] n the node
 *@param[out] names the names of the genomes
 *@param[out] genomes the leaf genomes
 */
void leaf_genomes(node *n, vector<string> &names, vector<genom> &genomes);

/**
 * append the names of leafs of the subtree starting at node n to the vector
 *@param[in] n the node n
 *@param[out] leaf_names the vector
 */
void leaf_names(node *n, vector<string> &names);

/**
 * @param[in] n the start node
 * @return the lower bound
 */
int lower_bound(node *n, map<string, int> &name_idx,
		vector<vector<unsigned> > &dist,
		vector<string> &l, vector<string> &r, vector<vector<unsigned> > &m);

/**
 * return the newick representation of the tree below the given node
 *@param[in] bt the tree
 *@param[in] printdist print the distances (edge length)
 *@param[in] inner names of the inner nodes
 *@return nwk the newick representation
 */
string newick(node * n, bool printdist, bool inner);

/**
 * return the newick representation of the tree below the given node
 *@param[in] bt the tree
 *@param[in] distance print the distances (edge length)
 *@param[in] inner names of the inner nodes
 */
void print_newick(node * n, bool printdist, bool inner);


/**
 * return the score of the subtree starting at the node + the score of the edge to the parent node
 *@param[in] n the node
 *@return the score
 */
int score(node *n);

/**
 *sort the subtree starting here: smaller (lexicographic by names) children left
 *@param[in] n the node
 *@param[in] start name of the smalles child in the subtree
 */
void sort(node * n, string &start);

/**
 * get all triples of the tree below the given node, i.e. the three neighbours of each inner node
 *@param[in] n the node
 *@param[out] trips the triples
 *@param[out] trips_names the names of the genomes
 *@param[out] medians the inner nodes, i.e. the medians of the triples
 *@param[out] meds_names the names of the genomes
 */
void triples(node *n, 
	vector<vector<genom> > &trips, vector<vector<string> > &trips_names, 
	vector<genom> &meds, vector<string> &meds_names);
