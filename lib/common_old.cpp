
//#define DEBUG_CHANGES
//#define DEBUG_FIND_COMMON
//#define DEBUG_NORMALIZE
//#define DEBUG_SRAC
//#define DEBUG_OVERLAPPING
//#define DEBUG
#include <algorithm>
#include <iostream>
#include <limits>

#include "common_old.hpp"

using namespace std;


int changes_common_intervals(
	const vector<genom> &genomes, const genom &g,
	vector<pair<unsigned, unsigned> > &ci_pre,
	vector<pair<unsigned, unsigned> > &ci_dif,
	int sig, int circular){

	int difference;
	vector<pair<unsigned, unsigned> > ci_post;
	vector<genom> genomes_post;

	ci_dif.clear();

		// get the common intervals without g
	if(ci_pre.size() == 0)
		ci_pre = find_common_intervals(genomes, sig, 0, circular, 0);

		// get the common intervals with g
	genomes_post = genomes;
	genomes_post.push_back(g);
	ci_post = find_common_intervals(genomes_post, sig, 0, circular, 0);
	genomes_post.clear();

	difference = (int)ci_pre.size() - (int)ci_post.size();
	if(difference > 0)
		set_difference(ci_pre.begin(), ci_pre.end(),
					ci_post.begin(), ci_post.end(), back_inserter(ci_dif));
#ifdef DEBUG_CHANGES
	cout << "changes "<< difference<<endl;
#endif//DEBUG_CHANGES
	return difference;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

extern "C" int changes_common_intervals(
	struct genome_struct **genomes_ptr, int m,
	int *g_ptr, int n,
	unsigned ***ci_pre_ptr, unsigned *ci_pre_size,
	unsigned ***ci_dif_ptr, unsigned *ci_dif_size,
	int sig, int circular){

	genom g;
	int difference;
	vector<genom> genomes;
	vector<pair<unsigned, unsigned> > ci_pre,
		ci_dif;

	g = genom(g_ptr, n, 0);
	for(int i=0; i<m; i++){
		genomes.push_back( genom(genomes_ptr[i]->genes, n, circular) );
	}

	if(*ci_pre_size != 0){
		for(unsigned i=0; i<*ci_pre_size; i++){
			ci_pre.push_back(pair<unsigned, unsigned>((*ci_pre_ptr)[0][i], (*ci_pre_ptr)[1][i]) );
		}
	}

	if(*ci_dif_size != 0){
		free( (*ci_dif_ptr)[0] );
		free( (*ci_dif_ptr)[1] );
		free( (*ci_dif_ptr) );
		*ci_dif_size = 0;
	}

	difference = changes_common_intervals(genomes, g, ci_pre, ci_dif, sig, circular);

	if(*ci_pre_size == 0){
		*ci_pre_size = ci_pre.size();
		*ci_pre_ptr = (unsigned **) malloc( 2 * sizeof(unsigned *));
		(*ci_pre_ptr)[0] = (unsigned *) malloc( *ci_pre_size * sizeof(unsigned));
		(*ci_pre_ptr)[1] = (unsigned *) malloc( *ci_pre_size * sizeof(unsigned));


		for(unsigned i=0; i<*ci_pre_size; i++){
			(*ci_pre_ptr)[0][i] = ci_pre[i].first;
			(*ci_pre_ptr)[1][i] = ci_pre[i].second;
		}
	}

	if(ci_dif.size() > 0){
		*ci_dif_size = ci_dif.size();

		*ci_dif_ptr = (unsigned **) malloc( 2 * sizeof(unsigned *));
		(*ci_dif_ptr)[0] = (unsigned *) malloc( ci_dif.size() * sizeof(unsigned));
		(*ci_dif_ptr)[1] = (unsigned *) malloc( ci_dif.size() * sizeof(unsigned));


		for(unsigned i=0; i<*ci_dif_size; i++){
			(*ci_dif_ptr)[0][i] = ci_dif[i].first;
			(*ci_dif_ptr)[1][i] = ci_dif[i].second;
		}
	}

	return difference;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int compareRange(vector<pair<unsigned, unsigned> > r1, vector<pair<unsigned, unsigned> > r2){
	return (abs((int)r1.back().second - (int)r1.back().first) < abs((int)r2.back().second - (int)r2.back().first));
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

vector<pair<unsigned, unsigned> > find_common_intervals(
		const genom &g1, const genom &g2,
		int sig, int irreducible, int circular, int replace){

	vector<genom> genomes;
	vector<pair<unsigned, unsigned> > ci;

	genomes.push_back(g1);
	genomes.push_back(g2);

	ci = find_common_intervals(genomes, sig, irreducible, circular, replace);
	genomes.clear();
	return ci;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

vector<pair<unsigned, unsigned> > find_common_intervals(
		vector<genom> g, int sig, int irreducible, int circular, int replace){

	bool discard; 		// for finding duplicate comon intervals
	genomes *g_com;		// genome structure for the code from stoye
	int shift;			// genomes are shifted by
	intervals *com_int;	// common interval structure for the code from stoye
	vector<pair<unsigned, unsigned> > common_intervals,
		candidates;		// more intervals which should be in common_intervals (possibly)
	vector<genom> p;
	int max,
		min;

#ifdef DEBUG_FIND_COMMON
	cout<<"find_common_intervals circ " << circular<< " sign "<< sig<< " for "<<endl;
	for(unsigned i=0; i<g.size(); i++)
		cout <<g[i]<<endl;
#endif

		// fill the genome structure used in the code from Jens Stoye
	g_com = (genomes*) malloc(sizeof(genomes));
	g_com->num = g.size();
	g_com->max = g[0].size()-1;

	g_com->len = (int*) malloc(g.size() * sizeof(int));
	g_com->g = (int**) malloc(g.size() * sizeof(int*));
	g_com->c = (int**) malloc(g.size() * sizeof(int*));
	g_com->s = (BOOL**) malloc(g.size() * sizeof(BOOL*));
	g_com->name = (char**) malloc(g.size() * sizeof(char*));

	for(unsigned i=0; i<g.size(); i++){
		g_com->len[i] = g[i].size();

		g_com->g[i] = (int*) malloc(g[i].size() * sizeof(int));
		g_com->c[i] = (int*) malloc(g[i].size() * sizeof(int));
		g_com->s[i] = (BOOL*) malloc(g[i].size() * sizeof(BOOL));
		g_com->name[i] = (char*) malloc(sizeof(char));

		for(unsigned j=0; j<g[i].size(); j++){
			g_com->g[i][j] = abs(g[i][j]) - 1;
			g_com->c[i][j] = GENOMES_NO_END;
			if(sig>0 && g[i][j] > 0)
				g_com->s[i][j] = 1;
			else
				g_com->s[i][j] = 0;
		}

		if(circular != 0) {
			g_com->c[i][ g[i].size()-1 ] = GENOMES_CIRCULAR;
		}else{
			g_com->c[i][ g[i].size()-1 ] = GENOMES_LINEAR;
		}
		g_com->name[i][0] = '\0';
	}

		// get the irreducible common intervals
	com_int = find_common_intervals(stdout, g_com, &shift, irreducible);

#ifdef DEBUG_FIND_COMMON
	cout << "stoyes code found "<<com_int->num<<" intervals"<<endl;
	for(int i=com_int->num-1;  i>=0; i--){
		cout << com_int->start[i]<<","<< com_int->end[i]<<" ";
	}
	cout <<" | "<<shift<<endl;
#endif

		// * transfer the common intervals to the vector of pairs
		// * for irreducible intervals :
		// 	- add all intervals which cross the border to candidates vector
		// 		they will be replaced by their complement and added to common intervalls
		//		it they are irreducible
		// 	- and remove intervals i which are the intersection of two other intervals (i = i_1 \cap i_2)
		//		 [there must exist
		//		- an interval j starting left of the start of i and ending at the same index as i
		// 		- and an interval j starting at the same index as i and ending right of the end of i ]
		// * for reducible and irreducible common intervals
		// 	- remove all duplicates
	p = vector<genom>(g.size());
	for(unsigned j=1; j<g.size(); j++){
		p[j] = g[j].identify_g(g[0]);
	}
//	int left,
//		right;
	for(int i=com_int->num-1;  i>=0; i--){
			// check id the same common interval was added before (duplicate)
		discard = false;
		for(int j=com_int->num-1; j>i; j--){
			if( com_int->start[i] == com_int->start[j] &&  com_int->end[i] == com_int->end[j]){
#ifdef DEBUG_FIND_COMMON
				cout << "duplicate "<< com_int->start[i]<<","<<com_int->end[i]<<endl;
#endif//DEBUG_FIND_COMMON
				discard = true;
				break;
			}
		}
		if(discard)
			continue;

//		left = right = 0;
		if(irreducible > 0){

			if(circular > 0){
					// intervals crossing the cut in the first genom (only possible for circular)
				if(replace > 0 && com_int->start[i] > com_int->end[i]){
#ifdef DEBUG_FIND_COMMON
					cout << "replace 0 " << com_int->start[i]<<","<<com_int->end[i]<<
						" by "<<com_int->end[i]+1 <<"," <<  com_int->start[i]-1<<endl;
#endif//DEBUG_FIND_COMMON
					candidates.push_back( pair<unsigned, unsigned>( com_int->end[i]+1, com_int->start[i]-1  ) );
					continue;
				}

					// intervals crossing the cut in the other genomes
					// therefor identify the two genomes j and 0 in order to get easy the
					// indices of element in genome j
					// an interval crosses the cut if the length of the interval in the first genome
					// is not equal to the difference of the maximal and minimal
					// indices in the second genome
				if(replace > 0 && com_int->start[i] < 1){
					for(unsigned j=1; j<g.size(); j++){

						max = 0;
						min = std::numeric_limits< int >::max();
						for(int k=com_int->start[i]; k<com_int->end[i]+1; k++){
							if ( abs( p[j][k] ) < min){
								min = abs(p[j][k]);
							}
							if ( abs( p[j][k] ) > max)
								max = abs( p[j][k] );
						}
						if( (com_int->end[i] - com_int->start[i]) != (max - min)){
	#ifdef DEBUG_FIND_COMMON
							cout << "replace "<<j<<" " << com_int->start[i]<<","<<com_int->end[i]<<
								" by "<<com_int->end[i]+1 <<"," << p.size()-1<<endl;
	#endif//DEBUG_FIND_COMMON

							candidates.push_back( pair<unsigned, unsigned>(com_int->end[i]+1,  p[j].size()-1) );
							discard = true;
							break;
						}
					}

					if(discard)
						continue;
				}
			}

				// remove intervals wich are the intersection of others
//			for(int j = com_int->num-1; j>i; j--){
//				if(com_int->end[i] == com_int->end[j] && com_int->start[i] > com_int->start[j])
//					left = 1;
//				if(com_int->start[i] == com_int->start[j] && com_int->end[i] < com_int->end[j])
//					right = 1;
//			}
//			if (left == 1 && right == 1){
//#ifdef DEBUG_FIND_COMMON
//				cout << "reducible "<< com_int->start[i]<<","<<com_int->end[i]<<endl;
//#endif//DEBUG_FIND_COMMON
//				continue;
//			}
		}

		common_intervals.push_back( pair<unsigned, unsigned>(com_int->start[i], com_int->end[i]) );
	}

		// check if the complementary intervals of intervals crossing the border
		// are reducible (then dont add) or irreducible (then add)
		// sort the common intervals if only the irreducible intervals are sought .. because
		// many of the algorithms rely on that the intervals are sorted
	if(irreducible != 0){
		remove_reducible(common_intervals, candidates, g[0].size());
		sort(common_intervals.begin(), common_intervals.end());
	}

#ifdef DEBUG_FIND_COMMON
	cout << "after removing unneeded "<<common_intervals.size()<<" intervals"<<endl;
#endif

	free_intervals(com_int);
	free_genomes(g_com);

	return common_intervals;
} //find_common_intervals()



// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void remove_reducible(vector<pair<unsigned, unsigned> > &common_intervals,
		const vector<pair<unsigned, unsigned> > &candidates, unsigned n){

	bool irreducible;

	vector<vector<unsigned> > overlapping_int;		// overlapping intervals
	vector<vector<bool> > ranges;

		// get the sets of overlapping irreducible common intervals
		// and their minimal and maximal indices

		// @todo DIRTY HACK (common.cpp uses int and common_old unsigned,
		// its not trivial to modify common to use unsigned because neg.
		// interval borders are used there for coding reversed nodes)
	vector<pair<int,int> > intci;
	for( unsigned i=0; i<common_intervals.size(); i++ )
		intci.push_back( common_intervals[i] );

	overlapping_ici(intci, overlapping_int);
	ranges = vector<vector<bool> >(common_intervals.size(), vector<bool>(n,false));
	for(unsigned i=0; i<overlapping_int.size(); i++){
		for(unsigned j=0; j<overlapping_int[i].size(); j++){
			ranges[i][ common_intervals[ overlapping_int[i][j] ].first ] = true;
			ranges[i][ common_intervals[ overlapping_int[i][j] ].second ] = true;
		}
	}
		// if a candidate starts and ends at the start/end points of overlapping common intervals
		// it (should) be reducible and can be discarded
	for(unsigned i=0; i<candidates.size(); i++){
		irreducible = true;
		for(unsigned j=0; j<ranges.size(); j++){
			if(ranges[j][ candidates[i].first ] == true && ranges[j][ candidates[i].second ] == true){
				irreducible = false;
				break;
			}
		}

		if(irreducible){
			common_intervals.push_back(candidates[i]);
#ifdef DEBUG_FIND_COMMON
				cout << "adding candidate "<<candidates[i].first<<","<<candidates[i].second<<endl;
#endif//DEBUG_FIND_COMMON
		}else{
#ifdef DEBUG_FIND_COMMON
		cout << "discard candidate "<<candidates[i].first<<","<<candidates[i].second<<endl;
#endif//DEBUG_FIND_COMMON
		}
	}
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int sub_genom_distance(genom p, pair<unsigned, unsigned> range, genom &next_perm, genom &alter, dstnc_inv *rd){
	genom sub,
		pos_id,
		neg_id;
	vector<int> sub_v;
	int dcp = std::numeric_limits< int >::max(),	// distance to the positive identity perm
		dcn = std::numeric_limits< int >::max(),	// --         "        --  negative --       "       --
		min_element = std::numeric_limits< int >::max();

	next_perm.clear();
	alter.clear();

		// get the subpermutation and its minimal element
	for(unsigned i=range.first; i<range.second; i++){
		sub_v.push_back(p[i]);
		if(abs(p[i]) < min_element)
			min_element = abs(p[i]);
	}

	sub.setChromosom(sub_v);
#ifdef DEBUG_SRAC
	cout << "sub ("<<range.first<<","<<range.second<<") ";
	//~ cout <<sub;
#endif
		// transform it into a permutation of the elements 1..sub.size()
	min_element--;
	for(unsigned i=0; i<sub.size(); i++){
		if (sub[i] > 0)
			sub[i] -= min_element;
		else
			sub[i] += min_element;
	}
#ifdef DEBUG_SRAC
	cout << " -> "<<sub;
#endif
		// construct the positive and neg identity permutations
		// compute the distance of the subperm. to the positive & negative identity permutation
		// for the complete permutation: only test to positive, i.e. the undirected permutation
		// model is implemented
	pos_id = genom(sub.size(), 0);
	dcp = rd->calc(sub, pos_id);
	if( range.first != 0 || range.second != p.size() ){
		neg_id = genom_nid(sub.size());
		dcn = rd->calc(sub, neg_id);
	}
#ifdef DEBUG_SRAC
	cout << " d(c+) = "<<dcp <<" d(c-) = "<< dcn <<" "<<endl;
#endif
		// if the distance to the positive id. perm is the smaller one OR
		// if the two distances are equal then sort
		// then subperm of next_perm to the positive
	if(dcp < dcn || dcp == dcn){
		next_perm = p;
		for(unsigned i=range.first; i<range.second; i++){
			next_perm[i] = pos_id[i-range.first]+min_element;
		}
	}
		// if the distance to the negative id. perm is the smaller one
		// then sort the subperm of next_perm to the positive
	if(dcn < dcp){
		next_perm = p;
		for(unsigned i=range.first; i<range.second; i++){
			next_perm[i] = neg_id[i-range.first]-min_element;
		}
	}
		// if the distances are equal then init the alternative permutation
		// as sorted to the negative
		// this must ALSO be done if the range is the complete permutation)
	if(dcn == dcp){
	//~ if(dcn == dcp && !(range.second-range.first == p.size())){
		alter = p;
		for(unsigned i=range.first; i<range.second; i++){
			alter[i] = neg_id[i-range.first]-min_element;
		}
	}

		// return the number of reversals needed
	//~ if(range.second-range.first == p.size()){
		//~ return dcp;
	//~ }else{
		if(dcn <= dcp){
			return dcn;
		}else{
			return dcp;
		}
	//~ }
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int srac(genom p, vector<vector<pair<unsigned, unsigned> > > ranges,
	unsigned cur_range_idx, unsigned cur_subrange_idx, dstnc_inv *rd ){

	int distance = 0,	// local distance
		dsub = 0;		// distance of subpermutations
	genom next_perm, 	// result of sorting a subrange
		alter;			// alternative result (if(d(c,c+) == d(c,c-)) )
#ifdef DEBUG_SRAC
	cout << "srac("<<cur_range_idx <<", "<<cur_subrange_idx <<"): "<<p<< endl;
#endif
		// for each set of ranges:
		// if more than one :
		//	- sort each range
		// 	- sort the union of the subintervals
		// if only one
		// 	- sort this
	for(; cur_range_idx<ranges.size(); cur_range_idx++){
#ifdef DEBUG_SRAC
		cout << "range : "<<cur_range_idx<< ":" << cur_subrange_idx <<endl;
#endif
			// if there is more than one subrange in this rangeset sort each subrange
			// for itself and than the range as whole
		if(ranges[cur_range_idx].size()>1){
			vector<int> min(ranges[cur_range_idx].size()-1),
				max(ranges[cur_range_idx].size()-1);
			bool inc = false,
				dec = false;

				// sort each subrange to positive or negative identity, depending on which possibility
				// is cheaper, if it is ambigous start recursion on both possibilities
			for(unsigned j=cur_subrange_idx; j<ranges[cur_range_idx].size()-1; j++){
				dsub = sub_genom_distance(p, ranges[cur_range_idx][j], next_perm, alter, rd);
				distance += dsub;
				if(alter.size() != 0){
					int a1, a2;
					a1 = srac(next_perm, ranges, cur_range_idx, j+1, rd);
					a2 = srac(alter, ranges, cur_range_idx, j+1, rd);
					if(a1 < a2){
						p = next_perm;
						//~ distance += a1;
					}else{
						p = alter;
						//~ distance += a2;
					}
				}else{
					p = next_perm;
				}
			}
			cur_subrange_idx = 0;

				// determine min and max per range: this is not done during
				// the for loop because if recursion - min and max are not known
			for(unsigned j=0; j<ranges[cur_range_idx].size()-1; j++){
				min[j] = abs( p[ ranges[cur_range_idx][j].first ] );
				max[j] = abs( p[ ranges[cur_range_idx][j].second-1 ] );
				if(min[j] > max[j])
					swap( min[j], max[j] );
			}
#ifdef DEBUG_SRAC
			cout << min.size()<<"(min,max): "<<endl;
			for(unsigned j=0; j<min.size(); j++)
				cout << "("<<min[j]<<","<<max[j]<<") ";
			cout << endl;
#endif
				// determine if the multi strip is inreasing or decreasing
			for(unsigned j=0; j<min.size()-1; j++){
				if(max[j]+1 == min[j+1]){
					inc |= true;
				}else if(min[j]-1 == max[j+1]){
					dec |= true;
				}else{
					cout << "cannot determine if block is inc-/decreasing" <<endl;
					exit(0);
				}
			}
				// sort increasing (increasing) strip by reversing negative (positive) blocks
			if(inc && !dec){
#ifdef DEBUG_SRAC
				cout << "increasing "<<endl;
#endif
				for(unsigned j=0; j<ranges[cur_range_idx].size()-1; j++){
					if(p[ ranges[cur_range_idx][j].first ] < 0){
						reverse(p, ranges[cur_range_idx][j].first, ranges[cur_range_idx][j].second-1);
						distance ++;
					}
				}
			}else if(dec && !inc){
#ifdef DEBUG_SRAC
				cout << "decreasing "<<endl;
#endif
				for(unsigned j=0; j<ranges[cur_range_idx].size()-1; j++){
					if(p[ ranges[cur_range_idx][j].first ] > 0){
						reverse(p, ranges[cur_range_idx][j].first, ranges[cur_range_idx][j].second-1);
						distance ++;
					}
				}
			}else{
				cout << "multistrip inc-&decreasing resp. nothing"<<endl;
				exit(0);
			}
#ifdef DEBUG_SRAC
			cout <<">>p"<<endl<< p<<endl;
#endif
		}
#ifdef DEBUG_SRAC
		cout << "now sort whole range"<<endl;
#endif
			// sort the whole range
		dsub = sub_genom_distance(p, ranges[cur_range_idx].back(), next_perm, alter, rd);
		distance += dsub;
		if(alter.size() != 0){
			int a1, a2;
			a1 = srac(next_perm, ranges, cur_range_idx+1, 0, rd);
			a2 = srac(alter, ranges, cur_range_idx+1, 0, rd);
			if(a1 < a2){
				p = next_perm;
				//~ distance += a1;
			}else{
				p = alter;
				//~ distance += a2;
			}
		}else{
			p = next_perm;
		}
#ifdef DEBUG_SRAC
		cout << "distance "<<distance<<endl;
#endif
	}
	return distance;

}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

int srac(genom &g1, genom &g2, int sig, int circular){

	vector<pair<unsigned, unsigned> > ci, 	// common intervals
		candidates;							// more possibly ci
	genom id, 								// identitiy permutation
		p;									// genom g2 'identified' relative to g1
	int distance = 0;
	vector<bool> range_borders;					// 1 if one of the overlapping interval starts or ends
	vector<vector<unsigned> > overlapping_int;	// indices of overlapping intervals
	vector<vector<pair<unsigned, unsigned> > > ranges;

	dstnc_inv *rd = new dstnc_inv(g1.size(), false);

	unsigned start_idx = 0, 	// start and end of ranges
		end_idx = 0,
		cur_start_idx = 0,
		cur_end_idx = 0;

		// init id and p
	id = genom(g1.size(), g1.getCircular());
	p = g2.identify_g(g1);

#ifdef DEBUG_SRAC
	cout <<"input "<<endl<<">g1"<<endl<< g1 << endl<<">g2"<<endl<< g2<<endl;
	cout <<"identitfy"<<endl<<" -> "<<endl<<">p"<<endl<<p<<endl<<">id"<<endl<<id<<endl;
#endif
		// get the irreducible common intervals (with replacing for circular ones)
	ci = find_common_intervals(p, id, sig, 1, circular, 1);

	if(candidates.size()){
		remove_reducible(ci, candidates, g1.size());
		sort(ci.begin(), ci.end());
	}

#ifdef DEBUG_SRAC
	cout << "irreducible common intervals "<<endl;
	for (unsigned i=0; i<ci.size(); i++)
		cout <<i<<" "<< ci[i].first<<","<<ci[i].second<<" "<<endl;
#endif


		// get the sets of overlapping common intervals
	vector<pair<int,int> > intci;
	for( unsigned i=0; i<ci.size(); i++ )
		intci.push_back( ci[i] );
	overlapping_ici(intci, overlapping_int);
#ifdef DEBUG_SRAC
	cout << "overlapping_int"<<endl;
	for(unsigned i=0; i<overlapping_int.size(); i++){
		cout << i << " -> " ;
		for(unsigned j=0; j<overlapping_int[i].size(); j++)
			cout << "["<< ci[ overlapping_int[i][j] ].first<<","<<ci[ overlapping_int[i][j] ].second <<"] ";
		cout << endl;
	}
#endif
		// construct the set of ranges
	ranges = vector<vector<pair<unsigned, unsigned> > >(overlapping_int.size());
	for(unsigned i=0; i<overlapping_int.size(); i++){
		range_borders.assign(p.size()+1, false);
		start_idx = ci[ overlapping_int[i][0] ].first;
		end_idx = 0;
		for(unsigned j=0; j<overlapping_int[i].size(); j++){
			range_borders[ ci[ overlapping_int[i][j] ].first ] = true;
			range_borders[ ci[ overlapping_int[i][j] ].second+1 ] = true;
			if( ci[ overlapping_int[i][j] ].second+1 > end_idx)
				end_idx = ci[ overlapping_int[i][j] ].second+1;
		}
		cur_start_idx = start_idx;
		for(unsigned j=start_idx; j<end_idx; j++){
			if(range_borders[j+1]){
				cur_end_idx = j+1;
				ranges[i].push_back(pair<unsigned, unsigned>(cur_start_idx, cur_end_idx));
				cur_start_idx = j+1;
			}
		}

		if(ranges[i].size()>1){
			ranges[i].push_back(pair<unsigned, unsigned>(ranges[i][0].first, ranges[i].back().second));
		}
	}

		// in the case of common intervals determined with sign information or
		// common intervals for circular permutations it is possible that not the whole
		// permutation is covered with common intervals, i.e. the function srac would return
		// a possibly unsorted permutation -> the uncovered area has to be sorted
		// (note that the sorted, i.e. covered area is sorted and consists therefore of
		// conserved adjacencies )  this can be done with the standard reversal distance
		// measure, because  permutations can be sorted optimally without cutting
		// conserved adjacencies [Tesler], i.e. the covered area.
		// therefor add artificial range (complete permutation) -> the work will be done automatically
	if(circular != 0 || sig != 0){
		ranges.push_back(vector<pair<unsigned, unsigned> >());
		ranges.back().push_back(pair<unsigned, unsigned>(0,p.size()));
	}


		// finally ensure that smaller ranges come first
	if(ranges.size()>1){
		sort(ranges.begin(), ranges.end(), compareRange);
	}
#ifdef DEBUG_SRAC
	cout << "ranges"<<endl;
	for(unsigned i=0; i<ranges.size(); i++){
		for(unsigned j=0; j<ranges[i].size();j++){
			cout <<"["<<ranges[i][j].first<<","<<ranges[i][j].second<<"] ";
		}
		cout << endl;
	}
#endif
	distance = srac(p, ranges, 0, 0, rd);
	delete rd;
	return distance;
}


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

extern "C" int srac_c(int *src_p, int *tgt_p, int sig, int circular,
	int n){

	genom src(src_p, n, circular),
		tgt(tgt_p, n, circular);

	return srac(src, tgt, sig, circular);
}


