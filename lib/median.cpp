#include <iostream>
#include <iterator>
#include <math.h>
#include <limits>
#include <numeric>

#include "common.hpp"
#include "median.hpp"

//#include "crex.hpp"
//#include "pqrtree.hpp"

//#define DEBUG_ANAMED
//#define DEBUG_TREXSOLVE
using namespace std;

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

mediansolver::~mediansolver(){}

unsigned mediansolver::circumference( const vector<genom> &genomes ) const{
	unsigned circ = 0;
	for(unsigned i=0; i<genomes.size(); i++){
		_dst->calc( genomes[i], genomes[(i+1)%genomes.size()] );
	}
	return circ;
}

dstnc *mediansolver::get_dstncfoo() const{
	return _dst;
}

unsigned mediansolver::lower_bound(const vector<genom> &problem) const {
	if (problem.size() != 3){
		cerr << "mediansolver::lower_bound: need three genomes"<<endl;
		exit(1);
	}
	return (unsigned) ceil( circumference(problem) / 2.0 );
}

unsigned mediansolver::upper_bound(const vector<genom> &problem ) const {
	if (problem.size() != 3){
		cerr << "mediansolver::upper_bound: need three genomes"<<endl;
		exit(1);
	}

	unsigned ub = std::numeric_limits< unsigned >::max(),
		ubt = 0;

	for( unsigned i=0; i<problem.size(); i++ ){
		ubt = 0;
		for( unsigned j=0; j<problem.size(); j++ ){
			if( i==j ){
				continue;
			}
			ubt += _dst->calc(problem[i], problem[j]);
		}
		if( ubt < ub ){
			ub = ubt;
		}
	}
	return ub;
}

void mediansolver::printopt() const {
	cerr << "mediansolver::printopt() should be implemented in the derived class"<<endl;
}

unsigned mediansolver::score( const vector<genom> &problem ) {
	int mediancnt;
	int score;
	vector<genom> medians;
	solve( problem, false, NO_BOUND, NO_BOUND, mediancnt, score, medians );

	return score;
}

ostream &operator<<(ostream &os, const mediansolver &ms){
        return ms.output(os);
}


unsigned mediansolver::spd(const vector<genom> &genomes) const{
	unsigned d01,
		d12,
		d20;

	if (genomes.size() != 3){
		cerr << "mediansolver::spd: need three genomes"<<endl;
		exit(EXIT_FAILURE);
	}

	d01 = _dst->calc( genomes[0], genomes[1] );
	d12 = _dst->calc( genomes[1], genomes[2] );
	d20 = _dst->calc( genomes[2], genomes[0] );

	return (((d01 > d20)?d01-d20:d20-d01) +
			((d01 > d12)?d01-d12:d12-d01) +
			((d20 > d12)?d20-d12:d12-d20));
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

mediancache::mediancache(mediansolver *solver){
	const mediancache* tmp;

	if( solver == NULL ){
		cerr << "mediancache called with NULL solver"<<endl;
		exit(0);
	}

	// don't allow recursive median caches
	tmp = dynamic_cast<const mediancache*>(solver);
	if( tmp != NULL ){
		cerr << "mediancache called with mediancache solver"<<endl;
		exit(0);
	}
	_solver = solver;
	_dst = solver->get_dstncfoo();
}

mediancache::~mediancache(){
	cout << "final mediancache size : "<< _cache.size()<<endl;

	delete _solver;
	_cache.clear();
	_score.clear();
	_medians.clear();
	_allmeds.clear();
}

//unsigned mediancache::distance( const genom &g1, const genom &g2, const vector<genom> &genomes ){
//	return _solver->distance( g1, g2, genomes );
//}

ostream &mediancache::output( ostream &os ) const {
	os << *_solver <<"(cached)";
	return os;
}

void mediancache::solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
		int &mediancnt, int &score, vector<genom> &medians) {

	int lb,
		ub;
	map<vector<genom>, int >::iterator cache_it;
	unsigned idx = std::numeric_limits< int >::max();

	// reset output
	medians.clear();
	mediancnt = 0;
	score = 0;

	cache_it = _cache.find( problem );

	// in any of the following cases the medians have to be computed
	// - median problem is not in the cache
	// - median problem is in the cache, but no medians are stored (upper bound was met in earlier computations)
	// - median problem is in the cache, but only one median was computed before and all medians are requested
	if(	cache_it == _cache.end() ||
		(cache_it != _cache.end() && _medians[ cache_it->second ].size() == 0 ) ||
		(cache_it != _cache.end() && allmed && not _allmeds[ cache_it->second ] ) ){

			// if we have a median, then we have a score which can be used for bounding
		if(cache_it != _cache.end() && _medians[ cache_it->second ].size() == 0 ){
			lb = _score[ cache_it->second ];
			ub = _score[ cache_it->second ];
		}else{
			lb = lower_bound;
			ub = upper_bound;
		}

		_solver->solve( problem, allmed, lower_bound, upper_bound, mediancnt, score, medians );

		// now update the cache
		// first get the position in the cache
		if( cache_it == _cache.end() ){
			idx = _medians.size();
			_cache[problem] = idx;
			_score.push_back( std::numeric_limits< int >::max() );
			_allmeds.push_back( false );
			_medians.push_back( vector<genom>() );
		}else{
			idx = cache_it->second;
		}

		if( score == std::numeric_limits< int >::max() || medians.size() == 0 ){
			_score[ idx ] = ub;
		}else{
			_score[ idx ] = score;
		}

		_allmeds[idx] = _allmeds[idx] | allmed;
		_medians[idx] = medians;
	}
	// otherwise the median(s) is/are in the cache ..
	else{
		if( score < lower_bound || score > upper_bound ){
			score = std::numeric_limits< int >::max();
		}
		else{
			mediancnt = _medians[ cache_it->second ].size();
			score = _score[ cache_it->second ];
			medians = _medians[ cache_it->second ];
		}
	}
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
unsigned cisolve::distance( const genom &g1, const genom &g2, const vector<genom> &genomes ){
	vector<pair<int, int> > comint;
	common_intervals(g1, g2, 0, 1, 0,  comint);
	//N1+N2-2CI
	return (g1.size()*(g1.size()+1)) - 2*comint.size();
}

ostream &cisolve::output( ostream &os ) const {
	os << "cisolve";
	return os;
}

vector<genom> cisolve::unpack(const vector<genom> &genomes) {
	unsigned n = genomes[0].size();
	vector<genom> renomes;	// the return genomes

	// make the unsigned as usual -> genomes of length 2n + 2
	for(unsigned i=0; i<genomes.size(); i++){
		renomes.push_back( genom(2*n + 2, 0) );
		renomes[i][0] = 0;
		for(unsigned j=0; j<genomes[i].size(); j++){
			if( genomes[i][j] > 0 ){
				renomes[i][2*j+1] = (2 * genomes[i][j]) - 1;
				renomes[i][2*j+2] = 2 * genomes[i][j];
			}else{
				renomes[i][2*j+1] = -2 * genomes[i][j];
				renomes[i][2*j+2] = (-2 * genomes[i][j]) - 1;
			}
		}
		renomes[i][2*n+1] = 2*n+1;
	}
	return renomes;
}

void cisolve::solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
		int &mediancnt, int &score, vector<genom> &medians) {

	if (problem.size() != 3){
		cerr << "cisolve::solve: need three genomes"<<endl;
		exit(1);
	}

	map< set<int>, unsigned > chr;	// the characters and the number of "presences"
	pqrtree pqr;					// a pqr tree
	set<int> tmpset;
	unsigned n = 0;

	n = problem[0].size();

	// iterate over all genomes and enumerate all intervalls
	for(unsigned i=0; i<problem.size(); i++){
		for(unsigned j=0; j<problem[i].size(); j++){
			tmpset.clear();
			for(unsigned k=j; k<problem[i].size(); k++){
				tmpset.insert(problem[i][k]);
					// exclude trivial sets :
					// - sets of size 1
					// - sets consisting of 2x-1, 2x for x \in 1..n
					// - the set containing all elements
				if(tmpset.size() < 2)
					continue;
				else if( tmpset.size() == 2 && ((problem[i][k]+1)/2 == (problem[i][k-1]+1)/2)){
					continue;
				}else if(tmpset.size() == n)
					continue;

					// if the set is not yet in the map -> insert
				if( chr.find(tmpset) == chr.end() ){
					chr[tmpset] = 1;
				}else{
					chr[tmpset]++;
				}
 			}
		}
	}

		// initialise universal pqr tree
	pqr = pqrtree(0, 2*n+1);
	for(unsigned i=1; i<=n; i++){
		tmpset.insert(2*i);
		tmpset.insert(2*i-1);
		pqr.reduce(tmpset);
		tmpset.clear();
	}

	for(map<set<int>, unsigned>::iterator it=chr.begin(); it!=chr.end(); it++){
		if( it->second > problem.size()/2 ){
			pqr.reduce(it->first);
		}
	}

	if( ! pqr.has_rnode() ){
		pqr.permutations( medians );
	}
}

vector<genom> cisolve::pack(const vector<genom> &genomes){
//	unsigned n = genomes[0].size();
//	vector<genom> renomes;	// the return genomes
//
//	// make the unsigned as usual -> genomes of length 2n + 2
//	for(unsigned i=0; i<genomes.size(); i++){
//		renomes.push_back( vector<int>(2*n + 2) );
//		renomes[i][0] = 0;
//		for(unsigned j=0; j<genomes[i].size(); j++){
//			if( genomes[i][j] > 0 ){
//				renomes[i][2*j+1] = (2 * genomes[i][j]) - 1;
//				renomes[i][2*j+2] = 2 * genomes[i][j];
//			}else{
//				renomes[i][2*j+1] = -2 * genomes[i][j];
//				renomes[i][2*j+2] = (-2 * genomes[i][j]) - 1;
//			}
//		}
//		renomes[i][2*n+1] = 2*n+1;
//	}
//	return renomes;
}
*/
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
unsigned trexsolve::distance( const genom &g1, const genom &g2, const vector<genom> &genomes ){
	crex *fwd,
		*bwd;
	unsigned d;

	fwd = new crex( g1, g2, _bps, _maxszen );
	bwd = new crex( g2, g1, _bps, _maxszen );
	d = min(fwd->size(ALTMIN), bwd->size(ALTMIN));
	delete fwd;
	delete bwd;
	return d;
}

ostream &trexsolve::output( ostream &os ) const {
	os << "trexsolve";
	return os;
}

void trexsolve::solve(const vector<genom> &problem, bool allmed, int lower_bound, int upper_bound,
		int &mediancnt, int &score, vector<genom> &medians) {

	rrrmt *tmps,			// temporary scenarios
		*tmpt;
	int s;
	map<genom, pair<unsigned, vector<bool> > > panc;	// putative ancestral genomes
		// and a pair containing the number of input genomes which generated them and the indices which generated them
	unsigned cons = 0;	// consistency
	vector<rrrmt*> cut;	// intersections of crex scenarios
	vector<genom> tmpg;

	medians.clear();
	if( problem.size() != 3 ){
		cerr << "error: trexsolve::solve is only implemented for three genomes"<<endl;
		exit(1);
	}

#ifdef DEBUG_TREXSOLVE
	cout << "problem "<<endl;
	cout << problem << endl;
#endif//DEBUG_TREXSOLVE
	cut = vector<rrrmt*>(problem.size(), NULL);
	for( unsigned i=0; i<problem.size(); i++ ){
		// get the intersection of scenarios toward i
		for(unsigned j=0; j<problem.size(); j++ ){
			if(i==j)
				continue;
#ifdef DEBUG_TREXSOLVE
			cout << j << " -> "<< i<< endl;
			tmps = new crex( problem[j], problem[i], _bps, _maxszen, _alt );
			cout << *tmps<< endl;
			delete tmps;
#endif//DEBUG_TREXSOLVE

			if( cut[i] == NULL ){
				cut[i] =  new crex( problem[j], problem[i], _bps, _maxszen, _alt );
			}else{
				tmpt = new crex( problem[j], problem[i], _bps, _maxszen, _alt );
				tmps = cut[i]->intersect( tmpt );
				delete tmpt;
				delete cut[i];
				cut[i] = tmps;
			}
		}

		// simplify before continuing
//		tmps = cut[i]->simplify();
//		if( tmps->isempty() ){
//			delete tmps;
//		}else{
//			delete cut[i];
//			cut[i] = tmps;
//		}

#ifdef DEBUG_TREXSOLVE
		cout << "-> intersection"<<endl;
		cout << *(cut[i]) <<endl;
		cout << "dapply for "<<problem[i]<<endl;
#endif//DEBUG_TREXSOLVE
		// reconstruct the ancestral genome implied by the intersection
//TODO implement deapplyall in rrrmt
		//tmpg = cut[i]->deapplyall(problem[i]);
#ifdef DEBUG_TREXSOLVE
		cout << "result of deapply"<<endl;
		for(unsigned j=0; j<tmpg.size(); j++){cout << tmpg[j]<<endl;}
#endif//DEBUG_TREXSOLVE
		for( unsigned k=0; k<tmpg.size(); k++ ){
			if( panc.find( tmpg[k] ) == panc.end() ){
				panc[tmpg[k]] = make_pair( 0, vector<bool>(cut.size(), false) );
			}
			panc[tmpg[k]].first++;
			cons = max( cons, panc[tmpg[k]].first);
			panc[tmpg[k]].second[i] = true;
		}
	}

		// determine the best consistency
	cons -= problem.size();

	// if the solution is allowed, i.e. it is consistent enough:
	//	- decide parsimonious between the putative ancestral genomes
	// else:
	//	- indicate that no solution was found
	if( cons <= _maxcons ){
			// remove those putative ancestral genomes which have not the maximal consistency
		for( map<genom, pair< unsigned, vector<bool> > >::iterator it = panc.begin(); it!=panc.end(); ){
			if( it->second.first - problem.size() != cons ){
				panc.erase(it++);
			}else{
				it++;
			}
		}

		score = std::numeric_limits< int >::max();
		for( map<genom, pair<unsigned, vector<bool> > >::iterator it = panc.begin(); it!=panc.end(); it++ ){
			s = 0;
			for( unsigned i=0; i<problem.size(); i++ ){
				tmps = new crex( it->first, problem[i], _bps, _maxszen );
				s += tmps->size(ALTMIN);
				delete tmps;
			}
			if( s <= score ){
				if( s<score ){
					score = s;
					medians.clear();
				}
				medians.push_back( it->first );
			}
		}
		mediancnt = medians.size();
	}else{
		mediancnt = 0;
		score = std::numeric_limits< int >::max();
	}

	// delete the intersection scenarios
	// we will use them later (not implemented yet)
	for( unsigned i=0; i<cut.size(); i++){
		delete cut[i];
	}
}
*/



// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//int lower_bound(const vector<genom> &genomes, int common, int pair_pd, hdata &hd){
//
//	if (genomes.size() != 3){
//		cerr << "lower_bound: need three genomes"<<endl;
//		exit(1);
//	}
//
//	return (int) ceil( circumference(genomes, common, pair_pd, hd) / 2.0 );
//}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

median_hcenum::median_hcenum( dstnc *dst, unsigned n, bool circular ){
	_dst = dst;
	// get all signed permutations of length n
	get_all_permutations(n, circular, true, _cand );
}

median_hcenum::~median_hcenum(){
	_cand.clear();
}

ostream &median_hcenum::output( ostream &os ) const{
	os << "GEM";
	return os;
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void median_hcenum::solve(const vector<genom> &problem, bool allmed,
		int lower_bound, int upper_bound, int &mediancnt, int &score,
		vector<genom> &medians){

	unsigned scand = 0;

	medians.clear();
	score = std::numeric_limits< int >::max();

	for(unsigned i=0; i<_cand.size(); i++){
		scand = _dst->calcsum(_cand[i], problem, problem);

		// check for the lower bound. note: 0 check holds per definition
		if( scand < lower_bound && lower_bound != std::numeric_limits< int >::max() ){
			continue;
		}

		// check for the upper bound
		if( scand > upper_bound && upper_bound != 0 ){
			continue;
		}

		if (scand <= score){
			if (scand < score){
				score = scand;
				medians.clear();
			}
			medians.push_back(_cand[i]);
		}
	}
	mediancnt = medians.size();
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//int upper_bound(const vector<genom> &genomes, int common, int pair_pd, hdata &hd){
//	int d01,
//		d12,
//		d20,
//		ub = std::numeric_limits< int >::max();
//
////	cerr << "reimplement sum_of_pairwise_differences in median.cpp !!!"<<endl;
////	exit(EXIT_FAILURE);
//
//	if (genomes.size() != 3){
//		cerr << "upper_bound: need three genomes"<<endl;
//		exit(1);
//	}
//
//	if( common == 0 ){
//		d01 = genomes[0].distance(genomes[1], hd);
//		d12 = genomes[1].distance(genomes[2], hd);
//		d20 = genomes[2].distance(genomes[0], hd);
//	}else{
//		if( pair_pd == 0 ){
//			d01 = perfect_distance_glob(genomes[0], genomes[1], genomes, genomes[0].size(), hd);
//			d12 = perfect_distance_glob(genomes[1], genomes[2], genomes, genomes[0].size(), hd);
//			d20 = perfect_distance_glob(genomes[2], genomes[0], genomes, genomes[0].size(), hd);
//		}else{
//			d01 = perfect_distance(genomes[0], genomes[1], hd);
//			d12 = perfect_distance(genomes[1], genomes[2], hd);
//			d20 = perfect_distance(genomes[2], genomes[0], hd);
//		}
//	}
//
//	ub = d01 + d20;
//	if ( d01 + d12 < ub )
//		ub = d01 + d12;
//	if ( d20 + d12 < ub )
//		ub = d20 + d12;
//
//	return ub;
//}
