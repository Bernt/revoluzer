/*
 * dstnc_coml.h
 *
 * distance = sum of the lengths of the common intervals
 *
 *  Created on: Mar 31, 2011
 *      Author: maze
 */

#ifndef DSTNC_COML_H_
#define DSTNC_COML_H_

#include "dstnc.hpp"

class dstnc_coml: public dstnc {
private:
	bool _circular; /* circularity */
	unsigned _n,	/* length of the considered gene orders */
		_max;		/* maximal number of common intervals for the length */
public:
	/**
	 * constructor
	 * @param[in] n length of the genomes
	 */
	dstnc_coml( unsigned n, bool circular );

	/**
	 * destructor
	 */
	virtual ~dstnc_coml();

	/**
	 * adapt the length weighted common interval distance calculator to a new genome length
	 * @param[in] n the new length
	 */
	void adapt(unsigned n);

	/**
	 * get the length weighted common interval distance between two gene orders
	 * @param[in] src a genome
	 * @param[in] tgt another genome
	 * @param[in] context @see distance::calc
	 * @return the reversal distance
	 */
	unsigned calc( const genom &src, const genom &tgt );

	/**
	 * return a pointer to a copy of a dstnc
	 * this is a virtual copy constructor
	 * @return the pointer
	 */
	dstnc* clone() const;

//	/**
//	 * virtual constructor
//	 * @return an empty instance of an object
//	 */
//	dstnc* create() const;

	/**
	 * output function
	 * @param[in] stream to write into
	 * @return the stream
	 */
	ostream & output(ostream &os) const;

};

#endif /* DSTNC_COML_H_ */
