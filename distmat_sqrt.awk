# awk skript to sqrt a distance matrix
# usage: cat DISTMATFILE | awk -f distmal_sqrt.awk

BEGIN{
	n=0;
}
{
if(n==0){
	print "   ",$1;
}else{
	printf("%-10s ",$1); 
	for(i=2; i<=NF; i++){
		printf("%f ", sqrt($(i)));
	} printf("\n");
}
n++;
}
